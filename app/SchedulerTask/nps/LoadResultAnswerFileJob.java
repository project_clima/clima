/**
 * @(#) LoadResultAnswerFileJob 28/03/2017
 */
package SchedulerTask.nps;

import com.jcraft.jsch.JSchException;
import java.io.File;
import java.io.FileOutputStream;
import java.util.List;
import jobs.nps.EmployeeEvaluatedJob;
import models.nps.FileNPS;
import models.nps.NpsAnswerTmpLog;
import play.Logger;
import play.jobs.Job;
import play.jobs.On;
import querys.nps.StructureTmpQuery;
import utils.nps.enums.LogFileNPSEnum;
import utils.nps.log.LogFileNPS;
import utils.nps.readFile.ReadTxtFile;
import utils.nps.sftp.SFTPConnection;
import utils.nps.vars.EnvironmentVar;
import utils.nps.writeFile.WriteLogFile;

/**
 * Job para la carga automatica de las calificaciones
 * @author Rodolfo Miranda -- Qualtop
 */
@On("0 0 4 * * ?")
public class LoadResultAnswerFileJob extends Job {

    @Override
    public void doJob() {
        // Read NpsAnswer file
        ReadTxtFile readTxtFile = new ReadTxtFile();
        File file = new File("temp.txt");
        FileOutputStream oFile;

        SFTPConnection sftpConnection = new SFTPConnection(EnvironmentVar.HOST,
                EnvironmentVar.USER, EnvironmentVar.PASSWORD,
                EnvironmentVar.PORT, EnvironmentVar.DIRECTORY);
        Logger.info("Inicio de la carga automatica de evaluaciones ...");
        try {
            sftpConnection.configSftp();
            sftpConnection.changeDirectory(EnvironmentVar.DIRECTORY);

            List<String> filesName = sftpConnection.
                    getVectorFiles(EnvironmentVar.DIRECTORY);
            if (filesName != null && !filesName.isEmpty()) {
                for (String fileName : filesName) {
                    if (!sftpConnection.getPath().equalsIgnoreCase(EnvironmentVar.DIRECTORY)) {
                        sftpConnection.changeDirectory(EnvironmentVar.DIRECTORY);
                    }
                    oFile = new FileOutputStream(file);
                    sftpConnection.getFile(fileName, oFile);
                    oFile.flush();
                    oFile.close();
                    readTxtFile.setFileReader(file);
                    //Delete Struct-Temp rows
                    Integer deleted = StructureTmpQuery.truncateTableAns();
                    // Read file
                    boolean success = readTxtFile.readTxtFile(null, false, null);

                    if (success) {
                        //File NPS registernull
                        FileNPS fileNps = new FileNPS();
                        LogFileNPS.updateLog(LogFileNPSEnum.AUTO, fileNps, fileName, "ANSWER", null);
                        fileNps.save();
                        // Load to DB
//                    for( NpsAnswerTmp record : records ){
//                        record.save();
//                    }
                        checkConnection(sftpConnection);
                        sftpConnection.changeDirectory(EnvironmentVar.DIRECTORY_PROCE);
                        sftpConnection.putFile(file.getName(), fileName);
                        sftpConnection.changeDirectory(EnvironmentVar.DIRECTORY);
                        sftpConnection.rmFile(fileName);
                        //Call Function PL/SQL||
                        int error = StructureTmpQuery.
                                callStoreProcedureLoadStructure(fileNps.idFile, "ANSWER", "");

                        if (error > 0) {
                            List<NpsAnswerTmpLog> listNpsLog = NpsAnswerTmpLog.findAll();
                            File log = WriteLogFile.writeLogNpsFile(listNpsLog);
                            int i = fileName.lastIndexOf('.');
                            String newName = fileName.substring(0, i);
                            checkConnection(sftpConnection);
                            sftpConnection.changeDirectory(EnvironmentVar.DIRECTORY_ERROR);
                            sftpConnection.putFile(log.getName(), newName + "_ERROR." + EnvironmentVar.TXT_EXT);
                        }
                        
                        //Inicia el envío de mails 
                        new EmployeeEvaluatedJob().now();
                    }
                }
            }
            Logger.info("Fin de la carga automatica de evaluaciones ...");
        } catch (Exception ex) {
            Logger.info("Error en la carga automatica de evaluaciones ..." + ex.getMessage());
        } finally {
            sftpConnection.disconnect();
        }
    }

    public void checkConnection(SFTPConnection sftpConnection) throws JSchException {
        boolean connected = true;
        boolean connectedChannel = true;

        connected = sftpConnection.getStatusSession();
        connectedChannel = sftpConnection.getStatusChannel();
        if (!connected || !connectedChannel) {
            sftpConnection.disconnect();
            sftpConnection.configSftp();
        }
    }
}
