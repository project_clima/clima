/**
 * @(#) GetAudioFilesJob 26/06/2017
 */
package SchedulerTask.nps;

import com.jcraft.jsch.JSchException;
import com.jcraft.jsch.SftpException;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;
import java.util.logging.Level;
import models.nps.NpsAnswerTmp;
import play.jobs.Job;
import utils.nps.sftp.SFTPConnection;
import utils.nps.vars.EnvironmentVar;

/**
 * Job para obtner los archivos de audio despues de una carga de Evaluaciones
 * @author Rodolfo Miranda -- Qualtop
 */
public class GetAudioFilesJob extends Job {

    public GetAudioFilesJob() {

    }

    /**
     * Metodo para la ejecucion del Job
     */
    public void doJob() {

        List<NpsAnswerTmp> answers = NpsAnswerTmp.findAll();
        if (answers != null && !answers.isEmpty()) {

            File media = new File(EnvironmentVar.MEDIA_DIRECTORY);
            boolean exits = true;

            //Conexion al SFTP
            SFTPConnection sftpConnection = new SFTPConnection(EnvironmentVar.HOST,
                    EnvironmentVar.USER, EnvironmentVar.PASSWORD,
                    EnvironmentVar.PORT, EnvironmentVar.DIRECTORY_RECORDS);

            try {

                if (!media.exists()) {
                    exits = media.mkdirs();
                }

                if (exits) {
                    sftpConnection.configSftp();
                    for (NpsAnswerTmp answer : answers) {

                        if (answer.recording != null) {

                            Path path = Paths.get(media.getPath() + File.separator + answer.recording);
                            if (Files.notExists(path)) {

                                FileOutputStream oFile = null;
                                try {
                                    sftpConnection.changeDirectory(EnvironmentVar.DIRECTORY_RECORDS);

                                    String month = answer.month;
                                    String day = answer.day;
                                    String year = answer.year;

                                    if (day.length() < 2) {
                                        day = "0" + day;
                                    }
                                    if (month.length() < 2) {
                                        month = "0" + month;
                                    }

                                    String folder = year + month;

                                    sftpConnection.
                                            changeDirectory(EnvironmentVar.DIRECTORY_RECORDS);
                                    sftpConnection.
                                            changeDirectory(EnvironmentVar.DIRECTORY_RECORDS +
                                                    "/" + folder + "/" + day);

                                    String filePro = media.getPath() + File.separator + answer.recording;

                                    oFile = new FileOutputStream(filePro);

                                    sftpConnection.getFile(answer.recording, oFile);
                                    oFile.flush();
                                    oFile.close();

                                } catch (SftpException ex) {
                                    if (oFile != null) {
                                        oFile.flush();
                                        oFile.close();
                                        Files.delete(path);
                                    }
                                } finally {

                                }
                            }
                        }
                    }
                }
            } catch (JSchException ex) {
                java.util.logging.Logger.
                        getLogger(LoadNPSStructureJob.class.getName()).
                        log(Level.SEVERE, null, ex);
            } catch (FileNotFoundException ex) {
                java.util.logging.Logger.getLogger(LoadNPSStructureJob.class.
                        getName()).log(Level.SEVERE, null, ex);
            } catch (IOException ex) {
                java.util.logging.Logger.getLogger(LoadNPSStructureJob.class.
                        getName()).log(Level.SEVERE, null, ex);
            } finally {
                sftpConnection.disconnect();
            }
        }
    }

}
