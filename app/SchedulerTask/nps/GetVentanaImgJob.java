/**
 * @(#) GetVentanaImgJob 7/08/2017
 */
package SchedulerTask.nps;

import com.jcraft.jsch.JSchException;
import com.jcraft.jsch.SftpException;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;
import play.Play;
import play.jobs.Job;
import play.jobs.On;
import utils.nps.sftp.SFTPConnection;
import utils.nps.vars.EnvironmentVar;

/**
 * Proceso para integrar del SFTP una imagen en el inicio del NPS
 * @author Rodolfo Miranda -- Qualtop
 */
@On("0 0 9 * * ?")
public class GetVentanaImgJob extends Job {

    public GetVentanaImgJob() {

    }

    public void doJob() {

        // Conexion al SFTP
        SFTPConnection sftpConnection = new SFTPConnection(EnvironmentVar.HOST,
                EnvironmentVar.USER, EnvironmentVar.PASSWORD,
                EnvironmentVar.PORT, EnvironmentVar.VENTANA_DIRECTORY);
        String path = Play.applicationPath + File.separator + "public" + File.separator + 
                "assets" +File.separator+ "images" +File.separator+  EnvironmentVar.VENTANA;
        FileOutputStream oFile = null;
        try {
            sftpConnection.configSftp();         
            oFile = new FileOutputStream(path);
            sftpConnection.changeDirectory(EnvironmentVar.VENTANA_DIRECTORY);
            sftpConnection.getFile(EnvironmentVar.VENTANA, oFile);
            
        } catch (SftpException ex) {
            Logger.getLogger(GetVentanaImgJob.class.getName()).log(Level.SEVERE, null, ex);
        } catch (FileNotFoundException ex) {
            Logger.getLogger(GetVentanaImgJob.class.getName()).log(Level.SEVERE, null, ex);
        } catch (JSchException ex) {
            Logger.getLogger(GetVentanaImgJob.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            sftpConnection.disconnect();
            if (oFile != null) {
                try {
                    oFile.flush();
                    oFile.close();
                    
                } catch (IOException ex) {
                    Logger.getLogger(GetVentanaImgJob.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        }

    }
}
