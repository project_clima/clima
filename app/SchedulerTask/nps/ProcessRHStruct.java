package SchedulerTask.nps;

import com.jcraft.jsch.JSchException;
import com.jcraft.jsch.SftpException;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.Serializable;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import javax.mail.MessagingException;
import javax.persistence.EntityManager;
import javax.persistence.Query;
import jobs.nps.EmployeeEvaluatedJob;
import models.Sections;
import models.StoreEmployee;
import models.Usuario;
import models.nps.FilesLoadNps;
import models.nps.FunctionByTitle;
import models.nps.NpsAnswerTmpLog;
import models.nps.NpsArea;
import models.nps.NpsEmpNotSales;
import models.nps.NpsPractice;
import models.nps.StructureTmpLog;
import play.Logger;
import play.db.jpa.JPA;
import play.jobs.Job;
import play.libs.Codec;
import play.libs.F;
import querys.nps.StructureTmpQuery;
import utils.GmailMailer;
import utils.nps.readFile.ReadCatAreaFile;
import utils.nps.readFile.ReadCatFunctionFile;
import utils.nps.readFile.ReadCatNotSalesFile;
import utils.nps.readFile.ReadCatPractFile;
import utils.nps.readFile.ReadCatSectionFile;
import utils.nps.readFile.ReadCatStoreFile;
import utils.nps.readFile.ReadCsvFile;
import utils.nps.readFile.ReadTxtFile;
import utils.nps.sftp.SFTPConnection;
import utils.nps.vars.EnvironmentVar;
import utils.nps.writeFile.WriteLogFile;

/**
 * Job para el procesado de las peticiones de las cargas
 * Example job that publishes progress as an event stream.
 */
public class ProcessRHStruct extends Job implements Serializable {

    private static final Object LOCK1 = new Object();
    private static final Object LOCK2 = new Object();
    
    public static Map<String, ProcessRHStruct> registry = new HashMap<String, ProcessRHStruct>();
    public String id = Codec.UUID();
    public String file = "";
    public String title = "";
    public String load_sap = "";
    public String userId = "";
    public String exception = "";
    public F.EventStream<Integer> percentComplete = new F.EventStream<Integer>();
    public String typeOperation = "";
    public Long loadFile = null;
    public int loadNps = 0;

    /**
     * Event stream for events that report job completion percentage.
     */
    public void doJob() {
        String fileName = "";
 
        try {
            final int percent = 25;
            int error = 0, errorCat = 0;
            File tmp = new File(file);
            List<String> errors = null;
            if (typeOperation.equals("sap")) {

                synchronized(LOCK1){
                    ReadCsvFile reader = new ReadCsvFile();
                    //File tmp = new File(file);
                    fileName = tmp.getName();
                    reader.setFileReader(tmp);
                    Integer deleted = StructureTmpQuery.truncateTableRH();
                    
                    // Read file
                    boolean success = reader.readCvsFile(this, true,loadFile);
                    tmp.delete();

                    if (success) {
                       
                        //Call Function PL/SQL
                        error = StructureTmpQuery.
                                callStoreProcedureLoadStructure(loadNps,
                                        "STRUCT", "FN_INS_STRUCTURE_PARTIAL");
                        
                        updatePercent((100 + error), loadFile);
                                               
                        sendFileErrors( error,  typeOperation, fileName);
                    }
                }
            } else if (typeOperation.equals("bd")) {
                synchronized(LOCK2){
                    ReadTxtFile reader = new ReadTxtFile();
                    //File tmp = new File(file);
                    fileName = tmp.getName();
                    reader.setFileReader(tmp);
                    Integer deleted = StructureTmpQuery.truncateTableAns();
                    
                    // Read file
                    boolean success = reader.readTxtFile(this, true, loadFile);
                    tmp.delete();
                    
                    if (success) {
       
                        error = StructureTmpQuery.
                                callStoreProcedureLoadStructure(loadNps,
                                        "ANSWER", "");
                        
                        
                        updatePercent((100 + error), loadFile);
                        
                        sendFileErrors( error,  typeOperation, fileName);
                        //Inicia el envío de mails 
                        new EmployeeEvaluatedJob().now();

                        //Traer los adios
                        new GetAudioFilesJob().now();
                    }
                }
            } else {
                fileName = tmp.getName();
                if (typeOperation.equals("ct")) {
                    ReadCatStoreFile catStore = new ReadCatStoreFile();
                    catStore.setFileReader(tmp);
                    
                    List<StoreEmployee> stores = catStore.readCvsFile(userId, true);
                    updatePercent(percent + 50, loadFile);
                    tmp.delete();
                    //File NPS register
                    
                    int total = stores.size();
                    float factor = (float) 25 / total;
                    int i = 1;
                    int j = 1;
                    for (StoreEmployee store : stores) {
                        updateZoneStore(store.idStore, store.zoneNps);
                        store.save();

                        if ((factor * i) >= 1.0) {
                            updatePercent(percent + 45 + j, loadFile);
                            i = 0;
                            j++;
                        }
                        i++;
                    }
                    updatePercent(100 + catStore.getErrores().size(), loadFile);
                   
                } else if (typeOperation.equals("ca")) {
                    ReadCatAreaFile catArea = new ReadCatAreaFile();
                    catArea.setFileReader(tmp);
                    List<NpsArea> areas = catArea.readCvsFile(userId);
                    tmp.delete();
                    
                    updatePercent(percent + 50, loadFile);
                    //File NPS register
                    
                    int total = areas.size();
                    float factor = (float) 25 / total;
                    int i = 1;
                    int j = 1;
                    for (NpsArea area : areas) {
                        area.save();
                        if ((factor * i) >= 1.0) {
                            updatePercent(percent + 45 + j, loadFile);
                            i = 0;
                            j++;
                        }
                        i++;
                    }
                    updatePercent(100 + catArea.getErrores().size(), loadFile);
                    
                } else if (typeOperation.equals("cp")) {
                    ReadCatPractFile catPract = new ReadCatPractFile();
                    catPract.setFileReader(tmp);
                    List<NpsPractice> practices = catPract.readCvsFile(userId);
                    
                    updatePercent(percent + 40, loadFile);
                    tmp.delete();
                    //File NPS register
                   
                    int total = practices.size();
                    float factor = (float) 25 / total;
                    int i = 1;
                    int j = 1;
                    for (NpsPractice pract : practices) {
                        pract.save();
                        if ((factor * i) >= 1.0) {
                            updatePercent(percent + 45 + j, loadFile);
                            i = 0;
                            j++;
                        }
                        i++;
                    }
                    updatePercent(100 + catPract.getErrores().size(), loadFile);
                    
                } else if (typeOperation.equals("cs")) {
                    ReadCatSectionFile reader = new ReadCatSectionFile();
                    reader.setFileReader(tmp);
                    // Read file
                    List<Sections> sections = reader.readCvsFile(userId);
                    
                    updatePercent(percent + 40, loadFile);
                    tmp.delete();
                    
                    int total = sections.size();
                    float factor = (float) 25 / total;
                    int i = 1;
                    int j = 1;
                    for (Sections section : sections) {
                        section.save();
                        if ((factor * i) >= 1.0) {
                            updatePercent(percent + 45 + j, loadFile);
                            i = 0;
                            j++;
                        }
                        i++;
                    }
                    updatePercent(100 + reader.getErrores().size(), loadFile);

                } else if (typeOperation.equals("cf")) {
                    ReadCatFunctionFile reader = new ReadCatFunctionFile();
                    reader.setFileReader(tmp);
                    // Read file
                    List<FunctionByTitle> functions = reader.readCvsFile(userId);
                    
                    updatePercent(percent + 40, loadFile);
                    tmp.delete();
                    
                    int total = functions.size();
                    float factor = (float) 25 / total;
                    int i = 1;
                    int j = 1;
                    for (FunctionByTitle function : functions) {
                        function.save();
                        if ((factor * i) >= 1.0) {
                            updatePercent(percent + 45 + j, loadFile);
                            i = 0;
                            j++;
                        }
                        i++;
                    }
                    updatePercent(100 + reader.getErrores().size(), loadFile);

                } else if (typeOperation.equals("cnv")) {
                    ReadCatNotSalesFile reader = new ReadCatNotSalesFile();
                    reader.setFileReader(tmp);
                    // Read file
                    List<NpsEmpNotSales> notSales = reader.readCvsFile(userId);
                    
                    updatePercent(percent + 40, loadFile);
                    tmp.delete();
                    
                    int total = notSales.size();
                    float factor = (float) 25 / total;
                    int i = 1;
                    int j = 1;
                    for (NpsEmpNotSales notSale : notSales) {
                        notSale.save();
                        if ((factor * i) >= 1.0) {
                            updatePercent(percent + 45 + j, loadFile);
                            i = 0;
                            j++;
                        }
                        i++;
                    }
                    updatePercent(100 + reader.getErrores().size(), loadFile);

                }
            }

            
        }catch (Exception ex) {
            //updateError("Error " + ex.toString(), loadFile);
            exception = ex.getMessage();
            Logger.info("Error en la carge de archivos " + ex.getMessage());
          
            updateError("Error en la carga de archivos " + exception, loadFile); 
        }finally {
            sendEmailTaskComplete();
        }
    }

    public void sendFileErrors(int error, String typeOperation, String fileName){
        if (error > 0) {
            SFTPConnection sftpConnection = null;
            try {
                
                sftpConnection = new SFTPConnection(EnvironmentVar.HOST,
                        EnvironmentVar.USER, EnvironmentVar.PASSWORD,
                        EnvironmentVar.PORT, EnvironmentVar.DIRECTORY);
                sftpConnection.configSftp();
                if (typeOperation.equals("sap")) {
                    List<StructureTmpLog> listStrcLog = StructureTmpLog.findAll();
                    File log = WriteLogFile.writeLogStrcFile(listStrcLog);
                    int i = fileName.lastIndexOf('.');
                    String newName = fileName.substring(0, i);
                    //checkConnection(sftpConnection);
                    sftpConnection.changeDirectory(EnvironmentVar.DIRECTORY_STRUCT_ERR);
                    sftpConnection.putFile(log.getName(), newName + "_ERROR." + EnvironmentVar.CSV_EXT);
                } else if (typeOperation.equals("bd")) {
                    List<NpsAnswerTmpLog> listNpsLog = NpsAnswerTmpLog.findAll();
                    File log = WriteLogFile.writeLogNpsFile(listNpsLog);
                    int i = fileName.lastIndexOf('.');
                    String newName = fileName.substring(0, i);
                    //checkConnection(sftpConnection);
                    sftpConnection.changeDirectory(EnvironmentVar.DIRECTORY_ERROR);
                    sftpConnection.putFile(log.getName(), newName + "_ERROR." + EnvironmentVar.TXT_EXT);
                }
            } catch (JSchException ex) {
                Logger.error(ex.getMessage());
            } catch (IOException ex) {
                Logger.error(ex.getMessage());
            } catch (SftpException ex) {
                Logger.error(ex.getMessage());
            }finally {
                 if (sftpConnection != null) {
                    sftpConnection.disconnect();
                }
            }

            }
    }
    
    public void sendEmailTaskComplete() {
        if (userId == null || userId.equals("")) {
            return;
        }

        Usuario user = Usuario.find("UPPER(username) = UPPER(?1)", userId.toUpperCase()).first();

        try {
            GmailMailer mailer = new GmailMailer();

            mailer.sendGeneral(
                    user.email,
                    "NPS :: Carga Manual de Archivos (" + typeOperation.toUpperCase() + ")",
                    "El proceso de carga manual ha finalizado (" + typeOperation.toUpperCase() + ").",
                    null
            );
        } catch (MessagingException e) {
            java.util.logging.Logger.getLogger(
                    ProcessRHStruct.class.getName()
            ).log(Level.SEVERE, null, e);
        }

    }

    public void updatePercent(int percentToUpdate, Long loadFile) {
        FilesLoadNps fileLoad = new FilesLoadNps();
        fileLoad.idFileLoad = loadFile;
        fileLoad.comments = "Procesando ...";
        fileLoad.percent = new Float(percentToUpdate);
        //em.clear();
        //em.close();
        saveFile(fileLoad);
    }
    
    public void updateError(String comment, Long loadFile) {
        FilesLoadNps fileLoad = new FilesLoadNps();
        fileLoad.idFileLoad = loadFile;
        fileLoad.comments = comment;
        fileLoad.percent = 0f;
        //em.clear();
        //em.close();
        saveFile(fileLoad);
    }

    private void updateZoneStore(Integer idStore, String idParent) {

        String statement = "UPDATE PORTALUNICO.PUL_STORE str\n"
                + "SET str.FC_ZONE_NPS = ?1 \n"
                + "WHERE str.FN_ID_STORE = ?2 ";

        EntityManager em = JPA.em();
        Query query = em.
                createNativeQuery(statement);
        query.setParameter(1, idParent);
        query.setParameter(2, idStore);
        
        query.executeUpdate();
        
    }

    private void writeErrors(List<String[]> errors) {
        //WriteLogFile.writeTmpLogNpsFile(errors);
    }
    
    private static void saveFile(FilesLoadNps fileLoad) {
        String filePro = EnvironmentVar.TXTS_DIRECTORY + File.separator + fileLoad.idFileLoad + "_.txt";
        
        try{
            File file = new File(filePro);

            if (file.exists()) {
                FileWriter writer = new FileWriter(file);
                writer.write(fileLoad.idFileLoad+","+fileLoad.percent+","+fileLoad.comments);
                writer.close();
            }
        } catch(IOException e){
            Logger.info("Error " + e.getMessage());
        }
    }
}
