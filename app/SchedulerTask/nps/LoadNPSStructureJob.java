/**
 * @(#) LoadOfStructureNps 15/03/2017
 */
package SchedulerTask.nps;

import com.jcraft.jsch.JSchException;
import com.jcraft.jsch.SftpException;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.sql.SQLException;
import java.text.ParseException;
import java.util.List;
import java.util.logging.Level;
import models.nps.FileNPS;
import models.nps.StructureTmp;
import models.nps.StructureTmpLog;
import play.Logger;
import play.jobs.Job;
import play.jobs.On;
import querys.nps.StructureTmpQuery;
import utils.nps.enums.LogFileNPSEnum;
import utils.nps.log.LogFileNPS;
import utils.nps.readFile.ReadCsvFile;
import utils.nps.sftp.SFTPConnection;
import utils.nps.vars.EnvironmentVar;
import utils.nps.writeFile.WriteLogFile;

/**
 * Job para la carga de estructura.
 * @author Rodolfo Miranda -- Qualtop
 */
@On("0 0 3 * * ?")
public class LoadNPSStructureJob extends Job {

    public String typeOperation = "";

    public String fileU = "";
    

    @Override
    public void doJob() {
        // Validate existence file Mapeo_Estructura_Sap_NPS.xlsx
        ReadCsvFile readCvsFile = new ReadCsvFile();
        File file = new File("temp.csv");
        String fileName = "";
        SFTPConnection sftpConnection = null;
        boolean automatic = true;

        Logger.info("Inicio de la carga automatica de estructura ...");
        
        try {
            //Conexion al SFTP
            sftpConnection = new SFTPConnection(EnvironmentVar.HOST,
                    EnvironmentVar.USER, EnvironmentVar.PASSWORD,
                    EnvironmentVar.PORT, EnvironmentVar.DIRECTORY);

            sftpConnection.configSftp();
            if (typeOperation != null && typeOperation.equals("")) {

                FileOutputStream oFile;
                sftpConnection.changeDirectory(EnvironmentVar.DIRECTORY_STRUCT);

                oFile = new FileOutputStream(file);
                fileName = sftpConnection.
                        getStructureFile(EnvironmentVar.DIRECTORY_STRUCT);
                if (fileName != null && !fileName.equals("")) {
                    sftpConnection.getFile(fileName, oFile);
                    oFile.flush();
                    oFile.close();
                } else {
                    throw new IOException("No hay archivos de estructura");
                }

            } else if (typeOperation != null && typeOperation.equals("manual")) {
                file = new File(fileU);
                fileName = file.getName();
                automatic = false;
            }
            readCvsFile.setFileReader(file);

            //Delete Struct-Temp rows
            Integer deleted = StructureTmpQuery.truncateTableRH();
            // Read file
            boolean success = readCvsFile.readCvsFile(null, false,null);

            if (success) {
                //File NPS register
                FileNPS fileNps = new FileNPS();
                LogFileNPS.updateLog(LogFileNPSEnum.AUTO, fileNps, fileName, "STRUCT", null);
                fileNps.save();

                // Load to DB
//                for (StructureTmp record : records) {
//                    record.save();
//                }
                if (automatic) {
                    checkConnection(sftpConnection);
                    sftpConnection.changeDirectory(EnvironmentVar.DIRECTORY_STRUCT_PRO);
                    sftpConnection.putFile(file.getName(), fileName);
                    sftpConnection.changeDirectory(EnvironmentVar.DIRECTORY_STRUCT);
                    sftpConnection.rmFile(fileName);
                }

                //Call Function PL/SQL
                int error = StructureTmpQuery.
                        callStoreProcedureLoadStructure(fileNps.idFile,
                                "STRUCT", "FN_INS_STRUCTURE");
                if (error > 0) {
                    List<StructureTmpLog> listStrcLog = StructureTmpLog.findAll();
                    File log = WriteLogFile.writeLogStrcFile(listStrcLog);
                    int i = fileName.lastIndexOf('.');
                    String newName = fileName.substring(0, i);
                    checkConnection(sftpConnection);
                    sftpConnection.changeDirectory(EnvironmentVar.DIRECTORY_STRUCT_ERR);
                    sftpConnection.putFile(log.getName(), newName + "_ERROR." + EnvironmentVar.CSV_EXT);
                }

            }
            Logger.info("Fin de la carga automatica de estructura ...");
        } catch (Exception ex) {
            Logger.info("Error de la carga automatica de estructura ..." + ex.getMessage());
        }  finally {
            if (sftpConnection != null) {
                sftpConnection.disconnect();
            }
        }
    }

    public void checkConnection(SFTPConnection sftpConnection) throws JSchException {
        boolean connected = true;
        boolean connectedChannel = true;

        connected = sftpConnection.getStatusSession();
        connectedChannel = sftpConnection.getStatusChannel();
        if (!connected || !connectedChannel) {
            sftpConnection.disconnect();
            sftpConnection.configSftp();
        }
    }
}
