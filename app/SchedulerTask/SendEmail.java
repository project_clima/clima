package SchedulerTask;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import json.entities.MailingList;
import models.*;
import play.Logger;
import play.Play;
import play.jobs.*;
import play.mvc.Http.Request;
import play.templates.Template;
import play.templates.TemplateLoader;
import querys.EmailQuery;
import querys.SendEmailQuery;
import utils.GmailMailer;

/**
 * Job para el envio de Corres
 * @author Rodolfo Miranda -- Qualtop
 */
public class SendEmail extends Job<String> {
    public Request request;
    
    public int surveyType;
    public int surveyId;
    public String storeTypes;
    public String zones;
    public List<String> stores;
    public String employeesToSend;
    public boolean sendToDirectors;
    
    /**
     * Constructor
     * @param surveyType
     * @param surveyId
     * @param storeTypes
     * @param zones
     * @param stores
     * @param employees
     * @param sendToDirectors
     * @param request 
     */
    public SendEmail(int surveyType, int surveyId, String storeTypes,
                     String zones, List<String> stores, String employees,
                     boolean sendToDirectors, Request request) {
        
        this.surveyType      = surveyType;
        this.surveyId        = surveyId;
        this.storeTypes      = storeTypes;
        this.zones           = zones;
        this.stores          = stores;
        this.employeesToSend = employees;
        this.sendToDirectors = sendToDirectors;
        this.request         = request;
    }
    
    /**
     * Metodo principal para el manejo del envio de correos
     * @return 
     */
    public String doJobWithResult() {
        if (this.surveyType > 0 && this.surveyType != 1) {
            return sendCorporative(this.surveyId, this.surveyType, this.employeesToSend);
        } else {
            return sendToStores(
                this.surveyId, this.surveyType, this.storeTypes, this.zones,
                this.stores, this.sendToDirectors
            );
        }
    }
    
    /**
     * Metodo para el envio de correo en encuestas personalizadas
     * @param surveyId
     * @param surveyType
     * @param employeesList
     * @return 
     */
    private String sendCorporative(int surveyId, int surveyType, String employeesList) {
        Survey survey                = Survey.findById(surveyId);
        Map<String, String> template = getEmailTemplate(surveyType);
        String emailContents;
        
        
        try {
            GmailMailer mailer = new GmailMailer();
            
            // Connect transport
            mailer.connect();
            
            List<MailingList> employees = SendEmailQuery.getEmployeesList(surveyId, employeesList); 
            
            int emailCounter = 1;
            
            for (MailingList employee : employees) {
                emailContents = prepareTemplate(
                    template.get("body"),
                    survey,
                    employee.getFirstName(),
                    employee.getFullName(),
                    getSurveyLink(survey.id, employee),
                    "",
                    ""
                );
                        
                mailer.send(
                    employee.getEmail(),
                    template.get("subject"),
                    emailContents,
                    survey.id,
                    Play.configuration.getProperty("lp.gmail.fromAddress")
                );
                
                Logger.info(employee.getEmail());
                
                if (emailCounter++ % 50 == 0) {
                    mailer.close();
                    mailer.connect();
                }
				
                //Thread.sleep(4000);
            }
            
            // Seve send date
            survey.sendDate = new Date();
            survey.save();
            
            // Close transport
            mailer.close();
            
            return "El envío ha finalizado correctamente";
        } catch (Exception ex) {
            return ex.getMessage();
        }
    }
    
    /**
     * Metodo para el envio de correo para las encuestas genericas
     * @param surveyId
     * @param surveyType
     * @param storeTypes
     * @param zones
     * @param stores
     * @param sendToDirectors
     * @return 
     */
    private String sendToStores(int surveyId, int surveyType, String storeTypes,
                                    String zones, List<String> stores, boolean sendToDirectors) {
        
        Survey survey                = Survey.findById(surveyId);
        Map<String, String> template = getEmailTemplate(surveyType);
        Template listTemplate        = TemplateLoader.load("EmailParametrization/employeeList.html");
        String surveyLink            = getSurveyLink(survey.id, null);
        
        String emailContents;
        String managerFullName;
        String usersTable;
        MailingList manager;
        MailingList director;
        
        try {
            GmailMailer mailer = new GmailMailer();
            
            // Connect transport
            mailer.connect();
            
            List<MailingList> employees = SendEmailQuery.getEmployeesStore(
                survey.id, storeTypes, zones, stores, survey.surveyDate
            );
            
            HashMap<String, List<MailingList>> hhRR = setDependants(employees);
            Map<String, Object> templateArgs = new HashMap();
            templateArgs.put("baseUrl", request.getBase());
            
            //Send employees
            int emailCounter = 1;
            
            for (String key : hhRR.keySet()) {
                if (hhRR.get(key) != null && hhRR.get(key).isEmpty()) {
                    Logger.info("Empty " + key);
                    continue;
                }
                
                manager    = EmailQuery.getEmailStore(key, survey.surveyDate);
                
                if (manager.getEmail() == null) {
                    continue;
                }
                
                usersTable = getEmployeesTable(listTemplate, hhRR.get(key), new MailingList());

                emailContents = prepareTemplate(
                    template.get("body"),
                    survey,
                    manager.getManager(),
                    manager.getManager(),
                    surveyLink,
                    usersTable,
                    manager.getDivision()
                );
                
                mailer.send(
                    manager.getEmail(),
                    template.get("subject"),
                    emailContents,
                    survey.id,
                    manager.getEmailCC() != null ? manager.getEmailCC() +
                            "," + Play.configuration.getProperty("lp.gmail.fromAddress") :
                            Play.configuration.getProperty("lp.gmail.fromAddress")
                );
                
                Logger.info("Store: " + key);
                
                if (emailCounter++ % 100 == 0) {
                    mailer.close();
                    mailer.connect();
                }
            }
            
            // Send to directors
            if (sendToDirectors) {
                List<MailingList> directorsZone = SendEmailQuery.getZoneDirectors(
                        survey.id, storeTypes, zones, stores, survey.surveyDate
                );
                
                for (MailingList directorZone : directorsZone) {
                    List<MailingList> directorList  = new ArrayList<>();
                    directorList.add(directorZone);

                    usersTable = getEmployeesTable(listTemplate, directorList, new MailingList());

                    emailContents = prepareTemplate(
                        template.get("body"),
                        survey,
                        "",
                        "",
                        surveyLink,
                        usersTable,
                        directorZone.getDivision()
                    );

                    mailer.send(
                        directorZone.getEmail(),
                        template.get("subject"),
                        emailContents,
                        survey.id,
                        directorZone.getEmailCC()
                    );

                    Logger.info("Director: " + directorZone.getEmail());
                    
                    if (emailCounter++ % 100 == 0) {
                        mailer.close();
                        mailer.connect();
                    }
                }
            }
            
            // Seve send date
            survey.sendDate = new Date();
            survey.save();            
            
            // Close transport
            mailer.close();
            
            return "El envío ha finalizado correctamente";
        } catch (Exception ex) {
            return ex.getMessage();
        }
    }
    
    /**
     * Metodo para almacenar los dependientes de cada empleado
     * @param employees
     * @return 
     */
    private HashMap<String, List<MailingList>> setDependants(List<MailingList> employees) {
        HashMap<String, List<MailingList>> hhRR = new HashMap();
        String key;
        
        for (MailingList employee : employees) {            
            key = employee.getDivision();
            
            employees = hhRR.get(key);
            employees = employees == null ? new ArrayList() : employees;

            employees.add(employee);
            hhRR.put(key, employees);
        }
        
        return hhRR;
    }
    
    /**
     * MEtodo para obtener el template del email
     * @param surveyTypeId
     * @return 
     */
    private Map getEmailTemplate(int surveyTypeId) {
        EmailTemplate template = EmailTemplate.find("type.id = ?1", surveyTypeId).first();
        
        String subject = template != null ? template.subject : "";
        String body    = template != null ? template.body : "";
        
        Map<String, String> templateParts = new HashMap();
        templateParts.put("subject", subject);
        templateParts.put("body", body);
        
        return templateParts;
    }
    
    /**
     * Metodo para generar el link de la encuesta
     * @param surveyId
     * @param employee
     * @return 
     */
    private String getSurveyLink(int surveyId, MailingList employee) {
        String baseUrl = Play.configuration.getProperty("application.baseUrl");
        
        if (employee != null) {
            return baseUrl + "surveys/apply/" +  surveyId + "/" +
                employee.getUserName() + "/" +
                employee.getPassword();
        }
        
        return baseUrl + "surveys/apply/" +  surveyId + "/";
    }
    
    /**
     * Metodo para el llenado del template
     * @param template
     * @param survey
     * @param name
     * @param fullName
     * @param link
     * @param table
     * @param store
     * @return 
     */
    private String prepareTemplate(String template, Survey survey, String name,
                                          String fullName, String link, String table, String store) {        
        HashMap<String, Object> vars = new HashMap();
        SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
        
        vars.put("%collaborator.name%", name);
        vars.put("%collaborator.fullName%", fullName);
        vars.put("%survey.name%", survey.name);
        vars.put("%survey.start%", sdf.format(survey.dateInitial));
        vars.put("%survey.end%", sdf.format(survey.dateEnd));
        vars.put("%survey.link%", link);
        vars.put("%usersTable%", table);
        vars.put("%store%", store);
        
        if (template != null) {
            for (String key : vars.keySet()) {
                
                template= template.replace(key, vars.get(key) != null ? vars.get(key).toString(): "");
            }
        }
        
        return template;
    }
    
    /**
     * Metodo para obtener los empleados a participar en la encuesta
     * @param template
     * @param employees
     * @param director
     * @return 
     */
    private String getEmployeesTable(Template template, List<MailingList> employees, MailingList director) {
        employees.add(director);
        
        HashMap<String, Object> tableArgs = new HashMap();
        tableArgs.put("employees", employees);
                
        return template.render(tableArgs);
    }
}

