package SchedulerTask;

import play.jobs.*;

@OnApplicationStart
public class Startup extends Job {
    
    @Override
    public void doJob() {
        javassist.runtime.Desc.useContextClassLoader = true;
    }
}