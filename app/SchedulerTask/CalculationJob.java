package SchedulerTask;

import java.text.SimpleDateFormat;
import java.util.Date;
import play.jobs.*;
import models.*;
import play.Logger;
import querys.ScoreEvaluation;
import querys.SearchQuery;

/**
 * Job para buscar en la base si hay alguna programacion de calculo de
 * calificaciones
 * @author Rodolfo Miranda -- Qualtop
 */
@On("0 * * * * ?")
public class CalculationJob extends Job {
    public void doJob() {
        ScoreTask task = ScoreTask.find(
            "to_char(FD_EXEC_DATE, 'DD/MM/YYYY HH24:MI') = to_char(SYSDATE, 'DD/MM/YYYY HH24:MI')"
        ).first();
        
        if (task != null) {
            Logger.info("Starting evaluation task");
            String structureDate = task.structureDate.replace("/", "");
            ScoreEvaluation.evaluate(structureDate, task.createdBy,task.id);
            if(task.status.equals("S")) {
                
                Logger.info("Inicia proceso de carga de calificaciones clima,"
                    + " proceso evaluate : " + task.status);
                int result = SearchQuery.climaScore(task.structureDate, task.createdBy);
                Logger.info("Fin del proceso de carga de calificaciones clima status : " + result);
            }
        }
    }
}

