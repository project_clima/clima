package controllers;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import java.sql.SQLException;
import java.util.*;
import java.util.logging.Level;
import java.util.logging.Logger;
import play.mvc.*;
import querys.nps.NpsHomeQuery;
import utils.nps.helpers.NpsControllerHelper;
import utils.nps.json.NpsHomeJsonObject;
import utils.nps.model.Area;
import utils.nps.model.Business;
import utils.nps.model.Chief;
import utils.nps.model.Group;
import utils.nps.model.Manager;
import utils.nps.model.Section;
import utils.nps.model.Seller;
import utils.nps.model.Store;
import utils.nps.model.Zone;

/**
 * Clase principal para el modulo NPS
 * @author Rodolfo Miranda -- Qualtop
 */
@With(Secure.class)
public class NPS extends Controller {
    
    //private static List<ZoneHomeModel> zones;
    public static void openQuestionsCategorization() {
        render();
    }

    public static void goalsAndContact() {
        render();
    }

    /**
     * Metodo para el procesamiento de las peticiones y obtener la grafica de 
     * NPS y Historico
     * @param firstDate
     * @param lastDate
     * @param zNps
     * @param zone
     * @param group
     * @param business
     * @param op
     * @param store
     * @param chief
     * @param man
     * @param section
     * @param type 
     */
    //@Check("home")
    public static void getNpsCompanyData(String firstDate, String lastDate,
            String zNps, String zone, String group, String business, String op,
            String store, String chief, String man, String section, String type) {

        String userId = session.get("idHome");
        NpsHomeJsonObject companyH = NpsControllerHelper.
                npsCompanyHelper(userId, firstDate, lastDate, zNps, zone, group,
                        business, op, store, chief, man, section, type);

        renderJSON(companyH);
    }

    /**
     * Metodo para obtner las areas segun los filtros seleccionados
     * @param firstDate
     * @param lastDate
     * @param zNps
     * @param zone
     * @param group
     * @param business
     * @param op
     * @param store
     * @param chief
     * @param man
     * @param section
     * @param type 
     */
    
    //@Check("home")
    public static void detailGlobalAreas(String firstDate, String lastDate,
            String zNps, String zone, String group, String business, String op,
            String store, String chief, String man, String section, String type) {

        String userId = session.get("idHome");
        List<Area> areas = NpsControllerHelper.
                detailGlobalAreasHelper(userId, firstDate, lastDate, zNps, zone,
                        group, business, op, store, chief, man, section, type);

        renderJSON(areas);
    }

    /**
     * Metodo para el procesamiento del TOP NPS
     * @param firstDate
     * @param lastDate
     * @param op
     * @param zone
     * @param group
     * @param business 
     */
    
    //@Check("home")
    public static void getNpsTop(String firstDate, String lastDate,
            String op, String zone, String group, String business) {

        String userId = session.get("idHome");
        NpsHomeJsonObject top = NpsControllerHelper.npsTopHelper(userId, firstDate,
                lastDate, op, zone, group, business);

        renderJSON(top);
    }

    /**
     * Metodo para obtener el Ranking de NPS segun los filtros seleccionado
     * @param firstDate
     * @param lastDate
     * @param zNps
     * @param zone
     * @param group
     * @param business
     * @param op
     * @param store
     * @param chief
     * @param man
     * @param section
     * @param type 
     */
    
    //@Check("home")
    public static void getNpsRanking(String firstDate, String lastDate,
            String zNps, String zone, String group, String business, String op,
            String store, String chief, String man, String section, String type) {

        String userId = session.get("idHome");
        NpsHomeJsonObject zonesD = NpsControllerHelper.npsRankingHelper(userId,
                firstDate, lastDate, zNps, zone, group, business, op, store, chief,
                man, section, type);

        renderJSON(zonesD);
    }

    /**
     * Metodo para obtener el detalle NPS en las Ubicaciones segun los filtros 
     * seleccionados
     * @param firstDate
     * @param lastDate
     * @param zNps
     * @param zone
     * @param group
     * @param business
     * @param op
     * @param store
     * @param chief
     * @param man
     * @param section
     * @param type 
     */
    
    //@Check("home")
    public static void detailStores(String firstDate, String lastDate,
            String zNps, String zone, String group, String business, String op,
            String store, String chief, String man, String section, String type) {

        String userId = session.get("idHome");
        List<Store> npsStores = NpsControllerHelper.npsDetailStoresHelper(
                userId, firstDate, lastDate, zNps, zone, group, business, op,
                store, chief, man, section, type);

        renderJSON(npsStores);
    }

    /**
     * Metodo para obtener el detalle NPS en los Grupos segun los filtros 
     * seleccionados
     * @param firstDate
     * @param lastDate
     * @param zNps
     * @param zone
     * @param group
     * @param business
     * @param op
     * @param store
     * @param chief
     * @param man
     * @param section
     * @param type 
     */
    //@Check("home")
    public static void detailGroup(String firstDate, String lastDate,
            String zNps, String zone, String group, String business, String op) {

        String userId = session.get("idHome");
        
        List<Group> npsGroups = NpsControllerHelper.npsDetailGroupsHelper(
                userId, firstDate, lastDate, op);

        renderJSON(npsGroups);
    }

    //@Check("home")
    public static void detailBusiness(String firstDate, String lastDate,
            String zNps, String zone, String group, String business, String op,
            String store, String chief, String man, String section, String type) {

        String userId = session.get("idHome");
        List<Business> npsBusiness = NpsControllerHelper.npsDetailBusinessHelper(
                userId, firstDate, lastDate, zNps, zone, group, business, op,
                store, chief, man, section, type);

        renderJSON(npsBusiness);
    }

    
    /**
     * Metodo para obtener el detalle NPS en los Gerentes segun los filtros 
     * seleccionados
     * @param firstDate
     * @param lastDate
     * @param zNps
     * @param zone
     * @param group
     * @param business
     * @param op
     * @param store
     * @param chief
     * @param man
     * @param section
     * @param type 
     */
    
    //@Check("home")
    public static void detailManager(String firstDate, String lastDate,
            String zNps, String zone, String group, String business, String op,
            String store, String chief, String man, String section, String type) {

        String userId = session.get("idHome");
        List<Manager> npsManagers = NpsControllerHelper.npsDetailManagerHelper(
                userId, firstDate, lastDate, zNps, zone, group, business, op, store,
                chief, man, section, type);

        renderJSON(npsManagers);
    }

    /**
     * Metodo para obtener el detalle NPS en los Jefes segun los filtros 
     * seleccionados
     * @param firstDate
     * @param lastDate
     * @param zNps
     * @param zone
     * @param group
     * @param business
     * @param op
     * @param store
     * @param chief
     * @param man
     * @param section
     * @param type 
     */
    //@Check("home")
    public static void detailChief(String firstDate, String lastDate,
            String zNps, String zone, String group, String business, String op,
            String store, String chief, String man, String section, String type) {

        String userId = session.get("idHome");
        List<Chief> npsChiefs = NpsControllerHelper.npsDetailChiefHelper(userId,
                firstDate, lastDate, zNps, zone, group, business, op, store,
                chief, man, section, type);

        renderJSON(npsChiefs);
    }

    /**
     * Metodo para obtener el detalle NPS en las Secciones segun los filtros 
     * seleccionados
     * @param firstDate
     * @param lastDate
     * @param zNps
     * @param zone
     * @param group
     * @param business
     * @param op
     * @param store
     * @param chief
     * @param man
     * @param section
     * @param type 
     */
    
    //@Check("home")
    public static void detailSection(String firstDate, String lastDate,
            String zNps, String zone, String group, String business, String op,
            String store, String chief, String man, String section, String type) {

        String userId = session.get("idHome");
        List<Section> sections = NpsControllerHelper.npsDetailSectionHelper(userId,
                firstDate, lastDate, zNps, zone, group, business, op, store,
                chief, man, section, type);

        renderJSON(sections);
    }

    /**
     * Metodo para obtener el detalle NPS en los Vendedores segun los filtros 
     * seleccionados
     * @param firstDate
     * @param lastDate
     * @param zNps
     * @param zone
     * @param group
     * @param business
     * @param op
     * @param store
     * @param chief
     * @param man
     * @param section
     * @param type 
     */
    
    //@Check("home")
    public static void detailSellers(String firstDate, String lastDate,
            String zNps, String zone, String group, String business, String op,
            String store, String chief, String man, String section, String type) {

        String userId = session.get("idHome");
        List<Seller> sellers = NpsControllerHelper.npsDetailSellerHelper(userId,
                firstDate, lastDate, zNps, zone, group, business, op, store,
                chief, man, section, type);

        renderJSON(sellers);
    }

    /**
     * Metodo para la comparativa NPS de Jefes
     * @param firstDate
     * @param lastDate
     * @param zNps
     * @param zone
     * @param group
     * @param business
     * @param op
     * @param store
     * @param chief
     * @param man
     * @param section
     * @param type 
     */
    
    //@Check("home")
    public static void compareChiefs(String firstDate, String lastDate,
            String zNps, String zone, String group, String business, String op,
            String store, String chief, String man, String section, String type) {

        String userId = session.get("idHome");
        List<Chief> chiefs = NpsControllerHelper.npsCompireChiefsHelper(userId,
                firstDate, lastDate, zNps, zone, group, business, op, store,
                chief, man, section, type);

        renderJSON(chiefs);
    }
    
    /**
     * Metodo para la comparativa NPS de Secciones
     * @param firstDate
     * @param lastDate
     * @param zNps
     * @param zone
     * @param group
     * @param business
     * @param op
     * @param store
     * @param chief
     * @param man
     * @param section
     * @param type 
     */
    
    //@Check("home")
    public static void compareSections(String firstDate, String lastDate,
            String zNps, String zone, String group, String business, String op,
            String store, String chief, String man, String section, String type) {

        String userId = session.get("idHome");
        List<Section> sections = NpsControllerHelper.npsCompireSectionsHelper(userId,
                firstDate, lastDate, zNps, zone, group, business, op, store,
                chief, man, section, type);

        renderJSON(sections);
    }
    
    /**
     * Metodo para el rankeo por (Ubicaciones, Gerentes, Jefes, Secciones)
     * @param firstDate
     * @param lastDate
     * @param zone
     * @param group
     * @param managers
     * @param chiefs
     * @param sections
     * @param business
     * @param op
     * @param store
     * @param rankear
     * @param zNps 
     */
    //@Check("home")
    public static void rankingFor(String firstDate, String lastDate,String zone,
            String group, String managers, String chiefs, String sections,
            String business, String op,String store, String rankear,
            String zNps) {

        String userId = session.get("idHome");
        NpsHomeJsonObject companyH = NpsControllerHelper.
                npsDetailRankearHelper(userId,firstDate, lastDate,zone, 
                    group, business, op, store,rankear,zNps,managers,chiefs,sections);

        renderJSON(companyH);
    }

    
    /**
     * Metodo para obtener la relacion Gerentes-Jefes
     * @param zNps 
     */
    //@Check("home")
    public static void getManagersChiefs(String zNps) {

        String userId = session.get("idHome");
        List<Zone> zonesStores = null;
        if (zNps != null && !zNps.equals("[]") && !zNps.isEmpty()) {
            Gson gson = new Gson();
            zonesStores = gson.fromJson(zNps, new TypeToken<List<Zone>>() {
            }.getType());
        }

        for (Zone zone : zonesStores) {
            for (Store store : zone.getStores()) {
                
                List<Manager> managers = NpsHomeQuery.getManagers(userId, 
                        store.getId());
                //store.setId(zone.getId());
                for (Manager m : managers) {
                    
                    List<Chief> chiefs = NpsHomeQuery.getChiefs(m.getId(),
                            store.getId());
                    m.setChiefs(chiefs);
                    //m.set(store.getId());
                }

                store.setManagers(managers);
            }
        }

        renderJSON(zonesStores);
    }

    
    /**
     * Metodo para obtener los grupos segun los filtros seleccionados
     * @param firstDate
     * @param lastDate
     * @param zNps
     * @param zone
     * @param group
     * @param business
     * @param op
     * @param store
     * @param chief
     * @param man
     * @param section
     * @param type 
     */
    //@Check("home")
    public static void getNpsGroups(
            String firstDate, String lastDate, String zNps, String zone,
            String group, String business, String op, String store,
            String chief, String man, String section, String type) {

        String userId = session.get("idHome");
        
        List<Group> groups = NpsControllerHelper.npsDetailGroupsHelper(userId,
                firstDate, lastDate, op);
        renderJSON(groups);
    }

    
    /**
     * Metodo para obtener las secciones en NPS por zona
     * @param zNps 
     */
    //@Check("home")
    public static void getNpsSections(String zNps) {

        String userId = session.get("idHome");

        List<Zone> sectons = NpsControllerHelper.getSections(zNps);
        renderJSON(sectons);
    }

    
    /**
     * Metodo para obtener las Zonas por grupo y Negocio
     * @param business
     * @param group 
     */
    //@Check("home")
    public static void getNpsZones(String business, String group) {
        String userId = session.get("idHome");
        try {
            List<Zone> zones = NpsHomeQuery.getZoneHome(business, group,userId);
            renderJSON(zones);
        } catch (SQLException ex) {
            Logger.getLogger(NPS.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
}
