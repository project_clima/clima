package controllers;

import java.util.ArrayList;
import java.util.List;
import models.EmployeeClima;
import models.PermisosEspeciales;
import play.mvc.Controller;
import utils.Queries;

public class SpecialPermissions extends Controller{
    public static void getSpecialPermissions(String structureDate){
        Queries queries = new Queries();
        int userId = Integer.valueOf(session.get("userId"));

        List<PermisosEspeciales> permisos = Queries.getPermisosEspeciales(userId);

        List data = new ArrayList();
        List<json.entities.SpecialPermissions> specialPermissionsList = new ArrayList();

        if (permisos.size() != 0) {
            for (PermisosEspeciales permiso : permisos) {
                json.entities.SpecialPermissions specialPermissions = new json.entities.SpecialPermissions();
                switch(Integer.valueOf(permiso.typePermit)){
                    case 1: //Solo su estructura

                    break;
                    case 2: //Su estuctura y los pares
                        /// sacar hermanos
                        int level = Integer.valueOf(session.get("level"));
                        specialPermissions.setLevel(level);
                        specialPermissions.setData(queries.getParesPermisosEspecialesAux(userId, structureDate));

                    break;
                    case 3: //Por nivel
                        int searchLevel = Integer.valueOf(permiso.permitValue);
                        specialPermissions.setLevel(searchLevel);
                        specialPermissions.setData(queries.getLevelPermisosEspecialesAux(searchLevel, structureDate));
                    break;
                    case 4: // Por estructura de una persona en especifico
                        Long employeeNumber = Long.valueOf(permiso.permitValue);
                        EmployeeClima employee = EmployeeClima.find("select e from EmployeeClima e where e.employeeNumber = ?", employeeNumber).first();
                        specialPermissions.setLevel(employee.vLevel);
                        specialPermissions.setData(queries.getEstructuraPermisosEspecialesAux(employeeNumber, structureDate));
                        specialPermissions.setEmployee(employee);

                    break;
                }
                specialPermissionsList.add(specialPermissions);
            }
        }
        renderJSON(specialPermissionsList);
    }
}
