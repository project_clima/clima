package controllers;

import play.*;
import play.mvc.*;
import java.util.*;
import models.*;
import play.db.jpa.JPA;
import utils.Pagination;
import play.templates.Template;
import play.templates.TemplateLoader;

@With(Secure.class)
public class EmailNPS extends SendSurvey {
    public static void emailTemplate() {
        String place = "NPS";

        renderTemplate("EmailTemplates/index.html", place);
    }

    public static void edit(int surveyTypeId) {
        SurveyType  type = SurveyType.findById(surveyTypeId);
        EmailTemplate template = EmailTemplate.find("type.id = ?1", surveyTypeId).first();
        
        String place = "NPS";
        renderTemplate("EmailTemplates/edit.html", template, type, surveyTypeId, place);
    }

    public static void preview(EmailTemplate template, int surveyTypeId) {                
        template = (template.type == null) ? (EmailTemplate) EmailTemplate.find("type.id = ?1", surveyTypeId).first() : template;
        
        HashMap<String, Object> vars = new HashMap();
        Template templateList = TemplateLoader.load("EmailTemplates/employee-list-sample.html");
        
        vars.put("%collaborator.name%", "Julieta");
        vars.put("%collaborator.fullName%", "Julieta Ornelas");
        vars.put("%survey.name%", "Encuesta NPS");
        vars.put("%survey.start%", "01-01-2016");
        vars.put("%survey.end%", "01-01-2017");
        vars.put("%survey.link%", "http://www.liverpool.com.mx/tienda/home.jsp");
        vars.put("%usersTable%", templateList.render());
        vars.put("%store%", "Fábricas de Francia CDMX");
        
        if (template != null && template.body != null) {
            for (String key : vars.keySet()) {
                template.body = template.body.replace(key, vars.get(key).toString());
            }
        }        
        
        render(template, surveyTypeId);
    }

    public static void list(int iDisplayLength, String startDate,
    String endDate, int iDisplayStart, String sEcho,
    String sSortDir_0, int iSortCol_0) {
        
        int start  = iDisplayStart == 0 ? iDisplayStart : iDisplayStart + 1;
        int end    = iDisplayLength + iDisplayStart;
        long count = SurveyType.count();        
        List types = SurveyType.find(
            "select t.subType, t.name, t.id from SurveyType t where t.id = 4"
        ).from(start).fetch(end);
        
        renderJSON(new Pagination(sEcho, count, count, types));
    }

    public static void emailSending() {
        String place = "NPS";
        List<SurveyType> surveyTypes = SurveyType.findAll();
        List<StoreEmployee> depots   = getDepots();

        renderTemplate("SendSurvey/index.html", surveyTypes, depots, place);
    }

    public static void emailMonitor() {
        List<SurveyType> surveyTypes = SurveyType.findAll();
        String place = "NPS";

        renderTemplate("EmailMonitor/index.html", surveyTypes, place);
    }
}
