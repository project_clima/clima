package controllers;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.Query;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import org.w3c.dom.Document;
import org.w3c.dom.Node;
import org.xml.sax.SAXException;

import models.Accion;
import models.Permiso;
import models.PermisosEspeciales;
import models.Rol;
import models.Seccion;
import models.SecurityLog;
import models.UserRole;
import models.Usuario;
import play.Logger;
import play.Play;
import play.data.validation.Valid;
import play.db.jpa.JPA;
import play.libs.XPath;
import play.mvc.Controller;
import play.mvc.With;
import querys.SpecialQuery;
import querys.StructureQuery;
import querys.UsuarioQuery;
import querys.csi.StructureQuerySCI;
import querys.csi.UsuarioQueryCSI;
import utils.Message;
import utils.Pagination;

/**
 * Clase controlar para el manejo de la seguridad del Portal (Login)
 * @author Rodolfo Miranda -- Qualtop
 */
@With(Secure.class)
public class Seguridad extends Controller {
	public final static Long MODULE_CSI = 1L;
	public final static Long MODULE_CLIMA = 2L;
	public final static Long MODULE_NPS = 3L;
	
        /**
         * MEtodo que regresa el login
         */
    public static void index() {
        render();
    }
    
    public static void users() {
        render();
    }

    /*
     * Roles
     */
    @Check("seguridad/roles")
    public static void roles() {
        render();
    }

    /**
     * MEtodo para obtener los roles por usuario
     */
    public static void obtenerRoles() {
    	List<Rol> lstRol = new ArrayList<Rol>();
    	String module = session.get("place");
    	
    	if(module.toUpperCase().equalsIgnoreCase("CSI")){
    		lstRol = Rol.find("idModule = ?1", MODULE_CSI).fetch();
    		
    	} else if (module.toUpperCase().equalsIgnoreCase("environment")) {
    		lstRol = Rol.find("idModule = ?1", MODULE_CLIMA).fetch();
    		
    	} else if (module.toUpperCase().equalsIgnoreCase("nps")) {
    		lstRol = Rol.find("idModule = ?1", MODULE_NPS).fetch();
    		
    	}
                
        render(lstRol);
    }
    
    /**
     * MEtodo para obtener permisos por ROl
     * @param idRol 
     */
    public static void getPermissions(Long idRol) {
        Rol oRol = Rol.findById(idRol);
        HashMap<String, HashMap<String, List>> mapPermissions = new HashMap();

        HashMap<String, List> mapPerm = null;
        List lstAction                = null;

        for (Accion oAccion : oRol.acciones) {
            String sectionName = oAccion.permiso.nombrePermiso;
            String moduleName  = oAccion.permiso.seccion.nombreSeccion;
            String accion      = oAccion.nombreAccion;
            mapPerm            = mapPermissions.get(moduleName);


            if (mapPerm == null) {
                mapPerm = new HashMap();
                lstAction = new ArrayList();
                lstAction.add(accion);
                mapPerm.put(sectionName, lstAction);
            } else {
                lstAction = mapPerm.get(sectionName) == null ? new ArrayList() : mapPerm.get(sectionName);
                lstAction.add(accion);
                mapPerm.put(sectionName, lstAction);
            }

            mapPermissions.put(moduleName, mapPerm);
        }

        render(mapPermissions);
    }

    public static void usuariosRol(Long id) {
        /*List<UsuarioRol> lstUsuarios = UsuarioRol.find("byRol.id", id).fetch();
        renderJSON(lstUsuarios);*/
    }

    /**
     * MEtodo para almacenar un Rol
     * @param rol
     * @param isEdit 
     */
    @Check({"seguridad/agregarrol", "seguridad/editarrol"})
    public static void saveRol(Rol rol, Boolean isEdit) {
        Rol roleStored = Rol.findById(rol.id);
        Message oMensaje   = null;
        String action = "agregado";
        String module = session.get("place");
        
        if(roleStored != null) {
            if(isEdit) {
                action = "modificado";
                roleStored.modifiedDate = new Date();
                roleStored.modifiedBy = session.get("username");
                roleStored.save();
                SecurityLog.saveLog("Roles de Usuario",
                            action + " rol " + roleStored.id,
                            session.get("username"));
                oMensaje = new Message("Rol " + action + " con éxito", "success", false);
                renderJSON(oMensaje);
                return;
            } else {
                oMensaje = new Message("El nombre del rol ya existe", "error", true);
                renderJSON(oMensaje);
                return;
            }
        } else {// save it:
            rol.id = null;
            rol.createdDate = new Date();
            rol.modifiedDate = new Date();
            rol.createdBy = session.get("username");
            rol.modifiedBy = session.get("username");
        }

        if(!isEdit){
        	if(module.toUpperCase().equals("CSI")){
        		rol.idModule = MODULE_CSI;
        	}else if(module.toUpperCase().equals("NPS")){
        		rol.idModule = MODULE_NPS;
        	}else if(module.toUpperCase().equals("ENVIRONMENT")){
        		rol.idModule = MODULE_CLIMA;
        	}
        }
        
        rol.save();
        
        SecurityLog.saveLog("Roles de Usuario",
                            action + " rol " + rol.id,
                            session.get("username"));
        oMensaje = new Message("Rol " + action + " con éxito", "success", false);
        
        renderJSON(oMensaje);
    }
    
    /**
     * Metodo para almacenar un Rol con un nombre ya existente
     * @param name
     * @param roleId 
     */
    public static void duplicatedRoleName(String name, Long roleId) {
        Rol role = Rol.find(
            "lower(nombreRol) = ?1 and id <> ?2", name.toLowerCase(), roleId
        ).first();
        
        renderJSON(role != null);
    }

    /**
     * MEtodo para obtener la lista de Roles por Rol
     * @param idRol 
     */
    public static void permisosRol(int idRol) {
        Rol oRol = Rol.findById(idRol);
        List<Seccion> lstSecciones = Seccion.findAll();
        render(lstSecciones, oRol);
    }

    /**
     * MEtodo para eliminar un Rol
     * @param id 
     */
    @Check("seguridad/eliminarrol")
    public static void eliminarRol(Long id) {
        Rol oRol = Rol.findById(id);

        SecurityLog.saveLog("Roles de Usuario",
                            "Eliminó rol " + oRol.id,
                            session.get("username"));

        oRol.delete();

        Message oMensaje = new Message("Rol eliminado con éxito", "success", false);
        renderJSON(oMensaje);
    }

    /*
     * Usuarios
     */
    @Check("seguridad/usuarios")
    public static void usuarios() {
    	String module = session.get("place");
    	if(module.toUpperCase().equals("CSI")){
        	users();
        }else{
        	render();
        }
        
    }

    /**
     * Metodo para obtener los usuarios en base de datos
     */
    public static void obtenerUsuarios() {
        List<Usuario> lstUsuarios = Usuario.find("ORDER BY 1 ASC").fetch();
        render(lstUsuarios);
    }

    /**
     * Metodo para obtener los Roles por Usuario
     * @param id 
     */
    public static void addUser(long id) {
    	String module = session.get("place");
        Usuario oUsuario = Usuario.findById(id);
        List<Rol> lstRoles = utils.Queries.getUserRoles(id, module);

        render(oUsuario, lstRoles);
    }

    /**
     * Metodo para editar un usuario
     * @param id 
     */
    public static void editUser(long id) {
    	String module = session.get("place");
    	Logger.info("module :::::::::::::::::: %s", module);
        Usuario oUsuario = Usuario.findById(id);
        List<Rol> lstRoles = utils.Queries.getUserRoles(id, module);

        render(oUsuario, lstRoles);
    }

    /**
     * Nombre para verificar el nombre de usuario
     * @param username 
     */
    public static void verify_username(String username) {
        Usuario oUsuario = Usuario.find("UPPER(username) LIKE UPPER(?)", username).first();
        renderJSON(oUsuario == null);
    }

    /**
     * Nombre para obtener los Roles por Usuario
     * @param id 
     */
    public static void formUsuario(long id) {
    	String module = session.get("place");
        Usuario oUsuario = Usuario.findById(id);
        
        List<Rol> lstRoles = utils.Queries.getUserRoles(id, module);

        render(oUsuario, lstRoles);
    }

    /**
     * MEtodo para obtener las fechas de estructura
     */
    public static void getDatesStructures(){
        StructureQuery queries = new StructureQuery();

        List datesStructure = queries.getDatesStructures();
        renderJSON(datesStructure);
    }


    /**
     * Metodo de busqueda por nombre de usuario y fecha de estructura
     * @param userName
     * @param structureDate
     * @return 
     */
    public static List getUserIdByUsername(String userName, String structureDate){
        UsuarioQuery queries= new UsuarioQuery();

        return queries.getUsersIdByName(userName, structureDate);
    }
    
    /**
     * MEtodo para obtener los usuarios por estructura
     * @param userName
     * @param userNumber
     * @param location
     * @param department
     * @param iDisplayLength
     * @param iDisplayStart
     * @param sEcho
     * @param structureDate 
     */
    public static void getStructureData(
    String userName, Integer userNumber, String location, String department, int iDisplayLength,
            int iDisplayStart, String sEcho, String structureDate){

        Pagination pagination = null;
        String condition = "";
        int conditions = 0;
        long count=0;
        UsuarioQuery queries = new UsuarioQuery();

        int start = iDisplayStart == 0 ? iDisplayStart : iDisplayStart + 1;
        int end = iDisplayLength + iDisplayStart;

        List oPR = null;

        if(location !=null && !location.equals("-1")){
            condition = "(TABLEROW.FC_DESC_LOCATION = '" + location + "' OR TABLEROW.FN_USERID IN (\n" +
                        "				SELECT FN_USERID\n" +
                        "				FROM   PUL_EMPLOYEE_CLIMA\n" +
                        "				WHERE  TO_CHAR(FD_STRUCTURE_DATE, 'MM/YYYY' ) = '"+ structureDate +"' \n" +
                        "				AND    FN_MANAGER IN (\n" +
                        "					SELECT FN_ID_USER \n" +
                        "					FROM   PUL_USER \n" +
                        "					WHERE  FC_DESC_LOCATION = '" + location + "'\n" +
                        "				)\n" +
                        "			)\n" +
                        ")";
            
            conditions++;
        }
        
        if(department != null && !department.equals("-1")){
            if(conditions > 0){
                condition +=" AND ";
            }
            
            condition += "(TABLEROW.FC_DEPARTMENT = '"+ department +"' \n" +
                            "        OR TABLEROW.FN_USERID IN (\n" +
                            "		SELECT FN_USERID\n" +
                            "				  FROM   PUL_EMPLOYEE_CLIMA\n" +
                            "				  WHERE  TO_CHAR(FD_STRUCTURE_DATE, 'MM/YYYY' ) = '"+ structureDate +"' \n" +
                            "				  AND    FN_MANAGER IN (SELECT FN_ID_USER FROM PUL_USER WHERE FC_DEPARTMENT = '"+ department +"')\n" +
                            "		  )\n" +
                            "	  )";
            
            conditions++;
        }
       
        if (userNumber != null) {
            if(conditions > 0){
                condition +=" AND ";
            }
            
            condition += "(TABLEROW.FN_EMPLOYEE_NUMBER = " + userNumber + "\n" +
                            "       OR (TABLEROW.FN_USERID IN (SELECT FN_USERID\n" +
                            "                                  FROM   PUL_EMPLOYEE_CLIMA\n" +
                            "                                  WHERE  TO_CHAR(FD_STRUCTURE_DATE, 'MM/YYYY' ) = '" + structureDate + "' \n" +
                            "                                  AND    fn_manager IN (SELECT FN_USERID\n" +
                            "                                                        FROM PUL_EMPLOYEE_CLIMA\n" +
                            "                                                        WHERE FN_EMPLOYEE_NUMBER = "+ userNumber + ")))\n" +
                            ")\n" +
                            "START WITH TABLEROW.FN_EMPLOYEE_NUMBER = " + userNumber + "\n" +
                            "CONNECT BY PRIOR TABLEROW.FN_USERID = TABLEROW.FN_MANAGER\n";
            
            conditions++;
        }
        
        if(userName != null && userName.trim().length() >0){
            if(conditions > 0){
                condition +=" AND ";
            }
            
            condition += "(TABLEROW.FC_USERNAME LIKE UPPER('%" +userName + "%') OR TABLEROW.FN_USERID IN (\n" +
                            "				SELECT FN_USERID\n" +
                            "				FROM   PUL_EMPLOYEE_CLIMA\n" +
                            "				WHERE  TO_CHAR(FD_STRUCTURE_DATE, 'MM/YYYY' ) = '" + structureDate + "' \n" +
                            "				AND  FN_MANAGER IN (\n" +
                            "					SELECT FN_ID_USER\n" +
                            "					FROM   PUL_USER\n" +
                            "					WHERE  FC_USERNAME LIKE UPPER('%" +userName + "%')\n" +
                            "				)\n" +
                            "			)\n" +
                            ")\n";
            conditions++;
        }
        
        if(conditions == 0){
            String name  = session.get("username");
            condition += "(TABLEROW.FC_USERNAME LIKE '%"+name+"%' \n" +
                "OR     TABLEROW.FN_USERID IN (select FN_USERID from PUL_EMPLOYEE_CLIMA where TO_CHAR(FD_STRUCTURE_DATE, 'MM/YYYY' ) = '" + structureDate + "' AND fn_manager in (select FN_ID_USER FROM PUL_USER WHERE FC_USERNAME LIKE '%"+name+"%')))";
        }

        //condition += " ORDER BY FN_USERID DESC";
            
        oPR= queries.getStructure(condition, start, end, structureDate);
        count=queries.getCountRowsUsers(condition, structureDate);
        pagination = new Pagination(sEcho,count,count, oPR);
        
        renderJSON(pagination);
    }       

    /**
     * Metodo para obtener las ubicaciones por fecha de estuctura
     * @param date 
     */
    public static void getLocation(String date) {
        StructureQuery queries = new StructureQuery();

        List locations = queries.getLocation(date);
        renderJSON(locations);
    }

    /**
     * Metodo para los departamentos por ubicacion y fecha de estructura
     * @param location
     * @param date 
     */
    public static void getDepartments(String location, String date) {
        StructureQuery queries = new StructureQuery();

        List departments = queries.getDepartments(location, date);
        renderJSON(departments);
    }

    /**
     * MEtodo guardar un usuario
     * @param usuario 
     */
    @Check("seguridad/agregarusuario")
    public static void saveUsuario(@Valid Usuario usuario) {
        /*
         * Validar nombre de usuario
         */
        Usuario oUsuarioValidacion = Usuario.find("username LIKE ?", usuario.username).first();

        if (oUsuarioValidacion != null && usuario.id == null) {
            flash.error("El nombre de usuario ya existe");
            usuarios();
        }

        //Se guarda/actualiza la información del usuario
        String accion = usuario.id != null ? "modificado" : "agregado";
        String action = usuario.id != null ? "Modificó" : "Agregó";

        usuario.save();

        SecurityLog.saveLog("Usuarios",
                            action + " usuario " + usuario.username,
                            session.get("username"));

        flash.success("Usuario " + accion + " con éxito", "success");
        String module = session.get("place");
        if(module.toUpperCase().equals("CSI")){
        	users();
        }else{
        	usuarios();
        }
    }

    /**
     * MEtodo para actualizar un usuario
     * @param usuario 
     */
    @Check("seguridad/editarusuario")
    public static void updateUsuario(@Valid Usuario usuario) {

        String module = session.get("place");

        Long idModule = getModule(module);

        if (usuario.id == null) {
            flash.error("Usuario no valido");
            usuarios();
        }

        List<UserRole> userRoles = UserRole.find("idUser = ?1", new BigDecimal(usuario.id)).fetch();

        List<Long> roleIds = new ArrayList<>();

        for (UserRole userRole : userRoles) {
            roleIds.add(userRole.idRole);
        }
        
        List<Rol> currentRoles = new ArrayList<>();
                
        if(roleIds.size() > 0) {
            currentRoles = Rol.find("id in (?1)", roleIds).fetch();
        }
        
        for (Rol rol : currentRoles) {
            //Agrega roles actuales que son de otros módulos
            if (rol.idModule != idModule) {
                usuario.roles.add(rol);
            }
        }

        usuario.save();

        SecurityLog.saveLog(
            "Usuarios",
            "Modificó usuario " + usuario.username,
            session.get("username")
        );

        flash.success("Se asignó el rol con éxito");

        if (module.toUpperCase().equals("CSI")) {
            users();
        } else {
            usuarios();
        }
    }
    
    /**
     * Metodo para saber en que modulo se esta trabajando
     * @param module
     * @return 
     */
    private static Long getModule(String module) {
    	if(module != null && module.equalsIgnoreCase("csi"))
    		return MODULE_CSI;
    	
    	if(module != null && module.equalsIgnoreCase("nps"))
    		return MODULE_NPS;
    	
    	if(module != null && module.equalsIgnoreCase("environment"))
    		return MODULE_CLIMA;
    	
    	return 0L;
    		
    }

    /**
     * MEtodo para eliminar un usuario
     * @param id 
     */
    @Check("seguridad/eliminarusuario")
    public static void eliminarUsuario(Long[] id) {
        for (Long idUser : id) {
            Usuario oUsuario = Usuario.findById(idUser);

            SecurityLog.saveLog("Usuarios",
                                "Eliminó usuario " + oUsuario.username,
                                session.get("username"));

            oUsuario.delete();
        }

        Message oMensaje = new Message("Usuario eliminado con éxito", "success", false);
        renderJSON(oMensaje);
    }

    /*
     * Extrae los permisos del XML de acciones
     */
    public static void obtenerPermisos() {
        String pathXmlSecurity = Play.applicationPath + File.separator
                               + "conf" + File.separator + "security.xml";


        DocumentBuilderFactory builderFactory =
        DocumentBuilderFactory.newInstance();
        DocumentBuilder builder = null;

        Document oXmlSecurity = null;

        try {
            builder = builderFactory.newDocumentBuilder();
        } catch (ParserConfigurationException  e) {
            e.printStackTrace();
        }

        try {
            oXmlSecurity = builder.parse(new FileInputStream(pathXmlSecurity));
        } catch (SAXException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

        String text = "";

        for (Node section : XPath.selectNodes("Security//Section", oXmlSecurity)) {
            String nombreSeccion = XPath.selectText("@name", section).trim();

            Seccion oSeccion = Seccion.find("byNombreSeccion", nombreSeccion).first();

            if (oSeccion == null) {
                oSeccion = new Seccion();
                oSeccion.nombreSeccion = nombreSeccion;
                oSeccion.save();
            }

            org.w3c.dom.NodeList nl = section.getChildNodes();

            for (Node modulo : XPath.selectNodes("Module", section)) {
                String nombreModulo = XPath.selectText("@name", modulo).trim();

                Permiso oPermiso = Permiso.find("byNombrePermiso", nombreModulo).first();

                if (oPermiso == null) {
                    oPermiso = new Permiso();
                    oPermiso.nombrePermiso = nombreModulo;
                    oPermiso.seccion = oSeccion;
                    oPermiso.save();
                }

                for (Node accion : XPath.selectNodes("Action", modulo)) {
                    String nombreAccion = XPath.selectText("@name", accion).trim();
                    String urlMapping = XPath.selectText("@url-mapping", accion).trim();

                    Accion oAccion = Accion.find("nombreAccion = ? And urlMapping = ?",
                                                 nombreAccion, urlMapping).first();

                    if (oAccion == null) {
                        oAccion = new Accion();
                        oAccion.nombreAccion = nombreAccion;
                        oAccion.urlMapping = urlMapping;
                        oAccion.permiso = oPermiso;
                        oAccion.save();
                    }

                }
            }
        }

        renderText("Permisos importados con éxito");
    }

    /*
     * Permisos
     */
    @Check("seguridad/permisos")
    public static void permisos(Long id) {
        Rol oRol = Rol.findById(id);

        if (oRol == null) {
            roles();
        }

        Long idModule = getModule(session.get("place"));
        Logger.info("value of id module :::::::::::::: %s", idModule);
        List<Seccion> lstSecciones = Seccion.find("idModule = ?1", idModule).fetch();
        render(lstSecciones, oRol);
    }

    /**
     * MEtodo para almacenar los permisos en session
     * @param rol 
     */
    @Check("seguridad/permisos")
    public static void savePermisos(Rol rol) {


         Boolean validaMsg= true;
 
         if(rol.acciones.size() == 0){
            validaMsg= false;
         }else{
            rol.save();    
         }


        SecurityLog.saveLog("Roles de Usuario / Permisos",
                            "Modificó/Asignó permisos a rol " + rol.id,
                            session.get("username"));

        if(validaMsg == true)
        flash.success("Permisos asignados con éxito");
        roles();
    }

    /**
     * MEtodo para obtener los permisos por usuario
     * @param idUsuario 
     */
    public static void obtenerPermisosUsuario(int idUsuario) {
        Usuario oUsuario = Usuario.findById(idUsuario);
        List<String> lstPermisos = new ArrayList<String>();

        for(Rol oRol : oUsuario.roles) {
            for (Accion oAccion : oRol.acciones) {
                if (!lstPermisos.contains(oAccion.urlMapping)) {
                    lstPermisos.add(oAccion.urlMapping);
                }
            }
        }

        renderJSON(lstPermisos);
    }

     /*
     * Permisos Especiales
     */

    public static void getLevels() {
        SpecialQuery queries = new SpecialQuery();

        List levels = queries.getLevels();
        renderJSON(levels);
    }

    public static void getVie() {
        SpecialQuery queries = new SpecialQuery();

        List listViews = queries.getVie();
        renderJSON(listViews);
    }

    /**
     * MEtodo para obtener los permisos especiales por usuario
     * @param id 
     */
    public static void permisosEspeciales(Long id) {
        Rol oRol = Rol.findById(id);

        if (oRol == null) {
            roles();
        }


        PermisosEspeciales permisosEspeciales = new PermisosEspeciales();

        PermisosEspeciales lstEspeciales = permisosEspeciales.find("role",oRol).first();
        render(lstEspeciales, oRol);
    }

    /**
     * Metodo para actualizar los permisos especiales
     * @param especial
     * @param rol 
     */
    public static void updateSpecial(@Valid PermisosEspeciales especial,Long rol) {
        if (rol == null) {
            flash.error("Especial no valido");
            roles();
        }

        Rol oRol = Rol.findById(rol);

        especial.role=oRol;

        especial.save();
        flash.success("Se asigno con éxito");
        roles();
    }
    
    
    //CSI
    
    public static void getDatesStructuresCSI(){
        StructureQuerySCI queries = new StructureQuerySCI();

        List datesStructure = queries.getDatesStructures();
        renderJSON(datesStructure);
    }
	
	public static void getStructureDataCSI(
    	    String userName, Integer userNumber, String location, String department, int iDisplayLength,
    	            int iDisplayStart, String sEcho, String structureDate){

    	        Pagination pagination = null;
    	        String condition = "";
    	        int conditions = 0;
    	        long count=0;
    	        UsuarioQueryCSI queries = new UsuarioQueryCSI();

    	        int start = iDisplayStart == 0 ? iDisplayStart : iDisplayStart + 1;
    	        int end = iDisplayLength + iDisplayStart;

    	        List oPR = null;

    	        if(location !=null && !location.equals("-1")){
    	                condition = "(TABLEROW.FC_DESC_LOCATION = '"+location + "'\n" +
    	                "OR     TABLEROW.FN_USERID IN (select FN_USERID from PORTALUNICO.PUL_EMPLOYEE_CSI where fn_manager in (select FN_ID_USER FROM PUL_USER WHERE FC_DESC_LOCATION= '"+ location+"')))";
    	                conditions++;
    	        }
    	        if(department != null && !department.equals("-1")){
    	            if(conditions > 0){
    	                condition +=" AND ";
    	            }
    	            condition +="(TABLEROW.FC_DEPARTMENT='"+ department +"' \n" +
    	                "OR     TABLEROW.FN_USERID IN (select FN_USERID from PORTALUNICO.PUL_EMPLOYEE_CSI where fn_manager in (select FN_ID_USER FROM PUL_USER WHERE FC_DEPARTMENT = '"+ department +"')))";
    	            conditions++;
    	        }
    	       
    	        if (userNumber != null) {
    	            if(conditions > 0){
    	                condition +=" AND ";
    	            }
    	            condition +="(TABLEROW.FN_EMPLOYEE_NUMBER ="+ userNumber+"\n" +
    	                "OR     (TABLEROW.FN_USERID IN (select FN_USERID from PORTALUNICO.PUL_EMPLOYEE_CSI where fn_manager in (select FN_USERID FROM PORTALUNICO.PUL_EMPLOYEE_CSI WHERE FN_EMPLOYEE_NUMBER = "+ userNumber +"))))\n" +
    	                "START WITH TABLEROW.FN_EMPLOYEE_NUMBER = " + userNumber + " CONNECT BY PRIOR TABLEROW.FN_USERID = TABLEROW.FN_MANAGER\n";
    	            conditions++;
    	        }
    	         if(userName != null && userName.trim().length() >0){
    	            if(conditions > 0){
    	                condition +=" AND ";
    	            }
    	            condition += "(TABLEROW.FC_USERNAME LIKE '%"+userName+"%' \n" +
    	                "OR     TABLEROW.FN_USERID IN (select FN_USERID from PORTALUNICO.PUL_EMPLOYEE_CSI where fn_manager in (select FN_ID_USER FROM PUL_USER WHERE FC_USERNAME LIKE '%"+userName+"%')))";
    	            conditions++;
    	        }
    	        if(conditions == 0){
    	            String name  = session.get("username");
    	            userName = name;
    	            condition += "(TABLEROW.FC_USERNAME LIKE '%"+name+"%' \n" +
    	                "OR     TABLEROW.FN_USERID IN (select FN_USERID from PORTALUNICO.PUL_EMPLOYEE_CSI where fn_manager in (select FN_ID_USER FROM PUL_USER WHERE FC_USERNAME LIKE '%"+name+"%')))";
    	        }

    	        //condition += " ORDER BY FN_USERID DESC";
    	            
    	        oPR= queries.getStructure(condition, start, end, structureDate, userName);
    	        count=queries.getCountRowsUsers(condition, structureDate, userName);
    	        pagination = new Pagination(sEcho,count,count, oPR);
    	        
    	        renderJSON(pagination);
    	    }
	
	public static void getLocationCSI(String date) {
        StructureQuerySCI queries = new StructureQuerySCI();

        List locations = queries.getLocation(date);
        renderJSON(locations);
    }

    public static void getDepartmentsCSI(String location, String date) {
    	StructureQuerySCI queries = new StructureQuerySCI();

        List departments = queries.getDepartments(location, date);
        renderJSON(departments);
    }
}
