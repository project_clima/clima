package controllers;

import play.mvc.*;
import java.util.*;
import models.*;
import play.data.validation.Valid;
import utils.Pagination;
import javax.persistence.Query;

@With(Secure.class)
public class Factors extends Controller {    
    public static void listFactors(int surveyId, int iDisplayLength, String startDate,
                              String endDate, int iDisplayStart, String sEcho,
                              String sSortDir_0, int iSortCol_0, String sSearch) {
        
        int start  = iDisplayStart;
        int end    = iDisplayLength;
        long count = Factor.count("survey.id = ?1", surveyId);       
        
        List factors = Factor.find(
            "select description, percent, points, id from Factor " + 
            "where survey.id = ?1", surveyId
        ).fetch();
        
        renderJSON(new Pagination(sEcho, count, count, factors));
    }
    
    public static void listSubFactors(int surveyId, int iDisplayLength, String startDate,
                              String endDate, int iDisplayStart, String sEcho,
                              String sSortDir_0, int iSortCol_0, String sSearch) {
        
        int start  = iDisplayStart;
        int end    = iDisplayLength;
        long count = Factor.count("from SubFactor sf join sf.factor f join f.survey s where s.id = ?1", surveyId);       
        
        List subFactor = SubFactor.find(
            "select  f.description, sf.description, sf.percent, sf.points, sf.id, " +
            "f.id, f.points " +
            "from SubFactor sf join sf.factor f join f.survey s " +
            "where s.id = ?1 order by f.description asc", surveyId
        ).fetch();
        
        renderJSON(new Pagination(sEcho, count, count, subFactor));
    }
    
    public static void formFactor(int surveyId, int factorId) {
        Factor factor = Factor.findById(factorId);
        render(surveyId, factor);
    }
    
    public static void saveFactor(Factor factor, float oldValue) {        
        if (factor.id == null) {
            factor.createdDate = new Date();
            factor.createdBy   = session.get("username");
            oldValue = 0f;
        }
        
        factor.percent      = factor.points;
        factor.modifiedDate = new Date();
        factor.modifiedBy   = session.get("username");

        factor.save();
        
        if (!Objects.equals(oldValue, factor.points)) {
            calculateSubfactorValues(factor.id);
        }
        
        renderJSON(new utils.Message(null, null, false));
    }
    
    public static void deleteFactor(int factorId) {
        Factor factor = Factor.findById(factorId);
        
        if (factor.subfactors.size() > 0) {
            renderJSON(
                new utils.Message(
                    "El factor no puede ser eliminado por que está asignado a " 
                    + factor.subfactors.size() + " subfactores",
                    null,
                    true
                )
            );
        }
        
        factor.delete();
        
        renderJSON(new utils.Message(null, null, false));
    }
    
    public static void formSubFactor(int surveyId, int subFactorId) {
        SubFactor subfactor  = SubFactor.findById(subFactorId);
        List<Factor> factors = Factor.find("survey.id = ?1", surveyId).fetch();
        render(factors, subfactor);
    }
    
    public static void saveSubFactor(@Valid SubFactor subfactor) {       
        if(validation.hasErrors()) {
            renderJSON(
                new utils.Message(
                    "Error al agregar subfactor. Por favor, revise que la información sea válida.",
                    null,
                    true
                )
            );
        }
        
        Boolean edit = !(subfactor.id == null);
        
        if (!edit) {
            subfactor.createdDate = new Date();
            subfactor.createdBy   = session.get("username");
        }
        
        subfactor.modifiedDate = new Date();
        subfactor.modifiedBy   = session.get("username");
        subfactor.save();
        
        if (!edit) {
            calculateSubfactorValues(subfactor.factor.id);
        }
        
        renderJSON(new utils.Message(null, null, false));
    }
    
    private static void calculateSubfactorValues(int factorId) {
        Factor factor = Factor.findById(factorId);
        
        if (factor.subfactors == null) {
            return;
        }
        
        Float points  = factor.subfactors.size() > 0 ? factor.points / factor.subfactors.size() : 0;
        Float percent = factor.subfactors.size() > 0 ? (float) 100 / factor.subfactors.size() : 0;
        
        for (SubFactor subfactor : factor.subfactors) {
            subfactor.points  = points;
            subfactor.percent = percent;
            
            subfactor.save();
        }
    }
    
    public static void deletesSubFactor(int subFactorId) {
        SubFactor subFactor = SubFactor.findById(subFactorId);
        int factorId = subFactor.factor.id;
        
        if (subFactor.questions.size() > 0) {
            renderJSON(
                new utils.Message(
                    "El subfactor no puede ser eliminado por que está asignado a " 
                    + subFactor.questions.size() + " preguntas",
                    null,
                    true
                )
            );
        }
        
        subFactor.delete();
        calculateSubfactorValues(factorId);
        
        renderJSON(new utils.Message(null, null, false));
    }
    
    public static void editPoints(int pk, Float value) {
        SubFactor subfactor  = SubFactor.findById(pk);
        Boolean isValidValue = validatePoints(subfactor, value);
        
        if (!isValidValue) {
            response.status = 400;
            renderJSON(
                "Por favor, ingrese un valor menor ya que se superaron los " + 
                "puntos del factor (" + subfactor.factor.points + ")"
            );
        }
        
        subfactor.points = value;
        subfactor.save();
    }
    
    public static Boolean validatePoints(SubFactor subfactor, Float newPoints) {
        Query sumQuery = SubFactor.em().createQuery(
            "select coalesce(sum(points), 0) from SubFactor where factor.id = ?1 and id <> ?2"
        );
        
        sumQuery.setParameter(1, subfactor.factor.id);
        sumQuery.setParameter(2, subfactor.id);
        
        Double sumPoints = (Double) sumQuery.getSingleResult();
        
        return !((sumPoints + newPoints) > subfactor.factor.points);
    }
    
    public static void updateSubfactorPoints(Map<String, Float> subfactors) {
        for (String key : subfactors.keySet()) {
            SubFactor subfactor = SubFactor.findById(Integer.valueOf(key));
            Float points        = subfactors.get(key);
            
            subfactor.points  = points;
            subfactor.percent = (subfactor.points * 100) / subfactor.factor.points;
            subfactor.save();
        }
        
        renderJSON(new utils.Message(null, null, false));
    }
}