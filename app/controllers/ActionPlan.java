package controllers;

import play.*;
import play.mvc.*;
import java.util.*;
import models.*;
import querys.ActionPlanQuery;
import java.io.IOException;
import java.text.SimpleDateFormat;
import javax.mail.MessagingException;
import json.entities.ResumeTable;
import net.sf.jxls.exception.ParsePropertyException;
import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import querys.MatrixQuery;
import querys.StructureQuery;
import play.templates.Template;
import play.templates.TemplateLoader;
import utils.GmailMailer;
import utils.Pagination;
import utils.RenderExcel;
import static utils.RenderPDF.renderPDF;

/**
 * Clase controlodar para el manejo del Plan de Accion
 * @author Rodolfo Miranda -- Qualtop
 */
@With(Secure.class)
public class ActionPlan extends Controller {

    /**
     * Metodo principal del controlador, regresa las fechas de las fotos del 
     * sistema
     */
    public static void index() {
        List structureDates = StructureQuery.getDatesStructures();
        render(structureDates);
    }
    
    /**
     * MEtodo de utileria para saber si tiene plan
     * @param date 
     */
    public static void canActionPlan(String date) {
        renderJSON(true);
    }
    
    /**
     * MEtodo para obtener el resumen de las calificaciones del arbol en Clima 
     * @param level
     * @param structureDate 
     */
    public static void getResume(int level, String structureDate) {      
        ResumeTable resumeTree = MatrixQuery.getResumeTree(level, structureDate);
        
        renderJSON(resumeTree);
    }

    /**
     * MEtodo para obtener informacion del plan de accion
     */
    public static void infoActionPlan() {
        render();
    }

    /**
     * 
     * @param userId
     * @param date
     * @param notCaptured 
     */
    public static void openToCapture(String userId, String date, Boolean notCaptured) {
        List elements     = ActionPlanQuery.openToCapture(userId, date, notCaptured);
        String surveyYear = date.split("/")[1];
        
        if (elements.isEmpty()) {
            renderJSON(elements);
        }
        
        render(elements, userId, surveyYear, notCaptured);
    }
    
    /**
     * 
     * @param userId
     * @param date
     * @param notCaptured 
     */
    public static void openToReview(String userId, String date, Boolean notCaptured) {
        List elements = ActionPlanQuery.openToCapture(userId, date, notCaptured);
        
        if (elements.isEmpty()) {
            renderJSON(elements);
        }
        
        render(elements, userId);
    }
    
    /**
     * Metodo para almacenar comentarios
     * @param actionPlan
     * @param employeeId
     * @param year 
     */
    public static void saveAnswers(List<PlanAccion> actionPlan, int employeeId, String year) {
        int previouslySaved    = 0;
        
        for (PlanAccion plan : actionPlan) {
            if (plan == null) {
                continue;
            }
            
            String history  = "";
            String comments = "";
            String approvalHistory = "";
            String statusReview    = "";
            int planId = plan.id != null ? plan.id : -1;
            
            PlanAccion prevActionPlan = PlanAccion.findById(planId);
            
            if (prevActionPlan != null) {
                previouslySaved++;
                history         = prevActionPlan.historic;
                comments        = prevActionPlan.comments;
                approvalHistory = prevActionPlan.approvalHistory;
                statusReview    = prevActionPlan.statusReview;
            }
            
            plan.historic = appendComment(plan.comments, history);            
            plan.creDate  = prevActionPlan == null ? new Date() : prevActionPlan.creDate;
            plan.comments = plan.comments == null || plan.comments.isEmpty() ? comments : plan.comments;
            plan.modDate  = new Date();
            plan.creFor   = prevActionPlan == null ? session.get("username") : prevActionPlan.creFor;
            plan.modFor   = session.get("username");
            plan.approvalHistory = approvalHistory;
            plan.statusReview    = statusReview;
            
            plan = plan.merge();
            plan.save();
        }
        
        if (previouslySaved == 0) {
            notifyCapture(employeeId, year);
        }
        
        response.status = 204;
    }
    
    /**
     * 
     * @param actionPlan
     * @param employeeId 
     */
    public static void saveReview(List<PlanAccion> actionPlan, int employeeId) {
        int numberOfRejected = 0;
        for (PlanAccion plan : actionPlan) {
            if (plan == null) {
                continue;
            }
            
            PlanAccion planToReview = PlanAccion.findById(plan.id);
            
            if (planToReview != null) {
                planToReview.approvalHistory = appendComment(plan.comments, planToReview.approvalHistory);
                planToReview.statusReview    = plan.statusReview.equals("yes") ? "APROBADO" : "RECHAZADO";
                planToReview.modDate         = new Date();
                planToReview.modFor          = session.get("username");
                numberOfRejected             += plan.statusReview.equals("yes") ? 0 : 1;
                
                planToReview.save();
            }
        }
        
        if (numberOfRejected > 0) {
            notifyRejection(employeeId);
        }
        
        response.status = 204;
    }
    
    /**
     * 
     * @param commentToAppend
     * @param comments
     * @return 
     */
    private static String appendComment(String commentToAppend, String comments) {
        comments = comments != null && !comments.isEmpty() ? comments : "";
        
        SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
        String newComment    = comments + sdf.format(new Date()) + ": " + commentToAppend + "|";
        
        return commentToAppend != null && !commentToAppend.isEmpty() ? newComment : comments;
    }

    /**
     * 
     * @param userId
     * @param date
     * @param statusPlan
     * @param statusReview
     * @param statusSelector
     * @param iDisplayLength
     * @param startDate
     * @param endDate
     * @param iDisplayStart
     * @param sEcho
     * @param totalRows
     * @param search 
     */
    public static void openMainTable(String userId, String date, String statusPlan,
                                     String statusReview, String statusSelector,
                                     int iDisplayLength, String startDate, String endDate,
                                     int iDisplayStart, String sEcho, int totalRows,
                                     String search) {
        
        int start = iDisplayStart == 0 ? iDisplayStart : iDisplayStart + 1;
        int end   = iDisplayLength + iDisplayStart;
        end = end > totalRows ? totalRows : end;
        
        List elements = ActionPlanQuery.openMainTable(
            userId, date, statusPlan, statusReview, statusSelector, start, end, search
        );
        
        renderJSON(new Pagination(sEcho, totalRows, totalRows, elements));   
    }

    /**
     * 
     */
    public static void getUserId() {
        renderText(session.get("userId"));
    }
    
    /**
     * Metodo para imprimir los resultados
     * @param userId
     * @param date 
     */
    public static void printActionPlan(String userId, String date) {
        EmployeeClima employee = EmployeeClima.find("id = ?1", Integer.valueOf(userId)).first();
        List elements = ActionPlanQuery.openToCapture(userId, date, false);
        String path = Play.applicationPath + "/public";
        
        response.setHeader("Content-Disposition", "attachment; filename=PlanDeAccion.pdf");
        renderPDF(path, elements, employee);
    }
    
    /**
     * MEtodo para exportar los resultados mostrados en pantalla
     * @param userId
     * @param date
     * @param statusPlan
     * @param statusReview
     * @param statusSelector
     * @param search
     * @param totalRows
     * @throws ParsePropertyException
     * @throws InvalidFormatException
     * @throws IOException 
     */
    public static void export(String userId, String date, String statusPlan,
                              String statusReview, String statusSelector,
                              String search, int totalRows)
    throws ParsePropertyException, InvalidFormatException, IOException {
        
        List elements = ActionPlanQuery.openMainTable(
            userId,
            date,
            statusPlan,
            statusReview,
            statusSelector,
            0,
            -1,
            search
        );
        
        renderArgs.put("elements", elements);
        renderArgs.put(RenderExcel.RA_FILENAME, "PlanDeAccion.xls");
        RenderExcel renderer = new RenderExcel("app/excel/views/ActionPlan/PlanDeAccion.xls");
        renderer.render();
    }
    
    /**
     * 
     * @param employeeId
     * @param year 
     */
    private static void notifyCapture(int employeeId, String year) {
        EmployeeClima employeeClima = EmployeeClima.find(
            "id = ?1", employeeId
        ).first();

        if (employeeClima != null) {
            EmployeeClima manager = EmployeeClima.find(
                "userId = ?1", employeeClima.manager
            ).first();

            sendEmail(manager, "Revisión Plan de Acción", "plan-captured-template", year);
        }
    }
    
    /**
     * 
     * @param employeeId 
     */
    private static void notifyRejection(int employeeId) {
        EmployeeClima employeeClima = EmployeeClima.find(
            "id = ?1", employeeId
        ).first();

        if (employeeClima != null) {
            sendEmail(employeeClima, "Rechazo Plan de Acción", "plan-rejection-template", "");
        }
    }
    
    /**
     * Metodo para el envio de correos
     * @param employee
     * @param subject
     * @param template
     * @param year 
     */
    private static void sendEmail(EmployeeClima employee, String subject, String template,
                                  String year) {
        try {
            if (employee != null && employee.email != null && !employee.email.isEmpty()) {
                
                GmailMailer mailer = new GmailMailer();

                String emailContents = getEmailContents(employee, template, year);

                mailer.sendGeneral(
                    employee.email,
                    "Liverpool Portal Único : " + subject,
                    emailContents,
                    null
                );
            }
        } catch(MessagingException e) {
            Logger.error(e.getMessage());
        }
    }

    /**
     * MEtodo para el llenado de las variables de la plantilla de correo
     * @param employee
     * @param emailTemplate
     * @param year
     * @return 
     */
    private static String getEmailContents(EmployeeClima employee, String emailTemplate,
                                           String year) {
        Template template = TemplateLoader.load("ActionPlan/" + emailTemplate + ".html");

        HashMap<String, Object> tableArgs = new HashMap();
        tableArgs.put("employee", employee);
        tableArgs.put("baseUrl", Play.configuration.getProperty("application.baseUrl"));
        tableArgs.put("year", year);

        return template.render(tableArgs);
    }
    
    /**
     * Metodo para el conteo de encuestas por plan 
     * @param userId
     * @param date
     * @param statusPlan
     * @param statusReview
     * @param search 
     */
    public static void getCount(String userId, String date, String statusPlan,
                                String statusReview, String search) {
        if ((userId.equals("50092262") || userId.equals("50074387"))
            && isEmpty(statusPlan) && isEmpty(statusReview) && isEmpty(search)
        ) {
            renderJSON(100);
        }
        
        long count = ActionPlanQuery.getPlanCount(
            userId, date, statusPlan, statusReview, search
        );
        
        renderJSON(count);
    }
    
    private static boolean isEmpty(String value) {
        return value == null || value.isEmpty() || value.equals("PENDIENTE");
    }
}
