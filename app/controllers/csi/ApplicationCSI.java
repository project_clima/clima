package controllers.csi;

import static json.csi.dto.RoundHelper.roundDouble2Decimales;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import controllers.Secure;
import json.csi.dto.GraphRawSeriesDto;
import json.csi.dto.GraphResponseWrappterDto;
import json.csi.dto.GraphResumeDto;
import json.csi.dto.GraphSerieDto;
import json.csi.dto.GraphSeriesResponseDto;
import json.csi.dto.MatrixRawAreaLineResultDto;
import json.csi.dto.MatrixRawLineReportResultDto;
import json.csi.dto.MatrixReportResponseWrapperDto;
import json.csi.dto.MatrixReportResultDto;
import json.csi.dto.MatrixReportRowResultDto;
import json.csi.dto.ResultAreaDto;
import json.csi.dto.ResultCatalogDto;
import json.csi.dto.ResultDetailDto;
import json.csi.dto.ResultHeaderDto;
import json.csi.dto.ResultResponseWrapperAreaDto;
import json.csi.dto.ResultResponseWrapperDto;
import json.csi.dto.SesionFilterWrapper;
import models.CSIType;
import models.csi.CsiDimensionResultsZone;
import play.db.jpa.GenericModel.JPAQuery;
import play.mvc.Controller;
import play.mvc.With;

@With(Secure.class)
public class ApplicationCSI extends Controller {
    public static List<String> cat = null;
    public static Integer csiType = null;
    public static Integer corporation = null;
    public static Integer management = null;
    public static Integer world = null;
    public static Integer section = null;
    public static Integer evaluated = null;
    public static Integer levelFilter = null;

    public static void csi() {
        List<CSIType> surveyTypes = CSIType.findAll();
        List<Object> surveys = SurveysCSI.getAllSurveyLists();
        render(surveyTypes, surveys);
    }
    
    /**
     * Método para obtener el gráfico en Inicio de CSI
     */
    public static void getGraphJSON() {
        cat = new ArrayList<>();
        GraphResumeDto resume = getGraphResumeData();
        GraphSeriesResponseDto series = new GraphSeriesResponseDto();

        SesionFilterWrapper csiFilters = new SesionFilterWrapper(session);

        if (csiFilters.getFilterCsiType() == null || csiFilters.getFilterCsiType().equals("")
                || csiFilters.getFilterCsiType().isEmpty()) {
            levelFilter = null;
        } else {
            levelFilter = 1;
            csiType = Integer.valueOf(csiFilters.getFilterCsiType());
        }

        if (levelFilter == null) {
            series = getGraphSeriesResponseDto();
        } else {
            if (csiType == 1 || csiType == 3) {
                series = getGraphSeriesResultsDto();
            } else if (csiType == 2) {
                series = getGraphSeriesMatrixDto();
            } else {
                series = getGraphSeriesAreasDto();
            }
        }

        GraphResponseWrappterDto response = new GraphResponseWrappterDto(resume, series, cat);

        renderJSON(response);
    }

    public static boolean isNull(String str) {
        if (str == null || str.equals("") || str.isEmpty()) {
            return true;
        } else {
            return false;
        }
    }
    
    /**
     * Obtiene un resumen de datos generales para el llenar gráfico circular
     * 
     * @return GraphResumeDto
     */
    public static GraphResumeDto getGraphResumeDto() {

        SesionFilterWrapper filter = new SesionFilterWrapper(session, "d.idSurvey", "s.surveyDate", 0);
        SesionFilterWrapper csiFilters = new SesionFilterWrapper(session);
        StringBuilder queryArea = new StringBuilder();

        String extraFilters = "";
        if (filter.getExtraFilters() != null && !filter.getExtraFilters().isEmpty()) {
            extraFilters = " AND " + filter.getExtraFilters() + " ";
        }

        String csiFiltersStr = "";
        if (csiFilters.getFilterCsiType() == null || csiFilters.getFilterCsiType().equals("")
                || csiFilters.getFilterCsiType().isEmpty()) {
        } else {
            csiFiltersStr = " AND cs.surveyType=" + csiFilters.getFilterCsiType() + " ";
        }

        if (csiFilters.getFilterCorporation() == null || csiFilters.getFilterCorporation().equals("")
                || csiFilters.getFilterCorporation().isEmpty()) {
        } else {
            csiFiltersStr = " AND d.idManager4Evaluado in (select ce.userId from CSIEmployee ce where ce.manager="
                    + csiFilters.getFilterCorporation() + " ) and d.idManager4Evaluado <> 0 ";
        }

        if (csiFilters.getFilterManagement() == null || csiFilters.getFilterManagement().equals("")
                || csiFilters.getFilterManagement().isEmpty()) {
        } else {
            csiFiltersStr = " AND d.idManager5Evaluado in (select ce.userId from CSIEmployee ce where ce.manager="
                    + csiFilters.getFilterManagement() + " ) and d.idManager5Evaluado <> 0 ";
        }

        if (csiFilters.getFilterWorld() == null || csiFilters.getFilterWorld().equals("")
                || csiFilters.getFilterWorld().isEmpty()) {
        } else {
            csiFiltersStr = " AND d.userIdEvaluated in (select ce.userId from CSIEmployee ce where ce.manager="
                    + csiFilters.getFilterWorld() + " ) and d.userIdEvaluated <> 0 ";
        }

        if (csiFilters.getFilterSection() == null || csiFilters.getFilterSection().equals("")
                || csiFilters.getFilterSection().isEmpty()) {
        } else {
            csiFiltersStr = " AND d.userIdEvaluated in (0," + csiFilters.getFilterSection()
                    + " ) and d.userIdEvaluated <> 0 ";
        }

        if (csiFilters.getFilterEvaluated() == null || csiFilters.getFilterEvaluated().equals("")
                || csiFilters.getFilterEvaluated().isEmpty() || csiFilters.getFilterEvaluated().contains("undefined")) {
        } else {
            csiFiltersStr = " AND d.userIdEvaluated in (select ce.userId from CSIEmployee ce where ce.userId="
                    + csiFilters.getFilterEvaluated() + " ) and d.userIdEvaluated <> 0 ";
        }

        queryArea.append("select new " + GraphResumeDto.class.getName() + "( ")

                .append(" sum(d.countPromotores)*1.0, ").append(" sum(d.countNeutros)*1.0, ")
                .append(" sum(d.countDetractores)*1.0, ").append(" avg(cs.goal)*1.0 ")

                .append(" ) ").append(" from CsiDimensionResultsZone d, CsiSurvey cs , Survey s")
                .append(" where d.idSurvey = cs.idSurvey ").append(" AND d.idSurvey = s.id	 ").append(csiFiltersStr)
                .append(filter.getCSITypeFilterSurveyTypeNameEqualsCSI("s.surveyType.name")).append(extraFilters);

        ;

        List<GraphResumeDto> rawSeries = null;

        if (filter.isHasDate()) {
            rawSeries = CsiDimensionResultsZone.find(queryArea.toString(), filter.getDateFrom(), filter.getDateTo())
                    .fetch();
        } else if (filter.isHasSurvey()) {
            rawSeries = CsiDimensionResultsZone.find(queryArea.toString(), filter.getFilterEncuesta()).fetch();
        } else {
            rawSeries = CsiDimensionResultsZone.find(queryArea.toString()).fetch();
        }

        if (rawSeries.isEmpty()) {
            return new GraphResumeDto(0d, 0d, 0d, 0d);
        }
        return rawSeries.get(0);
    }

    /**
     * Recupera los filtros de búsqueda
     * 
     * @return false
     */
    public static boolean getFilters() {
        SesionFilterWrapper csiFilters = new SesionFilterWrapper(session);

        if (csiFilters.getFilterCorporation() == null || csiFilters.getFilterCorporation().equals("")
                || csiFilters.getFilterCorporation().isEmpty()) {
        } else {
            levelFilter = 2;
            corporation = Integer.valueOf(csiFilters.getFilterCorporation());
        }

        if (csiFilters.getFilterManagement() == null || csiFilters.getFilterManagement().equals("")
                || csiFilters.getFilterManagement().isEmpty()) {
        } else {
            levelFilter = 3;
            management = Integer.valueOf(csiFilters.getFilterManagement());
        }

        if (csiFilters.getFilterWorld() == null || csiFilters.getFilterWorld().equals("")
                || csiFilters.getFilterWorld().isEmpty()) {
        } else {
            levelFilter = 4;
            world = Integer.valueOf(csiFilters.getFilterWorld());
        }

        if (csiFilters.getFilterSection() == null || csiFilters.getFilterSection().equals("")
                || csiFilters.getFilterSection().isEmpty()) {
        } else {
            levelFilter = 5;
            section = Integer.valueOf(csiFilters.getFilterSection());
        }

        if (csiFilters.getFilterEvaluated() == null || csiFilters.getFilterEvaluated().equals("")
                || csiFilters.getFilterEvaluated().isEmpty() || csiFilters.getFilterEvaluated().contains("undefined")) {
        } else {
            levelFilter = 6;
            evaluated = Integer.valueOf(csiFilters.getFilterEvaluated());
        }

        return false;
    }
    
    /**
     * Método para obtener los datos que llenarán el gráfico de barras,
     * en caso de haber elegido Tienda - Tienda o Tienda - Corporativo
     * 
     * @return ResultResponseWrapperDto
     */
    public static ResultResponseWrapperDto getResults() {

        SesionFilterWrapper filter = new SesionFilterWrapper(session, "d.idSurvey", "s.surveyDate", 0);

        String extraFilters = "";
        if (filter.getExtraFilters() != null && !filter.getExtraFilters().isEmpty()) {
            extraFilters = " AND " + filter.getExtraFilters() + " ";
        }

        getFilters();

        Map<Integer, ResultCatalogDto> mapCatalog = new HashMap<>();
        String filtroPrincipal = ResultsCSI.getPrincipalFilter(csiType, levelFilter, corporation, management, world,
                section, evaluated);

        if (filtroPrincipal == null || filtroPrincipal.trim().isEmpty()) {
            filtroPrincipal = " , CSIEmployee e where d.idSurvey = s.id ";
        } else {
            filtroPrincipal = filtroPrincipal + " and d.idSurvey = s.id ";
        }

        filtroPrincipal = filtroPrincipal + " " + filter.getCSITypeFilterSurveyTypeIN("s.surveyType.name") + " ";

        String queryHeader = " select new json.csi.dto.ResultHeaderDto(0," + ResultsCSIQuerys.ROW_HEADER_QUERY_BASE
                + " from CsiDimensionResultsZone d, Survey s " + filtroPrincipal + "  " + extraFilters;

        JPAQuery csiHeaderReportJPAQuery = null;
        csiHeaderReportJPAQuery = ResultsCSI.getFilteredDimResultsJPAQuery(filter, queryHeader);
        List<ResultHeaderDto> csiHeaderReport = csiHeaderReportJPAQuery.fetch();

        SesionFilterWrapper filterStructure = new SesionFilterWrapper(session, null, "e.structureDate", 0);

        String extraFiltersStructure = "";
        if (!filterStructure.getExtraFilters().trim().isEmpty()) {
            extraFiltersStructure = " AND " + filterStructure.getExtraFilters() + " ";
        }

        List<ResultCatalogDto> nextCatalog = ResultsCSI.getNextCatalogs(levelFilter, corporation, management, world,
                section, evaluated, filterStructure);

        String idsCatalogo = ResultsCSI.getCatalogos(nextCatalog);
        List<String> catalogos = new ArrayList<>();
        for (ResultCatalogDto resultCatalogDto : nextCatalog) {
            mapCatalog.put(resultCatalogDto.getId(), resultCatalogDto);
        }

        List<Integer> orders = ResultsCSI.getOrdersList(filterStructure, filtroPrincipal + " " + extraFiltersStructure);

        JPAQuery csiDetailReportJPAQuery = ResultsCSI.getDetailReport(levelFilter, orders, filtroPrincipal,
                extraFilters, corporation, idsCatalogo, management, world, section, evaluated, filter);

        nextCatalog = new ArrayList<>();
        List<ResultDetailDto> csiDetailReport = csiDetailReportJPAQuery.fetch();

        for (ResultDetailDto resultDetailDto : csiDetailReport) {
            nextCatalog.add(mapCatalog.get(resultDetailDto.getIdAgrupador()));
        }
        ResultResponseWrapperDto wrapper = new ResultResponseWrapperDto(csiHeaderReport, csiDetailReport, orders,
                nextCatalog);

        for (ResultCatalogDto result : nextCatalog) {
            catalogos.add(result.getDesc());
        }

        cat = catalogos;

        return wrapper;
    }

    /**
     * Método para obtener los resultados que llenarán el gráfico de barras, 
     * en caso de haber elegido Corporativo Corporativo
     * 
     * @return MatrixReportResponseWrapperDto
     */
    public static MatrixReportResponseWrapperDto getMatrix() {

        SesionFilterWrapper filter = new SesionFilterWrapper(session, "d.idSurvey", "s.surveyDate", 0);

        String extraFilters = "";
        if (filter.getExtraFilters() != null && !filter.getExtraFilters().isEmpty()) {
            extraFilters = " AND " + filter.getExtraFilters() + " ";
        }

        getFilters();

        List<Integer> ids = new ArrayList<>();
        SesionFilterWrapper filterStructure = new SesionFilterWrapper(session, null, "e.structureDate", 0);

        String filtroPrincipal = ", CSIEmployee e where d.idCsiType = 2";

        if (levelFilter == 1) {
            filtroPrincipal += " and d.idManager3Evaluado in(?1) ";
            ids = ResultsCSI.getMatrixEvaluatedAreas(levelFilter, null, filterStructure);
            StringBuilder sb = new StringBuilder();

            for (Integer s : ids) {
                sb.append(s).append(',');
            }
        } else if (levelFilter == 2) {
            filtroPrincipal += " and d.idManager4Evaluado in(?1) ";
            ids = ResultsCSI.getMatrixEvaluatedAreas(levelFilter, corporation, filterStructure);
            StringBuilder sb = new StringBuilder();

            for (Integer s : ids) {
                sb.append(s).append(',');
            }
        } else if (levelFilter == 3) {
            filtroPrincipal += " and d.idManager5Evaluado in(?1) ";
            ids = ResultsCSI.getMatrixEvaluatedAreas(levelFilter, management, filterStructure);

            StringBuilder sb = new StringBuilder();

            for (Integer s : ids) {
                sb.append(s).append(',');
            }
        } else if (levelFilter == 4) {
            filtroPrincipal += " and d.idManager5Evaluado in(?1)";
            ids = new ArrayList<>();
            ids.add(world);
            StringBuilder sb = new StringBuilder();

            for (Integer s : ids) {
                sb.append(s).append(',');
            }
        } else if (levelFilter == 5) {
            filtroPrincipal += " and d.userIdEvaluated in (?1)";
            ids = new ArrayList<>();
            ids.add(section);
            StringBuilder sb = new StringBuilder();

            for (Integer s : ids) {
                sb.append(s).append(',');
            }
        }
        String query = ResultsCSI.buildMatrixCorpQuery(filtroPrincipal, levelFilter, filter);

        if (levelFilter == 4 || levelFilter == 5) {
            query = ResultsCSI.buildMatrixCorpQuerySectionLevel(filtroPrincipal, filter);

        } else if (levelFilter == 6) {
            filtroPrincipal += " where d.userIdEvaluated in (?1)";
            ids = new ArrayList<>();

            ids.add(evaluated);
            query = ResultsCSI.buildMatrixCorpQueryEvaluatedEmployee(filtroPrincipal, filter);
        }

        JPAQuery jpaQuery = ResultsCSI.getFilteredDimResultsJPAQuery(filter, query, ids);

        List<MatrixRawLineReportResultDto> matrixReportResultDtoList = jpaQuery.fetch();

        Map<Integer, String> nextCatalogMap = new HashMap<>();

        for (MatrixRawLineReportResultDto matrixRawLineReportResultDto : matrixReportResultDtoList) {
            nextCatalogMap.put(matrixRawLineReportResultDto.getIdDireccionManager3Evaluado(),
                    matrixRawLineReportResultDto.getDireccionManager3Evaluado());
        }

        List<ResultCatalogDto> nextCatalog = new ArrayList<>();

        for (Integer idCatalog : nextCatalogMap.keySet()) {
            ResultCatalogDto catalog = new ResultCatalogDto(idCatalog, nextCatalogMap.get(idCatalog));
            nextCatalog.add(catalog);
        }

        MatrixReportResultDto matrixReport = ResultsCSI.getMatrixReportResultDto(matrixReportResultDtoList);

        MatrixReportResponseWrapperDto wrapper = new MatrixReportResponseWrapperDto(matrixReport, nextCatalog);

        List<String> catalogos = new ArrayList<>();

        for (ResultCatalogDto result : wrapper.getNextCatalog()) {
            catalogos.add(result.getDesc());
        }

        cat = catalogos;
        return wrapper;
    }

    /**
     * Método para obtener los resultados que llenarán el gráfico de barras, 
     * en caso de haber elegido Áreas evaluadas periódicamente
     * 
     * @return ResultResponseWrapperAreaDto
     */
    public static ResultResponseWrapperAreaDto getArea() {

        SesionFilterWrapper filter = new SesionFilterWrapper(session, "d.idSurvey", "s.surveyDate", 0);

        String extraFilters = "";
        if (filter.getExtraFilters() != null && !filter.getExtraFilters().isEmpty()) {
            extraFilters = " AND " + filter.getExtraFilters() + " ";
        }

        getFilters();

        String filtroPrincipal = ResultsCSI.getPrincipalFilterAreas(levelFilter, corporation, management, world,
                section, evaluated);

        String queryArea = ResultsCSI.createQuery4Areas(filtroPrincipal, levelFilter, filter);

        if (levelFilter == 4 || levelFilter == 5) {
            queryArea = ResultsCSI.createQuery4SectionAreas(filtroPrincipal, filter);

        } else if (levelFilter == 6) {
            queryArea = ResultsCSI.createQuery4Employee(filtroPrincipal, filter);
        }

        JPAQuery rawResultsBefore = ResultsCSI.getMatrixRawAreaLineResultDtoJPAQuery(filter, queryArea);
        List<MatrixRawAreaLineResultDto> rawResults = rawResultsBefore.fetch();

        List<ResultAreaDto> csiHeaderAreaReport = ResultsCSI.processRowHeaderAreaData(rawResults);

        List<ResultCatalogDto> nextCatalog = new ArrayList<>();

        for (ResultAreaDto resultArea : csiHeaderAreaReport) {
            ResultCatalogDto catag = new ResultCatalogDto(resultArea.getIdManager(), resultArea.getNombreArea());
            nextCatalog.add(catag);
        }
        ResultResponseWrapperAreaDto wrapper = new ResultResponseWrapperAreaDto(csiHeaderAreaReport, nextCatalog);

        List<String> catalogos = new ArrayList<>();

        for (ResultCatalogDto result : wrapper.getNextCatalog()) {
            catalogos.add(result.getDesc());
        }

        cat = catalogos;
        return wrapper;
    }
    
    /**
     * Método que obtiene los datos por año para llenar el gráfico de barras
     * 
     * @return List de tipo GraphRawSeriesDto
     */
    public static List<GraphRawSeriesDto> getYearResumeRawSeries() {
        SesionFilterWrapper filter = new SesionFilterWrapper(session, "d.idSurvey", "s.surveyDate", 0);
        StringBuilder queryArea = new StringBuilder();

        String extraFilters = "";
        if (filter.getExtraFilters() != null && !filter.getExtraFilters().isEmpty()) {
            extraFilters = " AND " + filter.getExtraFilters() + " ";
        }

        queryArea.append("select new " + GraphRawSeriesDto.class.getName() + "( ").append(" s.surveyDate, ")
                .append(" sum(d.countPromotores - d.countDetractores)*1.0, ")
                .append(" sum(d.countPromotores + d.countDetractores + d.countNeutros)*1.0 ").append(" ) ")
                .append(" from CsiDimensionResultsZone d, Survey s ").append(" where d.idSurvey = s.id ")
                .append(filter.getCSITypeFilterSurveyTypeNameEqualsCSI("s.surveyType.name")) // filtro
                                                                                             // para
                                                                                             // traer
                                                                                             // solamente
                                                                                             // las
                                                                                             // encuestas
                                                                                             // CSI
                .append(extraFilters).append(" group by ").append(" s.surveyDate ").append(" order by ")
                .append(" s.surveyDate ");

        List<GraphRawSeriesDto> rawSeries = null;

        if (filter.isHasDate()) {
            rawSeries = CsiDimensionResultsZone.find(queryArea.toString(), filter.getDateFrom(), filter.getDateTo())
                    .fetch();
        } else if (filter.isHasSurvey()) {
            rawSeries = CsiDimensionResultsZone.find(queryArea.toString(), filter.getFilterEncuesta()).fetch();
        } else {

            rawSeries = CsiDimensionResultsZone.find(queryArea.toString()).fetch();
        }

        return rawSeries;
    }

    static String colors[] = new String[] { "rgb(48, 104, 152)", "rgb(74, 145, 13)", "rgb(191, 255, 0)",
            "rgb(0, 255, 255)", "rgb(112,112,112)", "rgb(255, 0, 255)", "rgb(255, 0, 0)", "rgb(255, 204, 204)",
            "rgb(220,220,220)", "rgb(153, 0, 255)"

    };
    
    /**
     * Método para llenar el gráfico de barras en caso de no haber elegido ningún filtro
     * 
     * @return GraphSeriesResponseDto
     */
    private static GraphSeriesResponseDto getGraphSeriesResponseDto() {

        List<GraphRawSeriesDto> rawSeries = getYearResumeRawSeries();

        Map<Integer, GraphSerieDto> distinct = new HashMap<>();
        for (GraphRawSeriesDto r : rawSeries) {
            Calendar calendar = new GregorianCalendar();
            calendar.setTime(r.getSurveyDate());

            int year = calendar.get(Calendar.YEAR);
            int month = calendar.get(Calendar.MONTH);

            if (!distinct.containsKey(year)) {
                GraphSerieDto ser = new GraphSerieDto();
                distinct.put(year, ser);
                ser.setName("" + year);
            }
            GraphSerieDto serie = distinct.get(year);
            serie.getData()[month] = roundDouble2Decimales((r.getPromMenosDetra() * 100.0) / r.getTotalSumResult());
        }
        GraphSeriesResponseDto series = new GraphSeriesResponseDto();
        List<Integer> keys = new ArrayList<>();
        keys.addAll(distinct.keySet());

        for (int i = 0; i < distinct.size(); i++) {
            Integer key = keys.get(i);
            series.getColors().add(colors[i % colors.length]);
            series.getSeries().add(distinct.get(key));
        }

        return series;
    }
    
    /**
     * Método para llenar el gráfico de barras. 
     * <br> Para Tienda - Tienda o Tienda - Corporativo
     * 
     * @return GraphSeriesResponseDto
     */
    private static GraphSeriesResponseDto getGraphSeriesResultsDto() {
        ResultResponseWrapperDto rawSeries = null;
        rawSeries = getResults();

        List<GraphSerieDto> ser = new ArrayList<>();

        GraphSerieDto serie = new GraphSerieDto(rawSeries.getResultDetailDtoList().size());
        GraphSeriesResponseDto series = new GraphSeriesResponseDto();
        series.setSeries(ser);

        int cont = 0;
        for (ResultDetailDto result : rawSeries.getResultDetailDtoList()) {
            serie.getData()[cont] = roundDouble2Decimales(result.getResult());
            series.getColors().add(colors[cont % colors.length]);
            cont++;
        }
        serie.setName(" ");
        ser.add(serie);

        return series;
    }

    /**
     * Método para llenar el gráfico de barras. 
     * <br> Para Corporativo - Corporativo
     * 
     * @return GraphSeriesResponseDto
     */
    private static GraphSeriesResponseDto getGraphSeriesMatrixDto() {
        MatrixReportResponseWrapperDto rawSeries = null;
        rawSeries = getMatrix();

        List<GraphSerieDto> ser = new ArrayList<>();

        GraphSerieDto serie = new GraphSerieDto(rawSeries.getMatrixReport().getRows().size());
        GraphSeriesResponseDto series = new GraphSeriesResponseDto();
        series.setSeries(ser);

        int cont = 0;
        for (MatrixReportRowResultDto result : rawSeries.getMatrixReport().getRows().values()) {
            serie.getData()[cont] = roundDouble2Decimales(result.getTotal());
            series.getColors().add(colors[cont % colors.length]);
            cont++;
        }
        serie.setName(" ");
        ser.add(serie);

        return series;
    }
    
    /**
     * Método para llenar el gráfico de barras. 
     * <br> Para Áreas evaluadas periódicamente
     * 
     * @return GraphSeriesResponseDto
     */
    private static GraphSeriesResponseDto getGraphSeriesAreasDto() {
        ResultResponseWrapperAreaDto rawSeries = null;
        rawSeries = getArea();

        List<GraphSerieDto> ser = new ArrayList<>();

        GraphSerieDto serie = new GraphSerieDto(rawSeries.getResultHeaderAreaDtoList().size());
        GraphSeriesResponseDto series = new GraphSeriesResponseDto();
        series.setSeries(ser);

        int cont = 0;
        for (ResultAreaDto result : rawSeries.getResultHeaderAreaDtoList()) {
            serie.getData()[cont] = roundDouble2Decimales(result.getResult());
            series.getColors().add(colors[cont % colors.length]);
            cont++;
        }
        serie.setName(" ");
        ser.add(serie);

        return series;
    }

    
    private static GraphSerieDto getGraphSerieValues(String name) {

        Double[] values = new Double[12];
        for (int i = 0; i < 12; i++) {
            values[i] = Math.floor(Math.random() * 30) + 70;
        }
        GraphSerieDto temp = new GraphSerieDto(name, values);
        return temp;
    }
    
    /**
     * Recupera los datos para llenar el gráfico circular 
     * 
     * @return GraphResumeDto
     */
    private static GraphResumeDto getGraphResumeData() {
        GraphResumeDto resume = getGraphResumeDto();
        return resume;
    }

}
