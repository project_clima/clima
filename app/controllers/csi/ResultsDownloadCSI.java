package controllers.csi;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.poi.openxml4j.exceptions.InvalidFormatException;

import json.csi.dto.CsiDBRepor;
import json.csi.dto.CsiDBReporDTO;
import json.csi.dto.CsiDBReportDTO;
import json.csi.dto.MatrixRawAreaLineResultDto;
import json.csi.dto.MatrixRawLineReportResultDto;
import json.csi.dto.MatrixReportResultDto;
import json.csi.dto.MatrixReportRowResultDto;
import json.csi.dto.ResultAreaDto;
import json.csi.dto.ResultCatalogDto;
import json.csi.dto.ResultDetailDto;
import json.csi.dto.SesionFilterWrapper;
import models.csi.CsiDimensionResultsZone;
import net.sf.jxls.exception.ParsePropertyException;
import play.db.jpa.GenericModel.JPAQuery;
import utils.RenderExcel;

public class ResultsDownloadCSI extends ResultsCSI {

	/**
	 * Método para descargar en excel los datos correspondientes a los reportes
	 *  
	 * @param management id del Empleado
	 * 
	 * @throws ParsePropertyException
	 * @throws InvalidFormatException
	 * @throws IOException
	 */
    public static void downloadBD(Integer management)
            throws ParsePropertyException, InvalidFormatException, IOException {

        SimpleDateFormat format = new SimpleDateFormat("dd/MM/yyyy");
        Date date = new Date();
        String formatDate = format.format(date);
        Integer idEmploye = management;

        // RECORRER LISTA PARA OBTENER LAS PREGUNTAS DE LA ENCUESTA
        List<CsiDBReporDTO> listDB = CsiDimensionResultsZone.find(getQueryDetalleP1(), idEmploye).fetch();

        Integer maxPreguntas = 0;

        for (CsiDBReporDTO itemReport : listDB) {
            List<CsiDBRepor> listaPreguntas = CsiDimensionResultsZone
                    .find(getQueryDetalleP3(), itemReport.getIdEvaluated(), itemReport.getIdEvaluator()).fetch();
            itemReport.setPreguntaRespuestaList(listaPreguntas);
            if (listaPreguntas.size() > maxPreguntas) {
                maxPreguntas = listaPreguntas.size();
            }

        }
        List<Integer> auxPreguntas = new ArrayList<Integer>();
        for (int i = 1; i <= maxPreguntas; i++) {
            auxPreguntas.add(i);
        }

        List<CsiDBReportDTO> itemsDB = CsiDimensionResultsZone.find(getQueryDetalleP2()).fetch();

        renderArgs.put("itemsDB", listDB);
        renderArgs.put("auxPreguntas", auxPreguntas);
        renderArgs.put("itemsDB2", itemsDB);
        renderArgs.put(RenderExcel.RA_FILENAME, "DB_" + formatDate + ".xlsx");
        RenderExcel renderExcel = new RenderExcel("app/excel/views/ResultsCSI/downloadBD.xlsx");

        renderExcel.render();
    }

    /**
     * Método para descargar en excel los datos del reporte de Áreas evaluadas periódicamente
     * @param csiType
     * @param levelFilter
     * @param corporation
     * @param management
     * @param world
     * @param section
     * @param evaluated
     * 
     * @throws ParsePropertyException
     * @throws InvalidFormatException
     * @throws IOException
     */
    public void downloadAreas(Integer csiType, Integer levelFilter, Integer corporation, Integer management,
            Integer world, Integer section, Integer evaluated)
            throws ParsePropertyException, InvalidFormatException, IOException {

        SimpleDateFormat format = new SimpleDateFormat("dd/MM/yyyy");
        Date date = new Date();
        String formatDate = format.format(date);

        String filtroPrincipal = getPrincipalFilterAreas(levelFilter, corporation, management, world, section,
                evaluated);

        SesionFilterWrapper filter = new SesionFilterWrapper(session, "d.idSurvey", "s.surveyDate", 0);
        String queryArea = createQuery4Areas(filtroPrincipal, levelFilter, filter);

        if (levelFilter == 4 || levelFilter == 5) {
            queryArea = createQuery4SectionAreas(filtroPrincipal, filter);

        } else if (levelFilter == 6) {
            queryArea = createQuery4Employee(filtroPrincipal, filter);
        }

        JPAQuery rawResultsBefore = getMatrixRawAreaLineResultDtoJPAQuery(filter, queryArea);
        List<MatrixRawAreaLineResultDto> rawResults = rawResultsBefore.fetch();

        List<ResultAreaDto> csiHeaderAreaReport = processRowHeaderAreaData(rawResults);

        List<Object[]> results = new ArrayList<>();
        for (ResultAreaDto resultAreaDto : csiHeaderAreaReport) {
            Object[] row = new Object[13];
            row[0] = resultAreaDto.getNombreArea();
            row[1] = resultAreaDto.getEnero();
            row[2] = resultAreaDto.getFebrero();
            row[3] = resultAreaDto.getMarzo();
            row[4] = resultAreaDto.getAbril();
            row[5] = resultAreaDto.getMayo();
            row[6] = resultAreaDto.getJunio();
            row[7] = resultAreaDto.getJulio();
            row[8] = resultAreaDto.getAgosto();
            row[9] = resultAreaDto.getSeptiembre();
            row[10] = resultAreaDto.getOctubre();
            row[11] = resultAreaDto.getNoviembre();
            row[12] = resultAreaDto.getDiciembre();
            results.add(row);
        }

        renderArgs.put("structures", results);
        renderArgs.put(RenderExcel.RA_FILENAME, "Resultados áreas evaluadas periodicamente " + formatDate + ".xlsx");
        RenderExcel renderExcel = new RenderExcel("app/excel/views/ResultsCSI/areas.xlsx");
        renderExcel.render();

    }

    /**
     * Método para descargar en excel los datos del reporte de Corporativo - Corporativo
     * @param csiType
     * @param levelFilter
     * @param corporation
     * @param management
     * @param world
     * @param section
     * @param evaluated
     * 
     * @throws ParsePropertyException
     * @throws InvalidFormatException
     * @throws IOException
     */
    public void downloadCorporativo(Integer csiType, Integer levelFilter, Integer corporation, Integer management,
            Integer world, Integer section, Integer evaluated)
            throws ParsePropertyException, InvalidFormatException, IOException {

        SimpleDateFormat format = new SimpleDateFormat("dd/MM/yyyy");
        Date date = new Date();
        String formatDate = format.format(date);

        List<Integer> ids = new ArrayList<>();

        String filtroPrincipal = "";

        SesionFilterWrapper filter = new SesionFilterWrapper(session, "d.idSurvey", "s.surveyDate", 1);
        SesionFilterWrapper filterStructure = new SesionFilterWrapper(session, null, "e.structureDate", 0);

        if (levelFilter == 1) {
            filtroPrincipal = " where d.idManager3Evaluado in(?1) ";
            ids = getMatrixEvaluatedAreas(levelFilter, null, filterStructure);
        } else if (levelFilter == 2) {
            filtroPrincipal = " where d.idManager4Evaluado in(?1) ";
            ids = getMatrixEvaluatedAreas(levelFilter, corporation, filterStructure);
        } else if (levelFilter == 3) {
            filtroPrincipal = " where d.idManager5Evaluado in(?1) ";
            ids = getMatrixEvaluatedAreas(levelFilter, management, filterStructure);
        } else if (levelFilter == 4) {
            filtroPrincipal = " where d.idManager5Evaluado in(?1)";
            ids = new ArrayList<>();
            ids.add(world);
        } else if (levelFilter == 5) {
            filtroPrincipal = " where d.userIdEvaluated in (?1)";
            ids = new ArrayList<>();
            ids.add(section);

        }
        String query = buildMatrixCorpQuery(filtroPrincipal, levelFilter, filter);

        if (levelFilter == 4 || levelFilter == 5) {
            query = buildMatrixCorpQuerySectionLevel(filtroPrincipal, filter);

        } else if (levelFilter == 6) {
            filtroPrincipal = " where d.userIdEvaluated in (?1)";
            ids = new ArrayList<>();
            ids.add(evaluated);
            query = buildMatrixCorpQueryEvaluatedEmployee(filtroPrincipal, filter);
        }

        JPAQuery jpaQuery = getFilteredDimResultsJPAQuery(filter, query, ids);
        List<MatrixRawLineReportResultDto> matrixReportResultDtoList = jpaQuery.fetch();

        Map<Integer, String> nextCatalogMap = new HashMap<>();

        for (MatrixRawLineReportResultDto matrixRawLineReportResultDto : matrixReportResultDtoList) {
            nextCatalogMap.put(matrixRawLineReportResultDto.getIdDireccionManager3Evaluado(),
                    matrixRawLineReportResultDto.getDireccionManager3Evaluado());
        }

        List<ResultCatalogDto> nextCatalog = new ArrayList<>();

        for (Integer idCatalog : nextCatalogMap.keySet()) {
            ResultCatalogDto catalog = new ResultCatalogDto(idCatalog, nextCatalogMap.get(idCatalog));
            nextCatalog.add(catalog);
        }

        MatrixReportResultDto matrixReport = getMatrixReportResultDto(matrixReportResultDtoList);
        List<Object[]> report = new ArrayList<>();

        Object[] row = new Object[matrixReport.getCountColumns() + 2];
        ;
        int j = 0;

        row[j++] = "Areas";
        row[j++] = "Total";

        for (Integer idColumn : matrixReport.getColNames().keySet()) {
            row[j++] = matrixReport.getColNames().get(idColumn);
        }

        report.add(row); // Add headers

        for (Integer idRow : matrixReport.getRows().keySet()) {
            row = new Object[matrixReport.getCountColumns() + 2];
            MatrixReportRowResultDto resultRow = matrixReport.getRows().get(idRow);
            int i = 0;
            row[i++] = matrixReport.getRowNames().get(idRow);
            row[i++] = resultRow.getTotal();
            for (Integer idColumn : matrixReport.getColNames().keySet()) {
                row[i++] = resultRow.getValues().get(idColumn) != null ? resultRow.getValues().get(idColumn).getTotal()
                        : "--";
            }

            report.add(row);
        }

        renderArgs.put("structures", report);
        renderArgs.put(RenderExcel.RA_FILENAME, "Resultados Corporativo - Corporativo " + formatDate + ".xlsx");
        RenderExcel renderExcel = new RenderExcel("app/excel/views/ResultsCSI/corporativo.xlsx");
        renderExcel.render();
    }

    /**
     * Método para descargar en excel los datos de Tienda - Tienda o Tienda - Corporativo
     * @param csiType
     * @param levelFilter
     * @param corporation
     * @param management
     * @param world
     * @param section
     * @param evaluated
     * 
     * @throws ParsePropertyException
     * @throws InvalidFormatException
     * @throws IOException
     */
    public void downloadTienda(Integer csiType, Integer levelFilter, Integer corporation, Integer management,
            Integer world, Integer section, Integer evaluated)
            throws ParsePropertyException, InvalidFormatException, IOException {

        SesionFilterWrapper filter = new SesionFilterWrapper(session, "d.idSurvey", "s.surveyDate", 0);

        SimpleDateFormat format = new SimpleDateFormat("dd/MM/yyyy");
        Date date = new Date();
        String formatDate = format.format(date);

        String filtroPrincipal = getPrincipalFilter(csiType, levelFilter, corporation, management, world, section,
                evaluated);
        //String extraFilters = filter.getExtraFilters();

        String extraFilters = "";
        if (!filter.getExtraFilters().trim().isEmpty()) {
            extraFilters = " AND " + filter.getExtraFilters() + " ";
        }
        
        SesionFilterWrapper filterStructure = new SesionFilterWrapper(session, null, "e.structureDate", 0);
        List<ResultCatalogDto> nextCatalog = getNextCatalogs(levelFilter, corporation, management, world, section,
                evaluated, filterStructure);

        Map<Integer, String> mapCatalog = new HashMap<>();
        for (ResultCatalogDto resultCatalogDto : nextCatalog) {
            mapCatalog.put(resultCatalogDto.getId(), resultCatalogDto.getDesc());
        }
        String idsCatalogo = getCatalogos(nextCatalog);

        List<Integer> orders = getOrdersList(filterStructure, filtroPrincipal);

        List<ResultDetailDto> csiDetailReport = getDetailReport(levelFilter, orders, filtroPrincipal, extraFilters,
                corporation, idsCatalogo, management, world, section, evaluated, filter).fetch();

        List<Object[]> results = new ArrayList<>();
        for (ResultDetailDto resultDetailDto : csiDetailReport) {
            Object[] row = new Object[9];
            row[0] = mapCatalog.get(resultDetailDto.getIdAgrupador());
            row[1] = resultDetailDto.getResult();
            row[2] = resultDetailDto.getDetractores();
            row[3] = resultDetailDto.getNeutros();
            row[4] = resultDetailDto.getPromotores();
            row[5] = resultDetailDto.getP1();
            row[6] = resultDetailDto.getP2();
            row[7] = resultDetailDto.getP3();
            row[8] = resultDetailDto.getP4();
            results.add(row);
        }

        renderArgs.put("structures", results);
        renderArgs.put(RenderExcel.RA_FILENAME, "Resultados " + formatDate + ".xlsx");
        RenderExcel renderExcel = new RenderExcel("app/excel/views/ResultsCSI/results.xlsx");
        renderExcel.render();

    }
}
