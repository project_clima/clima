package controllers.csi;

import com.google.gson.Gson;
import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import play.mvc.*;
import java.util.*;
import json.entities.TreeData;
import models.*;
import net.sf.jxls.exception.ParsePropertyException;
import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import play.Logger;
import play.data.validation.Valid;
import play.data.validation.Validation;
import play.db.jpa.JPA;
import querys.SurveysGeneratorQuery;
import utils.Pagination;
import utils.RenderExcel;
import controllers.Secure;
import controllers.Check;

@With(Secure.class)
public class SurveysGenerator extends Controller {
	/**
	 * Muestra la encuesta seleccionada
	 * @param surveyId
	 */
    public static void sections(int surveyId) {
        Survey survey = Survey.findById(surveyId);
        
        render(survey);
    }
    
    /**
     * Método para guardar la encuesta
     * @param survey
     */
    public static void saveSection(@Valid Survey survey) {
        if(validation.hasErrors()) {
            renderJSON(new utils.Message(
                "Error al guardar la información. Por favor, revise el formulario cuidadosamente.",
                null,
                true)
            );
        }
        
        survey.modifiedBy = Usuario.find("username = ?1", session.get("username")).first();
        survey.modificationDate = new Date();
        
        survey.save();        
        renderJSON(new utils.Message(survey.id.toString(), "validation", false));
    }
    
    /**
     * Método para editar la sección de la encuesta
     * @param section
     * @param surveyId
     */
    public static void editSurveySection(String section, int surveyId) {
        Survey survey              = Survey.findById(surveyId);
        List<QuestionType> types   = QuestionType.find("id in (3, 6)").fetch();
        List<Factor> factors       = Factor.find("survey.id = ?1", surveyId).fetch();
        
        String template = "SurveysGenerator/" + section + "-section.html";
        
        renderArgs.put("survey", survey);
        renderArgs.put("types", types);
        renderArgs.put("factors", factors);
        renderTemplate(template);
    }
    
    /**
     * Obtiene la encuesta seleccionada
     * @param surveyId
     */
    public static void getSurvey(int surveyId) {
        Survey survey   = Survey.findById(surveyId);
        renderJSON(survey);
    }
    
    /**
     * Muestra una vista previa de la encuesta
     * @param surveyId
     */
    public static void preview(int surveyId) {
        Survey survey  = Survey.findById(surveyId);
        List questions = utils.Survey.getSurveyQuestions(surveyId);
        
        
        List<String[]> evaluated = new ArrayList();
        String[] person1 = {"1", "Mario", "Martínez"};
        String[] person2 = {"2", "Elizabeth", "Romo"};
        String[] person3 = {"3", "Claudia", "Sánchez"};
        
        evaluated.add(person1);
        evaluated.add(person2);
        evaluated.add(person3);
        
        response.setHeader("Cache-Control", "no-cache, no-store, max-age=0, must-revalidate, post-check=0, pre-check=0");
        render(surveyId, survey, questions, evaluated);
    }
}