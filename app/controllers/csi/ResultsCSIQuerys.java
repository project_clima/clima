package controllers.csi;

import java.util.List;

import json.csi.dto.ResultCatalogDto;
import json.csi.dto.SesionFilterWrapper;
import models.CSIEmployee;
import models.csi.CsiDimensionResultsZone;
import play.db.jpa.GenericModel.JPAQuery;
import play.mvc.Controller;

public class ResultsCSIQuerys extends Controller {
	/**
	 * Método para obtener la consulta que recupera los datos filtrados
	 * 
	 * @param filter
	 * @param queryHeader
	 * @return JPAQuery
	 */
    public static JPAQuery getFilteredDimResultsJPAQuery(SesionFilterWrapper filter, String queryHeader) {

        JPAQuery csiHeaderReportJPAQuery;
        if (filter.isHasDate() && filter.isHasSurvey()) {

            csiHeaderReportJPAQuery = CsiDimensionResultsZone.find(queryHeader, filter.getFilterEncuesta(),
                    filter.getDateFrom(), filter.getDateTo());
        } else if (filter.isHasDate()) {

            csiHeaderReportJPAQuery = CsiDimensionResultsZone.find(queryHeader, filter.getDateFrom(),
                    filter.getDateTo());
        } else if (filter.isHasSurvey()) {

            csiHeaderReportJPAQuery = CsiDimensionResultsZone.find(queryHeader, filter.getFilterEncuesta());
        } else {

            csiHeaderReportJPAQuery = CsiDimensionResultsZone.find(queryHeader);

        }

        return csiHeaderReportJPAQuery;
    }
    
    /**
     * Obtiene la consulta que recupera los datos filtrados
     * 
     * @param filter
     * @param queryHeader
     * @param obj ids de los catálogos
     * @return JPAQuery
     */
    public static JPAQuery getFilteredDimResultsJPAQuery(SesionFilterWrapper filter, String queryHeader,
            List<Integer> obj) {
        JPAQuery csiHeaderReportJPAQuery;

        if (filter.isHasDate() && filter.isHasSurvey()) {
            csiHeaderReportJPAQuery = CsiDimensionResultsZone.find(queryHeader, obj, filter.getFilterEncuesta(),
                    filter.getDateFrom(), filter.getDateTo());
        } else if (filter.isHasDate()) {
            csiHeaderReportJPAQuery = CsiDimensionResultsZone.find(queryHeader, obj, filter.getDateFrom(),
                    filter.getDateTo());
        } else if (filter.isHasSurvey()) {
            csiHeaderReportJPAQuery = CsiDimensionResultsZone.find(queryHeader, obj, filter.getFilterEncuesta());
        } else {
            csiHeaderReportJPAQuery = CsiDimensionResultsZone.find(queryHeader, obj);
        }
        return csiHeaderReportJPAQuery;
    }
    
    public static String getHederRowZoneColumnCal(String field) {
        return "(" + " sum(case when d.result > 8 then " + field + " else 0 end) " + "-"
                + " sum(case when d.result <=6 then " + field + " else 0 end)" + ") * 100.0	" + "/ " + "(" + "CASE sum("
                + field + " ) WHEN 0 THEN 1 ELSE sum(" + field + ") END " + ") ";
    }

    public static final String ROW_HEADER_QUERY_BASE = " sum(d.countPromotores - d.countDetractores ) * 100.0 "
            + "/ (CASE sum ( d.countDetractores + d.countNeutros + d.countPromotores ) " + " WHEN 0 THEN 1  "
            + " ELSE sum ( d.countDetractores + d.countNeutros + d.countPromotores ) END ) ,"
            + getHederRowZoneColumnCal("d.countZ1A") + " ," + getHederRowZoneColumnCal("d.countZ1B") + " ,"
            + getHederRowZoneColumnCal("d.countZ02") + " ," + getHederRowZoneColumnCal("d.countZ04") + " ,"
            + getHederRowZoneColumnCal("d.countZ04") + " ," + getHederRowZoneColumnCal("d.countZ05") + " ,"
            + getHederRowZoneColumnCal("d.countZ06") + " ," + getHederRowZoneColumnCal("d.countZ07") + " ,"
            + getHederRowZoneColumnCal("d.countZ08") + " ," + getHederRowZoneColumnCal("d.countZBT") + ") ";

    public static final String ROW_detail_QUERY_BASE = " sum(d.countPromotores - d.countDetractores ) * 100.0 "
            + "/ (CASE sum ( d.countDetractores + d.countNeutros + d.countPromotores ) " + " WHEN 0 THEN 1  "
            + " ELSE sum ( d.countDetractores + d.countNeutros + d.countPromotores ) END ) ,"
            + "sum(d.countDetractores * 100.0) / (CASE sum(d.countDetractores + d.countNeutros + d.countPromotores) WHEN 0 THEN 1 ELSE sum(d.countDetractores + d.countNeutros + d.countPromotores) END ),"
            + "sum(d.countNeutros     * 100.0) / (CASE sum(d.countDetractores + d.countNeutros + d.countPromotores) WHEN 0 THEN 1 ELSE sum(d.countDetractores + d.countNeutros + d.countPromotores) END ) ,"
            + "sum(d.countPromotores  * 100.0) / (CASE sum(d.countDetractores + d.countNeutros + d.countPromotores) WHEN 0 THEN 1 ELSE sum(d.countDetractores + d.countNeutros + d.countPromotores) END ) ,"

            + " ";

    public static String getDetailRowZoneColumnCal(int order) {
        return " (" + "SUM(" + "case when d.order = " + order
                + " then (d.countPromotores - d.countDetractores) else 0 end" + ") " + "/" + "SUM("
                + "case when d.order = " + order
                + " then (d.countPromotores + d.countNeutros + d.countDetractores) else 1 end" + ")" + ")*100.0  ";
    }

    /**
     * Método que obtiene la parte de la consulta que incluye el filtro de búsqueda
     * 
     * @param csiType
     * @param levelFilter
     * @param corporation
     * @param management
     * @param world
     * @param section
     * @param evaluated
     * @return String
     */
    public static String getPrincipalFilter(Integer csiType, Integer levelFilter, Integer corporation,
            Integer management, Integer world, Integer section, Integer evaluated) {

        String csiTypeFilter = " d.idCsiType = " + csiType;

        String filtroPrincipal = " ";

        if (levelFilter == 1) {
            filtroPrincipal = " where d.idManager3Evaluado in( 50092383 , 50092319 ) and" + csiTypeFilter;
        } else if (levelFilter == 2) {
            filtroPrincipal = " where d.idManager3Evaluado = " + corporation + " and" + csiTypeFilter;
        } else if (levelFilter == 3) {
            filtroPrincipal = " where d.idManager4Evaluado = " + management + " and" + csiTypeFilter;
        } else if (levelFilter == 4) {
            filtroPrincipal = " where d.idManager5Evaluado = " + world + " and" + csiTypeFilter;
        } else if (levelFilter == 5) {
            filtroPrincipal = " where d.userIdEvaluated = " + section + " and" + csiTypeFilter;
        } else if (levelFilter == 6) {
            filtroPrincipal = " where d.userIdEvaluated = " + evaluated + " and" + csiTypeFilter;
        }

        return filtroPrincipal;
    }

    /**
     * Obtiene los números de las preguntas
     * 
     * @param filter
     * @param filtroPrincipal
     * @return List de Integer
     */
    public static List<Integer> getOrdersList(SesionFilterWrapper filter, String filtroPrincipal) {
        String filtroAux = "";
        if (filtroPrincipal.indexOf("e.structureDate") >= 0) {
            filtroAux = ", CSIEmployee e ";
        }
        String query3 = "select distinct d.order " + " from CsiDimensionResultsZone d, Survey s " + filtroAux
                + filtroPrincipal + " order by d.order ASC ";

        return getFilteredDimResultsJPAQuery(filter, query3).fetch();
    }

    /**
     * Método que obtiene la parte de la consulta que incluye el filtro de búsqueda para Áreas evaluadas periódicamente
     * 
     * @param levelFilter
     * @param corporation
     * @param management
     * @param world
     * @param section
     * @param evaluated
     * @return String
     */
    public static String getPrincipalFilterAreas(Integer levelFilter, Integer corporation, Integer management,
            Integer world, Integer section, Integer evaluated) {

        String filtroPrincipal = " d.idCsiType = 4 and ";

        if (levelFilter == 1) {
            filtroPrincipal += " d.idManager3Evaluado  in (select distinct idManager3Evaluado from CsiDimensionResultsZone) ";
        } else if (levelFilter == 2) {
            filtroPrincipal += " d.idManager3Evaluado = " + corporation;
        } else if (levelFilter == 3) {
            filtroPrincipal += " d.idManager4Evaluado = " + management;
        } else if (levelFilter == 4) {
            filtroPrincipal += " d.idManager5Evaluado = " + world;
        } else if (levelFilter == 5) {
            filtroPrincipal += " d.userIdEvaluated = " + section;
        } else if (levelFilter == 6) {
            filtroPrincipal += " d.userIdEvaluated = " + evaluated;
        }

        return filtroPrincipal;
    }

    /**
     * Obtiene el query para recuperar los datos filtrados por nivel de Áreas evaluadas periódicamente
     * 
     * @param mainFilter
     * @param level
     * @param filter
     * @return String
     */
    public static String createQuery4Areas(String mainFilter, Integer level, SesionFilterWrapper filter) {

        level = level + 2;

        String extraFilters = "";
        if (filter.getExtraFilters() != null && !filter.getExtraFilters().isEmpty()) {
            extraFilters = " AND " + filter.getExtraFilters() + " ";
        }
        StringBuilder queryArea = new StringBuilder();
        queryArea.append("select new json.csi.dto.MatrixRawAreaLineResultDto( ").append(" s.surveyDate, ")
                .append(" d.idManager").append(level).append("Evaluado, ").append(" emp.department, ")
                .append(" sum(d.countPromotores - d.countDetractores)*1.0, ")
                .append(" sum(d.countPromotores + d.countDetractores + d.countNeutros)*1.0,")
                .append(" sum(d.countPromotores - d.countDetractores) * 100.0 / sum(d.countDetractores + d.countNeutros + d.countPromotores) ")
                .append(" ) ").append(" from CsiDimensionResultsZone d, CSIEmployee emp, Survey s").append(" where ")
                .append(mainFilter).append(" and d.idManager").append(level).append("Evaluado = emp.userId ")
                .append(" and d.idSurvey = s.id")

                .append(extraFilters).append(filter.getCSITypeFilterSurveyTypeNameEqualsCSI("s.surveyType.name"))
                .append(" group by ").append(" s.surveyDate, ").append(" d.idManager").append(level).append("Evaluado,")
                .append(" emp.department ").append(" order by ").append(" s.surveyDate,").append(" d.idManager")
                .append(level).append("Evaluado,").append(" emp.department ");
        return queryArea.toString();
    }

    /**
     * Obtiene el query para recuperar las secciones de los datos filtrados de Áreas evaluadas periódicamente
     * 
     * @param mainFilter
     * @param filter
     * @return String
     */
    public static String createQuery4SectionAreas(String mainFilter, SesionFilterWrapper filter) {

        String extraFilters = "";
        if (filter.getExtraFilters() != null && !filter.getExtraFilters().isEmpty()) {
            extraFilters = " AND " + filter.getExtraFilters() + " ";
        }
        StringBuilder queryArea = new StringBuilder();
        queryArea.append("select new json.csi.dto.MatrixRawAreaLineResultDto( ").append(" s.surveyDate, ")
                .append(" d.userIdEvaluated, ").append(" emp.department, ")
                .append(" sum(d.countPromotores - d.countDetractores)*1.0, ")
                .append(" sum(d.countPromotores + d.countDetractores + d.countNeutros)*1.0").append(" ) ")
                .append(" from CsiDimensionResultsZone d, CSIEmployee emp, Survey s").append(" where ")
                .append(mainFilter).append(" and d.userIdEvaluated = emp.userId").append(" and d.idSurvey = s.id")

                .append(extraFilters).append(filter.getCSITypeFilterSurveyTypeNameEqualsCSI("s.surveyType.name"))

                .append(" group by ").append(" s.surveyDate, ").append(" d.userIdEvaluated, ")
                .append(" emp.department ").append(" order by ").append(" s.surveyDate,").append(" d.userIdEvaluated,")
                .append(" emp.department ");

        return queryArea.toString();
    }

    /**
     * Obtiene el query para recuperar los empleados de los datos filtrados de Áreas evaluadas periódicamente
     * 
     * @param mainFilter
     * @param filter
     * @return String
     */
    public static String createQuery4Employee(String mainFilter, SesionFilterWrapper filter) {

        String extraFilters = "";
        if (filter.getExtraFilters() != null && !filter.getExtraFilters().isEmpty()) {
            extraFilters = " AND " + filter.getExtraFilters() + " ";
        }
        StringBuilder queryArea = new StringBuilder();
        queryArea.append("select new json.csi.dto.MatrixRawAreaLineResultDto( ").append(" s.surveyDate, ")
                .append(" d.userIdEvaluated, ").append(" CONCAT(emp.firstName, ' ', emp.lastName), ")
                .append(" sum(d.countPromotores - d.countDetractores)*1.0, ")
                .append(" sum(d.countPromotores + d.countDetractores + d.countNeutros)*1.0").append(" ) ")
                .append(" from CsiDimensionResultsZone d, CSIEmployee emp, Survey s").append(" where ")
                .append(mainFilter).append(" and d.userIdEvaluated = emp.userId").append(" and d.idSurvey = s.id")

                .append(extraFilters).append(filter.getCSITypeFilterSurveyTypeNameEqualsCSI("s.surveyType.name"))

                .append(" group by ").append(" s.surveyDate, ").append(" d.userIdEvaluated, ")
                .append(" CONCAT(emp.firstName, ' ', emp.lastName) ").append(" order by ").append(" s.surveyDate,")
                .append(" d.userIdEvaluated,").append(" CONCAT(emp.firstName, ' ', emp.lastName) ");

        return queryArea.toString();
    }

    /**
     * Método que obtiene los ids de los catálogos de áreas evaluadas periódicamente
     * 
     * @param level
     * @param idManager
     * @param filterStructure
     * @return List de Integer
     */
    public static List<Integer> getMatrixEvaluatedAreas(Integer level, Integer idManager,
            SesionFilterWrapper filterStructure) {

        StringBuffer query = new StringBuffer();
        query.append("select distinct ").append("d.idManager").append(level + 2).append("Evaluado")
                .append(" from  CsiDimensionResultsZone d , EmployeeCSI e ");

        String extraFilters = "";
        if (filterStructure.getExtraFilters() != null && !filterStructure.getExtraFilters().isEmpty()) {
            extraFilters = " AND " + filterStructure.getExtraFilters() + " ";
        }
        if (idManager != null) {
            query.append(" where d.idManager").append(level + 1).append("Evaluado = ").append(idManager);
            query.append(" and  ").append(" e.userId = ").append("d.idManager").append(level + 2).append("Evaluado ");

        } else {
            query.append(" where ").append(" e.userId = ").append("d.idManager").append(level + 2).append("Evaluado ");

        }
        query.append(" " + extraFilters + " ");

        JPAQuery jpqQuery = getFilteredDimResultsJPAQuery(filterStructure, query.toString());

        return jpqQuery.fetch();
    }

    /**
     * Obtiene el query para construir el reporte de Corporativo - Corporativo
     * 
     * @param filterPrincipal
     * @param level
     * @param filter
     * @return String
     */
    public static String buildMatrixCorpQuery(String filterPrincipal, Integer level, SesionFilterWrapper filter) {
        level = level + 2;

        String extraFilters = "";
        if (filter.getExtraFilters() != null && !filter.getExtraFilters().isEmpty()) {
            extraFilters = " AND " + filter.getExtraFilters() + " ";
        }
        String query = "select new json.csi.dto.MatrixRawLineReportResultDto( " + " d.idManager" + level + "Evaluado, " // igual
                + " emp.department, " + // igual

                " d.idDireccionManager3Evaluador, " + " d.direccionManager3Evaluador, " + // igual

                " SUM(d.countPromotores - d.countDetractores) * 1.0, " // igual
                + " SUM(d.countPromotores + d.countDetractores + d.countNeutros) * 1.0" + ")" + // igual

                " from CsiDimensionResultsZone d, CSIEmployee emp" // igual
                + " , Survey s " // otraaaa
                + filterPrincipal // igual

                + " and (d.idSurvey = s.id "

                + extraFilters // otraaaa
                + filter.getCSITypeFilterSurveyTypeNameEqualsCSI("s.surveyType.name") // otraaaa
                + ")" + " and d.idManager" + level + "Evaluado = emp.userId " // igual
                + "and d.direccionManager3Evaluador is not null " // igual

                + " group by " + " d.idManager" + level + "Evaluado, " + " emp.department, "
                + " d.idDireccionManager3Evaluador, " + " d.direccionManager3Evaluador "

                + " order by " + " d.idManager" + level + "Evaluado, " + " emp.department, " +

                " d.idDireccionManager3Evaluador, " + " d.direccionManager3Evaluador ";

        return query;
    }

    /**
     * Obtiene el query para construir el reporte corporativo - corporativo filtrado por sección
     * @param filterPrincipal
     * @param filter
     * @return String 
     */
    public static String buildMatrixCorpQuerySectionLevel(String filterPrincipal, SesionFilterWrapper filter) {

        String extraFilters = "";
        if (filter.getExtraFilters() != null && !filter.getExtraFilters().isEmpty()) {
            extraFilters = " AND " + filter.getExtraFilters() + " ";
        }
        String query = "select new json.csi.dto.MatrixRawLineReportResultDto( " + " d.userIdEvaluated, " // igual
                + " emp.department, " + // igual

                " d.idDireccionManager3Evaluador, " + " d.direccionManager3Evaluador, " + // igual

                " SUM(d.countPromotores - d.countDetractores) * 1.0, " // igual
                + " SUM(d.countPromotores + d.countDetractores + d.countNeutros) * 1.0" + ")" + // igual

                " from CsiDimensionResultsZone d, CSIEmployee emp" // igual
                + ", Survey s "

                + filterPrincipal // igual

                + " and (d.idSurvey = s.id " + extraFilters
                + filter.getCSITypeFilterSurveyTypeNameEqualsCSI("s.surveyType.name") + ")"

                + " and d.userIdEvaluated = emp.userId " // igual
                + "and d.direccionManager3Evaluador is not null " // igual

                + " group by " + " d.userIdEvaluated, " // igual
                + " emp.department, " + // igual

                " d.idDireccionManager3Evaluador, " + " d.direccionManager3Evaluador " + " order by " // igual
                + " d.userIdEvaluated, " + " emp.department, " + // igual

                " d.idDireccionManager3Evaluador, " + " d.direccionManager3Evaluador "; // igual

        return query;
    }

    /**
     * Obtiene el query para construir el reporte de corporativo corporativo que incluye el empleado evaluado
     * 
     * @param filterPrincipal
     * @param filter
     * @return String
     */
    public static String buildMatrixCorpQueryEvaluatedEmployee(String filterPrincipal, SesionFilterWrapper filter) {

        String extraFilters = "";
        if (filter.getExtraFilters() != null && !filter.getExtraFilters().isEmpty()) {
            extraFilters = " AND " + filter.getExtraFilters() + " ";
        }
        String query = "select new json.csi.dto.MatrixRawLineReportResultDto( " + " d.userIdEvaluated, "
                + " CONCAT(emp.firstName, ' ', emp.lastName), " +

                " d.idDireccionManager3Evaluador, " + " d.direccionManager3Evaluador, " +

                " SUM(d.countPromotores - d.countDetractores) * 1.0, "
                + " SUM(d.countPromotores + d.countDetractores + d.countNeutros) * 1.0" + ")" +

                " from CsiDimensionResultsZone d, CSIEmployee emp, Survey s " + filterPrincipal

                + " and (d.idSurvey = s.id " + extraFilters
                + filter.getCSITypeFilterSurveyTypeNameEqualsCSI("s.surveyType.name") + ") "
                + " and d.userIdEvaluated = emp.userId " + "and d.direccionManager3Evaluador is not null "
                + " group by " + " d.userIdEvaluated, " + " CONCAT(emp.firstName, ' ', emp.lastName), " +

                " d.idDireccionManager3Evaluador, " + " d.direccionManager3Evaluador " + " order by "
                + " d.userIdEvaluated, " + " CONCAT(emp.firstName, ' ',  emp.lastName), " +

                " d.idDireccionManager3Evaluador, " + " d.direccionManager3Evaluador ";

        return query;
    }

    /**
     * Obtiene la consulta para recuperar el detalle de la columna según el orden
     * @param orders
     * @return String
     */
    public static String getOrdersWhereString(List<Integer> orders) {
        StringBuilder sb = new StringBuilder();

        for (int i = 0; i < 15; i++) {
            if (i < orders.size()) {
                sb.append(getDetailRowZoneColumnCal(i + 1));
            } else {
                sb.append(" 1.0 ");
            }
            if (i < 14) {
                sb.append(" , ");
            }
        }

        return sb.toString();
    }

    /**
     * Obtiene el query para recuperar el detalle por nivel
     * @param filter
     * @param orders
     * @param level
     * @param idManager
     * @param idsCatalogo
     * @return String
     */
    public static String getQueryDetailsByLevel(SesionFilterWrapper filter, List<Integer> orders, Integer level,
            Integer idManager, String idsCatalogo) {
        Integer idLevelManager = level + 2;
        String catalogo = "in (0, " + idsCatalogo;

        if (idsCatalogo.equals(""))
            catalogo = "in (0";
        String extraFilters = " ";

        if (filter.getExtraFilters() != null && !filter.getExtraFilters().isEmpty()) {
            extraFilters = " AND " + filter.getExtraFilters() + " ";
        }

        return "select new json.csi.dto.ResultDetailDto( d.idManager" + idLevelManager + "Evaluado ,"
                + ROW_detail_QUERY_BASE + getOrdersWhereString(orders) + ") "

                + " from CsiDimensionResultsZone d, Survey s "

                + "where " + "  d.idSurvey = s.id " + extraFilters
                + filter.getCSITypeFilterSurveyTypeNameEqualsCSI("s.surveyType.name") + " AND d.idManager"
                + idLevelManager + "Evaluado " + catalogo + ") " + " AND d.idManager" + idLevelManager
                + "Evaluado  <>  0"

                + " group by d.idManager" + idLevelManager + "Evaluado" + " ";
    }

    /**
     * Obtiene el query que recupera el detalle por sección
     * @param filter
     * @param orders
     * @param idsCatalogo
     * @return String
     */
    public static String getQueryDetailsSectionLevel(SesionFilterWrapper filter, List<Integer> orders,
            String idsCatalogo) {

        String catalogo = "in (0, " + idsCatalogo;

        if (idsCatalogo.equals(""))
            catalogo = "in (0";

        String extraFilters = " ";
        if (filter.getExtraFilters() != null && !filter.getExtraFilters().isEmpty()) {
            extraFilters = " AND " + filter.getExtraFilters() + " ";
        }

        return "select new json.csi.dto.ResultDetailDto( d.userIdEvaluated ," + ROW_detail_QUERY_BASE
                + getOrdersWhereString(orders) + ") " + " from CsiDimensionResultsZone d, Survey s " + " where "

                + "  d.idSurvey = s.id " + extraFilters
                + filter.getCSITypeFilterSurveyTypeNameEqualsCSI("s.surveyType.name")

                + " AND d.userIdEvaluated " + catalogo + ") " + " and d.userIdEvaluated  <>  0 "
                + " group by d.userIdEvaluated" + " ";
    }

    /**
     * Obtiene el query que recupera los datos de Áreas evaluadas periódicamente
     * @param filter
     * @param queryArea
     * @return JPAQuery
     */
    public static JPAQuery getMatrixRawAreaLineResultDtoJPAQuery(SesionFilterWrapper filter, String queryArea) {
        JPAQuery rawResultsBefore;
        if (filter.isHasDate() && filter.isHasSurvey()) {
            rawResultsBefore = CsiDimensionResultsZone.find(queryArea.toString(), filter.getFilterEncuesta(),
                    filter.getDateFrom(), filter.getDateTo());
        } else if (filter.isHasDate()) {
            rawResultsBefore = CsiDimensionResultsZone.find(queryArea.toString(), filter.getDateFrom(),
                    filter.getDateTo());
        } else if (filter.isHasSurvey()) {
            rawResultsBefore = CsiDimensionResultsZone.find(queryArea.toString(), filter.getFilterEncuesta());
        } else {
            rawResultsBefore = CsiDimensionResultsZone.find(queryArea.toString());
        }
        return rawResultsBefore;
    }

    /**
     * Obtiene los catálogos siguientes de la búsqueda
     * @param idManager
     * @param filter
     * @return List de ResultCatalogDto
     */
    public static List<ResultCatalogDto> loadNextCatalog(Integer idManager, SesionFilterWrapper filter) {

        JPAQuery nextCatalogJPAquery;
        if (filter.isHasDate()) {
            String extra = " AND " + filter.getExtraSessionFiltersWhere(1);
            nextCatalogJPAquery = CSIEmployee.find("select new json.csi.dto.ResultCatalogDto(e.userId, e.department)  "
                    + " from CSIEmployee e" + " where e.manager = ?1  " + extra, idManager, filter.getDateFrom(),
                    filter.getDateTo());

        } else {
            nextCatalogJPAquery = CSIEmployee.find("select new json.csi.dto.ResultCatalogDto(e.userId, e.department)  "
                    + " from CSIEmployee e" + " where e.manager = ?1  ", idManager);
        }
        return nextCatalogJPAquery.fetch();
    }

    /**
     * Obtiene los catálogos del usuario por área
     * @param idUser
     * @param filter
     * @return List de ResultCatalogDto
     */
    public static List<ResultCatalogDto> loadUserIdCatalog(Integer idUser, SesionFilterWrapper filter) {
        JPAQuery nextCatalogJPAquery;
        if (filter.isHasDate()) {
            String extra = " AND " + filter.getExtraSessionFiltersWhere(1);
            nextCatalogJPAquery = CSIEmployee.find("select new json.csi.dto.ResultCatalogDto(e.userId, e.department)  "
                    + " from CSIEmployee e" + " where e.userId = ?1 " + extra, idUser, filter.getDateFrom(),
                    filter.getDateTo());

        } else {
            nextCatalogJPAquery = CSIEmployee.find("select new json.csi.dto.ResultCatalogDto(e.userId, e.department)  "
                    + " from CSIEmployee e" + " where e.userId = ?1 ", idUser);
        }

        return nextCatalogJPAquery.fetch();
    }

    /**
     * Obtiene los catálogos del usuario por nombre
     * @param idUser
     * @param filter
     * @return List de ResultCatalogDto
     */
    public static List<ResultCatalogDto> loadUserNameCatalog(Integer idUser, SesionFilterWrapper filter) {
        JPAQuery nextCatalogJPAquery;

        if (filter.isHasDate()) {
            String extra = " AND " + filter.getExtraSessionFiltersWhere(1);
            nextCatalogJPAquery = CSIEmployee.find(
                    "select new json.csi.dto.ResultCatalogDto(e.userId, concat(e.firstName, ' ', e.lastName))  "
                            + " from CSIEmployee e" + " where e.userId = ?1 " + extra,
                    idUser, filter.getDateFrom(), filter.getDateTo());

        } else {
            nextCatalogJPAquery = CSIEmployee
                    .find("select new json.csi.dto.ResultCatalogDto(e.userId, concat(e.firstName, ' ', e.lastName))  "
                            + " from CSIEmployee e" + " where e.userId = ?1 ", idUser);
        }

        return nextCatalogJPAquery.fetch();
    }

    /**
     * Obtiene el query que recupera el detalle de la pregunta 1
     * @return String
     */
    public static String getQueryDetalleP1() {
        StringBuffer query = new StringBuffer();
        query.append("SELECT ");
        query.append("	new json.csi.dto.CsiDBReporDTO( ");
        query.append("	CSIRES.idEvaluated,");
        query.append("	CSIRES.idEvaluator,");
        query.append("	EVALUADO.firstName,");
        query.append("	EVALUADO.lastName,");
        query.append("	CSIRES.subDivisionEvaluator ,");// zona
        query.append("	S.descriptionSubdivision,");// subDivision
        query.append("	S.descriptionStore,");// almacen
        query.append("	EVALUADOR.title,");// evaluator
        query.append("	EVALUADO.management,");// area4
        query.append("	CSIRES.direccionManager3Evaluador,");// area5
        query.append("	EVALUADO.title,");// area6
        query.append("	sysdate,");// fecha
        query.append("	CONCAT(EVALUADOR.firstName, ' ', EVALUADOR.lastName)");// nombreEvaluador
        query.append("	)");
        query.append(" FROM ");
        query.append("	CsiDimensionResultsZone CSIRES, ");
        query.append("	CSIEmployee EVALUADO, ");
        query.append("	CSIEmployee EVALUADOR, ");
        query.append("	StoreEmployee S ");
        query.append(" WHERE ");
        query.append("	CSIRES.idManager3Evaluado in( 50092383 , 50092319 ) AND");
        query.append("	EVALUADO.userId = CSIRES.userIdEvaluated");
        query.append("	AND EVALUADOR.userId = CSIRES.userIdEvaluator");
        query.append("	AND S.idStore = CSIRES.idStoreEvaluator");
        query.append("	AND CSIRES.idManager4Evaluado = ?1 ");
        query.append(" GROUP BY CSIRES.idEvaluated, CSIRES.idEvaluator, EVALUADO.firstName,");
        query.append("	EVALUADO.lastName, CSIRES.subDivisionEvaluator, S.descriptionSubdivision, S.descriptionStore,");
        query.append("	EVALUADOR.title, EVALUADO.management, CSIRES.direccionManager3Evaluador,");
        query.append("	EVALUADO.title, sysdate, CONCAT(EVALUADOR.firstName, ' ', EVALUADOR.lastName)");
        query.append(" ORDER BY");
        query.append(
                "	CSIRES.idEvaluated ASC, CSIRES.idEvaluator ASC, CSIRES.subDivisionEvaluator ASC, S.descriptionSubdivision ASC, S.descriptionStore ASC,");
        query.append("	EVALUADO.management ASC, CSIRES.direccionManager3Evaluador ASC, EVALUADO.title ASC");

        return query.toString();
    }

    /**
     * Query que obtiene la suma de las calificaciones por columna
     * @param calificion
     * @return String
     */
    public static String getSumColumnCal(String calificion) {
        return "SUM(CASE WHEN " + calificion + " = 0 THEN 1 ELSE 0 END), " + "SUM(CASE WHEN " + calificion
                + " = 1 THEN 1 ELSE 0 END), " + "SUM(CASE WHEN " + calificion + " = 2 THEN 1 ELSE 0 END), "
                + "SUM(CASE WHEN " + calificion + " = 3 THEN 1 ELSE 0 END), " + "SUM(CASE WHEN " + calificion
                + " = 4 THEN 1 ELSE 0 END), " + "SUM(CASE WHEN " + calificion + " = 5 THEN 1 ELSE 0 END), "
                + "SUM(CASE WHEN " + calificion + " = 6 THEN 1 ELSE 0 END), " + "SUM(CASE WHEN " + calificion
                + " = 7 THEN 1 ELSE 0 END), " + "SUM(CASE WHEN " + calificion + " = 8 THEN 1 ELSE 0 END), "
                + "SUM(CASE WHEN " + calificion + " = 9 THEN 1 ELSE 0 END), " + "SUM(CASE WHEN " + calificion
                + " = 10 THEN 1 ELSE 0 END)";
    }

    /**
     * Query que obtiene el detalle de la pregunta 2
     * @return String
     */
    public static String getQueryDetalleP2() {
        StringBuffer query2 = new StringBuffer();
        query2.append(" SELECT ");
        query2.append("	new json.csi.dto.CsiDBReportDTO(");
        query2.append("	CSIRES.idEvaluated, ");
        query2.append("	UEVA.lastName,");
        query2.append("	UEVA.firstName,");
        query2.append("	UEVA.email,");
        query2.append("	CSIRES.userIdEvaluated,");
        query2.append("	EVALUADO.management,");// direccion
        query2.append("	EVALUADO.department,");// mundo
        query2.append("	UEVA.department,");// seccion(title, area, areaC)
        query2.append("	SUM(CSIRES.countDetractores * 1.0),");
        query2.append("	SUM(CSIRES.countNeutros * 1.0),");
        query2.append("	SUM(CSIRES.countPromotores * 1.0), ");
        query2.append(getSumColumnCal("CSIRES.result"));
        query2.append("	)");
        query2.append(" FROM ");
        query2.append("	CsiDimensionResultsZone CSIRES, ");
        query2.append("	CSIEmployee UEVA, ");
        query2.append("	CSIEmployee EVALUADO ");
        query2.append(" WHERE ");
        query2.append(" CSIRES.idManager3Evaluado in( 50092383 , 50092319 ) AND");
        query2.append("	UEVA.id = CSIRES.idEvaluated AND");
        query2.append("	EVALUADO.userId = CSIRES.idManager5Evaluado");
        query2.append(" GROUP BY ");
        query2.append("	CSIRES.idEvaluated, UEVA.lastName,");
        query2.append("	UEVA.firstName, UEVA.email, CSIRES.userIdEvaluated,");
        query2.append("	EVALUADO.management, EVALUADO.department, UEVA.department");
        query2.append(" ORDER BY");
        query2.append("	CSIRES.idEvaluated ASC, EVALUADO.management ASC, EVALUADO.department ASC, UEVA.department ASC");

        return query2.toString();
    }

    /**
     * Query que obtiene el detalle de la pregunta 3
     * @return String
     */
    public static String getQueryDetalleP3() {
        StringBuffer query3 = new StringBuffer();
        query3.append(" SELECT ");
        query3.append("	new json.csi.dto.CsiDBRepor(");
        query3.append("	RES.order,");
        query3.append("	EVARES.result,");
        query3.append("	EVARES.comments ");
        query3.append("	)");
        query3.append(" FROM ");
        query3.append("	CsiDimensionResultsZone RES, ");
        query3.append("	CSIEvaluationResults EVARES ");
        query3.append(" WHERE ");
        query3.append("	EVARES.idEvaResults = RES.idEvaResults AND");
        query3.append("	RES.idEvaluated = ?1 AND");
        query3.append("	RES.idEvaluator = ?2 ");
        query3.append(" ORDER BY RES.order ASC");

        return query3.toString();
    }

}
