package controllers.csi;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import javax.persistence.Query;

import controllers.Secure;
import json.csi.dto.CsiSurveyDto;
import models.CSIType;
import models.Question;
import models.Survey;
import models.Usuario;
import models.csi.CsiEvaluationCross;
import models.csi.CsiEvaluationCrossLog;
import models.csi.CsiSurvey;
import models.csi.helpers.CsiSurveyHelper;
import play.Play;
import play.db.jpa.GenericModel.JPAQuery;
import play.db.jpa.JPA;
import play.mvc.Controller;
import play.mvc.Http.Response;
import play.mvc.With;

@With(Secure.class)
public class SurveysCSI extends Controller {

    public static void surveys() {
        List<Object> surveys = getAllSurveyLists();

        render(surveys);

    }

    /**
     * Query que obtiene las encuestas filtradas
     * @param filterEncuesta
     * @param dateFrom
     * @param dateTo
     * @return JPAQuery
     */
    private static JPAQuery getQueryFiltered(String filterEncuesta, Date dateFrom, Date dateTo) {
        JPAQuery query;

        if (filterEncuesta == null || filterEncuesta.isEmpty()) {
            if (dateFrom == null || dateTo == null) {
                // No se tienen filtros de nada ni de fechas
                query = Survey.find("surveyType.name = ?1 ", "CSI");
            } else {
                // Solamente se tienen las fechas
                query = Survey.find(
                        "surveyType.name = ?1 " + "AND creationDate >= ?2 " // from
                                + "AND creationDate <= ?3 " // to
                        , "CSI", dateFrom, dateTo);
            }
        } else {
            // se tiene el filtro de survey
            if (dateFrom == null || dateTo == null) {
                // Se tienen filtros de survey pero no de fecha
                query = Survey.find("id", new Integer(filterEncuesta));
            } else {
                // No se tienen filtros de fecha pero si el de survey
                query = Survey.find(
                        "surveyType.name = ?1 "
                                // + "AND surveyDate BETWEEN ?2 AND ?3"
                                + "AND creationDate >= ?2 " // from
                                + "AND creationDate <= ?3 " // to
                        , "CSI", dateFrom, dateTo);
            }
        }
        return query;
    }

    /**
     * Método para obtener todas las encuestas
     * @return List
     */
    public static List<Object> getAllSurveyLists() {
        List<Object> surveys = null;

        JPAQuery query = null;

        query = getQueryFiltered(null, null, null);

        if (query == null) {
            query = Survey.all();
        }
        surveys = query.fetch();
        return surveys;
    }

    /**
     * Método para mostrar todas las encuestas
     */
    public static void surveyList() {
        List<Object> surveys = getAllSurveyLists();

        render(surveys);
    }

    /**
     * Método para obtener las encuestas según el tipo
     * @param type
     */
    public static void surveyListJSON(int type) {
        List<Object> surveys = null;

        if (type <= 0) {
            surveys = getAllSurveyLists();
        } else {
            Query query = JPA.em().createNativeQuery(
                    "Select csis.fn_id_survey, s.fc_name from portalunico.pul_csi_survey csis inner join portalunico.pul_survey s on csis.fn_id_survey = s.fn_id_survey where fn_id_csi_type = "
                            + type);
            surveys = query.getResultList();
        }

        renderJSON(surveys);
    }

    /**
     * Método para llenar la tabla de encuestas
     */
    public static void surveyTable() {
        List<Object> surveys = getAllSurveyLists();

        render(surveys);
    }

    /**
     * Add a new CSI survey
     * 
     * @param nameSurvey
     * @param month
     * @param year
     * @param validityFrom
     * @param validityTo
     * @param goal
     */
    public static void add(String nameSurvey, String month, String year, Date validityFrom, Date validityTo,
            Integer goal, Integer surveyType, String area) {

        String username = session.get("username");
        Usuario user = (Usuario) Usuario.find("username", username.toUpperCase()).fetch().get(0);
        Survey survey = CsiSurveyHelper.createSurvey(year, month, nameSurvey, validityFrom, validityTo, user);

        survey = survey.save();

        CsiSurvey csiSurvey = new CsiSurvey();
        csiSurvey.idSurvey = survey.id;
        csiSurvey.goal = goal != null ? goal : 0;
        csiSurvey.surveyType = surveyType;
        csiSurvey.area = area;
        csiSurvey.impugned = 0;

        csiSurvey.save();

        surveys();
    }

    /**
     * Update CSI Survey with specific id
     * 
     * @param name
     * @param month
     * @param year
     * @param initDate
     * @param endDate
     * @param goal
     * @param idSurvey
     */
    public static void edit(String name, String month, String year, Date initDate, Date endDate, Integer goal,
            Integer idSurvey, Integer surveyType, String area) {

        Survey survey = Survey.findById(idSurvey);

        survey.name = name;
        survey.dateInitial = initDate;
        survey.dateEnd = endDate;
        survey.surveyDate = CsiSurveyHelper.getSurveyDate(year, month);
        survey.modificationDate = new Date();
        survey.modifiedBy = (Usuario) Usuario.find("username", session.get("username").toUpperCase()).fetch().get(0);

        survey.save();

        CsiSurvey csiSurvey = CsiSurvey.findById(idSurvey);
        csiSurvey.goal = goal;
        csiSurvey.area = area;
        csiSurvey.surveyType = surveyType;
        csiSurvey.save();

        surveys();
    }

    /**
     * Download an excel file that contains the Cross Evaluation Layout
     *  
     * @throws IOException
     */
    public static void downloadCrossEvalLayout() throws IOException {

        int nRead;
        byte[] data = new byte[16384];

        InputStream is = Play.classloader.getResourceAsStream("/excel/views/evaluations/cross/cruces.xlsx");

        ByteArrayOutputStream os = new ByteArrayOutputStream();

        while ((nRead = is.read(data, 0, data.length)) != -1) {
            os.write(data, 0, nRead);
        }

        os.flush();

        Response response = Response.current();
        response.setHeader("Content-Type", "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet");
        response.setHeader("Content-disposition", "attachment; filename=evaluacion-cruces.xlsx");
        response.out.write(os.toByteArray());
    }

    /**
     * Load CSI Survey by Id
     * 
     * @param id
     */
    public static void getSurveyById(Integer id) {

        Survey survey = Survey.findById(id);
        CsiSurvey csiSurvey = CsiSurvey.findById(id);

        Calendar period = Calendar.getInstance();
        period.setTime(survey.surveyDate);

        int month = period.get(Calendar.MONDAY) + 1;
        String monthString = month < 10 ? "0" + month : "" + month;

        CsiSurveyDto csiSurveyResponse = new CsiSurveyDto();

        csiSurveyResponse.setName(survey.name);
        csiSurveyResponse.setEndDate(survey.dateEnd);
        csiSurveyResponse.setGoal(csiSurvey != null ? csiSurvey.goal : 0);
        csiSurveyResponse.setInitDate(survey.dateInitial);
        csiSurveyResponse.setMonth(monthString);
        csiSurveyResponse.setYear("" + period.get(Calendar.YEAR));
        csiSurveyResponse.setId(survey.id);
        csiSurveyResponse.setArea(csiSurvey != null ? csiSurvey.area : null);
        csiSurveyResponse.setSurveyType(csiSurvey.surveyType);

        renderJSON(csiSurveyResponse);

    }

    public static void deleteSurvey() {
        render();
    }

    /**
     * Delete a survey with specific id
     * 
     * @param id
     */
    public static void delete(Integer id) {
        Object response = "";

        Long result = CsiEvaluationCross.find("select count(idSurvey) from CsiEvaluationCross where idSurvey = ?1", id)
                .first();
        Long resultLog = CsiEvaluationCrossLog
                .find("select count(idSurvey) from CsiEvaluationCrossLog where idSurvey = ?1", id).first();

        if (result == 0L) {

            if (resultLog > 0l) {
                CsiEvaluationCrossLog.delete("idSurvey = ?1", id);
                Question.delete("survey.id=?1", id);
            }
            CsiSurvey.delete("idSurvey = ?", id);
            Survey.delete("id = ?", id);
            response = "ok";
        } else {
            response = "error";
        }

        renderJSON(response);
    }

    /**
     * Render a modal for CSI survey edition
     */
    public static void editSurvey() {
        List<Integer> years = CsiSurveyHelper.getYearsRange();
        List<CSIType> surveyTypes = getSurveyTypes();

        render(years, surveyTypes);
    }

    public static void uploadResults() {
        render();
    }

    /**
     * Método para agregar una nueva encuesta
     */
    public static void addSurvey() {
        List<Integer> years = CsiSurveyHelper.getYearsRange();
        List<CSIType> surveyTypes = getSurveyTypes();

        render(years, surveyTypes);
    }

    /**
     * Método para obtener todos los tipos de encuestas
     * @return List de CSIType
     */
    private static List<CSIType> getSurveyTypes() {
        return CSIType.findAll();
    }

}
