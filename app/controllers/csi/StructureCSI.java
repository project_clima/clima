package controllers.csi;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.sql.CallableStatement;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.persistence.EntityManager;

import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.springframework.jdbc.support.nativejdbc.C3P0NativeJdbcExtractor;

import controllers.Secure;
import controllers.Structure;
import io.excel.commons.ExcelReader;
import io.excel.commons.exceptions.NotSupportedFileException;
import io.excel.model.Cell;
import io.excel.model.Row;
import java.text.ParseException;
import json.csi.dto.EmployeeSections;
import json.csi.dto.SectionSelect;
import json.csi.dto.SectionsResponse;
import models.EmployeeCSI;
import models.EmployeeSection;
import models.EmployeeSectionLog;
import models.Sections;
import models.csi.CsiEmployeeSections;
import net.sf.jxls.exception.ParsePropertyException;
import oracle.jdbc.OracleCallableStatement;
import oracle.jdbc.OracleConnection;
import oracle.jdbc.OracleTypes;
import play.db.DB;
import play.db.jpa.JPA;
import play.mvc.Controller;
import play.mvc.With;
import querys.csi.StructureQuerySCI;
import utils.Message;
import utils.Pagination;
import utils.PagingResult;
import utils.RenderExcel;

@With(Secure.class)
public class StructureCSI extends Controller {

    public static void structureQuery() {
        render();
    }

    /**
     * Método que obtiene las fechas de la Estructura CSI
     */
    public static void getDatesStructures() {
        StructureQuerySCI queries = new StructureQuerySCI();

        List datesStructure = queries.getDatesStructures();
        renderJSON(datesStructure);
    }

    /**
     * Llena la tabla de Estructura CSI
     * 
     * @param userName
     * @param userNumber
     * @param location
     * @param section
     * @param employeeName
     * @param iDisplayLength
     * @param level1
     * @param level2
     * @param level3
     * @param level4
     * @param level5
     * @param level6
     * @param level7
     * @param level8
     * @param iDisplayStart
     * @param sEcho
     * @param sSortDir_0
     * @param iSortCol_0
     * @param structureDate
     */
    public static void getStructureData(String userName, Integer userNumber, String location, String section,
            String employeeName, int iDisplayLength, String level1, String level2, String level3, String level4,
            String level5, String level6, String level7, String level8, int iDisplayStart, String sEcho,
            String sSortDir_0, int iSortCol_0, String structureDate) {

        Pagination pagination = null;
        String condition = " where ";
        String oneSection = " ";
        int conditions = 0;

        StructureQuerySCI queries = new StructureQuerySCI();
        String order = queries.getOrder(sSortDir_0, iSortCol_0);

        int start = iDisplayStart == 0 ? iDisplayStart : iDisplayStart + 1;
        int end = iDisplayLength + iDisplayStart;

        PagingResult oPR = null;

        if (section != null && !section.equals("-1")) {
            if (conditions > 0) {
                condition += " AND ";
            }
            condition += "FN_USERID IN (SELECT FN_USERID FROM PORTALUNICO.PUL_EMPLOYEE_SECTIONS WHERE FN_ID_SECTION = '"
                    + section + "' )";
            conditions++;
            oneSection = "and  PSE.FN_ID_SECTION = '" + section + "'";
        }

        if (location != null && !location.equals("-1")) {
            if (conditions > 0) {
                condition += " AND ";
            }
            condition += " FC_DESC_LOCATION = '" + location + "'";
            conditions++;
        }

        if (userName != null && userName.trim().length() > 0) {
            if (conditions > 0) {
                condition += " AND ";
            }

            condition += " FN_USERID in ( select FN_ID_USER from " + " PUL_USER " + " where FC_USERNAME like '%"
                    + userName.toUpperCase() + "%' " + ")";
            conditions++;
        }

        if (userNumber != null) {
            if (conditions > 0) {
                condition += " AND ";
            }
            condition += " FN_EMPLOYEE_NUMBER =  " + userNumber + " ";

            conditions++;
        }

        if (employeeName != null && employeeName.trim().length() > 0) {
            if (conditions > 0) {
                condition += " AND ";
            }
            condition += " UPPER(FC_FIRST_NAME) || ' ' || UPPER(FC_LAST_NAME) like '%" + employeeName.toUpperCase()
                    + "%'"
                    + " AND FN_ID_EMPLOYEE_CSI IN (SELECT CASE WHEN FN_EVALUATED_EMP = FN_ID_EMPLOYEE_CSI THEN  FN_EVALUATED_EMP ELSE FN_EVALUATOR_EMP END FROM PORTALUNICO.PUL_CSI_EVALUATION_CROSS "
                    + " WHERE  FN_EVALUATED_EMP = FN_ID_EMPLOYEE_CSI OR FN_EVALUATOR_EMP = FN_ID_EMPLOYEE_CSI)";
            conditions++;
        }

        String searchLevel = "";

        if (level1 != null && !level1.equals("-1")) {
            searchLevel = level1;
        }
        if (level2 != null && !level2.equals("-1")) {
            searchLevel = level2;
        }
        if (level3 != null && !level3.equals("-1")) {
            searchLevel = level3;
        }
        if (level4 != null && !level4.equals("-1")) {
            searchLevel = level4;
        }
        if (level5 != null && !level5.equals("-1")) {
            searchLevel = level5;
        }
        if (level6 != null && !level6.equals("-1")) {
            searchLevel = level6;
        }
        if (level7 != null && !level7.equals("-1")) {
            searchLevel = level7;
        }
        if (level8 != null && !level8.equals("-1")) {
            searchLevel = level8;
        }

        if (!searchLevel.equals("")) {
            if (conditions > 0) {
                condition += " AND ";
            }
            condition += " FN_USERID = '" + searchLevel + "'";
            conditions++;
        }

        if (conditions <= 0 || condition.equals("")) {
            condition = " ";
        }

        oPR = queries.getStructure(condition, order, start, end, structureDate, oneSection, iDisplayLength);
        pagination = new Pagination(sEcho, oPR.totalRecords, oPR.totalRecords, oPR.data);
        renderJSON(pagination);
    }

    /**
     * Obtiene las Ubicaciones de la estructura CSI según la fecha
     * @param date
     */
    public static void getLocation(String date) {
        StructureQuerySCI queries = new StructureQuerySCI();

        List locations = queries.getLocation(date);
        renderJSON(locations);
    }

    /**
     * Obtiene las secciones de la estructura CSI
     */
    public static void getSection() {
        StructureQuerySCI queries = new StructureQuerySCI();

        List sections = queries.getSection();
        renderJSON(sections);
    }

    /**
     * Obtiene las del empleado seleccionado
     * @param numEmployee
     */
    public static void getSectionsSelect(Integer numEmployee) {
        List<Object[]> resultSections = new ArrayList<>();

        List<Sections> result = Sections.findAll();
        Object idUser = EmployeeCSI.find("select userId from EmployeeCSI where employeeNumber = ?", numEmployee)
                .first();
        List<SectionSelect> sectionsSelected = EmployeeSection
                .find("select new json.csi.dto.SectionSelect(idSection) from EmployeeSection where userId = ?",
                        idUser)
                .fetch();

        SectionsResponse response = new SectionsResponse();
        response.setSections(result);
        response.setSectionSelected(sectionsSelected);
        renderJSON(response);
    }

    /**
     * Obtiene los departamentos del nivel 1
     * @param level
     */
    public static void getLevels1(int level) {
        StructureQuerySCI queries = new StructureQuerySCI();

        List levels = queries.getDepartmentsByLevel1(level);
        renderJSON(levels);
    }

    /**
     * Obtiene los departamentos según el nivel
     * @param department
     * @param date
     */
    public static void getLevels(int department, String date) {
        StructureQuerySCI queries = new StructureQuerySCI();

        List levels = queries.getDepartmentsByLevel(department, date, false);
        renderJSON(levels);
    }

    /**
     * Recupera la información del empleaod seleccionado
     * @param userId Número del empleado
     */
    public static void getInfoModal(int userId) {
        StructureQuerySCI queries = new StructureQuerySCI();

        List info = queries.getInfoModal(userId);
        renderJSON(info);
    }

    /**
     * Actualiza Secciones, Mail y ubicaciones de un empleado en específico
     * @param email
     * @param idEmployee
     * @param ubication
     * @param section
     * @param structureDate
     * @throws ParseException
     */
    public static void setPlanEmail(String email, int idEmployee, String ubication,
            Integer[] section, String structureDate) throws ParseException {
        SimpleDateFormat sdf = new SimpleDateFormat("MM/YYYY");
//        if (section.length == 1 && section[0] == null) {
//        } else {
            Integer idUser = EmployeeCSI
                    .find("select userId from EmployeeCSI where employeeNumber = ?1 "
                            + "and TO_CHAR(structureDate, 'MM/YYYY') = ?2 ", 
                            idEmployee, structureDate).first();
//            Date dateStructure = EmployeeCSI
//                    .find("select structureDate from EmployeeCSI where employeeNumber = ?", idEmployee).first();
            EmployeeSection.delete("userId = ?", idUser);
        if (section.length > 1 && section[0] != null) {
            for (int i = 0; i < section.length; i++) {
            	EmployeeSection employeeSections = new EmployeeSection();
                employeeSections.idSection = section[i];
                employeeSections.userId = idUser;
                employeeSections.structureDate = sdf.parse(structureDate);
                employeeSections.save();
            }
        }
        renderJSON(StructureQuerySCI.setPlanEmail(email, idEmployee, session.get("username"), ubication));
    }

    /**
     * Método para validar que el Manager esté correcto
     * @param managerId
     * @param managerName
     * @param employeeNumber
     */
    public static void validateManager(Integer managerId, String managerName, int employeeNumber) {
        StructureQuerySCI queries = new StructureQuerySCI();
        if (managerId == null) {
            managerId = 0;
        }
        EmployeeCSI employee = EmployeeCSI.find("employeeNumber = ?1", employeeNumber).first();
        List managers = queries.getManagers(managerId, managerName.toUpperCase(), employee.userId);

        renderJSON(managers);
    }

    /**
     * Método para aplicar el cambio de manager
     * @param userId
     * @param newManagerId
     */
    public static void changeManager(int userId, int newManagerId) {
        int update = 0;

        EmployeeCSI manager = EmployeeCSI.find("employeeNumber = ?1", newManagerId).first();
        EmployeeCSI employee = EmployeeCSI.find("employeeNumber = ?1", userId).first();

        if (manager.manager.equals(employee.userId)) {
            renderJSON(new Message("El nuevo jefe directo no puede ser asignado (error de redundancia)", null, true));
        }

        if (userId == newManagerId) {
            renderJSON(new Message("El jefe directo no puede ser el mismo empleado", null, true));
        }

        update = StructureQuerySCI.setManager(userId, newManagerId, session.get("username"));

        if (update == 0) {
            renderJSON(new Message("El jefe directo no pudo ser asignado", null, true));
        }

        renderJSON(new Message(null, null, false));
    }

    /**
     * Obtiene las funciones
     */
    public static void getFunction() {
        StructureQuerySCI queries = new StructureQuerySCI();

        List functions = queries.getFunctions();
        renderJSON(functions);
    }

    /**
     * Método para aplicar el cambio de función
     * @param userId
     * @param newFunction
     */
    public static void changeFunction(int userId, String newFunction) {
        StructureQuerySCI queries = new StructureQuerySCI();
        int update = 0;
        update = queries.setFunction(userId, newFunction, session.get("username"));
        renderJSON(update);
    }

    /**
     * Descarga en excel los datos de la Estructura CSI
     * 
     * @param userName
     * @param userNumber
     * @param location
     * @param section
     * @param employeeName
     * @param iDisplayLength
     * @param level1
     * @param level2
     * @param level3
     * @param level4
     * @param level5
     * @param level6
     * @param level7
     * @param level8
     * @param iDisplayStart
     * @param sEcho
     * @param sSortDir_0
     * @param iSortCol_0
     * @param structureDate
     * @throws ParsePropertyException
     * @throws InvalidFormatException
     * @throws IOException
     */
    public static void export(String userName, Integer userNumber, String location, String section, String employeeName,
            int iDisplayLength, String level1, String level2, String level3, String level4, String level5,
            String level6, String level7, String level8, int iDisplayStart, String sEcho, String sSortDir_0,
            int iSortCol_0, String structureDate) throws ParsePropertyException, InvalidFormatException, IOException {

        String condition = " where ";
        String oneSection = " ";
        int conditions = 0;

        StructureQuerySCI queries = new StructureQuerySCI();

        List structureSCI = null;

        if (section != null && !section.equals("-1")) {
            if (conditions > 0) {
                condition += " AND ";
            }
            condition += "FN_USERID IN (SELECT FN_USERID FROM PORTALUNICO.PUL_EMPLOYEE_SECTIONS WHERE FN_ID_SECTION = '"
                    + section + "' )";
            conditions++;
            oneSection = "and  PSE.FN_ID_SECTION = '" + section + "'";
        }

        if (location != null && !location.equals("-1")) {
            if (conditions > 0) {
                condition += " AND ";
            }
            condition += " FC_DESC_LOCATION = '" + location + "'";
            conditions++;
        }

        if (userName != null && userName.trim().length() > 0) {
            if (conditions > 0) {
                condition += " AND ";
            }

            condition += " FN_USERID in ( select FN_ID_USER from " + " PUL_USER " + " where FC_USERNAME like '%"
                    + userName.toUpperCase() + "%' " + ")";
            conditions++;
        }

        if (userNumber != null) {
            if (conditions > 0) {
                condition += " AND ";
            }
            condition += " FN_EMPLOYEE_NUMBER =  " + userNumber + " AND "
                    + " FN_ID_EMPLOYEE_CSI IN (SELECT CASE WHEN FN_EVALUATED_EMP = FN_ID_EMPLOYEE_CSI THEN  FN_EVALUATED_EMP ELSE FN_EVALUATOR_EMP END FROM PORTALUNICO.PUL_CSI_EVALUATION_CROSS "
                    + " WHERE  FN_EVALUATED_EMP = FN_ID_EMPLOYEE_CSI OR FN_EVALUATOR_EMP = FN_ID_EMPLOYEE_CSI)";
            conditions++;
        }

        if (employeeName != null && employeeName.trim().length() > 0) {
            if (conditions > 0) {
                condition += " AND ";
            }
            condition += " UPPER(FC_FIRST_NAME) || ' ' || UPPER(FC_LAST_NAME) like '%" + employeeName.toUpperCase()
                    + "%'"
                    + "AND FN_ID_EMPLOYEE_CSI IN (SELECT CASE WHEN FN_EVALUATED_EMP = FN_ID_EMPLOYEE_CSI THEN  FN_EVALUATED_EMP ELSE FN_EVALUATOR_EMP END FROM PORTALUNICO.PUL_CSI_EVALUATION_CROSS "
                    + " WHERE  FN_EVALUATED_EMP = FN_ID_EMPLOYEE_CSI OR FN_EVALUATOR_EMP = FN_ID_EMPLOYEE_CSI)";
            conditions++;
        }

        String searchLevel = "";

        if (level1 != null && !level1.equals("-1")) {
            searchLevel = level1;
        }
        if (level2 != null && !level2.equals("-1")) {
            searchLevel = level2;
        }
        if (level3 != null && !level3.equals("-1")) {
            searchLevel = level3;
        }
        if (level4 != null && !level4.equals("-1")) {
            searchLevel = level4;
        }
        if (level5 != null && !level5.equals("-1")) {
            searchLevel = level5;
        }
        if (level6 != null && !level6.equals("-1")) {
            searchLevel = level6;
        }
        if (level7 != null && !level7.equals("-1")) {
            searchLevel = level7;
        }
        if (level8 != null && !level8.equals("-1")) {
            searchLevel = level8;
        }

        if (!searchLevel.equals("")) {
            if (conditions > 0) {
                condition += " AND ";
            }
            condition += " FN_USERID = '" + searchLevel + "'";
            conditions++;
        }

        if (conditions <= 0 || condition.equals("")) {
            condition = " ";
        }

        SimpleDateFormat format = new SimpleDateFormat("MM/yyyy");
        Date date = new Date();
        String formatDate = format.format(date);

        structureSCI = queries.export(condition, structureDate, oneSection);

        renderArgs.put("structureSCI", structureSCI);
        renderArgs.put(RenderExcel.RA_FILENAME, "Estructura SCI " + formatDate + ".xlsx");
        RenderExcel renderExcel = new RenderExcel("app/excel/views/StructureCSI/structureCSI.xlsx");
        renderExcel.render();
    }

    /**
     * Carga las secciones del empleado a partir de un archivo en excel
     * @param file
     * @param userName
     * @param structureDate
     * @return boolean
     */
    public static boolean loadSectionsEmployee(File file, String userName, Date structureDate) {
        boolean load = true;
        Map<Integer, String> catSections = new HashMap<Integer, String>();
        Map<String, Integer> employeeSections = new HashMap<String, Integer>();
        try {
            ExcelReader excelReader = new ExcelReader();
            List<Row> rows = excelReader.readExcelFile(file);
            int index = 0;
            for (Row row : rows) {
                if (index > 0) {
                    EmployeeSections cargaRow = new EmployeeSections();
                    List<Cell> cells = row.getCells();
                    if (cells != null && !cells.isEmpty()) {
                        cargaRow.setUserId("" + cells.get(0).getValue());
                        if (cells.size() > 1) {
                            cargaRow.setSections("" + cells.get(1).getValue());

                            if (cargaRow.getSections() != null && cargaRow.getSections().trim().length() > 0) {

                                String[] sections = cargaRow.getSections().split("/");
                                for (int i = 0; i < sections.length; i++) {
                                    Integer idSection = Integer
                                            .parseInt(sections[i].trim().substring(0, sections[i].indexOf(" ")));
                                    String descSection = sections[i].trim().substring(sections[i].indexOf(" ") + 1);
                                    catSections.put(idSection, descSection);

                                    String employeeSectionId = cargaRow.getUserId() + "-" + idSection;
                                    employeeSections.put(employeeSectionId, idSection);
                                }
                            }
                        }
                    }
                }
                index++;
            }

            EntityManager em = JPA.em();
            em.getTransaction().commit();
            Sections section = null;
            int i = 0;
            // insertar sections
            for (Map.Entry<Integer, String> entry : catSections.entrySet()) {
                em.getTransaction().begin();
                section = Sections.findById(entry.getKey());
                if (section == null) {
                    section = new Sections();
                    section.id = entry.getKey();
                    section.createdBy = userName;
                    section.createdDate = Calendar.getInstance().getTime();
                } else {
                    section.modifiedBy = userName;
                    section.modifiedDate = Calendar.getInstance().getTime();
                }
                section.nombreSeccion = entry.getValue();

                section.save();
                em.persist(section);

                if (i <= 1000000) {
                    if ((i % 10000) == 0) {
                        em.getTransaction().commit();
                        em.clear();
                        em.getTransaction().begin();
                    }
                } else {
                    i++;
                }

                em.getTransaction().commit();

            }

            Integer userId = null;
            i = 0;

            SimpleDateFormat sdf = new SimpleDateFormat("MM/YYYY");
            String sd = sdf.format(structureDate);

            // insertar employeesections
            for (Map.Entry<String, Integer> entry : employeeSections.entrySet()) {

                em.getTransaction().begin();
                List<EmployeeCSI> employeeCSI = null;
                if (userId == null
                        || !userId.equals(Integer.parseInt(entry.getKey().substring(0, entry.getKey().indexOf("-"))))) {
                    userId = Integer.parseInt(entry.getKey().substring(0, entry.getKey().indexOf("-")));
                    employeeCSI = models.EmployeeCSI
                            .find("userId = ? and TO_CHAR(structureDate,'MM/YYYY' )= ? ", userId, sd).fetch();
                }

                if (employeeCSI != null && !employeeCSI.isEmpty()) {

                    List<EmployeeSection> employeeSectionList = EmployeeSection
                            .find("userId = ? and idSection = ? and TO_CHAR(structureDate,'MM/YYYY') = ?", userId,
                                    entry.getValue(), sd)
                            .fetch();
                    EmployeeSection employeeSection = new EmployeeSection();

                    employeeSection.userId = userId;
                    employeeSection.idSection = entry.getValue();
                    employeeSection.structureDate = structureDate;
                    if (employeeSectionList == null || employeeSectionList.isEmpty()) {
                        em.persist(employeeSection);
                    }

                } else {
                    EmployeeSectionLog employeeSectionLog = new EmployeeSectionLog();
                    employeeSectionLog.userId = userId;
                    employeeSectionLog.idSection = entry.getValue();
                    employeeSectionLog.createDate = Calendar.getInstance().getTime();
                    employeeSectionLog.typeError = 2;
                    employeeSectionLog.createFor = userName;
                    employeeSectionLog.structureDate = structureDate;
                    em.persist(employeeSectionLog);
                }

                if (i <= 1000000) {
                    if ((i % 10000) == 0) {
                        em.getTransaction().commit();
                        em.clear();
                        em.getTransaction().begin();
                    }
                } else {
                    i++;
                }
                em.getTransaction().commit();
            }

        } catch (FileNotFoundException e) {
            load = false;
            e.printStackTrace();
        } catch (IOException e) {

            load = false;
            e.printStackTrace();
        } catch (NotSupportedFileException e) {

            e.printStackTrace();
            load = false;
        }
        return load;

    }

    /**
     * Obtiene la fecha de la estructura del usuario
     * @param user
     * @return Date
     */
    public static java.sql.Date loadSnapStructure(String user) {

        java.sql.Date structureDate = null;
        OracleConnection connection = null;
        CallableStatement statement = null;

        try {
            connection = getOracleConnection();
            statement = connection.prepareCall("{ ? = call PORTALUNICO.FN_INS_EMPLOYEE_CSI(?) }");

            statement.setString(2, user);
            statement.registerOutParameter(1, OracleTypes.DATE);
            statement.execute();

            structureDate = ((OracleCallableStatement) statement).getDate(1);

        } catch (SQLException e) {
            Logger.getLogger(StructureCSI.class.getName()).log(Level.SEVERE, null, e);
        } finally {
            try {
                if (statement != null && !statement.isClosed()) {
                    statement.close();
                }
            } catch (SQLException e) {
                Logger.getLogger(StructureCSI.class.getName()).log(Level.SEVERE, null, e);
            }
        }

        return structureDate;

    }

    /**
     * Realiza la conexión a la vase de datos
     * @return OracleConnection
     * @throws SQLException
     */
    public static OracleConnection getOracleConnection() throws SQLException {
        C3P0NativeJdbcExtractor cp30NativeJdbcExtractor = new C3P0NativeJdbcExtractor();
        return (OracleConnection) cp30NativeJdbcExtractor.getNativeConnection(DB.getConnection());
    }

    /**
     * Obtiene los departamentos según la ubicación
     * @param location
     * @param date
     */
    public static void getDepartments(String location, String date) {
        StructureQuerySCI queries = new StructureQuerySCI();

        List departments = queries.getDepartments(location, date);
        renderJSON(departments);
    }
}
