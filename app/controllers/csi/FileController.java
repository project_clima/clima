package controllers.csi;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import io.excel.commons.ExcelReader;
import io.excel.commons.exceptions.NotSupportedFileException;
import io.excel.model.Cell;
import io.excel.model.Row;
import jobs.csi.CrossEvaluationAsyncCSIJob;
import jobs.csi.EvaluationAsyncCSIJob;
import json.csi.dto.SurveyCargaResponse;
import json.csi.dto.SurveyCargaRow;
import models.CSIEvaluationResults;
import models.EmployeeCSI;
import models.Question;
import models.QuestionType;
import models.Survey;
import models.Usuario;
import models.csi.CsiEvaluationCross;
import models.csi.CsiEvaluationCrossLog;
import models.csi.helpers.CsiEvaluationHelper;
import models.enume.ErrorTypeEnum;
import play.mvc.Controller;

public class FileController extends Controller {

    private static ExcelReader excelReader = new ExcelReader();
    public final static Integer NUMERIC_QUESTION = 6;

    /**
     * Método para cargar un archivo de cruces o resultados
     * 
     * @param csiFile archivo que se va a cargar
     * @param typeload tipo de archivo (cruce o resultados)
     * @param idUploadSurvey id de la encuesta
     */
    public static void uploadFile(File csiFile, String typeload, Integer idUploadSurvey) {
        SurveyCargaResponse response = new SurveyCargaResponse();
        boolean isValid = true;

        try {
            List<Row> rows = excelReader.readExcelFile(csiFile);

            if (rows.isEmpty()) {
                response.setIdResponse(500);
                response.setErrorMessage("El archivo no contiene información");
                isValid = false;
            }
            if (typeload.equals("1")) {
                if (!isCrossFileStructureValid(rows)) {
                    response.setIdResponse(500);
                    response.setErrorMessage("El archivo no contiene la estructura correcta, debe contener 2 columnas");
                    isValid = false;
                }
            }
            if (typeload.equals("1")) {
                List<CsiEvaluationCross> cross = CsiEvaluationCross
                        .find("Select id from CsiEvaluationCross where idSurvey = ?", idUploadSurvey).fetch();
                if (cross.size() > 0) {
                    response.setIdResponse(500);
                    response.setErrorMessage("Esta encuesta ya tiene cargado un cruce de evaluaciones.");
                    isValid = false;
                }
            } else {
                List<CSIEvaluationResults> results = CSIEvaluationResults
                        .find("Select idEvaResults from CSIEvaluationResults where idSurvey = ?", idUploadSurvey)
                        .fetch();
                if (results.size() > 0) {
                    response.setIdResponse(500);
                    response.setErrorMessage("Esta encuesta ya tiene resultados cargados.");
                    isValid = false;
                }
            }

            if (typeload.equals("2")) {
                if (!isResultsFileStructureValid(rows)) {
                    response.setIdResponse(500);
                    response.setErrorMessage(
                            "El archivo no contiene la estructura correcta, debe contener al menos 14 columnas");
                    isValid = false;
                }
            }

            if (isValid && typeload.equals("1")) {
                Long size = CsiEvaluationCross
                        .find("Select count(id) from CsiEvaluationCross where idSurvey = ?", idUploadSurvey).first();
                if (size != null && size > 0) {
                    response.setIdResponse(500);
                    response.setErrorMessage("Esta encuesta ya tiene cargado un cruce de evaluaciones.");
                    isValid = false;
                }
            }

            if (isValid && typeload.equals("2")) {
                Long size = CSIEvaluationResults
                        .find("Select count(idEvaResults) from CSIEvaluationResults where idSurvey = ?", idUploadSurvey)
                        .first();
                Long sizeCross = CsiEvaluationCross
                        .find("Select count(id) from CsiEvaluationCross where idSurvey = ?", idUploadSurvey).first();
                if (size != null && size > 0) {
                    response.setIdResponse(500);
                    response.setErrorMessage("Esta encuesta ya tiene resultados cargados.");
                    isValid = false;
                }
                
                if(sizeCross <= 0){
                	response.setIdResponse(500);
                    response.setErrorMessage("Se deben cargar cruces antes de cargar resultados.");
                    isValid = false;
                }
            }

            if (rows.size() > 1 && isValid) {
                if (typeload.equals("1")) {
                    rows = saveCrossEvaluationEmployees(rows, idUploadSurvey);
                    if (rows.isEmpty()) {
                        response.setIdResponse(500);
                        response.setErrorMessage("No hay empleados a evaluar para esta encuesta.");
                        isValid = false;
                    }
                } else {
                    rows = saveResultEvaluations(rows, idUploadSurvey);
                }

                if (isValid) {
                    response.setIdResponse(200);
                    if (typeload.equals("1")) {
                        for (Row row : rows) {
                            if (row.getIndex() != 1) { // If row is different at
                                                       // header
                                SurveyCargaRow cargaRow = new SurveyCargaRow();
                                List<Cell> cells = row.getCells();
                                if (cells != null && !cells.isEmpty()) {
                                    cargaRow.setKey("" + cells.get(0).getValue());
                                    if (cells.size() > 1) {
                                        cargaRow.setValue("" + cells.get(1).getValue());
                                    }
                                    response.getCargaRows().add(cargaRow);
                                }
                            }
                        }
                    }
                }
            }

        } catch (FileNotFoundException e) {
            response.setIdResponse(500);
            e.printStackTrace();
        } catch (IOException e) {
            response.setIdResponse(500);
        } catch (NotSupportedFileException e) {
            response.setIdResponse(500);
        }

        response.setLoadtype(typeload);
        renderJSON(response);

    }

    /**
     * Verifica que la estrucutura del archivo de cruce sea correcta
     * 
     * @param rows lista de filas
     * 
     * @return boolean
     */
    private static boolean isCrossFileStructureValid(List<Row> rows) {
        for (Row row : rows) {
            List<Cell> cells = row.getCells();
            int size = cells.size();
            
            for (Cell cell : cells) {
                String celda = cell.getValue().toString();

                if (celda.isEmpty()) {
                    size--;
                    //cells.remove(cell);

                }
            }
            
            if (size < 2 || size > 2)
                return false;
        }
        return true;
    }

    /**
     * Verfica que la estructura del archivo de resultados sea válida
     * 
     * @param rows lista de filas
     * 
     * @return boolean
     */
    private static boolean isResultsFileStructureValid(List<Row> rows) {
        for (Row row : rows) {
            if (row.getCells().size() < 14)
                return false;
        }
        return true;
    }

    /**
     * Guarda el crcue de evaluaciones de empleados
     * 
     * @param rows 
     * @param idSurvey
     * 
     * @return List de filas filtradas que se guardaron
     */
    private static List<Row> saveCrossEvaluationEmployees(List<Row> rows, Integer idSurvey) {
        Set<Integer> employees = new HashSet<Integer>();
        Map<Integer, Integer> mapEmployees = new HashMap<Integer, Integer>();
        List<Row> rowsFiltered = new ArrayList<Row>();
        List<Row> partRows = new ArrayList<>();
        List<CsiEvaluationCross> evaluations2Save = new ArrayList<>();
        List<CsiEvaluationCrossLog> logs2Save = new ArrayList<>();

        int rowsSize = rows.size();

        int iterations = rowsSize / 1000;

        int modRows = rowsSize % 1000;

        if (modRows > 0)
            iterations++;

        int position = 0;

        Survey survey = Survey.findById(idSurvey);
        Date surveyDate = survey.surveyDate;

        Calendar surveyCalendar = Calendar.getInstance();
        surveyCalendar.setTime(surveyDate);

        Date initDate = getDate(surveyDate, 1, 0, 0, 0, 0);
        Date endDate = getDate(surveyDate, 1, surveyCalendar.getActualMaximum(Calendar.DAY_OF_MONTH), 59, 59, 999);

        Date structureDate = EmployeeCSI
                .find("select MAX(e.structureDate) from EmployeeCSI e where e.structureDate between ?1 and ?2",
                        initDate, endDate)
                .first();

        for (int i = 0; i < iterations; i++) {
            employees = new HashSet<>();
            partRows = new ArrayList<>();
            for (int j = 0; j < 1000; j++) {
                if (position < rowsSize) {
                    Row row = rows.get(position++);
                    
                    if(row.getCells().size()==2){
                    	partRows.add(row);
                        if (row.getIndex() != 1) {
                            if (isNumber("" + row.getCells().get(0).getValue())) {
                                employees.add(Integer.parseInt("" + row.getCells().get(0).getValue())); // obtains
                                                                                                        // valuador
                            } else {
                                logs2Save.add(CsiEvaluationHelper.createCsiEvaluationCrossLog(idSurvey,
                                        "" + row.getCells().get(0).getValue(), ErrorTypeEnum.EMPLOYEE_NOT_FORMAT.getId()));
                            }

                            if (isNumber("" + row.getCells().get(1).getValue())) {
                                employees.add(Integer.parseInt("" + row.getCells().get(1).getValue())); // obtains
                                                                                                        // evaluado
                            } else {
                                logs2Save.add(CsiEvaluationHelper.createCsiEvaluationCrossLog(idSurvey,
                                        "" + row.getCells().get(1).getValue(), ErrorTypeEnum.EMPLOYEE_NOT_FORMAT.getId()));
                            }

                        }
                    }
                }
            }

            List employeesBd = EmployeeCSI
                    .find("select distinct e.employeeNumber, e.id from EmployeeCSI e where e.structureDate =?2 and e.employeeNumber in ?1",
                            new ArrayList<>(employees), structureDate)
                    .fetch();

            mapEmployees = new HashMap<>();
            if (!employeesBd.isEmpty()) {
                for (Object employee : employeesBd) {
                    Object[] data = (Object[]) employee;
                    mapEmployees.put(Integer.parseInt("" + data[0]), Integer.parseInt("" + data[1]));
                }

                for (Row row : partRows) {

                    if (row.getIndex() != 1) {
                        Integer evaluado = row.getCells().get(0).getIndex() == 0
                                ? Integer.valueOf("" + row.getCells().get(0).getValue())
                                : Integer.valueOf("" + row.getCells().get(1).getValue());
                        Integer evaluador = row.getCells().get(0).getIndex() == 1
                                ? Integer.valueOf("" + row.getCells().get(0).getValue())
                                : Integer.valueOf("" + row.getCells().get(1).getValue());
                        if (mapEmployees.containsKey(evaluador) && mapEmployees.containsKey(evaluado)) {
                            CsiEvaluationCross csiEvaluationCross = CsiEvaluationHelper.createCsiEvaluationCross(
                                    mapEmployees.get(evaluador), mapEmployees.get(evaluado), idSurvey,
                                    session.get("username"));
                            evaluations2Save.add(csiEvaluationCross);

                            rowsFiltered.add(row);

                        } else {
                            CsiEvaluationCrossLog csiEvaluationCrossLog = null;
                            if (mapEmployees.get(evaluador) == null) {
                                csiEvaluationCrossLog = CsiEvaluationHelper.createCsiEvaluationCrossLog(idSurvey,
                                        "" + evaluador, ErrorTypeEnum.EMPLOYEE_EVALUATOR_CROSS_NOT_EXISTS.getId());
                            } else {
                                csiEvaluationCrossLog = CsiEvaluationHelper.createCsiEvaluationCrossLog(idSurvey,
                                        "" + evaluado, ErrorTypeEnum.EMPLOYEE_EVALUATED_CROSS_NOT_EXISTS.getId());
                            }

                            if (csiEvaluationCrossLog != null)
                                logs2Save.add(csiEvaluationCrossLog);

                        }
                    }

                }
            }
        }
        String username = session.get("username");
        String emailUser = Usuario.find("select email from Usuario where username = UPPER(?1)", username).first();

        new CrossEvaluationAsyncCSIJob(logs2Save, evaluations2Save, idSurvey, emailUser).now();

        return rowsFiltered;

    }

    private static Date getDate(Date date, int day, int hour, int minute, int second, int millisecond) {
        Calendar calendar = Calendar.getInstance();

        calendar.setTime(date);
        calendar.set(Calendar.DAY_OF_MONTH, day);
        calendar.set(Calendar.HOUR_OF_DAY, hour);
        calendar.set(Calendar.MINUTE, minute);
        calendar.set(Calendar.SECOND, second);
        calendar.set(Calendar.MILLISECOND, millisecond);

        return calendar.getTime();
    }

    /**
     * Save the results obtained from
     * 
     * @param rows
     * @param idSurvey
     * @return List de filas
     */
    private static List<Row> saveResultEvaluations(List<Row> rows, Integer idSurvey) {

        Set<Integer> employeesEvaluated = new HashSet<Integer>();
        Set<Integer> employeesEvaluators = new HashSet<Integer>();
        Map<Integer, List<Cell>> rowsOreded = new HashMap<>();
        Map<Integer, Integer> mapEmployeesEvaluated = new HashMap<Integer, Integer>();
        Map<Integer, Integer> mapEmployeesEvaluators = new HashMap<Integer, Integer>();
        Map<Integer, String> headers = new HashMap<>();
        List<CSIEvaluationResults> results2save = new ArrayList<>();
        List<CsiEvaluationCrossLog> logs2Save = new ArrayList<>();

        List<Row> rowsFiltered = new ArrayList<Row>();

        Survey survey = Survey.findById(idSurvey);
        Date surveyDate = survey.surveyDate;

        Calendar surveyCalendar = Calendar.getInstance();
        surveyCalendar.setTime(surveyDate);

        Date initDate = getDate(surveyDate, 1, 0, 0, 0, 0);
        Date endDate = getDate(surveyDate, 1, surveyCalendar.getActualMaximum(Calendar.DAY_OF_MONTH), 59, 59, 999);

        Date structureDate = EmployeeCSI
                .find("select MAX(e.structureDate) from EmployeeCSI e where e.structureDate between ?1 and ?2",
                        initDate, endDate)
                .first();

       
        for (Row row : rows) {        	
            if (row.getIndex() != 1) {
                List<Cell> cells = row.getCells();
                Collections.sort(cells, new Comparator<Cell>() {
                    public int compare(Cell o1, Cell o2) {
                        return o1.getIndex().compareTo(o2.getIndex());
                    }
                });

                rowsOreded.put(row.getIndex(), cells);
                employeesEvaluated.add(Integer.parseInt("" + cells.get(0).getValue()));
                employeesEvaluators.add(Integer.parseInt("" + cells.get(4).getValue()));
            } else {
                for (Cell cell : row.getCells()) {
                    headers.put(cell.getIndex(), "" + cell.getValue());
                }

            }
        }

        Map<Integer, Integer> mapQuestions = createQuestions(headers, idSurvey);

        int rowsSize = employeesEvaluated.size();
        int iterations = rowsSize / 1000;
        int modRows = rowsSize % 1000;
        List employeesEvaluatedBd = new ArrayList<>();
        List employeesEvaluatorBd = new ArrayList<>();

        if (modRows > 0)
            iterations++;

        int position = 0;
        Integer[] evaluatedArray = employeesEvaluated.toArray(new Integer[] {});
        Integer[] evaluatorsArray = employeesEvaluators.toArray(new Integer[] {});

        for (int i = 0; i < iterations; i++) {
            List<Integer> partList = new ArrayList<>();
            for (int j = 0; j < 1000; j++) {
                if (position < rowsSize) {
                    partList.add(evaluatedArray[position++]);
                }
            }

            employeesEvaluatedBd.addAll(EmployeeCSI
                    .find("select distinct e.employeeNumber, e.id from EmployeeCSI e, CsiEvaluationCross ec where e.employeeNumber in ?1 and e.id = ec.idEvaluatedEmployee and e.structureDate = ?2",
                            partList, structureDate)
                    .fetch());
        }

        position = 0;
        rowsSize = employeesEvaluators.size();
        iterations = rowsSize / 1000;
        modRows = rowsSize % 1000;
        if (modRows > 0)
            iterations++;

        for (int i = 0; i < iterations; i++) {
            List<Integer> partList = new ArrayList<>();
            for (int j = 0; j < 1000; j++) {
                if (position < rowsSize) {
                    partList.add(evaluatorsArray[position++]);
                }
            }
            employeesEvaluatorBd.addAll(EmployeeCSI
                    .find("select distinct e.employeeNumber, e.id from EmployeeCSI e, CsiEvaluationCross ec where e.employeeNumber in ?1 and e.id = ec.idEvaluatorEmployee and e.structureDate = ?2",
                            partList, structureDate)
                    .fetch());
        }

        for (Object employee : employeesEvaluatedBd) {
            Object[] data = (Object[]) employee;
            mapEmployeesEvaluated.put(Integer.parseInt("" + data[0]), Integer.parseInt("" + data[1]));
        }

        for (Object employee : employeesEvaluatorBd) {
            Object[] data = (Object[]) employee;
            mapEmployeesEvaluators.put(Integer.parseInt("" + data[0]), Integer.parseInt("" + data[1]));
        }

        for (Row row : rows) {
            if (row.getIndex() != 1) {
                List<Cell> cells = rowsOreded.get(row.getIndex());
                List<CSIEvaluationResults> csiEvaluationResults = createEvaluationResultRow(cells,
                        mapEmployeesEvaluated, idSurvey, headers, mapEmployeesEvaluators, mapQuestions);
                if (csiEvaluationResults != null && !csiEvaluationResults.isEmpty()) {
                    for (CSIEvaluationResults csiEvaluationResult : csiEvaluationResults) {
                        results2save.add(csiEvaluationResult);
                        rowsFiltered.add(row);
                    }
                } else {
                    Integer employee = Integer.parseInt("" + cells.get(0).getValue());
                    Integer evaluator = Integer.parseInt("" + cells.get(4).getValue());
                    CsiEvaluationCrossLog csiEvaluationCrossLog = null;
                    if (!mapEmployeesEvaluated.containsKey(employee)) {
                        csiEvaluationCrossLog = CsiEvaluationHelper.createCsiEvaluationCrossLog(idSurvey,
                                "" + cells.get(0).getValue(),
                                ErrorTypeEnum.EMPLOYEE_EVALUATED_RESULTS_NOT_EXISTS.getId());
                    } else if (!mapEmployeesEvaluators.containsKey(evaluator)) {
                        csiEvaluationCrossLog = CsiEvaluationHelper.createCsiEvaluationCrossLog(idSurvey,
                                "" + cells.get(0).getValue(),
                                ErrorTypeEnum.EMPLOYEE_EVALUATOR_RESULTS_NOT_EXISTS.getId());
                    }

                    if (csiEvaluationCrossLog != null) {
                        logs2Save.add(csiEvaluationCrossLog);
                    }
                }
            }
        }

        String username = session.get("username");
        String emailUser = Usuario.find("select email from Usuario where username = ?1", username).first();

        EvaluationAsyncCSIJob saveResultsJob = new EvaluationAsyncCSIJob();
        saveResultsJob.setResults(results2save);
        saveResultsJob.setLogs(logs2Save);
        saveResultsJob.setIdSurvey(idSurvey);
        saveResultsJob.setEmail(emailUser);

        saveResultsJob.now();

        return rowsFiltered;
    }

    /**
     * Creates a new instance of CSIEvaluationResults, the data is obtained from
     * excel layout, that is mapped to Cells as follow: NUMERO_EMPLEADO -
     * position 0 ID_PREGUNTA - position >= 14 and position mod 2 is 0 RESULTADO
     * - position >= 14 and position mod 2 is 1
     * 
     * @param cells
     * @param mapEmployees
     * @param idSurvey
     * @return return a instance of CSIEvaluationResults if employee exists, in
     *         other case return null
     */
    private static List<CSIEvaluationResults> createEvaluationResultRow(List<Cell> cells,
            Map<Integer, Integer> mapEmployees, Integer idSurvey, Map<Integer, String> headers,
            Map<Integer, Integer> mapEvaluators, Map<Integer, Integer> questions) {

        List<CSIEvaluationResults> results = new ArrayList<>();

        Integer employee = Integer.parseInt("" + cells.get(0).getValue());
        Integer evaluator = Integer.parseInt("" + cells.get(4).getValue());
        if (mapEmployees.containsKey(employee) && mapEvaluators.containsKey(evaluator)) {

            int cellsMaxSize = headers.keySet().size();

            Integer idEmployee = mapEmployees.get(employee);
            Integer idEvaluator = mapEvaluators.get(evaluator);

            for (int i = 0; i < cells.size(); i++) {
                Cell cell = cells.get(i);
                if (cell.getIndex() < cellsMaxSize) {
                    if (cell.getIndex() >= 14 && (cell.getIndex() % 2) == 0) {

                        Integer idQuestion = questions.get((cell.getIndex() - 12) / 2);
                        Integer result = isNumber("" + cell.getValue()) ? Integer.valueOf("" + cell.getValue()) : null;
                        String resultText = !isNumber("" + cell.getValue()) ? "" + cell.getValue() : null;

                        String comments = null;
                        if ((i + 1) < cells.size()) {
                            Cell commentsCell = cells.get(i + 1);

                            if (commentsCell.getIndex().equals(cell.getIndex() + 1)) {
                                comments = "" + commentsCell.getValue();
                            }
                        }
                        String username = session.get("username");
                        CSIEvaluationResults csiResult = CsiEvaluationHelper.createCsiEvaluationResults(idSurvey,
                                idEmployee, comments, username, idQuestion, result, resultText, idEvaluator);

                        results.add(csiResult);

                    }
                }
            }

        }

        return results;
    }

    private static boolean isNumber(String value) {
        try {
            Integer.valueOf(value);
            return true;
        } catch (NumberFormatException e) {
            return false;
        }

    }

    /**
     * Método para crear una nuevas preguntas para la encuesta
     * 
     * @param headers Mapa que contiene las preguntas
     * @param idSurvey
     * @return Map de enteros
     */
    private static Map<Integer, Integer> createQuestions(Map<Integer, String> headers, Integer idSurvey) {

        Map<Integer, Integer> mapQuestions = new HashMap<>();

        List<Question> questions = Question.find("survey.id = ?", idSurvey).fetch();
        if (questions != null && !questions.isEmpty()) {
            for (Question question : questions) {
                mapQuestions.put(question.order, question.id);
            }

            return mapQuestions;
        }

        int order = 0;
        for (Integer idHeader : headers.keySet()) {
            if (idHeader >= 14 && (idHeader % 2) == 0) {
                Survey survey = new Survey();
                QuestionType questionType = new QuestionType();
                questionType.id = NUMERIC_QUESTION; // PREGUNTA NUMERICA
                survey.id = idSurvey;
                Question question = new Question();
                question.createdBy = session.get("username");
                question.survey = survey;
                question.order = ++order;
                question.creationDate = new Date();
                question.question = headers.get(idHeader);
                question.questionType = questionType;
                question.required = true;
                question.weighing = 0F;
                question.weighingPercent = 0F;
                question = question.save();
                mapQuestions.put(order, question.id);

            }
        }

        return mapQuestions;
    }

}
