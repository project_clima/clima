package controllers.csi;

import java.sql.CallableStatement;
import java.sql.SQLException;
import java.util.Date;

import oracle.jdbc.OracleConnection;

public class SnapStructure {

	/**
	 * Carga la estructura según la fecha
	 * @param date
	 */
    public static void loadSnapStructure(Date date) {

        OracleConnection connection = null;
        CallableStatement statement = null;
        java.sql.Date sqlDate = new java.sql.Date(date.getTime());

        try {
            connection = StructureCSI.getOracleConnection();
            statement = connection.prepareCall("call PORTALUNICO.PR_LOAD_DIM_ESTRUCTURA(?)");
            statement.setDate(1, sqlDate);
            statement.execute();

        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            try {
                if (statement != null && !statement.isClosed()) {
                    statement.close();
                }
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
    }
}
