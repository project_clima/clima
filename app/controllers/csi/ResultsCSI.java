package controllers.csi;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import javax.persistence.EntityManager;
import javax.persistence.Query;

import controllers.Secure;
import json.csi.dto.MatrixRawAreaLineResultDto;
import json.csi.dto.MatrixRawLineReportResultDto;
import json.csi.dto.MatrixReportResponseWrapperDto;
import json.csi.dto.MatrixReportResultDto;
import json.csi.dto.MatrixReportRowResultDto;
import json.csi.dto.MatrixReportValueResultDto;
import json.csi.dto.ResultAreaDto;
import json.csi.dto.ResultCatalogDto;
import json.csi.dto.ResultDetailDto;
import json.csi.dto.ResultHeaderDto;
import json.csi.dto.ResultResponseWrapperAreaDto;
import json.csi.dto.ResultResponseWrapperDto;
import json.csi.dto.SesionFilterWrapper;
import models.CSIEmployee;
import models.CSIType;
import models.csi.CsiImpugnedScore;
import play.db.jpa.GenericModel.JPAQuery;
import play.db.jpa.JPA;
import play.mvc.With;

@With(Secure.class)
public class ResultsCSI extends ResultsCSIQuerys {
    /**
     * Muestra las encuestas y tipos de CSI
     */
	public static void results() {
        List<CSIType> csiTypes = CSIType.findAll();
        List<Object> surveys = SurveysCSI.getAllSurveyLists();
        render(csiTypes, surveys);
    }

    /**
     * Método para obtener el reporte de results de Corp - Corp
     * 
     * @param csiType
     * @param levelFilter
     * @param corporation
     * @param management
     * @param world
     * @param section
     * @param evaluated
     */
    public static void getMatrixReportJSON(Integer csiType, Integer levelFilter, Integer corporation,
            Integer management, Integer world, Integer section, Integer evaluated) {
        List<Integer> ids = new ArrayList<>();

        SesionFilterWrapper filter = new SesionFilterWrapper(session, "d.idSurvey", "s.surveyDate", 1);
        SesionFilterWrapper filterStructure = new SesionFilterWrapper(session, null, "e.structureDate", 0);

        String filtroPrincipal = ", CSIEmployee e where d.idCsiType = 2";

        if (levelFilter == 1) {
            filtroPrincipal += " and d.idManager3Evaluado in(?1) ";
            ids = getMatrixEvaluatedAreas(levelFilter, null, filterStructure);
            StringBuilder sb = new StringBuilder();

            for (Integer s : ids) {
                sb.append(s).append(',');
            }

        } else if (levelFilter == 2) {
            filtroPrincipal += " and d.idManager4Evaluado in(?1) ";
            ids = getMatrixEvaluatedAreas(levelFilter, corporation, filterStructure);
            StringBuilder sb = new StringBuilder();

            for (Integer s : ids) {
                sb.append(s).append(',');
            }

        } else if (levelFilter == 3) {
            filtroPrincipal += " and d.idManager5Evaluado in(?1) ";
            ids = getMatrixEvaluatedAreas(levelFilter, management, filterStructure);

            StringBuilder sb = new StringBuilder();

            for (Integer s : ids) {
                sb.append(s).append(',');
            }

        } else if (levelFilter == 4) {
            filtroPrincipal += " and d.idManager5Evaluado in(?1)";
            ids = new ArrayList<>();
            ids.add(world);
            StringBuilder sb = new StringBuilder();

            for (Integer s : ids) {
                sb.append(s).append(',');
            }

        } else if (levelFilter == 5) {
            filtroPrincipal += " and d.userIdEvaluated in (?1)";
            ids = new ArrayList<>();
            ids.add(section);
            StringBuilder sb = new StringBuilder();

            for (Integer s : ids) {
                sb.append(s).append(',');
            }

        }
        String query = buildMatrixCorpQuery(filtroPrincipal, levelFilter, filter);

        if (levelFilter == 4 || levelFilter == 5) {
            query = buildMatrixCorpQuerySectionLevel(filtroPrincipal, filter);

        } else if (levelFilter == 6) {
            filtroPrincipal += " where d.userIdEvaluated in (?1)";
            ids = new ArrayList<>();

            ids.add(evaluated);
            query = buildMatrixCorpQueryEvaluatedEmployee(filtroPrincipal, filter);
        }

        JPAQuery jpaQuery = getFilteredDimResultsJPAQuery(filter, query, ids);

        List<MatrixRawLineReportResultDto> matrixReportResultDtoList = jpaQuery.fetch();

        Map<Integer, String> nextCatalogMap = new HashMap<>();

        for (MatrixRawLineReportResultDto matrixRawLineReportResultDto : matrixReportResultDtoList) {
            nextCatalogMap.put(matrixRawLineReportResultDto.getIdDireccionManager3Evaluado(),
                    matrixRawLineReportResultDto.getDireccionManager3Evaluado());
        }

        List<ResultCatalogDto> nextCatalog = new ArrayList<>();

        for (Integer idCatalog : nextCatalogMap.keySet()) {
            ResultCatalogDto catalog = new ResultCatalogDto(idCatalog, nextCatalogMap.get(idCatalog));
            nextCatalog.add(catalog);
        }

        MatrixReportResultDto matrixReport = getMatrixReportResultDto(matrixReportResultDtoList);

        MatrixReportResponseWrapperDto wrapper = new MatrixReportResponseWrapperDto(matrixReport, nextCatalog);

        renderJSON(wrapper);
    }

    /**
     * Metodo para obtener el reporte de results de Areas evaluadas
     * 
     * @param csiType
     * @param levelFilter
     * @param corporation
     * @param management
     * @param world
     * @param section
     * @param evaluated
     * @param ranking
     */
    public static void getTableReportAreaJSON(Integer csiType, Integer levelFilter, Integer corporation,
            Integer management, Integer world, Integer section, Integer evaluated, Integer ranking) {
        String filtroPrincipal = getPrincipalFilterAreas(levelFilter, corporation, management, world, section,
                evaluated);

        SesionFilterWrapper filter = new SesionFilterWrapper(session, "d.idSurvey", "s.surveyDate", 0);
        String queryArea = createQuery4Areas(filtroPrincipal, levelFilter, filter);

        if (levelFilter == 4 || levelFilter == 5) {
            queryArea = createQuery4SectionAreas(filtroPrincipal, filter);

        } else if (levelFilter == 6) {
            queryArea = createQuery4Employee(filtroPrincipal, filter);
        }

        JPAQuery rawResultsBefore = getMatrixRawAreaLineResultDtoJPAQuery(filter, queryArea);
        List<MatrixRawAreaLineResultDto> rawResults = rawResultsBefore.fetch();

        List<ResultAreaDto> csiHeaderAreaReport = processRowHeaderAreaData(rawResults);

        List<ResultCatalogDto> nextCatalog = new ArrayList<>();

        for (ResultAreaDto resultArea : csiHeaderAreaReport) {
            ResultCatalogDto catag = new ResultCatalogDto(resultArea.getIdManager(), resultArea.getNombreArea());

            nextCatalog.add(catag);
        }

        ResultResponseWrapperAreaDto wrapper = new ResultResponseWrapperAreaDto(csiHeaderAreaReport, nextCatalog);

        renderJSON(wrapper);
    }

    /**
     * Procesa las filas de la tabla de Áreas evaluadas periódicamente
     * 
     * @param data
     * @return List
     */
    protected static List<ResultAreaDto> processRowHeaderAreaData(List<MatrixRawAreaLineResultDto> data) {
        List<ResultAreaDto> csiHeaderAreaReportList = new ArrayList<>();
        Map<Integer, ResultAreaDto> resultsMatrix = new HashMap<>();

        for (MatrixRawAreaLineResultDto matrixResult : data) {
            Date surveyDate = matrixResult.getDate();
            Calendar calendar = Calendar.getInstance();
            calendar.setTime(surveyDate);
            int month = calendar.get(Calendar.MONTH);

            ResultAreaDto result = null;

            Double resultValue = matrixResult.getRawResult() / matrixResult.getTotalRawResult() * 100d;

            if (resultsMatrix.containsKey(matrixResult.getIdEvaluatedManager())) {
                result = resultsMatrix.get(matrixResult.getIdEvaluatedManager());

            } else {
                result = new ResultAreaDto();
                result.setNombreArea(matrixResult.getEvaluatedArea());
                result.setIdManager(matrixResult.getIdEvaluatedManager());
                result.setResult(matrixResult.getResult());
            }

            resultsMatrix.put(matrixResult.getIdEvaluatedManager(), addResult2Month(result, month, resultValue));

        }

        for (Integer idResult : resultsMatrix.keySet()) {
            csiHeaderAreaReportList.add(resultsMatrix.get(idResult));
        }

        return csiHeaderAreaReportList;
    }

    /**
     * Metodo para obtener el reporte de results de Tienda-Tienda y Tienda-Corp
     * 
     * @param csiType
     * @param levelFilter
     * @param corporation
     * @param management
     * @param world
     * @param section
     * @param evaluated
     * @param ranking
     */
    public static void getTableReportJSON(Integer csiType, Integer levelFilter, Integer corporation, Integer management,
            Integer world, Integer section, Integer evaluated, Integer ranking) {

        Map<Integer, ResultCatalogDto> mapCatalog = new HashMap<>();

        String filtroPrincipal = getPrincipalFilter(csiType, levelFilter, corporation, management, world, section,
                evaluated);
        if (filtroPrincipal == null || filtroPrincipal.trim().isEmpty()) {
            filtroPrincipal = " , CSIEmployee e where d.idSurvey = s.id ";
        } else {
            filtroPrincipal = filtroPrincipal + " and d.idSurvey = s.id ";
        }
        SesionFilterWrapper filter = new SesionFilterWrapper(session, "d.idSurvey", "s.surveyDate", 0);
        filtroPrincipal = filtroPrincipal + " " + filter.getCSITypeFilterSurveyTypeIN("s.surveyType.name") + " ";
        String extraFilters = "";
        if (!filter.getExtraFilters().trim().isEmpty()) {
            extraFilters = " AND " + filter.getExtraFilters() + " ";
        }
        String queryHeader = " select new json.csi.dto.ResultHeaderDto(0," + ROW_HEADER_QUERY_BASE
                + " from CsiDimensionResultsZone d, Survey s " + filtroPrincipal + "  " + extraFilters;

        // SE genera el reporte de los encabezados
        JPAQuery csiHeaderReportJPAQuery = null;
        csiHeaderReportJPAQuery = getFilteredDimResultsJPAQuery(filter, queryHeader);
        List<ResultHeaderDto> csiHeaderReport = csiHeaderReportJPAQuery.fetch();

        // SE genera el reporte para los catalogos
        SesionFilterWrapper filterStructure = new SesionFilterWrapper(session, null, "e.structureDate", 0);

        String extraFiltersStructure = "";
        if (!filterStructure.getExtraFilters().trim().isEmpty()) {
            extraFiltersStructure = " AND " + filterStructure.getExtraFilters() + " ";
        }

        List<ResultCatalogDto> nextCatalog = getNextCatalogs(levelFilter, corporation, management, world, section,
                evaluated, filterStructure);

        String idsCatalogo = getCatalogos(nextCatalog);

        for (ResultCatalogDto resultCatalogDto : nextCatalog) {
            mapCatalog.put(resultCatalogDto.getId(), resultCatalogDto);
        }

        // se agregan filtros para sacar las preguntas
        List<Integer> orders = getOrdersList(filterStructure, filtroPrincipal + " " + extraFiltersStructure);

        JPAQuery csiDetailReportJPAQuery = getDetailReport(levelFilter, orders, filtroPrincipal, extraFilters,
                corporation, idsCatalogo, management, world, section, evaluated, filter);

        nextCatalog = new ArrayList<>();
        List<ResultDetailDto> csiDetailReport = csiDetailReportJPAQuery.fetch();

        for (ResultDetailDto resultDetailDto : csiDetailReport) {
            nextCatalog.add(mapCatalog.get(resultDetailDto.getIdAgrupador()));
        }
        ResultResponseWrapperDto wrapper = new ResultResponseWrapperDto(csiHeaderReport, csiDetailReport, orders,
                nextCatalog);

        renderJSON(wrapper);
    }

    /**
     * Arma el Query para llenar la tabla de tienda - tienda o tienda - corporativo
     * 
     * @param levelFilter
     * @param orders
     * @param filtroPrincipal
     * @param extraFilters
     * @param corporation
     * @param idsCatalogo
     * @param management
     * @param world
     * @param section
     * @param evaluated
     * @param filter
     * @return JPAQuery
     */
    public static JPAQuery getDetailReport(Integer levelFilter, List<Integer> orders, String filtroPrincipal,
            String extraFilters, Integer corporation, String idsCatalogo, Integer management, Integer world,
            Integer section, Integer evaluated, SesionFilterWrapper filter) {

        JPAQuery csiDetailReport;

        String queryFirstLevelDetail = "select new json.csi.dto.ResultDetailDto( d.idManager3Evaluado ,"
                + ROW_detail_QUERY_BASE + getOrdersWhereString(orders) + ") "
                + " from CsiDimensionResultsZone d, Survey s " + filtroPrincipal + extraFilters
                + " group by d.idManager3Evaluado" + " ";

        if (levelFilter == 1) {
            csiDetailReport = getFilteredDimResultsJPAQuery(filter, queryFirstLevelDetail);
        } else if (levelFilter == 2) {
            csiDetailReport = getFilteredDimResultsJPAQuery(filter,
                    getQueryDetailsByLevel(filter, orders, levelFilter, corporation, idsCatalogo));
        } else if (levelFilter == 3) {
            csiDetailReport = getFilteredDimResultsJPAQuery(filter,
                    getQueryDetailsByLevel(filter, orders, levelFilter, management, idsCatalogo));
        } else if (levelFilter == 4) {
            csiDetailReport = getFilteredDimResultsJPAQuery(filter,
                    getQueryDetailsSectionLevel(filter, orders, idsCatalogo));
        } else if (levelFilter == 5) {
            csiDetailReport = getFilteredDimResultsJPAQuery(filter,
                    getQueryDetailsSectionLevel(filter, orders, "" + section));
        } else if (levelFilter == 6) {
            csiDetailReport = getFilteredDimResultsJPAQuery(filter,
                    getQueryDetailsSectionLevel(filter, orders, "" + evaluated));
        } else {
            // se repite el orden del nivel 1
            csiDetailReport = getFilteredDimResultsJPAQuery(filter, queryFirstLevelDetail);
        }

        return csiDetailReport;
    }

    /**
     * Obtiene los catálagos del siguiente filtro
     * 
     * @param levelFilter
     * @param corporation
     * @param management
     * @param world
     * @param section
     * @param evaluated
     * @param filterStructure
     * @return List de catálogos
     */
    protected static List<ResultCatalogDto> getNextCatalogs(Integer levelFilter, Integer corporation,
            Integer management, Integer world, Integer section, Integer evaluated,
            SesionFilterWrapper filterStructure) {
        List<ResultCatalogDto> nextCatalog = new ArrayList<>();

        String extraFilter = "";
        if (filterStructure.isHasDate()) {
            extraFilter = " AND " + filterStructure.getExtraFilters();
        }

        if(levelFilter == null)
        	levelFilter = -1;
        
        if (levelFilter == 1) {
            String query = "select new json.csi.dto.ResultCatalogDto(e.userId, e.department)  " + " from CSIEmployee e"
                    + " where e.userId in (50092383, 50092319)" + extraFilter;

            nextCatalog = getFilteredDimResultsJPAQuery(filterStructure, query).fetch();
        } else if (levelFilter == 2) {
            nextCatalog = loadNextCatalog(corporation, filterStructure);
        } else if (levelFilter == 3) {
            nextCatalog = loadNextCatalog(management, filterStructure);
        } else if (levelFilter == 4) {
            nextCatalog = loadNextCatalog(world, filterStructure);
        } else if (levelFilter == 5) {
            nextCatalog = loadUserNameCatalog(section, filterStructure);
        } else if (levelFilter == 6) {
            nextCatalog = loadUserNameCatalog(evaluated, filterStructure);
        }

        return nextCatalog;
    }

    /**
     * Setea los resultados de Áreas evaluadas periódicamente a los meses correspondientes
     * 
     * @param result
     * @param month
     * @param resultValue
     * @return ResultAreaDto
     */
    private static ResultAreaDto addResult2Month(ResultAreaDto result, int month, double resultValue) {
        switch (month) {
        case 0:
            result.setEnero(resultValue);
            break;

        case 1:
            result.setFebrero(resultValue);
            break;

        case 2:
            result.setMarzo(resultValue);
            break;

        case 3:
            result.setAbril(resultValue);
            break;

        case 4:
            result.setMayo(resultValue);
            break;

        case 5:
            result.setJunio(resultValue);
            break;

        case 6:
            result.setJulio(resultValue);
            break;

        case 7:
            result.setAgosto(resultValue);
            break;

        case 8:
            result.setSeptiembre(resultValue);
            break;

        case 9:
            result.setOctubre(resultValue);
            break;

        case 10:
            result.setNoviembre(resultValue);
            break;

        case 11:
            result.setDiciembre(resultValue);
            break;

        default:
            break;
        }

        return result;
    }

    /**
     * Método para llenar el reporte de Corp - Corp
     * 
     * @param matrixReportResultDtoList
     * @return MatrixReportResultDto
     */
    protected static MatrixReportResultDto getMatrixReportResultDto(
            List<MatrixRawLineReportResultDto> matrixReportResultDtoList) {

        MatrixReportResultDto report = new MatrixReportResultDto();

        Map<Integer, String> rowNames = new LinkedHashMap<>();
        Map<Integer, String> colNames = new LinkedHashMap<>();

        Map<Integer, MatrixReportRowResultDto> rows = new LinkedHashMap<>();

        for (MatrixRawLineReportResultDto line : matrixReportResultDtoList) {

            Integer idEvo = line.getIdDireccionManager3Evaluado();
            MatrixReportRowResultDto row = rows.get(idEvo);

            if (row == null) {// se crea el objeto Row
                row = new MatrixReportRowResultDto();
                rows.put(idEvo, row);
            }

            if (!rowNames.containsKey(idEvo)) { // Las columnas son el Evaluado
                String descEvo = line.getDireccionManager3Evaluado();

                if (descEvo == null || descEvo.trim().isEmpty()) {
                    descEvo = "" + idEvo;
                }
                rowNames.put(idEvo, descEvo);
            }

            Integer idEvr = line.getIdDireccionManager3Evaluador();
            if (!colNames.containsKey(idEvr)) { // Las columnas son el Evaluador
                String descEvr = line.getDireccionManager3Evaluador();
                colNames.put(idEvr, descEvr);
            }

            MatrixReportValueResultDto value = new MatrixReportValueResultDto(line);
            row.addValue(idEvr, value);
        }

        report.setRows(rows);
        report.setCountColumns(colNames.size());
        report.setCountRows(rowNames.size());
        report.setRowNames(rowNames);
        report.setColNames(colNames);

        return report;
    }

    /**
     * Obtiene los nombres del catálogos
     * 
     * @param nextCatalog
     * @return String 
     */
    protected static String getCatalogos(List<ResultCatalogDto> nextCatalog) {
        StringBuilder sb = new StringBuilder();
        for (int i = 0; i < nextCatalog.size(); i++) {
            if (i != 0) {
                sb.append(",");
            }
            sb.append(nextCatalog.get(i).getId());
        }
        return sb.toString();
    }

    /**
     * Obtiene el catálogo de dirección correspondiente
     * @param csiManager
     */
    public static void getAdress(int csiManager) {
        List<Object> addresses = CSIEmployee
                .find("select c.userId, c.department from CSIEmployee c where c.manager = ?1", csiManager).fetch();

        renderJSON(addresses);
    }

    /**
     * Obtiene el catálogo de mundo correspondiente
     * @param csiManager
     */
    public static void getWorld(int csiManager) {
        List<Object> worlds = CSIEmployee
                .find("select c.userId, c.department from CSIEmployee c where c.manager = ?1", csiManager).fetch();

        renderJSON(worlds);
    }

    /**
     * Obtiene el catálogo de sección correspondiente
     * @param csiManager
     */
    public static void getSection(int csiManager) {
        List<Object> sections = CSIEmployee
                .find("select c.userId, c.department from CSIEmployee c where c.manager = ?1", csiManager).fetch();

        renderJSON(sections);
    }

    /**
     * Obtiene el catálogo de área correspondiente
     * @param csiManager
     */
    public static void getArea(int csiManager) {
        List<Object> areas = CSIEmployee
                .find("select c.userId, c.title from CSIEmployee c where c.manager = ?1", csiManager).fetch();

        renderJSON(areas);
    }

    /**
     * Envía la impugnación de la calificación
     * 
     * @param val id del empleado
     */
    public static void sendRefuseMail(Integer val) {
        EntityManager em = JPA.em();

        Long exits;

        Object employee = CSIEmployee.find("select id from CSIEmployee where userId = ?1", val).first();

        exits = CsiImpugnedScore.find("select count(id) from CsiImpugnedScore where id = ?1", employee).first();

        if (exits > 0) {
            Query query2 = em.createNativeQuery(
                    "UPDATE PORTALUNICO.PUL_CSI_IMPUGNED_SCORE set FN_IMPUGNED = '1' where FN_ID_EMPLOYEE = ?");
            query2.setParameter(1, employee);
            query2.executeUpdate();
        } else {
            Query query = em.createNativeQuery(
                    "INSERT INTO PORTALUNICO.PUL_CSI_IMPUGNED_SCORE (FN_ID_EMPLOYEE,FN_ID_SURVEY,FN_IMPUGNED) VALUES (?,'0','1')");
            query.setParameter(1, employee);
            query.executeUpdate();
        }
    }

    /**
     * Muestra encuestas
     */
    public static void sendSurvey() {
        List survey = null;
        EntityManager em = JPA.em();
        Query query = em.createNativeQuery(
                "select ecsi.FC_LAST_NAME || ' ' || ecsi.FC_FIRST_NAME from PORTALUNICO.PUL_CSI_IMPUGNED_SCORE ims inner join PORTALUNICO.PUL_EMPLOYEE_CSI ecsi on ecsi.FN_ID_EMPLOYEE_CSI = ims.FN_ID_EMPLOYEE where ims.FN_IMPUGNED_STATUS = '1'");
        survey = query.getResultList();

        renderJSON(survey);
    }

    /**
     * Elimina impugnaciones
     */
    public static void eliminarImpugnar() {
        EntityManager em = JPA.em();
        Query query = em.createNativeQuery(
                "UPDATE PORTALUNICO.PUL_CSI_IMPUGNED_SCORE set FN_IMPUGNED_STATUS = '0' where FN_IMPUGNED_STATUS = '1'");
        query.executeUpdate();

    }

}
