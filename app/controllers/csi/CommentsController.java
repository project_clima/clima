package controllers.csi;

import java.io.IOException;
import java.text.DateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.Query;

import org.apache.poi.openxml4j.exceptions.InvalidFormatException;

import controllers.Filters;
import json.csi.dto.Question;
import json.csi.dto.QuestionsResponse;
import net.sf.jxls.exception.ParsePropertyException;
import play.db.jpa.JPA;
import play.mvc.Controller;
import utils.RenderExcel;

public class CommentsController extends Controller {
	
	/**
	 * Método para obtener los comentarios del empleado evaluado
	 * 
	 * @param id del usuario del Empleado evaluado
	 */
    public static void comments(Integer id) {

        String surveyFilter = session.get(session.getId() + "-filter-encuesta");
        String dateFilterFrom = "";
        String dateFilterTo = "";
        String querySurvey = "";
        String queryDate1 = "";
        String queryDate2 = "";
        List commentsCSI = null;
        EntityManager em = JPA.em();

        if (surveyFilter != null) {
            querySurvey = " and FN_ID_SURVEY = ?2";
        }

        if (session.get(session.getId() + "-filter-date-type") != null) {
            Date[] filterDate = Filters.getDateFilters(session.get(session.getId() + "-filter-date-type"), session);

            dateFilterFrom = DateFormat.getDateInstance().format(filterDate[0]);
            String[] aux = DateFormat.getDateInstance().format(filterDate[1]).split("/");
            int mes = (Integer.valueOf(aux[1]) + 1 > 12) ? 12 : Integer.valueOf(aux[1]) + 1;
            dateFilterTo = "01/" + mes + "/" + aux[2];
            queryDate1 = "inner join PORTALUNICO.PUL_SURVEY on ( PORTALUNICO.PUL_SURVEY.FN_ID_SURVEY = PORTALUNICO.PUL_CSI_EVALUATION_RESULTS.FN_ID_SURVEY)";
            queryDate2 = "and PORTALUNICO.PUL_SURVEY.FD_SURVEY_DATE between ?2 and ?3";
        }

        String queryStr = "select FN_ID_EVA_RESULTS, fc_comments from PORTALUNICO.PUL_CSI_EVALUATION_RESULTS "
                + queryDate1 + "where FN_ID_EVALUATED_EMP in (" + "select FN_ID_EMPLOYEE_CSI from "
                + "PORTALUNICO.pul_employee_csi" + " start with fn_userid = abs(?1)"
                + " CONNECT BY prior  FN_USERID = FN_MANAGER" + ")" + querySurvey + queryDate2
                + " and fc_comments is not null";
        Query q = em.createNativeQuery(queryStr);
        q.setParameter(1, id);
        if (!querySurvey.equals(""))
            q.setParameter(2, surveyFilter);

        if (!queryDate1.equals("")) {
            q.setParameter(2, dateFilterFrom);
            q.setParameter(3, dateFilterTo);
        }

        commentsCSI = q.getResultList();

        render(commentsCSI);
    }

    /**
     * Método para recuperar los comentarios según el filtro elegido
     * 
     * @param id filtro
     * @param csiId id del usuario del Empleado evaluado
     */
    public static void getComments(Integer id, Integer csiId) {

        switch (id) {
        case 0:
            getAll(csiId);
            break;
        case 3:

            getDetractor(csiId);
            break;
        case 4:

            getPromotor(csiId);
            break;
        case 5:

            getPasivos(csiId);
            break;
        default:

            break;
        }

    }

    
    /**
     * Método que recupera todos comentarios del empleado evaluado
     * 
     * @param id del usuario del Empleado evaluado
     */
    public static void getAll(Integer id) {

        List commentsCSI = null;
        EntityManager em = JPA.em();
        String surveyFilter = session.get(session.getId() + "-filter-encuesta");
        String querySurvey = "";
        String dateFilterFrom = "";
        String dateFilterTo = "";
        String queryDate1 = "";
        String queryDate2 = "";

        if (surveyFilter != null) {
            querySurvey = "and FN_ID_SURVEY = ?2";
        }

        if (session.get(session.getId() + "-filter-date-type") != null) {
            Date[] filterDate = Filters.getDateFilters(session.get(session.getId() + "-filter-date-type"), session);

            dateFilterFrom = DateFormat.getDateInstance().format(filterDate[0]);
            String[] aux = DateFormat.getDateInstance().format(filterDate[1]).split("/");
            int mes = (Integer.valueOf(aux[1]) + 1 > 12) ? 12 : Integer.valueOf(aux[1]) + 1;
            dateFilterTo = "01/" + mes + "/" + aux[2];

            queryDate1 = "inner join PORTALUNICO.PUL_SURVEY on ( PORTALUNICO.PUL_SURVEY.FN_ID_SURVEY = PORTALUNICO.PUL_CSI_EVALUATION_RESULTS.FN_ID_SURVEY)";
            queryDate2 = "and PORTALUNICO.PUL_SURVEY.FD_SURVEY_DATE between ?2 and ?3";
        }

        String queryStr = "select FC_COMMENTS from PORTALUNICO.PUL_CSI_EVALUATION_RESULTS " + queryDate1
                + "where FN_ID_EVALUATED_EMP in ("
                + "select FN_ID_EMPLOYEE_CSI from PORTALUNICO.pul_employee_csi start with fn_userid = abs(?) "
                + "CONNECT BY prior  FN_USERID = FN_MANAGER" + " ) " + querySurvey + queryDate2
                + " and fc_comments is not null";

        Query q = em.createNativeQuery(queryStr);
        q.setParameter(1, id);
        commentsCSI = q.getResultList();

        if (!querySurvey.equals(""))
            q.setParameter(2, surveyFilter);

        if (!queryDate1.equals("")) {
            q.setParameter(2, dateFilterFrom);
            q.setParameter(3, dateFilterTo);
        }

        List<Object[]> results = new ArrayList<>();
        for (int i = 0; i < commentsCSI.size(); i++) {
            Object[] row = new Object[2];
            row[0] = i + 1;
            row[1] = commentsCSI.get(i);
            results.add(row);
        }

        renderJSON(results);
    }

    /**
     * Obtiene todos los comentarios del empleado evaluado filtrados por palabra clave
     * @param id del usuario del Empleado evaluado
     */
    public static void getAllPorClave(Integer id) {

        List commentsCSI = null;
        EntityManager em = JPA.em();
        String surveyFilter = session.get(session.getId() + "-filter-encuesta");
        String querySurvey = "";
        String dateFilterFrom = "";
        String dateFilterTo = "";
        String queryDate1 = "";
        String queryDate2 = "";

        if (surveyFilter != null) {
            querySurvey = "and FN_ID_SURVEY = ?2";
        }

        if (session.get(session.getId() + "-filter-date-type") != null) {
            Date[] filterDate = Filters.getDateFilters(session.get(session.getId() + "-filter-date-type"), session);

            dateFilterFrom = DateFormat.getDateInstance().format(filterDate[0]);
            String[] aux = DateFormat.getDateInstance().format(filterDate[1]).split("/");
            int mes = (Integer.valueOf(aux[1]) + 1 > 12) ? 12 : Integer.valueOf(aux[1]) + 1;
            dateFilterTo = "01/" + mes + "/" + aux[2];

            queryDate1 = "inner join PORTALUNICO.PUL_SURVEY on ( PORTALUNICO.PUL_SURVEY.FN_ID_SURVEY = PORTALUNICO.PUL_CSI_EVALUATION_RESULTS.FN_ID_SURVEY)";
            queryDate2 = "and PORTALUNICO.PUL_SURVEY.FD_SURVEY_DATE between ?2 and ?3";
        }

        String queryStr = "select FC_COMMENTS from PORTALUNICO.PUL_CSI_EVALUATION_RESULTS " + queryDate1
                + "where FN_ID_EVALUATED_EMP in ("
                + "select FN_ID_EMPLOYEE_CSI from PORTALUNICO.pul_employee_csi start with fn_userid = abs(?) "
                + "CONNECT BY prior  FN_USERID = FN_MANAGER" + " ) " + querySurvey + queryDate2
                + " and fc_comments is not null";

        Query q = em.createNativeQuery(queryStr);
        q.setParameter(1, id);

        if (!querySurvey.equals(""))
            q.setParameter(2, surveyFilter);

        if (!queryDate1.equals("")) {
            q.setParameter(2, dateFilterFrom);
            q.setParameter(3, dateFilterTo);
        }

        commentsCSI = q.getResultList();

        renderJSON(commentsCSI);
    }

    /**
     * Obtiene las preguntas de las encuestas realizadas
     * 
     * @param id del usuario del empleado evaluado
     * @param csiId
     */
    public static void getQuestions(Integer id, Integer csiId) {

        List questionsCSI = null;
        EntityManager em = JPA.em();
        String surveyFilter = session.get(session.getId() + "-filter-encuesta");
        String querySurvey = "";
        String dateFilterFrom = "";
        String dateFilterTo = "";
        String queryDate1 = "";
        String queryDate2 = "";

        if (surveyFilter != null) {
            querySurvey = "and FN_ID_SURVEY = ?2";
        }

        if (session.get(session.getId() + "-filter-date-type") != null) {
            Date[] filterDate = Filters.getDateFilters(session.get(session.getId() + "-filter-date-type"), session);

            dateFilterFrom = DateFormat.getDateInstance().format(filterDate[0]);
            String[] aux = DateFormat.getDateInstance().format(filterDate[1]).split("/");
            int mes = (Integer.valueOf(aux[1]) + 1 > 12) ? 12 : Integer.valueOf(aux[1]) + 1;
            dateFilterTo = "01/" + mes + "/" + aux[2];

            queryDate1 = "inner join PORTALUNICO.PUL_SURVEY on ( PORTALUNICO.PUL_SURVEY.FN_ID_SURVEY = PORTALUNICO.PUL_CSI_EVALUATION_RESULTS.FN_ID_SURVEY)";
            queryDate2 = "and PORTALUNICO.PUL_SURVEY.FD_SURVEY_DATE between ?2 and ?3";
        }

        String queryStr = "select DISTINCT q.FN_SORT_ORDER from PORTALUNICO.PUL_CSI_EVALUATION_RESULTS er, PORTALUNICO.PUL_QUESTION q  "
                + queryDate1 + "where er.FN_ID_EVALUATED_EMP in ("
                + "select FN_ID_EMPLOYEE_CSI from PORTALUNICO.pul_employee_csi start with fn_userid = abs(?) "
                + "CONNECT BY prior  FN_USERID = FN_MANAGER " + ") " + querySurvey + queryDate2
                + " and er.fc_comments is not null and er.FN_ID_QUESTION = q.FN_ID_QUESTION";

        Query q = em.createNativeQuery(queryStr);
        q.setParameter(1, id);

        if (!querySurvey.equals(""))
            q.setParameter(2, surveyFilter);

        if (!queryDate1.equals("")) {
            q.setParameter(2, dateFilterFrom);
            q.setParameter(3, dateFilterTo);
        }

        questionsCSI = q.getResultList();

        Question question = null;
        QuestionsResponse response = new QuestionsResponse();
        for (int i = 0; i < questionsCSI.size(); i++) {
            Object value = questionsCSI.get(i);

            question = new Question(value, "Pregunta " + (i + 1));
            response.getQuestions().add(question);
        }

        renderJSON(response);

    }

    /**
     * Método para obtener los comentarios de los empleados evaluados filtrados por detractor
     * 
     * @param id del usuario del Empleado evaluado
     */
    public static void getDetractor(Integer id) {

        List commentsCSI = null;
        EntityManager em = JPA.em();
        String surveyFilter = session.get(session.getId() + "-filter-encuesta");
        String querySurvey = "";
        String dateFilterFrom = "";
        String dateFilterTo = "";
        String queryDate1 = "";
        String queryDate2 = "";

        if (surveyFilter != null) {
            querySurvey = "and FN_ID_SURVEY = ?2";
        }

        if (session.get(session.getId() + "-filter-date-type") != null) {
            Date[] filterDate = Filters.getDateFilters(session.get(session.getId() + "-filter-date-type"), session);

            dateFilterFrom = DateFormat.getDateInstance().format(filterDate[0]);
            String[] aux = DateFormat.getDateInstance().format(filterDate[1]).split("/");
            int mes = (Integer.valueOf(aux[1]) + 1 > 12) ? 12 : Integer.valueOf(aux[1]) + 1;
            dateFilterTo = "01/" + mes + "/" + aux[2];

            queryDate1 = "inner join PORTALUNICO.PUL_SURVEY on ( PORTALUNICO.PUL_SURVEY.FN_ID_SURVEY = PORTALUNICO.PUL_CSI_EVALUATION_RESULTS.FN_ID_SURVEY)";
            queryDate2 = "and PORTALUNICO.PUL_SURVEY.FD_SURVEY_DATE between ?2 and ?3";
        }

        String queryStr = "select FC_COMMENTS from PORTALUNICO.PUL_CSI_EVALUATION_RESULTS " + queryDate1
                + "where FN_ID_EVALUATED_EMP in ("
                + "select FN_ID_EMPLOYEE_CSI from PORTALUNICO.pul_employee_csi start with fn_userid = abs(?) "
                + "CONNECT BY prior  FN_USERID = FN_MANAGER" + " ) " + querySurvey + queryDate2
                + " and fc_comments is not null and FN_RESULT between 0 and 6";

        Query q = em.createNativeQuery(queryStr);
        q.setParameter(1, id);

        if (!querySurvey.equals(""))
            q.setParameter(2, surveyFilter);

        if (!queryDate1.equals("")) {
            q.setParameter(2, dateFilterFrom);
            q.setParameter(3, dateFilterTo);
        }

        commentsCSI = q.getResultList();
        List<Object[]> results = new ArrayList<>();
        for (int i = 0; i < commentsCSI.size(); i++) {
            Object[] row = new Object[2];
            row[0] = i + 1;
            row[1] = commentsCSI.get(i);
            results.add(row);
        }

        renderJSON(results);
    }
    
    /**
     * Método para obtener los comentarios de los empleados evaluados filtrados por promotor
     * 
     * @param id del usuario del Empleado evaluado
     */
    private static void getPromotor(Integer id) {

        List commentsCSI = null;
        EntityManager em = JPA.em();
        String surveyFilter = session.get(session.getId() + "-filter-encuesta");
        String querySurvey = "";
        String dateFilterFrom = "";
        String dateFilterTo = "";
        String queryDate1 = "";
        String queryDate2 = "";

        if (surveyFilter != null) {
            querySurvey = "and FN_ID_SURVEY = ?2";
        }

        if (session.get(session.getId() + "-filter-date-type") != null) {
            Date[] filterDate = Filters.getDateFilters(session.get(session.getId() + "-filter-date-type"), session);

            dateFilterFrom = DateFormat.getDateInstance().format(filterDate[0]);
            String[] aux = DateFormat.getDateInstance().format(filterDate[1]).split("/");
            int mes = (Integer.valueOf(aux[1]) + 1 > 12) ? 12 : Integer.valueOf(aux[1]) + 1;
            dateFilterTo = "01/" + mes + "/" + aux[2];

            queryDate1 = "inner join PORTALUNICO.PUL_SURVEY on ( PORTALUNICO.PUL_SURVEY.FN_ID_SURVEY = PORTALUNICO.PUL_CSI_EVALUATION_RESULTS.FN_ID_SURVEY)";
            queryDate2 = "and PORTALUNICO.PUL_SURVEY.FD_SURVEY_DATE between ?2 and ?3";
        }

        String queryStr = "select FC_COMMENTS from PORTALUNICO.PUL_CSI_EVALUATION_RESULTS " + queryDate1
                + "where FN_ID_EVALUATED_EMP in ("
                + "select FN_ID_EMPLOYEE_CSI from PORTALUNICO.pul_employee_csi start with fn_userid = abs(?) "
                + "CONNECT BY prior  FN_USERID = FN_MANAGER " + ") " + querySurvey + queryDate2
                + " and fc_comments is not null and FN_RESULT between 9 and 10";

        Query q = em.createNativeQuery(queryStr);
        q.setParameter(1, id);

        if (!querySurvey.equals(""))
            q.setParameter(2, surveyFilter);

        if (!queryDate1.equals("")) {
            q.setParameter(2, dateFilterFrom);
            q.setParameter(3, dateFilterTo);
        }

        commentsCSI = q.getResultList();
        List<Object[]> results = new ArrayList<>();
        for (int i = 0; i < commentsCSI.size(); i++) {
            Object[] row = new Object[2];
            row[0] = i + 1;
            row[1] = commentsCSI.get(i);
            results.add(row);
        }

        renderJSON(results);
    }

    /**
     * Método para obtener los comentarios de los empleados evaluados filtrados por pasivos
     * 
     * @param id del usuario del Empleado evaluado
     */
    public static void getPasivos(Integer id) {

        List commentsCSI = null;
        EntityManager em = JPA.em();
        String surveyFilter = session.get(session.getId() + "-filter-encuesta");
        String querySurvey = "";
        String dateFilterFrom = "";
        String dateFilterTo = "";
        String queryDate1 = "";
        String queryDate2 = "";

        if (surveyFilter != null) {
            querySurvey = " and FN_ID_SURVEY = ?2";
        }

        if (session.get(session.getId() + "-filter-date-type") != null) {
            Date[] filterDate = Filters.getDateFilters(session.get(session.getId() + "-filter-date-type"), session);

            dateFilterFrom = DateFormat.getDateInstance().format(filterDate[0]);
            String[] aux = DateFormat.getDateInstance().format(filterDate[1]).split("/");
            int mes = (Integer.valueOf(aux[1]) + 1 > 12) ? 12 : Integer.valueOf(aux[1]) + 1;
            dateFilterTo = "01/" + mes + "/" + aux[2];

            queryDate1 = "inner join PORTALUNICO.PUL_SURVEY on ( PORTALUNICO.PUL_SURVEY.FN_ID_SURVEY = PORTALUNICO.PUL_CSI_EVALUATION_RESULTS.FN_ID_SURVEY)";
            queryDate2 = "and PORTALUNICO.PUL_SURVEY.FD_SURVEY_DATE between ?2 and ?3";
        }

        String queryStr = "select FC_COMMENTS from PORTALUNICO.PUL_CSI_EVALUATION_RESULTS " + queryDate1
                + "where FN_ID_EVALUATED_EMP in (" + "select FN_ID_EMPLOYEE_CSI from PORTALUNICO.pul_employee_csi "
                + "start with fn_userid = abs(?) CONNECT BY prior  FN_USERID = FN_MANAGER " + ")" + querySurvey
                + queryDate2 + " and fc_comments is not null and FN_RESULT between 7 and 8";

        Query q = em.createNativeQuery(queryStr);
        q.setParameter(1, id);

        if (!querySurvey.equals(""))
            q.setParameter(2, surveyFilter);

        if (!queryDate1.equals("")) {
            q.setParameter(2, dateFilterFrom);
            q.setParameter(3, dateFilterTo);
        }

        commentsCSI = q.getResultList();
        List<Object[]> results = new ArrayList<>();
        for (int i = 0; i < commentsCSI.size(); i++) {
            Object[] row = new Object[2];
            row[0] = i + 1;
            row[1] = commentsCSI.get(i);
            results.add(row);
        }

        renderJSON(results);
    }
    
    /**
     * Método que obtiene los comentarios del empleado evaluado por pregunta
     * 
     * @param id sort order
     * @param csiId id del usuario del empleado evaluado
     */
    public static void getCommetsQuestions(Integer id, Integer csiId) {
        List commentsCSI = null;
        EntityManager em = JPA.em();
        String surveyFilter = session.get(session.getId() + "-filter-encuesta");
        String querySurvey = "";
        String dateFilterFrom = "";
        String dateFilterTo = "";
        String queryDate1 = "";
        String queryDate2 = "";

        if (surveyFilter != null) {
            querySurvey = " and FN_ID_SURVEY = ?2";
        }

        if (session.get(session.getId() + "-filter-date-type") != null) {
            Date[] filterDate = Filters.getDateFilters(session.get(session.getId() + "-filter-date-type"), session);

            dateFilterFrom = DateFormat.getDateInstance().format(filterDate[0]);
            String[] aux = DateFormat.getDateInstance().format(filterDate[1]).split("/");
            int mes = (Integer.valueOf(aux[1]) + 1 > 12) ? 12 : Integer.valueOf(aux[1]) + 1;
            dateFilterTo = "01/" + mes + "/" + aux[2];

            queryDate1 = "inner join PORTALUNICO.PUL_SURVEY on ( PORTALUNICO.PUL_SURVEY.FN_ID_SURVEY = PORTALUNICO.PUL_CSI_EVALUATION_RESULTS.FN_ID_SURVEY)";
            queryDate2 = "and PORTALUNICO.PUL_SURVEY.FD_SURVEY_DATE between ?2 and ?3";
        }

        String queryStr = "select FC_COMMENTS from PORTALUNICO.PUL_CSI_EVALUATION_RESULTS er,PORTALUNICO.PUL_QUESTION q "
                + queryDate1 + "where FN_ID_EVALUATED_EMP in ("
                + "select FN_ID_EMPLOYEE_CSI from PORTALUNICO.pul_employee_csi start with fn_userid = abs(?1) "
                + "CONNECT BY prior  FN_USERID = FN_MANAGER " + ")" + querySurvey + queryDate2
                + "and er.FN_ID_QUESTION = q.FN_ID_QUESTION and q.FN_SORT_ORDER = ?4 and fc_comments is not null";

        Query q = em.createNativeQuery(queryStr);
        q.setParameter(1, csiId);

        if (!querySurvey.equals(""))
            q.setParameter(2, surveyFilter);

        if (!queryDate1.equals("")) {
            q.setParameter(2, dateFilterFrom);
            q.setParameter(3, dateFilterTo);
        }

        q.setParameter(4, id);
        commentsCSI = q.getResultList();
        List<Object[]> results = new ArrayList<>();
        for (int i = 0; i < commentsCSI.size(); i++) {
            Object[] row = new Object[2];
            row[0] = i + 1;
            row[1] = commentsCSI.get(i);
            results.add(row);
        }
        renderJSON(results);
    }
    
    /**
     * Método para descargar el archvio de excel según el filtro elegido
     * 
     * @param id filtro
     * @param csiId id del usuario del Empleado evaluado
     * 
     * @throws ParsePropertyException
     * @throws InvalidFormatException
     * @throws IOException
     */
    public static void download(Integer id, Integer csiId)
            throws ParsePropertyException, InvalidFormatException, IOException {
        if (id == null || id == 2) {
            id = 0;
        }
        switch (id) {
        case 0:
            downloadAll(csiId);
            break;
        case 3:
            downloadDetractor(csiId);
            break;
        case 4:
            downloadPromotor(csiId);
            break;
        case 5:
            downloadPasivos(csiId);
            break;
        default:
            break;
        }
    }
    
    /**
     * Método para descargar todos los comentarios de los empleados evaluados
     * 
     * @param csiId id del usuario del empleado evaluado
     * 
     * @throws ParsePropertyException
     * @throws InvalidFormatException
     * @throws IOException
     */
    public static void downloadAll(Integer csiId) throws ParsePropertyException, InvalidFormatException, IOException {
        List commentsCSI = null;
        EntityManager em = JPA.em();
        String surveyFilter = session.get(session.getId() + "-filter-encuesta");
        String querySurvey = "";
        String dateFilterFrom = "";
        String dateFilterTo = "";
        String queryDate1 = "";
        String queryDate2 = "";

        if (surveyFilter != null) {
            querySurvey = " and FN_ID_SURVEY = ?2";
        }

        if (session.get(session.getId() + "-filter-date-type") != null) {
            Date[] filterDate = Filters.getDateFilters(session.get(session.getId() + "-filter-date-type"), session);

            dateFilterFrom = DateFormat.getDateInstance().format(filterDate[0]);
            String[] aux = DateFormat.getDateInstance().format(filterDate[1]).split("/");
            int mes = (Integer.valueOf(aux[1]) + 1 > 12) ? 12 : Integer.valueOf(aux[1]) + 1;
            dateFilterTo = "01/" + mes + "/" + aux[2];

            queryDate1 = "inner join PORTALUNICO.PUL_SURVEY on ( PORTALUNICO.PUL_SURVEY.FN_ID_SURVEY = PORTALUNICO.PUL_CSI_EVALUATION_RESULTS.FN_ID_SURVEY)";
            queryDate2 = "and PORTALUNICO.PUL_SURVEY.FD_SURVEY_DATE between ?2 and ?3";
        }

        String queryStr = "select fc_comments from PORTALUNICO.PUL_CSI_EVALUATION_RESULTS " + queryDate1
                + "where FN_ID_EVALUATED_EMP in ("
                + "select FN_ID_EMPLOYEE_CSI from PORTALUNICO.pul_employee_csi start with fn_userid = abs(?) "
                + "CONNECT BY prior  FN_USERID = FN_MANAGER " + ")" + querySurvey + queryDate2
                + " and fc_comments is not null";

        Query q = em.createNativeQuery(queryStr);
        q.setParameter(1, csiId);

        if (!querySurvey.equals(""))
            q.setParameter(2, surveyFilter);

        if (!queryDate1.equals("")) {
            q.setParameter(2, dateFilterFrom);
            q.setParameter(3, dateFilterTo);
        }

        commentsCSI = q.getResultList();

        renderArgs.put("commentsCSI", commentsCSI);
        renderArgs.put(RenderExcel.RA_FILENAME, "Comentarios.xlsx");
        RenderExcel renderExcel = new RenderExcel("app/excel/views/ResultsCSI/comments.xlsx");
        renderExcel.render();
    }

    /**
     * Método para descargar comentarios de los empleados evaluados filtrados por detractor
     * 
     * @param csiId id del usuario del empleado evaluado
     * 
     * @throws ParsePropertyException
     * @throws InvalidFormatException
     * @throws IOException
     */
    public static void downloadDetractor(Integer csiId)
            throws ParsePropertyException, InvalidFormatException, IOException {
        List commentsCSI = null;
        EntityManager em = JPA.em();
        String surveyFilter = session.get(session.getId() + "-filter-encuesta");
        String querySurvey = "";
        String dateFilterFrom = "";
        String dateFilterTo = "";
        String queryDate1 = "";
        String queryDate2 = "";

        if (surveyFilter != null) {
            querySurvey = " and FN_ID_SURVEY = ?2";
        }

        if (session.get(session.getId() + "-filter-date-type") != null) {
            Date[] filterDate = Filters.getDateFilters(session.get(session.getId() + "-filter-date-type"), session);

            dateFilterFrom = DateFormat.getDateInstance().format(filterDate[0]);
            String[] aux = DateFormat.getDateInstance().format(filterDate[1]).split("/");
            int mes = (Integer.valueOf(aux[1]) + 1 > 12) ? 12 : Integer.valueOf(aux[1]) + 1;
            dateFilterTo = "01/" + mes + "/" + aux[2];

            queryDate1 = "inner join PORTALUNICO.PUL_SURVEY on ( PORTALUNICO.PUL_SURVEY.FN_ID_SURVEY = PORTALUNICO.PUL_CSI_EVALUATION_RESULTS.FN_ID_SURVEY)";
            queryDate2 = "and PORTALUNICO.PUL_SURVEY.FD_SURVEY_DATE between ?2 and ?3";
        }

        String queryStr = "select fc_comments from PORTALUNICO.PUL_CSI_EVALUATION_RESULTS " + queryDate1
                + "where FN_ID_EVALUATED_EMP in ("
                + "select FN_ID_EMPLOYEE_CSI from PORTALUNICO.pul_employee_csi start with fn_userid = abs(?) "
                + "CONNECT BY prior  FN_USERID = FN_MANAGER" + ")" + querySurvey + queryDate2
                + " and fc_comments is not null and FN_RESULT between 0 and 6";

        Query q = em.createNativeQuery(queryStr);
        q.setParameter(1, csiId);

        if (!querySurvey.equals(""))
            q.setParameter(2, surveyFilter);

        if (!queryDate1.equals("")) {
            q.setParameter(2, dateFilterFrom);
            q.setParameter(3, dateFilterTo);
        }

        commentsCSI = q.getResultList();

        renderArgs.put("commentsCSI", commentsCSI);
        renderArgs.put(RenderExcel.RA_FILENAME, "Comentarios.xlsx");
        RenderExcel renderExcel = new RenderExcel("app/excel/views/ResultsCSI/comments.xlsx");
        renderExcel.render();
    }

    /**
     * Método para descargar los comentarios de los empleados evaluados filtrados por promotor
     * 
     * @param csiId id del usuario del empleado evaluado
     * 
     * @throws ParsePropertyException
     * @throws InvalidFormatException
     * @throws IOException
     */
    public static void downloadPromotor(Integer csiId)
            throws ParsePropertyException, InvalidFormatException, IOException {
        List commentsCSI = null;
        EntityManager em = JPA.em();
        String surveyFilter = session.get(session.getId() + "-filter-encuesta");
        String querySurvey = "";
        String dateFilterFrom = "";
        String dateFilterTo = "";
        String queryDate1 = "";
        String queryDate2 = "";

        if (surveyFilter != null) {
            querySurvey = " and FN_ID_SURVEY = ?2";
        }

        if (session.get(session.getId() + "-filter-date-type") != null) {
            Date[] filterDate = Filters.getDateFilters(session.get(session.getId() + "-filter-date-type"), session);

            dateFilterFrom = DateFormat.getDateInstance().format(filterDate[0]);
            String[] aux = DateFormat.getDateInstance().format(filterDate[1]).split("/");
            int mes = (Integer.valueOf(aux[1]) + 1 > 12) ? 12 : Integer.valueOf(aux[1]) + 1;
            dateFilterTo = "01/" + mes + "/" + aux[2];

            queryDate1 = "inner join PORTALUNICO.PUL_SURVEY on ( PORTALUNICO.PUL_SURVEY.FN_ID_SURVEY = PORTALUNICO.PUL_CSI_EVALUATION_RESULTS.FN_ID_SURVEY)";
            queryDate2 = "and PORTALUNICO.PUL_SURVEY.FD_SURVEY_DATE between ?2 and ?3";
        }

        String queryStr = "select fc_comments from PORTALUNICO.PUL_CSI_EVALUATION_RESULTS " + queryDate1
                + "where FN_ID_EVALUATED_EMP in ("
                + "select FN_ID_EMPLOYEE_CSI from PORTALUNICO.pul_employee_csi start with fn_userid = abs(?) "
                + "CONNECT BY prior  FN_USERID = FN_MANAGER " + ")" + querySurvey + queryDate2
                + " and fc_comments is not null and FN_RESULT between 9 and 10";

        Query q = em.createNativeQuery(queryStr);
        q.setParameter(1, csiId);

        if (!querySurvey.equals(""))
            q.setParameter(2, surveyFilter);

        if (!queryDate1.equals("")) {
            q.setParameter(2, dateFilterFrom);
            q.setParameter(3, dateFilterTo);
        }

        commentsCSI = q.getResultList();

        renderArgs.put("commentsCSI", commentsCSI);
        renderArgs.put(RenderExcel.RA_FILENAME, "Comentarios.xlsx");
        RenderExcel renderExcel = new RenderExcel("app/excel/views/ResultsCSI/comments.xlsx");
        renderExcel.render();
    }

    /**
     * Método para descargar los comentarios de los empleados evaluados filtrados por pasivos
     * 
     * @param csiId id del usuario del empleado evaluado
     * @throws ParsePropertyException
     * @throws InvalidFormatException
     * @throws IOException
     */
    public static void downloadPasivos(Integer csiId)
            throws ParsePropertyException, InvalidFormatException, IOException {
        List commentsCSI = null;
        EntityManager em = JPA.em();

        String surveyFilter = session.get(session.getId() + "-filter-encuesta");
        String querySurvey = "";
        String dateFilterFrom = "";
        String dateFilterTo = "";
        String queryDate1 = "";
        String queryDate2 = "";

        if (surveyFilter != null) {
            querySurvey = " and FN_ID_SURVEY = ?2";
        }

        if (session.get(session.getId() + "-filter-date-type") != null) {
            Date[] filterDate = Filters.getDateFilters(session.get(session.getId() + "-filter-date-type"), session);

            dateFilterFrom = DateFormat.getDateInstance().format(filterDate[0]);
            String[] aux = DateFormat.getDateInstance().format(filterDate[1]).split("/");
            int mes = (Integer.valueOf(aux[1]) + 1 > 12) ? 12 : Integer.valueOf(aux[1]) + 1;
            dateFilterTo = "01/" + mes + "/" + aux[2];

            queryDate1 = "inner join PORTALUNICO.PUL_SURVEY on ( PORTALUNICO.PUL_SURVEY.FN_ID_SURVEY = PORTALUNICO.PUL_CSI_EVALUATION_RESULTS.FN_ID_SURVEY)";
            queryDate2 = "and PORTALUNICO.PUL_SURVEY.FD_SURVEY_DATE between ?2 and ?3";
        }

        String queryStr = "select fc_comments from PORTALUNICO.PUL_CSI_EVALUATION_RESULTS " + queryDate1
                + "where FN_ID_EVALUATED_EMP in ("
                + "select FN_ID_EMPLOYEE_CSI from PORTALUNICO.pul_employee_csi start with fn_userid = abs(?) "
                + "CONNECT BY prior  FN_USERID = FN_MANAGER " + ")" + querySurvey + queryDate2
                + " and fc_comments is not null and FN_RESULT between 7 and 8";

        Query q = em.createNativeQuery(queryStr);
        q.setParameter(1, csiId);

        if (!querySurvey.equals(""))
            q.setParameter(2, surveyFilter);

        if (!queryDate1.equals("")) {
            q.setParameter(2, dateFilterFrom);
            q.setParameter(3, dateFilterTo);
        }

        commentsCSI = q.getResultList();

        renderArgs.put("commentsCSI", commentsCSI);
        renderArgs.put(RenderExcel.RA_FILENAME, "Comentarios.xlsx");
        RenderExcel renderExcel = new RenderExcel("app/excel/views/ResultsCSI/comments.xlsx");
        renderExcel.render();
    }

    /**
     * Método para descargar los comentarios de los empleados evaluados filtrados por pregunta
     * 
     * @param id de la pregunta
     * @param csiId id del usuario del empleado evaluado
     * 
     * @throws ParsePropertyException
     * @throws InvalidFormatException
     * @throws IOException
     */
    public static void downloadQuestions(Integer id, Integer csiId)
            throws ParsePropertyException, InvalidFormatException, IOException {
        List commentsCSI = null;
        EntityManager em = JPA.em();
        String surveyFilter = session.get(session.getId() + "-filter-encuesta");
        String querySurvey = "";
        String dateFilterFrom = "";
        String dateFilterTo = "";
        String queryDate1 = "";
        String queryDate2 = "";

        if (surveyFilter != null) {
            querySurvey = " and FN_ID_SURVEY = ?2";
        }

        if (session.get(session.getId() + "-filter-date-type") != null) {
            Date[] filterDate = Filters.getDateFilters(session.get(session.getId() + "-filter-date-type"), session);

            dateFilterFrom = DateFormat.getDateInstance().format(filterDate[0]);
            String[] aux = DateFormat.getDateInstance().format(filterDate[1]).split("/");
            int mes = (Integer.valueOf(aux[1]) + 1 > 12) ? 12 : Integer.valueOf(aux[1]) + 1;
            dateFilterTo = "01/" + mes + "/" + aux[2];

            queryDate1 = "inner join PORTALUNICO.PUL_SURVEY on ( PORTALUNICO.PUL_SURVEY.FN_ID_SURVEY = PORTALUNICO.PUL_CSI_EVALUATION_RESULTS.FN_ID_SURVEY)";
            queryDate2 = "and PORTALUNICO.PUL_SURVEY.FD_SURVEY_DATE between ?2 and ?3";
        }

        String queryStr = "select fc_comments from PORTALUNICO.PUL_CSI_EVALUATION_RESULTS " + queryDate1
                + "where FN_ID_EVALUATED_EMP in ("
                + "select FN_ID_EMPLOYEE_CSI from PORTALUNICO.pul_employee_csi start with fn_userid = abs(?1) "
                + "CONNECT BY prior  FN_USERID = FN_MANAGER " + ")" + querySurvey + queryDate2
                + " and fc_comments is not null and FN_ID_QUESTION = ?4";

        Query q = em.createNativeQuery(queryStr);
        q.setParameter(1, csiId);

        if (!querySurvey.equals(""))
            q.setParameter(2, surveyFilter);

        if (!queryDate1.equals("")) {
            q.setParameter(2, dateFilterFrom);
            q.setParameter(3, dateFilterTo);
        }

        q.setParameter(4, id);

        commentsCSI = q.getResultList();

        renderArgs.put("commentsCSI", commentsCSI);
        renderArgs.put(RenderExcel.RA_FILENAME, "Comentarios.xlsx");
        RenderExcel renderExcel = new RenderExcel("app/excel/views/ResultsCSI/comments.xlsx");
        renderExcel.render();
    }
    
    /**
     * Método para descargar los comentarios de los empleados evaluados filtrados por palabra
     * 
     * @param word
     * @param csiId id del usuario del empleado evaluado
     * 
     * @throws ParsePropertyException
     * @throws InvalidFormatException
     * @throws IOException
     */
    public static void downloadWord(String word, Integer csiId)
            throws ParsePropertyException, InvalidFormatException, IOException {

        List commentsCSI = null;
        EntityManager em = JPA.em();
        String surveyFilter = session.get(session.getId() + "-filter-encuesta");
        String querySurvey = "";
        String dateFilterFrom = "";
        String dateFilterTo = "";
        String queryDate1 = "";
        String queryDate2 = "";

        if (surveyFilter != null) {
            querySurvey = " and FN_ID_SURVEY = ?2";
        }

        if (session.get(session.getId() + "-filter-date-type") != null) {
            Date[] filterDate = Filters.getDateFilters(session.get(session.getId() + "-filter-date-type"), session);

            dateFilterFrom = DateFormat.getDateInstance().format(filterDate[0]);
            String[] aux = DateFormat.getDateInstance().format(filterDate[1]).split("/");
            int mes = (Integer.valueOf(aux[1]) + 1 > 12) ? 12 : Integer.valueOf(aux[1]) + 1;
            dateFilterTo = "01/" + mes + "/" + aux[2];

            queryDate1 = "inner join PORTALUNICO.PUL_SURVEY on ( PORTALUNICO.PUL_SURVEY.FN_ID_SURVEY = PORTALUNICO.PUL_CSI_EVALUATION_RESULTS.FN_ID_SURVEY)";
            queryDate2 = "and PORTALUNICO.PUL_SURVEY.FD_SURVEY_DATE between ?2 and ?3";
        }

        String queryStr = "select fc_comments from PORTALUNICO.PUL_CSI_EVALUATION_RESULTS " + queryDate1
                + "where FN_ID_EVALUATED_EMP in ("
                + "select FN_ID_EMPLOYEE_CSI from PORTALUNICO.pul_employee_csi start with fn_userid = abs(?1) "
                + "CONNECT BY prior  FN_USERID = FN_MANAGER" + ")" + querySurvey + queryDate2
                + " and fc_comments like ?4";

        Query q = em.createNativeQuery(queryStr);
        q.setParameter(1, csiId);

        if (!querySurvey.equals(""))
            q.setParameter(2, surveyFilter);

        if (!queryDate1.equals("")) {
            q.setParameter(2, dateFilterFrom);
            q.setParameter(3, dateFilterTo);
        }

        q.setParameter(4, "%" + word + "%");
        commentsCSI = q.getResultList();

        renderArgs.put("commentsCSI", commentsCSI);
        renderArgs.put(RenderExcel.RA_FILENAME, "Comentarios.xlsx");
        RenderExcel renderExcel = new RenderExcel("app/excel/views/ResultsCSI/comments.xlsx");
        renderExcel.render();
    }
}
