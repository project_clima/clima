package controllers.csi;

import java.sql.CallableStatement;
import java.sql.SQLException;
import java.util.Date;

import oracle.jdbc.OracleConnection;
import play.Logger;

public class SnapEvaluationResults {

	/**
	 * Carga los resultados de una encuesta
	 * @param idSurvey
	 */
    public static void loadSnapResults(Integer idSurvey) {
        OracleConnection connection = null;
        CallableStatement statement = null;

        try {
            connection = StructureCSI.getOracleConnection();
            statement = connection.prepareCall("call PORTALUNICO.PR_LOAD_DIM_RESULTADOS(?)");
            statement.setInt(1, idSurvey);
            statement.execute();
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            try {
                if (statement != null && !statement.isClosed()) {
                    statement.close();
                }
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
    }
}
