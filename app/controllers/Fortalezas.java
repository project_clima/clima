package controllers;

import java.sql.SQLException;
import play.mvc.*;
import java.util.*;
import java.util.logging.Level;
import java.util.logging.Logger;
import json.entities.FortalezasList;
import models.*;
import querys.FortalezasQuery;
import utils.RenderExcel;
import net.sf.jxls.exception.ParsePropertyException;
import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import java.io.IOException;
import querys.StructureQuery;

/**
 * Clase controlador para el manejo de las debilidades y fortalezas
 * @author Rodolfo Miranda -- Qualtop
 */
@With(Secure.class)
public class Fortalezas extends Controller {

    /**
     * Metodo inicial que devuelve las fechas de estructura
     */
    public static void index() {
        List structureDates = StructureQuery.getDatesStructures();
        render(structureDates);
    }

    /**
     * MEtodo que devuelve las debilidades o fortalezas segun la fecha seleccionada
     * y el nivel de la jerarquia
     * @param level
     * @param reporte
     * @param structureDate 
     */
    public static void getFortalezas(int level, String reporte, String structureDate) {
        try {
            String order = reporte.equals("debilidades") ? "asc" : "desc";

            List<FortalezasList> fortalezasTree = FortalezasQuery.getFortalezasCoorp(
                level, structureDate, order, "tree"
            );
            
            List<FortalezasList> fortalezasDirect = FortalezasQuery.getFortalezasCoorp(
                level, structureDate, order, "direct"
            );
            
            Object data = new Object[]{fortalezasTree, fortalezasDirect};
            renderJSON(data);
        } catch (SQLException ex) {
            Logger.getLogger(Fortalezas.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     * Metodo que exporta el resultado de la busqueda por usuario seleccionado
     * y fecha de estructura
     * @param level
     * @param reporte
     * @param structureDate
     * @throws ParsePropertyException
     * @throws InvalidFormatException
     * @throws IOException 
     */
    public static void export(int level, String reporte, String structureDate)
                            throws ParsePropertyException, InvalidFormatException, IOException{
        try {
            FortalezasQuery queries = new FortalezasQuery();
            StructureQuery structureQuery = new StructureQuery();
            
            String order = "";
            if (reporte.equals("debilidades")) {
                order = "asc";
            } else {
                order = "desc";
            }

            List<FortalezasList> fortalezas = new ArrayList();
            if (structureQuery.isStore(level, structureDate) == 1) {
                fortalezas = queries.getFortalezasTienda(level, structureDate, order);
            } else{
                fortalezas = queries.getFortalezasCoorp(level, structureDate, order, "tree");
            }

            renderArgs.put("fortalezas", fortalezas);
            renderArgs.put("name", reporte.toUpperCase());
            renderArgs.put(RenderExcel.RA_FILENAME, "Reporte_"+reporte+".xls");
            RenderExcel renderer = new RenderExcel("app/excel/views/Fortalezas/export.xls");
            renderer.render();
        } catch (SQLException ex) {
            Logger.getLogger(Fortalezas.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public static void getDepartment(){
        EmployeeClima employeeClima = EmployeeClima.find("userId = ?", Integer.valueOf(session.get("userId"))).first();
        renderJSON(employeeClima);
    }
}
