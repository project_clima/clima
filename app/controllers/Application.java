package controllers;

import java.util.Calendar;
import play.mvc.*;
import utils.nps.util.FunctionsUtil;

import controllers.csi.ApplicationCSI;
import java.util.Arrays;
import java.util.List;
import models.*;

@With(Secure.class)
public class Application extends Controller {
    public static void index() {
        session.put("place", "environment");
        render();
    }
    
    public static void nps() {
        String home = "";
        if (session.contains("permisos")) {
            String sessionPermisos = session.get("permisos");
            String permisos = sessionPermisos.substring(1, sessionPermisos.length() - 1);
            List<String> aPermisosUsuario = Arrays.asList(permisos.split("\\s*,\\s*"));

            if(aPermisosUsuario.contains("home/user") || 
                    ( !aPermisosUsuario.contains("home/user") && !aPermisosUsuario.contains("home/all"))){
                session.put("idHome",session.get("userId"));
            }
        }
    	session.put("place", "nps");
        session.put("month", FunctionsUtil.getMonth());
        session.put("year", Calendar.getInstance().get(Calendar.YEAR));
        
        render(home);
    }

    public static void csi() {
    	session.put("place", "csi");
    	ApplicationCSI.csi();
    }
}