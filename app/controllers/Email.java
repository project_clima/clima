package controllers;

import play.mvc.*;
import java.util.*;
import javax.mail.MessagingException;
import models.*;
import utils.GmailMailer;
import utils.Message;

@With(Secure.class)
public class Email extends Controller {
    public static void index() {
        render();
    }

    public static void sendMail(String destino, String asunto, String mensaje)
    throws MessagingException {
        GmailMailer mailer = new GmailMailer();

        try{
            mailer.send(destino, asunto, mensaje, 0, null);            
            renderJSON(new Message("El correo se envió con éxito", "", false));
        } catch (Exception e){
            renderJSON(new Message(e.getMessage(), "", true));
        }
    }
}
