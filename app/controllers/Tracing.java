package controllers;

import java.util.*;
import play.mvc.*;
import querys.nps.TrackingReportQuery;

/**
 * Clase principal para el manejo del Reporte De Seguimiento
 * @author Rodolfo Miranda -- Qualtop
 */
@With(Secure.class)
public class Tracing extends Controller {

    /**
     * Metodo principal de despliegue
     */
    public static void index() {
        render();
    }
    
    /**
     * Metodo principal para obtener los datos segun los filtros seleccionados
     * @param level
     * @param userId
     * @param groupId
     * @param businessId
     * @param zoneId
     * @param storeId
     * @param manager
     * @param startDate
     * @param endDate 
     */
    public static void getLevelData(String level, Integer userId, Integer groupId,
                                    Integer businessId, String zoneId, String storeId,
                                    String manager, String startDate, String endDate) {
        String template = "Tracing/" + level + ".html";
        
        List results = TrackingReportQuery.getData(
            userId, groupId, businessId, zoneId, storeId, manager, startDate, endDate
        );
        
        HashMap<String, List> detailsByBusiness = new HashMap();
        
        if (businessId != null && level.equals("business")) {
            detailsByBusiness = TrackingReportQuery.getBusinessDetail(
                null, groupId, businessId, startDate, endDate
            );
        }
        
        renderArgs.put("groupId", groupId);
        renderArgs.put("businessId", businessId);
        renderArgs.put("zoneId", zoneId);
        renderArgs.put("storeId", storeId);
        renderArgs.put("manager", manager);
        renderArgs.put("results", results);
        renderArgs.put("details", detailsByBusiness);
        renderTemplate(template);
    }
}
