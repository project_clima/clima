package controllers;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import play.mvc.*;
import java.util.*;
import json.entities.AutoComplete;
import json.entities.Select2;
import models.*;
import play.Logger;
import querys.StoresQuery;
import querys.StructureQuery;
import utils.Message;
import utils.Pagination;

/**
 * Clase controler para el manejo de las fotos en CLima
 * @author Rodolfo Miranda -- Qualtop
 */
@With(Secure.class)
public class Stores extends Controller {
    
    /**
     * Metodo que regresa las fechas de las estructuras que haya en base
     */
    @Check("stores/index")
    public static void index() {
        List structureDates = StoresQuery.getStructureDates();
        render(structureDates);
    }
    
    /**
     * Metodo que regrea las fechas de las fotos que haya en la base
     */
    public static void getStructureDates() {
        List structureDates = StoresQuery.getStructureDates();
        renderJSON(structureDates);
    }
    
    /**
     * Metodo que regresa las diferentes tiendas dependiendo la fecha de foto
     * @param iDisplayLength
     * @param iDisplayStart
     * @param sEcho
     * @param search
     * @param period 
     */
    public static void list(int iDisplayLength, int iDisplayStart, String sEcho,
                            String search, String period) {
        
        int start = iDisplayStart == 0 ? iDisplayStart : iDisplayStart + 1;
        int end   = iDisplayLength + iDisplayStart;
        
        long count  = StoreEmployee.count(
            "(to_char(idStore) like ?1 or lower(descriptionStore) like lower(?2)) " +
            "and to_char(structureDate, 'mm/yyyy') = ?3",
            "%" + search + "%", "%" + search + "%", period
        );
        
        List stores = StoresQuery.getStores(search, period, start, end);
        
        renderJSON(new Pagination(sEcho, count, count, stores));  
    }
    
    /**
     * MEtodo para agregar una tienda a la foto dependiendo la fecha de estructura
     * @param store
     * @param structureDate
     * @param isEdit
     * @throws ParseException 
     */
    public static void save(StoreEmployee store, String structureDate, Boolean isEdit)
    throws ParseException {
        SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
        String message = "Tienda agregada correctamente";
        
        store.structureDate = sdf.parse("15/" + structureDate);
        
        if (isEdit) {
            message = "Tienda modificada correctamente";            
        } else {
            store.createdDate = new Date();
            store.createdBy   = session.get("username");
        }
        
        if (store.country.equals("MX") && !store.descriptionSubdivision.contains("México")) {
            store.descriptionSubdivision = "México " + store.descriptionSubdivision;
        }
        
        store.modifiedDate = new Date();
        store.modifiedBy   = session.get("username");
        
        store.save();
        
        renderJSON(new Message(message));
    }
    
    /**
     * Elimina una tienda de la foto.
     * @param storeId 
     */
    @Check("stores/delete")
    public static void delete(int storeId) {
        StoreEmployee store = StoreEmployee.findById(storeId);
        long related = EmployeeClima.count(
            "idStore = ?1 and structureDate = ?2",
            store.idStore, store.structureDate
        );
        
        if (related > 0) {
            renderJSON(new Message(
                "La tienda no puede ser eliminada porque tiene empleados relacionados",
                "danger",
                true
            ));
        }
        
        try {
            store.delete();
            renderJSON(new Message("Tienda eliminada correctamente", "success", false));
        } catch (Exception e) {
            renderJSON(new Message("La tienda no puede ser eliminada", "danger", true));
        }
    }
    /**
     * Metodo que regresa los empleados que pertenecen a la tienda seleccionada
     * @param storeId
     * @param hrName
     * @param shName
     * @param otherName 
     */
    public static void form(int storeId, String hrName, String shName, String otherName) {
        StoreEmployee store = StoreEmployee.findById(storeId);
        List structureDates = StoresQuery.getStructureDates();
        List<StoreEmployee> storesSubDiv = StoreEmployee.
                find("select distinct subDivision ,descriptionSubdivision "
                        + "from StoreEmployee").fetch();
        
        render(storeId == 0 ? null : store, structureDates,storesSubDiv);
    }
    
    /**
     * Metodo que devuelve la busqueda dependiendo la fecha de estructura
     * @param search
     * @param structurePeriod 
     */
    public static void getEmployees(String search, String structurePeriod) {
        List<EmployeeClima> employees = EmployeeClima.find(
            "(to_char(employeeNumber) like ?1 or (lower(firstName || lastName) like lower(?2))) " + 
            "and to_char(structureDate, 'mm/yyyy') = ?3",
            "%" + search +  "%", "%" + search +  "%", structurePeriod
        ).fetch(50);
        
        Select2 selct2 = new Select2();
        
        for (EmployeeClima employee : employees) {
            String employeeName = employee.firstName + " " + employee.lastName;
            selct2.items.add(new AutoComplete(employee.userId, employeeName));
        }
        
        selct2.total_count = selct2.items.size();
        renderJSON(selct2);
    }
    
    /**
     * Metodo para verificar el id de empleado
     * @param id
     * @param originalId
     * @param structureDate 
     */
    public static void verifyId(int id, int originalId, String structureDate) {
        if (id != originalId) {
            StoreEmployee store = StoreEmployee.find(
                "idStore = ?1 and to_char(structureDate, 'mm/yyyy') = ?2",
                id, structureDate 
            ).first();
            renderJSON(store == null);
        }
        
        renderJSON(true);
    }
    
    /**
     * Metodo que copia la foto dependiendo la fecha de estructura
     * @param sourcePeriod
     * @param destPeriod 
     */
    public static void copy(String sourcePeriod, String destPeriod) {
        if (sourcePeriod.equals(destPeriod)) {
            response.status = 422;
            renderJSON(new Message("El periodo destino no puede ser igual al periodo destino."));
        }
        
        List<StoreEmployee> stores = StoreEmployee.find(
            "to_char(structureDate, 'mm/yyyy') = ?1", sourcePeriod
        ).fetch();
        
        try {
            for (StoreEmployee store : stores) {
                if (!existInPeriod(store.idStore, destPeriod)) {
                    copyStore(store, destPeriod);
                }
            }
            
            renderJSON(new Message("Tiendas copiadas exitosamente."));
        } catch (ParseException e) {
            Logger.error(e.getMessage());
        }
        
        renderJSON(new Message("Las tienda no fueron copiadas.", "warning", true));
    }
    
    /**
     * Metodo que copia las tiendas dependiendo la fecha de la foto
     * @param originalStore
     * @param destPeriod
     * @throws ParseException 
     */
    private static void copyStore(StoreEmployee originalStore, String destPeriod)
    throws ParseException {
        SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
        Date period = sdf.parse("15/" + destPeriod);
        
        StoreEmployee store = new StoreEmployee();
        
        store.idStore                = originalStore.idStore;
        store.descriptionStore       = originalStore.descriptionStore;
        store.subDivision            = originalStore.subDivision;
        store.country                = originalStore.country;
        store.descriptionSubdivision = originalStore.descriptionSubdivision;
        store.humanResources         = getRelatedEmployee(originalStore.humanResources, destPeriod);
        store.other                  = getRelatedEmployee(originalStore.other, destPeriod);
        store.storeHead              = getRelatedEmployee(originalStore.storeHead, destPeriod);
        store.modifiedBy             = session.get("username");
        store.createdBy              = session.get("username");
        store.createdDate            = new Date();
        store.modifiedDate           = new Date();
        store.zoneNps                = originalStore.zoneNps;
        store.groupId                = originalStore.groupId;
        store.busId                  = originalStore.busId;
        store.structureDate          = period;
        
        store.save();
    }
    
    private static boolean existInPeriod(Integer storeId, String period) {
        StoreEmployee store = StoreEmployee.find(
            "idStore = ?1 and to_char(structureDate, 'mm/yyyy') = ?2",
            storeId, period
        ).first();
        
        return store != null;
    }
    
    private static Integer getRelatedEmployee(Integer userId, String period) {
        if (userId == null) {
            return null;
        }
        
        EmployeeClima employee = EmployeeClima.find(
            "userId = ?1 and to_char(structureDate, 'mm/yyyy') = ?2",
            userId, period
        ).first();
        
        if (employee == null) {
            return null;
        }
        
        return employee.userId;
    }
}