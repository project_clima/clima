package controllers;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import java.io.IOException;
import java.lang.reflect.Type;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.*;
import java.util.logging.Level;
import java.util.logging.Logger;
import models.*;
import net.sf.jxls.exception.ParsePropertyException;
import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import play.mvc.*;
import play.mvc.results.Result;
import querys.HorizontalAnalysisQuery;
import querys.StructureQuery;
import utils.Pagination;
import utils.PagingResult;
import utils.RenderExcel;

/**
 * Clase controlador para el manejo del analisis horizontal
 * @author Rodolfo Miranda -- Qualtop
 */
@With(Secure.class)
public class HorizontalAnalysis extends Controller {

    /**
     * Metodo de inicio /horizontalAnalysis
     */
    public static void index() {
        List structureDates = StructureQuery.getDatesStructures();
        List zones = HorizontalAnalysisQuery.getComboRegion();
        render(structureDates, zones);
    }
    
    /**
     * Metodo que obtiene el analisis horizontal general
     * @param date 
     */
    public static void getDefaultTable(String date) {
        List elements = HorizontalAnalysisQuery.getDefaultTable(date);
        renderJSON(elements);
    }
    
    /**
     * 
     * @param date
     * @param zone
     * @param position 
     */
    public static void getTable(String date, String zone, String position) {
        zone     = zone != null && !zone.equals("-1") ? zone : "";
        position = position != null && !position.equals("-1") ? position : "";
        
        List elements = HorizontalAnalysisQuery.getAnalysisTable(date, zone, position);
        renderJSON(elements);
    }

    /**
     * Clasificacion por gerente-tienda
     * @param date 
     */
    public static void getByStoreManager(String date) {
        HorizontalAnalysisQuery queries = new HorizontalAnalysisQuery();

        List elements = queries.getByStoreManager(date);
        renderJSON(elements);
    }

    /**
     * 
     */
    public static void getByOperationsManager() {
        HorizontalAnalysisQuery queries = new HorizontalAnalysisQuery();

        List elements = queries.getByOperationsManager();
        renderJSON(elements);
    }

    /**
     * Obtener el director por direccion
     * @param area
     * @param text
     * @param date 
     */
    public static void getByDirectorOrDireccion(String area, String text, String date) {
        HorizontalAnalysisQuery queries = new HorizontalAnalysisQuery();

        List elements = queries.getByDirectorOrDireccion(area, text, date);
        renderJSON(elements);
    }

    /**
     * 
     */
    public static void getComboRegion() {
        HorizontalAnalysisQuery queries = new HorizontalAnalysisQuery();

        List elements = HorizontalAnalysisQuery.getComboRegion();
        renderJSON(elements);
    }

    /**
     * Obtener los gerentes por fecha de estructura
     * @param date 
     */
    public static void getByManagement(String date) {
        HorizontalAnalysisQuery queries = new HorizontalAnalysisQuery();

        List elements = queries.getByManagement(date);
        renderJSON(elements);
    }

    /**
     * Obtener las ubicaciones
     * @param date 
     */
    public static void getTableMalls(String date) {
        HorizontalAnalysisQuery queries = new HorizontalAnalysisQuery();

        List elements = queries.getTableMalls(date);
        renderJSON(elements);
    }
    

    /**
     * Obtener los jefes
     * @param par1
     * @param par2 
     */
    public static void getByBoss(String par1, String par2) {
        HorizontalAnalysisQuery queries = new HorizontalAnalysisQuery();

        List elements = queries.getByBoss(par1, par2);
        renderJSON(elements);
    }

    /**
     * 
     * @param zone
     * @param date 
     */
    public static void getBossTable(String zone, String date) {
        HorizontalAnalysisQuery queries = new HorizontalAnalysisQuery();

        List elements = queries.getBossTable(zone, date);
        renderJSON(elements);
    }

    /**
     * 
     * @param area
     * @param date 
     */
    public static void getTableOperationManager(String area, String date) {
        HorizontalAnalysisQuery queries = new HorizontalAnalysisQuery();

        List elements = queries.getTableOperationManager(area, date);
        renderJSON(elements);
    }

    /**
     * 
     * @param area
     * @param date 
     */
    public static void getTableOperationManagerAll(String area, String date) {
        List elements = HorizontalAnalysisQuery.getTableOperationManagerAll(area, date);
        renderJSON(elements);
    }

    /**
     * 
     * @param area 
     */
    public static void getTableByArea(String area) {
        HorizontalAnalysisQuery queries = new HorizontalAnalysisQuery();

        List elements = queries.getTableByArea(area);
        renderJSON(elements);
    }
}
