package controllers;

import play.mvc.*;
import java.util.*;
import models.*;
import play.Logger;
import querys.SnapshotQuery;
import querys.StoresQuery;
import utils.Message;
import utils.Pagination;

/**
 * Clase controlador para el manejo de las fotos en Clima
 * @author Rodolfo Miranda -- Qualtop
 */
@With(Secure.class)
public class StructureSnapshots extends Controller {
    
    /**
     * Clase que devuelve las fechas de las fotos existentes
     */
    public static void index() {
        List structureDates = StoresQuery.getStructureDates();
        
        List snapshots = EmployeeClima.find(
            "select distinct to_char(structureDate, 'mm/yyyy') from EmployeeClima " +
            "order by to_char(structureDate, 'mm/yyyy') desc"
        ).fetch();
        
        render(snapshots, structureDates);
    }
    
    /**
     * Metodo que devuelve las fechas de estructura por periodo seleccionado
     * @param iDisplayLength
     * @param startDate
     * @param endDate
     * @param iDisplayStart
     * @param sEcho
     * @param sSortDir_0
     * @param iSortCol_0
     * @param sSearch 
     */
    public static void list(int iDisplayLength, String startDate,
                              String endDate, int iDisplayStart, String sEcho,
                              String sSortDir_0, int iSortCol_0, String sSearch) {
        
        List surveys = EmployeeClima.find(
            "select distinct to_char(structureDate, 'mm/yyyy') from EmployeeClima " +
            "order by to_char(structureDate, 'mm/yyyy') desc"
        ).fetch();        
        
        renderJSON(new Pagination(sEcho, 0, 0, surveys));
    }
    
    /**
     * Metodo que crea una nueva fecha de foto
     * @param period
     * @param originalPeriod 
     */
    public static void create(String period, String originalPeriod) {
        EmployeeClima snapshot = EmployeeClima.find(
            "to_char(structureDate, 'mm/yyyy') = ?1", period
        ).first();
        
        if (snapshot != null && (originalPeriod == null || originalPeriod.equals(""))) {
            renderJSON(new Message(null, "exists", false));
        }
        
        if (snapshot != null && !(originalPeriod == null || originalPeriod.equals(""))) {
            response.status = 422;
            renderJSON(new Message(null, "exists", false));
        }
        
        if (originalPeriod == null || originalPeriod.equals("")) {
            createSnapshot(period);
        } else {
            updateSnapshot(period, originalPeriod);
        }        
    }
    
    /**
     * Metodo que reemplada una estructura
     * @param period 
     */
    public static void replace(String period) {       
        createSnapshot(period);
    }
    
    /**
     * Metodo que elimina una foto
     * @param period 
     */
    public static void delete(String period) {
        int employees = SnapshotQuery.getEmployeesNumber(period);
        
        if (employees > 0) {
            response.status = 400;
            renderJSON(new Message(null, null, true));
        }
        
        EmployeeClima.delete("to_char(structureDate, 'mm/yyyy') = ?1", period);
        response.status = 204;
    }
    
    /**
     * Metodo que crea la foto en base de datos dependiendo el periodo seleccionado
     * @param period 
     */
    private static void createSnapshot(String period) {
        String date = "15/" + period;
        Logger.info("Foto: proceso iniciado ...");
        int result = SnapshotQuery.createSnapshot(date, session.get("username"));
        
        if (result == 0) {
            response.status = 500;
            renderJSON(new Message(null, null, true));
        }
        
        renderJSON(new Message(null, null, false));
    }
    
    /**
     * Metodo para la actualizacion de una foto existente
     * @param newPeriod
     * @param originalPeriod 
     */
    private static void updateSnapshot(String newPeriod, String originalPeriod) {
        int employees = SnapshotQuery.getEmployeesNumber(originalPeriod);
        
        if (employees > 0) {
            response.status = 400;
            renderJSON(new Message(null, null, true));
        }
        
        String date = "15/" + newPeriod;
        
        int result = SnapshotQuery.updateSnapshot(date, originalPeriod, session.get("username"));
        
        if (result == 0) {
            response.status = 500;
            renderJSON(new Message(null, null, true));
        }
        
        renderJSON(new Message(null, null, false));
    }
}