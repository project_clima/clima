package controllers;

import play.*;
import play.mvc.*;
import java.util.*;
import models.*;
import play.db.jpa.JPA;
import utils.Pagination;
import play.templates.Template;
import play.templates.TemplateLoader;

@With(Secure.class)
public class EmailCSI extends SendSurvey {
    public static void emailTemplate() {
    	String place = "CSI";

    	render(place);
    }

    /**
     * Muestra el template para editar el email
     * @param templateId
     */
    public static void edit(int templateId) {
        SurveyType type = SurveyType.findById(5);
        EmailTemplate template = EmailTemplate.find("id = ?1", templateId).first();
        
        if (template == null) {
            template = new EmailTemplate();
        }
        
        String place = "CSI";
        render(template, type, templateId, place);
    }

    /**
     * Muestra un preview del template
     * @param template
     * @param templateId
     */
    public static void preview(EmailTemplate template, int templateId) {                
        template = (template.type == null) ? (EmailTemplate) EmailTemplate.findById(templateId) : template;
        int surveyTypeId = template.type.id;
        
        HashMap<String, Object> vars = new HashMap();
        Template templateList = TemplateLoader.load("EmailTemplates/employee-list-sample.html");
        
        vars.put("%collaborator.name%", "Julieta");
        vars.put("%collaborator.fullName%", "Julieta Ornelas");
        vars.put("%survey.name%", "Encuesta CSI");
        vars.put("%survey.start%", "01-01-2016");
        vars.put("%survey.end%", "01-01-2017");
        vars.put("%survey.link%", "http://www.liverpool.com.mx/tienda/home.jsp");
        vars.put("%usersTable%", templateList.render());
        vars.put("%store%", "Fábricas de Francia CDMX");
        
        if (template.body != null) {
            for (String key : vars.keySet()) {
                template.body = template.body.replace(key, vars.get(key).toString());
            }
        }
        
        render(template, surveyTypeId);
    }

    /**
     * Obtiene una lista de templates de email paginados
     * @param iDisplayLength
     * @param startDate
     * @param endDate
     * @param iDisplayStart
     * @param sEcho
     * @param sSortDir_0
     * @param iSortCol_0
     */
    public static void list(int iDisplayLength, String startDate,
    String endDate, int iDisplayStart, String sEcho,
    String sSortDir_0, int iSortCol_0) {
        
        int start  = iDisplayStart == 0 ? iDisplayStart : iDisplayStart + 1;
        int end    = iDisplayLength + iDisplayStart;
        long count = EmailTemplate.count("type = 5");
        List types = EmailTemplate.find(
            "select t.subject, 'Encuesta CSI', t.id from EmailTemplate t where t.type = 5"
        ).from(start).fetch(end);
        
        renderJSON(new Pagination(sEcho, count, count, types));
    }

    /**
     * Muestra el email que será enviado
     */
    public static void emailSending() {
        String place = "CSI";
        
        List<Object> surveys = Survey.find(
            "surveyType.id = ?1 and sysdate between dateInitial and dateEnd + 1 " + 
            "order by dateInitial desc", 5
        ).fetch();
        
        List<EmailTemplate> templates = EmailTemplate.find(
            "type.id = ? order by id desc", 5
        ).fetch(20);

        renderTemplate("EmailCSI/index.html", surveys, templates, place);
    }

    /**
     * Obtiene una lista de encuestas en las que la fecha actual esté dentro de su rango
     */
    public static void emailMonitor() {
        List<Object> surveys = Survey.find(
            "surveyType.id = ?1 and sysdate between dateInitial and dateEnd + 1 " + 
            "order by dateInitial desc", 5
        ).fetch();
        
        String place = "CSI";

        render(surveys, place);
    }
}
