package controllers;

import play.mvc.*;
import java.util.*;
import models.*;
import json.entities.AutoComplete;
import json.entities.Select2;
import play.libs.F;

@With(Secure.class)
public class SendSurveyCSI extends Controller {
    /**
     * Obtiene los empleados evaluadores de una encuesta
     * @param surveyId
     * @param search
     */
    public static void getSurveyEmployees(int surveyId, String search) {        
        StringBuilder query = new StringBuilder();
        
        query.append("select employee")
            .append(" from EmployeeCSI employee")
            .append(" where employee.id IN ")
            .append(" (select distinct idEvaluatorEmployee from CsiEvaluationCross where idSurvey = ?1)")
            .append(" and (lower(employee.firstName) like lower(?2) or lower(employee.lastName) like lower(?3))")
            .append(" order by employee.firstName, employee.lastName asc");
        
        List<EmployeeCSI> evaluators = EmployeeCSI.find(
            query.toString(),
            surveyId, "%" + search +  "%", "%" + search +  "%"
        ).fetch(50);
        
        Select2 selct2 = new Select2();
        
        for (EmployeeCSI evaluator : evaluators) {
            String employeeName = evaluator.firstName + " " + evaluator.lastName;
            selct2.items.add(new AutoComplete(evaluator.id, employeeName));
        }
        
        selct2.total_count = selct2.items.size();
        renderJSON(selct2);
    }
    
    /**
     * Envía el email a los empleados seleccionados
     * @param surveyId
     * @param templateId
     * @param employees
     */
    public static void sendEmail(int surveyId, int templateId, String employees) {        
        async.SendEmailCSI sendEmailJob = new async.SendEmailCSI(
            surveyId, templateId, employees, request
        );
        
        F.Promise<String> sendJob = sendEmailJob.now();        
        String result = await(sendJob);
        
        renderJSON(new utils.Message(result, "", false));
    }
}
