package controllers;
import java.util.List;

import models.csi.helpers.CsiSurveyHelper;
import play.mvc.Controller;
import play.mvc.With;


@Deprecated()
@With(Secure.class)
public class CSI extends Controller {

    public static void results() {
        render();
    }
 // Se copia al controlador SurveyCSI a menos que se utilice 
    public static void newSurvey() {
    	List<Integer> years = CsiSurveyHelper.getYearsRange();
    	render(years);
    }

    public static void details() {
        render();
    }

    public static void emailTemplate() {
        render();
    }

    public static void emailMonitor() {
        render();
    }

    public static void emailSending() {
        render();
    }

    public static void detailsRow(String direction) {
        render(direction);
    }
    

    
    
    
    
    
    
}
