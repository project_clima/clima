package controllers;

import play.mvc.*;
import java.util.*;
import models.*;
import com.google.gson.Gson;
import java.io.IOException;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;
import net.sf.jxls.exception.ParsePropertyException;
import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import utils.PagingResult;
import utils.Pagination;
import querys.StructureQuery;
import utils.RenderExcel;
import utils.Message;

/**
 * Clase controlador para el manejo de la estructura en Clima
 * @author Rodolfo Miranda -- Qualtop
 */
@With(Secure.class)
public class Structure extends Controller {
    /**
     * MEtodo principal
     */
    public static void index() {
        render();
    }

    public static void getStructure(){
        render();
    }

    /**
     * Metodo que regresa las fechas de estructura.
     */
    public static void getDatesStructures(){
        StructureQuery queries = new StructureQuery();

        List datesStructure = queries.getDatesStructures();
        renderJSON(datesStructure);
    }

    /**
     * Metodo que regresa la lista de usuarios por fecha de estructura
     * @param userName
     * @param userNumber
     * @param location
     * @param department
     * @param iDisplayLength
     * @param level1
     * @param level2
     * @param level3
     * @param level4
     * @param level5
     * @param level6
     * @param level7
     * @param level8
     * @param iDisplayStart
     * @param sEcho
     * @param sSortDir_0
     * @param iSortCol_0
     * @param structureDate 
     */
    public static void getStructureData(
    String userName, Long userNumber, String location, String department,
    int iDisplayLength, String level1, String level2, String level3, String level4,
    String level5, String level6, String level7, String level8, int iDisplayStart,
    String sEcho, String sSortDir_0, int iSortCol_0, String structureDate) {


        Pagination pagination = null;
        String condition = "";
        int conditions = 0;

        StructureQuery queries = new StructureQuery();
        String order = queries.getOrder(sSortDir_0, iSortCol_0);

        int start = iDisplayStart == 0 ? iDisplayStart : iDisplayStart + 1;
        int end = iDisplayLength + iDisplayStart;

        PagingResult oPR = null;


        if ((location != null && !location.equals("-1")) || (department != null && !department.equals("-1")) ) {
            if (location != null && !location.equals("-1")) {
                condition = " FC_DESC_LOCATION = '" + location + "'";
                conditions ++;
            }

            if (department != null && !department.equals("-1")) {
                if (conditions > 0) {
                    condition += " AND ";
                }
                condition = " FC_DEPARTMENT = '" + department + "'";
                conditions ++;
            }

            oPR= queries.getStructure(condition, order, start, end, structureDate);
            pagination = new Pagination(sEcho, oPR.totalRecords, oPR.totalRecords, oPR.data);

        } else{
            if (userName != null && userName.trim().length() > 0) {
                condition += " FN_USERID in ( select FN_ID_USER from " + 
                            " PUL_USER " + 
                            " where FC_USERNAME like '%" + userName.toUpperCase() + "%' "+")";
                conditions ++;
            }

            if (userNumber != null) {
                if (conditions > 0) {
                    condition += " AND ";
                }
                condition += " FN_EMPLOYEE_NUMBER = " + userNumber;
            }

            String searchLevel = "";

            if (level1 != null && !level1.equals("-1")) {
                searchLevel = level1;
            }
            if (level2 != null && !level2.equals("-1")) {
                searchLevel = level2;
            }
            if (level3 != null && !level3.equals("-1")) {
                searchLevel = level3;
            }
            if (level4 != null && !level4.equals("-1")) {
                searchLevel = level4;
            }
            if (level5 != null && !level5.equals("-1")) {
                searchLevel = level5;
            }
            if (level6 != null && !level6.equals("-1")) {
                searchLevel = level6;
            }
            if (level7 != null && !level7.equals("-1")) {
                searchLevel = level7;
            }
            if (level8 != null && !level8.equals("-1")) {
                searchLevel = level8;
            }

            if (!searchLevel.equals("")) {
                condition = " FN_USERID = '" + searchLevel + "'";
            }

            if (condition.equals("")) {
                condition = " FN_LEVEL = 1";
            }

            oPR= queries.getStructure(condition, order, start, end, structureDate);
            pagination = new Pagination(sEcho, oPR.totalRecords, oPR.totalRecords, oPR.data);
        }


        renderJSON(pagination);
    }

    /**
     * Metodo que regresa las diferentes localidades por fecha de estructura
     * @param date 
     */
    public static void getLocation(String date) {
        StructureQuery queries = new StructureQuery();

        List locations = queries.getLocation(date);
        renderJSON(locations);
    }

    /**
     * Metodo que regresa los departamentos por fecha de estructura
     * @param location
     * @param date 
     */
    public static void getDepartments(String location, String date) {
        StructureQuery queries = new StructureQuery();

        List departments = queries.getDepartments(location, date);
        renderJSON(departments);
    }

    /**
     * Metodo que regresa el primer nivel de los filtros.
     * @param level
     * @param date 
     */
    public static void getLevels1(int level, String date){
        List levels = StructureQuery.getDepartmentsByLevel1(level, date);
        renderJSON(levels);
    }

    /**
     * Metodo generico que regresa los siguientes niveles en los filtros
     * @param department
     * @param date 
     */
    public static void getLevels(int department, String date) {
        StructureQuery queries = new StructureQuery();

        List levels = queries.getDepartmentsByLevel(department, date, false);
        renderJSON(levels);
    }

    /**
     * Metodo que proporciona informacion para el modal
     * @param userId 
     */
    public static void getInfoModal(Long userId) {
        StructureQuery queries = new StructureQuery();

        List info = queries.getInfoModal(userId);
        renderJSON(info);
    }

    /**
     * Metodo que proporciona si un empleado pertenece a una tienda
     * @param userId
     * @param structureDate 
     */
    public static void isStore(Long userId, String structureDate) {
        try {
            EmployeeClima employee = EmployeeClima.find("employeeNumber = ?1 and "
                    + "to_char(structureDate, 'MM/YYYY') = ?2", userId, structureDate).first();
            StructureQuery queries = new StructureQuery();

            int isStore = queries.isStore(employee.userId, structureDate);
            renderJSON(isStore);
        } catch (SQLException ex) {
            Logger.getLogger(Structure.class.getName()).log(Level.SEVERE, null, ex);
        }
    }


    /**
     * MEtodo que regresa los gerentes por empleado seleccionado
     * @param managerId
     * @param managerName
     * @param employeeNumber 
     */
    public static void validateManager(Long managerId, String managerName, Long employeeNumber) {
        StructureQuery queries = new StructureQuery();
        if (managerId == null) {
            managerId = 0L;
        }
        EmployeeClima employee = EmployeeClima.find("employeeNumber = ?1", employeeNumber).first();
        List managers = queries.getManagers(managerId, managerName.toUpperCase(), employee.userId);

        renderJSON(managers);
    }

    /**
     * Metodo que cambia el gerente en la base de datos
     * @param userId
     * @param newManagerId
     * @param structureDate 
     */
    public static void changeManager(Long userId, Long newManagerId, String structureDate) {
        try {
            int update = 0;
            
            EmployeeClima manager  = EmployeeClima.find("employeeNumber = ?1 "
                    + "AND TO_CHAR(structureDate,'MM/YYYY') = ?2", newManagerId,
                    structureDate).first();
            EmployeeClima employee = EmployeeClima.find("employeeNumber = ?1 "
                    + "AND TO_CHAR(structureDate,'MM/YYYY') = ?2", userId,
                    structureDate).first();
            
            if (manager.manager.equals(employee.userId)) {
                renderJSON(new Message("El nuevo jefe directo no puede ser asignado (error de redundancia)", null, true));
            }
            
            if (userId == newManagerId) {
                renderJSON(new Message("El jefe directo no puede ser el mismo empleado", null, true));
            }
            
            update = StructureQuery.setManager(employee.userId, manager.userId,
                    session.get("username"),structureDate);
            
            if (update == 0) {
                renderJSON(new Message("El jefe directo no pudo ser asignado", null, true));
            }
            
            renderJSON(new Message(null, null, false));
        } catch (SQLException ex) {
            Logger.getLogger(Structure.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     * MEtodo que regresa la lista de funciones existentes en base de datos
     */
    public static void getFunction() {
        StructureQuery queries = new StructureQuery();

        List functions = queries.getFunctions();
        renderJSON(functions);
    }

    /**
     * Metodo que cambia la funcion por usuario seleccionado
     * @param userId
     * @param newFunction 
     */
    public static void changeFunction(Long userId, String newFunction) {
        StructureQuery queries = new StructureQuery();
        int update = 0;
        update = queries.setFunction(userId, newFunction, session.get("username"));
        renderJSON(update);
    }

    /**
     * Metodo que proporciona el empleado en tabla por medio de su username
     * @param userName
     * @return 
     */
    public static int getUserIdByUsername(String userName){
        Usuario user = Usuario.find("username = ?!", userName).first();
        return Integer.getInteger(user.id.toString());
    }

    /**
     * Metodo que cambia el plan de accion
     * @param email
     * @param plan
     * @param idEmployee
     * @param idSurvey
     * @param date 
     */
    public static void setPlanEmail(String email, int plan, Long idEmployee, 
            int idSurvey, String date) {        
        renderJSON(StructureQuery.setPlanEmail(email, plan, idEmployee,
                session.get("username"), idSurvey, date));
    }

    /**
     * Metodo que recalcula el nivel jerarquico por estructura seleccionada
     * @param structureDate
     * @return 
     */
    public static int recalcLevel(String structureDate) {
        int result = 0;
        try {
            StructureQuery queries = new StructureQuery();
            result = queries.recalcLevel(structureDate);
        } catch (SQLException ex) {
            Logger.getLogger(Structure.class.getName()).log(Level.SEVERE, null, ex);
        }
        return result;
    }

    /**
     * Metodo para la exportacion de los usuarios en base de datos
     * @param structureDate
     * @param userName
     * @param userNumber
     * @param location
     * @param department
     * @param level1
     * @param level2
     * @param level3
     * @param level4
     * @param level5
     * @param level6
     * @param level7
     * @param level8
     * @throws ParsePropertyException
     * @throws InvalidFormatException
     * @throws IOException 
     */
    public static void export(String structureDate, String userName, Long userNumber, 
                            String location, String department, String level1, String level2,
                            String level3, String level4, String level5, String level6,
                            String level7,String level8)
                            throws ParsePropertyException, InvalidFormatException, IOException{

        String condition = "";
        int conditions = 0;

        StructureQuery queries = new StructureQuery();

        List oPR = null;


        if ((location != null && !location.equals("-1")) || (department != null && !department.equals("-1")) ) {
           if (location != null && !location.equals("-1")) {
               condition = " FC_DESC_LOCATION = '" + location + "'";
               conditions ++;
           }

           if (department != null && !department.equals("-1")) {
               if (conditions > 0) {
                   condition += " AND ";
               }
               condition = " FC_TITLE = '" + department + "'";
               conditions ++;
           }

           oPR= queries.getExport(condition, structureDate);

        } else{
            if (userName != null && userName.trim().length() > 0) {
               condition += " FN_USERID in ( select FN_ID_USER from " + 
                            " PUL_USER " + 
                            " where FC_USERNAME like '%" + userName.toUpperCase() + "%' " +")";
               conditions ++;
            }

            if (userNumber != null) {
               if (conditions > 0) {
                   condition += " AND ";
               }
               condition += " FN_EMPLOYEE_NUMBER = " + userNumber;
            }

            String searchLevel = "";

            if (level1 != null && !level1.equals("-1")) {
               searchLevel = level1;
            }
            if (level2 != null && !level2.equals("-1")) {
               searchLevel = level2;
            }
            if (level3 != null && !level3.equals("-1")) {
               searchLevel = level3;
            }
            if (level4 != null && !level4.equals("-1")) {
               searchLevel = level4;
            }
            if (level5 != null && !level5.equals("-1")) {
               searchLevel = level5;
            }
            if (level6 != null && !level6.equals("-1")) {
               searchLevel = level6;
            }
            if (level7 != null && !level7.equals("-1")) {
               searchLevel = level7;
            }
            if (level8 != null && !level8.equals("-1")) {
               searchLevel = level8;
            }

            if (!searchLevel.equals("")) {
               condition = " FN_USERID = '" + searchLevel + "'";
            }

            if (condition.equals("")) {
                condition = " FN_LEVEL = 1";
            }

            oPR= queries.getExport(condition, structureDate);
        }
        renderArgs.put("structures", oPR);
        renderArgs.put(RenderExcel.RA_FILENAME, "ReporteEstructura.xlsx");
        RenderExcel renderer = new RenderExcel("app/excel/views/Structure/structure.xlsx");
        renderer.render();
    }

    public static void getSurveys(int userId) {
        
    }
}
