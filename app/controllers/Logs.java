package controllers;

import play.data.validation.*;
import play.mvc.*;
import models.*;
import utils.PagingResult;
import utils.Pagination;

@With(Secure.class)
public class Logs extends Controller {
    @Check("logs/list")
    public static void list() {
        render();
    }
    
    public static void getLogs(String user, int iDisplayLength, String startDate,
                               String endDate, int iDisplayStart, String sEcho,
                               String sSortDir_0, int iSortCol_0) {
        
        String condition = user != null && user.trim().length() > 0
                                ? "LOGUSR LIKE '%" + user + "%'"
                                : "LOGUSR = LOGUSR";
        String dateCondition = "";
        
        if (startDate.trim().length() > 0 && endDate.trim().length() > 0) {
            dateCondition = " AND DATE(LOGUPD) BETWEEN DATE('" + startDate + "') AND DATE('" + endDate + "')";
            condition += dateCondition;
        }
        
        String order = getOrder(sSortDir_0, iSortCol_0);
        
        int start = iDisplayStart == 0 ? iDisplayStart : iDisplayStart + 1;
        PagingResult oPR = utils.Queries.getLogs(condition, order, start, iDisplayLength + iDisplayStart);
        Pagination oPagination = new Pagination(sEcho, oPR.totalRecords, oPR.totalRecords, oPR.data);
        
        renderJSON(oPagination);        
    }
    
    public static String getOrder(String sSortDir_0, int iSortCol_0) {
        String order;
        
        switch (iSortCol_0) {
            case 0 : order = "ORDER BY LOGUSR " + sSortDir_0;
                break;            
            case 1 : order = "ORDER BY LOGDES " + sSortDir_0;
                break;
            case 2 : order = "ORDER BY LOGUPD " + sSortDir_0;
                break;
            default : order = "ORDER BY LOGUSR " + sSortDir_0;
                break;
        }
        
        return order;
    }
}
