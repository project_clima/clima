package controllers;

import com.google.gson.Gson;
import java.io.IOException;
import java.math.RoundingMode;
import java.text.DecimalFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import play.mvc.*;
import java.util.*;
import javax.persistence.Query;
import json.entities.TreeData;
import models.*;
import net.sf.jxls.exception.ParsePropertyException;
import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import play.Logger;
import play.data.validation.Valid;
import play.data.validation.Validation;
import play.db.jpa.JPA;
import utils.Message;
import utils.Pagination;
import utils.RenderExcel;

@With(Secure.class)
public class Questions extends Controller {    
    public static void getTotalWeighing(int surveyId, int questionWeighing) {
        Object sum = Question.find(
            "select sum(weighing) as weighing from Question where survey.id = ?1",
            surveyId
        ).first();
        
        int surveyWeighing = sum == null ? 0 : Integer.parseInt(sum.toString());
        
        renderJSON((surveyWeighing + questionWeighing) <= 100);
    }
    
    public static void saveQuestion(@Valid Question question, List<QuestionChoices> choices, Integer oldSubfactor) {       
        if(validation.hasErrors()) {
            renderJSON(
                new utils.Message(
                    "Error al agregar la pregunta. Por favor, revise que la información sea válida.",
                    null,
                    true
                )
            );
        }
        
        if (question.id == null) {
            question.creationDate = new Date();
            question.createdBy    = session.get("username");
        }
        
        question.modificationDate = new Date();
        question.modifiedBy       = Usuario.find("username = ?1", session.get("username")).first();

        question.save();        
        saveChoices(question, choices);
        
        Integer  newSubfactor = question.subFactor != null ? question.subFactor.id : null;
        
        if (!Objects.equals(oldSubfactor, newSubfactor))
        {
            querys.SurveysGeneratorQuery.calculatePercentageFactors(question.survey.id);
        }        
        
        renderJSON(new utils.Message(null, null, false));
    }
    
    private static void saveChoices(Question question, List<QuestionChoices> choices) {
        QuestionChoices.delete("question.id = ?1", question.id);
        
        if (choices == null) {
            return;
        }
        
        String questionType = question.questionType.name;
        
        int inversedIndex = choices.size();
        
        for (QuestionChoices choice : choices) {
            if (choice == null) {
                continue;
            }
            
            choice.choiceValue  = questionType.equals("CARAS") ?
                                    Integer.toString(inversedIndex--) : choice.choiceText;
            
            choice.question     = Question.findById(question.id);
            choice.createdBy    = session.get("username");
            choice.creationDate = new Date();
            choice.modifiedBy   = session.get("username");
            choice.modificationDate = new Date();
            choice.save();
        }
    }
    
    public static void listQuestions(int surveyId, int iDisplayLength, String startDate,
                              String endDate, int iDisplayStart, String sEcho,
                              String sSortDir_0, int iSortCol_0, String sSearch) {
        
        int start  = iDisplayStart;
        int end    = iDisplayLength;
        long count = Question.count(
            "survey.id = ?1 AND (LOWER(question) LIKE LOWER(?2) or LOWER(questionType.name) LIKE LOWER(?3))",
            surveyId, "%" + sSearch + "%", "%" + sSearch + "%"
        );       
        
        List questions = Question.find(
            "select q.order, qt.name, q.question, q.weighing, TO_CHAR(q.modificationDate, 'DD/MM/YYYY'), " +
            "u.nombre, q.id, qt.id " +                    
            "from Question q JOIN q.questionType qt LEFT JOIN q.modifiedBy u JOIN q.survey s " +
            "WHERE s.id = ?1 AND (LOWER(q.question) LIKE LOWER(?2) or LOWER(qt.name) LIKE LOWER(?3)) order by q.order asc, q.id asc",
            surveyId, "%" + sSearch + "%", "%" + sSearch + "%"
        ).from(start).fetch(end);
        
        renderJSON(new Pagination(sEcho, count, count, questions));
    }
    
    public static void editQuestion(int questionId) {
        Question question = Question.findById(questionId);
        List<Factor> factors = Factor.find("survey.id = ?1", question.survey.id).fetch();
        
        render(question, factors);
    }
    
    public static void deleteQuestion(int questionId) {
        Question question = Question.findById(questionId);
        int surveyId      = question.survey.id;
        try {
            QuestionChoices.delete("question.id = ?1", questionId);
            Question.delete("id = ?1", questionId);
            querys.SurveysGeneratorQuery.calculatePercentageFactors(surveyId);
            
            renderJSON(new utils.Message(null, null, false));
        } catch (Exception e) {
            renderJSON(new utils.Message(e.getMessage(), null, true));
        }
    }
    
    public static void editValue(Integer pk, Float value) {
        Question question   = Question.findById(pk);
        SubFactor subfactor = question.subFactor;
        double maxValue     = getMaxValue(subfactor, pk);
        
        DecimalFormat df = new DecimalFormat("#.####");
        df.setRoundingMode(RoundingMode.CEILING);

        if (value > maxValue) {
            response.status = 400;
            renderJSON("El valor debe ser menor o igual a " + df.format(maxValue));
        }
        
        question.weighing = value;
        question.save();
    }
    
    private static double getTotalWeigh(SubFactor subfactor, int questionId) {
        int questions = (int) Question.count(
            "questionType.id = 2 and subFactor.id = ?1 and survey.id = ?2",
            subfactor.id, subfactor.factor.survey.id
        );
        
        float questionWeighPercent = (float) ((100 / questions) * subfactor.points) / 100;
        float totalWeigh = questionWeighPercent * questions;
        
        return totalWeigh;
    }
    
    private static double getMaxValue(SubFactor subfactor, int questionId) {
        if (subfactor == null) {
            response.status = 400;
            renderJSON("La pregunta no esta relacionada a un factor");
        }
        
        try {
            Query sumQuery = Question.em().createQuery(
                "select coalesce(sum(weighing), 0) from Question where " + 
                "subFactor.id = ?1 and survey.id = ?2 " +
                "and id <> ?3"
            );

            sumQuery.setParameter(1, subfactor.id);
            sumQuery.setParameter(2, subfactor.factor.survey.id);
            sumQuery.setParameter(3, questionId);

            Double getTotalWeigh = (Double) sumQuery.getSingleResult();

            return subfactor.points - getTotalWeigh;
        } catch (Exception e) {
            response.status = 400;
            renderJSON("El puntaje del subfactor no es válido o bien no ha sido calculado");
        }
        
        return 0;
    }
    
    public static void getFacesQuestions(int surveyId) {
        List<Question> questions = Question.find(
            "survey.id = ?1 and lower(questionType.name) = 'caras' order by subFactor.description asc", surveyId
        ).fetch();
        
        render(questions);
    }
    
    public static void updateWeigh(Map<String, Float> questions) {
        if (questions != null) {
            for (String key : questions.keySet()) {
                Question question = Question.findById(Integer.valueOf(key));
                Float points      = questions.get(key);

                question.weighing        = points;
                question.weighingPercent = (question.weighing * 100) / question.subFactor.points;
                question.save();
            }
        }
        
        renderJSON(new utils.Message(null, null, false));
    }
}