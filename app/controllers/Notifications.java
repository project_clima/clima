package controllers;

import play.*;
import play.mvc.*;
import java.util.*;
import models.*;
import play.db.jpa.JPA;
import javax.persistence.Query;
import querys.NotificationsQuery;
import com.google.gson.*;

@With(Secure.class)
public class Notifications extends Controller {

    public static Boolean needsReview() {
        String userId = session.get("userId");

        NotificationsQuery queries = new NotificationsQuery();

        List elements = queries.needsReview(userId);
        if (elements.size() > 0) {
            if (Integer.parseInt(elements.get(0).toString()) > 0) {
                return true;
            }
        }
        return false;
    }

    public static Boolean hasRejected() {
        String userId = session.get("userId");

        NotificationsQuery queries = new NotificationsQuery();

        List elements = queries.hasRejected(userId);
        if (elements.size() > 0) {
            if (Integer.parseInt(elements.get(0).toString()) > 0) {
                return true;
            }
        }
        return false;
    }
}
