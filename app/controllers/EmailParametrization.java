package controllers;

import java.text.SimpleDateFormat;
import play.mvc.*;
import java.util.*;
import json.entities.MailingList;
import models.*;
import play.Play;
import querys.EmailQuery;
import utils.GmailMailer;
import play.templates.Template;
import play.templates.TemplateLoader;
import utils.Pagination;

@With(Secure.class)
public class EmailParametrization extends Controller {
    public static void index() {
        List areas  = SurveyEmployee.find(
            "select distinct ec.area from EmployeeClima ec where ec.area is not null order by ec.area"
        ).fetch();
        
        List levels = SurveyEmployee.find(
            "select distinct ec.vLevel from EmployeeClima ec where ec.vLevel is not null order by ec.vLevel"
        ).fetch();
        
        render(areas, levels);
    }
    
    public static void list(int iDisplayLength, String startDate,
                              String endDate, int iDisplayStart, String sEcho,
                              String sSortDir_0, int iSortCol_0) {
        
        int start        = iDisplayStart == 0 ? iDisplayStart : iDisplayStart + 1;
        int end          = iDisplayLength + iDisplayStart;
        long count       = Survey.count();        
        List surveys     = Survey.find(
            "select s.id , s.name, s.id, s.id from Survey s"
        ).from(start).fetch(end);
        
        renderJSON(new Pagination(sEcho, count, count, surveys));
    }
    
    public static void getPositions(String term) {
        List functions = EmployeeClima.find(
            "select distinct ec.function from EmployeeClima ec where lower(ec.function) like lower(?)",
            "%" + term + "%"
        ).fetch(30);
        
        renderJSON(functions);
    }

    public static void createMail() {
    	render();
    }

    public static void simpleEmailSending() {
    	render();
    }

    public static void getSurveys() {
        List<Object> surveys = Survey.find(
            "select s.id, s.name, st.id from Survey s JOIN s.surveyType st"
        ).fetch();
        
        renderJSON(surveys);
    }

    public static void sendEmailsCorporativos(int surveyId, int surveyType) throws Exception {
        Survey survey                = Survey.findById(surveyId);
        Map<String, String> template = getEmailTemplate(surveyType);
        String emailContents;
        
        GmailMailer mailer = new GmailMailer();
        
        try {
            
            List<MailingList> employees = EmailQuery.getEmployeesTest(surveyId);
            
            for (MailingList employee : employees) {
                emailContents = prepareTemplate(
                    template.get("body"),
                    survey,
                    employee.getFirstName(),
                    employee.getFullName(),
                    getSurveyLink(survey.id, employee),
                    "",
                    ""
                );
                        
                mailer.send(
                    employee.getEmail(),
                    template.get("subject"),
                    emailContents,
                    survey.id,
                    null
                );
            }
            
            // Seve send date
            survey.sendDate = new Date();
            survey.save();
            
            renderJSON(new utils.Message("", "", false));
        } catch (Exception ex) {
            throw new Exception(ex);
        }
    }
    
    public static void sendEmailsTienda(int surveyId, int surveyType) throws Exception {
        Survey survey       = Survey.findById(surveyId);
        List<String> stores = EmailQuery.getStoreList();
        GmailMailer mailer  = new GmailMailer();
        
        for (String store : stores) {
            sendToStore(survey, surveyType, store, "", mailer);
        }
    }
    
    private static void sendToStore(Survey survey, int surveyType, String store, String zone, GmailMailer mailer) throws Exception {
        Map<String, String> template = getEmailTemplate(surveyType);
        Template listTemplate        = TemplateLoader.load("EmailParametrization/employeeList.html");
        String surveyLink            = getSurveyLink(survey.id, null);
        
        String emailContents;
        String managerFullName;
        String usersTable;
        
        try {            
            List<MailingList> employees = EmailQuery.getEmployeesStoreTest(survey.id, store, 8);
            MailingList director        = EmailQuery.getAreaDirector(survey.id, store, 8);
                    
            HashMap<String, List<MailingList>> hhRR = setDependants(employees);
            Map<String, Object> templateArgs = new HashMap();
            templateArgs.put("baseUrl", Play.configuration.getProperty("application.baseUrl"));
            
            for (String key : hhRR.keySet()) {
                MailingList manager = EmailQuery.getEmailStore(key, survey.surveyDate);
                        
                if (manager.getEmail() != null && !manager.getEmail().equals("")) {
                    usersTable      = getEmployeesTable(listTemplate, hhRR.get(key), director);

                    emailContents = prepareTemplate(
                        template.get("body"),
                        survey,
                        manager.getManager(),
                        manager.getManager(),
                        surveyLink,
                        usersTable,
                        key
                    );
                    
                    mailer.send(
                        manager.getEmail(),
                        template.get("subject"),
                        emailContents,
                        survey.id,
                        null
                    );
                }
            }
            
            // Seve send date
            survey.sendDate = new Date();
            survey.save();
        } catch (Exception ex) {
            throw new Exception(ex);
        }
    }
    
    private static HashMap<String, List<MailingList>> setDependants(List<MailingList> employees) {
        HashMap<String, List<MailingList>> hhRR = new HashMap();
        String key;
        
        for (MailingList employee : employees) {            
            key = employee.getDivision();
            
            employees = hhRR.get(key);
            employees = employees == null ? new ArrayList() : employees;

            employees.add(employee);
            hhRR.put(key, employees);
        }
        
        return hhRR;
    }
    
    private static Map getEmailTemplate(int surveyTypeId) {
        EmailTemplate template = EmailTemplate.find("type.id = ?1", surveyTypeId).first();
        
        String subject = template != null ? template.subject : "";
        String body    = template != null ? template.body : "";
        
        Map<String, String> templateParts = new HashMap();
        templateParts.put("subject", subject);
        templateParts.put("body", body);
        
        return templateParts;
    }
    
    private static String getSurveyLink(int surveyId, MailingList employee) {
        if (employee != null) {
            return request.getBase() + "/surveys/apply/" +  surveyId + "/" +
                employee.getUserName() + "/" +
                employee.getPassword();
        }
        
        return request.getBase() + "/surveys/apply/" +  surveyId + "/";
    }
    
    private static String prepareTemplate(String template, Survey survey, String name,
                                          String fullName, String link, String table, String store) {        
        HashMap<String, Object> vars = new HashMap();
        SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
        
        vars.put("%collaborator.name%", name);
        vars.put("%collaborator.fullName%", fullName);
        vars.put("%survey.name%", survey.name);
        vars.put("%survey.start%", sdf.format(survey.dateInitial));
        vars.put("%survey.end%", sdf.format(survey.dateEnd));
        vars.put("%survey.link%", link);
        vars.put("%usersTable%", table);
        vars.put("%store%", store);
        
        if (template != null) {
            for (String key : vars.keySet()) {
                template= template.replace(key, vars.get(key).toString());
            }
        }
        
        return template;
    }
    
    private static String getEmployeesTable(Template template, List<MailingList> employees, MailingList director) {
        employees.add(director);
        
        HashMap<String, Object> tableArgs = new HashMap();
        tableArgs.put("employees", employees);
                
        return template.render(tableArgs);
    }
}
