package controllers;

import java.io.UnsupportedEncodingException;
import java.util.*;
import models.*;
import play.data.validation.Valid;
import play.mvc.*;
import play.templates.Template;
import play.templates.TemplateLoader;
import utils.Pagination;

/**
 * Clase controlador para el manejo de las plantillas de correo
 * @author Rodolfo Miranda -- Qualtop
 */
@With(Secure.class)
public class EmailTemplates extends Controller {
    /**
     * Metodo que obtiene los tipos de encuestas
     */
    public static void index() {
        List<SurveyType> types  = SurveyType.findAll();
        
        render(types);
    }
    
    /**
     * Metodo que obtiene las encuestas por tipo de encuesta
     * @param iDisplayLength
     * @param startDate
     * @param endDate
     * @param iDisplayStart
     * @param sEcho
     * @param sSortDir_0
     * @param iSortCol_0 
     */
    public static void list(int iDisplayLength, String startDate,
                              String endDate, int iDisplayStart, String sEcho,
                              String sSortDir_0, int iSortCol_0) {
        
        int start  = iDisplayStart == 0 ? iDisplayStart : iDisplayStart + 1;
        int end    = iDisplayLength + iDisplayStart;
        long count = SurveyType.count();        
        List types = SurveyType.find(
            "select t.subType, t.name, t.id from SurveyType t where t.id not in (4, 5)"
        ).from(start).fetch(end);
        
        renderJSON(new Pagination(sEcho, count, count, types));
    }
    
    /**
     * Metodo para editar una encuesta
     * @param surveyTypeId 
     */
    public static void edit(int surveyTypeId) {
        SurveyType  type = SurveyType.findById(surveyTypeId);
        EmailTemplate template = EmailTemplate.find("type.id = ?1", surveyTypeId).first();
        
        render(template, type, surveyTypeId);
    }
    
    /**
     * Metodo para guardar una platilla de encuesta
     * @param template
     * @param place 
     */
    public static void save(@Valid EmailTemplate template, String place) {
        if(validation.hasErrors()) {
            flash.error("Datos no válidos. Por favor revise la información");
            edit(template.type.id);
        }
        
        if(validation.hasErrors()) {
            renderJSON(new utils.Message(
                "Error al guardar la información. Por favor, revise el formulario cuidadosamente.",
                null,
                true)
            );
        }
        
        if (template.id == null) {
            template.creationDate = new Date();
            template.createdBy    = session.get("username");
            flash.success("Plantilla creada correctamente");
        } else {
            template.modificationDate = new Date();
            template.modifiedBy       = session.get("username");
            flash.success("Plantilla actualizada correctamente");
        }
        
        template.save();
        
        if (place != null && place.equals("CSI")) {
            EmailCSI.emailTemplate();
        }
        
        index();
    }
    
    /**
     * MEtodo que muestra la vista previa de la plantilla de correo
     * @param template
     * @param surveyTypeId
     * @throws UnsupportedEncodingException 
     */
    public static void preview(EmailTemplate template, int surveyTypeId) 
            throws UnsupportedEncodingException {
        boolean isNull = template.type == null;
        
        template = (template.type == null) ? 
                (EmailTemplate) EmailTemplate.find("type.id = ?1", 
                                surveyTypeId).first() : template;
        
        HashMap<String, Object> vars = new HashMap();
        Template templateList = TemplateLoader.load("EmailTemplates/employee-list-sample.html");
        
        vars.put("%collaborator.name%", "Julieta");
        vars.put("%collaborator.fullName%", "Julieta Ornelas");
        vars.put("%survey.name%", "Encuesta Clima");
        vars.put("%survey.start%", "01-01-2016");
        vars.put("%survey.end%", "01-01-2017");
        vars.put("%survey.link%", "http://www.liverpool.com.mx/tienda/home.jsp");
        vars.put("%usersTable%", templateList.render());
        vars.put("%store%", "Fábricas de Francia CDMX");
        
        if (template != null && template.body != null) {
            for (String key : vars.keySet()) {
                template.body = template.body.replace(key, vars.get(key).toString());
            }
            if(!isNull) {
                template.body = java.net.URLDecoder.decode(template.body, "UTF-8");
            }
        }        
        
        render(template, surveyTypeId);
    }
}
