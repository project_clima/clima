package controllers;

import play.mvc.*;
import java.util.*;
import models.*;
import play.Logger;
import querys.SurveysQuery;

/**
 * Clase controlador para el manejo de las Encuestas
 * @author Rodolfo Miranda -- Qualtop
 */
public class Surveys extends Controller {
    @Before(unless={"apply"})
    static void checkAccess() throws Throwable {
        Secure.checkAccess();
    }

    /**
     * Metodo principal /index
     */
    public static void index() {                
        render();
    }
    
    /**
     * MEtodo que redirecciona a la edicion de encuesta
     */
    public static void edit() {
        render();
    }
    
    /**
     * Metodo de apoyo para la edicion de Seccion por encuesta
     * @param section 
     */
    public static void editSurveySection(String section) {
        String template = "Surveys/" + section + "-section.html";
        renderTemplate(template);
    }
    
    /**
     * MEtodo para la aplicacion de la encuesta
     * @param surveyId
     * @param username
     * @param password
     * @throws Throwable 
     */
    public static void apply(Integer surveyId, String username, String password) throws Throwable {
        if (username == null || password == null) {
            Secure.checkAccess();
        } else {
            authenticate(surveyId, username, password);
        }
        
        SurveyEmployee employee = getSurveyEmployee(surveyId);
        
        if (!isValidSurvey(employee)) {
            session.clear();
            controllers.Secure.login();
        }
        
        Survey survey     = employee.survey;
        List questions    = utils.Survey.getSurveyQuestions(surveyId);        
        String manager    = getNamePersonToEvaluate(employee, survey.surveyType.subType);
        String answerMode = getAnswerMode(survey.surveyType.subType);
        HashMap answers   = getSurveyAnswers(surveyId, employee.employeeClima.id, answerMode);
        
        response.setHeader("Cache-Control", "no-cache, no-store, max-age=0, must-revalidate, post-check=0, pre-check=0");
        render(surveyId, survey, questions, answers, manager, answerMode);
    }
    
    /**
     * MEtodo para obtener el nombre de la persona a evaluar
     * @param employee
     * @param surveyType
     * @return 
     */
    private static String getNamePersonToEvaluate(SurveyEmployee employee, String surveyType) {
        if (surveyType.toLowerCase().equals("usuario genérico")) {
            session.put("displayName", "");
            return employee.employeeClima.firstName + " " + employee.employeeClima.lastName;
        }
        
        return getEmployeesManager(employee.employeeClima.manager);
    }
    
    /**
     * Metodo de utileria
     * @param subType
     * @return 
     */
    private static String getAnswerMode(String subType) {
        if (subType.toLowerCase().equals("usuario genérico")) {
            return "shared";
        }
        
        return "single";
    }
    
    /**
     * Metodo para obtener las respuestas por encuesta
     * @param surveyId
     * @param userId
     * @param answerMode
     * @return 
     */
    private static HashMap<Integer, String[]> getSurveyAnswers(Integer surveyId, Integer userId, String answerMode) {
        HashMap<Integer, String[]> responses = new HashMap<>();
        
        if (answerMode.equals("shared")) {
            return responses;
        }
        
        List<QuestionAnswer> answers = QuestionAnswer.find(
            "surveyId = ? and employee = ?", surveyId, userId
        ).fetch();
        
        for (QuestionAnswer answer : answers) {
            String choiceSelected    = answer.choice != null ? answer.choice.id.toString() : "";
            String[] currentResponse = {choiceSelected, answer.answerText};
            responses.put(answer.question.id, currentResponse);
        }
        
        return responses;
    }
    
    /**
     * MEtodo para obtener el Empleado Clima de la encuesta
     * @param surveyId
     * @return 
     */
    private static SurveyEmployee getSurveyEmployee(Integer surveyId) {
        surveyId        = surveyId == null ? -1 : surveyId;
        String username = session.get("username");
        
        return getEmployeeSurvey(surveyId, username);
    }
    
    /**
     * Metodo para obtener el Empleado Clima por encuesta y username
     * @param surveyId
     * @param username
     * @return 
     */
    private static SurveyEmployee getSurveyEmployee(Integer surveyId, String username) {
        surveyId = surveyId == null ? -1 : surveyId;
        return getEmployeeSurvey(surveyId, username);
    }
    
    /**
     * Metodo para obtener el Empleado Clima por encuesta y username
     * @param surveyId
     * @param username
     * @return 
     */
    private static SurveyEmployee getEmployeeSurvey(Integer surveyId, String username) {
        SurveyEmployee employeeSurvey = SurveyEmployee.find(
            "survey.id = ? and (username = ? or upper(username) = upper(?))",
            surveyId,
            username,
            utils.Misc.md5(username)
        ).first();
        
        return employeeSurvey;
    }
    
    /**
     * MEtodo para autenticar la entrada a la encuesta
     * @param surveyId
     * @param username
     * @param password
     * @throws Throwable 
     */
    private static void authenticate(Integer surveyId, String username, String password) throws Throwable {
        session.clear();
        
        if (controllers.Security.authenticate(username, password, surveyId)) {
            apply(surveyId, null, null);
        } else {
            flash.error("secure.error");
            controllers.Secure.login();
        }
    }
    
    /**
     * Metodo par saber si una encuesta aun sigue abierta
     * @param employee
     * @return 
     */
    private static Boolean isValidSurvey(SurveyEmployee employee) {
        return 
            employee != null &&
            inTime(employee.survey) &&
            canAnswer(employee) &&
            belongsToEmployee(employee.employeeClima.id);
    }
    
    /**
     * Metodo de apoyo para saber si la encuesta sigue abierta
     * @param survey
     * @return 
     */
    private static Boolean inTime(Survey survey) {
        Date currentDate = new Date();
        Date endDate     = getEndDate(survey.dateEnd);
        
        Boolean inTime   = currentDate.after(survey.dateInitial) && 
                           currentDate.before(endDate);
        
        if (!inTime) {
            flash.error("La encuesta no se encuentra vigente.");
        }
        
        return inTime;
    }
    
    /**
     * Metodo de utileria para saber el ultimo dia del mes
     * @param endDate
     * @return 
     */
    private static Date getEndDate(Date endDate) {
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(endDate);
        calendar.add(Calendar.DATE, 1);
        
        return calendar.getTime();
    }
    
    /**
     * Metodo para saber si aun hay algun empleado sin contestar la encuesta
     * @param surveyEmployee
     * @return 
     */
    private static Boolean canAnswer(SurveyEmployee surveyEmployee) {
        Boolean canAnswer = false;
        
        try {
            Survey survey     = surveyEmployee.survey;
            String answerMode = getAnswerMode(survey.surveyType.subType);

            int maxAttemps     = surveyEmployee.maxAttempts != null ? 
                                        surveyEmployee.maxAttempts : 1;

            int currentAttemps = surveyEmployee.attempts != null ? 
                                        surveyEmployee.attempts : 0;

            if (answerMode.equals("single")) {
                canAnswer = currentAttemps == 0;
            } else {
                canAnswer = maxAttemps > currentAttemps;
            }

            if (!canAnswer) {
                flash.error("El plan ya fue alcanzado. La encuesta ya no puede ser contestada.");
            }
        } catch (Exception e) {
            flash.error("La encuesta no puede ser contestada en estos momentos.");
            Logger.error(e.getMessage());
        }
        
        return canAnswer;
    }
    
    private static Boolean belongsToEmployee(Integer surveyEmployee) {
        return surveyEmployee.toString().equals(session.get("userId"));
    }
    
    /**
     * Metodo para saber el gerente de un empleado
     * @param managerId
     * @return 
     */
    private static String getEmployeesManager(Integer managerId) {
        EmployeeClima employee = EmployeeClima.find("userId = ?1", managerId).first();
        
        if (employee != null) {
            return employee.firstName + " " + employee.lastName;
        }
        
        return null;
    }
    
    /**
     * MEtodo para guardar los resultados de una encuesta
     * @param surveyId
     * @param answers
     * @param close
     * @param userId
     * @param username 
     */
    public static void save(Integer surveyId, Map<String, String[]> answers, Boolean close, int userId, String username) {      
        SurveyEmployee employee = getSurveyEmployee(surveyId, username);
        String answerMode = getAnswerMode(employee.survey.surveyType.subType);
        Date answerDate   = new Date();
        Long attempt      = getIdBySurvey(userId, surveyId, answerMode);
        
        if (!canAnswer(employee)) {
            renderJSON(new utils.Message("Maximun attempts reached", "", true));
        }
        
        try {
            if (answers.size() > 0) {
                for (String key : answers.keySet()) {                    
                    saveAnswers(
                        answers.get(key), userId, Integer.parseInt(key),
                        answerMode, answerDate, attempt
                    );
                }
            }
            
            if (close != null && close == true) {
                int result = finishSurvey(employee);
                Logger.info("Se contesto encuesta : " + surveyId + " usuario " 
                        + userId + " username " + username + " : FN_INS_SINGLE_SCORE " + result);
            }
            
            renderJSON(new utils.Message("", "", false));
        } catch (Exception e) {
            renderJSON(new utils.Message(e.getMessage(), "", true));
        }
    }
    
    /**
     * MEtodo para guardar las respuestas de las preguntas de una encuesta
     * @param userAnswers
     * @param userId
     * @param questionId
     * @param answerMode
     * @param answerDate
     * @param attempt 
     */
    private static void saveAnswers(String[] userAnswers, int userId, int questionId,
                                    String answerMode, Date answerDate, Long attempt) {
        Question question   = Question.findById(questionId);
        String questionType = question.questionType.name;        
        
        if (userAnswers.length > 1 || questionType.equals("OPCIONES MULTIPLES")) {
            if (answerMode.equals("single")) {
                QuestionAnswer.delete(
                    "employee = ? and question.id = ?", userId, question.id
                );
            }
            
            for (String answer : userAnswers) {
                saveSingleAnswer(answer, question, userId, null, answerMode, answerDate, attempt);
            }
        } else {
            QuestionAnswer questionAnswer = QuestionAnswer.find(
                "employee = ? and question.id = ?", userId, question.id
            ).first();
            
            saveSingleAnswer(
                userAnswers[0], question, userId, questionAnswer, answerMode, answerDate, attempt
            );
        }
    }
    
    /**
     * MEtodo para guardar las respuestas de una encuesta 
     * @param userAnswer
     * @param question
     * @param userId
     * @param questionAnswer
     * @param answerMode
     * @param answerDate
     * @param attempt 
     */
    private static void saveSingleAnswer(String userAnswer, Question question, 
                                         int userId, QuestionAnswer questionAnswer,
                                         String answerMode, Date answerDate, Long attempt) {
        
        QuestionAnswer answer = questionAnswer == null || answerMode.equals("shared")
                                    ? new QuestionAnswer() : questionAnswer;
        
        String[] answerParts = userAnswer.split("\\|");
        String createdBy     = session.get("username");   
        int surveyId         = question.survey.id;
        
        answer.answerText = answerParts[0];
        answer.question   = question;
        answer.employee   = userId;
        answer.surveyId   = surveyId;
        answer.idBySurvey = answer.idBySurvey == null ? attempt : answer.idBySurvey;
        
        answer.creationDate = answer.creationDate == null ? answerDate : answer.creationDate;
        
        if (createdBy.length() > 30) {
            createdBy = createdBy.substring(0, 30);
        }
        
        answer.createdBy    = answer.createdBy == null ? createdBy : answer.createdBy;
        
        answer.modificationDate = answerDate;
        answer.modifiedBy       = createdBy;
        
        if (answerParts.length == 2) {            
            answer.choice = QuestionChoices.findById(Integer.parseInt(answerParts[1]));
        }
        
        answer.save();
    }
    
    /**
     * Metodo para cerrar la encuesta
     * @param user
     * @return 
     */
    private static int finishSurvey(SurveyEmployee user) {        
        int result = -1;
        if (user != null) {
            user.attempts = user.attempts == null ? 0 : user.attempts;          
            user.attempts++;
            user.save();
            
            result = SurveysQuery.insertSingleScore(user.survey.id, user.employeeClima.id, user.username);
        }
        return result;
    }
    
    /**
     * MEtodo para saber las preguntas por encuesta
     * @param userId
     * @param surveyId
     * @param answerMode
     * @return 
     */
    private static Long getIdBySurvey(int userId, int surveyId, String answerMode) {
        QuestionAnswer questionAnswer = QuestionAnswer.find(
            "employee = ? and surveyId = ?", userId, surveyId
        ).first();
        
        if (questionAnswer != null && answerMode.equals("single")) {
            return questionAnswer.idBySurvey;
        } else {
            return SurveysQuery.getIdBySurvey();
        }
    }
}