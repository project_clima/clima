package controllers;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import java.io.IOException;
import java.lang.reflect.Type;
import java.util.*;
import net.sf.jxls.exception.ParsePropertyException;
import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import play.mvc.*;
import utils.RenderExcel;

@With(Secure.class)
public class ExcelController extends Controller {

    public static void generateExcel(String json, String template, String fileName)
            throws ParsePropertyException, InvalidFormatException, IOException {
        String name = fileName;

        if (name == null) {
            String[] pathParts = template.split("/", -1);
            name = pathParts[pathParts.length - 1];
        }

        // A list of lists (the table rows):
        Type listType = new TypeToken<List<List<String>>>() {
        }.getType();
        List<List<String>> rows = new Gson().fromJson(json, listType);

        renderArgs.put("rows", rows);
        renderArgs.put(RenderExcel.RA_FILENAME, name);
        RenderExcel renderer = new RenderExcel("app/excel/views/" + template);
        renderer.render();
    }
}
