package controllers;

import com.google.gson.Gson;
import java.io.IOException;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.*;
import java.util.logging.Level;
import java.util.logging.Logger;
import json.entities.HistoricoList;
import json.entities.HistoryResults;
import models.*;
import net.sf.jxls.exception.ParsePropertyException;
import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import play.mvc.*;
import querys.HistoryQuery;
import utils.Pagination;
import utils.PagingResult;
import utils.RenderExcel;

/**
 * Clase controlador para el manejo del reporte historico de clima
 * @author Rodolfo Miranda -- Qualtop
 */
@With(Secure.class)
public class History extends Controller {

    /**
     * Metodo principa
     */
    public static void index() {
        render();
    }
    
    /**
     * MEtodo para obtener los resultados del reporte historico
     * @param option
     * @param twoYearsAgo
     * @throws SQLException 
     */
    public static void getResults(String option, String twoYearsAgo)
    throws SQLException {
        LinkedHashMap<String, HistoricoList> environment = HistoryQuery.getEnvironment(option, twoYearsAgo);
        LinkedHashMap<String, HistoricoList> leadership  = HistoryQuery.getLeadership(option, twoYearsAgo);
        
        renderJSON(joinResults(environment, leadership));
    }
    
    /**
     * Metodo que cruza los resultados lederago y clima
     * @param environment
     * @param leadership
     * @return 
     */
    private static List<HistoryResults> joinResults(LinkedHashMap<String, HistoricoList> environment,
                                                    LinkedHashMap<String, HistoricoList> leadership) {
        List<HistoryResults> results = new ArrayList();
        HistoryResults hR;
        
        for (String key : environment.keySet()) {
            hR = new HistoryResults();
            HistoricoList envResults  = environment.get(key);
            HistoricoList leadResults = leadership.get(key);
            
            hR.setLevel(key);
            hR.setEnvFirstPercent(envResults.getFirstPercent());
            hR.setEnvFirstYear(envResults.getFirstYear());
            hR.setEnvSecondPercent(envResults.getSecondPercent());
            hR.setEnvSecondYear(envResults.getSecondYear());
            
            hR.setLeadFirstPercent(leadResults.getFirstPercent());
            hR.setLeadFirstYear(leadResults.getFirstYear());
            hR.setLeadSecondPercent(leadResults.getSecondPercent());
            hR.setLeadSecondYear(leadResults.getSecondYear());
            
            results.add(hR);
        }
        
        return results;
    }

    /**
     * MEtodo para obtener los resultados de clima dos años atras
     * @param option
     * @param twoYearsAgo
     * @throws SQLException 
     */
    public static void getEnvironment(String option, String twoYearsAgo) throws SQLException {
        HistoryQuery queries = new HistoryQuery();
        List elements = convertInto(queries.getEnvironment(option, twoYearsAgo));
        renderJSON(elements);
    }

    /**
     * MEtodo para obtener los resultados de liderazgo dos años atras
     * @param option
     * @param twoYearsAgo
     * @throws SQLException 
     */
    public static void getLeadership(String option, String twoYearsAgo) throws SQLException {
        HistoryQuery queries = new HistoryQuery();

        List elements = convertInto(queries.getLeadership(option, twoYearsAgo));
        renderJSON(elements);
    }

    /**
     * MEtodo para exportar los resultados a excel
     * @param option
     * @param twoYearsAgo
     * @param levelName
     * @throws ParsePropertyException
     * @throws InvalidFormatException
     * @throws IOException
     * @throws SQLException 
     */
    public static void generateExcel(String option, String twoYearsAgo, String levelName)
            throws ParsePropertyException, InvalidFormatException, IOException, SQLException {        
        LinkedHashMap<String, HistoricoList> environment = HistoryQuery.getEnvironment(option, twoYearsAgo);
        LinkedHashMap<String, HistoricoList> leadership  = HistoryQuery.getLeadership(option, twoYearsAgo);
        
        List<HistoryResults> rows = joinResults(environment, leadership);

        renderArgs.put("rows", rows);
        renderArgs.put("levelName", levelName);
        renderArgs.put("lastYear", twoYearsAgo);
        renderArgs.put("currentYear", Integer.valueOf(twoYearsAgo) + 1);

        renderArgs.put(RenderExcel.RA_FILENAME, "Historico.xls");
        RenderExcel renderer = new RenderExcel("app/excel/views/History/exportHistorico.xls");
        renderer.render();
    }

    private static List<HistoricoList> convertInto(LinkedHashMap<String, HistoricoList> historico) {
        List<HistoricoList> list = new ArrayList();
        for (Map.Entry<String, HistoricoList> entry : historico.entrySet()) {
            list.add(entry.getValue());
        }
        return list;
    }
}
