package controllers;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import java.sql.SQLException;
import play.*;
import play.mvc.*;
import java.util.*;
import java.util.logging.Level;
import models.*;
import querys.nps.NpsHomeQuery;
import utils.nps.util.FormatDateUtil;
import utils.nps.json.NpsHomeJsonObject;
import utils.nps.model.Manager;
import utils.nps.model.Zone;
import utils.nps.model.Section;
import utils.nps.model.Store;
import utils.nps.model.ZoneHomeModel;

@With(Secure.class)
public class StructureRanking extends Controller {

    public static void index() {
        render();
    }
    
    public static void getTopNps( String firstDate, String lastDate , String z){
        List<ZoneHomeModel> zones = null;
        Gson gson = new Gson();
        String userId = null;
        
        List<Zone> zAreas = new ArrayList<>();
        NpsHomeJsonObject nps = new NpsHomeJsonObject();
        List<String> strsList = new ArrayList<>();
        
        float [] months = null;
        float [] monthsLess1Year = null;
        float [] triMonth = null;
        float [] triMonthLess1Year = null;
        
        String strs[] = {};
        
        List<Store> npsStores = null;
        List<Manager> managers = new ArrayList<>();            
        List<Section> sections = new ArrayList<>();
        
//        if( (firstDate == null && lastDate == null) || (firstDate.isEmpty()
//                && lastDate.isEmpty()) ){
//            firstDate =  "01/"+NPS.getMonth()+"/"+
//                    Calendar.getInstance().get(Calendar.YEAR);
//            lastDate = FormatDateUtil.ownFormat.format( new Date() );
//        }
//        if( z != null && !z.equals("[]") && !z.isEmpty()){
//            zones = gson.fromJson(z, new TypeToken<List<ZoneHomeModel>>(){}.getType());
//            //System.out.println(zonesStores);
//        }
                
//        try {
//            if( zones != null ){
//                for( ZoneHomeModel zone : zones ){
//                    //System.out.println(zone.getStores());
//                    List<String> storesIds = new ArrayList<>();
//                    for( Store s : zone.getStores() ){
//                        storesIds.add(s.getIdStore());
//                        strsList.add( s.getIdStore());
//                    }
//                    zone.setStoresIds(storesIds);
//                }
                
//                for( ZoneHomeModel zI : zones ){
//                        if( zI.getStoresIds() != null && !zI.getStoresIds().isEmpty() ){
//                            NpsZoneArea area =  
//                                    NpsHomeQuery.getNpsArea(userId,
//                                    zI.getStoresIds().
//                                    toArray(new String[zI.getStoresIds().size()]),
//                                    null, 
//                            firstDate, lastDate, zI.getId());
//                            //System.out.println(area.toString());
//                            zAreas.add(area);
//                        }
//                        
//                    }
//            }else{
//            zones = NpsHomeQuery.getZoneHome(userId);     
//                                           
//                for( ZoneHomeModel zone : zones ){
//                    //System.out.println(zone.getStores());
//                    for( Store s : zone.getStores() ){
//                        //npsStores.add(s);
//                        strsList.add( s.getIdStore());
//                        
//                    }
//                }
                
//                for( ZoneHomeModel zI : zones ){
//                    NpsZoneArea area =  NpsHomeQuery.getNpsArea(userId, null,null,
//                            firstDate, lastDate, zI.getId());                    
//                    zAreas.add(area);
//                }
//            }
//                strs = new String[strsList.size()];
//                strs = strsList.toArray(strs);
//                
                
//                npsStores = NpsHomeQuery.
//                        getMoreHightStoresNps(strs, firstDate, lastDate, "desc");
//                managers = NpsHomeQuery.getMoreHightManagerNps(strs, 
//                        firstDate, lastDate,"desc");
//                
//                //sections
//                sections = NpsHomeQuery.getMoreHightSectionNps(strs, 
//                        firstDate, lastDate, "desc");
                
//                
//                
//            nps.setStoresNpsTotal(npsStores);
//            nps.setManagersTotal(managers);
//            nps.setSectionsTotal(sections);
//            nps.setzAreas(zAreas);
//            renderJSON(nps);
//                
//        } catch (SQLException ex) {
//            java.util.logging.Logger.getLogger(StructureRanking.class.getName()).log(Level.SEVERE, null, ex);
//        }
    }
}
