package controllers;

import static controllers.ResultMatrix.setSubfactores;
import java.sql.SQLException;
import play.*;
import play.mvc.*;
import java.util.*;
import json.entities.ResultMatrixFactor;
import json.entities.ResultMatrixList;
import json.entities.ResumeTable;
import json.entities.SpecialPermissions;
import models.*;
import querys.StructureQuery;
import querys.MatrixQuery;
import utils.Queries;

/**
 * Clase controlador principal del modulo de Clima
 * @author Rodolfo Miranda -- Qualtop
 */
@With(Secure.class)
public class DashboardEnviroment extends Controller{
    
    /**
     * Metodo que regresa las fechas de estructura
     */
   public static void index() {
        List structureDates = StructureQuery.getDatesStructures();
        render(structureDates);
    }
   
   /**
    * MEtodo para el manejo del primer nivel de los departamentos
    * @param level
    * @param date 
    */
   public static void getLevels1(int level, String date){
        List levels = StructureQuery.getDepartmentsByLevel1(level, date);
        renderJSON(levels);
    }

   /**
    * Metodo generico para el manejo de los diferentes departamentos dependiendo
    * los filtros
    * @param department
    * @param date 
    */
    public static void getLevels(int department, String date) {
        List levels = StructureQuery.getDepartmentsByLevelAllowed(department, date);
        renderJSON(levels);
    }
    
    /**
     * Metodo para clasificar los factores
     * @param factors
     * @return 
     */
    private static LinkedHashMap<String, List<ResultMatrixList>> setFactores(List<ResultMatrixList> factors) {
        LinkedHashMap<String, List<ResultMatrixList>> resultMatrix = new LinkedHashMap();

        for (ResultMatrixList factor : factors) {
            factors = resultMatrix.get(factor.getFactor());
            factors = factors == null ? new ArrayList() : factors;
            factors.add(factor);

            resultMatrix.put(factor.getFactor(), factors);
        }


        return resultMatrix;
    }
    
    /**
     * Metodo para el pintado de las graficas
     * @param departments
     * @param structureDate 
     */
    public static void getDashboardData(String[] departments, String structureDate) {
        List<Object> matrixData = new ArrayList();
        
        matrixData.add(getFactors(departments, structureDate));
        matrixData.add(getSubfactors(departments, structureDate));
        
        renderJSON(matrixData);
    }
    
    /**
     * Metodo que obtiene los factores por departamento y fecha seleccionados
     * @param departments
     * @param structureDate
     * @return 
     */
    public static LinkedHashMap<String, List<ResultMatrixList>> getFactors(String[] departments, String structureDate) {
        try {
            List<ResultMatrixList> factores = MatrixQuery.getFactores(departments, structureDate);
            return setFactores(factores);
        } catch (SQLException ex) {
            Logger.error(ex.getMessage());
        }
        
        return null;
    }
    
    /**
     * Metodo que obitne los subfactores por departamento y fecha seleccionados
     * @param departments
     * @param structureDate
     * @return 
     */
    public static HashMap<String, List<ResultMatrixList>> getSubfactors(String[] departments, String structureDate) {
        try {
            List<ResultMatrixList> subFactors = MatrixQuery.getSubFactor(departments, structureDate);
            HashMap<String, List<ResultMatrixList>> resultMatrix = setFactores(subFactors);
            return setSubfactores(resultMatrix);
        } catch (SQLException ex) {
            Logger.error(ex.getMessage());
        }
        
        return null;
    }
    
    /**
     * Metodo que obtiene las calificaciones de los factores por departameto y
     * fecha de estructura seleccionados
     * @param departments
     * @param structureDate 
     */
     public static void getFactores(String[] departments, String structureDate) {
        try {
            MatrixQuery queries = new MatrixQuery();
            List<ResultMatrixList> factores = queries.getFactores(departments, structureDate);

            LinkedHashMap<String, List<ResultMatrixList>> resultMatrix = setFactores(factores);
            List<ResultMatrixFactor> jsonFactor = new ArrayList();

            renderJSON(resultMatrix);
        } catch (SQLException ex) {
            Logger.error(ex.getMessage());
        }
    }
     
     /**
      * MEtodo que obtiene las calificaciones de los subfactores por medio de los
      * departamentos y fecha seleccionados
      * @param departments
      * @param structureDate 
      */
     public static void getSubfactores(String[] departments, String structureDate) {
        try {
            MatrixQuery queries = new MatrixQuery();
            List<ResultMatrixList> subFactors = queries.getSubFactor(departments, structureDate);
            HashMap<String, List<ResultMatrixList>> resultMatrix = setFactores(subFactors);
            HashMap<String, List<ResultMatrixList>> subfactores = setSubfactores(resultMatrix);
            renderJSON(subfactores);
        } catch (SQLException ex) {
            Logger.error(ex.getMessage());
        }


    }

     /**
      * Metodo que obtiene los resultados globales por periodo de estructura
      * @param level
      * @param structureDate 
      */
    public static void getResume(int level, String structureDate) {      
        ResumeTable resumeDirect = MatrixQuery.getResume(level, structureDate);
        ResumeTable resumeTree   = MatrixQuery.getResumeTree(level, structureDate);
        ResumeTable[] resumeTables = new ResumeTable[] {resumeDirect, resumeTree};
        
        renderJSON(resumeTables);
    }
     
    /**
     * Metodo que obtiene los permisos especiales
     */
    public static void getSpecialPermissions(String date){
        Queries queries = new Queries();
        int userId = Integer.valueOf(session.get("userId"));
        
        List<PermisosEspeciales> permisos = Queries.getPermisosEspeciales(userId);
        
        List data = new ArrayList();
        List<SpecialPermissions> specialPermissions = new ArrayList();
        
        if (!permisos.isEmpty()) {
            for (PermisosEspeciales permiso : permisos) {
                SpecialPermissions specialPermission = new SpecialPermissions();
                
                switch(Integer.valueOf(permiso.typePermit)){
                    case 1: //Solo su estructura
                        
                    break;
                    case 2: //Su estuctura y los pares
                        /// sacar hermanos
                        int level = Integer.valueOf(session.get("level"));
                        specialPermission.setLevel(level);
                        specialPermission.setData(queries.getParesPermisosEspeciales(userId, date));
                        
                    break;
                    case 3: //Por nivel
                        int searchLevel = Integer.valueOf(permiso.permitValue);
                        specialPermission.setLevel(searchLevel);
                        specialPermission.setData(queries.getLevelPermisosEspeciales(searchLevel, date));
                    break;
                    case 4: // Por estructura de una persona en especifico
                        Long employeeNumber = Long.valueOf(permiso.permitValue);
                        EmployeeClima employee = EmployeeClima.find("select e from EmployeeClima e where e.employeeNumber = ?", employeeNumber).first();
                        specialPermission.setLevel(employee.vLevel);
                        specialPermission.setData(queries.getEstructuraPermisosEspeciales(employeeNumber, date));
                        
                    break;
                }
                
                specialPermissions.add(specialPermission);
            }
        }
        renderJSON(specialPermissions);
    }
    
    public static void getDepartment(String date) {
        String[] departmentEmployee = StructureQuery.getDepartmentName(
            Integer.valueOf(session.get("userId")), date
        );
        
        renderJSON(departmentEmployee);
    }
}
