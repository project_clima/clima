package controllers;

import java.sql.SQLException;
import play.*;
import play.mvc.*;
import java.util.*;
import json.entities.ResultMatrixFactor;
import json.entities.ResultMatrixList;
import json.entities.ResumeTable;
import json.entities.SpecialPermissions;
import models.*;
import org.apache.commons.lang3.tuple.Pair;
import play.db.jpa.GenericModel;
import querys.StructureQuery;
import querys.MatrixQuery;
import querys.MatrixQueryDirect;
import utils.Queries;

/**
 * Clase controlador para el manejo de la matriz de resultados
 * @author Rodolfo Miranda -- Qualtop
 */
@With(Secure.class)
public class ResultMatrix extends Controller {

    /**
     * Metodo que regresa las fechas de estructura en base de datos
     * @param structureDate 
     */
    public static void index(String structureDate) {
        List structureDates = StructureQuery.getDatesStructures();
        render(structureDates, structureDate);
    }

    /**
     * MEtodo que regresa los depatamentos del primer nivel de los filtros 
     * @param level
     * @param date 
     */
     public static void getLevels1(int level, String date){
        List levels = StructureQuery.getDepartmentsByLevel1(level, date);
        renderJSON(levels);
    }

     /**
      * Metodo generico para la busqued de los siguientes departamentos para los 
      * niveles de los filtros
      * @param department
      * @param date 
      */
    public static void getLevels(int department, String date) {
        List levels = StructureQuery.getDepartmentsByLevelAllowed(department, date);
        renderJSON(levels);
    }

    /**
     * Metodo que regresa los factores de la matriz
     * @param factors
     * @return 
     */
    private static LinkedHashMap<String, List<ResultMatrixList>> setFactores(List<ResultMatrixList> factors) {
        LinkedHashMap<String, List<ResultMatrixList>> resultMatrix = new LinkedHashMap();

        for (ResultMatrixList factor : factors) {
            factors = resultMatrix.get(factor.getFactor());
            factors = factors == null ? new ArrayList() : factors;
            factors.add(factor);

            resultMatrix.put(factor.getFactor(), factors);
        }


        return resultMatrix;
    }

    /**
     * Metodo que regresa la lista de subfactores
     * @param factores
     * @return 
     */
    public static  HashMap<String, List<ResultMatrixList>> setSubfactores(HashMap<String, List<ResultMatrixList>> factores) {
        HashMap<String, List<ResultMatrixList>> hashResult = new HashMap();

        for (Map.Entry<String, List<ResultMatrixList>> entry : factores.entrySet()) {
            List<ResultMatrixList> results = new ArrayList();

            for (ResultMatrixList resulMatrixList : entry.getValue()) {
                results = hashResult.get(resulMatrixList.getSubFactor());
                results = results == null ? new ArrayList() : results;
                results.add(resulMatrixList);

                hashResult.put(resulMatrixList.getSubFactor(), results);

            }
        }

        return hashResult;
    }

    /**
     * MEtodo que clasifica las preguntas por subfactor
     * @param subfactores
     * @return 
     */
    public static HashMap<String, List<ResultMatrixList>> setQuestions(HashMap<String, List<ResultMatrixList>> subfactores) {
        HashMap<String, List<ResultMatrixList>> hashResult = new HashMap();

        for (Map.Entry<String, List<ResultMatrixList>> entry : subfactores.entrySet()) {
            List<ResultMatrixList> results = new ArrayList();

            for (ResultMatrixList resultMatrix : entry.getValue()) {
                results = hashResult.get(resultMatrix.getQuestion());
                results = results == null ? new ArrayList() : results;
                results.add(resultMatrix);

                hashResult.put(resultMatrix.getQuestion(), results);
            }
        }

        return hashResult;
    }
    
    /**
     * Metodo que obtiene la matriz de resultados para las calificaciones directas
     * @param departments
     * @param structureDate 
     */
    public static void getMatrixData(String[] departments, String structureDate) {
        List<Object> matrixData = new ArrayList();
        
        matrixData.add(getFactors(departments, structureDate));
        matrixData.add(getSubfactors(departments, structureDate));
        matrixData.add(getQuestions(departments, structureDate));
        
        renderJSON(matrixData);
    }
    
    /**
     * MEtodo que obtiene la matriz de resultados para las calificaciones de su arbol
     * @param userId
     * @param structureDate 
     */
    public static void getMatrixDataDirect(String userId, String structureDate) {
        List<Object> matrixData = new ArrayList();
        
        matrixData.add(getFactorsDirect(userId, structureDate));
        matrixData.add(getSubfactorsDirect(userId, structureDate));
        matrixData.add(getQuestionsDirect(userId, structureDate));
        
        renderJSON(matrixData);
    }
    
    /**
     * MEtodo que obtiene los factores de sus calificaciones directas
     * @param userId
     * @param structureDate
     * @return 
     */
    private static LinkedHashMap<String, Float> getFactorsDirect(String userId, String structureDate) {
        try {
            List<ResultMatrixList> factorsList = MatrixQueryDirect.getFactors(userId, structureDate);
            
            LinkedHashMap<String, Float> resultMatrix = new LinkedHashMap();
            
            for (ResultMatrixList factor : factorsList) {
                resultMatrix.put(factor.getFactor(), factor.getScore());
            }

            return resultMatrix;
            
        }  catch (SQLException ex) {
            Logger.error(ex.getMessage());
        }
        
        return null;
    }
    
    /**
     * Metodo que obtiene la lista de subfactores de sus calificaciones directas
     * @param userId
     * @param structureDate
     * @return 
     */
    private static LinkedHashMap<String, List<String>> getSubfactorsDirect(String userId, String structureDate) {
        try {
            List<ResultMatrixList> subfactorsList = MatrixQueryDirect.getSubfactors(userId, structureDate);
            
            LinkedHashMap<String, List<String>> resultMatrix = new LinkedHashMap();
            List<String> subfactors;
            
            for (ResultMatrixList subfactor : subfactorsList) {
                subfactors = resultMatrix.get(subfactor.getFactor());
                subfactors = subfactors == null ? new ArrayList() : subfactors;
                
                subfactors.add(subfactor.getSubFactor() + "|" + subfactor.getScore());
                
                resultMatrix.put(subfactor.getFactor(), subfactors);
            }

            return resultMatrix;
            
        }  catch (SQLException ex) {
            Logger.error(ex.getMessage());
        }
        
        return null;
    }
    
    /**
     * MEtodo que obtiene sus preguntas de sus calificaciones directas
     * @param userId
     * @param structureDate
     * @return 
     */
    private static LinkedHashMap<String, List<String>> getQuestionsDirect(String userId, String structureDate) {
        try {
            List<ResultMatrixList> questionsList = MatrixQueryDirect.getQuestions(userId, structureDate);
            
            LinkedHashMap<String, List<String>> resultMatrix = new LinkedHashMap();
            List<String> subfactors;
            
            for (ResultMatrixList question : questionsList) {
                subfactors = resultMatrix.get(question.getSubFactor());
                subfactors = subfactors == null ? new ArrayList() : subfactors;
                subfactors.add(question.getQuestion() + "|" + question.getScore());
                
                resultMatrix.put(question.getSubFactor(), subfactors);
            }

            return resultMatrix;
            
        }  catch (SQLException ex) {
            Logger.error(ex.getMessage());
        }
        
        return null;
    }
    
    /**
     * Metodo que obtiene los factores directos
     * @param departments
     * @param structureDate
     * @return 
     */
    public static LinkedHashMap<String, List<ResultMatrixList>> getFactors(String[] departments, String structureDate) {
        try {
            List<ResultMatrixList> factores = MatrixQuery.getFactores(departments, structureDate);
            return setFactores(factores);
        } catch (SQLException ex) {
            Logger.error(ex.getMessage());
        }
        
        return null;
    }
    
    /**
     * MEtodo que obtiene los subfactores de su arbol
     * @param departments
     * @param structureDate
     * @return 
     */
    public static HashMap<String, List<ResultMatrixList>> getSubfactors(String[] departments, String structureDate) {
        try {
            List<ResultMatrixList> subFactors = MatrixQuery.getSubFactor(departments, structureDate);
            HashMap<String, List<ResultMatrixList>> resultMatrix = setFactores(subFactors);
            return setSubfactores(resultMatrix);
        } catch (SQLException ex) {
            Logger.error(ex.getMessage());
        }
        
        return null;
    }
    
    /**
     * MEtodo que obtiene las preguntas dependiendo la fecha de estructura y los filtros
     * @param departments
     * @param structureDate
     * @return 
     */
    public static HashMap<String, List<ResultMatrixList>> getQuestions(String[] departments, String structureDate) {
         try {
            List questionsList = MatrixQuery.getQuestion(departments, structureDate);
            HashMap<String, List<ResultMatrixList>> resultMatrix = setFactores(questionsList);
            HashMap<String, List<ResultMatrixList>> subfactores  = setSubfactores(resultMatrix);
            return setQuestions(subfactores);
         } catch (SQLException ex) {
             Logger.error(ex.getMessage());
         }
         
         return null;
    }

    public static void getFactores(String[] departments, String structureDate) {
        try {
            MatrixQuery queries = new MatrixQuery();
            List<ResultMatrixList> factores = queries.getFactores(departments, structureDate);

            LinkedHashMap<String, List<ResultMatrixList>> resultMatrix = setFactores(factores);
            List<ResultMatrixFactor> jsonFactor = new ArrayList();

            renderJSON(resultMatrix);
        } catch (SQLException ex) {
            Logger.error(ex.getMessage());
        }
    }

    public static void getSubfactores(String[] departments, String structureDate) {
        try {
            MatrixQuery queries = new MatrixQuery();
            List<ResultMatrixList> subFactors = queries.getSubFactor(departments, structureDate);
            HashMap<String, List<ResultMatrixList>> resultMatrix = setFactores(subFactors);
            HashMap<String, List<ResultMatrixList>> subfactores = setSubfactores(resultMatrix);
            renderJSON(subfactores);
        } catch (SQLException ex) {
            Logger.error(ex.getMessage());
        }


    }

     public static void getQuestion(String[] departments, String structureDate) {
         try {

            MatrixQuery queries = new MatrixQuery();

             List questionsList = queries.getQuestion(departments, structureDate);
             HashMap<String, List<ResultMatrixList>> resultMatrix = setFactores(questionsList);
             HashMap<String, List<ResultMatrixList>> subfactores = setSubfactores(resultMatrix);
             HashMap<String, List<ResultMatrixList>> questions = setQuestions(subfactores);
             renderJSON(questions);

         } catch (SQLException ex) {
             Logger.error(ex.getMessage());
         }
    }

    public static void getResume(int level, String structureDate) {      
        ResumeTable resumeDirect = MatrixQuery.getResume(level, structureDate);
        ResumeTable resumeTree   = MatrixQuery.getResumeTree(level, structureDate);
        ResumeTable[] resumeTables = new ResumeTable[] {resumeDirect, resumeTree};
        
        renderJSON(resumeTables);
    }

    public static void getSpecialPermissions(String date){
        Queries queries = new Queries();
        int userId = Integer.valueOf(session.get("userId"));
        
        List<PermisosEspeciales> permisos = Queries.getPermisosEspeciales(userId);
        
        List data = new ArrayList();
        List<SpecialPermissions> specialPermissions = new ArrayList();
        
        if (!permisos.isEmpty()) {
            for (PermisosEspeciales permiso : permisos) {
                SpecialPermissions specialPermission = new SpecialPermissions();
                
                switch(Integer.valueOf(permiso.typePermit)){
                    case 1: //Solo su estructura
                        
                    break;
                    case 2: //Su estuctura y los pares
                        /// sacar hermanos
                        int level = Integer.valueOf(session.get("level"));
                        specialPermission.setLevel(level);
                        specialPermission.setData(queries.getParesPermisosEspeciales(userId, date));
                        
                    break;
                    case 3: //Por nivel
                        int searchLevel = Integer.valueOf(permiso.permitValue);
                        specialPermission.setLevel(searchLevel);
                        specialPermission.setData(queries.getLevelPermisosEspeciales(searchLevel, date));
                    break;
                    case 4: // Por estructura de una persona en especifico
                        Long employeeNumber = Long.valueOf(permiso.permitValue);
                        EmployeeClima employee = EmployeeClima.find("select e from EmployeeClima e where e.employeeNumber = ? "
                                + " and TO_CHAR (e.structureDate, 'MM/YYYY') = ?", employeeNumber, date).first();
                        specialPermission.setLevel(employee.vLevel);
                        specialPermission.setData(queries.getEstructuraPermisosEspeciales(employeeNumber, date));
                        
                    break;
                }
                
                specialPermissions.add(specialPermission);
            }
        }
        renderJSON(specialPermissions);
    }

}
