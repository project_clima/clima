package controllers;

import play.mvc.*;
import java.util.*;
import models.*;
import controllers.csi.SnapEvaluationResults;
import models.csi.CsiEvaluationCross;
import play.Logger;

public class SurveyCSI extends Controller {
    @Before(unless={"apply"})
    static void checkAccess() throws Throwable {
        Secure.checkAccess();
    }
    
    public static void apply(Integer surveyId, String userId) throws Throwable {
        if (userId == null) {
            checkAccess();
        } else {
            authenticate(userId, surveyId);
        }
        
        Survey survey = Survey.findById(surveyId);
        
        if (!isValidSurvey(survey)) {
            session.clear();
            controllers.Secure.login();
        }
        
        List questions = utils.Survey.getSurveyQuestions(surveyId);        
        List<EmployeeCSI> evaluated = getEmployeesToEvaluate(surveyId);
        
        response.setHeader("Cache-Control", "no-cache, no-store, max-age=0, must-revalidate, post-check=0, pre-check=0");
        render(surveyId, survey, questions, evaluated);
    }
    
    private static List<EmployeeCSI> getEmployeesToEvaluate(Integer surveyId) {
        Integer userId = Integer.valueOf(session.get("userId"));
        
        StringBuilder query = new StringBuilder();
        
        query.append("select distinct employee.id, employee.firstName, employee.lastName")
            .append(" from EmployeeCSI employee, CsiEvaluationCross evaluation")
            .append(" where employee.id = evaluation.idEvaluatedEmployee")
            .append(" and evaluation.idSurvey = ?1 and evaluation.idEvaluatorEmployee = ?2")
            .append(" and evaluation.idEvaluatedEmployee NOT IN (")
            .append(" select distinct idEmployee from CSIEvaluationResults where")
            .append(" idEvaluator = ?3 and idSurvey = ?4")
            .append(" )")
            .append(" order by employee.firstName, employee.lastName asc");
        
        List<EmployeeCSI> evaluated =  EmployeeCSI.find(
            query.toString(), surveyId, userId, userId, surveyId
        ).fetch();
        
        if (evaluated.isEmpty()) {
            try {
                flash.error("La encuesta ya fue contestada en su totalidad.");
                controllers.Secure.login();
            } catch (Throwable ex) {
                Logger.error(ex.getMessage());
            }            
        }
        
        return evaluated;
    }
    
    private static void authenticate(String userId, Integer surveyId) throws Throwable {
        session.clear();
        
        if (controllers.Security.authenticate(userId, surveyId)) {
            apply(surveyId, null);
        } else {
            flash.error("Encuesta no válida.");
            controllers.Secure.login();
        }
    }
    
    private static Boolean isValidSurvey(Survey survey) {
        if (survey == null) {
            return false;
        }
        
        CsiEvaluationCross cross = CsiEvaluationCross.find(
            "idEvaluatorEmployee = ?1 and idSurvey = ?2",
            Integer.valueOf(session.get("userId")), survey.id
        ).first();
        
        return cross != null && inTime(survey);
    }
    
    private static Boolean inTime(Survey survey) {
        Date currentDate = new Date();
        Date endDate     = getEndDate(survey.dateEnd);
        
        Boolean inTime   = currentDate.after(survey.dateInitial) && 
                           currentDate.before(endDate);
        
        if (!inTime) {
            flash.error("La encuesta no se encuentra vigente.");
        }
        
        return inTime;
    }
    
    private static Date getEndDate(Date endDate) {
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(endDate);
        calendar.add(Calendar.DATE, 1);
        
        return calendar.getTime();
    }
    
    public static void save(Integer surveyId, Map<String, String> answers,
                            Map<String, String> answersAlternative, Boolean close) {
        if (answers.isEmpty()) {
            response.status = 400;
            renderJSON(new utils.Message("Invalid data"));
        }
        
        for (String key : answers.keySet()) {
            String[] keyPair  = key.split("_"); //[0] Question Id, [1] Evaluated Id
            Question question = Question.findById(Integer.valueOf(keyPair[0]));
            
            if (question == null) {
                continue;
            }
            
            Integer result = question.questionType.name.equals("NUMERICA") ? Integer.valueOf(answers.get(key)) : null;
            String resultText = question.questionType.name.equals("ABIERTAS") ? answers.get(key) : null;
            
            saveAnswers(
                result, resultText, answersAlternative.get(key), surveyId,
                Integer.valueOf(keyPair[0]), Integer.valueOf(keyPair[1])
            );
        }
        
        if (close != null && close == true) {
            recalculateResults(surveyId);
        }
        
        renderJSON(answers);
    }
    
    private static void saveAnswers(Integer result, String resultText, String comment,
                                    Integer surveyId, Integer questionId, Integer evaluated) {
        CSIEvaluationResults csiResult = new CSIEvaluationResults();
        
        csiResult.result      = result;
        csiResult.resultText  = resultText;
        csiResult.comments    = comment;
        csiResult.idEmployee  = evaluated;
        csiResult.idEvaluator = Integer.valueOf(session.get("userId"));
        csiResult.idQuestion  = questionId;
        csiResult.idSurvey    = surveyId;
        csiResult.createdBy   = session.get("username");
        csiResult.createdDate = new Date();
        
        csiResult.save();
    }

    private static void recalculateResults(Integer surveyId) {        
        SnapEvaluationResults.loadSnapResults(surveyId);
    }
}