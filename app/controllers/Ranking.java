package controllers;

import java.io.IOException;
import java.sql.SQLException;
import play.*;
import play.mvc.*;
import java.util.*;
import java.util.logging.Level;
import models.*;
import net.sf.jxls.exception.ParsePropertyException;
import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import querys.RankingQuery;
import querys.SearchQuery;
import querys.StructureQuery;
import utils.RenderExcel;

/**
 * Clase controlador para el manejo del ranking de Clima
 * @author Rodolfo Miranda -- Qualtop
 */
@With(Secure.class)
public class Ranking extends Controller {

    /**
     * MEtodo principal el cual regresa las fechas de estructuras existentes
     * en base de datos
     */
    public static void index() {
        List structureDates = StructureQuery.getDatesStructures();
        render(structureDates);
    }

    /**
     * Metodo que devuelve el ranking por usuarios y por fecha
     * @param id
     * @param date 
     */
    public static void getRankingByUser(String id, String date) {
        RankingQuery queries = new RankingQuery();

        List elements = queries.getRankingByUser(id, date);
        renderJSON(elements);
    }

    /**
     * Metodo para obtener el ranking por fecha y filtros
     * @param option
     * @param date 
     */
    public static void getRankingByOption(String option, String date) {
        RankingQuery queries = new RankingQuery();
        Calendar calendar = new GregorianCalendar();
        String[] split = date.split("/");
        List elements = new ArrayList();
        try {
            
            int year = 0;
            int month = 0;

            if(split.length > 1){
                year = Integer.parseInt(split[1]);
                month = Integer.parseInt(split[0]);
            }

            calendar.set(year, (month - 1), 1);

            Date firstDate = calendar.getTime();

            int maxDay = calendar.getActualMaximum(Calendar.DAY_OF_MONTH);

            calendar.set(year, (month - 1), maxDay);

            Date endDate = calendar.getTime();

            elements = queries.getRankingByOption(option, firstDate, endDate);
            
        } catch (SQLException ex) {
            java.util.logging.Logger.getLogger(Ranking.class.getName()).log(Level.SEVERE, null, ex);
        }
        renderJSON(elements);
    }
    
    /**
     * Metodo para exportar los resultados mostrados
     * @param option
     * @param date
     * @throws ParsePropertyException
     * @throws InvalidFormatException
     * @throws IOException 
     */
    public static void export(String option, String date)
    throws ParsePropertyException, InvalidFormatException, IOException {
        Calendar calendar = new GregorianCalendar();
        String[] split = date.split("/");
                
        List results = new ArrayList();
        try {
            int year = 0;
            int month = 0;

            if(split.length > 1){
                year = Integer.parseInt(split[1]);
                month = Integer.parseInt(split[0]);
            }

            calendar.set(year, (month - 1), 1);

            Date firstDate = calendar.getTime();

            int maxDay = calendar.getActualMaximum(Calendar.DAY_OF_MONTH);

            calendar.set(year, (month - 1), maxDay);

            Date endDate = calendar.getTime();

            results = RankingQuery.getRankingByOption(option, firstDate, endDate);
        } catch (SQLException ex) {
            java.util.logging.Logger.getLogger(Ranking.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        renderArgs.put("rows", results);
        renderArgs.put(RenderExcel.RA_FILENAME, "Ranking.xls");
        RenderExcel renderer = new RenderExcel("app/excel/views/Ranking/RankingAreaReporte.xls");
        renderer.render();
    }
}
