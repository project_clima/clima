package controllers;

import play.*;
import play.mvc.*;
import java.util.*;
import models.*;
import play.db.jpa.JPA;
import com.google.gson.*;

@With(Secure.class)
public class RolesCSI extends Seguridad {
    /**
     * Muestra la página de roles
     */
	public static void roles() {
    	String place = "CSI";
        renderTemplate("Seguridad/roles.html", place);
    }

    /**
     * Muesta la página de permisos según el id del rol y el lugar en el que se encuentra
     * @param id
     * @param place
     */
    public static void permisos(Long id, String place) {
        Rol oRol = Rol.findById(id);

        if (oRol == null) {
            roles();
        }

        List<Seccion> lstSecciones = Seccion.find("idModule = ?1", Seguridad.MODULE_CSI).fetch();
        renderTemplate("Seguridad/permisos.html", lstSecciones, oRol, place);
    }

    /**
     * Muestra la página de permisos especiales según el id del rol y el lugar en el que se encuentra
     * @param id
     * @param place
     */
    public static void permisosEspeciales(Long id, String place) {
        Rol oRol = Rol.findById(id);

        if (oRol == null) {
            roles();
        }

        PermisosEspeciales permisosEspeciales = new PermisosEspeciales();
        PermisosEspeciales lstEspeciales = permisosEspeciales.find("role", oRol).first();
        renderTemplate("Seguridad/permisosEspeciales.html", lstEspeciales, oRol, place);
    }
}
