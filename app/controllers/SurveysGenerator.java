package controllers;

import com.google.gson.Gson;
import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import play.mvc.*;
import java.util.*;
import json.entities.TreeData;
import models.*;
import net.sf.jxls.exception.ParsePropertyException;
import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import play.Logger;
import play.data.validation.Valid;
import play.data.validation.Validation;
import play.db.jpa.JPA;
import querys.SurveysGeneratorQuery;
import utils.Pagination;
import utils.RenderExcel;

/**
 * Clase controlador para la generacion de encuestas en Clima
 * @author Rodolfo Miranda -- Qualtop
 */
@With(Secure.class)
public class SurveysGenerator extends Controller {
    @Check("encuestas/list")
    public static void index() {
        render();
    }
    
    /**
     * Metodo para guardar encuesta
     */
    @Check("surveysgenerator/addSurvey")
    public static void addSurvey() {
        List<SurveyType> surveyTypes = SurveyType.findAll();
        List<Integer> years = getYearsRange();
        
        render(surveyTypes, years);
    }
    
    /**
     * MEtodo para obtener el rango de año por encuesta
     * @return 
     */
    private static List<Integer> getYearsRange() {
        int minYear = querys.SurveysGeneratorQuery.getYearStructure("min");
        int maxYear = querys.SurveysGeneratorQuery.getYearStructure("max");
        
        List<Integer> years = new ArrayList();
        for (int year = minYear; year <= maxYear; year++) {
            years.add(year);
        }
        
        return years;
    }
    
    /**
     * MEtodo para obtener el inicio de la estructura por año y encuesta
     * @param surveyId
     * @param month
     * @param year 
     */
    public static void getRootStructure(int surveyId, String month, String year) {
        List<TreeData> data = querys.SurveysGeneratorQuery.getRoot(surveyId, month, year);
        renderJSON(data);
    }
    
    /**
     * Metodo para obtener la esgtructura por encuesta, tipo de encuesta, 
     * año y id de usuario
     * @param id
     * @param type
     * @param month
     * @param year
     * @param surveyId 
     */
    public static void getStructure(String id, String type, String month, String year, int surveyId) {
        List<TreeData> data = querys.SurveysGeneratorQuery.structure(id, type, month, year, surveyId);
        renderJSON(data);
    }
    
    /**
     * Metodo para obtener la lista de encuestas
     * @param iDisplayLength
     * @param startDate
     * @param endDate
     * @param iDisplayStart
     * @param sEcho
     * @param sSortDir_0
     * @param iSortCol_0
     * @param sSearch 
     */
    public static void list(int iDisplayLength, String startDate,
                              String endDate, int iDisplayStart, String sEcho,
                              String sSortDir_0, int iSortCol_0, String sSearch) {
        
        int start        = iDisplayStart;
        int end          = iDisplayLength;
        long count       = Survey.count("LOWER(name) LIKE LOWER(?1) and surveyType.id not in (4,5)", "%" + sSearch + "%");
        
        List surveys     = Survey.find(
            "select s.id, s.name, t.subType, TO_CHAR(s.creationDate, 'DD/MM/YYYY')," +
            "TO_CHAR(s.modificationDate, 'DD/MM/YYYY'), u.nombre," +                    
            "s.id from Survey s LEFT JOIN s.surveyType t LEFT JOIN s.modifiedBy u " +
            "where LOWER(s.name) LIKE LOWER(?1) and t.id not in (4,5) " +
            "order by s.id asc", "%" + sSearch + "%"
        ).from(start).fetch(end);        
        
        renderJSON(new Pagination(sEcho, count, count, surveys));
    }
    
    @Check("surveysgenerator/editSurvey")
    public static void sections(int surveyId) {
        Survey survey = Survey.findById(surveyId);
        
        render(survey);
    }
    
    /**
     * Metodo para guardar encuesta creada
     * @param survey
     * @param month
     * @param year
     * @param userIds
     * @param deselectedUserIds
     * @throws ParseException 
     */
    public static void save(@Valid Survey survey, String month, String year, String userIds, String deselectedUserIds)
    throws ParseException {
        if(validation.hasErrors()) {
            renderJSON(new utils.Message(
                "Error al guardar la información. Por favor, revise el formulario cuidadosamente.",
                null,
                true)
            );
        }
        
        if (survey.id == null) {            
            survey.createdBy  = session.get("username");
            survey.creationDate = new Date();
        }
        
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        
        survey.surveyDate = sdf.parse(year + "-" + month + "-01");
        survey.modifiedBy = Usuario.find("username = ?1", session.get("username")).first();
        survey.modificationDate = new Date();
        
        survey.save();
        querys.SurveysGeneratorQuery.deleteSurveyEmployees(survey.id, deselectedUserIds);
        querys.SurveysGeneratorQuery.saveSurveyEmployees(survey, userIds, month, year);
        
        renderJSON(new utils.Message(survey.id.toString(), "validation", false));
    }
    
    /**
     * Metodo para guardar las secciones
     * @param survey 
     */
    public static void saveSection(@Valid Survey survey) {
        if(validation.hasErrors()) {
            renderJSON(new utils.Message(
                "Error al guardar la información. Por favor, revise el formulario cuidadosamente.",
                null,
                true)
            );
        }
        
        survey.modifiedBy = Usuario.find("username = ?1", session.get("username")).first();
        survey.modificationDate = new Date();
        
        survey.save();        
        renderJSON(new utils.Message(survey.id.toString(), "validation", false));
    }
    
    /**
     * Metodo para editar las encuetas
     * @param section
     * @param surveyId 
     */
    public static void editSurveySection(String section, int surveyId) {
        Survey survey              = Survey.findById(surveyId);
        List<QuestionType> types   = QuestionType.find("id not in (5, 6)").fetch();
        List<Factor> factors       = Factor.find("survey.id = ?1", surveyId).fetch();
        
        String template = "SurveysGenerator/" + section + "-section.html";
        
        renderArgs.put("survey", survey);
        renderArgs.put("types", types);
        renderArgs.put("factors", factors);
        renderTemplate(template);
    }
    
    /**
     * Metodo para las encuestas por tipo
     * @param surveyId 
     */
    public static void getSurvey(int surveyId) {
        Survey survey   = Survey.findById(surveyId);
        renderJSON(survey);
    }
    
    /**
     * Metodo para obtener los campos a editar en una encuesta
     * @param surveyId 
     */
    @Check("surveysgenerator/editSurvey")
    public static void editSurvey(int surveyId) {
        Survey survey = Survey.findById(surveyId);
        List<SurveyType> surveyTypes = SurveyType.findAll();
        List<Integer> years = getYearsRange();
        
        SimpleDateFormat sfdM = new SimpleDateFormat("MM");
        SimpleDateFormat sfdY = new SimpleDateFormat("yyyy");
        
        String month = survey.surveyDate != null ? sfdM.format(survey.surveyDate) : "";
        String year  = survey.surveyDate != null ? sfdY.format(survey.surveyDate) : "";
        render(survey, surveyTypes, month, year, years);
    }
    
    /**
     * Mtodo para eliminar una encuesta
     * @param surveyId 
     */
    @Check("surveysgenerator/deleteSurvey")
    public static void deleteSurvey(int surveyId) {
        Survey survey = Survey.findById(surveyId);
        
        if (utils.Survey.inTime(survey)) {
            renderJSON(new utils.Message("La encuesta no puede ser eliminada porque se encuentra vigente.", null, true));
        }
        
        List<Question> questions = Question.find("survey.id = ?1", surveyId).fetch();
        List<Factor> factors     = Factor.find("survey.id = ?1", surveyId).fetch();
        
        try {
            for (Question question : questions) {
                QuestionChoices.delete("question.id = ?1", question.id);
                question.delete();
            }
            
            SurveysGeneratorQuery.deleteSubfactorScore(surveyId);
            
            for (Factor factor : factors) {
                SubFactor.delete("factor.id = ?1", factor.id);
                factor.delete();
            }
            
            SurveyEmployee.delete("survey.id = ?1", surveyId);
            Survey.delete("id = ?1", surveyId);
            
            renderJSON(new utils.Message("Encuesta eliminada exitosamente.", null, false));
        } catch (Exception e) {
            Logger.error(e.getMessage());
            renderJSON(new utils.Message("La encuesta no pudo ser eliminada. Por favor, intente nuevamente.", null, true));
        }
    }
    
    /**
     * Metodo para copiar encuesta
     * @param surveyId 
     */
    @Check("surveysgenerator/copy")
    public static void copy(int surveyId) {
        Survey survey =  Survey.findById(surveyId);
        
        try {
            copySurvey(survey);            
            renderJSON(new utils.Message(null, null, false));
        } catch (Exception e) {
            Logger.error(e.getMessage());
        }
    }
    
    /**
     * Metodo para exportar los resultados mostrados en pantalla
     * @param surveyId
     * @throws ParsePropertyException
     * @throws InvalidFormatException
     * @throws IOException 
     */
    @Check("surveysgenerator/export")
    public static void export(int surveyId)
    throws ParsePropertyException, InvalidFormatException, IOException{
        Survey survey = Survey.findById(surveyId);
        LinkedHashMap<String, List<Question>> questions = getSurveyQuestions(surveyId);
        
        renderArgs.put("questions", questions);
        renderArgs.put("survey", survey);
        renderArgs.put(RenderExcel.RA_FILENAME, survey.name + ".xls");
        RenderExcel renderer = new RenderExcel("app/excel/views/Surveys/export.xls");
        renderer.render();
    }
    
    /**
     * Metodo que muesta la vista previa de una encuesta
     * @param surveyId 
     */
    public static void preview(int surveyId) {
        Survey survey  = Survey.findById(surveyId);
        List questions = utils.Survey.getSurveyQuestions(surveyId);        
        
        response.setHeader("Cache-Control", "no-cache, no-store, max-age=0, must-revalidate, post-check=0, pre-check=0");
        render(surveyId, survey, questions);
    }
    
    /**
     * Metodo para copiar una encuesta
     * @param original
     * @throws ParseException 
     */
    private static void copySurvey(Survey original) throws ParseException {
        Survey clone = new Survey();
        
        clone.name             = original.name + " - Copia de Encuesta";
        clone.initialSection   = original.initialSection;
        clone.endSection       = original.endSection;        
        clone.dateInitial      = original.dateInitial;
        clone.dateEnd          = original.dateEnd;
        clone.surveyDate       = original.surveyDate;
        clone.surveyType       = original.surveyType;
        clone.creationDate     = new Date();
        clone.modificationDate = new Date();
        clone.createdBy        = session.get("username");
        clone.modifiedBy       = Usuario.find("username = ?1", session.get("username")).first();
        
        clone.save();
        
        copyQuestions(clone, original.questions);
        copyFactors(original, clone);
        copySurveyEmployees(original.id, clone.id, session.get("username"));
    }
    
    /**
     * MEtodo que copia las preguntas de una encuesta a otra
     * @param survey
     * @param questions 
     */
    private static void copyQuestions(Survey survey, List<Question> questions) {
        for (Question question : questions) {
            Question cloneQuestion = new Question();
            
            cloneQuestion.question         = question.question;
            cloneQuestion.weighing         = question.weighing;
            cloneQuestion.weighingPercent  = question.weighingPercent;            
            cloneQuestion.order            = question.order;
            cloneQuestion.required         = question.required;            
            cloneQuestion.survey           = survey;
            cloneQuestion.subFactor        = question.subFactor;
            cloneQuestion.questionType     = question.questionType;
            cloneQuestion.creationDate     = new Date();
            cloneQuestion.modificationDate = new Date();
            cloneQuestion.createdBy        = survey.createdBy;
            cloneQuestion.modifiedBy       = survey.modifiedBy;
            
            cloneQuestion.save();
            
            copyChoices(cloneQuestion, question.choices);
        }
    }
    
    /**
     * Metodo que copias las opciones de una encuesta a otra
     * @param question
     * @param choices 
     */
    private static void copyChoices(Question question, List<QuestionChoices> choices) {
        for (QuestionChoices choice : choices) {
            QuestionChoices cloneChoice = new QuestionChoices();
            
            cloneChoice.choiceText         = choice.choiceText;
            cloneChoice.choiceValue        = choice.choiceValue;
            cloneChoice.choiceNumericValue = choice.choiceNumericValue;
            cloneChoice.creationDate       = new Date();
            cloneChoice.modificationDate   = new Date();
            cloneChoice.createdBy          = question.createdBy;
            cloneChoice.modifiedBy         = question.createdBy;
            cloneChoice.question           = question;
            
            cloneChoice.save();
        }
    }
    
    /**
     * Metodo que copia los factores de una encuesta a otra
     * @param original
     * @param cloned 
     */
    private static void copyFactors(Survey original, Survey cloned) {
        List<Factor> factors = Factor.find("survey.id = ?1", original.id).fetch();
        
        for (Factor factor : factors) {
            Factor factorClone = new Factor();
            
            factorClone.description  = factor.description;            
            factorClone.percent      = factor.percent;
            factorClone.points       = factor.points;
            factorClone.createdBy    = cloned.createdBy;
            factorClone.modifiedBy   = cloned.createdBy;
            factorClone.createdDate  = new Date();
            factorClone.modifiedDate = new Date();
            factorClone.survey       = cloned;
            
            factorClone.save();
            copySubfactors(factorClone, factor.subfactors, cloned.id);
        }                
    }
    
    /**
     * Metodo que copia los subfactores de una encuesta a otra
     * @param factor
     * @param subfactors
     * @param surveyId 
     */
    private static void copySubfactors(Factor factor, List<SubFactor> subfactors, int surveyId) {        
        for (SubFactor subfactor : subfactors) {
            SubFactor subFactorClone = new SubFactor();
            
            subFactorClone.description  = subfactor.description;            
            subFactorClone.percent      = subfactor.percent;
            subFactorClone.points       = subfactor.points;
            subFactorClone.createdBy    = factor.createdBy;
            subFactorClone.modifiedBy   = factor.modifiedBy;
            subFactorClone.createdDate  = new Date();
            subFactorClone.modifiedDate = new Date();
            subFactorClone.factor       = factor;
            
            subFactorClone.save();
            SurveysGeneratorQuery.updateSubfactors(surveyId, subfactor.id, subFactorClone.id);
        }                
    }
    
    /**
     * MEtodo para obtener las preguntas por encuestas
     * @param surveyId
     * @return 
     */
    private static LinkedHashMap<String, List<Question>> getSurveyQuestions(int surveyId) {
        List<Question> surveyQuestions = Question.find("survey.id = ? ORDER BY questionType", surveyId).fetch();   
        
        LinkedHashMap<String, List<Question>> questionTypes = new LinkedHashMap<>();
        List<Question> questions;
        
        String keyType;
        
        for (Question question : surveyQuestions) {
            keyType = question.questionType.name;
            
            if (questionTypes.get(keyType) == null) {
                questions = new ArrayList<>();
            } else {
                questions = questionTypes.get(keyType);
            }
            
            questions.add(question);
            questionTypes.put(keyType, questions);
        }
        
        return questionTypes;
    }
    
    private static void copySurveyEmployees(int originalId, int copiedId, String username) {
        SurveysGeneratorQuery.copySurveyEmployees(originalId, copiedId, username);
    }
}