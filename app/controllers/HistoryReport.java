package controllers;

import com.google.gson.*;
import com.google.gson.reflect.TypeToken;
import java.lang.reflect.Type;
import java.text.ParseException;
import java.util.*;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.persistence.Query;
import models.vo.*;
import play.db.jpa.JPA;
import play.mvc.*;
import querys.nps.HistoricReportQuery;
import querys.nps.TrackingReportQuery;
import utils.DateUtils;
import utils.nps.util.FormatDateUtil;

/**
 * Clase para el manejo del Reporte Historico
 * @author Rodolfo Miranda -- Qualtop
 */

@With(Secure.class)
public class HistoryReport extends Controller {
    public static Gson gson = new GsonBuilder().setDateFormat("dd/mm/yyyy").create();

    /**
     * Metodo para el despliegue inicial
     */
    
    //@Check("Histórico")
    public static void index() {
        if (session.contains("permisos")) {
            String sessionPermisos = session.get("permisos");
            String permisos = sessionPermisos.substring(1, sessionPermisos.length() - 1);
            List<String> aPermisosUsuario = Arrays.asList(permisos.split("\\s*,\\s*"));

            if (aPermisosUsuario.contains("reportes/historicouser") ||
                    ( !aPermisosUsuario.contains("reportes/historicouser") && !aPermisosUsuario.contains("reportes/historicoall"))) {
                session.put("idHis", session.get("userId"));
            }
        }
        render();
    }

    /**
     * Metodo para obtener el nivel de Ubicaciones
     * @param businessId
     * @param groupId
     * @param startDate
     * @param endDate 
     */
    
    public static void getTemplateLocations(Integer businessId,
        Integer groupId, String startDate, String endDate) {
        HashMap<String, List> detailsByBusiness = new HashMap();

        detailsByBusiness = TrackingReportQuery.getBusinessDetail(
            null, groupId, businessId, startDate, endDate
        );

        renderArgs.put("details", detailsByBusiness);
        renderArgs.put("MONTHS", DateUtils.MONTHS);
        renderTemplate("HistoryReportTemplates/locations.html");
    }

    /**
     * Metodo para obtener el detalle del Reporte Historico para las
     * ubicaciones. 
     * @param jsonData
     * @param highestMonth
     * @param lowestMonth
     * @param currYear
     * @param prevYear
     * @param area 
     */
    
    public static void getDetailLocations(String jsonData, Integer highestMonth, Integer lowestMonth,
        Integer currYear, Integer prevYear, String area) {
        Type theType = new TypeToken<HashMap<String, HistoryNPSRow>>(){}.getType();
        HashMap<String, HistoryNPSRow> data = gson.fromJson(jsonData, theType);
        List list = new ArrayList();

        for (HistoryNPSRow value : data.values()) {
            list.add(value);
        }
        
        renderArgs.put("list", list);
        renderArgs.put("lowestMonth", lowestMonth);
        renderArgs.put("highestMonth", highestMonth);
        renderArgs.put("currYear", currYear);
        renderArgs.put("prevYear", prevYear);
        renderArgs.put("area", area);
        renderArgs.put("MONTHS", DateUtils.MONTHS);

        renderTemplate("HistoryReportTemplates/locations.html");
    }

    /**
     * Metodo para obtener el siguiente template en la logica de negocio
     * 
     * @param level
     * @param jsonData
     * @param lowestMonth
     * @param highestMonth
     * @param groupId
     * @param businessId
     * @param zoneId
     * @param storeId
     * @param managerId
     * @param bossId
     * @param sectionId
     * @param area 
     */
    
    public static void renderTemplateTable(String level, String jsonData,
        Integer lowestMonth,
        Integer highestMonth,
        Integer groupId,
        Integer businessId,
        String zoneId,
        Integer storeId,
        Integer managerId,
        Integer bossId,
        Integer sectionId,
        String area) {

        String template = "HistoryReportTemplates/" + level + ".html";

        Type theType = new TypeToken<HashMap<String, HistoryNPSRow>>(){}.getType();
        HashMap<String, HistoryNPSRow> data = gson.fromJson(jsonData, theType);

        List<HistoryNPSRow> list = new ArrayList<HistoryNPSRow>();
        for (HistoryNPSRow value : data.values()) {
            list.add(value);
        }

        Integer currYear = session.get("yearH") != null ? Integer.parseInt(session.get("yearH")) :
                            Calendar.getInstance().get(Calendar.YEAR);
        Integer prevYear = currYear - 1;

        if (groupId != null && businessId != null) {
            List areas = getAreas(groupId,businessId);
            renderArgs.put("areas",areas);
        }
  
        renderArgs.put("list", list);
        renderArgs.put("lowestMonth", lowestMonth);
        renderArgs.put("highestMonth", highestMonth);
        renderArgs.put("currYear", currYear);
        renderArgs.put("prevYear", prevYear);
        renderArgs.put("MONTHS", DateUtils.MONTHS);

        renderArgs.put("groupId", groupId);
        renderArgs.put("area", area);
        renderArgs.put("businessId", businessId);   
        renderArgs.put("zoneId", zoneId);
        renderArgs.put("storeId", storeId);
        renderArgs.put("managerId", managerId);
        renderArgs.put("bossId", bossId);
        renderArgs.put("sectionId", sectionId);
        renderArgs.put("MONTHS", DateUtils.MONTHS);
        renderTemplate(template);
    }

    /**
     * Metodo para obtener los datos del siguiente nivel en el reporte
     * @param level
     * @param currLevel
     * @param idQuestion
     * @param groupId
     * @param businessId
     * @param area
     * @param zoneId
     * @param storeId
     * @param managerId
     * @param bossId
     * @param sectionId
     * @param startDate
     * @param endDate
     * @param isSelect 
     */
    
    //@Check("Histórico")
    public static void getLevelData(String level, String currLevel, Integer idQuestion,
            Integer groupId, Integer businessId, String area,
            String zoneId, Integer storeId, Integer managerId,
            Integer bossId, Integer sectionId, String startDate,
            String endDate, Boolean isSelect) {
        
        String user = session.get("idHis");
        Integer userId = user != null ? Integer.parseInt( user ) : null;

        String option = "T";
        if (area != null && !area.equals("")) {
            option = "A";
        }

        if (idQuestion == null) {
            idQuestion = 202;
        }

        int year = startDate != null && !startDate.equalsIgnoreCase("")
                ? Integer.parseInt(startDate.split("/")[2]) : Calendar.getInstance().get(Calendar.YEAR);

        startDate = startDate != null && !startDate.equalsIgnoreCase("")
                ? startDate : "01/01/" + year;

        endDate = endDate != null && !endDate.equalsIgnoreCase("")
                ? endDate : "31/12/" + year;

        HashMap<String, List> results = new HashMap();

        //Llamado para obtner los datos por medio de una funcion ORACLE
        results = HistoricReportQuery.getData(
                userId, idQuestion, groupId, businessId, area, zoneId, storeId, managerId,
                bossId, sectionId, startDate, endDate, option
        );

        String first = startDate != null && !startDate.equalsIgnoreCase("")
                ? getStartDate(startDate) : "01/01/" + (year - 1);
        String last = startDate != null && !startDate.equalsIgnoreCase("")
                ? getEndDate(endDate) : "31/12/" + (year - 1);

        HashMap<String, List> resultsPast = new HashMap();

        //Llamado para obtner los datos por medio de una funcion ORACLE
        resultsPast = HistoricReportQuery.getData(
                userId, idQuestion, groupId, businessId, area, zoneId, storeId, managerId,
                bossId, sectionId, first, last, option
        );

        HashMap<String, List> detailsByBusiness = new HashMap();
        HashMap<String, List> detailsByBusinessPast = new HashMap();

        if (businessId != null && (currLevel != null && currLevel.equals("business"))) {
            //Llamado para obtner los datos por medio de una funcion ORACLE
            detailsByBusiness = HistoricReportQuery.getBusinessDetail(
                userId, idQuestion, groupId, businessId, area, startDate, endDate,
                option
            );
            //Llamado para obtner los datos por medio de una funcion ORACLE
            detailsByBusinessPast = HistoricReportQuery.getBusinessDetail(
                userId, idQuestion, groupId, businessId, area, first, last,
                option
            );
        }
        session.put("yearH", year);
        HashMap<String, Object> data = new HashMap();
        data.put("results", results);
        data.put("resultsPrevYear", resultsPast);
        data.put("business", detailsByBusiness);
        data.put("businessPrevYear", detailsByBusinessPast);
        data.put("currYear", year);
        data.put("prevYear", (year - 1));
        data.put("groupId", groupId);
        data.put("businessId", businessId);
        data.put("zoneId", zoneId);
        data.put("storeId", storeId);
        data.put("managerId", managerId);
        data.put("bossId", bossId);
        data.put("area", area);
        data.put("isSelect", isSelect);
        data.put("sectionId", sectionId);
        data.put("MONTHS", DateUtils.MONTHS);

        renderJSON(data);
    }

    
    /**
     * Metodo para obtener los acumulados por año
     * 
     * @param level
     * @param idQuestion
     * @param groupId
     * @param businessId
     * @param area
     * @param zoneId
     * @param storeId
     * @param manager
     * @param chief
     * @param section
     * @param startDate
     * @param endDate 
     */
    public static void getAcumulados(String level, Integer idQuestion,
            Integer groupId, Integer businessId, String area,
            String zoneId, Integer storeId, Integer manager,
            Integer chief, Integer section, String startDate,
            String endDate) {

        String user = session.get("idHis");
        Integer userId = user != null ? Integer.parseInt( user ) : null;

        String option = "T";
        if (area != null && !area.equals("")) {
            option = "A";
        }

        if (idQuestion == null) {
            idQuestion = 202;
        }

        int year = startDate != null && !startDate.equalsIgnoreCase("")
                ? Integer.parseInt(startDate.split("/")[2]) : Calendar.getInstance().get(Calendar.YEAR);

        startDate = startDate != null && !startDate.equalsIgnoreCase("")
                ? startDate : "01/01/" + year;

        endDate = endDate != null && !endDate.equalsIgnoreCase("")
                ? endDate : FormatDateUtil.ownFormat.format(new Date());

        float[] acumulados = new float[2];

        acumulados[0] = HistoricReportQuery.getAcumulado(userId, idQuestion,
                groupId, businessId, area, zoneId, storeId, manager,
                chief, section, startDate, endDate, option);

        String first = startDate != null && !startDate.equalsIgnoreCase("")
                ? getStartDate(startDate) : "01/01/" + (year - 1);
        String last = startDate != null && !startDate.equalsIgnoreCase("")
                ? getEndDate(endDate) : "31/12/" + (year - 1);

        acumulados[1] = HistoricReportQuery.getAcumulado(userId, idQuestion,
                groupId, businessId, area, zoneId, storeId, manager,
                chief, section, first, last, option);

        renderJSON(acumulados);
    }

    /**
     * Metodo de utileria para obtener la fecha de un año anterior
     * @param startDate
     * @return 
     */
    
    private static String getStartDate(String startDate) {

        String start = "";
        try {
            Date date = FormatDateUtil.ownFormat.parse(startDate);
            Calendar calendar = Calendar.getInstance();
            calendar.setTime(date);
            calendar.add(Calendar.YEAR, -1);
            
            start = FormatDateUtil.ownFormat.format(calendar.getTime());

        } catch (ParseException ex) {
            Logger.getLogger(HistoryReport.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        return start;
    }

    /**
     * Metodo de utileria para obtner la fecha final un año anterior
     * @param startDate
     * @return 
     */
    
    private static String getEndDate(String startDate) {

        String last = "";
        try {
            Date date = FormatDateUtil.ownFormat.parse(startDate);
            Calendar calendar = Calendar.getInstance();
            calendar.setTime(date);
            calendar.add(Calendar.YEAR, -1);
            
            last = FormatDateUtil.ownFormat.format(calendar.getTime());
            
        } catch (ParseException ex) {
            Logger.getLogger(HistoryReport.class.getName()).log(Level.SEVERE, null, ex);
        }
        return last;
    }
    
    /**
     * Metodo para obtener el catalo de areas del sistema
     * @param group
     * @param business
     * @return 
     */
    
    public static List getAreas(Integer group, Integer business){
        String statement = "select distinct (select fc_desc_area from pul_nps_area where fc_id_area =  na.fc_id_area) desc_area, fc_id_area\n" +
                            "from   pul_nps_answer na\n" +
                            "where  fn_store_group_id   = :group\n" +
                            "and    fn_store_business_id = :business";
        
        List areas = null;
        Query query = JPA.em().
                createNativeQuery(statement);
        query.setParameter("group", group);
        query.setParameter("business", business);
                
        areas = (List) query.getResultList();
        return areas;
        //renderJSON(areas);
    }
}
