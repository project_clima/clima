package controllers;

import java.io.IOException;
import play.mvc.*;
import java.util.*;
import net.sf.jxls.exception.ParsePropertyException;
import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import querys.SearchQuery;
import querys.StructureQuery;
import utils.Pagination;
import utils.RenderExcel;

/**
 * Clase controlador para el manejo de busqueda de resultados
 * @author Rodolfo Miranda -- Qualtop
 */
@With(Secure.class)
public class SearchResults extends Controller {
    
    /**
     * Metodo para obtner las fechas de estructura actuales
     */
    @Check("searchresults/index")
    public static void index() {
        List structureDates = StructureQuery.getDatesStructures();
        render(structureDates);
    }
    
    /**
     * Metodo para obtener la busqueda de resultados segun filtros seleccionados
     * @param search
     * @param date
     * @param iDisplayLength
     * @param iDisplayStart
     * @param sEcho 
     */
    public static void results(String search, String date, int iDisplayLength,
                               int iDisplayStart, String sEcho) {
        
        int start    = iDisplayStart == 0 ? iDisplayStart : iDisplayStart + 1;
        int end      = iDisplayLength + iDisplayStart;
        
        List results = SearchQuery.search(search, date, start, end, true);
        List resultsCount = SearchQuery.search(search, date, start, end, false);     
        long count   = resultsCount.size();
        
        renderJSON(new Pagination(sEcho, count, count, results));      
    }
    
    /**
     * Metodo para exportar resultados mostrados en pantalla
     * @param search
     * @param date
     * @throws ParsePropertyException
     * @throws InvalidFormatException
     * @throws IOException 
     */
    public static void export(String search, String date)
    throws ParsePropertyException, InvalidFormatException, IOException {
        List results = SearchQuery.search(search, date, 0, 10, false);
        
        renderArgs.put("results", results);
        renderArgs.put(RenderExcel.RA_FILENAME, "Resultados.xls");
        RenderExcel renderer = new RenderExcel("app/excel/views/SearchResults/export.xls");
        renderer.render();
    }
}
