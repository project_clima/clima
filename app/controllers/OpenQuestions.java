package controllers;

import play.mvc.*;
import java.util.*;
import java.util.logging.Level;
import java.util.logging.Logger;
import json.entities.ResumeTable;
import models.*;
import querys.SurveysQuery;
import utils.RenderExcel;
import net.sf.jxls.exception.ParsePropertyException;
import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import java.io.IOException;
import java.sql.SQLException;
import json.entities.FortalezasList;
import org.apache.commons.lang3.StringUtils;
import querys.MatrixQuery;
import querys.StructureQuery;
import utils.Pagination;

/**
 * Clase controlador para el manejo de las preguntas abiertas
 * @author Rodolfo Miranda -- Qualtop
 */
@With(Secure.class)
public class OpenQuestions extends Controller {

    /**
     * Metodo principal, regresa las fechas de estructura
     */
    public static void index() {
        List structureDates = StructureQuery.getDatesStructures();
        render(structureDates);
    }
    
    /**
     * Metodo que obtiene las preguntas abiertas por usuario y fecha de estructura
     *
     * @param userId
     * @param structureDate
     * @param firstRequest 
     */
    public static void getOpenQuestions(String userId, String structureDate, boolean firstRequest) {
        List questions = SurveysQuery.getOpenQuestions(userId, structureDate, firstRequest);
        HashMap<String, List<Integer>> questionsGrouped = parseQuestions(questions);
        
        renderJSON(questionsGrouped);
    }
    
    /**
     * Metodo para obtener las respuestas por encuesta y usuario de la preguntas abiertas
     * @param surveys
     * @param question
     * @param userId
     * @param structureDate
     * @param iDisplayLength
     * @param startDate
     * @param endDate
     * @param iDisplayStart
     * @param sEcho 
     */
    public static void questionAnswers(String surveys, String question, String userId,
                                       String structureDate, int iDisplayLength,
                                       String startDate, String endDate,
                                       int iDisplayStart, String sEcho) {
        
        int start        = iDisplayStart == 0 ? iDisplayStart : iDisplayStart + 1;
        int end          = iDisplayLength + iDisplayStart;
        
        long count   = SurveysQuery.getCountQuestionAnswers(
            surveys, question, userId, structureDate, false
        );
        
        List answers = SurveysQuery.getQuestionAnswers(
            surveys, question, userId, structureDate, start, end, false
        );
        
        renderJSON(new Pagination(sEcho, count, count, answers));        
    }
    
    /**
     * MEtodo para obtener las respuestas de las preguntas abiertas directas por
     * encuesta, fecha de estructura
     * @param surveys
     * @param question
     * @param userId
     * @param structureDate
     * @param iDisplayLength
     * @param startDate
     * @param endDate
     * @param iDisplayStart
     * @param sEcho 
     */
    public static void questionAnswersDirect(String surveys, String question, String userId,
                                       String structureDate, int iDisplayLength,
                                       String startDate, String endDate,
                                       int iDisplayStart, String sEcho) {
        
        int start    = iDisplayStart == 0 ? iDisplayStart : iDisplayStart + 1;
        int end      = iDisplayLength + iDisplayStart;
        
        long count   = SurveysQuery.getCountQuestionAnswers(
            surveys, question, userId, structureDate, true
        );
        
        List answers = SurveysQuery.getQuestionAnswers(
            surveys, question, userId, structureDate, start, end, true
        );
        
        renderJSON(new Pagination(sEcho, count, count, answers));        
    }
    
    /**
     * Metodo para exportas los resultados mostrados en pantalla
     * @param question
     * @param userId
     * @param structureDate
     * @param direct
     * @throws ParsePropertyException
     * @throws InvalidFormatException
     * @throws IOException 
     */
    public static void exportAnswers(String question, String userId,
                                     String structureDate, boolean direct)
    throws ParsePropertyException, InvalidFormatException, IOException {
        long count   = SurveysQuery.getCountQuestionAnswers(
            "", question, userId, structureDate, direct
        );
        
        List answers = SurveysQuery.getQuestionAnswers(
            "", question, userId, structureDate, 0, (int) count, direct
        );
        
        renderArgs.put("answers", answers);
        renderArgs.put("question", question);
        renderArgs.put(RenderExcel.RA_FILENAME, "PreguntasAbiertasRespuestas.xls");
        RenderExcel renderer = new RenderExcel("app/excel/views/OpenQuestions/exportAnswers.xls");
        renderer.render();
    }
    
    /**
     * MEtodo para devolver las preguntas ordendas
     * @param openQuestions
     * @return 
     */
    private static HashMap<String, List<Integer>> parseQuestions(List openQuestions) {
        HashMap<String, List<Integer>> questions = new HashMap();
        List<Integer> surveyIds;

        for (Object question : openQuestions) {
            Object[] rowQuestion = (Object[]) question;
            
            String key = rowQuestion[0].toString();            
            surveyIds  = questions.get(key);
            surveyIds  = surveyIds == null ? new ArrayList() : surveyIds;
            
            surveyIds.add(Integer.valueOf(rowQuestion[1].toString()));
            questions.put(key, surveyIds);
        }
        
        return questions;
    }

    public static void getSelectSurveys() {
        SurveysQuery queries = new SurveysQuery();

        List survey = queries.getSurveys();
        renderJSON(survey);
    }

    public static void getSelectQuestionType(){
        List<QuestionType> typeQuestions = QuestionType.find("select tQ from QuestionType tQ").fetch();
        renderJSON(typeQuestions);
    }

    public static void getQuestions(int surveyId, int typeQuestionId) {
        SurveysQuery queries = new SurveysQuery();

        List questions = queries.getQuestionsByType(surveyId, typeQuestionId);
        renderJSON(questions);
    }

    public static void getOpenAnswers(int questionId) {
        SurveysQuery queries = new SurveysQuery();
        List openAnswers = queries.getOpenAnswersByQuestion(questionId);
        renderJSON(openAnswers);
    }

    public static void getOpenAnswersCount(int questionId) {
        SurveysQuery queries = new SurveysQuery();
        List openAnswers = queries.getOpenAnswersCount(questionId);
        renderJSON(openAnswers);
    }

    public static void getOpenAnswersByRange(int questionId, int rowStart, int rowEnd) {
        SurveysQuery queries = new SurveysQuery();
        List openAnswers = queries.getOpenAnswersByQuestionByRange(questionId, rowStart, rowEnd);
        renderJSON(openAnswers);
    }

    public static void getMultiAnswers(int questionId) {
        SurveysQuery queries = new SurveysQuery();

        List openAnswers = queries.getMultiAnswersByQuestion(questionId);
        renderJSON(openAnswers);
    }

    public static void getResume(int surveyId) {
        SurveysQuery queries = new SurveysQuery();
        
        ResumeTable resume = queries.getResume(surveyId);
        renderJSON(resume);
    }

    /**
     * Metodo para expotar los resultados por encuesta
     * @param surveyId
     * @throws ParsePropertyException
     * @throws InvalidFormatException
     * @throws IOException 
     */
    public static void export(int surveyId)
                            throws ParsePropertyException, InvalidFormatException, IOException{
        try {
            SurveysQuery queries         = new SurveysQuery();
             List<FortalezasList> export = queries.getExport(surveyId);

             renderArgs.put("fortalezas", export);
             renderArgs.put(RenderExcel.RA_FILENAME, "Reporte_debilidades.xls");
             RenderExcel renderer = new RenderExcel("app/excel/views/Fortalezas/export.xls");
             renderer.render();
        } catch (SQLException ex){
            Logger.getLogger(OpenQuestions.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
}
