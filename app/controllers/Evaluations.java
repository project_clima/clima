package controllers;

import java.io.File;
import java.io.IOException;
import java.util.*;
import models.NpsAnswer;
import models.nps.NpsArea;
import models.nps.NpsPractice;
import models.nps.Tracing;
import net.sf.jxls.exception.ParsePropertyException;
import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import play.Play;
import play.mvc.*;
import querys.nps.NpsHomeQuery;
import utils.Pagination;
import utils.RenderExcel;
import utils.nps.helpers.EvaluetionsControllerHelper;
import utils.nps.model.ChallengeModel;
import utils.nps.model.EvaluationsModel;
import utils.nps.model.NpsAnswerTmp;
import utils.nps.model.TracingModelHelper;

/**
 * Clase para el manejo de las evalaciones
 * path: /evaluations/index
 * @author Rodolfo Miranda -- Qualtop
 */
@With(Secure.class)
public class Evaluations extends Controller {

    /**
     * Metodo inicio al dar click en Detalle de Evaluaciones
     */
    //@Check("evaluaciones")
    public static void index() {

        if (session.contains("permisos")) {
            String sessionPermisos = session.get("permisos");
            String permisos = sessionPermisos.substring(1, sessionPermisos.length() - 1);
            List<String> aPermisosUsuario = Arrays.asList(permisos.split("\\s*,\\s*"));

            if (aPermisosUsuario.contains("detalleevaluaciones/user") || 
                    ( !aPermisosUsuario.contains("detalleevaluaciones/user") && !aPermisosUsuario.contains("detalleevaluaciones/all"))) {
                session.put("idEva", session.get("userId"));
            }
        }
        render();
    }

    /**
     * Metodo que devuelve el detalle de evaluación al dar click 
     * en la tabla
     * 
     * @param rowId     identificador de evaluacion
     * @param calif1    calificacion 201
     * @param calif2    calificacion 202
     * @param desc1     campo abierto de respuesta 1
     * @param desc2     campo abierto de respuesta 2
     * @param cat1      recomendacion o rechazo pregunta 1
     * @param cat2      recomendacion o rechazo pregunta 2
     * @param audio     id de audio de la evaluacion
     */
    
    public static void detailsEvaluation(String rowId, String calif1, String calif2,
            String desc1, String desc2, String cat1, String cat2, String audio) {
        //EvaluationDetail evaD = EvaluationDetailsQuery.getEvaluationDetail(rowId);        
        render(rowId, calif1, calif2, desc1, desc2, cat1, cat2, audio);
    }

    /**
     * Metodo para el detalle y acciones de Impugnacion para una evaluacion
     * 
     * @param rowId
     * @param operation
     * @param text
     * @param status
     * @param qualif1
     * @param qialif2
     * @param idEmployee
     * @param idArea
     * @param type
     * @param practice 
     */
    
    @Check("impugnaciones")
    public static void challengesDetail(String rowId, String operation, String text,
            String status, String qualif1, String qialif2, String idEmployee,
            String idArea, String type, String practice) {
        String userId = null;
         String userName = session.get("username");
        String displyName = NpsHomeQuery.getFullNameUserID(session.get("userId"));

        NpsAnswer nps = null;

        if ( check("replica/senduser") || check("impugnar/send")
                || check("impugnar/respond") || check("replicar/responduser")) {
            userId = session.get("userId");
            nps = NpsAnswer.find(
                    "select m \n"
                    + " from NpsAnswer m \n"
                    + " WHERE  m.id = ? \n"
                    + " AND ((m.userid = ? OR  ?  IS NULL) \n"
                    + " OR  (m.userIdChief  = ? OR ? IS NULL)\n"
                    + " OR  (m.userIdManager  = ? OR ? IS NULL) \n"
                    + " OR  (m.userIdDirector = ? OR ? IS NULL) \n"
                    + " OR  (m.userIdZone     = ? OR ? IS NULL) )\n",
                     Long.parseLong(rowId), Long.parseLong(userId),
                     Long.parseLong(userId), Long.parseLong(userId),
                     Long.parseLong(userId), Long.parseLong(userId),
                     Long.parseLong(userId), Long.parseLong(userId),
                     Long.parseLong(userId), Long.parseLong(userId),
                     Long.parseLong(userId)).first();
        }

        if (nps != null || check("impugnar/sendall") || check("impugnar/respondall")
                || check("replicar/respondall") || check("replicar/sendall")) {
            
            ChallengeModel challenge;
                challenge = EvaluetionsControllerHelper.
                    changeChallengerDetail(rowId, operation, text, status, practice,
                            qualif1, qialif2, idEmployee, idArea, type, userId,
                            userName);

            List<NpsArea> areas = NpsArea.findAll();
            List<NpsPractice> practices = NpsPractice.findAll();

            challenge.setFullName(displyName);
            challenge.setUserId(userId);
            challenge.setUserName(userName);
            challenge.setImpug( challenge.getStatus().equals("4") && 
                    ( check("impugnar/respondall") || check("impugnar/respond")) );
            
            render(challenge, areas, practices);
        }
        renderText("No permitido");
    }

    /**
     * Metodo para el detalle y acciones para Seguimiento de una evaluacion
     * @param rowId
     * @param operation
     * @param text
     * @param status 
     */
    
    @Check("seguimiento")
    public static void tracingDetail(String rowId, String operation, String text,
            String status) {

        String userId = null;
        String userName = session.get("username");
        NpsAnswer nps = null;

        if (check("seguimiento/see") || check("seguimiento/make")) {
            userId = session.get("userId");
            nps = NpsAnswer.find(
                    "select m \n"
                    + " from NpsAnswer m \n"
                    + " WHERE  m.id = ? \n"
                    + " AND ((m.userid = ? OR  ?  IS NULL) \n"
                    + " OR  (m.userIdChief  = ? OR ? IS NULL)\n"
                    + " OR  (m.userIdManager  = ? OR ? IS NULL) \n"
                    + " OR  (m.userIdDirector = ? OR ? IS NULL) \n"
                    + " OR  (m.userIdZone     = ? OR ? IS NULL) )\n",
                     Long.parseLong(rowId), Long.parseLong(userId),
                     Long.parseLong(userId), Long.parseLong(userId),
                     Long.parseLong(userId), Long.parseLong(userId),
                     Long.parseLong(userId), Long.parseLong(userId),
                     Long.parseLong(userId), Long.parseLong(userId),
                     Long.parseLong(userId)).first();
        }

        if (nps != null || check("seguimiento/seeall") || check("seguimiento/makeall")) {
            TracingModelHelper model = EvaluetionsControllerHelper.
                    changeTraicingDetail(rowId, operation, text, status, session.get("userId"), userName);
            NpsAnswerTmp aT = model.getaT();
            List<Tracing> tracings = model.getTracings();
            render(aT, tracings);
        }
        renderText("No permitido");
    }

    /**
     * Metodo para la paginacion el cual devuelve el numero de evaluaciones
     * segun los filtros seleccionados
     * 
     * @param firstDate
     * @param lastDate
     * @param store
     * @param manager
     * @param chief
     * @param section
     * @param zone
     * @param group
     * @param business
     * @param sSearch 
     */
    
    public static void getCount(String firstDate, String lastDate, String store,
            String manager, String chief, String section, String zone,
            String group, String business, String sSearch) {

        String userId = session.get("idEva");

        EvaluationsModel model = EvaluetionsControllerHelper.
                getEvaluationsHelper(userId,
                        firstDate != null && firstDate.equals("") ? null : firstDate,
                        lastDate != null && lastDate.equals("") ? null : lastDate,
                        store != null && store.equals("") ? null : store,
                        manager != null && manager.equals("") ? null : manager,
                        chief != null && chief.equals("") ? null : chief,
                        section != null && section.equals("") ? null : section,
                        zone != null && zone.equals("") ? null : zone,
                        group != null && group.equals("") ? null : group,
                        business != null && business.equals("") ? null : business,
                        0,
                        0,
                        1,
                        1,
                        "asc",
                        sSearch != null && sSearch.equals("") ? null : sSearch
                );
        renderJSON(model.getCount());
    }

    /**
     * Metodo para obtener las evaluaciones segun los filtros seleccionados
     * @param firstDate
     * @param lastDate
     * @param store
     * @param manager
     * @param flag
     * @param chief
     * @param section
     * @param zone
     * @param group
     * @param business
     * @param iDisplayLength
     * @param iDisplayStart
     * @param sEcho
     * @param totalRows
     * @param iSortCol_0
     * @param sSortDir_0
     * @param sSearch 
     */
    
    public static void getEvaluations(
            String firstDate, String lastDate, String store, String manager, int flag,
            String chief, String section, String zone, String group, String business,
            int iDisplayLength, int iDisplayStart, String sEcho, int totalRows,
            int iSortCol_0, String sSortDir_0, String sSearch) {

        String userId = session.get("idEva");

        int start = iDisplayStart == 0 ? iDisplayStart : iDisplayStart + 1;
        int end = iDisplayLength != -1 ? iDisplayLength + iDisplayStart : totalRows;
        end = end > totalRows ? totalRows : end;
        int sort = 0;

        if (iSortCol_0 == 0) {
            sort = 3;
        } else if (iSortCol_0 >= 1) {
            sort = iSortCol_0 + 5;
        }

        EvaluationsModel model = EvaluetionsControllerHelper.
                getEvaluationsHelper(userId,
                        firstDate != null && firstDate.equals("") ? null : firstDate,
                        lastDate != null && lastDate.equals("") ? null : lastDate,
                        store != null && !store.equals("") ? store : null,
                        manager != null && !manager.equals("") ? manager : null,
                        chief != null && !chief.equals("") ? chief : null,
                        section != null && !section.equals("") ? section : null,
                        zone != null && zone.equals("") ? null : zone,
                        group != null && group.equals("") ? null : group,
                        business != null && business.equals("") ? null : business,
                        start,
                        end,
                        flag,
                        sort,
                        sSortDir_0,
                        sSearch != null && sSearch.equals("") ? null : sSearch
                );

        renderJSON(new Pagination(sEcho, totalRows, totalRows, model.getEvaluations()));
    }

    /**
     * Metodo para la descarga de las evaluaciones 
     * segun los filtros seleccionados
     * 
     * @param firstDate
     * @param lastDate
     * @param store
     * @param manager
     * @param flag
     * @param chief
     * @param section
     * @param zone
     * @param group
     * @param business
     * @param search
     * @throws ParsePropertyException
     * @throws InvalidFormatException
     * @throws IOException 
     */
    
    public static void downloadEvaluations(
            String firstDate, String lastDate, String store, String manager, int flag,
            String chief, String section, String zone, String group, String business,
            String search) throws ParsePropertyException, InvalidFormatException, IOException {

        String userId = session.get("idEva");

        List answers = new ArrayList<>();
        flag = userId != null ? 0 : 1;

        answers = EvaluetionsControllerHelper.
                getEvaluationsXLSHelper(userId,
                        firstDate != null && firstDate.equals("") ? null : firstDate,
                        lastDate != null && lastDate.equals("") ? null : lastDate,
                        store != null && store.equals("") ? null : store,
                        manager != null && manager.equals("") ? null : manager,
                        chief != null && chief.equals("") ? null : chief,
                        section != null && section.equals("") ? null : section,
                        zone != null && zone.equals("") ? null : zone,
                        group != null && group.equals("") ? null : group,
                        business != null && business.equals("") ? null : business,
                        flag,
                        search != null && search.equals("") ? null : search
                );
        renderArgs.put("answers", answers);
        renderArgs.put(RenderExcel.RA_FILENAME, "Evaluaciones.xls");
        RenderExcel renderer = null;
        if (flag == 0) {
            renderer = new RenderExcel("app/excel/views/EvaluationDetail/export_partial.xls");
        } else {
            renderer = new RenderExcel("app/excel/views/EvaluationDetail/export_full.xls");
        }

        renderer.render();
    }

    /**
     * Metodo para obtener los permisos asociados a un usarios con referente
     * a Impugnaciones o Seguimieto
     * 
     * @param permiso
     * @return 
     */
    
    private static boolean check(String permiso) {
        if (!session.contains("permisos")) {
            return false;
        }

        String sessionPermisos = session.get("permisos");
        String permisos = sessionPermisos.substring(1, sessionPermisos.length() - 1);
        List<String> aPermisosUsuario = Arrays.asList(permisos.split("\\s*,\\s*"));

        return aPermisosUsuario.contains(permiso.toLowerCase());
    }

    /**
     * Metodo para saber si un audio de una evaluacion esta en el 
     * archivo de audios de la aplicacion
     * 
     * @param filename 
     */
    
    public static void getRecordAudio(String filename) {
        String mediaPath = Play.configuration.getProperty("media.folder");
        String replace   = File.separator.equals("/") ? "/$" : "\\\\$";        
        String filePath  = mediaPath.replaceAll(replace, "") + File.separator ;
        
        File audioFile = new File(filePath + filename);

        if (audioFile.exists()) {
            response.setContentTypeIfNotSet("audio/mpeg");
            renderBinary(audioFile, filename);
        }
        
        response.status = 404;
    }
}
