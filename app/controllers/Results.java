package controllers;

import java.util.*;
import play.Logger;
import play.data.validation.*;
import play.mvc.*;
import querys.ProgressQuery;
import querys.ResultsQuery;
import querys.StructureQuery;
import utils.Pagination;
import utils.PagingResult;

/**
 * Clase controlador para el manejo del avance en clima
 * @author Rodolfo Miranda -- Qualtop
 */
@With(Secure.class)
public class Results extends Controller {

    /**
     * Metodo principal, obtiene las fechas de estructura en base
     */
    @Check("results/list")
    public static void list() {
        List structureDates = StructureQuery.getDatesStructures();
        render(structureDates);
    }
    
    /**
     * Metodo para obtener el avance de las encuetas por usuario y fecha de estructura
     * @param userId
     * @param date 
     */
    public static void progress(int userId, String date) {        
        List progress = null;
        try {
            progress = userId != 50092262 ?
                    ProgressQuery.getProgress(userId, date) :
                    ProgressQuery.getInitialProgress(date);
        } catch (Exception ex) {
            Logger.error(ex.getMessage());
            boolean error = true;
            render(userId, progress,error);
        }
        
        render(userId, progress);
    }

    /**
     * Metodo para los logs del avance en clima
     * @param user
     * @param iDisplayLength
     * @param startDate
     * @param endDate
     * @param iDisplayStart
     * @param sEcho
     * @param sSortDir_0
     * @param iSortCol_0 
     */
    public static void getLogs(String user, int iDisplayLength, String startDate,
            String endDate, int iDisplayStart, String sEcho,
            String sSortDir_0, int iSortCol_0) {

        String condition = user != null && user.trim().length() > 0
                ? "LOGUSR LIKE '%" + user + "%'"
                : "LOGUSR = LOGUSR";
        String dateCondition = "";

        if (startDate.trim().length() > 0 && endDate.trim().length() > 0) {
            dateCondition = " AND DATE(LOGUPD) BETWEEN DATE('" + startDate + "') AND DATE('" + endDate + "')";
            condition += dateCondition;
        }

        String order = getOrder(sSortDir_0, iSortCol_0);

        int start = iDisplayStart == 0 ? iDisplayStart : iDisplayStart + 1;
        PagingResult oPR = utils.Queries.getLogs(condition, order, start, iDisplayLength + iDisplayStart);
        Pagination oPagination = new Pagination(sEcho, oPR.totalRecords, oPR.totalRecords, oPR.data);

        renderJSON(oPagination);
    }

    /**
     * Metodo de utileria para el query dinamico
     * @param sSortDir_0
     * @param iSortCol_0
     * @return 
     */
    public static String getOrder(String sSortDir_0, int iSortCol_0) {
        String order;

        switch (iSortCol_0) {
            case 0:
                order = "ORDER BY LOGUSR " + sSortDir_0;
                break;
            case 1:
                order = "ORDER BY LOGDES " + sSortDir_0;
                break;
            case 2:
                order = "ORDER BY LOGUPD " + sSortDir_0;
                break;
            default:
                order = "ORDER BY LOGUSR " + sSortDir_0;
                break;
        }

        return order;
    }

    /**
     * Metodo para obtener el avance en el nivel uno de la jeraarquia
     * @param structureDate
     * @param level
     * @param survey
     * @param user
     * @param types
     * @param iDisplayLength
     * @param startDate
     * @param endDate
     * @param iDisplayStart
     * @param sEcho
     * @param sSortDir_0
     * @param iSortCol_0 
     */
    public static void getNivelUno(String structureDate, String level, String survey, String user, String types, int iDisplayLength, String startDate,
            String endDate, int iDisplayStart, String sEcho,
            String sSortDir_0, int iSortCol_0) {

        String condition = null;
        String dateCondition = "";

        String order = getOrder(sSortDir_0, iSortCol_0);

        int start = iDisplayStart == 0 ? iDisplayStart : iDisplayStart + 1;

        PagingResult oPR = null;
        if ("1".equals(types)) {
            oPR = utils.Queries.getZonaUno(survey, condition, order, start, iDisplayLength + iDisplayStart);
        } else if ("3".equals(types) || "2".equals(types)) {
            oPR = utils.Queries.getComprasCorp(survey, level, condition, order, start, iDisplayLength + iDisplayStart);
        } else {
            oPR = utils.Queries.getNivelUno(structureDate, order, start, iDisplayLength + iDisplayStart);
        }

        Pagination oPagination = new Pagination(sEcho, oPR.totalRecords, oPR.totalRecords, oPR.data);

        renderJSON(oPagination);
    }

    /**
     * Metodo para obtener el avance despues del nivel uno, por filtros seleccionados
     * @param structureDate
     * @param survey
     * @param area
     * @param types
     * @param usuario
     * @param nivel
     * @param iDisplayLength
     * @param startDate
     * @param endDate
     * @param iDisplayStart
     * @param sEcho
     * @param sSortDir_0
     * @param iSortCol_0 
     */
    public static void getNivelN(String structureDate, String survey, String area, String types, String usuario, String nivel, int iDisplayLength, String startDate,
            String endDate, int iDisplayStart, String sEcho,
            String sSortDir_0, int iSortCol_0) {

        String condition = null;
        String dateCondition = "";

        String order = getOrder(sSortDir_0, iSortCol_0);

        int start = iDisplayStart == 0 ? iDisplayStart : iDisplayStart + 1;
        PagingResult oPR = null;
        if ("1".equals(types)) {
            oPR = utils.Queries.getNivelNZona(survey, area, condition, order, start, iDisplayLength + iDisplayStart);
        } else if ("3".equals(types) || "2".equals(types)) {
            oPR = utils.Queries.getComprasCorp(survey, nivel, condition, order, start, iDisplayLength + iDisplayStart);
        } else {
            oPR = utils.Queries.getNivelN(structureDate, usuario, nivel, condition, order, start, iDisplayLength + iDisplayStart);
        }

        Pagination oPagination = new Pagination(sEcho, oPR.totalRecords, oPR.totalRecords, oPR.data);

        renderJSON(oPagination);
    }

    public static void getNivelDiv(String survey, String div, String types, String usuario, String nivel, int iDisplayLength, String startDate,
            String endDate, int iDisplayStart, String sEcho,
            String sSortDir_0, int iSortCol_0) {

        String condition = null;
        String dateCondition = "";

        String order = getOrder(sSortDir_0, iSortCol_0);

        int start = iDisplayStart == 0 ? iDisplayStart : iDisplayStart + 1;
        PagingResult oPR = utils.Queries.getNivelDiv(survey, div, condition, order, start, iDisplayLength + iDisplayStart);

        Pagination oPagination = new Pagination(sEcho, oPR.totalRecords, oPR.totalRecords, oPR.data);

        renderJSON(oPagination);
    }

    /**
     * Metodo para validar las encuestas. 
     */
    public static void getValidSurveys() {
        renderJSON(utils.Queries.getValidSurveys());
    }

    /**
     * Metodo para obtener los departamentos del nivel1
     * @param level 
     */
    public static void getLevels1(int level) {
        ResultsQuery queries = new ResultsQuery();

        List levels = queries.getDepartmentsByLevel1(level);
        renderJSON(levels);
    }

    /***
     * Metodo para obtener los departamenos por filtros seleccionados
     * @param level
     * @param department
     * @param date 
     */
    public static void getLevels(int level, int department, String date) {
        List levels = StructureQuery.getDepartmentsByLevel(department, date, false);
        renderJSON(levels);
    }

    public static void getDatesStructures() {
        StructureQuery queries = new StructureQuery();

        List datesStructure = queries.getDatesStructures();
        renderJSON(datesStructure);
    }

}
