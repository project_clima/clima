package controllers;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;
import json.entities.AutoComplete;
import models.EmployeeClima;
import models.FunctionEmployee;
import models.QuestionAnswer;
import models.StoreEmployee;
import models.SurveyEmployee;
import play.mvc.*;
import querys.DuplicatesQuery;
import querys.StructureQuery;
import utils.Message;
import utils.Pagination;

/**
 * Clase controlador para el manejo de los duplicados y vacantes en clima
 * @author Rodolfo Miranda -- Qualtop
 */
@With(Secure.class)
public class DuplicatesVacancies extends Controller {
    
    /**
     * MEtodo que regresa las fechas de fotos existentes en base de datos
     */
    public static void index() {
        List structureDates = StructureQuery.getDatesStructures();        
        render(structureDates);
    }
    
    /**
     * Metodo que regresa la lista de empleados por fecha
     * @param term
     * @param date 
     */
    public static void getEmployees(String term, String date) {
        StringBuilder conditions = new StringBuilder();
        
        conditions.append("(lower(firstName || ' ' || lastName) like lower(?1)")
            .append(" or to_char(employeeNumber) like ?2)")
            .append(" and to_char(structureDate, 'mm/yyyy') = ?3")
            .append(" order by firstName, lastName asc");
        
        List<EmployeeClima> employees = EmployeeClima.find(
            conditions.toString(),
            "%" + term + "%",
            "%" + term + "%",
            date
        ).fetch(12);
        
        List<AutoComplete> dependants = new ArrayList(12);
        
        for (EmployeeClima employee : employees) {            
            String employeeName = employee.firstName + " " + employee.lastName;
            dependants.add(new AutoComplete(employee.userId, employeeName));
        }
        
        renderJSON(dependants);
    }
    
    /**
     * Metodo que regresa las tiendas existentes por fecha de estructura
     * @param structureDate
     * @return 
     */
    private static List<StoreEmployee> getStoresList(String structureDate) {
        return StoreEmployee.find(
            "to_char(structureDate, 'mm/yyyy') = ?1 order by descriptionStore asc",
                structureDate
        ).fetch();
    }
    
    /**
     * Metodo que regresa la lista de tiendas por fecha de estructura
     * @param structureDate 
     */
    public static void getStores(String structureDate) {
        List<StoreEmployee> stores = getStoresList(structureDate);        
        render(stores);
    }
    
    /**
     * Metodo que regresa la lista de duplicados por fecha de estructura
     * @param structureDate
     * @param iDisplayLength
     * @param iDisplayStart
     * @param sEcho
     * @param search 
     */
    public static void listDuplicates(String structureDate, int iDisplayLength,
                                       int iDisplayStart, String sEcho, String search) {        
        int start  = iDisplayStart == 0 ? iDisplayStart : iDisplayStart + 1;
        int end    = iDisplayLength + iDisplayStart;        
        long count = DuplicatesQuery.countDuplicates(structureDate, search);
        
        List duplicates = DuplicatesQuery.getDuplicates(structureDate, search, start, end);
        
        renderJSON(new Pagination(sEcho, count, count, duplicates));        
    }
    
    /**
     * Metodo que regresa los dependientes por empleado
     * @param userId
     * @param period
     * @throws ParseException 
     */
    public static void duplicate(Integer userId, String period) throws ParseException {
        SimpleDateFormat sdf = new SimpleDateFormat("MM/yyyy");
        List structureDates = StructureQuery.getDatesStructures();        
        List<FunctionEmployee> functions = FunctionEmployee.find("order by descriptionFunction asc").fetch();
        List<StoreEmployee> stores = null;
        
        boolean isEdit = userId != null;
        List<EmployeeClima> dependants = null;
        EmployeeClima  manager = null;
        
        userId = isEdit ? userId : -9999;
        
        EmployeeClima employee = EmployeeClima.findById(userId);
        
        if (employee != null) {           
            dependants = EmployeeClima.find("manager = ?1 and TO_CHAR(structureDate,"
                + "'mm/yyyy') = ?2", employee.userId, sdf.format(employee.structureDate)).fetch();
            manager = EmployeeClima.find("userId = ?1", employee.manager).first();
            stores = getStoresList(sdf.format(employee.structureDate));
        }else {
            employee = new EmployeeClima();
            employee.structureDate = sdf.parse(period);
        }
        
        render(stores, functions, structureDates, employee, dependants, isEdit, manager);
    }
    
    /**
     * Metodo que guarda en base de datos un duplicado o vacante
     * @param structureDate
     * @param userId
     * @param location
     * @param function
     * @param position
     * @param dependants
     * @param type
     * @param managerId 
     */
    public static void saveDuplicate(String structureDate, Integer userId, Integer location,
                                     String function, String position, String[] dependants,
                                     String type, Integer managerId) {     
        
        SimpleDateFormat sdf = new SimpleDateFormat("MM/yyyy");
        List<EmployeeClima> dependantsO = null;
        dependantsO = EmployeeClima.find("manager = ?1 and TO_CHAR(structureDate,"
                + "'mm/yyyy') = ?2", userId, structureDate).fetch();
        
        Integer[][] dependantsTotal = getDependants(dependants,dependantsO);
        
        int result = DuplicatesQuery.saveDuplicate(
            structureDate, userId, position, location, function, dependantsTotal, type,
            session.get("username"), managerId
        );
        
        String verb = type.equals("I") ? "agregado" : "modificado";
        
        if (result > 0) {
            renderJSON(new Message("Duplicado " + verb + " correctamente.", "success"));            
            return;
        }
        
        response.status = 400;
        renderJSON(new Message("El duplicado no fue agregado. Por favor, intente nuevamente."));
    }
    
    /**
     * MEtodo que elimina de la base de datos un empleado duplicado
     * @param userId
     * @param period 
     */
    public static void deleteDuplicate(Integer userId, String period) {
        Long dependants = EmployeeClima.count("manager = ?1 and TO_CHAR(structureDate,"
                + "'mm/yyyy') = ?2", userId, period);
        
        if (dependants > 0) {
            renderJSON(new Message(
                "El registro no puede ser eliminado porque tiene dependientes asociados.",
                "warning", true
            ));
            return;
        }
        
        EmployeeClima employee = EmployeeClima.find("userId = ?1 and TO_CHAR(structureDate,"
                + "'mm/yyyy') = ?2", userId,period).first();
        Long answered = QuestionAnswer.count("employee", employee.id);
        
        if (answered > 0) {
            renderJSON(new Message("El registro no puede ser eliminado porque tiene encuestas contestadas.",
                "warning", true
            ));
            return;
        }
        
        SurveyEmployee survey = SurveyEmployee.find("employeeClima = ?1", employee).first();
        if (survey != null) {
            survey.delete();
        }
        
        employee.delete();
        renderJSON(new Message("Registro eliminado correctamente.", "success", false));
    }
    
    /**
     * Metodo que regresa la lista de empleados vacantes
     * @param structureDate
     * @param iDisplayLength
     * @param iDisplayStart
     * @param sEcho
     * @param search 
     */
    public static void listVacancies(String structureDate, int iDisplayLength,
                                       int iDisplayStart, String sEcho, String search) {        
        int start  = iDisplayStart == 0 ? iDisplayStart : iDisplayStart + 1;
        int end    = iDisplayLength + iDisplayStart;        
        long count = DuplicatesQuery.countVacancies(structureDate, search);
        
        List duplicates = DuplicatesQuery.getVacancies(structureDate, search, start, end);
        
        renderJSON(new Pagination(sEcho, count, count, duplicates));        
    }
    
    /**
     * Metodo que regresa la lista de vacantes por empleado
     * @param userId
     * @param period
     * @throws ParseException 
     */
    public static void vacant(Integer userId, String period) throws ParseException {
        SimpleDateFormat sdf = new SimpleDateFormat("MM/yyyy");
        List structureDates = StructureQuery.getDatesStructures();
        List<StoreEmployee> stores = null;
        List<FunctionEmployee> functions = FunctionEmployee.find("order by descriptionFunction asc").fetch();
        
        boolean isEdit = userId != null;
        List<EmployeeClima> dependants = null;
        EmployeeClima manager = null;
        
        userId = isEdit ? userId : -9999;
        
        EmployeeClima employee = EmployeeClima.findById(userId);
        
        if (employee != null) {      
            dependants = EmployeeClima.find("manager = ?1 and TO_CHAR(structureDate,"
                + "'mm/yyyy') = ?2", employee.userId, sdf.format(employee.structureDate)).fetch();
            manager = EmployeeClima.find("userId = ?1", employee.manager).first();
            stores = getStoresList(sdf.format(employee.structureDate));
        }else {
            employee = new EmployeeClima();
            employee.structureDate = sdf.parse(period);
        }
        
        render(stores, functions, structureDates, employee, dependants, manager, isEdit);
    }
    
    /**
     * Metodo que almacena en base de datos un empleado vacante
     * @param structureDateVacant
     * @param managerUserId
     * @param location
     * @param function
     * @param position
     * @param dependants
     * @param actionVacant
     * @param userId 
     */
    public static void saveVacant(String structureDateVacant, Integer managerUserId,
                                  Integer location, String function, String position,
                                  String[] dependants, String actionVacant, Integer userId) {
        SimpleDateFormat sdf = new SimpleDateFormat("MM/yyyy");
        List<EmployeeClima> dependantsO = null;
        dependantsO = EmployeeClima.find("manager = ?1 and TO_CHAR(structureDate,"
                + "'mm/yyyy') = ?2", userId, structureDateVacant).fetch();
        
        Integer[][] dependantsTotal = getDependants(dependants,dependantsO);
        
        int result = DuplicatesQuery.saveVacant(
            structureDateVacant, managerUserId, position, location, function, dependantsTotal,
            actionVacant, userId, session.get("username")
        );
        
        String verb = actionVacant.equals("I") ? "agregada" : "modificada";
        
        if (result > 0) {
            renderJSON(new Message("Vacante " + verb + " correctamente.", "success"));            
            return;
        }
        
        response.status = 400;
        renderJSON(new Message("La vacante no fue agregado. Por favor, intente nuevamente."));
    }

    /**
     * Metodo que regresa un lista de dependientes 
     * @param dependants
     * @param dependantsO
     * @return 
     */
    private static Integer[][] getDependants(String[] dependants,
            List<EmployeeClima> dependantsO) {
        List<Integer> dep = new ArrayList<>();
        
        for(EmployeeClima clima : dependantsO) {
            dep.add(clima.userId);
        }
        
        String[] newDependants = getNewDepentans(dependants,dep);
        Integer[][] dependantsT = 
                new Integer[(dependantsO.size() + newDependants.length)][2];
        int i = 0;
        for(Integer id : dep) {
            dependantsT[i][0] = id;
            
                if(contains(id, dependants)){
                    dependantsT[i][1] = 0;
                }else {
                    dependantsT[i][1] = -1;
                }
            i++;
        }
        
        for(int k =0; k < newDependants.length; k++){
            dependantsT[i][0] = Integer.parseInt(newDependants[k]);
            dependantsT[i][1] = 1;
            i++;
        }
        return dependantsT;
    }

    private static boolean contains(Integer id, String[] dependants) {
        if(dependants != null) {
            for (String dependant : dependants) {
                if (dependant.equals(id.toString())) {
                    return true;
                }
            }
        
        }
        return false;
    }

    private static String[] getNewDepentans(String[] dependants, 
            List<Integer> dep) {
        List<String> news = new ArrayList<>();
        if(dependants != null) {
            for(int i = 0; i < dependants.length; i++) {
                if(!dep.contains(Integer.parseInt(dependants[i]))) {
                    news.add(dependants[i]);
                }
            }
        }
        
        return news.toArray(new String[news.size()]);
    }
}
