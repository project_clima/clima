package controllers;

import play.*;
import play.mvc.*;
import java.util.*;
import models.*;
import java.lang.reflect.Type;
import com.google.gson.*;
import com.google.gson.reflect.TypeToken;

/**
 * Clase controlador para el manejo de las metas
 * @author Rodolfo Miranda -- Qualtop
 */
@With(Secure.class)
public class Goals extends Controller {

    /**
     * Metodo de inicio
     */
    @Check("goals/paramgoals")
    public static void paramGoals() {
        render();
    }

    /**
     * Metodo que devuelve las metas por año
     * @param year 
     */
    public static void getGoals(Integer year) {
        List<Goal> goals = Goal.find("year = ?1 order by minValue DESC", year).fetch();
        renderJSON(goals);
    }

    /**
     * Metodo que devuelve las metas en base de datos
     */
    public static void getGoalYears() {
        List<Integer> years  = Goal.find("SELECT distinct year from Goal order by year ASC").fetch();
        renderJSON(years);
    }

    /**
     * Metodo para salvar las diferentes metas creadas por el usuario
     * @param goalsJSON
     * @param year 
     */
    public static void saveGoals(String goalsJSON, String year) {
        Gson gson = new GsonBuilder().setDateFormat("dd/mm/yyyy").create();

        Type listType = new TypeToken<List<Goal>>() {
        }.getType();
        List<Goal> goals = gson.fromJson(goalsJSON, listType);
        List<Goal> repeatedOnes = new ArrayList<Goal>();
        Boolean repeated = false;
        
        if (repeated) {
            renderText("{\"repeated\":" + gson.toJson(repeatedOnes) + "}");
            return;
        } else {// Save all:
            for (Goal g : goals) {
                Goal goal = Goal.find("id = ?1 AND year = ?2", g.id, g.year).first();

                // If it exists:
                if (goal != null) {
                    goal.modifiedDate = new Date();
                    goal.modifiedBy = session.get("username");
                    goal.year = g.year;
                    goal.description = g.description;
                    goal.minValue = g.minValue;
                    goal.maxValue = g.maxValue;
                    goal.save();
                } else {
                    // New one:
                    g.id = null;
                    g.modifiedDate = null;
                    g.modifiedBy = null;
                    g.createdBy = session.get("username");
                    g.save();
                }
            }

            String allErased = "false";
            if(goals.size() == 0) {
                allErased = "true";
            }

            renderText("{\"ok\":true, \"year\":\"" + year + "\", \"allErased\":" + allErased + "}");
        }
    }

    /**
     * Metodo para borrar un o unas metas seleccionadas por el usuario
     * @param idsJSON 
     */
    public static void removeGoals(String idsJSON) {
        Gson gson = new Gson();

        Type listType = new TypeToken<List<Integer>>() {
        }.getType();
        List<Integer> ids = gson.fromJson(idsJSON, listType);

        for (Integer i : ids) {
            Goal g = Goal.findById(i);

            if (g != null) {
                g.delete();
            }
        }
    }
}
