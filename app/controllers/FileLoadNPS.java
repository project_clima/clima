package controllers;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.logging.Level;
import models.NpsAnswer;
import models.NpsQuestionAnswer;
import models.Sections;
import models.StoreEmployee;
import models.nps.FileNPS;
import models.nps.FilesLoadNps;
import models.nps.FunctionByTitle;
import models.nps.NpsArea;
import models.nps.NpsEmpNotSales;
import models.nps.NpsPractice;
import net.sf.jxls.exception.ParsePropertyException;
import org.apache.commons.io.IOUtils;
import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import play.*;
import play.libs.Files;
import play.mvc.*;
import querys.nps.EvaluationDetailsQuery;
import utils.RenderExcel;
import utils.nps.enums.LogFileNPSEnum;
import utils.nps.log.LogFileNPS;
import utils.nps.readFile.ReadCatAreaFile;
import utils.nps.readFile.ReadCatFunctionFile;
import utils.nps.readFile.ReadCatNotSalesFile;
import utils.nps.readFile.ReadCatPractFile;
import utils.nps.readFile.ReadCatSectionFile;
import utils.nps.readFile.ReadCatStoreFile;
import utils.nps.util.FormatDateUtil;
import utils.nps.vars.EnvironmentVar;
import utils.nps.writeFile.WriteLogFile;

/**
 * Clase para el manejo de cargas de archivos en el sistema NPS
 * @author Rodolfo Miranda -- Qualtop
 */

@With(Secure.class)
public class FileLoadNPS extends Controller {

    /**
     * Metodo inicial para la pantalla principal
     */
    //@Check("goals/paramgoals")
    public static void index() {
        render();
    }

    /**
     * Metodo para la carga de archivos y mapeo de variables de archivo
     * @param typeO
     * @param title
     * @param file
     * @param load_sap
     * @throws FileNotFoundException 
     */
    
    public static void uploadFile(String typeO, String title, File file,
            String load_sap)
            throws FileNotFoundException {
        try {

            String filePro = new File(Play.getFile("").getAbsolutePath())
                    + File.separator + "uploads" + File.separator + file.getName();
            FileOutputStream moveTo = new FileOutputStream(filePro);

            IOUtils.copy(new FileInputStream(file), moveTo);

            session.put("fileName", title);
            session.put("type", typeO);
            session.put("filePro", filePro);
            session.put("load_sap", load_sap);

        } catch (IOException ex) {
            java.util.logging.Logger.getLogger(FileLoadNPS.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     * Metodo para la implementacion de la logica dependiendo
     * el tipo de carga
     * @throws ParseException 
     */
    
    public static void manualLoadFileJob() throws ParseException {

        String t = session.get("type");
        String fileName = session.get("fileName");
        String tP = "";

        switch (t) {
            case "sap":
                tP = "STRUCT";
                break;
            case "bd":
                tP = "ANSW";
                break;
            case "ct":
                tP = "STRUCT";
                break;
            case "cs":
                tP = "STRUCT";
                break;
            case "cf":
                tP = "STRUCT";
                break;
            case "ca":
                tP = "STRUCT";
                break;
            case "cnv":
                tP = "STRUCT";
                break;
            case "cp":
                tP = "STRUCT";
                break;
        }

        String username = session.get("username");

        FileNPS fileNps = new FileNPS();
        LogFileNPS.updateLog(LogFileNPSEnum.AUTO, fileNps, fileName,
                tP, username);
        fileNps.save();

        FilesLoadNps fileLoad = getFileRegister(username, fileNps.idFile);
        //session.put("fileId", fileLoad.idFileLoad);

        final SchedulerTask.nps.ProcessRHStruct process
                = new SchedulerTask.nps.ProcessRHStruct();
        process.title = session.get("fileName");
        process.file = session.get("filePro");
        process.typeOperation = session.get("type");
        process.load_sap = session.get("load_sap");
        process.userId = session.get("username");
        process.loadFile = fileLoad.idFileLoad;
        process.loadNps = fileNps.idFile;
        process.now();
        final String processId = process.id;

        renderText(fileLoad.idFileLoad);
    }

    /**
     * Metodo para la descarga automatica de un archivo de errores,
     * en el caso que la carga lo halla generado
     * @param file 
     */
    
    public static void downloadFileError(File file) {
        int errorCat = 0;
        List<String> errors = null;
        String username = session.get("username");
        try {
            String fileName = file.getName();
            if (fileName.toLowerCase().contains("AREA".toLowerCase())) {
                ReadCatAreaFile catArea = new ReadCatAreaFile();
                catArea.setFileReader(file);
                catArea.readCvsFile(username);
                errorCat = catArea.getErrores().size();
                errors = catArea.getErrores();
            } else if (fileName.toLowerCase().contains("PRACTICA".toLowerCase())) {
                ReadCatPractFile catPract = new ReadCatPractFile();
                catPract.setFileReader(file);
                catPract.readCvsFile(username);
                errorCat = catPract.getErrores().size();
                errors = catPract.getErrores();
            } else if (fileName.toLowerCase().contains("TIENDA".toLowerCase())) {
                ReadCatStoreFile catStore = new ReadCatStoreFile();
                catStore.setFileReader(file);
                catStore.readCvsFile(username, false);
                errorCat = catStore.getErrores().size();
                errors = catStore.getErrores();
            } else if (fileName.toLowerCase().contains("SECCION".toLowerCase())) {
                ReadCatSectionFile catStore = new ReadCatSectionFile();
                catStore.setFileReader(file);
                catStore.readCvsFile(username);
                errorCat = catStore.getErrores().size();
                errors = catStore.getErrores();
            } else if (fileName.toLowerCase().contains("FUNCION".toLowerCase())) {
                ReadCatFunctionFile catStore = new ReadCatFunctionFile();
                catStore.setFileReader(file);
                catStore.readCvsFile(username);
                errorCat = catStore.getErrores().size();
                errors = catStore.getErrores();
            } else if (fileName.toLowerCase().contains("NO".toLowerCase())) {
                ReadCatNotSalesFile catStore = new ReadCatNotSalesFile();
                catStore.setFileReader(file);
                catStore.readCvsFile(username);
                errorCat = catStore.getErrores().size();
                errors = catStore.getErrores();
            }
            if (errorCat > 0) {
                File errorFile = WriteLogFile.write(errors, fileName);
                InputStream binaryData = new FileInputStream(errorFile);
                response.setContentTypeIfNotSet("text/csv; charset=ISO-8859-1");
                renderBinary(binaryData, errorFile.getName());
            }

        } catch (FileNotFoundException ex) {
            java.util.logging.Logger.getLogger(FileLoadNPS.class.getName())
                    .log(Level.SEVERE, null, ex);
        } catch (Exception ex) {
            java.util.logging.Logger.getLogger(FileLoadNPS.class.getName()).log(Level.SEVERE, null, ex);
        }

    }

    /**
     * Metodo para la descarga de catalogos desde la pantalla.
     * @param type 
     */
    
    public static void downloadCatalog(String type) {

        try {
            List records = new ArrayList<>();
            String template = "";
            String path = "";
            switch (type) {
                case "ct":
                    records = StoreEmployee.find("select m.idStore, m.descriptionStore,"
                            + "m.zoneNps,m.groupId,m.busId from StoreEmployee m "
                            + "where  m.structureDate IN (select max(s.structureDate) from StoreEmployee s)\n"
                            + "order by m.idStore").fetch();
                    template = "Catalogo de Tiendas.xls";
                    path = "app/excel/views/Catalogs/tiendas.xls";
                    break;
                case "ca":
                    records = NpsArea.find("Select m.idArea, m.desc from NpsArea m").fetch();
                    template = "Catalogo de Areas.xls";
                    path = "app/excel/views/Catalogs/areas.xls";
                    break;
                case "cp":
                    records = NpsPractice.find("select m.practice, m.desc, m.idArea"
                            + " from NpsPractice m").fetch();
                    template = "Catalogo de Practicas.xls";
                    path = "app/excel/views/Catalogs/practicas.xls";
                    break;
                case "cs":
                    records = Sections.find("select m.id ,m.nombreSeccion from Sections m").fetch();
                    template = "Catalogo de Secciones.xls";
                    path = "app/excel/views/Catalogs/secciones.xls";
                    break;
                case "cf":
                    records = FunctionByTitle.find("select m.key.title, m.key.idFunction, s.descriptionFunction"
                            + " from FunctionByTitle m, FunctionEmployee s where m.key.idFunction = s.idFunction ").fetch();
                    template = "Catalogo de Funciones.xls";
                    path = "app/excel/views/Catalogs/funciones.xls";
                    break;
                case "cnv":
                    records = NpsEmpNotSales.find("select m.empNumber, m.idStore,"
                            + "m.idArea, m.name, m.userId, m.userIdChief,"
                            + "m.userIdManager, m.userIdDirector, m.userIdZonal, m.idSection"
                            + " from NpsEmpNotSales m").fetch();
                    template = "Catalogo de No Ventas.xls";
                    path = "app/excel/views/Catalogs/ventas.xls";
                    break;
                default:
                    break;
            }

            renderArgs.put("records", records);
            renderArgs.put(RenderExcel.RA_FILENAME, template);
            RenderExcel renderer = null;
            renderer = new RenderExcel(path);

            renderer.render();
        } catch (ParsePropertyException ex) {
            java.util.logging.Logger.getLogger(FileLoadNPS.class.getName()).log(Level.SEVERE, null, ex);
        } catch (InvalidFormatException ex) {
            java.util.logging.Logger.getLogger(FileLoadNPS.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            java.util.logging.Logger.getLogger(FileLoadNPS.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     * Metodo para el borrado segun mes/año de evaluaciones
     * @param month
     * @param year
     * @return 
     */
    public static Integer eraserMonth(Long month, Long year) {

        List<NpsAnswer> answers = NpsAnswer.find("month = ?1 and year = ?2", month, year).fetch();

        for (NpsAnswer answer : answers) {
            NpsQuestionAnswer.delete("idAnswer = ?1", answer.id);
        }

        Integer erasers = NpsAnswer.delete("month = ?1 and year = ?2", month, year);

        return erasers;
    }

    /**
     * Metodo para obtener el estatus del avance en la carga de archivos
     * @param idFile 
     */
    
    public static void getStatus(Long idFile) {
        //String idFile = session.get("fileId");
        if (idFile != null) {
            String filePro = EnvironmentVar.TXTS_DIRECTORY + File.separator + idFile + "_.txt";
            String percent = "";
            String comments = "";
           
            try{
                File file = new File(filePro);

                if (file.exists()) {
                    FileReader reader = new FileReader(file);
                    BufferedReader buffer = 
                            new BufferedReader(reader);
                    String line = buffer.readLine();
                    reader.close();
                    
                    if (line != null) {
                        String fields[] = line.split(",");
                        percent = fields[1];
                        comments = fields[2];
                    }
                    
                    renderText(percent + "," + comments+ "," + idFile);
                }else {
                    renderText("null,null");
                }
            } catch(IOException e){
                Logger.info("Error " + e.getMessage());
            }
        }
        
            FilesLoadNps file = FilesLoadNps.findById(idFile);       
    }

    /**
     * Metodo para escribir el archivo de avance segun el id de proceso
     * @param username
     * @param idNps
     * @return 
     */
    
    public static FilesLoadNps getFileRegister(String username, int idNps) {

        try {
            FilesLoadNps fileLoad = new FilesLoadNps();
            fileLoad.idFileLoad = EvaluationDetailsQuery.getNextValFileLoad();
            fileLoad.creFor = username;
            fileLoad.creDate = FormatDateUtil.ownFormat2.
                    parse(FormatDateUtil.ownFormat2.format(new Date()));
            fileLoad.idFile = idNps;
            fileLoad.startDate = FormatDateUtil.ownFormat2.
                    parse(FormatDateUtil.ownFormat2.format(new Date()));
            
            saveFile(fileLoad);

            return fileLoad;
        } catch (ParseException ex) {
            Logger.info(ex.getMessage());
        }

        return null;

    }

    /**
     * Metodo de utileria para la escritura de archivo
     * @param fileLoad 
     */
    
    private static void saveFile(FilesLoadNps fileLoad) {
        String filePro = EnvironmentVar.TXTS_DIRECTORY + File.separator + fileLoad.idFileLoad + "_.txt";
        
        try{
            File file = new File(filePro);

            if (!file.exists()) {
                FileWriter writer = new FileWriter(file);
                writer.write(fileLoad.idFileLoad+","+fileLoad.percent+","+fileLoad.comments+"\n");
                writer.close();
            }
        } catch(IOException e){
            Logger.info("Error " + e.getMessage());
        }
    }
    
    /**
     * Metodo para el borrado del archivo de avance despues de terminar el
     * proceso de carga
     * @param idFile 
     */
    
    public static void deleteFile(Long idFile) {
        //String idFile = session.get("fileId");
        boolean exit = false;
        if (idFile != null) {
            String filePro = EnvironmentVar.TXTS_DIRECTORY + File.separator + idFile + "_.txt";
            
            File file = new File(filePro);
            if (file.exists()) {
                exit = Files.delete(file);
            }
        }   
        renderText(exit);
    }
}
