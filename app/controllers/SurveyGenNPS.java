package controllers;

import play.*;
import play.mvc.*;
import java.util.*;
import models.*;

@With(Secure.class)
public class SurveyGenNPS extends Controller {

    public static void index() {
        String place = "NPS";
        renderTemplate("SurveysGenerator/index.html", place);
    }

    public static void sections(int surveyId) {
        Survey survey = Survey.findById(surveyId);
        String place = "NPS";
        renderTemplate("SurveysGenerator/sections.html", survey, place);
    }
}
