package controllers;

import play.*;
import play.mvc.*;
import java.util.*;
import models.*;
import play.db.jpa.JPA;
import javax.persistence.Query;
import querys.ActionPlanQuery;
import com.google.gson.*;
import javax.mail.MessagingException;
import play.templates.Template;
import play.templates.TemplateLoader;
import utils.GmailMailer;
import querys.ChangeQuery;

public class ChangePassword extends Controller {

    public static void change(String userId) throws Throwable {
        if (userId == null || userId.isEmpty()) {
            controllers.Secure.login();
        }
        
        EmployeeClima employeeClima = EmployeeClima.find(
                "UPPER(standard_hash(to_char(userId), 'MD5')) = UPPER(?1)",
                userId
        ).first();
        
        if (employeeClima == null) {
            controllers.Secure.login();
        }
        
        render(employeeClima);
    }

    public static void updatePsw() {
        render();
    }

    public static void changePsw(String userId, String psw) {
        int update = 0;
        update = ChangeQuery.setNewPassword(userId, psw);
        renderJSON(update);
    }

    public static void changeNewPws(String pwsOld, String psw) {
        Usuario user = Usuario.find("username = ?1", session.get("username")).first();
        int update = 0;
        update = ChangeQuery.setNew(user.id, psw, pwsOld);
        renderJSON(update);
    }

    public static void sendChangePsw() {
        render();
    }

    public static void sendEmail(String email) throws MessagingException {
        if (email != null && ! email.isEmpty()) {            
            EmployeeClima employeeClima = EmployeeClima.find(
                "UPPER(email) = UPPER(?1)", email
            ).first();

            if (employeeClima != null) {
                String to = employeeClima.email;
                GmailMailer mailer = new GmailMailer();
                
                String emailContents = getEmailContents(employeeClima);

                mailer.sendGeneral(
                    to,
                    "Liverpool Portal Único :: Recuperar Contraseña",
                    emailContents,
                    null
                );                
            }
        }
    }

    private static String getEmailContents(EmployeeClima employee) {
        Template template = TemplateLoader.load("ChangePassword/recover-password-email-template.html");

        HashMap<String, Object> tableArgs = new HashMap();
        tableArgs.put("employee", employee);
        tableArgs.put("baseUrl", Play.configuration.getProperty("application.baseUrl"));

        return template.render(tableArgs);
    }
}
