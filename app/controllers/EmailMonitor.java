package controllers;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import play.mvc.*;
import java.util.*;
import java.text.SimpleDateFormat;
import json.entities.SurveySendSummary;
import models.*;
import net.sf.jxls.exception.ParsePropertyException;
import utils.Pagination;
import net.sf.jxls.transformer.XLSTransformer;
import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.apache.poi.ss.usermodel.Workbook;
import play.vfs.VirtualFile;
import utils.RenderExcel;

/**
 * Clase controlador para el manejo del monitor de correos
 * @author Rodolfo Miranda -- Qualtop
 */
public class EmailMonitor extends Controller {
    /**
     * Metodo que obtiene los tipos de encuestas en el modulo de Clima
     */
    public static void index() {
        List<SurveyType> surveyTypes = SurveyType.find(
            "id not in (4, 5)"
        ).fetch();
        
        render(surveyTypes);
    }
    
    /**
     * Metodo para obtener las encuestas por tipo de encuesta
     * @param surveyTypeId 
     */
    public static void getSurveys(int surveyTypeId) {
        List<Object> surveys = Survey.find(
            "select id, name from Survey where surveyType.id = ?1 and sysdate between dateInitial and dateEnd",
            surveyTypeId
        ).fetch();
        
        renderJSON(surveys);
    }
    
    /**
     * Metodo para obtener la encuestas por id de encuesta
     * @param surveyId 
     */
    public static void survey(int surveyId) {
        SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy H:mm:s");
        Survey survey = Survey.findById(surveyId);
        
        long sent     = EmailSendLog.count("surveyId = ?1 and sent = 1", surveyId);
        long notSent  = EmailSendLog.count("surveyId = ?1 and sent = 0", surveyId);
        String date   = survey.sendDate != null ? sdf.format(survey.sendDate) : "No enviada";
        
        renderJSON(new SurveySendSummary(date, sent, notSent));
    }
    
    /**
     * Metodo que detalla el tipo de encuesta que es
     * @param surveyId
     * @param type
     * @param iDisplayLength
     * @param startDate
     * @param endDate
     * @param iDisplayStart
     * @param sEcho
     * @param sSortDir_0
     * @param iSortCol_0 
     */
    public static void detail(int surveyId, boolean type, int iDisplayLength, String startDate,
                              String endDate, int iDisplayStart, String sEcho,
                              String sSortDir_0, int iSortCol_0) {
        
        int start        = iDisplayStart;
        int end          = iDisplayLength;        
        String condition = "surveyId = ?1 and sent = ?2";
        long count       = EmailSendLog.count(condition, surveyId, type);
        
        List employees = EmailSendLog.find(
            "select esl.employeeNumber, esl.name, esl.email, esl.function, esl.message, " +
            "esl.sendDate from EmailSendLog esl where esl.surveyId = ?1 and esl.sent = ?2 " +
            " order by id desc",
            surveyId, type
        ).from(start).fetch(end);
        
        renderJSON(new Pagination(sEcho, count, count, employees));        
    }
    
    /**
     * MEtodo para exportar los resultados mostrados en pantalla
     * @param surveyId
     * @param type
     * @throws ParsePropertyException
     * @throws InvalidFormatException
     * @throws IOException 
     */
    public static void export(int surveyId, boolean type)
    throws ParsePropertyException, InvalidFormatException, IOException {
        List employees = EmailSendLog.find(
            "select esl.employeeNumber, esl.name, esl.email, esl.function, esl.message, " +
            "esl.sendDate from EmailSendLog esl where esl.surveyId = ?1 and esl.sent = ?2",
            surveyId, type
        ).fetch();
        
        String fileName = type ? "ReporteEnviados.xls" : "ReporteNoEnviados.xls";
        
        renderArgs.put("employees", employees);
        renderArgs.put(RenderExcel.RA_FILENAME, fileName);
        RenderExcel renderer = new RenderExcel("app/excel/views/EmailMonitor/export.xls");
        renderer.render();
    }
}
