package controllers;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.List;

import org.apache.poi.openxml4j.exceptions.InvalidFormatException;

import io.excel.commons.ExcelReader;
import io.excel.commons.exceptions.NotSupportedFileException;
import io.excel.model.Row;
import jobs.nps.DirectorEvaluatedJob;
import jobs.nps.EmployeeEvaluatedJob;
import jobs.nps.EmployeeHeadEvaluatedJob;
import jobs.nps.ManagerEvaluatedJob;
import jobs.nps.OperationsEvaluatedJob;
import jobs.nps.ZoneBossEvaluatedJob;
import json.csi.dto.SurveyCargaResponse;
import net.sf.jxls.exception.ParsePropertyException;
import play.Logger;
import play.mvc.Controller;
import play.mvc.With;
import utils.RenderExcel;

@With(Secure.class)
public class EmailSendingNPS extends Controller {

    private static ExcelReader excelReader = new ExcelReader();

    public static void index() {
        render();
    }

    public static void uploadFile(File npsFile) {
        String typeload = "1";

        SurveyCargaResponse response = new SurveyCargaResponse();
        boolean isValid = true;

        try {
            List<Row> rows = excelReader.readExcelFile(npsFile);

            if (rows.isEmpty()) {
                response.setIdResponse(500);
                response.setErrorMessage("El archivo no contiene información");
                Logger.info("El archivo no contiene información");
                isValid = false;
            }
            if (typeload.equals("1")) {
                if (!isCrossFileStructureValid(rows)) {
                    response.setIdResponse(500);
                    response.setErrorMessage("El archivo no contiene la estructura correcta, debe contener 5 columnas");
                    Logger.info("El archivo no contiene la estructura correcta, debe contener 5 columnas");
                    isValid = false;
                }
            }

            /*
             * if(rows.size() > 1 && isValid) { if(typeload.equals("1")) { rows
             * = saveCrossEvaluationEmployees(rows, idUploadSurvey);
             * if(rows.isEmpty()){ response.setIdResponse(500);
             * response.setErrorMessage(
             * "No hay empleados a evaluar para esta encuesta."); isValid =
             * false; } } else { rows = saveResultEvaluations(rows,
             * idUploadSurvey); }
             * 
             * if(isValid){ response.setIdResponse(200);
             * if(typeload.equals("1")) { for (Row row : rows) {
             * if(row.getIndex() != 1) { //If row is different at header
             * SurveyCargaRow cargaRow = new SurveyCargaRow(); List<Cell> cells
             * = row.getCells(); if (cells != null && !cells.isEmpty()){
             * cargaRow.setKey(""+cells.get(0).getValue()); if (cells.size() >
             * 1){ cargaRow.setValue(""+cells.get(1).getValue()); }
             * response.getCargaRows().add(cargaRow); } } } } } }
             */

        } catch (FileNotFoundException e) {
            response.setIdResponse(500);
            e.printStackTrace();
        } catch (IOException e) {
            response.setIdResponse(500);
            // TODO Auto-generated catch block
            e.printStackTrace();
        } catch (NotSupportedFileException e) {
            response.setIdResponse(500);
            // TODO Auto-generated catch block
            e.printStackTrace();
        }

        renderJSON(response);

    }

    private static boolean isCrossFileStructureValid(List<Row> rows) {
        for (Row row : rows) {
            if (row.getCells().size() < 5)
                return false;
            else if (row.getCells().size() > 5)
                return false;
        }
        return true;
    }

    public static void sendMailSalesMan() {

        new EmployeeEvaluatedJob().now();

        String response = "200";

        renderJSON(response);
    }

    public static void sendMailHeadEvaluated() {

        new EmployeeHeadEvaluatedJob().now();

        String response = "200";

        renderJSON(response);
    }

    public static void sendMailManager() {

        new ManagerEvaluatedJob().now();

        String response = "200";

        renderJSON(response);
    }

    public static void sendMailDirector() {

        new DirectorEvaluatedJob().now();

        String response = "200";

        renderJSON(response);
    }

    public static void sendMailZoneBoss() {

        new ZoneBossEvaluatedJob().now();

        String response = "200";

        renderJSON(response);
    }

    public static void sendMailOperations() {

        new OperationsEvaluatedJob().now();

        String response = "200";

        renderJSON(response);
    }

    public static void downloadLayout() throws ParsePropertyException, InvalidFormatException, IOException {
        renderArgs.put(RenderExcel.RA_FILENAME, "Layout.xlsx");
        RenderExcel renderExcel = new RenderExcel("app/excel/views/mailsNPS/layout.xlsx");
        renderExcel.render();
    }
}