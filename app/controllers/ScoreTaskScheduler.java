package controllers;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import play.mvc.*;
import java.util.*;
import models.*;
import querys.StructureQuery;
import utils.Message;

@With(Secure.class)
public class ScoreTaskScheduler extends Controller {
    public static void addCalculationTask() {
        List structureDates = StructureQuery.getDatesStructures();
        
        render(structureDates);
    }
    
    public static void save(String structureDate, String executionDay, String executionHour) {
        SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy H:mm");
        ScoreTask task       = new ScoreTask();
        String executionDate = executionDay + " " + executionHour;
        
        try {
            task.structureDate    = structureDate;
            task.executionDate    = sdf.parse(executionDate);
            task.createdBy        = session.get("username");
            task.modifiedBy       = session.get("username");
            task.creationDate     = new Date();
            task.modificationDate = new Date();
            task.save();
            
            renderJSON(
                new Message(
                    String.format(
                        "Tarea programada correctamente. " + 
                        "Se ejecutará el %s a las %s horas",
                        executionDay,
                        executionHour
                    ),
                    null,
                    false
                )
            );
        } catch (ParseException e) {
            renderJSON(new Message(
                    "La tarea no pudo ser programada. Por favor, intente nuevamente",
                    null,
                    true
                )
            );
        }
    }
}