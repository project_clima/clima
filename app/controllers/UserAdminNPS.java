package controllers;

import java.math.BigDecimal;
import java.util.List;

import javax.persistence.FlushModeType;
import javax.persistence.Query;

import models.Rol;
import models.UserRole;
import play.db.jpa.JPA;
import play.mvc.Controller;
import play.mvc.With;

@With(Secure.class)
public class UserAdminNPS extends Controller {
    public static final int HOME_VER_TODO = 105;
    public static final int HOME_VER = 106;
    public static final int EVALUACIONES_VER_TODO = 107;
    public static final int EVALUACIONES_VER = 108;
    public static final int REPORTES_VER_TODO = 104;
    public static final int REPORTES_VER = 124;
    public static final int IMPUGNAR_REALIZAR_TODO = 109;
    public static final int IMPUGNAR_REALIZAR = 110;
    public static final int IMPUGNAR_VER_TODO = 111;
    public static final int IMPUGNAR_VER = 112;
    public static final int IMPUGNAR_CONTESTAR_TODO = 113;
    public static final int IMPUGNAR_CONTESTAR = 114;
    public static final int SEGUIMIENTO_REALIZAR_TODO = 115;
    public static final int SEGUIMIENTO_REALIZAR = 116;
    public static final int SEGUIMIENTO_VER_TODO = 117;
    public static final int SEGUIMIENTO_VER = 118;
    public static final int REPLICA_VER_TODO = 125;
    public static final int REPLICA_CONTESTAR_TODO = 127;
    public static final int REPLICA_CONTESTAR = 120;
    public static final int REPLICA_VER = 126;
    public static final int ENVIAR_CORREO = 93;
    public static final int HISTORICO_VER = 123;
    public static final int HISTORICO_VER_TODO = 100;

    public static void index() {
        render();
    }

    public static void addEmployeeNumber() {
        render();
    }

    public static void getNPSFunctions() {
        Query query = JPA.em().createNativeQuery(
                "Select fc_desc_function, fn_id_function from portalunico.pul_function order by fc_desc_function asc");
        List list = query.getResultList();
        renderJSON(list);
    }

    public static void getFunctionRoles(String functionName, Integer idFunction) {
        Query query = JPA.em().createNativeQuery("SELECT RA.FN_ACTION_ID FROM PORTALUNICO.PUL_ROLE_ACTION RA "
                + "INNER JOIN PORTALUNICO.PUL_ROLE R ON RA.FN_ID_ROLE = R.FN_ID_ROLE " + "WHERE R.FC_NAME_ROLE = 'NPS-"
                + functionName + "-" + idFunction + "' " + "AND R.FN_ID_MODULE = 3");
        renderJSON(query.getResultList());
    }

    public static void getEmployeeRoles(int employeeNumber) {
        Query query = JPA.em().createNativeQuery("SELECT DISTINCT RA.FN_ACTION_ID FROM PORTALUNICO.PUL_ROLE_ACTION RA "
                + "INNER JOIN PORTALUNICO.PUL_ROLE R ON RA.FN_ID_ROLE = R.FN_ID_ROLE "
                + "INNER JOIN PORTALUNICO.PUL_USER_ROLE UR ON RA.FN_ID_ROLE = UR.FN_ID_ROLE "
                + "INNER JOIN PORTALUNICO.PUL_STRUCTURE S ON UR.FN_ID_USER = S.FN_USERID "
                + "WHERE S.FN_EMPID = " + employeeNumber + " AND R.FN_ID_MODULE = 3");
        renderJSON(query.getResultList());
    }

    public static void validateEmployeeNumber(Integer employeeNumber) {
        Query query = JPA.em().createNativeQuery(
                "Select fn_userid, fc_firstname, fc_lastname, fc_desc_function from portalunico.pul_structure s inner join portalunico.pul_user u on fn_userid = fn_id_user"
                        + " inner join portalunico.pul_function f on s.fn_id_function = f.fn_id_function where fn_empid = "
                        + employeeNumber);
        renderJSON(query.getResultList());
    }

    public static void asignarPermisosPorFuncion(int home, int evaluaciones, int impugnaciones, int seguimiento,
            int replica, int reporte, int notificaciones, int historico, String nameFunction, Integer idFunction) {
        Query query = JPA.em().createNativeQuery(
                "Select fn_userid from portalunico.pul_structure s " 
                + "inner join portalunico.pul_function f on s.fn_id_function = f.fn_id_function "
                + "where f.fn_id_function = " + idFunction);
        List<BigDecimal> usuarios = query.getResultList();
        String nombreRol = "NPS-" + nameFunction + "-" + idFunction;
        Query revisarRole = JPA.em()
                .createNativeQuery("Select fn_id_role from portalunico.pul_role where fc_name_role = '" + nombreRol
                        + "' and fn_id_module = 3");
        BigDecimal idRole = BigDecimal.ZERO;

        if (revisarRole.getResultList().size() < 1) {
            idRole = (BigDecimal) JPA.em().createNativeQuery("SELECT MAX(FN_ID_ROLE)+1 FROM PORTALUNICO.PUL_ROLE")
                    .getResultList().get(0);
            Query crearRol = JPA.em()
                    .createNativeQuery("INSERT INTO PORTALUNICO.PUL_ROLE (FN_ID_ROLE, FC_NAME_ROLE, FN_ID_MODULE)"
                            + "VALUES(" + idRole + ", " + "'NPS-" + nameFunction + "-" + idFunction + "', 3)");
            crearRol.setFlushMode(FlushModeType.AUTO);
            crearRol.executeUpdate();
        } else {
            idRole = (BigDecimal) revisarRole.getResultList().get(0);
        }

        for (BigDecimal usuarioId : usuarios) {
            Query queryUsuario = JPA.em().createNativeQuery(
                    "Select fn_id_user from portalunico.pul_nps_type_assign_role where upper(fn_type_role) = 'INDIVIDUAL' and fn_id_user = "
                            + usuarioId);

            if (queryUsuario.getResultList().size() < 1) {
                List assignType = JPA.em()
                        .createNativeQuery("Select * from portalunico.pul_nps_type_assign_role where fn_id_user = "
                                + usuarioId + " and upper(fn_type_role) = 'FUNCTION'")
                        .getResultList();

                if (assignType.size() < 1) {
                    JPA.em().createNativeQuery(
                            "INSERT INTO PORTALUNICO.pul_nps_type_assign_role (FN_ID_USER, FN_TYPE_ROLE) VALUES("
                                    + usuarioId + ", 'FUNCTION')")
                            .executeUpdate();
                }
                String queryPrincipio = "INSERT INTO PORTALUNICO.PUL_ROLE_ACTION(FN_ID_ROLE, FN_ACTION_ID) VALUES";

                String queryHome = getQueryHome(home, idRole);
                String queryEval = getQueryEval(evaluaciones, idRole);
                String queryRepl = getQueryRepl(replica, idRole);
                String queryRepo = getQueryRepo(reporte, idRole);
                String queryNoti = getQueryNoti(notificaciones, idRole);
                String querySeg = getQuerySeg(seguimiento, idRole);
                String queryHis = getQueryHis(historico, idRole);

                if (impugnaciones == 5) {
                    String queryImp1 = getQueryImp(5, idRole);
                    String queryImp2 = getQueryImp(3, idRole);
                    if (!queryImp1.isEmpty())
                        JPA.em().createNativeQuery(queryPrincipio + queryImp1).executeUpdate();
                    if (!queryImp2.isEmpty())
                        JPA.em().createNativeQuery(queryPrincipio + queryImp2).executeUpdate();
                } else if (impugnaciones == 6) {
                    String queryImp1 = getQueryImp(6, idRole);
                    String queryImp2 = getQueryImp(4, idRole);
                    if (!queryImp1.isEmpty())
                        JPA.em().createNativeQuery(queryPrincipio + queryImp1).executeUpdate();
                    if (!queryImp2.isEmpty())
                        JPA.em().createNativeQuery(queryPrincipio + queryImp2).executeUpdate();
                } else {
                    String queryImp1 = getQueryImp(impugnaciones, idRole);
                    if (!queryImp1.isEmpty())
                        JPA.em().createNativeQuery(queryPrincipio + queryImp1).executeUpdate();
                }

                if (!queryHome.isEmpty())
                    JPA.em().createNativeQuery(queryPrincipio + queryHome).executeUpdate();
                if (!queryEval.isEmpty())
                    JPA.em().createNativeQuery(queryPrincipio + queryEval).executeUpdate();
                if (!queryRepl.isEmpty())
                    JPA.em().createNativeQuery(queryPrincipio + queryRepl).executeUpdate();
                if (!queryRepo.isEmpty())
                    JPA.em().createNativeQuery(queryPrincipio + queryRepo).executeUpdate();
                if (!queryNoti.isEmpty())
                    JPA.em().createNativeQuery(queryPrincipio + queryNoti).executeUpdate();
                if (!querySeg.isEmpty())
                    JPA.em().createNativeQuery(queryPrincipio + querySeg).executeUpdate();
                if (!queryHis.isEmpty())
                    JPA.em().createNativeQuery(queryPrincipio + queryHis).executeUpdate();

                Query userRole = JPA.em()
                        .createNativeQuery("Select fn_id_role from portalunico.pul_user_role where fn_id_user = "
                                + usuarioId + " and fn_id_role = " + idRole);
                if (userRole.getResultList().size() < 1) {
                    JPA.em().createNativeQuery("INSERT INTO PORTALUNICO.PUL_USER_ROLE (FN_ID_USER, FN_ID_ROLE)"
                            + "VALUES(" + usuarioId + ", " + idRole + ")").executeUpdate();
                }
            }
        }
    }

    public static boolean existeRoleAction(BigDecimal idRole, int action) {
        return (JPA.em().createNativeQuery("Select fn_id_role from PORTALUNICO.PUL_ROLE_ACTION where fn_id_role = "
                + idRole + " and fn_action_id=" + action).getResultList().size() > 0);
    }

    public static String getQueryEval(int eval, BigDecimal idRole) {
        String query = "";

        switch (eval) {
        case 0:
            deleteRoleAction(idRole, EVALUACIONES_VER);
            deleteRoleAction(idRole, EVALUACIONES_VER_TODO);
            break;
        case 1:
            deleteRoleAction(idRole, EVALUACIONES_VER_TODO);
            if (!existeRoleAction(idRole, EVALUACIONES_VER))
                query = "(" + idRole + "," + EVALUACIONES_VER + ")";
            break;
        case 2:
            deleteRoleAction(idRole, EVALUACIONES_VER);
            if (!existeRoleAction(idRole, EVALUACIONES_VER_TODO))
                query = "(" + idRole + "," + EVALUACIONES_VER_TODO + ")";
            break;
        }
        return query;
    }

    public static String getQueryHis(int hist, BigDecimal idRole) {
        String query = "";

        switch (hist) {
        case 0:
            deleteRoleAction(idRole, HISTORICO_VER);
            deleteRoleAction(idRole, HISTORICO_VER_TODO);
            break;
        case 1:
            deleteRoleAction(idRole, HISTORICO_VER_TODO);
            if (!existeRoleAction(idRole, HISTORICO_VER))
                query = "(" + idRole + "," + HISTORICO_VER + ")";
            break;
        case 2:
            deleteRoleAction(idRole, HISTORICO_VER);
            if (!existeRoleAction(idRole, HISTORICO_VER_TODO)) 
            	query = "(" + idRole + "," + HISTORICO_VER_TODO + ")";                          
            break;
        }
        return query;
    }

    public static String getQueryHome(int home, BigDecimal idRole) {
        String query = "";

        switch (home) {
        case 0:
            deleteRoleAction(idRole, HOME_VER);
            deleteRoleAction(idRole, HOME_VER_TODO);
            break;
        case 1:
            deleteRoleAction(idRole, HOME_VER_TODO);
            if (!existeRoleAction(idRole, HOME_VER))
                query = "(" + idRole + "," + HOME_VER + ")";
            break;
        case 2:
            deleteRoleAction(idRole, HOME_VER);
            if (!existeRoleAction(idRole, HOME_VER_TODO))
                query = "(" + idRole + "," + HOME_VER_TODO + ")";
            break;
        }
        return query;
    }

    public static String getQueryRepl(int repl, BigDecimal idRole) {
        String query = "";

        switch (repl) {
        case 0:
            deleteRoleAction(idRole, REPLICA_VER);
            deleteRoleAction(idRole, REPLICA_VER_TODO);
            deleteRoleAction(idRole, REPLICA_CONTESTAR_TODO);
            deleteRoleAction(idRole, REPLICA_CONTESTAR);
            break;
        case 1:
            deleteRoleAction(idRole, REPLICA_CONTESTAR_TODO);
            deleteRoleAction(idRole, REPLICA_CONTESTAR);
            deleteRoleAction(idRole, REPLICA_VER_TODO);
            if (!existeRoleAction(idRole, REPLICA_VER))
                query = "(" + idRole + "," + REPLICA_VER + ")";
            break;
        case 2:
            deleteRoleAction(idRole, REPLICA_CONTESTAR_TODO);
            deleteRoleAction(idRole, REPLICA_CONTESTAR);
            deleteRoleAction(idRole, REPLICA_VER);
            if (!existeRoleAction(idRole, REPLICA_VER_TODO))
                query = "(" + idRole + "," + REPLICA_VER_TODO + ")";
            break;
        case 3:
            deleteRoleAction(idRole, REPLICA_VER);
            deleteRoleAction(idRole, REPLICA_VER_TODO);
            deleteRoleAction(idRole, REPLICA_CONTESTAR_TODO);
            if (!existeRoleAction(idRole, REPLICA_CONTESTAR))
                query = "(" + idRole + "," + REPLICA_CONTESTAR + ")";
            break;
        case 4:
            deleteRoleAction(idRole, REPLICA_VER);
            deleteRoleAction(idRole, REPLICA_VER_TODO);
            deleteRoleAction(idRole, REPLICA_CONTESTAR);
            if (!existeRoleAction(idRole, REPLICA_CONTESTAR_TODO))
                query = "(" + idRole + "," + REPLICA_CONTESTAR_TODO + ")";
            break;
        }
        return query;
    }

    public static String getQueryNoti(int noti, BigDecimal idRole) {
        String query = "";
        
        switch (noti) {
        case 0:
            deleteRoleAction(idRole, ENVIAR_CORREO);
            break;
        case 1:
            if (!existeRoleAction(idRole, ENVIAR_CORREO))
                query = "(" + idRole + "," + ENVIAR_CORREO + ")";
            break;
        }
        return query;
    }

    public static String getQueryRepo(int repo, BigDecimal idRole) {
        String query = "";
        
        switch (repo) {
        case 0:
            deleteRoleAction(idRole, REPORTES_VER);
            deleteRoleAction(idRole, REPORTES_VER_TODO);
            break;
        case 1:
            deleteRoleAction(idRole, REPORTES_VER_TODO);
            if (!existeRoleAction(idRole, REPORTES_VER))
                query = "(" + idRole + "," + REPORTES_VER + ")";
            break;
        case 2:
            deleteRoleAction(idRole, REPORTES_VER);
            if (!existeRoleAction(idRole, REPORTES_VER_TODO))
                query = "(" + idRole + "," + REPORTES_VER_TODO + ")";
            break;
        }
        return query;
    }

    public static String getQuerySeg(int seg, BigDecimal idRole) {
        String query = "";

        switch (seg) {
        case 0:
            deleteRoleAction(idRole, SEGUIMIENTO_REALIZAR);
            deleteRoleAction(idRole, SEGUIMIENTO_REALIZAR_TODO);
            deleteRoleAction(idRole, SEGUIMIENTO_VER);
            deleteRoleAction(idRole, SEGUIMIENTO_VER_TODO);
            break;
        case 1:
            deleteRoleAction(idRole, SEGUIMIENTO_VER_TODO);
            deleteRoleAction(idRole, SEGUIMIENTO_REALIZAR);
            deleteRoleAction(idRole, SEGUIMIENTO_REALIZAR_TODO);
            if (!existeRoleAction(idRole, SEGUIMIENTO_VER))
                query = "(" + idRole + "," + SEGUIMIENTO_VER + ")";
            break;
        case 2:
            deleteRoleAction(idRole, SEGUIMIENTO_REALIZAR);
            deleteRoleAction(idRole, SEGUIMIENTO_REALIZAR_TODO);
            deleteRoleAction(idRole, SEGUIMIENTO_VER);
            if (!existeRoleAction(idRole, SEGUIMIENTO_VER_TODO))
                query = "(" + idRole + "," + SEGUIMIENTO_VER_TODO + ")";
            break;
        case 3:
            deleteRoleAction(idRole, SEGUIMIENTO_REALIZAR_TODO);
            deleteRoleAction(idRole, SEGUIMIENTO_VER);
            deleteRoleAction(idRole, SEGUIMIENTO_VER_TODO);
            if (!existeRoleAction(idRole, SEGUIMIENTO_REALIZAR))
                query = "(" + idRole + "," + SEGUIMIENTO_REALIZAR + ")";
            break;
        case 4:
            deleteRoleAction(idRole, SEGUIMIENTO_REALIZAR);
            deleteRoleAction(idRole, SEGUIMIENTO_VER);
            deleteRoleAction(idRole, SEGUIMIENTO_VER_TODO);
            if (!existeRoleAction(idRole, SEGUIMIENTO_REALIZAR_TODO))
                query = "(" + idRole + "," + SEGUIMIENTO_REALIZAR_TODO + ")";
            break;
        }
        return query;
    }

    public static String getQueryImp(int imp, BigDecimal idRole) {
        String query = "";

        switch (imp) {
        case 0:
            deleteRoleAction(idRole, IMPUGNAR_CONTESTAR);
            deleteRoleAction(idRole, IMPUGNAR_CONTESTAR_TODO);
            deleteRoleAction(idRole, IMPUGNAR_REALIZAR);
            deleteRoleAction(idRole, IMPUGNAR_REALIZAR_TODO);
            deleteRoleAction(idRole, IMPUGNAR_VER);
            deleteRoleAction(idRole, IMPUGNAR_VER_TODO);
            break;
        case 1:
            deleteRoleAction(idRole, IMPUGNAR_CONTESTAR);
            deleteRoleAction(idRole, IMPUGNAR_CONTESTAR_TODO);
            deleteRoleAction(idRole, IMPUGNAR_REALIZAR);
            deleteRoleAction(idRole, IMPUGNAR_REALIZAR_TODO);
            deleteRoleAction(idRole, IMPUGNAR_VER_TODO);
            if (!existeRoleAction(idRole, IMPUGNAR_VER))
                query = "(" + idRole + "," + IMPUGNAR_VER + ")";
            break;
        case 2:
            deleteRoleAction(idRole, IMPUGNAR_CONTESTAR);
            deleteRoleAction(idRole, IMPUGNAR_CONTESTAR_TODO);
            deleteRoleAction(idRole, IMPUGNAR_REALIZAR);
            deleteRoleAction(idRole, IMPUGNAR_REALIZAR_TODO);
            deleteRoleAction(idRole, IMPUGNAR_VER);
            if (!existeRoleAction(idRole, IMPUGNAR_VER_TODO))
                query = "(" + idRole + "," + IMPUGNAR_VER_TODO + ")";
            break;
        case 3:
            deleteRoleAction(idRole, IMPUGNAR_CONTESTAR);
            deleteRoleAction(idRole, IMPUGNAR_CONTESTAR_TODO);
            deleteRoleAction(idRole, IMPUGNAR_REALIZAR_TODO);
            deleteRoleAction(idRole, IMPUGNAR_VER);
            deleteRoleAction(idRole, IMPUGNAR_VER_TODO);
            if (!existeRoleAction(idRole, IMPUGNAR_REALIZAR))
                query = "(" + idRole + "," + IMPUGNAR_REALIZAR + ")";
            break;
        case 4:
            deleteRoleAction(idRole, IMPUGNAR_CONTESTAR);
            deleteRoleAction(idRole, IMPUGNAR_CONTESTAR_TODO);
            deleteRoleAction(idRole, IMPUGNAR_REALIZAR);
            deleteRoleAction(idRole, IMPUGNAR_VER);
            deleteRoleAction(idRole, IMPUGNAR_VER_TODO);
            if (!existeRoleAction(idRole, IMPUGNAR_REALIZAR_TODO))
                query = "(" + idRole + "," + IMPUGNAR_REALIZAR_TODO + ")";
            break;
        case 5:
            deleteRoleAction(idRole, IMPUGNAR_CONTESTAR_TODO);
            deleteRoleAction(idRole, IMPUGNAR_REALIZAR_TODO);
            deleteRoleAction(idRole, IMPUGNAR_VER);
            deleteRoleAction(idRole, IMPUGNAR_VER_TODO);
            if (!existeRoleAction(idRole, IMPUGNAR_CONTESTAR))
                query = "(" + idRole + "," + IMPUGNAR_CONTESTAR + ")";
            break;
        case 6:
            deleteRoleAction(idRole, IMPUGNAR_CONTESTAR);
            deleteRoleAction(idRole, IMPUGNAR_REALIZAR);
            deleteRoleAction(idRole, IMPUGNAR_VER);
            deleteRoleAction(idRole, IMPUGNAR_VER_TODO);
            if (!existeRoleAction(idRole, IMPUGNAR_CONTESTAR_TODO))
                query = "(" + idRole + "," + IMPUGNAR_CONTESTAR_TODO + ")";
            break;
        }
        return query;
    }

    public static int deleteRoleAction(BigDecimal idRole, int action) {
        JPA.em().createNativeQuery("DELETE FROM PORTALUNICO.PUL_ROLE_ACTION WHERE FN_ID_ROLE = " + idRole
                + " AND FN_ACTION_ID = " + action).executeUpdate();
        return 0;
    }

    public static void asignarPermisoIndividual(Integer admin, int home, int evaluaciones, int impugnaciones,
            int seguimiento, int replica, int reporte, int notificaciones, int historico, int employeeNumber) {
        long module = 3;        
        
        Long idRol = Rol.find("select id from Rol where idModule = ?1 and nombreRol like ?2", module, "Administrador")
                .first();

        Query queryUser = JPA.em().createNativeQuery(
                "Select fn_userid from portalunico.pul_structure s " + "where s.fn_empid = " + employeeNumber);
        List<BigDecimal> usuario = queryUser.getResultList();

        if (admin == 1) {
            Object existe = UserRole
                    .find("select idUser from UserRole where idUser = ?1 and idRole = ?2", usuario.get(0), idRol)
                    .first();
            if (existe != null) {
            } else {
                UserRole userRole = new UserRole();
                userRole.idRole = idRol;
                userRole.idUser = usuario.get(0);
                userRole.save();
            }
        } else {
            if (admin == 2) {
                UserRole.delete("idRole = ?1 and idUser = ?2", idRol, usuario.get(0));
            }
        }

        Query query = JPA.em().createNativeQuery(
                "Select fn_userid from portalunico.pul_structure s " + "where s.fn_empid = " + employeeNumber);
        List<BigDecimal> usuarios = query.getResultList();
        List assignType = JPA.em()
                .createNativeQuery(
                        "Select * from portalunico.pul_nps_type_assign_role where fn_id_user = " + usuarios.get(0))
                .getResultList();

        if (assignType.size() < 1) {
            JPA.em().createNativeQuery(
                    "INSERT INTO PORTALUNICO.pul_nps_type_assign_role (FN_ID_USER, FN_TYPE_ROLE) VALUES("
                            + usuarios.get(0) + ", 'INDIVIDUAL')")
                    .executeUpdate();
        } else {
            JPA.em().createNativeQuery(
                    "UPDATE PORTALUNICO.pul_nps_type_assign_role SET FN_TYPE_ROLE = 'INDIVIDUAL' WHERE FN_ID_USER = "
                            + usuarios.get(0))
                    .executeUpdate();
        }
        String nombreRol = "NPS-Custome-role-" + employeeNumber;

        Query revisarRole = JPA.em()
                .createNativeQuery("Select r.fn_id_role from portalunico.pul_role r "
                		+ "INNER JOIN PORTALUNICO.PUL_USER_ROLE UR ON R.FN_ID_ROLE = UR.FN_ID_ROLE "
                        + "INNER JOIN PORTALUNICO.PUL_STRUCTURE S ON UR.FN_ID_USER = S.FN_USERID "
                        + "WHERE S.FN_EMPID = " + employeeNumber
                        + " and r.fn_id_module = 3");
        BigDecimal idRole = BigDecimal.ZERO;
        idRole = (BigDecimal) JPA.em().createNativeQuery("SELECT MAX(FN_ID_ROLE)+1 FROM PORTALUNICO.PUL_ROLE")
                    .getResultList().get(0);

        if (revisarRole.getResultList().size() < 1) {
            JPA.em().createNativeQuery("INSERT INTO PORTALUNICO.PUL_ROLE (FN_ID_ROLE, FC_NAME_ROLE, FN_ID_MODULE)"
                    + "VALUES(" + idRole + ", " + "'" + nombreRol + "', 3)").executeUpdate();
        } else {
            int cont = 0;
        	for(Object id:revisarRole.getResultList()){
        		Query name = JPA.em().createNativeQuery("Select fc_name_role from portalunico.pul_role where fn_id_role = " + (BigDecimal)id);
        		
        		String rolName = (String)name.getSingleResult(); 
        		if(!rolName.equals(nombreRol)){
        			BigDecimal rol = (BigDecimal)id;
        			if(!rolName.equals("Administrador")){
        				UserRole.delete("idRole = ?1 and idUser = ?2", rol.longValue(), usuario.get(0));
        			}
        		}else{
        			idRole = (BigDecimal)id;
                    cont++;
        		}
        	}
        	
        	if(cont==0){
                JPA.em().createNativeQuery("INSERT INTO PORTALUNICO.PUL_ROLE (FN_ID_ROLE, FC_NAME_ROLE, FN_ID_MODULE)"
                + "VALUES(" + idRole + ", " + "'" + nombreRol + "', 3)").executeUpdate();
            }
        }

        String queryPrincipio = "INSERT INTO PORTALUNICO.PUL_ROLE_ACTION(FN_ID_ROLE, FN_ACTION_ID) VALUES";
        String queryHome = getQueryHome(home, idRole);
        String queryEval = getQueryEval(evaluaciones, idRole);
        String queryRepl = getQueryRepl(replica, idRole);
        String queryRepo = getQueryRepo(reporte, idRole);
        String queryNoti = getQueryNoti(notificaciones, idRole);
        String querySeg = getQuerySeg(seguimiento, idRole);
        String queryHis = getQueryHis(historico, idRole);

        if (impugnaciones == 5) {
            String queryImp1 = getQueryImp(5, idRole);
            String queryImp2 = getQueryImp(3, idRole);
            if (!queryImp1.isEmpty())
                JPA.em().createNativeQuery(queryPrincipio + queryImp1).executeUpdate();
            if (!queryImp2.isEmpty())
                JPA.em().createNativeQuery(queryPrincipio + queryImp2).executeUpdate();
        } else if (impugnaciones == 6) {
            String queryImp1 = getQueryImp(6, idRole);
            String queryImp2 = getQueryImp(4, idRole);
            if (!queryImp1.isEmpty())
                JPA.em().createNativeQuery(queryPrincipio + queryImp1).executeUpdate();
            if (!queryImp2.isEmpty())
                JPA.em().createNativeQuery(queryPrincipio + queryImp2).executeUpdate();
        } else {
            String queryImp1 = getQueryImp(impugnaciones, idRole);
            if (!queryImp1.isEmpty())
                JPA.em().createNativeQuery(queryPrincipio + queryImp1).executeUpdate();
        }

        if (!queryHome.isEmpty())
            JPA.em().createNativeQuery(queryPrincipio + queryHome).executeUpdate();
        if (!queryEval.isEmpty())
            JPA.em().createNativeQuery(queryPrincipio + queryEval).executeUpdate();
        if (!queryRepl.isEmpty())
            JPA.em().createNativeQuery(queryPrincipio + queryRepl).executeUpdate();
        if (!queryRepo.isEmpty())
            JPA.em().createNativeQuery(queryPrincipio + queryRepo).executeUpdate();
        if (!queryNoti.isEmpty())
            JPA.em().createNativeQuery(queryPrincipio + queryNoti).executeUpdate();
        if (!querySeg.isEmpty())
            JPA.em().createNativeQuery(queryPrincipio + querySeg).executeUpdate();
        if (!queryHis.isEmpty())
            JPA.em().createNativeQuery(queryPrincipio + queryHis).executeUpdate();

        Query userRole = JPA.em()
                .createNativeQuery("Select fn_id_role from portalunico.pul_user_role where fn_id_user = "
                        + usuarios.get(0) + " and fn_id_role = " + idRole);
        if (userRole.getResultList().size() < 1) {
            JPA.em().createNativeQuery("INSERT INTO PORTALUNICO.PUL_USER_ROLE (FN_ID_USER, FN_ID_ROLE)" + "VALUES("
                    + usuarios.get(0) + ", " + idRole + ")").executeUpdate();
        }
    }

    public static void getIsAdmin(Integer employeeNumber) {
        long module = 3;
        String response = "";
        Long idRol = Rol.find("select id from Rol where idModule = ?1 and nombreRol like ?2", module, "Administrador")
                .first();
        Query queryUser = JPA.em().createNativeQuery(
                "Select fn_userid from portalunico.pul_structure s " + "where s.fn_empid = " + employeeNumber);
        List<BigDecimal> usuario = queryUser.getResultList();

        Object existe = UserRole
                .find("select idUser from UserRole where idUser = ?1 and idRole = ?2", usuario.get(0), idRol).first();
        if (existe != null) {
            response = "ok";
        } else {
            response = "error";
        }
        renderText(response);
    }
}
