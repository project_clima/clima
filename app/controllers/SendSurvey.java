package controllers;

import java.sql.SQLException;
import java.text.SimpleDateFormat;
import play.*;
import play.mvc.*;
import java.util.*;
import models.*;
import json.entities.AutoComplete;
import json.entities.MailingList;
import json.entities.Select2;
import play.libs.F;
import play.templates.Template;
import play.templates.TemplateLoader;
import querys.EmailQuery;
import querys.SendEmailQuery;
import utils.GmailMailer;

/**
 * Clase controlador para el manejo del envio de encuestas
 * @author Rodolfo Miranda -- Qualtop
 */
@With(Secure.class)
public class SendSurvey extends Controller {
    /**
     * Metodo para obtener las encuestas los tipos de encuestas de clima
     */
    public static void index() {
        List<SurveyType> surveyTypes = SurveyType.find("id not in (4, 5)").fetch();
        List<StoreEmployee> depots   = getDepots();
        render(surveyTypes, depots);
    }
    
    /**
     * MEtodo para obtener las diferentes encuestas
     * @param surveyTypeId 
     */
    public static void getSurveys(int surveyTypeId) {
        List<Object> surveys = Survey.find(
            "select id, name, surveyType.id from Survey where surveyType.id = ?1 and sysdate between dateInitial and dateEnd " + 
            "order by dateInitial desc", surveyTypeId
        ).fetch();
        
        renderJSON(surveys);
    }
    
    /**
     * Metodo para obtener los empleados por encuetas y palabra de busqued
     * @param surveyId
     * @param search 
     */
    public static void getSurveyEmployees(int surveyId, String search) {
        List<SurveyEmployee> employees = SurveyEmployee.find(
            "survey.id = ?1 and (lower(employeeClima.firstName) like lower(?2) or lower(employeeClima.lastName) like lower(?3))",
            surveyId, "%" + search +  "%", "%" + search +  "%"
        ).fetch(50);
        
        Select2 selct2 = new Select2();
        
        for (SurveyEmployee employee : employees) {
            String employeeName = employee.employeeClima.firstName + " " + employee.employeeClima.lastName;
            selct2.items.add(new AutoComplete(employee.employeeClima.userId, employeeName));
        }
        
        selct2.total_count = selct2.items.size();
        renderJSON(selct2);
    }
    
    /**
     * Metodo para obtener los diferentes departamentos por zonas
     * @return 
     */
    public static List<StoreEmployee> getDepots() {
        List<StoreEmployee> depots = StoreEmployee.find(
            "select distinct subDivision from StoreEmployee where lower(descriptionSubdivision) like '%zona%'"
        ).fetch();
        
        return depots;
    }
    
    /**
     * Metodo para obtener las tiendas por zona, tipo y palabra de busqueda
     * @param type
     * @param zone
     * @param search 
     */
    public static void getStores(String type, String zone, String search) {
        List<StoreEmployee> stores = StoreEmployee.find(
            getConditions(type, zone, search)
        ).fetch();
        
        renderJSON(stores);
    }
    
    /**
     * MEtodo de utileria para armar querys dinamicos
     * @param type
     * @param zone
     * @param search
     * @return 
     */
    public static String getConditions(String type, String zone, String search) {
        String conditions = "1 = 1";
        
        if (!type.equals("all")) {
            conditions += " and lower(descriptionSubdivision) like lower('%" + type + "%')";
        }
        
        if (!zone.equals("all")) {
            conditions += " and subDivision = '" + zone + "'";
        }
        
        return conditions;
    }
    
    /**
     * Metodo que ejecuta el envio de correos
     * @param surveyType
     * @param surveyId
     * @param storeTypes
     * @param zones
     * @param stores
     * @param employees
     * @param sendToDirectors 
     */
    public static void sendEmail(int surveyType, int surveyId, String storeTypes,
                                String zones, List<String> stores, String employees,
                                boolean sendToDirectors) {
        
        SchedulerTask.SendEmail sendEmailJob = new SchedulerTask.SendEmail(
            surveyType, surveyId, storeTypes, zones, stores, employees, sendToDirectors, request
        );
        
        F.Promise<String> sendJob = sendEmailJob.now();
        
        String result = await(sendJob);
        
        renderJSON(new utils.Message(result, "", false));
    }
}
