package controllers;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.text.ParseException;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import controllers.Secure;
import json.csi.dto.CsiSurveyDto;
import models.Survey;
import models.Usuario;
import models.csi.CsiSurvey;
import models.csi.helpers.CsiSurveyHelper;
import models.csi.helpers.DateHelper;
import play.Logger;
import play.Play;
import play.mvc.Controller;
import play.mvc.Http.Response;
import play.mvc.Scope.Session;
import play.mvc.With;

@With(Secure.class)
/**
 * Clase que sirve para filtrar en el menú de la izquierda, es un componente Comun 
 *  
 * @author Support
 *
 */
public class Filters extends Controller {
	
	/**
	 * Método que obtiene la fecha seleccionada en los filtros
	 * @param session
	 * @return String
	 */
	public static String getSessionFilterDateType(Session session) {
		return session.get(session.getId() + "-filter-date-type");
	}
	/**
	 * Método para obtener la encuesta seleccionada en el filtro
	 * @param session
	 * @return Integer
	 */
	public  static Integer getSessionFilterEncuesta(Session session) {
		String temp=  session.get(session.getId() + "-filter-encuesta");
		if (temp != null && !temp.trim().isEmpty()){
			return  new Integer(temp);	
		}
		return null;
	}
	
	/**
	 * Método para obtener el tipo de encuesta seleccionada en el filtro
	 * @param session
	 * @return String
	 */
	public static String getSessionFilterCSIType(Session session){
		return session.get(session.getId() + "-filter-csi-type");
	}
	
	/**
	 * Método que aplica el filtro de tipo de encuesta
	 * @param id
	 */
	public static void applyCSITypeFilter(String id){
		cleanCorporationFilter();
		cleanManagementFilter();
		cleanWorldFilter();
		cleanSectionFilter();
		cleanEvaluatedFilter();
		if(!id.isEmpty() && id != null){
			session.put(session.getId() + "-filter-csi-type", id);
		}
	}
	
	/**
	 * Limpia el filtro de tipo de encuesta
	 * @return false
	 */
	public static boolean cleanCSITypeFilter(){
		session.remove(session.getId() + "-filter-csi-type");
		return false;
	}
	
	/**
	 * Limpia el filtro de Corporación
	 * @return false
	 */
	public static boolean cleanCorporationFilter(){
		session.remove(session.getId() + "-filter-corporation");
		return false;
	}
	
	/**
	 * Obtiene la corporación seleccionada en el filtro
	 * @param session
	 * @return String
	 */
	public static String getSessionFilterCorporation(Session session){
		return session.get(session.getId() + "-filter-corporation");
	}
	
	/**
	 * Método que aplica el filtro de corporación
	 * @param id
	 */
	public static void applyCorporationFilter(String id){
		//cleanCSITypeFilter();
		cleanManagementFilter();
		cleanWorldFilter();
		cleanSectionFilter();
		cleanEvaluatedFilter();
		if(!id.isEmpty() && id != null){
			session.put(session.getId() + "-filter-corporation", id);
		}
	}
	
	/**
	 * Limpia el filtro de Manager
	 * @return false
	 */
	public static boolean cleanManagementFilter(){
		session.remove(session.getId() + "-filter-management");
		return false;
	}
	
	/**
	 * Obtiene el Manager seleccionado en el filtro
	 * @param session
	 * @return String
	 */
	public static String getSessionFilterManagement(Session session){
		return session.get(session.getId() + "-filter-management");
	}
	
	/**
	 * Aplicar el filtro de manager
	 * @param id
	 */
	public static void applyManagementFilter(String id){
		//cleanCSITypeFilter();
		cleanCorporationFilter();
		cleanWorldFilter();
		cleanSectionFilter();
		cleanEvaluatedFilter();
		if(!id.isEmpty() && id != null){
			session.put(session.getId() + "-filter-management", id);
		}
	}
	
	/**
	 * Limpia el filtro de Mundo
	 * @return false
	 */
	public static boolean cleanWorldFilter(){
		session.remove(session.getId() + "-filter-world");
		return false;
	}
	
	/**
	 * Obtiene el Mundo seleccionado en el filtro
	 * @param session
	 * @return String
	 */
	public static String getSessionFilterWorld(Session session){
		return session.get(session.getId() + "-filter-world");
	}
	
	/**
	 * Aplica el filtro de Mundo
	 * @param id
	 */
	public static void applyWorldFilter(String id){
		//cleanCSITypeFilter();
		cleanManagementFilter();
		cleanCorporationFilter();
		cleanSectionFilter();
		cleanEvaluatedFilter();
		if(!id.isEmpty() && id != null){
			session.put(session.getId() + "-filter-world", id);
		}
	}
	
	/**
	 * Limpia el filtro de sección
	 * @return false
	 */
	public static boolean cleanSectionFilter(){
		session.remove(session.getId() + "-filter-section");
		return false;
	}
	
	/**
	 * Obtiene la sección seleccionada en el filtro
	 * @param session
	 * @return String
	 */
	public static String getSessionFilterSection(Session session){
		return session.get(session.getId() + "-filter-section");
	}
	
	/**
	 * Aplica el filtro de sección
	 * @param id
	 */
	public static void applySectionFilter(String id){
		//cleanCSITypeFilter();
		cleanManagementFilter();
		cleanWorldFilter();
		cleanCorporationFilter();
		cleanEvaluatedFilter();
		if(!id.isEmpty() && id != null){
			session.put(session.getId() + "-filter-section", id);
		}
	}
	
	/**
	 * Limpia el filtro de Evaluado
	 * @return false
	 */
	public static boolean cleanEvaluatedFilter(){
		session.remove(session.getId() + "-filter-evaluated");
		return false;
	}
	
	/**
	 * Obtiene el evaluado seleccionado en el filtro
	 * @param session
	 * @return String
	 */
	public static String getSessionFilterEvaluated(Session session){
		return session.get(session.getId() + "-filter-evaluated");
	}
	
	/**
	 * Aplica el filtro de evaluado
	 * @param id
	 */
	public static void applyEvaluatedFilter(String id){
		//cleanCSITypeFilter();
		cleanManagementFilter();
		cleanWorldFilter();
		cleanSectionFilter();
		cleanCorporationFilter();
		if(!id.isEmpty() && id != null){
			session.put(session.getId() + "-filter-evaluated", id);
		}
	}
	
	/**
	 * Aplica el filtro de encuesta
	 * @param id
	 */
	public static void applySurveyFilter(String id){
			
		Logger.debug("applySurveyFilter(id="+id+")");
		if (id != null && !id.isEmpty()){
			cleanDatePeriodFilter();
			cleanDateQuarterFilter();
			cleanDateMonthFilter();
			session.put(session.getId() + "-filter-encuesta", id);
		}
		else {
			cleanySurveyFilter();
		}
		renderText("SUCCESS");
	}
	
	/**
	 * Obtiene la fecha seleccionada en el filtro
	 * @param filterDateType
	 * @param session
	 * @return Date[]
	 */
	public static Date[] getDateFilters(String filterDateType, Session session) {
		Date dateFrom = null;
		Date dateTo = null;
		if (filterDateType != null && !filterDateType.isEmpty()){
			if (filterDateType.equals("Trimestral")){
				// year=2017
				String year = session.get(session.getId() + "-filter-date-quarter-year");
				//querter=Q1
				String quarter = session.get(session.getId() + "-filter-date-quarter-quarter");
				Logger.debug("Trimestral:: year=" + year +", quarter=" + quarter+"-");
				try {
					if (quarter.equals("Q1")){
						dateFrom = DateHelper.moverAPrimeraHora(year+"/01/01","yyyy/MM/dd");
//						dateTo   = DateHelper.ultimoDiaDelMes(  year+"/03/01","yyyy/MM/dd");
						dateTo   = DateHelper.restarUnMilliSegundo(year+"/04/01","yyyy/MM/dd");
					}
					else if (quarter.equals("Q2")){
						dateFrom = DateHelper.moverAPrimeraHora(   year+"/04/01","yyyy/MM/dd");
						dateTo   = DateHelper.restarUnMilliSegundo(year+"/07/01","yyyy/MM/dd");
					}
					else if (quarter.equals("Q3")){
						dateFrom = DateHelper.moverAPrimeraHora(   year+"/07/01","yyyy/MM/dd");
						dateTo   = DateHelper.restarUnMilliSegundo(year+"/10/01","yyyy/MM/dd");
					}
					else if (quarter.equals("Q4")){
						dateFrom = DateHelper.moverAPrimeraHora(   year+"/10/01","yyyy/MM/dd");
						dateTo   = DateHelper.restarUnMilliSegundo(year+"/12/01","yyyy/MM/dd");
					}
					
				} catch (ParseException e) {
					Logger.debug("exception e="+ e);
				}
				Logger.debug("after dateFrom="+ dateFrom + ",dateTo="+dateTo+",");
				Logger.debug("dentro de filterDateType == "+filterDateType);
			}
			else if (filterDateType.equals("Mensual")){
				//month=02/2017
				String month = session.get(session.getId() + "-filter-date-monthly-month");
				Logger.debug("Mensual:: month=" + month+"-" );
				try {
					dateFrom = DateHelper.moverAPrimeraHora("01/"+month,"dd/MM/yyyy");
					dateTo   = DateHelper.ultimoDiaDelMes(  "01/"+month,"dd/MM/yyyy");
				} catch (ParseException e) {
					Logger.debug("exception e="+ e);
					
				}
				
				Logger.debug("after dateFrom="+ dateFrom + ",dateTo="+dateTo+",");
				Logger.debug("dentro de filterDateType == "+filterDateType);
			}
			else if (filterDateType.equals("Periodo")){
				
				String dateFromString = session.get(session.getId() + "-filter-date-period-from");
				String dateToString =  session.get(session.getId() + "-filter-date-period-to");

				Logger.debug("Periodo:: dateFromString=" + dateFromString +", dateToString=" + dateToString+"-");
				try {
					dateFrom = DateHelper.moverAPrimeraHora(dateFromString,"dd/MM/yyyy");
					dateTo   = DateHelper.moverAUltimaHora(   dateToString,"dd/MM/yyyy");
				} catch (ParseException e) {
					Logger.debug("exception e="+ e);
				}
				
				Logger.debug("after dateFrom="+ dateFrom + ",dateTo="+dateTo+",");
				Logger.debug("dentro de filterDateType == "+filterDateType);
			}
		}
		return new Date[]{dateFrom, dateTo};
	}

	/**
	 * Aplica el filtro de fecha. Busca por año
	 * @param year
	 * @param quarter
	 */
	public static void applyDateQuarterFilter(String year, String quarter){
		if (year != null && !year.isEmpty() && 
				quarter != null && !quarter.isEmpty()){
			session.remove(session.getId() + "-filter-encuesta");
			session.put(session.getId() + "-filter-date-quarter-year", year);
			session.put(session.getId() + "-filter-date-quarter-quarter", quarter);
			session.put(session.getId() + "-filter-date-type","Trimestral");
			Logger.info("FilterDate ... " + session.get(session.getId() + "-filter-date-type"));
		}
		else {
			cleanDateFilter();
		}
		renderText("SUCCESS");
	}
	/**
	 * Limpia el filtro de fecha (por año)
	 * @return true
	 */
	public static boolean cleanDateQuarterFilter(){
		
		session.remove(session.getId() + "-filter-date-quarter-year");
		session.remove(session.getId() + "-filter-date-quarter-year");
		session.remove(session.getId() + "-filter-date-type");
		return true;
	}
	
	/**
	 * Aplica el filtro de fecha. Busca por mes
	 * @param month
	 */
	public static void applyDateMonthFilter(String month){
		if (month != null && !month.isEmpty()){
			session.remove(session.getId() + "-filter-encuesta");
			session.put(session.getId() + "-filter-date-monthly-month", month);
			session.put(session.getId() + "-filter-date-type","Mensual");
		}
		else {
			cleanDateFilter();
		}
		renderText("SUCCESS");
	}
	
	/**
	 * Limpia el filtro de fecha (por mes)
	 * @return true
	 */
	public static boolean cleanDateMonthFilter(){
			
		session.remove(session.getId() + "-filter-date-monthly-month");
		session.remove(session.getId() + "-filter-date-type");
		return true;
	}

	/**
	 * Limpia el filtro de encuesta
	 */
	public static void cleanySurveyFilter(){
		session.remove(session.getId() + "-filter-encuesta");
		renderText("SUCCESS");
	}

	/**
	 * Aplica el filtro por periodo de fecha
	 * @param dateFrom
	 * @param dateTo
	 */
	public static void applyDatePeriodFilter(String dateFrom,String dateTo){
		if (dateFrom != null && !dateFrom.isEmpty() && 
				dateTo != null && !dateTo.isEmpty()){
			session.remove(session.getId() + "-filter-encuesta");
			session.put(session.getId() + "-filter-date-period-from", dateFrom);
			session.put(session.getId() + "-filter-date-period-to", dateTo);
			session.put(session.getId() + "-filter-date-type","Periodo");
		}
		else {
			cleanDateFilter();
		}
		renderText("SUCCESS");
	}
	/**
	 * Limpia el filtro de periodo de fecha
	 * @return
	 */
	public static boolean cleanDatePeriodFilter(){

		session.remove(session.getId() + "-filter-date-period-from");
		session.remove(session.getId() + "-filter-date-period-to");
		session.remove(session.getId() + "-filter-date-type");
		return true;
		
	}
	
	/**
	 * Limpia los filtros de fechas
	 */
	public static void cleanDateFilter(){
		cleanDatePeriodFilter();
		cleanDateQuarterFilter();
		cleanDateMonthFilter();
		renderText("SUCCESS");
	}
   
}
