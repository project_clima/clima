package controllers;

import com.google.common.collect.ObjectArrays;
import java.io.IOException;
import java.sql.SQLException;
import play.mvc.*;
import java.util.*;
import json.entities.ResultMatrixList;
import json.entities.ResumeTable;
import net.sf.jxls.exception.ParsePropertyException;
import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import querys.StructureQuery;
import querys.MatrixQuery;
import utils.RenderExcel;

@With(Secure.class)
public class ExportResultMatrix extends Controller {
    
    public static void export(int level, String structureDate)
    throws SQLException, ParsePropertyException, InvalidFormatException, IOException {       
        ResumeTable resume  = MatrixQuery.getResume(level, structureDate);
        String[] userIds    = getDeparments(StructureQuery.getDepartmentsByLevel(level, structureDate, true), false);        
        List departmentsRaw = StructureQuery.getDepartmentsByLevelAllowed(level, structureDate);
        
        String[] departmentEmployee         = StructureQuery.getDepartmentName(level, structureDate); 
        HashMap<String, String> departments = getDepartmensWithUserName(departmentsRaw, departmentEmployee, level);
        
        userIds     = ObjectArrays.concat(new String[]{Integer.toString(level)}, userIds, String.class);
        
        HashMap<String, List<ResultMatrixList>> factoresMap   = setFactores(MatrixQuery.getFactores(userIds, structureDate));
        HashMap<String, List<ResultMatrixList>> subfactorsMap = setSubfactores(setFactores(MatrixQuery.getSubFactor(userIds, structureDate)));
        HashMap<String, List<ResultMatrixList>> questions     = getQuestions(userIds, structureDate);
        
        renderArgs.put("departmentEmployee", departmentEmployee);
        renderArgs.put("resume", resume);
        renderArgs.put("departments", departments);
        renderArgs.put("factors", parseFactorsToExport(factoresMap));
        renderArgs.put("subfactors", parseSubFactorsToExport(subfactorsMap));
        renderArgs.put("questions", parseQuestionsToExport(questions));
        renderArgs.put(RenderExcel.RA_FILENAME, "MatrizDeResultados.xls");
        
        RenderExcel renderer = new RenderExcel("app/excel/views/ExportResultMatrix/export.xls");
        
        Integer hide = (resume.getSurveys() < 3 || resume.getAnswers() < 3) ? 0 : null;
        
        renderer.renderWithHide(hide);
    }
    
    private static HashMap<String, List<ResultMatrixList>> getQuestions(String[] departments, String structureDate)
    throws SQLException {
        List questionsList = MatrixQuery.getQuestion(departments, structureDate);        
        HashMap<String, List<ResultMatrixList>> resultMatrix = setFactores(questionsList);
        HashMap<String, List<ResultMatrixList>> subfactores  = setSubfactores(resultMatrix);
        HashMap<String, List<ResultMatrixList>> questions    = setQuestions(subfactores);
        
        return questions;
    }
    
    private static TreeMap<String, TreeMap<String, Float>> parseFactorsToExport(HashMap<String, List<ResultMatrixList>> entity) {
        TreeMap<String, TreeMap<String, Float>> result = new TreeMap();
        TreeMap<String, Float> departmentsScore;
        
        for (Map.Entry<String, List<ResultMatrixList>> entry : entity.entrySet()) {
            List<ResultMatrixList> resultMatrix = entry.getValue();
            
            departmentsScore = result.get(entry.getKey());
            departmentsScore = departmentsScore != null ? departmentsScore : new TreeMap();
            
            for (ResultMatrixList deparmentScore : resultMatrix) {
                departmentsScore.put(deparmentScore.getDepartment(), deparmentScore.getScore());
            }
            
            result.put(entry.getKey(), departmentsScore);
        }
        
        return result;
    }
    
    private static TreeMap<String,  TreeMap<String, TreeMap<String,Float>>> 
        parseSubFactorsToExport(HashMap<String, List<ResultMatrixList>> subFactors) {
            
        TreeMap<String, TreeMap<String, TreeMap<String,Float>>> result = new TreeMap();
        
        TreeMap<String, TreeMap<String, Float>> subfactorMatrix;
        TreeMap<String, Float> departmentsScore;
        
        for (Map.Entry<String, List<ResultMatrixList>> subFactor : subFactors.entrySet()) {
            List<ResultMatrixList> subfactorResults = subFactor.getValue();
            
            String subfactorName = subFactor.getKey();
            String factorName    = subfactorResults.get(0).getFactor();
            
            if (factorName == null) {
                continue;
            }
            
            subfactorMatrix = result.get(factorName);
            subfactorMatrix = subfactorMatrix != null ? subfactorMatrix : new TreeMap();
            
            departmentsScore = subfactorMatrix.get(subfactorName);
            departmentsScore = departmentsScore != null ? departmentsScore : new TreeMap();
            
            for (ResultMatrixList subfactorResult : subfactorResults) {
                departmentsScore.put(subfactorResult.getDepartment(), subfactorResult.getScore());
            }
            
            subfactorMatrix.put(subfactorName, departmentsScore);
            result.put(factorName, subfactorMatrix);
        }
        
        return result;
    }
    
        
    private static TreeMap<String,  TreeMap<String, TreeMap<String,Float>>> 
        parseQuestionsToExport(HashMap<String, List<ResultMatrixList>> questions) {
            
        TreeMap<String, TreeMap<String, TreeMap<String,Float>>> result = new TreeMap();
        
        TreeMap<String, TreeMap<String, Float>> questionsMatrix;
        TreeMap<String, Float> departmentsScore;
        
        for (Map.Entry<String, List<ResultMatrixList>> question : questions.entrySet()) {
            List<ResultMatrixList> questionResults = question.getValue();
            
            String questionName  = question.getKey();
            String subfactorName = questionResults.get(0).getSubFactor();
            
            questionsMatrix = result.get(subfactorName);
            questionsMatrix = questionsMatrix != null ? questionsMatrix : new TreeMap();
            
            departmentsScore = questionsMatrix.get(questionName);
            departmentsScore = departmentsScore != null ? departmentsScore : new TreeMap();
            
            for (ResultMatrixList questionResult : questionResults) {
                departmentsScore.put(questionResult.getDepartment(), questionResult.getScore());
            }
            
            questionsMatrix.put(questionName, departmentsScore);
            result.put(subfactorName, questionsMatrix);
        }
        
        return result;
    }
        
    private static String[] getDeparments(List departments, boolean asText) {
        String[] departmentsList = new String[departments.size()];
        
        int index = 0;
        for (Object department : departments) {
            Object[] row = (Object[]) department;
            departmentsList[index++] = asText 
                ? row[0].toString() + "-" + row[1].toString() : row[1].toString();
        }
        
        return departmentsList;
    }
    
    private static LinkedHashMap<String, String> getDepartmensWithUserName(
        List departmentsRaw, String[] departmentEmployee, int level
    ) {
        LinkedHashMap<String, String> departmentsMap = new LinkedHashMap();
        
        departmentsMap.put(departmentEmployee[0] + "-" + level, departmentEmployee[0] + "-" + departmentEmployee[2]);
        
        for (Object departmentRaw : departmentsRaw) {
            Object[] row = (Object[]) departmentRaw;
            
            if (Integer.valueOf(row[4].toString()) > 2 && Integer.valueOf(row[5].toString()) > 2) {            
                departmentsMap.put(
                    row[0].toString() + "-" + row[1].toString(),
                    row[0].toString() + "-" + row[3].toString()
                );
            }
        }
        
        return departmentsMap;
    }
    
    private static HashMap<String, List<ResultMatrixList>> setFactores(List<ResultMatrixList> factors) {
        HashMap<String, List<ResultMatrixList>> resultMatrix = new HashMap();

        for (ResultMatrixList factor : factors) {
            factors = resultMatrix.get(factor.getFactor());
            factors = factors == null ? new ArrayList() : factors;
            factors.add(factor);

            resultMatrix.put(factor.getFactor(), factors);
        }
        
        return resultMatrix;
    }

    public static  HashMap<String, List<ResultMatrixList>> setSubfactores(HashMap<String, List<ResultMatrixList>> factores) {
        HashMap<String, List<ResultMatrixList>> hashResult = new HashMap();

        for (Map.Entry<String, List<ResultMatrixList>> entry : factores.entrySet()) {
            List<ResultMatrixList> results;

            for (ResultMatrixList resulMatrixList : entry.getValue()) {
                results = hashResult.get(resulMatrixList.getSubFactor());
                results = results == null ? new ArrayList() : results;
                results.add(resulMatrixList);

                hashResult.put(resulMatrixList.getSubFactor(), results);

            }
        }

        return hashResult;
    }

    public static HashMap<String, List<ResultMatrixList>> setQuestions(HashMap<String, List<ResultMatrixList>> subfactores) {
        HashMap<String, List<ResultMatrixList>> hashResult = new HashMap();

        for (Map.Entry<String, List<ResultMatrixList>> entry : subfactores.entrySet()) {
            List<ResultMatrixList> results;

            for (ResultMatrixList resultMatrix : entry.getValue()) {
                results = hashResult.get(resultMatrix.getQuestion());
                results = results == null ? new ArrayList() : results;
                results.add(resultMatrix);

                hashResult.put(resultMatrix.getQuestion(), results);
            }
        }

        return hashResult;
    }
}
