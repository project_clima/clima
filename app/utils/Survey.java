package utils;

import com.google.common.collect.Lists;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.OutputStream;
import java.text.Normalizer;
import play.Play;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import models.Question;
import models.QuestionChoices;

/**
 *
 * @author Jorge
 */
public class Survey {
    public static String getTemplate(String type) {
        HashMap<String, String> templates = templates();
        
        return templates.get(cleanTypeName(type));
    }
    
    public static HashMap<String, String> templates() {
        HashMap<String, String> templates = new HashMap<String, String>();
        
        templates.put("caras", "faces");
        templates.put("abiertas", "open");
        templates.put("opcionesmultiples", "multiple");
        templates.put("si/no", "yesno");
        
        return templates;
    }
    
    public static String cleanTypeName(String s) 
    {
        s = Normalizer.normalize(s, Normalizer.Form.NFD);
        s = s.replaceAll("[\\p{InCombiningDiacriticalMarks}]", "").replaceAll(" ", "").toLowerCase();
        return s;
    }
    
    public static List sortChoices(List choices) {
        Collections.sort(choices, new Comparator<QuestionChoices>() {
            @Override
            public int compare(QuestionChoices o1, QuestionChoices o2) {
                String choiceValue1 = o1.choiceValue != null ? o1.choiceValue  : "";
                String choiceValue2 = o2.choiceValue != null ? o2.choiceValue  : "";
                
                return choiceValue1.compareTo(choiceValue2);
            }
        });
        
        return Lists.reverse(choices);
    }
    
    public static Boolean inTime(models.Survey survey) {
        Date currentDate = new Date();
        return currentDate.after(survey.dateInitial) && currentDate.before(survey.dateEnd);
    }
    
    public static List getSurveyQuestions(int surveyId) {
        List<Question> surveyQuestions = Question.find("survey.id = ? ORDER BY order", surveyId).fetch();   
        
        LinkedHashMap<String, List<Question>> questionTypes = new LinkedHashMap<>();
        List paginatedQuestions = new ArrayList();
        List<Question> questions;
        
        String keyType;
        int questionQuantity = 0;
        
        for (Question question : surveyQuestions) {
            keyType = question.questionType.name;
            
            if (questionTypes.get(keyType) == null) {
                questions = new ArrayList<>();
            } else {
                questions = questionTypes.get(keyType);
            }
            
            questions.add(question);
            questionTypes.put(keyType, questions);
            
            if (++questionQuantity % 10 == 0 || questionQuantity == surveyQuestions.size()) {
                paginatedQuestions.add(questionTypes);
                questionTypes = new LinkedHashMap<>();
            }
        }
        
        return paginatedQuestions;
    }
    
    public static String isRequired(Boolean required) {
        return required != null && required == true ? "required" : "";
    }
}
