package utils;
import java.util.*;
/**
 *
 * @author hdt9974
 */
public class Pagination {
    public String sEcho;
    public long iTotalRecords;
    public long iTotalDisplayRecords;
    public List aaData;
    
    public Pagination (String sEcho, long iTotalRecords, long iTotalDisplayRecords, List aaData) {
        this.sEcho                = sEcho;
        this.iTotalRecords        = iTotalRecords;
        this.iTotalDisplayRecords = iTotalDisplayRecords;
        this.aaData               = aaData;
    }
}