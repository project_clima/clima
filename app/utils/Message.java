package utils;

public class Message {
    public String message;
    public String type;
    public boolean error;
    
    public Message(String message, String type, boolean isError) {
        this.message = message;
        this.type    = type;
        this.error = isError;
    }
    
    public Message(String message) {
        this.message = message;
    }
    
    public Message(String message, String type) {
        this.message = message;
        this.type    = type;
    }
}
