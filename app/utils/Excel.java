package utils;

import org.apache.poi.hssf.usermodel.HSSFCell;
import org.apache.poi.ss.usermodel.DateUtil;
import org.apache.poi.xssf.usermodel.XSSFCell;

public class Excel {
    public static boolean isNumeric(HSSFCell cell) {
        if (cell.getCellType() == HSSFCell.CELL_TYPE_NUMERIC) {
            if (!DateUtil.isCellDateFormatted(cell)) {
                return true;
            }
        }
        return false;
    }

    public static boolean isString(HSSFCell cell) {
        return cell.getCellType() == HSSFCell.CELL_TYPE_STRING;
    }
    
    public static boolean isNumeric(XSSFCell cell) {
        if (cell.getCellType() == XSSFCell.CELL_TYPE_NUMERIC) {
            if (!DateUtil.isCellDateFormatted(cell)) {
                return true;
            }
        }
        return false;
    }

    public static boolean isString(XSSFCell cell) {
        return cell.getCellType() == XSSFCell.CELL_TYPE_STRING;
    }
}
