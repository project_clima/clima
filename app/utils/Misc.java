package utils;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.OutputStream;
import java.math.BigInteger;
import java.nio.charset.StandardCharsets;
import java.security.NoSuchAlgorithmException;
import play.Play;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Map;

/**
 *
 * @author Jorge
 */
public class Misc {
    public static String saveFile(File file, String name) {
        String path = Play.applicationPath + File.separator + "files";
        OutputStream out = null;
        String originalName = file.getName();
        String extension = originalName.substring(originalName.lastIndexOf("."), originalName.length());
        String fileName = path + File.separator + name + extension;

        try {
            out = new FileOutputStream(new File(fileName));
            InputStream archivoOP = new FileInputStream(file);

            int read = 0;
            final byte[] bytes = new byte[1024];

            while ((read = archivoOP.read(bytes)) != -1) {
                out.write(bytes, 0, read);
            }

            out.close();
            
            return fileName;
        } catch (Exception e) {
            return null;
        }
    }

    public static String md5(String input) {
        try {
            java.security.MessageDigest md5 = java.security.MessageDigest.getInstance("MD5");
            md5.update(StandardCharsets.UTF_8.encode(input));
            return String.format("%032x", new BigInteger(1, md5.digest()));
        } catch ( NoSuchAlgorithmException e) {
            return "";            
        }
    }
    
    public static boolean checked(Integer key, Map<String, String[]> map, Integer choice) {
        if (key == null){
            return false;
        }
        
        return map.get(key) != null && map.get(key)[0].equals(choice.toString());
    }
}
