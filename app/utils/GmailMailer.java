package utils;

import java.io.File;
import java.util.Date;
import java.util.Properties;
import javax.activation.DataHandler;
import javax.activation.DataSource;
import javax.activation.FileDataSource;
import javax.mail.BodyPart;

import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.Multipart;
import javax.mail.NoSuchProviderException;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMultipart;
import play.Play;
import models.EmailSendLog;
import play.Logger;
import play.vfs.VirtualFile;

public class GmailMailer {
    private final String server   = Play.configuration.getProperty("lp.gmail.server");
    private final String port     = Play.configuration.getProperty("lp.gmail.port");
    private final String user     = Play.configuration.getProperty("lp.gmail.user");
    private final String password = Play.configuration.getProperty("lp.gmail.password");
    private final String from     = Play.configuration.getProperty("lp.gmail.fromAddress");
    private final boolean ssl;
    private final Properties properties;
    private final Session session;
    private final Transport transport;

    public GmailMailer()
    throws NoSuchProviderException, MessagingException {
        this.ssl        = Play.configuration.getProperty("lp.gmail.ssl").equals("true");        
        this.properties = getProperties();
        this.session    = Session.getInstance(this.properties, null);
        this.transport  = session.getTransport("smtp");
    }
    
    private Properties getProperties() {
        Properties props = new Properties();
        props.put("mail.debug", "false");
        props.put("mail.smtp.auth", true);
        props.put("mail.smtp.starttls.enable", this.ssl);
        props.put("mail.smtp.host", this.server);
        props.put("mail.smtp.port", this.port);
        
        return props;
    }
    
    public void send(String to, String subject, String body, int surveyId, String cc) {
        String error = "";

        try {
            MimeMessage message = new MimeMessage(this.session);
            message.setFrom(new InternetAddress(this.from));
            
            if (to != null && !to.equals("")) {
                message.addRecipient(Message.RecipientType.TO, new InternetAddress(to));
            }
            
            if (cc != null && !cc.equals("")) {
                Message.RecipientType type = (to != null && !to.equals("")) ?
                                                Message.RecipientType.CC : Message.RecipientType.TO;
                
                String[] recipientList = cc.split(",");
                
                for (String recipient : recipientList) {
                    message.addRecipient(type, new InternetAddress(recipient.trim()));
                }
            }
            
            message.setSubject(subject, "UTF-8");
            message.setSentDate(new Date());
            message.setContent(body, "text/html; charset=UTF-8");
            message.saveChanges();            
                     
            this.transport.sendMessage(message, message.getAllRecipients());
        } catch (MessagingException e) {
            error = e.getMessage();
            Logger.error(e.getMessage());
        }
        
        EmailSendLog.saveLog(to, surveyId, error, error.equals(""));
    }
    
    public void connect() throws MessagingException {
        this.transport.connect(this.server, this.user, this.password);       
    }
    
    public void close() throws MessagingException {
        if (this.transport.isConnected()) {
            this.transport.close();
        }
    }
    
    private void addAttachment(Multipart multipart, File attachment)
    throws MessagingException {        
        DataSource source = new FileDataSource(attachment);
        BodyPart messageBodyPart = new MimeBodyPart();        
        messageBodyPart.setDataHandler(new DataHandler(source));
        messageBodyPart.setFileName(attachment.getName());
        multipart.addBodyPart(messageBodyPart);
    }

    public void sendGeneral(String to, String subject, String body, String cc) {
        try {
            this.connect();
            
            MimeMessage message = new MimeMessage(this.session);
            message.setFrom(new InternetAddress(this.from));
            
            if (to != null && !to.equals("")) {
                message.addRecipient(Message.RecipientType.TO, new InternetAddress(to));
            }
            
            if (cc != null && !cc.equals("")) {
                Message.RecipientType type = (to != null && !to.equals("")) ?
                                                Message.RecipientType.CC : Message.RecipientType.TO;
                
                message.addRecipient(type, new InternetAddress(cc));
            }
                      
            message.setSubject(subject, "UTF-8");
            message.setSentDate(new Date());
            message.setContent(body, "text/html; charset=UTF-8");
            message.saveChanges();
            
            this.transport.sendMessage(message, message.getAllRecipients());
            this.close();
        } catch (MessagingException e) {
            Logger.error(e.getMessage());
        }
    }
    
    public void sendWithAttachment(String to, String subject, String body, File attachment) {
        try {
            this.connect();
            
            MimeMessage message   = new MimeMessage(this.session);
            
            message.setFrom(new InternetAddress(this.from));
            
            if (to != null && !to.equals("")) {
                message.addRecipient(Message.RecipientType.TO, new InternetAddress(to));
            }
            
            message.setSubject(subject, "UTF-8");
            message.setSentDate(new Date());
            
            //Add attachment
            if (attachment != null) {
                Multipart multipart   = new MimeMultipart();
                
                MimeBodyPart htmlPart = new MimeBodyPart();                
                htmlPart.setContent(body, "text/html; charset=utf-8");
                
                multipart.addBodyPart(htmlPart);
                this.addAttachment(multipart, attachment);
                message.setContent(multipart);
            } else {
                message.setContent(body, "text/html; charset=utf-8");
            }

            message.saveChanges();
            
            this.transport.sendMessage(message, message.getAllRecipients());
            this.close();
        } catch (MessagingException e) {
            Logger.error(e.getMessage());
        }
    }
}
