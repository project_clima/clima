/**
 * @(#) LogError 17/03/2017
 */

package utils;

import java.util.ArrayList;
import java.util.List;

/**
 * Class for set errors logs
 * @author Rodolfo Miranda -- Qualtop
 */
public class LogErrorList {

    private static LogErrorList instance = null;
    private List<String> log;
    
    public LogErrorList(){
       
    }
    
    public static LogErrorList getInstance(){
        if( instance == null ){
            instance = new LogErrorList();
            instance.initializeLog();
        }
        return instance;
    }
    
    private void initializeLog(){
        log = new ArrayList<>();
    }
    
    public void setError(String error){
        log.add(error);
    }
}
