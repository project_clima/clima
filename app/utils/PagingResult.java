package utils;
import java.util.*;

public class PagingResult {
    public  long totalRecords;
    public  List data;
    
    public PagingResult(long totalRecords, List data) {
        this.totalRecords = totalRecords;
        this.data = data;
    }
}
