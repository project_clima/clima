package utils;

import javax.mail.MessagingException;
import javax.mail.NoSuchProviderException;

import play.Logger;

public class MailUtils {
	
	
	public static void sendMail(String subject, String body, Integer surveyId, String emailUser) {
		Logger.info("email " + emailUser);
		try {
			
			GmailMailer mailer = new GmailMailer();
			mailer.connect();
			mailer.send(
                    emailUser,
                    subject,
                    body,
                    surveyId,
                    null
                ); 
			
		} catch (NoSuchProviderException e) {			
			e.printStackTrace();
		} catch (MessagingException e) {			
			e.printStackTrace();
		}
	}

}
