package utils;

import play.Play;
import org.apache.commons.mail.DefaultAuthenticator;
import org.apache.commons.mail.EmailException;
import org.apache.commons.mail.HtmlEmail;

public class SimpleMail {
    private final String host     = Play.configuration.getProperty("mail.smtp.host");
    private final String port     = Play.configuration.getProperty("mail.smtp.port");
    private final String user     = Play.configuration.getProperty("mail.smtp.user");
    private final String password = Play.configuration.getProperty("mail.smtp.pass");
    
    public HtmlEmail SimpleMail(String subject, String from, String fromName) throws EmailException  {
        return getMailer(subject, from, fromName);
    }
    
    public HtmlEmail SimpleMail(String subject, String from) throws EmailException  {
        return getMailer(subject, from, null);
    }
    
    public HtmlEmail SimpleMail(String subject) throws EmailException  {
        return getMailer(subject, null, null);
    }
    
    public HtmlEmail getMailer(String subject, String from, String fromName) throws EmailException {
        DefaultAuthenticator auth = new DefaultAuthenticator(
            this.user,
            this.password
        );
        
        from = from == null ? this.user : from;
        fromName = fromName == null ? this.user : fromName;
        
        HtmlEmail emailSender = new HtmlEmail();
        emailSender.setCharset("utf-8");
        emailSender.setHostName(this.host);
        emailSender.setSmtpPort(Integer.parseInt(this.port));
        emailSender.setAuthenticator(auth);
        emailSender.setStartTLSEnabled(true);
        emailSender.setTLS(true);
        emailSender.setFrom(from, fromName);                
        emailSender.setSubject(subject);
        
        return emailSender;
    }
}
