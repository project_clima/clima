package utils;

import java.util.Calendar;
import java.util.Date;

public class DateUtils {
    public static Date initDateHour(int hour, int minute, int second, int millisecond) {
        Calendar date = Calendar.getInstance();
        date.set(Calendar.HOUR_OF_DAY, hour);
        date.set(Calendar.MINUTE, minute);
        date.set(Calendar.SECOND, second);
        date.set(Calendar.MILLISECOND, millisecond);

        return date.getTime();
    }

    public static Date initDate(int year, int month, int day, int hour, int minute, int second, int millisecond) {
        Calendar date = Calendar.getInstance();
        date.set(Calendar.YEAR, year);
        date.set(Calendar.MONTH, month);
        date.set(Calendar.DAY_OF_MONTH, day);
        date.set(Calendar.HOUR_OF_DAY, hour);
        date.set(Calendar.MINUTE, minute);
        date.set(Calendar.SECOND, second);
        date.set(Calendar.MILLISECOND, millisecond);

        return date.getTime();
    }

    public final static String MONTHS[] = new String[] {
        "Enero",
        "Febrero",
        "Marzo",
        "Abril",
        "Mayo",
        "Junio",
        "Julio",
        "Agosto",
        "Septiembre",
        "Octubre",
        "Noviembre",
        "Diciembre"
    };
}
