/**
 * @(#) DbConnection 4/04/2017
 */

package utils.nps.connectionDB;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import play.Play;
import utils.nps.vars.EnvironmentVar;

/**
 * Clase para la conexion a la base de datos manual
 * @author Rodolfo Miranda -- Qualtop
 */
public class DbConnection {

    /**
     * Metodo para armar la conexinon a la base de datos
     * @return 
     */
    public static Connection getDBConnection() {
     
        Connection dbConnection = null;

        try {
            Class.forName(EnvironmentVar.DB_DRIVER);
        } catch (ClassNotFoundException e) {

        }
	try {
            dbConnection = DriverManager.getConnection(
            Play.configuration.getProperty("db.default.url"), 
            Play.configuration.getProperty("db.default.user"),
            Play.configuration.getProperty("db.default.pass"));
            return dbConnection;
	} catch (SQLException e) {
            System.out.println(e.getMessage());
	}
	return dbConnection;
    }

}
