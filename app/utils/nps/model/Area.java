/**
 * @(#) Area 6/04/2017
 */
package utils.nps.model;

/**
 * Clase wrapper para el manejo de areas
 * @author Rodolfo Miranda -- Qualtop
 */
public class Area {

    private String id;
    private String idZone;
    private String desc;
    private float nps;
    private String type;

    public float getNps() {
        return nps;
    }

    public void setNps(float nps) {
        this.nps = nps;
    }

    public String getIdZone() {
        return idZone;
    }

    public void setIdZone(String idZone) {
        this.idZone = idZone;
    }

    public String getDesc() {
        return desc;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }
    
}
