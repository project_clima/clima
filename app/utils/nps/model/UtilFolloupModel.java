/**
 * @(#) UtilFolloupModel 8/05/2017
 */
package utils.nps.model;

/**
 * Clase wrapper para el Seguimiento
 * @author Rodolfo Miranda -- Qualtop
 */
public class UtilFolloupModel {

    private String zone;
    private int promotoras;
    private String percentPromo;
    private int detra_pasivas;
    private String percentDetra;
    private int totalRespuestas;
    private int numGerentes;
    private int seguimiento;
    private String percentSeg;
    private int sin_seguimiento;
    private String percentSinS;
    private int closed;
    private String percentCd;

    public String getZone() {
        return zone;
    }

    public void setZone(String zone) {
        this.zone = zone;
    }

    public int getPromotoras() {
        return promotoras;
    }

    public void setPromotoras(int promotoras) {
        this.promotoras = promotoras;
    }

    public int getDetra_pasivas() {
        return detra_pasivas;
    }

    public void setDetra_pasivas(int detra_pasivas) {
        this.detra_pasivas = detra_pasivas;
    }

    public int getTotalRespuestas() {
        return totalRespuestas;
    }

    public void setTotalRespuestas(int totalRespuestas) {
        this.totalRespuestas = totalRespuestas;
    }

    public int getNumGerentes() {
        return numGerentes;
    }

    public void setNumGerentes(int numGerentes) {
        this.numGerentes = numGerentes;
    }

    public int getSeguimiento() {
        return seguimiento;
    }

    public void setSeguimiento(int seguimiento) {
        this.seguimiento = seguimiento;
    }

    public int getSin_seguimiento() {
        return sin_seguimiento;
    }

    public void setSin_seguimiento(int sin_seguimiento) {
        this.sin_seguimiento = sin_seguimiento;
    }

    public String getPercentPromo() {
        return percentPromo;
    }

    public void setPercentPromo(String percentPromo) {
        this.percentPromo = percentPromo;
    }

    public String getPercentDetra() {
        return percentDetra;
    }

    public void setPercentDetra(String percentDetra) {
        this.percentDetra = percentDetra;
    }

    public String getPercentSeg() {
        return percentSeg;
    }

    public void setPercentSeg(String percentSeg) {
        this.percentSeg = percentSeg;
    }

    public int getClosed() {
        return closed;
    }

    public void setClosed(int closed) {
        this.closed = closed;
    }

    public String getPercentSinS() {
        return percentSinS;
    }

    public void setPercentSinS(String percentSinS) {
        this.percentSinS = percentSinS;
    }

    public String getPercentCd() {
        return percentCd;
    }

    public void setPercentCd(String percentCd) {
        this.percentCd = percentCd;
    }

}
