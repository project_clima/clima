/**
 * @(#) Practice 19/05/2017
 */

package utils.nps.model;

/**
 * Clase wrapper para las preacticas
 * @author Rodolfo Miranda -- Qualtop
 */
public class Practice {

    private String idPractice;
    private String desc;
    private String idArea;
    private String type;

    public String getIdPractice() {
        return idPractice;
    }

    public void setIdPractice(String idPractice) {
        this.idPractice = idPractice;
    }

    public String getDesc() {
        return desc;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }

    public String getIdArea() {
        return idArea;
    }

    public void setIdArea(String idArea) {
        this.idArea = idArea;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }
    
}
