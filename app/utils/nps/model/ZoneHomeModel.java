/**
 * @(#) ZoneHomeModel 4/04/2017
 */
package utils.nps.model;

import java.util.List;

/**
 * Clase Wrapper para las Zonas
 * @author Rodolfo Miranda -- Qualtop
 */
public class ZoneHomeModel {

    private String zoneId;
    private String id;
    private List<String> storesIds;
    private List<Store> stores;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }
    
    public String getZoneId() {
        return zoneId;
    }

    public void setZoneId(String zoneId) {
        this.zoneId = zoneId;
    }

    public List<Store> getStores() {
        return stores;
    }

    public void setStores(List<Store> stores) {
        this.stores = stores;
    }

    public List<String> getStoresIds() {
        return storesIds;
    }

    public void setStoresIds(List<String> storesIds) {
        this.storesIds = storesIds;
    }

    @Override
    public String toString() {
        return "ZoneHomeModel{" + "zoneId=" + zoneId + ", id=" + id
                + ", stores=" + stores + '}';
    }

}
