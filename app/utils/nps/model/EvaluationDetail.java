/**
 * @(#) EvaluationDetail 19/04/2017
 */
package utils.nps.model;

/**
 * Clase dto para el detalle de evaluaciones
 * @author Rodolfo Miranda -- Qualtop
 */
public class EvaluationDetail {

    private String id;
    private String answer1;
    private String answer2;
    private String answer1_1;
    private String answer2_1;
    private String cat_answer1_1;
    private String cat_answer2_1;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getAnswer1() {
        return answer1;
    }

    public void setAnswer1(String answer1) {
        this.answer1 = answer1;
    }

    public String getAnswer2() {
        return answer2;
    }

    public void setAnswer2(String answer2) {
        this.answer2 = answer2;
    }

    public String getAnswer1_1() {
        return answer1_1;
    }

    public void setAnswer1_1(String answer1_1) {
        this.answer1_1 = answer1_1;
    }

    public String getAnswer2_1() {
        return answer2_1;
    }

    public void setAnswer2_1(String answer2_1) {
        this.answer2_1 = answer2_1;
    }

    public String getCat_answer1_1() {
        return cat_answer1_1;
    }

    public void setCat_answer1_1(String cat_answer1_1) {
        this.cat_answer1_1 = cat_answer1_1;
    }

    public String getCat_answer2_1() {
        return cat_answer2_1;
    }

    public void setCat_answer2_1(String cat_answer2_1) {
        this.cat_answer2_1 = cat_answer2_1;
    }

    @Override
    public String toString() {
        return "EvaluationDetail{" + "id=" + id + ", answer1=" + answer1 + ", answer2=" + answer2 + ", answer1_1=" + answer1_1 + ", answer2_1=" + answer2_1 + ", cat_answer1_1=" + cat_answer1_1 + ", cat_answer2_1=" + cat_answer2_1 + '}';
    }

}
