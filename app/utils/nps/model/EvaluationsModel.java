/**
 * @(#) EvaluationsModel 9/06/2017
 */

package utils.nps.model;

import java.util.List;

/**
 * Clase dto para las evaluaciones, al exportar en excel
 * @author Rodolfo Miranda -- Qualtop
 */
public class EvaluationsModel {

    private List evaluations;
    private int count;

    public List getEvaluations() {
        return evaluations;
    }

    public void setEvaluations(List evaluations) {
        this.evaluations = evaluations;
    }

    public int getCount() {
        return count;
    }

    public void setCount(int count) {
        this.count = count;
    }
    
    
}
