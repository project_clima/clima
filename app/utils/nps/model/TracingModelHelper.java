/**
 * @(#) TracingModelHelper 10/06/2017
 */

package utils.nps.model;

import java.util.List;
import models.nps.Tracing;

/**
 * Clase wrapper para los Seguimientos
 * @author Rodolfo Miranda -- Qualtop
 */
public class TracingModelHelper {

    private List<Tracing> tracings;
    private NpsAnswerTmp aT;

    public List<Tracing> getTracings() {
        return tracings;
    }

    public void setTracings(List<Tracing> tracings) {
        this.tracings = tracings;
    }

    public NpsAnswerTmp getaT() {
        return aT;
    }

    public void setaT(NpsAnswerTmp aT) {
        this.aT = aT;
    }
    
    
}
