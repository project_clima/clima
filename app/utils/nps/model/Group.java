/**
 * @(#) Groups 11/05/2017
 */
package utils.nps.model;

import java.util.List;

/**
 * Clase Wrapper para los grupos
 * @author Rodolfo Miranda -- Qualtop
 */
public class Group {

    private String id;
    private String desc;
    private String nps;
    private String npsP;
    private List<Business> business;
    private List<Area> areas;

    public List<Business> getBusiness() {
        return business;
    }

    public void setBusiness(List<Business> business) {
        this.business = business;
    }

    public String getNps() {
        return nps;
    }

    public void setNps(String nps) {
        this.nps = nps;
    }

    public List<Area> getAreas() {
        return areas;
    }

    public void setAreas(List<Area> areas) {
        this.areas = areas;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getDesc() {
        return desc;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }

    public String getNpsP() {
        return npsP;
    }

    public void setNpsP(String npsP) {
        this.npsP = npsP;
    }

}

