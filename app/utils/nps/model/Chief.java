/**
 * @(#) Chief 18/04/2017
 */
package utils.nps.model;

import java.util.List;

/**
 * Clase dto para los jefes
 * @author Rodolfo Miranda -- Qualtop
 */
public class Chief {

    private String desc;
    private String id;
    private String title;
    private String nps;
    private String npsP;
    private String parent;
    private String idParent;
    private String idStore;
    private String descStore;
    private List<Area> areas;
    private List<Section> sections;

    public String getDesc() {
        return desc;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }

    public List<Area> getAreas() {
        return areas;
    }

    public void setAreas(List<Area> areas) {
        this.areas = areas;
    }

    public List<Section> getSections() {
        return sections;
    }

    public void setSections(List<Section> sections) {
        this.sections = sections;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getNps() {
        return nps;
    }

    public void setNps(String nps) {
        this.nps = nps;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getParent() {
        return parent;
    }

    public void setParent(String parent) {
        this.parent = parent;
    }

    public String getIdParent() {
        return idParent;
    }

    public void setIdParent(String idParent) {
        this.idParent = idParent;
    }

    public String getIdStore() {
        return idStore;
    }

    public void setIdStore(String idStore) {
        this.idStore = idStore;
    }

    public String getDescStore() {
        return descStore;
    }

    public void setDescStore(String descStore) {
        this.descStore = descStore;
    }

    public String getNpsP() {
        return npsP;
    }

    public void setNpsP(String npsP) {
        this.npsP = npsP;
    }

}
