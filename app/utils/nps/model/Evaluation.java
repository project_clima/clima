/**
 * @(#) Evaluation 10/04/2017
 */
package utils.nps.model;

/**
 * Clase wrapper para las evaluciones
 * @author Rodolfo Miranda -- Qualtop
 */
public class Evaluation {

    private String id;
    private String fecha;
    private String canal;
    private String ubicacion;
    private String area;
    private String nombre;
    private String vendedor;
    private String seccion;
    private String grabacion;
    private String seguimiento;
    private String idImpugnacion;
    private String challenge;
    private String pregunta1;
    private String pregunta2;
    private String openAnswer1;
    private String openAnswer2;
    private String typeAnswer1;
    private String typeAnswer2;
    private String userIdManager;
    private String userIdChief;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getArea() {
        return area;
    }

    public void setArea(String area) {
        this.area = area;
    }

    public String getChallenge() {
        return challenge;
    }

    public void setChallenge(String challenge) {
        this.challenge = challenge;
    }

    public String getOpenAnswer1() {
        return openAnswer1;
    }

    public void setOpenAnswer1(String openAnswer1) {
        this.openAnswer1 = openAnswer1;
    }

    public String getOpenAnswer2() {
        return openAnswer2;
    }

    public void setOpenAnswer2(String openAnswer2) {
        this.openAnswer2 = openAnswer2;
    }

    public String getTypeAnswer1() {
        return typeAnswer1;
    }

    public void setTypeAnswer1(String typeAnswer1) {
        this.typeAnswer1 = typeAnswer1;
    }

    public String getTypeAnswer2() {
        return typeAnswer2;
    }

    public void setTypeAnswer2(String typeAnswer2) {
        this.typeAnswer2 = typeAnswer2;
    }

    public String getUserIdManager() {
        return userIdManager;
    }

    public void setUserIdManager(String userIdManager) {
        this.userIdManager = userIdManager;
    }

    public String getUserIdChief() {
        return userIdChief;
    }

    public void setUserIdChief(String userIdChief) {
        this.userIdChief = userIdChief;
    }

    public String getFecha() {
        return fecha;
    }

    public void setFecha(String fecha) {
        this.fecha = fecha;
    }

    public String getCanal() {
        return canal;
    }

    public void setCanal(String canal) {
        this.canal = canal;
    }

    public String getUbicacion() {
        return ubicacion;
    }

    public void setUbicacion(String ubicacion) {
        this.ubicacion = ubicacion;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getVendedor() {
        return vendedor;
    }

    public void setVendedor(String vendedor) {
        this.vendedor = vendedor;
    }

    public String getSeccion() {
        return seccion;
    }

    public void setSeccion(String seccion) {
        this.seccion = seccion;
    }

    public String getGrabacion() {
        return grabacion;
    }

    public void setGrabacion(String grabacion) {
        this.grabacion = grabacion;
    }

    public String getSeguimiento() {
        return seguimiento;
    }

    public void setSeguimiento(String seguimiento) {
        this.seguimiento = seguimiento;
    }

    public String getPregunta1() {
        return pregunta1;
    }

    public void setPregunta1(String pregunta1) {
        this.pregunta1 = pregunta1;
    }

    public String getPregunta2() {
        return pregunta2;
    }

    public void setPregunta2(String pregunta2) {
        this.pregunta2 = pregunta2;
    }

    public String getIdImpugnacion() {
        return idImpugnacion;
    }

    public void setIdImpugnacion(String idImpugnacion) {
        this.idImpugnacion = idImpugnacion;
    }

}
