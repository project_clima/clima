/**
 * @(#) Complaint 12/04/2017
 */

package utils.nps.model;

/**
 * CLase wrapper para las impugnaciones
 * @author Rodolfo Miranda -- Qualtop
 */
public class Complaint {

    private String complaint;
    private String idCom;
    private String area;
    private String total;

    public String getComplaint() {
        return complaint;
    }

    public void setComplaint(String complaint) {
        this.complaint = complaint;
    }

    public String getIdCom() {
        return idCom;
    }

    public void setIdCom(String idCom) {
        this.idCom = idCom;
    }

    public String getArea() {
        return area;
    }

    public void setArea(String area) {
        this.area = area;
    }

    public String getTotal() {
        return total;
    }

    public void setTotal(String total) {
        this.total = total;
    }
    
}
