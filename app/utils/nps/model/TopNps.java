/**
 * @(#) TopNps 13/05/2017
 */

package utils.nps.model;

import java.util.List;

/**
 * Clase wrapper para el manejo del Top NPS
 * @author Rodolfo Miranda -- Qualtop
 */
public class TopNps {

    List<Manager> managers;
    List<Section> sections;
    List<Store> stores;

    public List<Manager> getManagers() {
        return managers;
    }

    public void setManagers(List<Manager> managers) {
        this.managers = managers;
    }

    public List<Section> getSections() {
        return sections;
    }

    public void setSections(List<Section> sections) {
        this.sections = sections;
    }

    public List<Store> getStores() {
        return stores;
    }

    public void setStores(List<Store> stores) {
        this.stores = stores;
    }
    
}
