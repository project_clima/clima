/**
 * @(#) NpsZoneArea 6/04/2017
 */
package utils.nps.model;

import java.util.List;

/**
 * Clase Wrapper para la zona
 * @author Rodolfo Miranda -- Qualtop
 */
public class Zone {

    private String id;
    private String desc;
    private List<Area> areas;
    private float nps;
    private float npsP;
    private List<Store> stores;
    private List<String> ids;

    public List<Store> getStores() {
        return stores;
    }

    public void setStores(List<Store> stores) {
        this.stores = stores;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public List<Area> getAreas() {
        return areas;
    }

    public void setAreas(List<Area> areas) {
        this.areas = areas;
    }

    public float getNps() {
        return nps;
    }

    public void setNps(float nps) {
        this.nps = nps;
    }

    public String getDesc() {
        return desc;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }

    public List<String> getIds() {
        return ids;
    }

    public void setIds(List<String> ids) {
        this.ids = ids;
    }

    public float getNpsP() {
        return npsP;
    }

    public void setNpsP(float npsP) {
        this.npsP = npsP;
    }

}
