/**
 * @(#) NpsAnswerTmp 20/04/2017
 */
package utils.nps.model;

/**
 * Clase wrapper para Las evaluaciones
 * @author Rodolfo Miranda -- Qualtop
 */
public class NpsAnswerTmp {

    private String idClient;
    private String nameClient;
    private String ip;
    private String phone;
    private String email;
    private String doc;
    private String terminal;
    private String idFollow;
    private String sku;
    private String description;
    private String idAnswer;
    
    public String getIdAnswer() {
        return idAnswer;
    }

    public void setIdAnswer(String idAnswer) {
        this.idAnswer = idAnswer;
    }

    public String getIdClient() {
        return idClient;
    }

    public void setIdClient(String idClient) {
        this.idClient = idClient;
    }

    public String getNameClient() {
        return nameClient;
    }

    public void setNameClient(String nameClient) {
        this.nameClient = nameClient;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getDoc() {
        return doc;
    }

    public void setDoc(String doc) {
        this.doc = doc;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getIdFollow() {
        return idFollow;
    }

    public void setIdFollow(String idFollow) {
        this.idFollow = idFollow;
    }

    public String getIp() {
        return ip;
    }

    public void setIp(String ip) {
        this.ip = ip;
    }

    public String getSku() {
        return sku;
    }

    public void setSku(String sku) {
        this.sku = sku;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getTerminal() {
        return terminal;
    }

    public void setTerminal(String terminal) {
        this.terminal = terminal;
    }

}
