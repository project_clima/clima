/**
 * @(#) Function 30/06/2017
 */

package utils.nps.model;

/**
 * Clase Dto para las funciones
 * @author Rodolfo Miranda -- Qualtop
 */
public class Function {

    private String title;
    private String idFunction;
    private String type;

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getIdFunction() {
        return idFunction;
    }

    public void setIdFunction(String idFunction) {
        this.idFunction = idFunction;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }
    
    
}
