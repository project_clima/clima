/**
 * @(#) Store 6/04/2017
 */
package utils.nps.model;

import java.util.List;

/**
 * Clase wrapper para las Ubicaciones
 * @author Rodolfo Miranda -- Qualtop
 */
public class Store implements Comparable<Store>{

    private String id;
    private String desc;
    private String nps;
    private String npsP;
    private String parent;
    private String idParent;
    private List<Manager> managers;
    private List<Area> areas;
    private String group;
    private String business;
    private String type;
    private List<String> ids;
    
    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getDesc() {
        return desc;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }

    public String getNps() {
        return nps;
    }

    public void setNps(String nps) {
        this.nps = nps;
    }

    public List<Manager> getManagers() {
        return managers;
    }

    public void setManagers(List<Manager> managers) {
        this.managers = managers;
    }

    public List<Area> getAreas() {
        return areas;
    }

    public void setAreas(List<Area> areas) {
        this.areas = areas;
    }
    
    @Override
    public int compareTo(Store o) {
       String compareNps = ((Store) o).getNps();
       
       return compareNps.compareTo(this.getNps());
    }

    public String getGroup() {
        return group;
    }

    public void setGroup(String group) {
        this.group = group;
    }

    public String getBusiness() {
        return business;
    }

    public void setBusiness(String business) {
        this.business = business;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getParent() {
        return parent;
    }

    public void setParent(String parent) {
        this.parent = parent;
    }

    public String getIdParent() {
        return idParent;
    }

    public void setIdParent(String idParent) {
        this.idParent = idParent;
    }

    public List<String> getIds() {
        return ids;
    }

    public void setIds(List<String> ids) {
        this.ids = ids;
    }

    public String getNpsP() {
        return npsP;
    }

    public void setNpsP(String npsP) {
        this.npsP = npsP;
    }

}
