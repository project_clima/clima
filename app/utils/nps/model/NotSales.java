/**
 * @(#) NotSales 30/06/2017
 */

package utils.nps.model;

import java.util.Date;

/**
 * Clase wrapper para el catalo de No Ventas
 * @author Rodolfo Miranda -- Qualtop
 */
public class NotSales {

    private String empNumber;
    private String idStore;
    private String idArea;    
    private String name;
    private String userIdChief;
    private String userIdManager;
    private String userIdDirector;
    private String userIdZonal;
    private String zone;
    private String idSection;
    private String userId;   
    private String type;

    public String getEmpNumber() {
        return empNumber;
    }

    public void setEmpNumber(String empNumber) {
        this.empNumber = empNumber;
    }

    public String getIdStore() {
        return idStore;
    }

    public void setIdStore(String idStore) {
        this.idStore = idStore;
    }

    public String getIdArea() {
        return idArea;
    }

    public void setIdArea(String idArea) {
        this.idArea = idArea;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getUserIdChief() {
        return userIdChief;
    }

    public void setUserIdChief(String userIdChief) {
        this.userIdChief = userIdChief;
    }

    public String getUserIdManager() {
        return userIdManager;
    }

    public void setUserIdManager(String userIdManager) {
        this.userIdManager = userIdManager;
    }

    public String getUserIdDirector() {
        return userIdDirector;
    }

    public void setUserIdDirector(String userIdDirector) {
        this.userIdDirector = userIdDirector;
    }

    public String getUserIdZonal() {
        return userIdZonal;
    }

    public void setUserIdZonal(String userIdZonal) {
        this.userIdZonal = userIdZonal;
    }

    public String getZone() {
        return zone;
    }

    public void setZone(String zone) {
        this.zone = zone;
    }

    public String getIdSection() {
        return idSection;
    }

    public void setIdSection(String idSection) {
        this.idSection = idSection;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }
    
    
}
