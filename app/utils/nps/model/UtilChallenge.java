/**
 * @(#) UtilChallenge 4/05/2017
 */
package utils.nps.model;

/** 
 * Clase wrapper de utilidad para las Impugnaciones
 * @author Rodolfo Miranda -- Qualtop
 */
public class UtilChallenge {

    private int year;
    private long countChalls[];

    public int getYear() {
        return year;
    }

    public void setYear(int year) {
        this.year = year;
    }

    public long[] getCountChalls() {
        return countChalls;
    }

    public void setCountChalls(long[] countChalls) {
        this.countChalls = countChalls;
    }

}
