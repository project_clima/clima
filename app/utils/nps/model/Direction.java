/**
 * @(#) Direction 18/04/2017
 */
package utils.nps.model;

/**
 * Clase wrapper para las direcciones en NPS
 * @author Rodolfo Miranda -- Qualtop
 */
public class Direction {

    private String nameDir;
    private String npsDir;

    public String getNameDir() {
        return nameDir;
    }

    public void setNameDir(String nameDir) {
        this.nameDir = nameDir;
    }

    public String getNpsDir() {
        return npsDir;
    }

    public void setNpsDir(String npsDir) {
        this.npsDir = npsDir;
    }

}
