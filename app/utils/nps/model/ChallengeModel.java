/**
 * @(#) ChallengeModel 11/06/2017
 */

package utils.nps.model;

import java.util.Date;
import java.util.List;
import models.nps.Challenge;

/**
 * Clase dto para el manejo de las impugnaciones
 * @author Rodolfo Miranda -- Qualtop
 */
public class ChallengeModel {

    private String idAnswer;
    private String userId;
    private String userName;
    private String fullName;
    private String status;
    private Date dateImpugn;
    private List<Challenge> challenges;
    private String qualif1;
    private String qualif2;
    private String idEmployee;
    private String idArea;
    private String idPractice;
    private String type;
    private String original1;
    private String original2;
    private String original3;
    private boolean impug;

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public List<Challenge> getChallenges() {
        return challenges;
    }

    public void setChallenges(List<Challenge> challenges) {
        this.challenges = challenges;
    }

    public Date getDateImpugn() {
        return dateImpugn;
    }

    public void setDateImpugn(Date dateImpugn) {
        this.dateImpugn = dateImpugn;
    }

    public String getIdAnswer() {
        return idAnswer;
    }

    public void setIdAnswer(String idAnswer) {
        this.idAnswer = idAnswer;
    }

    public String getFullName() {
        return fullName;
    }

    public void setFullName(String fullName) {
        this.fullName = fullName;
    }

    public String getQualif1() {
        return qualif1;
    }

    public void setQualif1(String qualif1) {
        this.qualif1 = qualif1;
    }

    public String getQualif2() {
        return qualif2;
    }

    public void setQualif2(String qualif2) {
        this.qualif2 = qualif2;
    }

    public String getIdEmployee() {
        return idEmployee;
    }

    public void setIdEmployee(String idEmployee) {
        this.idEmployee = idEmployee;
    }

    public String getIdArea() {
        return idArea;
    }

    public void setIdArea(String idArea) {
        this.idArea = idArea;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getOriginal1() {
        return original1;
    }

    public void setOriginal1(String original1) {
        this.original1 = original1;
    }

    public String getOriginal2() {
        return original2;
    }

    public void setOriginal2(String original2) {
        this.original2 = original2;
    }

    public String getIdPractice() {
        return idPractice;
    }

    public void setIdPractice(String idPractice) {
        this.idPractice = idPractice;
    }

    public String getOriginal3() {
        return original3;
    }

    public void setOriginal3(String original3) {
        this.original3 = original3;
    }

    public boolean isImpug() {
        return impug;
    }

    public void setImpug(boolean impug) {
        this.impug = impug;
    }

}
