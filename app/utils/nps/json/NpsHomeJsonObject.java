/**
 * @(#) NpsHomeJsonObject 31/03/2017
 */
package utils.nps.json;

import java.util.List;
import utils.nps.model.Area;
import utils.nps.model.Chief;
import utils.nps.model.Manager;
import utils.nps.model.Seller;
import utils.nps.model.Store;
import utils.nps.model.TopNps;
import utils.nps.model.Zone;
import utils.nps.model.ZoneHomeModel;

/**
 * Clase representativa del objeto Json para la respuesta del procesamiento NPS
 * @author Rodolfo Miranda -- Qualtop
 */
public class NpsHomeJsonObject {

    private float[] npsCompany;
    private List<ZoneHomeModel> zones;
    private Float[] months;
    private Float[] monthsLess1;
    private Float[] triMonths;
    private Float[] triMonthsLess1;
    private List<Zone> zAreas;
    private TopNps topAsc;
    private TopNps topDesc;
    private String question;
    private List<Area> areas;
    private String[] goal;
    private String type;
    private List<Manager> mans;
    private List<Chief> chfs;
    private List<Seller> sells;
    private List<Store> stores;

    public List<ZoneHomeModel> getZones() {
        return zones;
    }

    public void setZones(List<ZoneHomeModel> zones) {
        this.zones = zones;
    }

    public float[] getNpsCompany() {
        return npsCompany;
    }

    public void setNpsCompany(float[] npsCompany) {
        this.npsCompany = npsCompany;
    }

    public Float[] getMonths() {
        return months;
    }

    public void setMonths(Float[] months) {
        this.months = months;
    }

    public List<Zone> getzAreas() {
        return zAreas;
    }

    public void setzAreas(List<Zone> zAreas) {
        this.zAreas = zAreas;
    }

    public Float[] getMonthsLess1() {
        return monthsLess1;
    }

    public void setMonthsLess1(Float[] monthsLess1) {
        this.monthsLess1 = monthsLess1;
    }

    public Float[] getTriMonths() {
        return triMonths;
    }

    public void setTriMonths(Float[] triMonths) {
        this.triMonths = triMonths;
    }

    public Float[] getTriMonthsLess1() {
        return triMonthsLess1;
    }

    public void setTriMonthsLess1(Float[] triMonthsLess1) {
        this.triMonthsLess1 = triMonthsLess1;
    }

    public String getQuestion() {
        return question;
    }

    public void setQuestion(String question) {
        this.question = question;
    }

    public TopNps getTopAsc() {
        return topAsc;
    }

    public void setTopAsc(TopNps topAsc) {
        this.topAsc = topAsc;
    }

    public List<Area> getAreas() {
        return areas;
    }

    public void setAreas(List<Area> areas) {
        this.areas = areas;
    }

    public TopNps getTopDesc() {
        return topDesc;
    }

    public void setTopDesc(TopNps topDesc) {
        this.topDesc = topDesc;
    }

    public String[] getGoal() {
        return goal;
    }

    public void setGoal(String[] goal) {
        this.goal = goal;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public List<Manager> getMans() {
        return mans;
    }

    public void setMans(List<Manager> mans) {
        this.mans = mans;
    }

    public List<Chief> getChfs() {
        return chfs;
    }

    public void setChfs(List<Chief> chfs) {
        this.chfs = chfs;
    }

    public List<Seller> getSells() {
        return sells;
    }

    public void setSells(List<Seller> sells) {
        this.sells = sells;
    }

    public List<Store> getStores() {
        return stores;
    }

    public void setStores(List<Store> stores) {
        this.stores = stores;
    }

}
