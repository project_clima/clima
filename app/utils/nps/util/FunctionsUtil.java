/**
 * @(#) FunctionsUtil 25/05/2017
 */
package utils.nps.util;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Locale;

/**
 * Clase de utileria para obtener el mes de una fecha
 * @author Rodolfo Miranda -- Qualtop
 */
public class FunctionsUtil {

    public static String getMonth() {
        Calendar cal = Calendar.getInstance();
        return new SimpleDateFormat("MMMM",new Locale("es", "ES"))
                .format(cal.getTime()).toUpperCase();
    }
}
