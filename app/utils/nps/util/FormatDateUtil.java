/**
 * @(#) FormatDateUtil 17/03/2017
 */

package utils.nps.util;

import java.text.SimpleDateFormat;

/**
 * Clase de utilidad para el manejo de las fechas
 * @author Rodolfo Miranda -- Qualtop
 */
public class FormatDateUtil {

    public static final String format = "dd/MM/yyyy";
    public static final String format2 = "dd/MM/yyyy HH:mm:ss";
        
    public static final SimpleDateFormat ownFormat = new SimpleDateFormat(format);
    public static final SimpleDateFormat ownFormat2 = new SimpleDateFormat(format2);
}
