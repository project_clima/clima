/**
 * @(#) LogFileNPS 22/03/2017
 */

package utils.nps.log;

import java.text.ParseException;
import java.util.Date;
import models.nps.FileNPS;
import querys.nps.StructureTmpQuery;
import utils.nps.vars.EnvironmentVar;
import utils.nps.util.FormatDateUtil;
import utils.nps.enums.LogFileNPSEnum;

/**
 * Clase de apoyo para el manejo del Log en las cargas de archivos
 * @author Rodolfo Miranda -- Qualtop
 */
public class LogFileNPS {

    public static void updateLog(LogFileNPSEnum type,
            FileNPS fileNps,String name,String operation, String userId) throws ParseException{
        switch(type){
            case AUTO:
                fileNps.type = EnvironmentVar.LOAD_AUTO;
                break;
            case MANUAL:
                fileNps.type = EnvironmentVar.LOAD_MANUAL;
                break;
            default:
                break;
        }
        
        if( operation.equals(EnvironmentVar.INS_NPS_STRUCT) ){
            fileNps.extension= EnvironmentVar.CSV_EXT;
            
        }else if( operation.equals(EnvironmentVar.INS_NPS_ANSWER) ){
            fileNps.extension= EnvironmentVar.TXT_EXT;
            
        }
        fileNps.fileName = name;
        fileNps.module = EnvironmentVar.MODULE;  
        fileNps.loadDate = FormatDateUtil.ownFormat.
                       parse(FormatDateUtil.ownFormat.format(new Date()));
            
        fileNps.status = "PROC";
        fileNps.createFor = userId != null ? userId.toUpperCase() : 
                StructureTmpQuery.getUser().toUpperCase();
        fileNps.createDate = FormatDateUtil.ownFormat.
                        parse(FormatDateUtil.ownFormat.format(new Date()));
    }
}
