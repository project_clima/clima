/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package utils.nps.enums;

/**
 * Enum para el layout del archivo de evaluaciones.
 * @author Rodolfo Miranda -- Qualtop
 */
public enum TxtField {
 
    EMPID(0), 
    AREA(1),
    PRACTICES(2),
    YEAR(3),
    MONTH(4),
    DAY(5),
    ID_PERSON_EVALUATED(6), 
    ANSWER_1(7),
    ANSWER_2(8),
    ANSWER_3(9), 
    RECORDING(10), 
    CLIENT_ID(11),
    ID_CATEGORY_1_1(12), 
    ID_CATEGORY_1_2(13),
    ID_CATEGORY_1_3(14),
    CLIENT_NAME(15), 
    LADA(16),
    PHONE(17),
    ID_STORE(18), 
    ANSWER_4(19), 
    EMAIL(20), 
    ANSWER_5(21),
    SURVEY_TYPE(22),
    ID_CATEGORY_2_1(23),
    ID_CATEGORY_2_2(24), 
    ID_CATEGORY_2_3(25),
    DOC(26),
    TERMINAL(27),
    TICKET_DAY(28), 
    TICKET_MONTH(29),
    TICKET_YEAR(30),
    IP(31),
    SKU(32),
    SKU_DESC(33),
    NUMBER_COLUMNS(34),
    CRE_FOR(35),
    CRE_DATE(36),
    MOD_FOR(37),
    MOD_DATE(38);
    
    private final int i;
		
    TxtField(int i) {
        this.i = i;
    }
    
    public int getI(){
        return i;
    }
}
