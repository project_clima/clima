/*
 * enum Field 17 / 03 / 2017
 */
package utils.nps.enums;

/**
 * Enum para el layout del archivo de Estructura
 * @author Rodolfo Miranda -- Qualtop
 */
public enum Field {
    STATUS(0),
    USERID (1),
    COUNTRY (2),
    CUSTOM01(3),
    CUSTOM02(4),
    CUSTOM03(5),
    CUSTOM04(6),
    CUSTOM05(7),
    CUSTOM06(8),
    CUSTOM07(9),
    CUSTOM08(10),
    CUSTOM09(11),
    CUSTOM10(12),
    CUSTOM13(13),
    CUSTOM_MANAGER(14),
    DEFAULT_LOCALE(15),
    DEPARTMENT(16),
    DIVISION(17),
    EMAIL(18),
    EMPID(19),
    FIRSTNAME(20),
    GENDER(21),
    HIREDATE(22),
    HR(23),
    JOBCODE(24),
    LASTNAME(25),
    LOCATION(26),
    MANAGER(27),
    TIMEZONE(28),
    TITLE(29),
    USERNAME(30);
    
    private final int i;
		
    Field(int i) {
        this.i = i;
    }
    
    public int getI(){
        return i;
    }
}
