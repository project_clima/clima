/**
 * @(#) LogFileNPSEnum 22/03/2017
 */

package utils.nps.enums;

/**
 * Enum para los tipos de estatus en la carga de estructura y evaluaciones
 * @author Rodolfo Miranda -- Qualtop
 */
public enum LogFileNPSEnum {

    READ,
    PROC,
    AUTO,
    MANUAL;
}
