/**
 * @(#) WriteLogNpsFile 17/05/2017
 */

package utils.nps.writeFile;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.util.List;
import models.nps.NpsAnswerTmpLog;
import models.nps.StructureTmpLog;
import utils.nps.vars.EnvironmentVar;

/**
 * Clase de utilidad para escribir los archivos de erroes en las cargas
 * de archivos en el sistema NPS
 * @author Rodolfo Miranda -- Qualtop
 */
public class WriteLogFile {

    /**
     * Metodo que genera el archivo de errores para una carga de evaluaciones
     * @param list
     * @return
     * @throws IOException 
     */
    public static File writeLogNpsFile(List<NpsAnswerTmpLog> list) throws IOException{
        BufferedWriter bwr = null;
        File fileLog = new File("tempLog.txt");
       
            StringBuffer buffer = new StringBuffer();
            
            for( NpsAnswerTmpLog strcLog : list ){
                
                buffer.append(strcLog.emId).append(EnvironmentVar.DELIMITER_TXTW);
                buffer.append(strcLog.idArea).append(EnvironmentVar.DELIMITER_TXTW);
                buffer.append(strcLog.idPractices).append(EnvironmentVar.DELIMITER_TXTW);
                buffer.append(strcLog.year).append(EnvironmentVar.DELIMITER_TXTW);
                buffer.append(strcLog.month).append(EnvironmentVar.DELIMITER_TXTW);
                buffer.append(strcLog.day).append(EnvironmentVar.DELIMITER_TXTW);
                buffer.append(strcLog.idPersonEva).append(EnvironmentVar.DELIMITER_TXTW);
                buffer.append(strcLog.ans1).append(EnvironmentVar.DELIMITER_TXTW);
                buffer.append(strcLog.ans2).append(EnvironmentVar.DELIMITER_TXTW);
                buffer.append(strcLog.ans3).append(EnvironmentVar.DELIMITER_TXTW);
                buffer.append(strcLog.rcording).append(EnvironmentVar.DELIMITER_TXTW);
                buffer.append(strcLog.idClient).append(EnvironmentVar.DELIMITER_TXTW);
                buffer.append(strcLog.idCat1_1).append(EnvironmentVar.DELIMITER_TXTW);
                buffer.append(strcLog.idCat1_2).append(EnvironmentVar.DELIMITER_TXTW);
                buffer.append(strcLog.idCat1_3).append(EnvironmentVar.DELIMITER_TXTW);
                buffer.append(strcLog.clientName).append(EnvironmentVar.DELIMITER_TXTW);
                buffer.append(strcLog.lada).append(EnvironmentVar.DELIMITER_TXTW);
                buffer.append(strcLog.phone).append(EnvironmentVar.DELIMITER_TXTW);
                buffer.append(strcLog.store).append(EnvironmentVar.DELIMITER_TXTW);
                buffer.append(strcLog.ans4).append(EnvironmentVar.DELIMITER_TXTW);
                buffer.append(strcLog.email).append(EnvironmentVar.DELIMITER_TXTW);
                buffer.append(strcLog.ans5).append(EnvironmentVar.DELIMITER_TXTW);
                buffer.append(strcLog.surveyType).append(EnvironmentVar.DELIMITER_TXTW);
                buffer.append(strcLog.cat2_1).append(EnvironmentVar.DELIMITER_TXTW);
                buffer.append(strcLog.cat2_2).append(EnvironmentVar.DELIMITER_TXTW);
                buffer.append(strcLog.cat2_3).append(EnvironmentVar.DELIMITER_TXTW);
                buffer.append(strcLog.doc).append(EnvironmentVar.DELIMITER_TXTW);
                buffer.append(strcLog.terminal).append(EnvironmentVar.DELIMITER_TXTW);
                buffer.append(strcLog.tickeyDay).append(EnvironmentVar.DELIMITER_TXTW);
                buffer.append(strcLog.ticketMonth).append(EnvironmentVar.DELIMITER_TXTW);
                buffer.append(strcLog.ticketYear).append(EnvironmentVar.DELIMITER_TXTW);
                buffer.append(strcLog.ip).append(EnvironmentVar.DELIMITER_TXTW);
                buffer.append(strcLog.sku).append(EnvironmentVar.DELIMITER_TXTW);
                buffer.append(strcLog.descSku).append(EnvironmentVar.DELIMITER_TXTW);
                buffer.append(strcLog.comments).append("\n");
            }   bwr = new BufferedWriter(new OutputStreamWriter
                    (new FileOutputStream(fileLog), "Cp1252"));
            //write contents of StringBuffer to a file
            bwr.write(buffer.toString());
            //flush the stream
            bwr.flush();
            //close the stream
            bwr.close();

            
        return fileLog;
    }
    
    /**
     * MEtodo que escribe el archivo de errores para las cargas de estructura
     * @param list
     * @return
     * @throws IOException 
     */
    public static File writeLogStrcFile(List<StructureTmpLog> list) throws IOException{
        BufferedWriter bwr = null;
        File fileLog = new File("tempLog.csv");
       
            StringBuffer buffer = new StringBuffer();
            
            for( StructureTmpLog strcLog : list ){
                
                buffer.append(strcLog.status).append(EnvironmentVar.DELIMITER_CSV);
                buffer.append(strcLog.userId).append(EnvironmentVar.DELIMITER_CSV);
                buffer.append(strcLog.country).append(EnvironmentVar.DELIMITER_CSV);
                buffer.append(strcLog.custom01).append(EnvironmentVar.DELIMITER_CSV);
                buffer.append(strcLog.custom02).append(EnvironmentVar.DELIMITER_CSV);
                buffer.append(strcLog.custom03).append(EnvironmentVar.DELIMITER_CSV);
                buffer.append(strcLog.custom04).append(EnvironmentVar.DELIMITER_CSV);
                buffer.append(strcLog.custom05).append(EnvironmentVar.DELIMITER_CSV);
                buffer.append(strcLog.custom06).append(EnvironmentVar.DELIMITER_CSV);
                buffer.append(strcLog.custom07).append(EnvironmentVar.DELIMITER_CSV);
                buffer.append(strcLog.custom08).append(EnvironmentVar.DELIMITER_CSV);
                buffer.append(strcLog.custom09).append(EnvironmentVar.DELIMITER_CSV);
                buffer.append(strcLog.custom10).append(EnvironmentVar.DELIMITER_CSV);
                buffer.append(strcLog.custom13).append(EnvironmentVar.DELIMITER_CSV);
                buffer.append(strcLog.customManager).append(EnvironmentVar.DELIMITER_CSV);
                buffer.append(strcLog.defaultLocale).append(EnvironmentVar.DELIMITER_CSV);
                buffer.append(strcLog.deparment).append(EnvironmentVar.DELIMITER_CSV);
                buffer.append(strcLog.division).append(EnvironmentVar.DELIMITER_CSV);
                buffer.append(strcLog.email).append(EnvironmentVar.DELIMITER_CSV);
                buffer.append(strcLog.empId).append(EnvironmentVar.DELIMITER_CSV);
                buffer.append(strcLog.firstName).append(EnvironmentVar.DELIMITER_CSV);
                buffer.append(strcLog.gender).append(EnvironmentVar.DELIMITER_CSV);
                buffer.append(strcLog.hireDate).append(EnvironmentVar.DELIMITER_CSV);
                buffer.append(strcLog.hr).append(EnvironmentVar.DELIMITER_CSV);
                buffer.append(strcLog.jobcode).append(EnvironmentVar.DELIMITER_CSV);
                buffer.append(strcLog.lastname).append(EnvironmentVar.DELIMITER_CSV);
                buffer.append(strcLog.location).append(EnvironmentVar.DELIMITER_CSV);
                buffer.append(strcLog.manager).append(EnvironmentVar.DELIMITER_CSV);
                buffer.append(strcLog.timezone).append(EnvironmentVar.DELIMITER_CSV);
                buffer.append(strcLog.title).append(EnvironmentVar.DELIMITER_CSV);
                buffer.append(strcLog.username).append(EnvironmentVar.DELIMITER_CSV);
                buffer.append(strcLog.obs).append("\n");
            }   
            bwr = new BufferedWriter(new OutputStreamWriter
                (new FileOutputStream(fileLog), "Cp1252"));
            //write contents of StringBuffer to a file
            bwr.write(buffer.toString());
            //flush the stream
            bwr.flush();
            //close the stream
            bwr.close();

            
        return fileLog;
    }
    
    /**
     * MEtodo para el archivo de errores en las cargas de catalogos
     * @param catError
     * @param fileName
     * @return
     * @throws Exception 
     */
    public static File write(List<String> catError, String fileName) throws Exception
   {
       BufferedWriter bwr = null;
        int i = fileName.lastIndexOf('.');
        String newName = fileName.substring(0,i);
        String csv = newName + "_ERROR"+".csv";
        File file =new File(csv);   
        //Create record
        String [] record = {""};
        StringBuffer buffer = new StringBuffer();
        for( String error : catError ){
            buffer.append(error).append("\r\n");
        }
        bwr = new BufferedWriter(new OutputStreamWriter
                (new FileOutputStream(file), "Cp1252"));
            //write contents of StringBuffer to a file
            bwr.write(buffer.toString());
            //flush the stream
            bwr.flush();
        //close the writer
        bwr.close();
        return file; 
   }
}
