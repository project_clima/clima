/**
 * @(#) NpsControllerHelper 25/05/2017
 */
package utils.nps.helpers;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import controllers.NPS;
import java.sql.Connection;
import java.sql.SQLException;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import querys.nps.NpsHomeQuery;
import utils.nps.connectionDB.DbConnection;
import utils.nps.json.NpsHomeJsonObject;
import utils.nps.model.Area;
import utils.nps.model.Business;
import utils.nps.model.Chief;
import utils.nps.model.Group;
import utils.nps.model.Manager;
import utils.nps.model.Section;
import utils.nps.model.Seller;
import utils.nps.model.Store;
import utils.nps.model.TopNps;
import utils.nps.model.Zone;
import utils.nps.util.FormatDateUtil;
import utils.nps.vars.EnvironmentVar;

/**
 * Clase de apoyo para el procesamiento de las peticiones en NPS
 * @author Rodolfo Miranda -- Qualtop
 */
public class NpsControllerHelper {

    /**
     * Metodo de apoyo para el obtencion de la grafica circular e histirico segun
     * los filtros seleccionados
     * @param userId
     * @param firstDate
     * @param lastDate
     * @param zNps
     * @param zone
     * @param group
     * @param business
     * @param op
     * @param store
     * @param chief
     * @param man
     * @param section
     * @param type
     * @return 
     */
    public static NpsHomeJsonObject npsCompanyHelper(String userId, String firstDate,
            String lastDate, String zNps, String zone, String group, String business,
            String op, String store, String chief, String man, String section,
            String type) {

        NpsHomeJsonObject companyH = new NpsHomeJsonObject();
        Gson gson = new Gson();
        List<Zone> zonesStores = null;
        int y = Calendar.getInstance().get(Calendar.YEAR);
        String zoneNps = "";

        Float[] months = null;
        Float[] monthsLess1Year = null;
        Float[] triMonth = null;
        Float[] triMonthLess1Year = null;
        float[] result = null;
        String strs[] = null;
        String chfs[] = null;
        String mans[] = null;
        String sctns[] = null;

        if (op == null || op.equals("")) {
            op = "202";
        }
        Connection oracleConnection = null;

        try {
            oracleConnection = DbConnection.getDBConnection();

            if (store != null && !store.equalsIgnoreCase("")) {
                strs = new String[]{store};
            }
            if (man != null && !man.equalsIgnoreCase("")) {
                mans = new String[]{man};
            }
            if (chief != null && !chief.equalsIgnoreCase("")) {
                chfs = new String[]{chief};
            }
            if (section != null && !section.equalsIgnoreCase("")) {
                sctns = new String[]{section};
            }
            // Nps Company
            result = NpsHomeQuery.getNpsCompany(userId, firstDate,
                    lastDate, strs, mans, chfs, sctns, Integer.parseInt(op),
                    zone, group, business, oracleConnection);
            //Months
            months = NpsHomeQuery.getNpsMonths(userId,
                    "01/01/" + (y), "31/12/" + (y), strs, mans, chfs, sctns,
                    Integer.parseInt(op), zone, group, business, oracleConnection);
            monthsLess1Year = NpsHomeQuery.getNpsMonths(userId,
                    "01/01/" + (y - 1), "31/12/" + (y - 1), strs, mans, chfs, sctns,
                    Integer.parseInt(op), zone, group, business, oracleConnection);
            triMonth = NpsHomeQuery.getNpsTriMonths(userId,
                    "01/01/" + (y), "31/12/" + (y), strs, mans, chfs, sctns,
                    Integer.parseInt(op), zone, group, business, oracleConnection);
            triMonthLess1Year = NpsHomeQuery.getNpsTriMonths(userId,
                    "01/01/" + (y - 1), "31/12/" + (y - 1), strs, mans, chfs, sctns,
                    Integer.parseInt(op), zone, group, business, oracleConnection);

            companyH.setQuestion(op);
            companyH.setMonthsLess1(monthsLess1Year);
            companyH.setNpsCompany(result);
            companyH.setMonths(months);
            companyH.setTriMonths(triMonth);
            companyH.setTriMonthsLess1(triMonthLess1Year);
            //companyH.setAreas(gAreas);
            type = type == null ? "Compañia" : type;
            companyH.setType(type);
            return companyH;
        } catch (SQLException ex) {
            java.util.logging.Logger.getLogger(NPS.class.getName()).
                    log(Level.SEVERE, null, ex);
        } finally {
            if (oracleConnection != null) {
                try {
                    oracleConnection.close();
                } catch (SQLException ex) {
                    Logger.getLogger(NpsControllerHelper.class.getName()).
                            log(Level.SEVERE, null, ex);
                }
            }
        }
        return companyH;
    }

    /**
     * Metodo de apoyo para obtener el top en la grafica NPS
     * @param userId
     * @param firstDate
     * @param lastDate
     * @param op
     * @param zone
     * @param group
     * @param business
     * @return 
     */
    
    public static NpsHomeJsonObject npsTopHelper(String userId, String firstDate,
            String lastDate, String op, String zone, String group,
            String business) {

        NpsHomeJsonObject top = new NpsHomeJsonObject();
        TopNps topAsc = new TopNps();
        TopNps topDesc = new TopNps();
        String[] goal = {};

        if (op == null || op.equals("")) {
            op = "202";
        }
        Connection oracleConnection = null;

        try {
            oracleConnection = DbConnection.getDBConnection();
            topAsc = NpsHomeQuery.getNpsTop(userId, firstDate,
                    lastDate, "ASC", Integer.parseInt(op), zone, group,
                    business, oracleConnection);
            topDesc = NpsHomeQuery.getNpsTop(userId, firstDate,
                    lastDate, "DESC", Integer.parseInt(op), zone, group,
                    business, oracleConnection);
            goal = NpsHomeQuery.getNpsGoal(userId, firstDate,
                    lastDate, EnvironmentVar.goal, Integer.parseInt(op),
                    zone, group, business, oracleConnection);
        } catch (SQLException ex) {
            Logger.getLogger(NPS.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            if (oracleConnection != null) {
                try {
                    oracleConnection.close();
                } catch (SQLException ex) {
                    Logger.getLogger(NpsControllerHelper.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        }
        
        top.setTopAsc(topAsc);
        top.setTopDesc(topDesc);
        top.setGoal(goal);

        return top;
    }

    /**
     * Metodo de apoyo para obtener las areas globales segun los filtros 
     * seleccionados
     * @param userId
     * @param firstDate
     * @param lastDate
     * @param zNps
     * @param zone
     * @param group
     * @param business
     * @param op
     * @param store
     * @param chief
     * @param man
     * @param section
     * @param type
     * @return 
     */
    
    public static List<Area> detailGlobalAreasHelper(String userId, String firstDate,
            String lastDate, String zNps, String zone, String group, String business,
            String op, String store, String chief, String man, String section,
            String type) {

        List<Area> gAreas = new ArrayList<>();
        String strs[] = null;
        String chfs[] = null;
        String mans[] = null;
        String sctns[] = null;

        if (store != null && !store.equalsIgnoreCase("")) {
            strs = store.split(",");
        }
        if (man != null && !man.equalsIgnoreCase("")) {
            mans = man.split(",");
        }
        if (chief != null && !chief.equalsIgnoreCase("")) {
            chfs = chief.split(",");
        }
        if (section != null && !section.equalsIgnoreCase("")) {
            sctns = section.split(",");
        }
        if (op == null || op.equals("")) {
            op = "202";
        }
        Connection oracleConnection = null;

        try {
            oracleConnection = DbConnection.getDBConnection();
            gAreas = NpsHomeQuery.getNpsGlobalArea(userId, firstDate,
                    lastDate, strs, mans, chfs, sctns, Integer.parseInt(op),
                    zone, group, business, oracleConnection);
        } catch (SQLException ex) {
            Logger.getLogger(NPS.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            if (oracleConnection != null) {
                try {
                    oracleConnection.close();
                } catch (SQLException ex) {
                    Logger.getLogger(NpsControllerHelper.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        }

        return gAreas;
    }

    /**
     * Metodo para obtener el detalle de grupos en la logica de negocio segun los
     * filtros seleccionados
     * @param userId
     * @param firstDate
     * @param lastDate
     * @param op
     * @return 
     */
    
    public static List<Group> npsDetailGroupsHelper(String userId,
            String firstDate, String lastDate, String op) {

        List<Group> groups = null;
        float array1[];

        Connection oracleConnection = null;
        oracleConnection = DbConnection.getDBConnection();
        if (op == null || op.equals("")) {
            op = "202";
        }
        
        groups = NpsHomeQuery.getGroups(firstDate,lastDate,userId,oracleConnection);
        
        if (groups != null) {
            for (Group g : groups) {

                try {
                    array1 = NpsHomeQuery.getNpsCompany(userId, firstDate,
                            lastDate, null, null, null, null, Integer.parseInt(op),
                            null, g.getId(), null, oracleConnection);
                    
                    Date date = FormatDateUtil.ownFormat.parse(firstDate != null
                            && !firstDate.equalsIgnoreCase("") ? firstDate : 
                            FormatDateUtil.ownFormat.format(new Date()));
                    Calendar calendar = Calendar.getInstance();
                    calendar.setTime(date);
                    calendar.add(Calendar.MONTH, -1);
                    calendar.set(Calendar.DATE,1);
                    
                    String first = FormatDateUtil.ownFormat.format(calendar.getTime());
                    
                    calendar.set(Calendar.DATE, calendar.getActualMaximum(Calendar.DAY_OF_MONTH));
                                        
                    String last = FormatDateUtil.ownFormat.format(calendar.getTime());
                    g.setNps(Float.toString(array1[3]));
                    
                    array1 = NpsHomeQuery.getNpsCompany(userId, first,
                            last, null, null, null, null, Integer.parseInt(op),
                            null, g.getId(), null, oracleConnection);
                    
                    g.setNpsP(Float.toString(array1[3]));
                    
                    g.setAreas(NpsHomeQuery.getNpsGlobalArea(userId, firstDate,
                            lastDate, null, null, null, null, Integer.parseInt(op),
                            null, g.getId(), null, oracleConnection));
                    g.setBusiness(NpsHomeQuery.getBusiness(g.getId(),firstDate,lastDate,
                            userId,oracleConnection));
                } catch (SQLException ex) {
                    Logger.getLogger(NpsControllerHelper.class.getName()).
                            log(Level.SEVERE, null, ex);
                } catch (ParseException ex) {
                    Logger.getLogger(NpsControllerHelper.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        }
        if (oracleConnection != null) {
            try {
                oracleConnection.close();
            } catch (SQLException ex) {
                Logger.getLogger(NpsControllerHelper.class.getName()).
                        log(Level.SEVERE, null, ex);
            }
        }
        return groups;
    }

    /**
     * Metodo de apoyo para obtener el detalle de los negocios segun los filtros
     * seleccioados
     * @param userId
     * @param firstDate
     * @param lastDate
     * @param zNps
     * @param zone
     * @param group
     * @param business
     * @param op
     * @param store
     * @param chief
     * @param man
     * @param section
     * @param type
     * @return 
     */
    
    public static List<Business> npsDetailBusinessHelper(String userId,
            String firstDate, String lastDate, String zNps, String zone,
            String group, String business, String op, String store,
            String chief, String man, String section, String type) {

        float array1[];

        Connection oracleConnection = null;
        oracleConnection = DbConnection.getDBConnection();
        
        List<Business> buness = NpsHomeQuery.getBusiness(group,firstDate,lastDate,
                userId,oracleConnection);
        
        if (op == null || op.equals("")) {
            op = "202";
        }
        if (buness != null) {
            for (Business g : buness) {

                try {
                    array1 = NpsHomeQuery.getNpsCompany(userId, firstDate,
                            lastDate, null, null, null, null, Integer.parseInt(op),
                            zone, null, g.getId(), oracleConnection);

                    g.setNps(Float.toString(array1[3]));
                    
                    Calendar calendar = Calendar.getInstance();
                    calendar.add(Calendar.MONTH, -1);
                    calendar.set(Calendar.DATE,1);
                    
                    String first = FormatDateUtil.ownFormat.format(calendar.getTime());
                    
                    calendar.set(Calendar.DATE, calendar.getActualMaximum(Calendar.DAY_OF_MONTH));
                                        
                    String last = FormatDateUtil.ownFormat.format(calendar.getTime());
                    
                    array1 = NpsHomeQuery.getNpsCompany(userId, first,
                            last, null, null, null, null, Integer.parseInt(op),
                            null, g.getId(), null, oracleConnection);
                    
                    g.setNpsP(Float.toString(array1[3]));
                    
                    g.setAreas(NpsHomeQuery.getNpsGlobalArea(userId, firstDate,
                            lastDate, null, null, null, null, Integer.parseInt(op),
                            zone, null, g.getId(), oracleConnection));

                } catch (SQLException ex) {
                    Logger.getLogger(NpsControllerHelper.class.getName()).
                            log(Level.SEVERE, null, ex);
                }
            }
        }
        if (oracleConnection != null) {
            try {
                oracleConnection.close();
            } catch (SQLException ex) {
                Logger.getLogger(NpsControllerHelper.class.getName()).
                        log(Level.SEVERE, null, ex);
            }
        }
        return buness;
    }

    /**
     * Metodo de apoyo para obtener las zonas de cada negocio segun los filtros
     * seleccionados
     * @param userId
     * @param firstDate
     * @param lastDate
     * @param zNps
     * @param zone
     * @param group
     * @param business
     * @param op
     * @param store
     * @param chief
     * @param man
     * @param section
     * @param type
     * @return 
     */
    
    public static NpsHomeJsonObject npsRankingHelper(String userId,
            String firstDate, String lastDate,
            String zNps, String zone, String group, String business, String op,
            String store, String chief, String man, String section, String type) {

        NpsHomeJsonObject zonesD = new NpsHomeJsonObject();
        List<Zone> zones = new ArrayList();
        Gson gson = new Gson();
        List<Zone> zonesStores = null;
        String strs[] = null;
        String chfs[] = null;
        String mans[] = null;
        String sctns[] = null;

        String zoneNps;
        Connection oracleConnection = null;
        try {

            oracleConnection = DbConnection.getDBConnection();
            if (op == null || op.equals("")) {
                op = "202";
            }
            if (zNps != null && !zNps.equals("[]") && !zNps.isEmpty()) {
                zonesStores = gson.fromJson(zNps, new TypeToken<List<Zone>>() {
                }.getType());
            }
            if (zonesStores != null) {
                zoneNps = getZoneString(zonesStores);
                //strs = getIdStores(zoneNps,zonesStores);
                //mans = getIdManagers(zoneNps,zonesStores);
                //chfs = getIdChiefs(zoneNps,zonesStores);
                //sctns = getSections(zoneNps,zonesStores);
            } else {
                if (store != null && !store.equalsIgnoreCase("")) {
                    strs = new String[]{store};
                } else if (chief != null && !chief.equalsIgnoreCase("")) {
                    chfs = new String[]{chief};
                }
            }
            if (chfs != null && chfs.length > 0) {
                strs = null;
            }

            zones = NpsHomeQuery.getNpsArea(userId, firstDate, lastDate, strs,
                    mans, chfs, sctns, Integer.parseInt(op), zone, group,
                    business, oracleConnection);

        } catch (SQLException ex) {
            Logger.getLogger(NPS.class
                    .getName()).log(Level.SEVERE, null, ex);
        } finally {
            if (oracleConnection != null) {
                try {
                    oracleConnection.close();
                } catch (SQLException ex) {
                    Logger.getLogger(NpsControllerHelper.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        }
        zonesD.setzAreas(zones);
        return zonesD;
    }

    /**
     * Metodo de apoyo para obtener el detalle de las ubicaciones segun los filtros
     * seleccionados
     * @param userId
     * @param firstDate
     * @param lastDate
     * @param zNps
     * @param zone
     * @param group
     * @param business
     * @param op
     * @param store
     * @param chief
     * @param man
     * @param section
     * @param type
     * @return 
     */
    
    public static List<Store> npsDetailStoresHelper(String userId,
            String firstDate, String lastDate,
            String zNps, String zone, String group, String business, String op,
            String store, String chief, String man, String section, String type) {

        List<Store> npsStores = new ArrayList<>();
        List<Store> npsStoresTmp = new ArrayList<>();
        List<Zone> zonesStores = null;
        String[] strs = null;
        String[] chfs = null;
        String[] mns = null;
        String[] sctns = null;

        Connection oracleConnection = null;
        try {

            oracleConnection = DbConnection.getDBConnection();
            if (op == null || op.equals("")) {
                op = "202";
            }
            if (zNps != null && !zNps.equals("[]") && !zNps.isEmpty()) {
                Gson gson = new Gson();
                zonesStores = gson.fromJson(zNps, new TypeToken<List<Zone>>() {
                }.getType());
                List<String> zones = new ArrayList<>();
                List<String> stores = new ArrayList<>();
                if (zonesStores != null) {
                    zones = getIdZones(zonesStores);
                    stores = getIdStoresD(zonesStores);
//                    if( chfs.length > 0 ){
//                        strs = null;
//                    }
                }
                for (String z : zones) {
                    npsStoresTmp.addAll(NpsHomeQuery.getNpsStore(userId, firstDate, lastDate,
                            null, mns, chfs, sctns, Integer.parseInt(op),
                            z, group, business, oracleConnection));
                }
                if (!stores.isEmpty()) {
                    for (Store st : npsStoresTmp) {
                        if (stores.contains(st.getId())) {
                            npsStores.add(st);
                        }
                    }
                } else {
                    npsStores = npsStoresTmp;
                }

            } else {
                npsStores = NpsHomeQuery.getNpsStore(userId, firstDate, lastDate,
                        strs, mns, chfs, sctns, Integer.parseInt(op),
                        zone, group, business, oracleConnection);

            }
        } catch (SQLException ex) {
            Logger.getLogger(NPS.class
                    .getName()).log(Level.SEVERE, null, ex);
        } finally {
            if (oracleConnection != null) {
                try {
                    oracleConnection.close();
                } catch (SQLException ex) {
                    Logger.getLogger(NpsControllerHelper.class.getName()).
                            log(Level.SEVERE, null, ex);
                }
            }
        }

        return npsStores;
    }

    /**
     * Metodo de apoyo para obtener el detalle de los gerentes seguns los filtros
     * seleccionados
     * @param userId
     * @param firstDate
     * @param lastDate
     * @param zNps
     * @param zone
     * @param group
     * @param business
     * @param op
     * @param store
     * @param chief
     * @param man
     * @param section
     * @param type
     * @return 
     */
    
    public static List<Manager> npsDetailManagerHelper(String userId,
            String firstDate, String lastDate, String zNps, String zone,
            String group, String business, String op, String store,
            String chief, String man, String section, String type) {

        List<Manager> npsManagers = null;
        List<Zone> zonesStores = null;
        String[] chfs = null;
        Connection oracleConnection = null;

        try {

            oracleConnection = DbConnection.getDBConnection();

            if (store == null || store.equalsIgnoreCase("")) {

                if (zNps != null && !zNps.equals("[]") && !zNps.isEmpty()) {

                    Gson gson = new Gson();
                    zonesStores = gson.fromJson(zNps, new TypeToken<List<Zone>>() {
                    }.getType());

                    if (zonesStores != null) {
                        //strs = getIdStores(zonesStores);
                        //                   chfs = getIdChiefs(zonesStores);
                        //                    if( chfs.length > 0 ){
                        //                        strs = null;
                        //                    }
                    }
                    npsManagers = NpsHomeQuery.getNpsManager(userId, firstDate,
                            lastDate, null, null, chfs, null,
                            Integer.parseInt(op), zone, group, business, oracleConnection);
                }
            } else {
                npsManagers = NpsHomeQuery.getNpsManager(userId, firstDate,
                        lastDate, store.split(","), null, chfs, null,
                        Integer.parseInt(op), zone, group, business, oracleConnection);

            }

        } catch (SQLException ex) {
            Logger.getLogger(NPS.class
                    .getName()).log(Level.SEVERE, null, ex);
        } finally {
            if (oracleConnection != null) {
                try {
                    oracleConnection.close();
                } catch (SQLException ex) {
                    Logger.getLogger(NpsControllerHelper.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        }

        return npsManagers;
    }

    /**
     * Metodo de apoyo para obtener el detalle de jefes segun los filtros 
     * seleccionados
     * @param userId
     * @param firstDate
     * @param lastDate
     * @param zNps
     * @param zone
     * @param group
     * @param business
     * @param op
     * @param store
     * @param chief
     * @param man
     * @param section
     * @param type
     * @return 
     */
    
    public static List<Chief> npsDetailChiefHelper(String userId,
            String firstDate, String lastDate,
            String zNps, String zone, String group, String business, String op,
            String store, String chief, String man, String section, String type) {

        List<Chief> npsChiefs = null;
        List<Zone> zonesStores = null;
        String strs[] = null;
        String chfs[] = null;
        String mans[] = null;
        String sctns[] = null;
        Connection oracleConnection = null;

        try {

            oracleConnection = DbConnection.getDBConnection();

            if (man == null || man.equalsIgnoreCase("")) {
                if (zNps != null && !zNps.equals("[]") && !zNps.isEmpty()) {

                    Gson gson = new Gson();
                    zonesStores = gson.fromJson(zNps,
                            new TypeToken<List<Zone>>() {
                            }.getType());

                    if (zonesStores != null) {
                        //strs = getIdStores(zonesStores);
                        //                   chfs = getIdChiefs(zonesStores);
                        //                    if( chfs.length > 0 ){
                        //                        strs = null;
                        //                    }
                    }
                    npsChiefs = NpsHomeQuery.getNpsChief(userId, firstDate, lastDate,
                            null, chfs, null, null, Integer.parseInt(op),
                            zone, group, business, oracleConnection);
                }
            } else {
                if (store != null && !store.equalsIgnoreCase("")) {
                    strs = new String[]{store};
                } else if (chief != null && !chief.equalsIgnoreCase("")) {
                    chfs = new String[]{chief};
                } else if (section != null && !section.equalsIgnoreCase("")) {
                    sctns = new String[]{section};
                }
                npsChiefs = NpsHomeQuery.getNpsChief(userId, firstDate, lastDate,
                        strs, man.split(","), chfs, sctns, Integer.parseInt(op),
                        zone, group, business, oracleConnection);

            }

        } catch (SQLException ex) {
            Logger.getLogger(NPS.class
                    .getName()).log(Level.SEVERE, null, ex);
        } finally {
            if (oracleConnection != null) {
                try {
                    oracleConnection.close();
                } catch (SQLException ex) {
                    Logger.getLogger(NpsControllerHelper.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        }

        return npsChiefs;
    }

    /**
     * Metodo de apoyo para obtener el detalle de Secciones segun los filtros
     * seleccionados
     * @param userId
     * @param firstDate
     * @param lastDate
     * @param zNps
     * @param zone
     * @param group
     * @param business
     * @param op
     * @param store
     * @param chief
     * @param man
     * @param section
     * @param type
     * @return 
     */
    
    public static List<Section> npsDetailSectionHelper(String userId,
            String firstDate, String lastDate,
            String zNps, String zone, String group, String business, String op,
            String store, String chief, String man, String section, String type) {

        List<Section> sections = null;
        List<Zone> zonesStores = null;
        String strs[] = null;
        String chfs[] = null;
        String mans[] = null;
        String sctns[] = null;
        Connection oracleConnection = null;
        try {

            oracleConnection = DbConnection.getDBConnection();
            if (chief == null || chief.equalsIgnoreCase("")) {
                if (zNps != null && !zNps.equals("[]") && !zNps.isEmpty()) {
                    Gson gson = new Gson();
                    zonesStores = gson.fromJson(zNps, new TypeToken<List<Zone>>() {
                    }.getType());

                    if (zonesStores != null) {
                        //strs = getIdStores(zonesStores);
                        getIdChiefs(zonesStores);
                        //                    if( chfs.length > 0 ){
                        //                        strs = null;
                        //                    }
                    }

                    sections = new ArrayList<>();
                    for (Zone z : zonesStores) {
                        if (z.getStores() != null) {
                            for (Store s : z.getStores()) {
                                sections.addAll(NpsHomeQuery.getNpsSections(
                                        userId, firstDate, lastDate, new String[]{s.getId()},
                                        null, s.getIds().toArray(new String[s.getIds().size()]),
                                        null, Integer.parseInt(op),
                                        zone, group, business, oracleConnection));
                            }
                        }
                    }

                }
            } else {
                if (store != null && !store.equalsIgnoreCase("")) {
                    strs = new String[]{store};
                }
                if (man != null && !man.equalsIgnoreCase("")) {
                    mans = new String[]{man};
                }
                if (section != null && !section.equalsIgnoreCase("")) {
                    sctns = new String[]{section};
                }
                sections = NpsHomeQuery.getNpsSections(userId, firstDate, lastDate,
                        strs, mans, chief.split(","), sctns, Integer.parseInt(op),
                        zone, group, business, oracleConnection);

            }

        } catch (SQLException ex) {
            Logger.getLogger(NPS.class
                    .getName()).log(Level.SEVERE, null, ex);
        } finally {
            if (oracleConnection != null) {
                try {
                    oracleConnection.close();
                } catch (SQLException ex) {
                    Logger.getLogger(NpsControllerHelper.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        }

        return sections;
    }

    /**
     * Metodo de apoyo para obtener el detalle de vendedores segun los filtros
     * seleccionados
     * @param userId
     * @param firstDate
     * @param lastDate
     * @param zNps
     * @param zone
     * @param group
     * @param business
     * @param op
     * @param store
     * @param chief
     * @param man
     * @param section
     * @param type
     * @return 
     */
    
    public static List<Seller> npsDetailSellerHelper(String userId,
            String firstDate, String lastDate,
            String zNps, String zone, String group, String business, String op,
            String store, String chief, String man, String section, String type) {

        List<Seller> sellers = null;
        List<Zone> zonesStores = null;
        String strs[] = null;
        String chfs[] = null;
        String mans[] = null;
        String sctns[] = null;
        Connection oracleConnection = null;
        try {

            oracleConnection = DbConnection.getDBConnection();
            if (section == null || section.equalsIgnoreCase("")) {
                if (zNps != null && !zNps.equals("[]") && !zNps.isEmpty()) {
                    Gson gson = new Gson();
                    zonesStores = gson.fromJson(zNps, new TypeToken<List<Zone>>() {
                    }.getType());

                    if (zonesStores != null) {
                        //strs = getIdStores(zonesStores);
                        //                    chfs = getIdChiefs(zonesStores);
                        //                    if( chfs.length > 0 ){
                        //                        strs = null;
                        //                    }
                    }
                    sellers = NpsHomeQuery.getNpsSellers(userId, firstDate, lastDate,
                            null, new String[]{chief}, null, null, Integer.parseInt(op),
                            zone, group, business, oracleConnection);
                }
            } else {
                if (store != null && !store.equalsIgnoreCase("")) {
                    strs = new String[]{store};
                }
                if (man != null && !man.equalsIgnoreCase("")) {
                    mans = new String[]{man};
                } else if (chief != null && !chief.equalsIgnoreCase("")) {
                    chfs = new String[]{chief};
                }
                sellers = NpsHomeQuery.getNpsSellers(userId, firstDate, lastDate,
                        strs, mans, chfs, section.split(","), Integer.parseInt(op),
                        zone, group, business, oracleConnection);

            }

        } catch (SQLException ex) {
            Logger.getLogger(NPS.class
                    .getName()).log(Level.SEVERE, null, ex);
        } finally {
            if (oracleConnection != null) {
                try {
                    oracleConnection.close();
                } catch (SQLException ex) {
                    Logger.getLogger(NpsControllerHelper.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        }

        return sellers;
    }

    /**
     * Metodo de apoyo para obtener las zonas segun el negocio y grupo
     * @param zones
     * @return 
     */
    
    public static List<String> getIdZones(List<Zone> zones) {
        List<String> zonesIds = new ArrayList<>();
        for (Zone z : zones) {
            zonesIds.add(z.getId());
        }
        return zonesIds;
    }

    /**
     * Metodo de apoyo para obtener los ids de las zonas
     * @param zones 
     */
    
    public static void getIdStores(List<Zone> zones) {
        List<String> storesIds = new ArrayList<>();
        for (Zone z : zones) {
            if (z.getStores() != null && !z.getStores().isEmpty()) {
                for (Store s : z.getStores()) {
                    storesIds.add(s.getId());
                }
            }
            z.setIds(storesIds);
        }
        //return storesIds.toArray(new String[storesIds.size()]);
    }

    /**
     * MEtodo de apoyo para obtener los id de las zonas 
     * @param zones
     * @return 
     */
    
    public static List<String> getIdStoresD(List<Zone> zones) {
        List<String> storesIds = new ArrayList<>();
        for (Zone z : zones) {
            if (z.getStores() != null && !z.getStores().isEmpty()) {
                for (Store s : z.getStores()) {
                    storesIds.add(s.getId());
                }
            }
            //z.setIds(storesIds);
        }
        return storesIds;
    }

    /**
     * MEtodo para obtener los ids de los Gerentes
     * @param zone
     * @param zones
     * @return 
     */
    
    public static String[] getIdManagers(String zone, List<Zone> zones) {
        List<String> mansIds = new ArrayList<>();
        for (Zone z : zones) {
            if (z.getDesc().equalsIgnoreCase(zone)) {
                for (Store s : z.getStores()) {
                    if (s.getManagers() != null && !s.getManagers().isEmpty()) {
                        for (Manager m : s.getManagers()) {
                            mansIds.add(s.getId());
                        }
                    }
                }
            }
        }
        return mansIds.toArray(new String[mansIds.size()]);
    }

    /**
     * Metodo de apoyo para obtener las secciones segun las zonas
     * @param zNps
     * @return 
     */
    
    public static List<Zone> getSections(String zNps) {

        List<Zone> zonesStores = null;
        if (zNps != null && !zNps.equals("[]") && !zNps.isEmpty()) {
            Gson gson = new Gson();
            zonesStores = gson.fromJson(zNps, new TypeToken<List<Zone>>() {
            }.getType());

            for (Zone zone : zonesStores) {
                if (zone.getStores() != null) {
                    for (Store s : zone.getStores()) {
                        if (s.getManagers() != null && !s.getManagers().isEmpty()) {
                            for (Manager m : s.getManagers()) {
                                if (m.getChiefs() != null && !m.getChiefs().isEmpty()) {
                                    for (Chief ch : m.getChiefs()) {
                                        List<Section> sections = NpsHomeQuery.getSections(ch.getId(), s.getId());
                                        if (sections != null && !sections.isEmpty()) {
                                            ch.setDesc(sections.get(0).getParent());
                                        }

                                        ch.setSections(sections);
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }

        return zonesStores;
    }

    /**
     * MEtodo de apoyo para obtener las secciones segun las zonas
     *
     * @param zone
     * @param zones
     * @return 
     */
    
    public static String[] getSections(String zone, List<Zone> zones) {
        List<String> secsIds = new ArrayList<>();
        for (Zone z : zones) {
            if (z.getDesc().equalsIgnoreCase(zone)) {
                for (Store s : z.getStores()) {
                    if (s.getManagers() != null && !s.getManagers().isEmpty()) {
                        for (Manager m : s.getManagers()) {
                            if (m.getChiefs() != null && !m.getChiefs().isEmpty()) {
                                for (Chief ch : m.getChiefs()) {
                                    if (ch.getSections() != null && ch.getSections().isEmpty()) {
                                        for (Section sec : ch.getSections()) {
                                            secsIds.add(sec.getId());
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
        return secsIds.toArray(new String[secsIds.size()]);
    }

    /**
     * MEtodo de apoyo para obtener los ids de Jefes segun las zonas
     * @param zones 
     */
    
    public static void getIdChiefs(List<Zone> zones) {

        for (Zone z : zones) {

            if (z.getStores() != null && !z.getStores().isEmpty()) {
                for (Store s : z.getStores()) {
                    List<String> chiefsIds = new ArrayList<>();
                    if (s.getManagers() != null && !s.getManagers().isEmpty()) {
                        for (Manager m : s.getManagers()) {
                            if (m.getChiefs() != null && !m.getChiefs().isEmpty()) {

                                for (Chief ch : m.getChiefs()) {
                                    chiefsIds.add(ch.getId());
                                }
                            }
                        }
                    }
                    s.setIds(chiefsIds);
                }

            }
        }
        //return chiefsIds.toArray(new String[chiefsIds.size()]);
    }

    public static String getZoneString(List<Zone> zones) {
        for (Zone zone : zones) {
            if (zone.getDesc() != null) {
                return zone.getId();
            }
        }
        return "";
    }

    /**
     * Metodo de apoyo para obtener el ranking for de NPS
     * @param userId
     * @param firstDate
     * @param lastDate
     * @param zone
     * @param group
     * @param business
     * @param op
     * @param store
     * @param rankear
     * @param zNps
     * @param mans
     * @param chfs
     * @param sections
     * @return 
     */
    
    public static NpsHomeJsonObject npsDetailRankearHelper(String userId,
            String firstDate, String lastDate, String zone, String group,
            String business, String op, String store, String rankear,
            String zNps, String mans, String chfs, String sections) {

        NpsHomeJsonObject rankearFor = new NpsHomeJsonObject();
        List<Manager> managers = null;
        List<Chief> chiefs = null;
        List<Store> stores = null;
        List<Seller> sellers = null;
       
        Connection oracleConnection = null;

        try {

            oracleConnection = DbConnection.getDBConnection();

            if (rankear != null && !rankear.equalsIgnoreCase("")) {
                if (rankear.equalsIgnoreCase("STORE")) {

                    stores = NpsHomeQuery.getNpsRankingForStore(userId, firstDate,
                            lastDate, store != null && !store.equals("") ? store.split(",") : null, 
                            mans != null && !mans.equals("") ? mans.split(",") : null, 
                            chfs != null && !chfs.equals("") ? chfs.split(",") : null, 
                            sections != null && !sections.equals("") ? sections.split(",") : null,
                            Integer.parseInt(op), zone.split(","), group, business,
                            oracleConnection, rankear);
                    rankearFor.setStores(stores);
                } else if (rankear.equalsIgnoreCase("MANAGER")) {

                    managers = NpsHomeQuery.getNpsRankingForManager(userId, firstDate,
                            lastDate, store != null && !store.equals("") ? store.split(",") : null,
                            mans != null && !mans.equals("") ? mans.split(",") : null, 
                            chfs != null && !chfs.equals("") ? chfs.split(",") : null, 
                            sections != null && !sections.equals("") ? sections.split(",") : null,
                            Integer.parseInt(op), zone != null && !zone.equals("") 
                            ? zone.split(",") : null, group, business,oracleConnection, rankear);

                    rankearFor.setMans(managers);

                } else if (rankear.equalsIgnoreCase("CHIEF")) {

                    chiefs = NpsHomeQuery.getNpsRankingForChief(userId, firstDate,
                            lastDate, store != null && !store.equals("") ? store.split(",") : null,
                            mans != null && !mans.equals("") ? mans.split(",") : null, 
                            chfs != null && !chfs.equals("") ? chfs.split(",") : null, 
                            sections != null && !sections.equals("") ? sections.split(",") : null,
                            Integer.parseInt(op), zone != null && !zone.equals("") 
                            ? zone.split(",") : null, group, business,oracleConnection, rankear);
                            
                    rankearFor.setChfs(chiefs);

                } else if (rankear.equalsIgnoreCase("SELLER")) {
                    
                    sellers = 
                            NpsHomeQuery.getNpsRankingForSeller(userId, firstDate,
                            lastDate, store != null && !store.equals("") ? store.split(",") : null,
                            mans != null && !mans.equals("") ? mans.split(",") : null, 
                            chfs != null && !chfs.equals("") ? chfs.split(",") : null, 
                            sections != null && !sections.equals("") ? sections.split(",") : null,
                            Integer.parseInt(op), zone != null && !zone.equals("") 
                            ? zone.split(",") : null, group, business,oracleConnection, rankear);
                       
                    rankearFor.setSells(sellers);
                }
            }
            rankearFor.setType(rankear);
        } catch (SQLException ex) {
            Logger.getLogger(NPS.class
                    .getName()).log(Level.SEVERE, null, ex);
        } finally {
            if (oracleConnection != null) {
                try {
                    oracleConnection.close();
                } catch (SQLException ex) {
                    Logger.getLogger(NpsControllerHelper.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        }

        return rankearFor;
    }

    /**
     * Metodo de apoyo para obtener las comparativas de JEFES
     * @param userId
     * @param firstDate
     * @param lastDate
     * @param zNps
     * @param zone
     * @param group
     * @param business
     * @param op
     * @param store
     * @param chief
     * @param man
     * @param section
     * @param type
     * @return 
     */
    
    public static List<Chief> npsCompireChiefsHelper(String userId, String firstDate, String lastDate, String zNps, String zone, String group,
            String business, String op, String store, String chief, String man,
            String section, String type) {

        List<Chief> chiefs = new ArrayList<>();
        List<Chief> chiefsTmp = new ArrayList<>();
        List<String> idChiefs = new ArrayList<>();;
        List<String> idMans = new ArrayList<>();
        List<Zone> zonesStores = null;
        Connection oracleConnection = null;

        try {

            oracleConnection = DbConnection.getDBConnection();

            if (zNps != null && !zNps.equals("[]") && !zNps.isEmpty()) {

                Gson gson = new Gson();
                zonesStores = gson.fromJson(zNps, new TypeToken<List<Zone>>() {
                }
                        .getType());

                if (zonesStores != null) {
                    for (Zone z : zonesStores) {
                        if (z.getStores() != null) {
                            for (Store s : z.getStores()) {
                                if (s.getManagers() != null) {
                                    for (Manager m : s.getManagers()) {
                                        idMans.add(m.getId());
                                        if (m.getChiefs() != null) {
                                            for (Chief c : m.getChiefs()) {
                                                idChiefs.add(c.getId());
                                            }
                                        }
                                    }
                                    if (!idMans.isEmpty()) {
                                        chiefsTmp.addAll(NpsHomeQuery.
                                                getNpsChief(userId, firstDate,
                                                        lastDate, s.getId().split(","),
                                                        idMans.toArray(new String[idMans.
                                                                size()]), null, null, Integer.
                                                                parseInt(op), null, group, business,
                                                        oracleConnection));
                                    }
                                }
                                idMans.clear();
                            }
                        }
                    }
                }

                if (!idChiefs.isEmpty()) {
                    for (Chief chf : chiefsTmp) {
                        if (idChiefs.contains(chf.getId())) {
                            chiefs.add(chf);
                        }
                    }
                }
            }
        } catch (SQLException ex) {
            Logger.getLogger(NPS.class
                    .getName()).log(Level.SEVERE, null, ex);
        } finally {
            if (oracleConnection != null) {
                try {
                    oracleConnection.close();
                } catch (SQLException ex) {
                    Logger.getLogger(NpsControllerHelper.class.getName()).
                            log(Level.SEVERE, null, ex);
                }
            }
        }

        return chiefs;
    }

    /**
     * MEtodo de apoyo para obtener la comparativa de secciones
     * @param userId
     * @param firstDate
     * @param lastDate
     * @param zNps
     * @param zone
     * @param group
     * @param business
     * @param op
     * @param store
     * @param chief
     * @param man
     * @param section
     * @param type
     * @return 
     */
    
    public static List<Section> npsCompireSectionsHelper(String userId,
            String firstDate, String lastDate, String zNps, String zone, String group,
            String business, String op, String store, String chief, String man,
            String section, String type) {

        List<Section> sectionsTmp = new ArrayList<>();
        List<Section> sections = new ArrayList<>();
        List<String> idSections = new ArrayList<>();
        List<String> idChiefs = new ArrayList<>();
        List<String> idMans = new ArrayList<>();
        List<Zone> zonesStores = null;
        Connection oracleConnection = null;

        try {

            oracleConnection = DbConnection.getDBConnection();

            if (zNps != null && !zNps.equals("[]") && !zNps.isEmpty()) {

                Gson gson = new Gson();
                zonesStores = gson.fromJson(zNps, new TypeToken<List<Zone>>() {
                }
                        .getType());

                if (zonesStores != null) {
                    for (Zone z : zonesStores) {
                        if (z.getStores() != null) {
                            for (Store s : z.getStores()) {
                                if (s.getManagers() != null) {
                                    for (Manager m : s.getManagers()) {

                                        if (m.getChiefs() != null) {
                                            for (Chief c : m.getChiefs()) {
                                                if (c.getSections() != null) {
                                                    for (Section sec : c.getSections()) {
                                                        idSections.add(sec.getId());
                                                    }

                                                    sectionsTmp.addAll(
                                                            NpsHomeQuery
                                                                    .getNpsSections(userId,
                                                                            firstDate, lastDate, s.getId().split(","),
                                                                            m.getId().split(","),
                                                                            c.getId().split(","), null,
                                                                            Integer.parseInt(op), null,
                                                                            group, business,
                                                                            oracleConnection)
                                                    );
                                                    if (!idSections.isEmpty()) {
                                                        for (Section sec : sectionsTmp) {
                                                            if (idSections.
                                                                    contains(sec.getId())) {
                                                                if (!sections.contains(sec)) {
                                                                    sections.add(sec);
                                                                }
                                                            }
                                                        }
                                                    }
                                                }
                                            }
                                            idSections.clear();
                                            sectionsTmp.clear();
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
        } catch (SQLException ex) {
            Logger.getLogger(NPS.class
                    .getName()).log(Level.SEVERE, null, ex);
        } finally {
            if (oracleConnection != null) {
                try {
                    oracleConnection.close();
                } catch (SQLException ex) {
                    Logger.getLogger(NpsControllerHelper.class.getName()).
                            log(Level.SEVERE, null, ex);
                }
            }
        }

        return sections;
    }

}
