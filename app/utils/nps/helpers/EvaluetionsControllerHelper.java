/**
 * @(#) EvaluationsControllerHelper 8/06/2017
 */
package utils.nps.helpers;

import SchedulerTask.nps.LoadNPSStructureJob;
import com.jcraft.jsch.JSchException;
import com.jcraft.jsch.SftpException;
import controllers.Evaluations;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.sql.SQLException;
import java.text.ParseException;
import java.util.Date;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import models.NpsAnswer;
import models.NpsQuestionAnswer;
import models.nps.Challenge;
import models.nps.ChallengeUtil;
import models.nps.NPS_KEY_CHALLENGE;
import models.nps.NPS_KEY_TRACING;
import models.nps.Tracing;
import play.Play;
import querys.nps.EvaluationDetailsQuery;
import querys.nps.NpsHomeQuery;
import utils.nps.model.ChallengeModel;
import utils.nps.model.EvaluationsModel;
import utils.nps.model.NpsAnswerTmp;
import utils.nps.model.TracingModelHelper;
import utils.nps.sftp.SFTPConnection;
import utils.nps.util.FormatDateUtil;
import utils.nps.vars.EnvironmentVar;

/**
 * Clase de ayuda para el procesamiento de las evaluaciones.
 * @author Rodolfo Miranda -- Qualtop
 */
public class EvaluetionsControllerHelper {

    /**
     * Metodo para obtener las evaluaciones segun los filtros seleccionados
     * @param userId
     * @param firstDate
     * @param lastDate
     * @param store
     * @param manager
     * @param chief
     * @param section
     * @param zone
     * @param group
     * @param business
     * @param ini
     * @param end
     * @param flag
     * @param column
     * @param order
     * @param employeeId
     * @return 
     */
    public static EvaluationsModel getEvaluationsHelper(String userId, String firstDate,
            String lastDate, String store, String manager, String chief,
            String section, String zone, String group, String business,
            int ini, int end, int flag, int column, String order, String employeeId) {

        EvaluationsModel model = null;

        try {
            //Llamado a la funcion oracle
            model = EvaluationDetailsQuery.getEvaluations(userId, firstDate,
                    lastDate, store != null ? store.split(",") : null,
                    manager != null ? manager.split(",") : null,
                    chief != null ? chief.split(",") : null,
                    section != null ? section.split(",") : null,
                    zone, group, business, ini, end, flag, column, order, employeeId);

        } catch (SQLException ex) {
            java.util.logging.Logger.getLogger(Evaluations.class.getName()).
                    log(Level.SEVERE, null, ex);
        } catch (ParseException ex) {
            java.util.logging.Logger.getLogger(Evaluations.class.getName()).
                    log(Level.SEVERE, null, ex);
        }

        return model;
    }

    /**
     * Metodo para obtener la descarga de evaluaciones segun los filtros 
     * seleccionados
     * @param userId
     * @param firstDate
     * @param lastDate
     * @param store
     * @param manager
     * @param chief
     * @param section
     * @param zone
     * @param group
     * @param business
     * @param flag
     * @param search
     * @return 
     */
    public static List getEvaluationsXLSHelper(String userId, String firstDate,
            String lastDate, String store, String manager, String chief,
            String section, String zone, String group, String business,
            int flag, String search) {

        List model = null;

        try {
            //Llamado a la funcion ORACLE
            model = EvaluationDetailsQuery.getEvaluationsXLS(userId, firstDate,
                    lastDate, store != null ? store.split(",") : null,
                    manager != null ? manager.split(",") : null,
                    chief != null ? chief.split(",") : null,
                    section != null ? section.split(",") : null,
                    zone, group, business, flag,search);

        } catch (SQLException ex) {
            java.util.logging.Logger.getLogger(Evaluations.class.getName()).
                    log(Level.SEVERE, null, ex);
        } catch (ParseException ex) {
            java.util.logging.Logger.getLogger(Evaluations.class.getName()).
                    log(Level.SEVERE, null, ex);
        }

        return model;
    }

    /**
     * Metodo para el manejo del Seguimiento en el detalle de Evaluaciones
     * @param rowId
     * @param operation
     * @param text
     * @param idFollow
     * @param userId
     * @param userName
     * @return 
     */
    
    public static TracingModelHelper changeTraicingDetail(String rowId, String operation,
            String text, String idFollow, String userId, String userName) {

        int status = idFollow != null && !idFollow.equals("")
                ? Integer.parseInt(idFollow) : 0;

        if (operation != null && !operation.equals("")) {
            try {

                if (operation.equals("process")) {
                    status = 2;
                } else if (operation.equals("closed")) {
                    status = 3;
                }
                Tracing tracing = new Tracing();
                tracing.key = new NPS_KEY_TRACING();
                tracing.key.idAnswer = Integer.parseInt(rowId);
                tracing.key.idFollow = EvaluationDetailsQuery.getNextValTracing();
                tracing.status = status;
                tracing.text = text;
                tracing.userName = userName;
                tracing.date = FormatDateUtil.ownFormat.
                        parse(FormatDateUtil.ownFormat.format(new Date()));
                tracing.title = NpsHomeQuery.getTitleUserID(userId);
                tracing.save();
                //NpsAnswer answer = NpsAnswer.findById(Integer.parseInt(rowId));

                NpsAnswer answer = NpsAnswer.findById(Long.parseLong(rowId));
                answer.idFollow = Long.valueOf(status);
                answer.save();

            } catch (ParseException ex) {
                java.util.logging.Logger.getLogger(Evaluations.class.getName()).log(Level.SEVERE, null, ex);
            }
        }

        List<Tracing> tracings = Tracing.find(
                "SELECT m FROM Tracing m WHERE m.key.idAnswer = ?"
                + "order by m.date desc",
                Integer.parseInt(rowId)).fetch();

        //NpsAnswer answer = NpsAnswer.find("idAnswer = ?1",Integer.parseInt(rowId) ).first();
        Object[] o = NpsAnswer.find("select m.clientId, m.clientName,m.ip,"
                + " m.phone,m.sku,m.email,m.skuDesc,m.doc, "
                + " m.terminal, m.idFollow from NpsAnswer m where "
                + " m.id = ?", Long.parseLong(rowId)).first();

        NpsAnswerTmp aT = new NpsAnswerTmp();
        aT.setIdAnswer(rowId);
        if (o[0] != null) {
            aT.setIdClient(o[0].toString());
        }
        if (o[1] != null) {
            aT.setNameClient(o[1].toString());
        }
        if (o[2] != null) {
            aT.setIp(o[2].toString());
        }
        if (o[3] != null) {
            aT.setPhone(o[3].toString());
        }
        if (o[4] != null) {
            aT.setSku(o[4].toString());
        }
        if (o[5] != null) {
            aT.setEmail(o[5].toString());
        }
        if (o[6] != null) {
            aT.setDescription(o[6].toString());
        }
        if (o[7] != null) {
            aT.setDoc(o[7].toString());
        }
        if (o[8] != null) {
            aT.setTerminal(o[8].toString());
        }
        if (o[9] != null) {
            aT.setIdFollow(o[9].toString());
        }
        TracingModelHelper model = new TracingModelHelper();
        model.setTracings(tracings);
        model.setaT(aT);

        return model;
    }

    /**
     * Metodo para el manejo de las Impugnaciones en el detalle de Evaluaciones
     * @param rowId
     * @param operation
     * @param text
     * @param status
     * @param practice
     * @param qualif1
     * @param qualif2
     * @param idEmployee
     * @param idArea
     * @param type
     * @param userId
     * @param userName
     * @return 
     */
    
    public static ChallengeModel changeChallengerDetail(String rowId, String operation,
            String text, String status, String practice, String qualif1, String qualif2,
            String idEmployee, String idArea, String type, String userId, String userName) {

        ChallengeModel challenge = new ChallengeModel();
        Integer newStatus = 0;
        String statusId = "";
        Date dateImpug = null;
        String type2 = "null";
        String original1 = "";
        String original2 = "";
        String original3 = "";
        String already   = "";

        List<Challenge> challenges = null;
        //Roles and Permission
        try {

            if (operation != null && !operation.equals("")) {
                Challenge newChallenge = new Challenge();
                newChallenge.key = new NPS_KEY_CHALLENGE();
                newChallenge.key.idAnswer = Long.parseLong(rowId);
                newChallenge.key.idImpugn = EvaluationDetailsQuery.getNextValChallenge();
                newChallenge.username = userName;
                newChallenge.date = FormatDateUtil.ownFormat2.
                        parse(FormatDateUtil.ownFormat2.format(new Date()));
                if (type.equalsIgnoreCase("3")) {
                    newChallenge.field1 = qualif1;
                    newChallenge.field2 = qualif2;
                } else if (type.equalsIgnoreCase("4")) {
                    newChallenge.field1 = idEmployee;
                    newChallenge.field2 = idArea;
                    newChallenge.field3 = practice;
                }
                newChallenge.type = type;
                newChallenge.text = text;
                switch (operation) {
                    case "impugnar":
                        newChallenge.idStatus = 4;
                        newStatus = newChallenge.idStatus;
                        break;
                    case "replica":
                        if (status.equalsIgnoreCase("6")) {
                            newChallenge.idStatus = 7;
                            newStatus = newChallenge.idStatus;
                        }
                        break;
                    case "procede":
                        if (status.equalsIgnoreCase("4") || status.equalsIgnoreCase("7")
                                || status.equalsIgnoreCase("9")) {
//                            if (status.equalsIgnoreCase("9")) {
//                                newChallenge.idStatus = 8;
//                            } 
//                            if (status.equalsIgnoreCase("7")) {
//                                newChallenge.idStatus = null;
//                            } 
//                            else if (status.equalsIgnoreCase("4")) {
//                                newChallenge.idStatus = null;
//                            }
                            
                            if (type.equalsIgnoreCase("2")) {
                                //Eliminar Evaluacion
                                if (!eliminateEvaluation(rowId)) {
                                    throw new Exception("Error al realizar operacion "
                                            + "de eliminacion de evaluación");
                                }
                                if (status.equalsIgnoreCase("7")) {
                                    newChallenge.idStatus = 8;
                                    
                                } 
                                    else if (status.equalsIgnoreCase("4")) {
                                    newChallenge.idStatus = 5;
                                   
                                }
                                
                            } else if (type.equalsIgnoreCase("3")) {
                                //Cambiar calificacion
                                if (!changeQualifsEvaluation(rowId, qualif1, qualif2,newChallenge)) {
                                    throw new Exception("Error al realizar operacion "
                                            + "cambio de calificación de la evaluación");
                                }
                                if (status.equalsIgnoreCase("7")) {
                                    newChallenge.idStatus = 8;
                                    newChallenge.field3   = "Y";
                                } 
                                else if (status.equalsIgnoreCase("4")) {
                                    newChallenge.idStatus = 5;
                                    newChallenge.field3   = "Y";
                                }
                            } else if (type.equalsIgnoreCase("4")) {
                                //Cambiar Area y Practica
                                String error = changAreaPracticeId(rowId, idEmployee, idArea, practice,newChallenge);
                                if (status.equalsIgnoreCase("7")) {
                                    newChallenge.idStatus = null;
                                } 
                                else if (status.equalsIgnoreCase("4")) {
                                    newChallenge.idStatus = null;
                                }
                            }
                        }
                        newStatus = newChallenge.idStatus;
                        break;
                    case "no-procede":
                        if (status.equalsIgnoreCase("9")) {
                            newChallenge.idStatus = 10;
                            newStatus = newChallenge.idStatus;
                        } else if (status.equalsIgnoreCase("4")) {
                            newChallenge.idStatus = 6;
                            newStatus = newChallenge.idStatus;
                        } else if (status.equalsIgnoreCase("7")) {
                            newChallenge.idStatus = 9;
                            newStatus = newChallenge.idStatus;
                        }
                        break;
                    default:
                        break;
                }
                newChallenge.save();
                NpsAnswer answer = NpsAnswer.findById(Long.parseLong(rowId));
                answer.idImpugn = newStatus != null ? Long.valueOf(newStatus) : null;
                answer.save();
            }

            challenges = Challenge.find("SELECT m FROM Challenge m WHERE m.key.idAnswer = ?"
                    + "order by m.date desc",
                    Long.parseLong(rowId)).fetch();

            Challenge chall = Challenge.find("SELECT m FROM Challenge m WHERE m.key.idAnswer = ?"
                    + "and m.idStatus = 4 and m.date in (select max(s.date) from Challenge s where s.idStatus = 4 )",
                    Long.parseLong(rowId)).first();

            if (chall != null) {
                dateImpug = (Date) chall.date;
            }

            if( challenges != null && !challenges.isEmpty() ) {
                Challenge ch2 = challenges.get(0);
                
                type2 = ch2.type;

                type2 = type2 != null ? type2 : "null";

                statusId    = ch2.idStatus != null ? Integer.toString(ch2.idStatus) : null;
                already     = ch2.field3;
                                
                if (!type2.equals("") && !type2.equals("null")) {
                    if (type2.equals("3")) {
//                        if( statusId != null && ((statusId.equals("5") || statusId.equals("8")) ||
//                            (statusId.equals("6") && already.equals("Y")) )){
//                            original1 = ch2.field1;
//                            original2 = ch2.field2;
//                        }else{
                            qualif1   = ch2.field1;
                            qualif2   = ch2.field2;
//                        }
                    } else if (type2.equals("4")) {
//                        if( statusId == null && (already != null && already.equalsIgnoreCase("")) ){
//                            original1 = ch2.field1;
//                            original2 = ch2.field2;
//                            original3 = ch2.field3;
//                        }else{
                            idEmployee = ch2.field1;
                            idArea = ch2.field2;
                            practice = ch2.field3;
//                        }
                    }

                }
            }
            
            NpsAnswer answer = NpsAnswer.find("select m from NpsAnswer m where "
                    + "m.id = ?", Long.parseLong(rowId.trim())).first();

            if (answer != null) {
                Long obj = answer.idImpugn;

                statusId = obj != null ? Integer.toString(obj.intValue()) : "0";
                if (!type2.equals("") && !type2.equals("null")) {
                    if (type2.equals("3")) {
                        if (answer.questionsAnswers != null) {
                            for (NpsQuestionAnswer question : answer.questionsAnswers) {
                                if (question.idQuestion == 201) {
                                    if( ( statusId.equals("6") && already != null && already.equals("Y") )
                                            || statusId.equals("8") )
                                        qualif1 = question.textAnswer;
                                    else
                                        original1 = question.textAnswer;
                                } else if (question.idQuestion == 202) {
                                    if( ( statusId.equals("6") && already != null && already.equals("Y") ) 
                                            || statusId.equals("8") )
                                        qualif2 = question.textAnswer;
                                    else
                                        original2 = question.textAnswer;
                                }
                            }
                        }
                    } else if (type2.equals("4")) {
                    
                        if( statusId.equals("0") && (already != null && already.equalsIgnoreCase("")) ){
                            idEmployee = Long.toString(answer.idEmployeeNumber);
                            idArea = answer.area;
                            practice = answer.practice;
                        }else{
                            original1 = Long.toString(answer.idEmployeeNumber);
                            original2 = answer.area;
                            original3 = answer.practice;
                        }
                    }
                }else{
                    idArea = answer.area;
                    if (answer.questionsAnswers != null) {
                            for (NpsQuestionAnswer question : answer.questionsAnswers) {
                                if (question.idQuestion == 201) {
                                    qualif1 = question.textAnswer;
                                } else if (question.idQuestion == 202) {
                                    qualif2 = question.textAnswer;
                                }
                            }
                        }
                }
            }

            challenge.setIdAnswer(rowId);
            challenge.setStatus(statusId);
            challenge.setChallenges(challenges);
            challenge.setDateImpugn(dateImpug);
            challenge.setType(type != null ? type : type2);
            challenge.setIdEmployee(idEmployee != null && !idEmployee.equals("-") ? idEmployee : "null");
            challenge.setIdArea(idArea != null && !idArea.equals("-") ? idArea.toUpperCase() : "null");
            challenge.setIdPractice(practice != null && !practice.equals("-") ? practice : "null");
            challenge.setQualif1(qualif1 != null && !qualif1.equals("-") ? qualif1 : "null");
            challenge.setQualif2(qualif2 != null && !qualif2.equals("-") ? qualif2 : "null");
            challenge.setOriginal1(original1);
            challenge.setOriginal2(original2);
            challenge.setOriginal3(original3);
        } catch (ParseException ex) {
            java.util.logging.Logger.getLogger(Evaluations.class.getName()).log(Level.SEVERE, null, ex);
        } catch (Exception ex) {
            Logger.getLogger(EvaluetionsControllerHelper.class.getName()).log(Level.SEVERE, null, ex);
        }
        return challenge;
    }

    /**
     * Metodo de apoyo para la eliminacion de una evaluacion
     * @param idAnswer
     * @return 
     */
    
    public static boolean eliminateEvaluation(String idAnswer) {

        NpsAnswer answer = NpsAnswer.findById(Long.parseLong(idAnswer));
        if (answer != null) {
            answer.status = 0;
            answer.save();
            return true;
        }
        return false;
    }

    /**
     * Metodo de apoyo para el cambio de calificaciones en el detalle de 
     * Evaluaciones
     * @param idAnswer
     * @param qualif1
     * @param qualif2
     * @param chall
     * @return 
     */
    
    private static boolean changeQualifsEvaluation(String idAnswer, String qualif1,
            String qualif2, Challenge chall) {

        NpsAnswer answer = NpsAnswer.findById(Long.parseLong(idAnswer));
        if (answer != null) {
            
            for (NpsQuestionAnswer question : answer.questionsAnswers) {
                if (question.idQuestion == 201) {
                    chall.field1 = question.textAnswer;
                    question.textAnswer = qualif1;
                } else if (question.idQuestion == 202) {
                    chall.field2 = question.textAnswer;
                    question.textAnswer = qualif2;
                }
            }
           
            answer.save();
            return true;
        }

        return false;
    }

    /**
     * Metodo de apoyo para el cambio de id de empleado y/o area y/o practica
     * en el detalle de evaluaciones
     * @param idAnswer
     * @param idEmployee
     * @param idArea
     * @param practice
     * @param chall
     * @return 
     */
    
    private static String changAreaPracticeId(String idAnswer, String idEmployee,
            String idArea, String practice,Challenge chall) {

        ChallengeUtil util = EvaluationDetailsQuery.getInfoChangeUserId(idEmployee);
        
        if(util.getError() == null) {
        
            NpsAnswer answer = NpsAnswer.findById(Long.parseLong(idAnswer));
            if (answer != null) {
                chall.field2 = answer.area;
                answer.area = idArea;
                chall.field3 = answer.practice;
                answer.practice = practice;
                chall.field1 = Long.toString(answer.idEmployeeNumber);
                answer.idEmployeeNumber = Long.parseLong(idEmployee);
                // Update structure fields
                answer.userName = NpsHomeQuery.getFullNameUserID(Long.toString(util.getUserId()));
                answer.idStore = util.getIdStore();
                answer.zone = util.getZone();
                answer.userid = util.getUserId();
                answer.userIdChief = util.getChiefId();
                answer.userIdManager = util.getManagerId();
                answer.userIdDirector = util.getDirectorId();
                answer.userIdZone = util.getUserZone();
                answer.section = Integer.toString(util.getSection());
                
                answer.save();
                return null;
            }
        }

        return util.getError();
    }

    /**
     * Metodo de apoyo para la validacion del empleado en estructura
     * @param idEmployee
     * @return 
     */
    
    public static boolean validateIdEmp(String idEmployee) {
        
        ChallengeUtil util = EvaluationDetailsQuery.getInfoChangeUserId(idEmployee);
            if(util.getError() == null)
                return true;
            else
                return false;
    }
}
