/**
 * @(#) EnvironmentVar 16/03/2017
 */

package utils.nps.vars;

import play.Play;

/**
 * Clase de utilidad para el mapeo de las diferentes tipos de variables en el 
 * Sistema NPS
 * 
 * @author Rodolfo Miranda -- Qualtop
 */
public class EnvironmentVar {

    public static final String HOST = 
            Play.configuration.get("ftp.host").toString();
    public static final String USER = 
            Play.configuration.get("ftp.user").toString();
    public static final String PORT = 
            Play.configuration.get("ftp.port").toString();
    public static final String PASSWORD = 
            Play.configuration.get("ftp.pwd").toString();
    public static final String DIRECTORY = 
            Play.configuration.get("ftp.path.load.automatic.results").
                    toString();
    public static final String DIRECTORY_RECORDS = 
            Play.configuration.get("ftp.path.load.automatic.records").
                    toString();
    public static final String DIRECTORY_PROCE = 
            Play.configuration.get("ftp.path.load.automatic.procesados").
                    toString();
    public static final String DIRECTORY_ERROR = 
            Play.configuration.get("ftp.path.load.automatic.errores").
                    toString();
    
    public static final String DIRECTORY_STRUCT = 
            Play.configuration.get("ftp.path.load.automatic.struct").
                    toString();
    
    public static final String DIRECTORY_STRUCT_PRO = 
            Play.configuration.get("ftp.path.load.automatic.struct.procesados").
                    toString();
    
    public static final String DIRECTORY_STRUCT_ERR = 
            Play.configuration.get("ftp.path.load.automatic.struct.errores").
                    toString();
    public static final String MEDIA_DIRECTORY =
            Play.configuration.get("media.folder").
                    toString();
    
    public static final String TXTS_DIRECTORY =
            Play.configuration.get("txts.folder").
                    toString();
    
    public static final String VENTANA_DIRECTORY =
            Play.configuration.get("ftp.path.load.ventana").
                    toString();
    
    public static final int NUMLINE = 2;
    
    public static final String DELIMITER_CSV = ",";
    
    public static final String DELIMITER_TXT = "\\|";
    
    public static final String DELIMITER_TXTW = "|";
    
    public static final String CSV_EXT = "csv";
    
    public static final String TXT_EXT = "txt";
    
    public static final String XLS_EXT = "xls";
    
    public static final String XLSX_EXT = "xlsx";
    
    public static final String TXT_SUF = "INFO";
    
    public static final String CSV_SUF = "UserDirectory";
    
    public static final String XLS_SUF = "Secciones";
    
    public static final String VENTANA = "ventana.jpg";
    
    public static final String MODULE = "STRUC";
    
    public static final String LOAD_AUTO = "AUTO";
    
    public static final String LOAD_MANUAL = "MANUAL";
 
    public static final String DB_PLSQL = "DB";
    
    public static final String INS_NPS_ANSWER = "ANSWER";
    
    public static final String INS_NPS_STRUCT = "STRUCT";
    
    public static final int    CHALLENGE_STATUS = 4;
    
    public static final int    CHALLENGE_PROCEDE_STATUS = 5;
    
    public static final int    CHALLENGE_NOT_PROCEDE_STATUS = 6;
    
    public static final int    CHALLENGE_REPLY_STATUS = 7;
    
    public static final int    CHALLENGE_REPLY_PROCEDE_STATUS = 8;
    
    public static final int    CHALLENGE_REPLY_NOT_PROCEDE_STATUS = 9;
    
    public static final String DB_DRIVER = "oracle.jdbc.driver.OracleDriver";

    public static final int goal = 90;
}
