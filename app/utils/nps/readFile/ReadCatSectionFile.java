/**
 * @(#) ReadCatSectionFile 30/06/2017
 */
package utils.nps.readFile;

import com.opencsv.CSVReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import models.Sections;
import utils.nps.model.Section;
import utils.nps.util.FormatDateUtil;

/**
 *
 * @author Rodolfo Miranda -- Qualtop
 */
public class ReadCatSectionFile {

    private File file;
    private List<String> errores;

    public ReadCatSectionFile() {
        this.errores = new ArrayList<>();
    }

    public void setFileReader(File file) {
        this.file = file;
    }

    /**
     * Description : Method for Initialize the bufferedReader of file name on
     * FTP
     *
     * @return BufferedReader
     * @throws FileNotFoundException
     */
    private CSVReader getBufferReader() {
        if (file != null) {
            try {
                CSVReader br = null;
                br = new CSVReader(new InputStreamReader(new FileInputStream(file), "Cp1252"));
                return br;
            } catch (FileNotFoundException ex) {
                Logger.getLogger(ReadCsvFile.class.getName()).log(Level.SEVERE, null, ex);
            } catch (UnsupportedEncodingException ex) {
                Logger.getLogger(ReadCsvFile.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        return null;

    }

    /**
     * Metodo principal para la lectura de los registros
     * @param userId
     * @return 
     */
    public List<Sections> readCvsFile(String userId) {
        String[] line;

        boolean valid = true;
        List<Sections> sections = new ArrayList<>();
        CSVReader br = getBufferReader();

        try {

            while ((line = br.readNext()) != null) {
                //init check since 3 line
                if (!line[0].startsWith("ID")) {

                    Section section = new Section();
                    if (line.length > 1) {
                        section.setId(line[0].trim());
                        section.setDesc(line[1].trim());

                        valid = vaidateSection(section);
                        if (valid) {
                            Sections s = null;
                            if (section.getType().equalsIgnoreCase("insert")) {
                                s = new Sections();
                                s.createdBy = userId.toUpperCase();
                                s.createdDate = FormatDateUtil.ownFormat.
                                        parse(FormatDateUtil.ownFormat.format(new Date()));
                            } else if (section.getType().equalsIgnoreCase("update")) {
                                s = Sections
                                        .findById(Integer.parseInt(section.getId()));
                                s.modifiedBy = userId.toUpperCase();
                                s.modifiedDate = FormatDateUtil.ownFormat.
                                        parse(FormatDateUtil.ownFormat.format(new Date()));
                            }
                            s.id = Integer.parseInt(section.getId());
                            s.nombreSeccion = section.getDesc();
                            sections.add(s);
                        }
                    } else {
                        getErrores().add(line[0] + ","
                                + ",No cumple con el numero de parametros");
                    }
                }
            }
        } catch (FileNotFoundException ex) {
            Logger.getLogger(ReadCsvFile.class.getName()).log(Level.SEVERE,
                    null, ex);
        } catch (IOException ex) {
            Logger.getLogger(ReadCsvFile.class.getName()).log(Level.SEVERE,
                    null, ex);
        } catch (ParseException ex) {
            Logger.getLogger(ReadCatPractFile.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            try {
                br.close();
            } catch (IOException ex) {
                Logger.getLogger(ReadCatPractFile.class.getName()).log(Level.SEVERE, null, ex);
            }
        }

        return sections;
    }

    private boolean vaidateSection(Section section) {

        if (section.getId().length() > 0 && section.getId().length() < 5) {
            if (section.getDesc().length() > 0 && section.getDesc().length() < 101) {

                Sections sections = Sections.findById(Integer.parseInt(section.getId()));
                if (sections != null) {
                    section.setType("update");
                } else {
                    section.setType("insert");
                } 
            } else {
                getErrores().add(section.getId() + ","
                        + section.getDesc() + "," 
                        + ",La descripción es mayor a 100 caracteres o viene nulo");
                return false;
            }
        } else {
            getErrores().add(section.getId() + ","
                    + section.getDesc() + "," 
                    + ",El id seccion es mayor a 4 caracteres o viene nulo");
            return false;
        }

        return true;
    }

    public List<String> getErrores() {
        return errores;
    }

    public void setErrores(List<String> errores) {
        this.errores = errores;
    }
}
