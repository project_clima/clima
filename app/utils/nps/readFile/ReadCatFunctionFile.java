/**
 * @(#) ReadCatFunctionFile 30/06/2017
 */

package utils.nps.readFile;

import com.opencsv.CSVReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import models.FunctionEmployee;
import models.nps.FunctionByTitle;
import models.nps.NPS_KEY_FUNCTION;
import utils.nps.model.Function;
import utils.nps.util.FormatDateUtil;

/**
 *
 * @author Rodolfo Miranda -- Qualtop
 */
public class ReadCatFunctionFile {

    private File file;
    private List<String> errores;

    public ReadCatFunctionFile() {
        this.errores = new ArrayList<>();
    }

    public void setFileReader(File file) {
        this.file = file;
    }

    /**
     * Description : Method for Initialize the bufferedReader of file name on
     * FTP
     *
     * @return BufferedReader
     * @throws FileNotFoundException
     */
    private CSVReader getBufferReader() {
        if (file != null) {
            try {
                CSVReader br = null;
                br = new CSVReader(new InputStreamReader(new FileInputStream(file), "Cp1252"));
                return br;
            } catch (FileNotFoundException ex) {
                Logger.getLogger(ReadCsvFile.class.getName()).log(Level.SEVERE, null, ex);
            } catch (UnsupportedEncodingException ex) {
                Logger.getLogger(ReadCsvFile.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        return null;

    }

    /**
     * MEtodo principal para la lectura de los registros
     * @param userId
     * @return 
     */
    public List<FunctionByTitle> readCvsFile(String userId) {
        String[] line;

        boolean valid = true;
        List<FunctionByTitle> functions = new ArrayList<>();
        CSVReader br = getBufferReader();

        try {

            while ((line = br.readNext()) != null) {
                //init check since 3 line
                if (!line[0].toUpperCase().startsWith("TITULO")) {

                    Function function = new Function();
                    if (line.length > 1) {
                        if (line[0].trim().toUpperCase().contains("NO_")) {
                            deleteFunction(line[0].trim(), line[1].trim());
                            continue;
                        }
                        
                        function.setIdFunction(line[1].trim());
                        function.setTitle(line[0].trim());

                        valid = vaidateFunction(function);
                        if (valid) {
                            FunctionByTitle fnByTitle = null;
                            
                            if (function.getType().equalsIgnoreCase("insert")) {
                                fnByTitle = new FunctionByTitle();
                                fnByTitle.key = new NPS_KEY_FUNCTION();
                                fnByTitle.createFor = userId.toUpperCase();
                                fnByTitle.createDate = FormatDateUtil.ownFormat.
                                        parse(FormatDateUtil.ownFormat.format(new Date()));
                            } else if (function.getType().equalsIgnoreCase("update")) {
                                NPS_KEY_FUNCTION keyFunction = new NPS_KEY_FUNCTION(
                                    function.getTitle(),
                                    Integer.parseInt(function.getIdFunction().trim())
                                );
                                
                                fnByTitle = FunctionByTitle.findById(keyFunction);
                                
                                fnByTitle.modifiedFor = userId.toUpperCase();
                                fnByTitle.modifiedDate = FormatDateUtil.ownFormat.
                                parse(FormatDateUtil.ownFormat.format(new Date()));
                            }
                            
                            fnByTitle.key.title = function.getTitle();
                            fnByTitle.key.idFunction = Integer.parseInt(function.getIdFunction().trim());
                            functions.add(fnByTitle);
                        }
                    } else {
                        getErrores().add(line[0] + ","
                                + ",No cumple con el numero de parametros");
                    }
                }
            }
        } catch (FileNotFoundException ex) {
            Logger.getLogger(ReadCsvFile.class.getName()).log(Level.SEVERE,
                    null, ex);
        } catch (IOException ex) {
            Logger.getLogger(ReadCsvFile.class.getName()).log(Level.SEVERE,
                    null, ex);
        } catch (ParseException ex) {
            Logger.getLogger(ReadCatPractFile.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            try {
                br.close();
            } catch (IOException ex) {
                Logger.getLogger(ReadCatPractFile.class.getName()).log(Level.SEVERE, null, ex);
            }
        }

        return functions;
    }

    /**
     * Metodo para la validacion de los registros
     * @param function
     * @return 
     */
    private boolean vaidateFunction(Function function) {

        if (function.getTitle().length() > 0 && function.getTitle().length() < 9
                && containsString(function.getTitle())) {
            if (function.getIdFunction().length() > 0 && function.getIdFunction().length() < 20
                    && function.getIdFunction().matches("[+]?\\d*\\.?\\d+")
                    && existFunction(function.getIdFunction().trim())) {

                FunctionByTitle fnEmployee = FunctionByTitle.find("key.title = ? and key.idFunction = ?",
                        function.getTitle(),
                        Integer.parseInt(function.getIdFunction().trim())).first();
                
                if (fnEmployee != null) {
                    function.setType("update");
                } else {
                    function.setType("insert");
                } 
            } else {
                getErrores().add(function.getTitle()+ ","
                        + function.getIdFunction()+ "," 
                        + ",La descripción es mayor a 19 caracteres o viene nulo"
                                + " o no es numerico o no existe en la tabla de funciones");
                return false;
            }
        } else {
            getErrores().add(function.getTitle()+ ","
                    + function.getIdFunction()+ "," 
                    + ",El id seccion es mayor a 9 caracteres o viene nulo o no"
                            + "es una cadena valida");
            return false;
        }

        return true;
    }

    public static boolean containsString(String word){
        
        if( word.equalsIgnoreCase("JEFE") || word.equalsIgnoreCase("GERENTE") ||
                word.equalsIgnoreCase("DIRECTOR")|| word.equalsIgnoreCase("ZONAL")){
            return true;
        }
        
        return false;
    }
    
    public static boolean existFunction(String idFunction){
        
        FunctionEmployee function = FunctionEmployee.findById(Integer.
                parseInt(idFunction));
        
        if(function != null ){
            return true;
        }
        
        return false;
    }
    
    public List<String> getErrores() {
        return errores;
    }

    public void setErrores(List<String> errores) {
        this.errores = errores;
    }
    
    private void deleteFunction(String functionWithPrefix, String id) {        
        FunctionByTitle function = FunctionByTitle.find(
            "upper(key.title) = ?1 and key.idFunction = ?2",
            functionWithPrefix.replace("NO_", ""),
            Integer.valueOf(id)
        ).first();
        
        if (function != null) {
            function.delete();
        }
    }
}
