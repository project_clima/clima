/**
 * @(#) ReadCatPractFile 19/05/2017
 */

package utils.nps.readFile;

import com.opencsv.CSVReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import models.nps.NpsArea;
import models.nps.NpsPractice;
import utils.nps.model.Practice;
import utils.nps.util.FormatDateUtil;

/**
 *
 * @author Rodolfo Miranda -- Qualtop
 */
public class ReadCatPractFile {

    private File file;
    private List<String> errores;    
    public ReadCatPractFile(){
        this.errores = new ArrayList<>();
    }
    
    public void setFileReader(File file){
        this.file = file;
    }
        /**
     * Description :
     *  Method for Initialize the bufferedReader of file name on FTP
     * @return BufferedReader
     * @throws FileNotFoundException 
     */
    private CSVReader getBufferReader() {
        if( file != null ){
            try {
                CSVReader br = null;
                br = new CSVReader( new InputStreamReader(new FileInputStream(file),"Cp1252") );
                return br;
            } catch (FileNotFoundException ex) {
                Logger.getLogger(ReadCsvFile.class.getName()).log(Level.SEVERE, null, ex);
            } catch (UnsupportedEncodingException ex) {
                Logger.getLogger(ReadCsvFile.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        return null;
        
    }
    
    public List<NpsPractice> readCvsFile(String userId){
        String [] line;
        
        boolean valid               = true;
        List<NpsPractice> practices = new ArrayList<>();
        CSVReader br                = getBufferReader();
                
        try{
            
            while ((line = br.readNext()) != null) {
                //init check since 3 line
                if( !line[0].contains("ID_") ){

                    Practice practice = new Practice();
                    if( line.length > 2 ){
                        practice.setIdPractice(line[0].trim());
                        practice.setDesc(line[1].trim());
                        practice.setIdArea(line[2].trim());
                        valid = vaidatePractice(practice);
                        if( valid ){
                            NpsPractice p = null;
                            if( practice.getType().equalsIgnoreCase("insert") ){
                                p = new NpsPractice();
                                p.creFor = userId.toUpperCase();
                                p.creDate = FormatDateUtil.ownFormat.
                                    parse(FormatDateUtil.ownFormat.format(new Date()));
                            }else if( practice.getType().equalsIgnoreCase("update") ){
                                p = NpsPractice
                                        .findById(practice.getIdPractice());
                                p.modFor = userId.toUpperCase();
                                p.modDate = FormatDateUtil.ownFormat.
                                    parse(FormatDateUtil.ownFormat.format(new Date()));
                            }
                            p.practice = practice.getIdPractice();
                            p.idArea = practice.getIdArea();
                            p.desc = practice.getDesc();
                            practices.add(p);
                        }
                    }else{
                        getErrores().add(line[0]+","+
                            ",La descripción es mayor a 255 caracteres o viene nulo");
                    }           
                }
            }
        } catch (FileNotFoundException ex) {
            Logger.getLogger(ReadCsvFile.class.getName()).log(Level.SEVERE,
                    null, ex);
        } catch (IOException ex) {
            Logger.getLogger(ReadCsvFile.class.getName()).log(Level.SEVERE,
                    null, ex);
        } catch (ParseException ex) {
            Logger.getLogger(ReadCatPractFile.class.getName()).log(Level.SEVERE, null, ex);
        }finally{
            try {
                br.close();
            } catch (IOException ex) {
                Logger.getLogger(ReadCatPractFile.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        
        return practices;
    }

    private boolean vaidatePractice(Practice practice) {
        
        if( practice.getIdPractice().length() > 0 && practice.getIdPractice().length() < 11 ){
            if( practice.getDesc().length() > 0 && practice.getDesc().length() < 256 ){
                if( practice.getIdArea().length() > 0 && practice.getIdArea().length() < 11 ){
                    NpsArea npsArea = NpsArea.findById(practice.getIdArea());
                    if( npsArea != null ){
                        NpsPractice pract = NpsPractice
                                .findById(practice.getIdPractice());
                        if( pract != null ){
                            practice.setType("update");
                        }else{
                            practice.setType("insert");
                        }
                    }else{
                        getErrores().add(practice.getIdPractice()+","+
                            practice.getDesc()+","+practice.getIdArea()+
                                ",El área no existe");
                        return false;
                    }
                }else{
                    getErrores().add(practice.getIdPractice()+","+
                            practice.getDesc()+","+practice.getIdArea()
                            +",El id area es mayor a 10 caracteres o viene nulo");
                    return false;
                }
            }else{
                getErrores().add(practice.getIdPractice()+","+
                            practice.getDesc()+","+practice.getIdArea()
                        +",La descripción es mayor a 255 caracteres o viene nulo");
                return false;
            }
        }else{
            getErrores().add(practice.getIdPractice()+","+
                            practice.getDesc()+","+practice.getIdArea()
                    +",El id practica es mayor a 10 caracteres o viene nulo");
            return false;
        }
        
        return true;
    }

    public List<String> getErrores() {
        return errores;
    }

    public void setErrores(List<String> errores) {
        this.errores = errores;
    }
}
