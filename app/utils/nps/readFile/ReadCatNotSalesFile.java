/**
 * @(#) ReadCatNotSalesFile 30/06/2017
 */
package utils.nps.readFile;

import com.opencsv.CSVReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import models.Sections;
import models.StoreEmployee;
import models.Structure;
import models.nps.NpsArea;
import models.nps.NpsEmpNotSales;
import utils.nps.model.NotSales;
import utils.nps.util.FormatDateUtil;

/**
 * Clase de utilidad para la lectura de los archivos del catalogo de No Ventas
 * @author Rodolfo Miranda -- Qualtop
 */
public class ReadCatNotSalesFile {

    private File file;
    private List<String> errores;

    public ReadCatNotSalesFile() {
        this.errores = new ArrayList<>();
    }

    public void setFileReader(File file) {
        this.file = file;
    }

    /**
     * Description : Method for Initialize the bufferedReader of file name on
     * FTP
     *
     * @return BufferedReader
     * @throws FileNotFoundException
     */
    private CSVReader getBufferReader() {
        if (file != null) {
            try {
                CSVReader br = null;
                br = new CSVReader(new InputStreamReader(new FileInputStream(file), "Cp1252"));
                return br;
            } catch (FileNotFoundException ex) {
                Logger.getLogger(ReadCsvFile.class.getName()).log(Level.SEVERE, null, ex);
            } catch (UnsupportedEncodingException ex) {
                Logger.getLogger(ReadCsvFile.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        return null;

    }

    /**
     * Metodo principal para la lectura de los registros
     * @param userId
     * @return 
     */
    public List<NpsEmpNotSales> readCvsFile(String userId) {
        String[] line;

        boolean valid = true;
        List<NpsEmpNotSales> notSales = new ArrayList<>();
        CSVReader br = getBufferReader();

        try {

            while ((line = br.readNext()) != null) {
                //init check since 3 line
                if (!line[0].startsWith("ID")) {

                    NotSales notSale = new NotSales();
                    if (line.length > 9) {
                        notSale.setEmpNumber(line[0].trim());
                        notSale.setIdStore(line[1].trim());
                        notSale.setIdArea(line[2].trim());
                        notSale.setName(line[3].trim());
                        notSale.setUserId(line[4].trim());
                        notSale.setUserIdChief(line[5].trim());
                        notSale.setUserIdManager(line[6].trim());
                        notSale.setUserIdDirector(line[7].trim());
                        notSale.setUserIdZonal(line[8].trim());
                        notSale.setIdSection(line[9].trim());

                        valid = vaidateNotSale(notSale);
                        if (valid) {
                            NpsEmpNotSales notSaleEmp = null;
                            if (notSale.getType().equalsIgnoreCase("insert")) {
                                notSaleEmp = new NpsEmpNotSales();
                                notSaleEmp.createFor = userId.toUpperCase();
                                notSaleEmp.createDate = FormatDateUtil.ownFormat.
                                        parse(FormatDateUtil.ownFormat.format(new Date()));
                            } else if (notSale.getType().equalsIgnoreCase("update")) {
                                notSaleEmp = NpsEmpNotSales.find(
                                                        "select m from NpsEmpNotSales m where m.empNumber = ? ",
                                                        Long.parseLong(notSale.getEmpNumber())).first();
                                
                                notSaleEmp.modifiedFor = userId.toUpperCase();
                                notSaleEmp.modifiedDate = FormatDateUtil.ownFormat.
                                        parse(FormatDateUtil.ownFormat.format(new Date()));
                            }

                            notSaleEmp.empNumber = Long.parseLong(notSale.getEmpNumber());
                            notSaleEmp.idStore = Integer.parseInt(notSale.getIdStore());
                            notSaleEmp.idArea = notSale.getIdArea();
                            notSaleEmp.name = notSale.getName();
                            notSaleEmp.userIdChief = Long.parseLong(notSale.getUserIdChief());
                            notSaleEmp.userIdManager = Long.parseLong(notSale.getUserIdManager());
                            notSaleEmp.userIdDirector = Long.parseLong(notSale.getUserIdDirector());
                            notSaleEmp.userIdZonal = Long.parseLong(notSale.getUserIdZonal());
                            StoreEmployee store = StoreEmployee.
                                find("select m from StoreEmployee m where m.idStore = ?",
                                        Integer.parseInt(notSale.getIdStore())).first();
                            notSaleEmp.zone = store.zoneNps;
                            notSaleEmp.idSection = Integer.parseInt(notSale.getIdSection());
                            notSaleEmp.userId = Long.parseLong(notSale.getUserId());
                            
                            
                            notSales.add(notSaleEmp);
                        }
                    } else {
                        getErrores().add(line[0] + ","
                                + ",No cumple con el numero de parametros");
                    }
                }
            }
        } catch (FileNotFoundException ex) {
            Logger.getLogger(ReadCsvFile.class.getName()).log(Level.SEVERE,
                    null, ex);
        } catch (IOException ex) {
            Logger.getLogger(ReadCsvFile.class.getName()).log(Level.SEVERE,
                    null, ex);
        } catch (ParseException ex) {
            Logger.getLogger(ReadCatPractFile.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            try {
                br.close();
            } catch (IOException ex) {
                Logger.getLogger(ReadCatPractFile.class.getName()).log(Level.SEVERE, null, ex);
            }
        }

        return notSales;
    }

    private boolean vaidateNotSale(NotSales notSale) {

        if (notSale.getEmpNumber().length() > 0 && notSale.getEmpNumber().length() < 21
                && containsEmpNumber(notSale.getEmpNumber())) {
            if (notSale.getIdStore().matches("-?[1-9]\\d*|0")
                    && existStore(notSale.getIdStore())) {

                if (existArea(notSale.getIdArea())) {
                    if (notSale.getName().length() > 0 && notSale.getName().length() < 301) {
                        if (notSale.getUserId().matches("-?[1-9]\\d*|0")
                                && notSale.getUserId().length() > 0 && notSale.getUserId().length() < 21
                                && containsUserId(notSale.getUserId())) {
                            if (notSale.getUserIdChief().matches("-?[1-9]\\d*|0")
                                    && notSale.getUserIdChief().length() > 0 && notSale.getUserIdChief().length() < 21
                                    && containsUserIdChief("6666666666", notSale.getUserIdChief())) {

                                if (notSale.getUserIdManager().matches("-?[1-9]\\d*|0")
                                        && notSale.getUserIdManager().length() > 0 && notSale.getUserIdManager().length() < 21
                                        && containsUserIdChief("7777777777", notSale.getUserIdManager())) {

                                    if (notSale.getUserIdDirector().matches("-?[1-9]\\d*|0")
                                            && notSale.getUserIdDirector().length() > 0 && notSale.getUserIdDirector().length() < 21
                                            && containsUserIdChief("8888888888", notSale.getUserIdDirector())) {

                                        if (notSale.getUserIdZonal().matches("-?[1-9]\\d*|0") && notSale.getUserIdZonal().length() > 0
                                                && notSale.getUserIdZonal().length() < 21
                                                && containsUserIdChief("9999999999", notSale.getUserIdZonal())) {

                                            if (notSale.getIdSection().matches("-?[1-9]\\d*|0")
                                                    && notSale.getIdSection().length() > 0 && notSale.getIdSection().length() < 5
                                                    && existSection(notSale.getIdSection())) {
                                                NpsEmpNotSales fnEmployee = NpsEmpNotSales.find(
                                                        "select m from NpsEmpNotSales m where m.empNumber = ? ",
                                                        Long.parseLong(notSale.getEmpNumber())).first();

                                                if (fnEmployee != null) {
                                                    notSale.setType("update");
                                                } else {
                                                    notSale.setType("insert");
                                                }
                                            } else {
                                                getErrores().add(notSale.getEmpNumber() + "," + notSale.getIdStore() + "," + notSale.getIdArea() + ","
                                                        + notSale.getName() + "," + notSale.getUserId() + "," + notSale.getUserIdChief() + ","
                                                        + notSale.getUserIdManager() + "," + notSale.getUserIdDirector() + "," + notSale.getUserIdZonal() + ","
                                                        + notSale.getIdSection() + "," + ",La seccion es invalida");
                                                return false;
                                            }
                                        } else {
                                            getErrores().add(notSale.getEmpNumber() + "," + notSale.getIdStore() + "," + notSale.getIdArea() + ","
                                                    + notSale.getName() + "," + notSale.getUserId() + "," + notSale.getUserIdChief() + ","
                                                    + notSale.getUserIdManager() + "," + notSale.getUserIdDirector() + "," + notSale.getUserIdZonal() + ","
                                                    + notSale.getIdSection() + "," + ",El id zonal es invalido");
                                            return false;
                                        }
                                    } else {
                                        getErrores().add(notSale.getEmpNumber() + "," + notSale.getIdStore() + "," + notSale.getIdArea() + ","
                                                + notSale.getName() + "," + notSale.getUserId() + "," + notSale.getUserIdChief() + ","
                                                + notSale.getUserIdManager() + "," + notSale.getUserIdDirector() + "," + notSale.getUserIdZonal() + ","
                                                + notSale.getIdSection() + "," + ",El id director es invalido");
                                        return false;
                                    }
                                } else {
                                    getErrores().add(notSale.getEmpNumber() + "," + notSale.getIdStore() + "," + notSale.getIdArea() + ","
                                            + notSale.getName() + "," + notSale.getUserId() + "," + notSale.getUserIdChief() + ","
                                            + notSale.getUserIdManager() + "," + notSale.getUserIdDirector() + "," + notSale.getUserIdZonal() + ","
                                            + notSale.getIdSection() + "," + ",El id gerente es invalido");
                                    return false;
                                }
                            } else {
                                getErrores().add(notSale.getEmpNumber() + "," + notSale.getIdStore() + "," + notSale.getIdArea() + ","
                                        + notSale.getName() + "," + notSale.getUserId() + "," + notSale.getUserIdChief() + ","
                                        + notSale.getUserIdManager() + "," + notSale.getUserIdDirector() + "," + notSale.getUserIdZonal() + ","
                                        + notSale.getIdSection() + "," + ",El id jefe es invalido");
                                return false;
                            }
                        } else {
                            getErrores().add(notSale.getEmpNumber() + "," + notSale.getIdStore() + "," + notSale.getIdArea() + ","
                                    + notSale.getName() + "," + notSale.getUserId() + "," + notSale.getUserIdChief() + ","
                                    + notSale.getUserIdManager() + "," + notSale.getUserIdDirector() + "," + notSale.getUserIdZonal() + ","
                                    + notSale.getIdSection() + "," + ",El id user es invalido");
                            return false;
                        }
                    } else {
                        getErrores().add(notSale.getEmpNumber() + "," + notSale.getIdStore() + "," + notSale.getIdArea() + ","
                                + notSale.getName() + "," + notSale.getUserId() + "," + notSale.getUserIdChief() + ","
                                + notSale.getUserIdManager() + "," + notSale.getUserIdDirector() + "," + notSale.getUserIdZonal() + ","
                                + notSale.getIdSection() + "," + ",Error en el nombre de empleado");
                        return false;
                    }

                } else {
                    getErrores().add(notSale.getEmpNumber() + "," + notSale.getIdStore() + "," + notSale.getIdArea() + ","
                            + notSale.getName() + "," + notSale.getUserId() + "," + notSale.getUserIdChief() + ","
                            + notSale.getUserIdManager() + "," + notSale.getUserIdDirector() + "," + notSale.getUserIdZonal() + ","
                            + notSale.getIdSection() + "," + ",El area no existe");
                    return false;
                }
            } else {
                getErrores().add(notSale.getEmpNumber() + "," + notSale.getIdStore() + "," + notSale.getIdArea() + ","
                        + notSale.getName() + "," + notSale.getUserId() + "," + notSale.getUserIdChief() + ","
                        + notSale.getUserIdManager() + "," + notSale.getUserIdDirector() + "," + notSale.getUserIdZonal() + ","
                        + notSale.getIdSection() + "," + ",Error en el id tienda");
                return false;
            }
        } else {
            getErrores().add(notSale.getEmpNumber() + "," + notSale.getIdStore() + "," + notSale.getIdArea() + ","
                    + notSale.getName() + "," + notSale.getUserId() + "," + notSale.getUserIdChief() + ","
                    + notSale.getUserIdManager() + "," + notSale.getUserIdDirector() + "," + notSale.getUserIdZonal() + ","
                    + notSale.getIdSection() + "," + ",Error en el empleado id");
            return false;
        }

        return true;
    }

    public static boolean existStore(String idStore) {

        StoreEmployee store = StoreEmployee.
                find("select m from StoreEmployee m where m.idStore = ?",
                        Integer.parseInt(idStore)).first();

        if (store != null) {
            return true;
        }

        return false;
    }

    public static boolean existArea(String idArea) {

        NpsArea area = NpsArea.
                findById(idArea);

        if (area != null) {
            return true;
        }

        return false;
    }

    public static boolean existSection(String idSection) {

        Sections section = Sections.
                findById(Integer.parseInt(idSection));

        if (section != null) {
            return true;
        }

        return false;
    }

    public static boolean containsEmpNumber(String empId) {

        Structure structure = Structure.
                find("select m from Structure m where m.empid = ?",
                        Integer.parseInt(empId)).first();

        if (structure != null) {
            return false;
        }

        return true;
    }

    public static boolean containsUserId(String userId) {


        Structure structure = Structure.
                findById(Long.parseLong(userId));

        if (structure != null) {
            return false;
        }

        return true;
    }

    public static boolean containsUserIdChief(String field, String userIdChief) {

        if (!userIdChief.equals(field)) {

            Structure structure = Structure.
                    findById(Long.parseLong(userIdChief));

            if (structure != null) {
                return true;
            } else {
                NpsEmpNotSales notSale = NpsEmpNotSales.find("select m"
                        + " from NpsEmpNotSales m where m.userId = ?",
                        Long.parseLong(userIdChief)).first();
                if (notSale != null) {
                    return true;
                }
            }
            return false;
        }

        return true;
    }

    public List<String> getErrores() {
        return errores;
    }

    public void setErrores(List<String> errores) {
        this.errores = errores;
    }
}
