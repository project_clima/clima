/**
 * @(#) ReadCvsFile 14/03/2017
 */
package utils.nps.readFile;

import SchedulerTask.nps.ProcessRHStruct;
import com.opencsv.CSVReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.LineNumberReader;
import java.io.UnsupportedEncodingException;
import java.util.logging.Level;
import java.util.logging.Logger;
import models.nps.FilesLoadNps;
import models.nps.StructureTmp;
import utils.nps.enums.Field;

/**
 *
 * Class for read Cvs File from FTP
 *
 * @author Rodolfo Miranda -- Qualtop
 */
public class ReadCsvFile {

    //Variable name of the cvs file
    private File file;

    //Constructor 
    public ReadCsvFile() {

    }

    public void setFileReader(File file) {
        this.file = file;
    }

    /**
     * Description : Method for Initialize the bufferedReader of file name on
     * FTP
     *
     * @return BufferedReader
     * @throws FileNotFoundException
     */
    private CSVReader getBufferReader() {
        if (file != null) {
            try {
                CSVReader br = null;
                br = new CSVReader(new InputStreamReader(new FileInputStream(file), "Cp1252"));
                return br;
            } catch (FileNotFoundException ex) {
                Logger.getLogger(ReadCsvFile.class.getName()).log(Level.SEVERE, null, ex);
            } catch (UnsupportedEncodingException ex) {
                Logger.getLogger(ReadCsvFile.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        return null;

    }
    
    /**
     * Count total number of lines
     * 
     * @return int
     */
    public int countLineNumber() {
        int lines = 0;

        if (file == null) {
            return lines;
        }

        try {
            LineNumberReader lineNumberReader = new LineNumberReader(new FileReader(file));
            lineNumberReader.skip(Long.MAX_VALUE);
            lines = lineNumberReader.getLineNumber();
            lineNumberReader.close();

        } catch (FileNotFoundException e) {
            System.out.println("FileNotFoundException Occurred" + e.getMessage());
        } catch (IOException e) {
            System.out.println("IOException Occurred" + e.getMessage());
        }

        return lines;
    }

    /**
     * Description: Method for read cvs file
     *
     * @param process
     * @param print
     * @param loadFile
     * @return
     */
    public boolean readCvsFile(ProcessRHStruct process, boolean print, Long loadFile) {
        String[] line;

        Long total = new Long(countLineNumber());
        CSVReader br = getBufferReader();

        final int percent = 25;
        float factor = (float) 65 / total;
        int i = 1;
        int j = 1;

        try {
            while ((line = br.readNext()) != null) {
                //init check since 3 line
                if (!line[0].equalsIgnoreCase("STATUS")) {
                    // use comma as separator
                    //if(ValidateNpsString.validateString(line,nLine)){
                    //    lines.addRightLine(line);
                    //}else{
                    //    lines.addBadLine(line);
                    //}
                    StructureTmp registry = new StructureTmp();

                    for (Field field : Field.values()) {
                        switch (field) {
                            case STATUS:
                                registry.status = line[field.getI()].trim().
                                        replace("\"", "");
                                break;
                            case USERID:
                                registry.userId = line[field.getI()].trim().
                                        replace("\"", "");
                                ;
                                break;
                            case COUNTRY:
                                registry.country = line[field.getI()].trim().
                                        replace("\"", "");
                                ;
                                break;
                            case CUSTOM01:
                                registry.custom01 = line[field.getI()].trim().
                                        replace("\"", "");
                                ;
                                break;
                            case CUSTOM02:
                                registry.custom02 = line[field.getI()].trim().
                                        replace("\"", "");
                                ;
                                break;
                            case CUSTOM03:
                                registry.custom03 = line[field.getI()].trim().
                                        replace("\"", "");
                                ;
                                break;
                            case CUSTOM04:
                                registry.custom04 = line[field.getI()].trim().
                                        replace("\"", "");
                                ;
                                break;
                            case CUSTOM05:
                                registry.custom05 = line[field.getI()].trim().
                                        replace("\"", "");
                                ;
                                break;
                            case CUSTOM06:
                                registry.custom06 = line[field.getI()].trim().
                                        replace("\"", "");
                                ;
                                break;
                            case CUSTOM07:
                                registry.custom07 = line[field.getI()].trim().
                                        replace("\"", "");
                                ;
                                break;
                            case CUSTOM08:
                                registry.custom08 = line[field.getI()].trim().
                                        replace("\"", "");
                                ;
                                break;
                            case CUSTOM09:
                                registry.custom09 = line[field.getI()].trim().
                                        replace("\"", "");
                                ;
                                break;
                            case CUSTOM10:
                                registry.custom10 = line[field.getI()].trim().
                                        replace("\"", "");
                                ;
                                break;
                            case CUSTOM13:
                                registry.custom13 = line[field.getI()].trim().
                                        replace("\"", "");
                                ;
                                break;
                            case CUSTOM_MANAGER:
                                registry.customManager = line[field.getI()].
                                        trim().
                                        replace("\"", "");
                                ;
                                break;
                            case DEFAULT_LOCALE:
                                registry.defaultLocale = line[field.getI()].
                                        trim().
                                        replace("\"", "");
                                ;
                                break;
                            case DEPARTMENT:
                                registry.deparment = line[field.getI()].
                                        trim().
                                        replace("\"", "");
                                ;
                                break;
                            case DIVISION:
                                registry.division = line[field.getI()].trim().
                                        replace("\"", "");
                                ;
                                break;
                            case EMAIL:
                                registry.email = line[field.getI()].trim().
                                        replace("\"", "");
                                ;
                                break;
                            case EMPID:
                                registry.empId = line[field.getI()].trim().
                                        replace("\"", "");
                                ;
                                break;
                            case FIRSTNAME:
                                registry.firstName = line[field.getI()].
                                        trim().
                                        replace("\"", "");
                                ;
                                break;
                            case GENDER:
                                registry.gender = line[field.getI()].trim().
                                        replace("\"", "");
                                ;
                                break;
                            case HIREDATE:
                                registry.hireDate = line[field.getI()].trim().
                                        replace("\"", "");
                                ;
                                break;
                            case HR:
                                registry.hr = line[field.getI()].trim().
                                        replace("\"", "");
                                ;
                                break;
                            case JOBCODE:
                                registry.jobcode = line[field.getI()].trim().
                                        replace("\"", "");
                                ;
                                break;
                            case LASTNAME:
                                registry.lastname = line[field.getI()].trim().
                                        replace("\"", "");
                                ;
                                break;
                            case LOCATION:
                                registry.location = line[field.getI()].trim().
                                        replace("\"", "");
                                ;
                                break;
                            case MANAGER:
                                registry.manager = line[field.getI()].trim().
                                        replace("\"", "");
                                ;
                                break;
                            case TIMEZONE:
                                registry.timezone = line[field.getI()].trim().
                                        replace("\"", "");
                                ;
                                break;
                            case TITLE:
                                registry.title = line[field.getI()].trim().
                                        replace("\"", "");
                                ;
                                break;
                            case USERNAME:
                                registry.username = line[field.getI()].trim().
                                        replace("\"", "");
                                ;
                                break;
                        }
                    }
                    //records.add(registry);
                    registry.save();

                    if (print) {
                        if ((factor * i) >= 1.0) {
                            process.updatePercent(percent + j, loadFile);
                            i = 0;
                            j++;
                        }
                        i++;
                    }
                }

            }
            return true;
        } catch (FileNotFoundException ex) {
            Logger.getLogger(ReadCsvFile.class.getName()).log(Level.SEVERE,
                    null, ex);
            return false;
        } catch (IOException ex) {
            Logger.getLogger(ReadCsvFile.class.getName()).log(Level.SEVERE,
                    null, ex);
            return false;
        } finally {
            try {
                br.close();
            } catch (IOException ex) {
                Logger.getLogger(ReadCsvFile.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }
}
