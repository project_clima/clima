/**
 * @(#) ReadCatStoreFile 19/05/2017
 */
package utils.nps.readFile;

import play.Logger;
import com.opencsv.CSVReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.logging.Level;

import javax.persistence.Query;
import models.NpsAnswer;
import models.StoreEmployee;
import models.nps.NpsBusiness;
import models.nps.NpsGroup;
import play.db.jpa.JPA;
import utils.nps.model.Store;
import utils.nps.util.FormatDateUtil;

/**
 *
 * @author Rodolfo Miranda -- Qualtop
 */
public class ReadCatStoreFile {

    private File file;
    private List<String> errores;

    public ReadCatStoreFile() {
        this.errores = new ArrayList<>();
    }

    public void setFileReader(File file) {
        this.file = file;
    }

    /**
     * Description : Method for Initialize the bufferedReader of file name on
     * FTP
     *
     * @return BufferedReader
     * @throws FileNotFoundException
     */
    private CSVReader getBufferReader() {
        if (file != null) {
            try {
                CSVReader br = null;
                br = new CSVReader(new InputStreamReader(new FileInputStream(file), "Cp1252"));
                return br;
            } catch (FileNotFoundException ex) {
                Logger.error(ex.getMessage());
            } catch (UnsupportedEncodingException ex) {
                Logger.error(ex.getMessage());
            }
        }
        return null;

    }

    /**
     * Metodo principal para la lectura de los registros
     * @param userId
     * @param update
     * @return 
     */
    public List<StoreEmployee> readCvsFile(String userId, boolean update) {
        String[] line;
        boolean valid = true;
        List<StoreEmployee> stores = new ArrayList<>();
        CSVReader br = getBufferReader();
        try {

            while ((line = br.readNext()) != null) {
                //init check since 3 line
                if (!line[0].startsWith("ID")) {

                    Store store = new Store();
                    if (line.length > 4) {
                        store.setId(line[0].trim());
                        store.setDesc(line[1].trim());
                        store.setIdParent(line[2].trim());
                        store.setGroup(line[3].trim());
                        store.setBusiness(line[4].trim());
                        valid = vaidateStore(store);
                        if (valid) {
                            StoreEmployee storeE = null;
                            if (store.getType().equalsIgnoreCase("insert")) {
                                Date structure = (Date) StoreEmployee.
                                        find("select max(m.structureDate) from StoreEmployee m").first();
                                storeE = new StoreEmployee();
                                storeE.createdBy = userId.toUpperCase();
                                storeE.createdDate = FormatDateUtil.ownFormat.
                                        parse(FormatDateUtil.ownFormat.format(new Date()));
                                storeE.structureDate = structure;
                            } else if (store.getType().equalsIgnoreCase("update")) {
                                storeE = StoreEmployee.find("select m from StoreEmployee m where m.idStore = ? \n"
                                        + " and m.structureDate IN (select max(s.structureDate) from StoreEmployee s)",
                                        Integer.parseInt(store.getId())).first();
                                storeE.modifiedBy = userId.toUpperCase();
                                storeE.modifiedDate = FormatDateUtil.ownFormat.
                                        parse(FormatDateUtil.ownFormat.format(new Date()));

                                updateAnswers(storeE.idStore, store.getGroup(),
                                        store.getBusiness(), store.getIdParent(), update);
                                updateNotSale(storeE.idStore, store.getIdParent(), update);

                            }
                            storeE.idStore = Integer.parseInt(store.getId());
                            storeE.descriptionStore = store.getDesc();
                            storeE.zoneNps = store.getIdParent();
                            storeE.groupId = Integer.parseInt(store.getGroup());
                            storeE.busId = Integer.parseInt(store.getBusiness());

                            stores.add(storeE);
                        }
                    } else {
                        getErrores().add(store.getId() + ","
                                + store.getDesc() + "," + store.getIdParent() + ","
                                + store.getGroup() + "," + store.getBusiness() + ","
                                + ",El numero de argumentos no es valido");
                    }
                }
            }
        } catch (FileNotFoundException ex) {
            Logger.error(ex.getMessage());
        } catch (IOException ex) {
            Logger.error(ex.getMessage());
        } catch (ParseException ex) {
            Logger.error(ex.getMessage());
        } finally {
            try {
                br.close();
            } catch (IOException ex) {
                Logger.error(ex.getMessage());
            }
        }

        return stores;
    }

    private boolean vaidateStore(Store store) {

        if (store.getId().length() > 0 && store.getId().length() < 20
                && store.getId().matches("[+]?\\d*\\.?\\d+")) {
            if (store.getDesc().length() > 0 && store.getDesc().length() < 101) {
                if (store.getIdParent().length() > 0 && store.getIdParent().length() < 101) {
                    if (store.getGroup().length() > 0 && store.getGroup().matches("[+]?\\d*\\.?\\d+")
                            && exitsGroup(store.getGroup())) {
                        if (store.getBusiness().length() > 0 && store.getBusiness().matches("[+]?\\d*\\.?\\d+")
                                && exitsBusiness(store.getBusiness())) {

                            Long count = StoreEmployee.count("idStore = ? \n"
                                    + " and structureDate IN (select max(m.structureDate) from StoreEmployee m)",
                                    Integer.parseInt(store.getId()));
                            if (count > 0) {
                                store.setType("update");
                            } else {
                                store.setType("insert");
                            }
                        } else {
                            getErrores().add(store.getId() + ","
                                    + store.getDesc() + "," + store.getIdParent() + ","
                                    + store.getGroup() + "," + store.getBusiness() + ","
                                    + ",El id negocio no existe");
                            return false;
                        }
                    } else {
                        getErrores().add(store.getId() + ","
                                + store.getDesc() + "," + store.getIdParent() + ","
                                + store.getGroup() + "," + store.getBusiness() + ","
                                + ",El id negocio no existe");
                        return false;
                    }
                } else {
                    getErrores().add(store.getId() + ","
                            + store.getDesc() + "," + store.getIdParent() + ","
                            + store.getGroup() + "," + store.getBusiness() + ","
                            + ",El id zona no cumple con las reglas de negocio");
                    return false;
                }
            } else {
                getErrores().add(store.getId() + ","
                        + store.getDesc() + "," + store.getIdParent() + ","
                        + store.getGroup() + "," + store.getBusiness() + ","
                        + ",La descripción de tienda no es valida");
                return false;
            }
        } else {
            getErrores().add(store.getId() + ","
                    + store.getDesc() + "," + store.getIdParent() + ","
                    + store.getGroup() + "," + store.getBusiness() + ","
                    + ",El id de tienda no es valido");
            return false;
        }

        return true;
    }

    public List<String> getErrores() {
        return errores;
    }

    public void setErrores(List<String> errores) {
        this.errores = errores;
    }

    private boolean exitsGroup(String group) {
        NpsGroup g = NpsGroup.findById(Integer.parseInt(group));
        if (g != null) {
            return true;
        } else {
            return false;
        }
    }

    private boolean exitsBusiness(String business) {
        NpsBusiness b = NpsBusiness.findById(Integer.parseInt(business));
        return b != null;
    }

    private void updateAnswers(Integer store, String group, String business,
            String idParent, boolean update) {
        try {
            if (update) {
                String statement = "UPDATE PORTALUNICO.PUL_NPS_ANSWER answ\n"
                        + "SET answ.FC_ZONE = ?1 , answ.FN_STORE_GROUP_ID = ?2 , \n"
                        + "answ.FN_STORE_BUSINESS_ID = ?3 WHERE answ.FN_ID_STORE = ?4 ";

                Query query = JPA.em().
                        createNativeQuery(statement);
                query.setParameter(1, idParent);
                query.setParameter(2, Integer.parseInt(group));
                query.setParameter(3, Integer.parseInt(business));
                query.setParameter(4, store);
                int total = query.executeUpdate();
            }
        }catch(Exception e){
            Logger.error(e.getMessage());
        }
    }

    private void updateNotSale(Integer store, String idParent, boolean update) {
        if (update) {
            String statement = "UPDATE PORTALUNICO.PUL_NPS_EMP_NOT_SALES notSales "
                    + "SET notSales.FC_ZONE = ?1 "
                    + "WHERE notSales.FN_ID_STORE = ?2 ";

            Query query = JPA.em().
                    createNativeQuery(statement);
            query.setParameter(1, idParent);
            query.setParameter(2, store);
            int total = query.executeUpdate();
        }
    }
}
