package utils.nps.readFile;
/**
 * @(#)ReadTxtFile 14/02/2017
 */

/**
 *
 * Class for read txt file from FTP
 * 
 * @author Rodolfo Miranda (Qualtop)
 * 
 */

import SchedulerTask.nps.ProcessRHStruct;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStreamReader;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import models.nps.FilesLoadNps;
import models.nps.NpsAnswerTmp;
import utils.nps.enums.TxtField;

public class ReadTxtFile {
    
    //Variable name of the file txt
    private File file;
    
    //Constructor with a file variable
    public ReadTxtFile(){
    
    }
    
    public void setFileReader(File file){
        this.file = file;
    }
    
    /**
     * Description :
     *  Method for Initialize the bufferedReader of file name on FTP
     * @return BufferedReader
     * @throws FileNotFoundException 
     */
    private BufferedReader getBufferReader() throws FileNotFoundException, IOException{
        BufferedReader br = null;
       
            br = new BufferedReader( new InputStreamReader(new FileInputStream(file),"Cp1252") );
            //br.close();
            
        
        return br;
    }
    
    /**
     * Description:
     * Method for read txt file
     */
    public boolean readTxtFile(ProcessRHStruct process, boolean print, Long loadFile){
        
        BufferedReader br       = null;
        List<NpsAnswerTmp> list = new ArrayList<>();
        
        try {
           
            br              = getBufferReader();
            String line     = "";
            String fields[] = {};
            
            Long total = 0l;
            while (br.readLine() != null) total++;
            br.close();
            br              = getBufferReader();
                       
            final int percent = 25;
            float factor = (float) 65 / total;
            int i = 1;
            int j = 1;
            // Load to DB
                
            while( (line = br.readLine()) != null ){
                line = line.replace("\"", "");
                fields = line.split("\\|");
                NpsAnswerTmp registry = new NpsAnswerTmp();
                
                if (fields.length < 34) {
                    registry.columns = fields.length;
                    registry.save();
                    continue;
                }
                               
                if( fields[TxtField.EMPID.getI()] != null){
                                                           
                    for( TxtField field : Arrays.copyOfRange(TxtField.values(), 0, fields.length )){
                        switch(field){
                            case EMPID:
                                registry.empId = fields[field.getI()] != null ?
                                        fields[field.getI()].trim() : null;
                                
                                break;
                            case AREA:
                                registry.area = fields[field.getI()] != null ?
                                        fields[field.getI()].trim() : null;
                                
                                break;
                            case PRACTICES:
                                registry.practice = fields[field.getI()] != null ?
                                        fields[field.getI()].trim() : null;
                                
                                break;
                            case YEAR:
                                registry.year = fields[field.getI()] != null ?
                                        fields[field.getI()].trim() : null;
                                
                                break;
                            case MONTH:
                                registry.month = fields[field.getI()] != null ?
                                        fields[field.getI()].trim() : null;
                                
                                break;
                            case DAY:
                                registry.day = fields[field.getI()] != null ?
                                        fields[field.getI()].trim() : null;
                                
                                break;
                            case ID_PERSON_EVALUATED:
                                registry.personEvaluated = fields[field.getI()] != null ?
                                        fields[field.getI()].trim() : null;
                                
                                break;
                            case ANSWER_1:
                                registry.answer1 = fields[field.getI()] != null ?
                                        fields[field.getI()].trim() : null;
                                
                                break;
                            case ANSWER_2:
                                registry.answer2 = fields[field.getI()] != null ?
                                        fields[field.getI()].trim() : null;
                                
                                break;
                            case ANSWER_3:
                                registry.answer3 = fields[field.getI()] != null ?
                                        fields[field.getI()].trim() : null;
                                
                                break;
                            case RECORDING:
                                registry.recording = fields[field.getI()] != null ?
                                        fields[field.getI()].trim() : null;
                                
                                break;
                            case CLIENT_ID:
                                registry.clientId = fields[field.getI()] != null ?
                                        fields[field.getI()].trim() : null;
                                
                                break;
                            case ID_CATEGORY_1_1:
                                registry.category1_1 = fields[field.getI()] != null ?
                                        fields[field.getI()].trim() : null;
                                
                                break;
                            case ID_CATEGORY_1_2:
                                registry.category1_2 = fields[field.getI()] != null ?
                                        fields[field.getI()].trim() : null;
                                
                                break;
                            case ID_CATEGORY_1_3:
                                registry.category1_3 = fields[field.getI()] != null ?
                                        fields[field.getI()].trim() : null;
                                
                                break;
                            case CLIENT_NAME:
                                registry.clientName = fields[field.getI()] != null ?
                                        fields[field.getI()].trim() : null;
                                
                                break;
                            case LADA:
                                registry.lada = fields[field.getI()] != null ?
                                        fields[field.getI()].trim() : null;
                                
                                break;
                            case PHONE:
                                registry.phone = fields[field.getI()] != null ?
                                        fields[field.getI()].trim() : null;
                                
                                break;
                            case ID_STORE:
                                registry.store = fields[field.getI()] != null ?
                                        fields[field.getI()].trim() : null;
                                
                                break;
                            case ANSWER_4:
                                registry.answer4 = fields[field.getI()] != null ?
                                        fields[field.getI()].trim() : null;
                                
                                break;
                            case EMAIL:
                                registry.email = fields[field.getI()] != null ?
                                        fields[field.getI()].trim() : null;
                                
                                break;
                            case ANSWER_5:
                                registry.answer5 = fields[field.getI()] != null ?
                                        fields[field.getI()].trim() : null;
                                
                                break;
                            case SURVEY_TYPE:
                                registry.surveyType = fields[field.getI()] != null ?
                                        fields[field.getI()].trim() : null;
                                
                                break;
                            case ID_CATEGORY_2_1:
                                registry.category2_1 = fields[field.getI()] != null ?
                                        fields[field.getI()].trim() : null;
                                
                                break;
                            case ID_CATEGORY_2_2:
                                registry.category2_2 = fields[field.getI()] != null ?
                                        fields[field.getI()].trim() : null;
                                
                                break;
                            case ID_CATEGORY_2_3:
                                registry.category2_3 = fields[field.getI()] != null ?
                                        fields[field.getI()].trim() : null;
                                
                                break;
                            case DOC:
                                registry.doc = fields[field.getI()] != null ?
                                        fields[field.getI()].trim() : null;
                                
                                break;
                            case TERMINAL:
                                registry.terminal = fields[field.getI()] != null ?
                                        fields[field.getI()].trim() : null;
                                
                                break;
                            case TICKET_DAY:
                                registry.ticketDay = fields[field.getI()] != null ?
                                        fields[field.getI()].trim() : null;
                                
                                break;
                            case TICKET_MONTH:
                                registry.tickectMonth = fields[field.getI()] != null ?
                                        fields[field.getI()].trim() : null;
                                
                                break;
                            case TICKET_YEAR:
                                registry.ticketYear = fields[field.getI()] != null ?
                                        fields[field.getI()].trim() : null;
                                
                                break;
                            case IP:
                                registry.ip = fields[field.getI()] != null ?
                                        fields[field.getI()].trim() : null;
                                
                                break;
                            case SKU:
                                registry.sku = fields[field.getI()] != null ?
                                        fields[field.getI()].trim() : null;
                                
                                break;
                            case SKU_DESC:
                                registry.skuDesc = fields[field.getI()] != null ?
                                        fields[field.getI()].trim() : null;
                                
                                break;
                        }
                    }
                    registry.columns = fields.length;
                }
                //registry.idTmp = StructureTmpQuery.getNextValChallenge();
                //list.add(registry);
                
                registry.save();
                
                if (print) {
                    if( ( factor * i  ) >= 1.0 ){
                        process.updatePercent(percent + j, loadFile);
                        i = 0;
                        j++;
                    }
                    i++;
                }
            }
            return true;
        } catch (IOException ex) {
            Logger.getLogger(ReadTxtFile.class.getName()).
                    log(Level.SEVERE, null, ex);
            
            return false;
        }finally {
            if( br != null ){
                try {      
                    br.close();
                } catch (IOException ex) {
                    Logger.getLogger(ReadTxtFile.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        }
    }
}
