/**
 * @(#) ReadCatAreaFile 19/05/2017
 */

package utils.nps.readFile;

import com.opencsv.CSVReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import models.nps.NpsArea;
import models.nps.StructureTmp;
import utils.nps.util.FormatDateUtil;
import utils.nps.enums.Field;
import utils.nps.model.Area;

/**
 * Clase de utilidad para leer los archivos del catalogo de Areas
 * @author Rodolfo Miranda -- Qualtop
 */
public class ReadCatAreaFile {

    private File file;
    private List<String> errores;    
    public ReadCatAreaFile(){
        this.errores = new ArrayList<>();
    }
    
    public void setFileReader(File file){
        this.file = file;
    }
        /**
     * Description :
     *  Method for Initialize the bufferedReader of file name on FTP
     * @return BufferedReader
     * @throws FileNotFoundException 
     */
    private CSVReader getBufferReader() {
        if( file != null ){
            try {
                CSVReader br = null;
                br = new CSVReader( new InputStreamReader(new FileInputStream(file),"Cp1252") );
                return br;
            } catch (FileNotFoundException ex) {
                Logger.getLogger(ReadCsvFile.class.getName()).log(Level.SEVERE, null, ex);
            } catch (UnsupportedEncodingException ex) {
                Logger.getLogger(ReadCsvFile.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        return null;
        
    }
    
    /**
     * Metodo principal para la lectura de los registros.
     * @param userId
     * @return 
     */
    
    public List<NpsArea> readCvsFile(String userId){
        String [] line;
        
        boolean valid       = true;
        List<NpsArea> areas = new ArrayList<>();
        CSVReader br        = getBufferReader();
                
        try{
            
            while ((line = br.readNext()) != null) {
                //init check since 3 line
                if( !line[0].contains("ID_")){

                    Area area = new Area();
                    if( line.length > 1 ){
                        area.setId(line[0].trim());
                        area.setDesc(line[1].trim());
                        valid = vaidateArea(area);
                        if( valid ){
                            NpsArea a = null;
                            if( area.getType().equalsIgnoreCase("insert") ){
                                a = new NpsArea();
                                a.creFor = userId.toUpperCase();
                                a.creDate = FormatDateUtil.ownFormat.
                                parse(FormatDateUtil.ownFormat.format(new Date()));
                            }else if( area.getType().equalsIgnoreCase("update") ){
                                a = NpsArea.findById(area.getId());
                                a.modFor = userId.toUpperCase();
                                a.modDate = FormatDateUtil.ownFormat.
                                parse(FormatDateUtil.ownFormat.format(new Date()));
                            }
                            a.idArea = area.getId();
                            a.desc = area.getDesc();
                            areas.add(a);
                        }
                    }else{
                        getErrores().add(line[0]+","+
                            ",La descripción es mayor a 255 caracteres o viene nulo");
                    }        
                }
            }
        } catch (FileNotFoundException ex) {
            Logger.getLogger(ReadCsvFile.class.getName()).log(Level.SEVERE,
                    null, ex);
        } catch (IOException ex) {
            Logger.getLogger(ReadCsvFile.class.getName()).log(Level.SEVERE,
                    null, ex);
        } catch (ParseException ex) {
            Logger.getLogger(ReadCatAreaFile.class.getName()).log(Level.SEVERE, null, ex);
        }finally{
            try {
                br.close();
            } catch (IOException ex) {
                Logger.getLogger(ReadCatAreaFile.class.getName())
                        .log(Level.SEVERE, null, ex);
            }
        }
        
        return areas;
    }

    /**
     * Validar el area en la base
     * @param area
     * @return 
     */
    
    private boolean vaidateArea(Area area) {
        
        if( area.getId().length() > 0 && area.getId().length() < 11 ){
            if( area.getDesc().length() > 0 && area.getDesc().length() < 256 ){
                NpsArea npsArea = NpsArea.findById(area.getId());
                if( npsArea != null ){
                    area.setType("update");
                }else{
                    area.setType("insert");
                }
            }else{
                getErrores().add(area.getId()+","+
                    area.getDesc()+",La descripción es mayor a 255 caracteres o viene nulo");
                return false;
            }
        }else{
            getErrores().add(area.getId()+","+
                    area.getDesc()+",El id es mayor a 10 caracteres o viene nulo");
            return false;
        }
        
        return true;
    }

    public List<String> getErrores() {
        return errores;
    }

    public void setErrores(List<String> errores) {
        this.errores = errores;
    }
}
