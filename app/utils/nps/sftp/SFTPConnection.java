/**
 * @(#) SFTPConnection 16/03/2017
 */

package utils.nps.sftp;

import com.jcraft.jsch.Channel;
import com.jcraft.jsch.ChannelSftp;
import com.jcraft.jsch.ChannelSftp.LsEntry;
import com.jcraft.jsch.JSch;
import com.jcraft.jsch.JSchException;
import com.jcraft.jsch.Session;
import com.jcraft.jsch.SftpATTRS;
import com.jcraft.jsch.SftpException;
import java.io.FileOutputStream;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Properties;
import java.util.Vector;
import utils.nps.vars.EnvironmentVar;

/**
 * Class for SFTP connection
 * @author Rodolfo Miranda -- Qualtop
 */
public class SFTPConnection {

    private ChannelSftp sftp;
    private Session sftpSession;
    
    private final String host;  
    private final String user;  
    private final String port;
    private final String password;
    private final String directory;
    
    /**
     * Constructor with parameters
     * @param host
     * @param user
     * @param password
     * @param port
     * @param directory 
     */
    public SFTPConnection(String host, String user, String password,String port,
            String directory){
        this.host = host;
        this.user = user;
        this.port = port;
        this.password = password;
        this.directory = directory;
    }
    
    /**
     * Configuration Sftp Method
     */
    public void configSftp() throws JSchException{
        JSch jsch = new JSch();
        
         
            sftpSession = jsch.getSession(user, host,
                    Integer.valueOf(port));
            
            Properties config   = new Properties(); 
        
            config.put("StrictHostKeyChecking", "no");
            sftpSession.setConfig(config);
            sftpSession.setConfig( "PreferredAuthentications", "password");
            sftpSession.setPassword(password);
            sftpSession.connect();
            
            Channel channel  = sftpSession.openChannel("sftp");
            sftp = ( ChannelSftp ) channel;
            sftp.connect();
            
        
    }
    
    /**
     * Change directory Sftp Method
     * @param directory 
     */
      public void changeDirectory(String directory) throws SftpException {
            sftp.cd(directory);
    }
    
    /**
     * Return the InputStream that contain the file in SFTP server
     * @param fileName
     * @param out
     * @throws com.jcraft.jsch.SftpException
     */
    public void getFile(String fileName, FileOutputStream out) 
            throws SftpException{
            
            sftp.get(fileName,out);
    }
    
    public void putFile(String src, String dst) 
            throws SftpException{
            
            sftp.put(src,dst,ChannelSftp.OVERWRITE);
    }
    
    public void rmFile(String src) 
            throws SftpException{
            
            sftp.rm(src);
    }
    
    /**
     * Metodo que identifica cuantos archivos de carga de evaluaciones hay en el
     * sftp
     * @return
     * @throws SftpException 
     */
    public List<String> getVectorFiles(String path) throws SftpException{
        String filename = "";
        String extension = "";
        String suffix = "";
        List<String> filesName = new ArrayList<>();
            Vector<ChannelSftp.LsEntry> v = sftp.ls(path);
            for( LsEntry v1:v ){
                SftpATTRS att = v1.getAttrs();
                if( !att.isDir() ){
                    filename = v1.getFilename();
                    int i = filename.lastIndexOf('.');
                    if (i >= 0) {
                        extension = filename.substring(i+1);
                    }
                    int j = filename.indexOf("_");
                    suffix = filename.substring(0, j);
                
                    if( extension.equalsIgnoreCase(EnvironmentVar.TXT_EXT) &&
                            suffix.equalsIgnoreCase(EnvironmentVar.TXT_SUF)){
                        filesName.add(filename);
                    }
                }
            }

        return filesName;
    }
    
    /**
     * Metodo para saber si hay archivo de estructura
     * @param path
     * @return
     * @throws SftpException 
     */
    public String getStructureFile(String path) throws SftpException{
        String filename = "";
        String extension = "";
        String suffix = "";
        
            Vector<ChannelSftp.LsEntry> v = sftp.ls(path);
            for( LsEntry v1:v ){
                SftpATTRS att = v1.getAttrs();
                if( !att.isDir() ){
                    filename = v1.getFilename();
                    int i = filename.lastIndexOf('.');
                    if (i >= 0) {
                        extension = filename.substring(i+1);
                    }
                    
                    String[] t = filename.split("\\d");
                    suffix = t[0];
                
                    if( suffix.equalsIgnoreCase(EnvironmentVar.CSV_SUF )  &&
                            extension.equalsIgnoreCase(EnvironmentVar.CSV_EXT)){
                        return filename;
                    }
                }
            }

        return "";
    }
    
    /**
     * MEtodo para identificar el archivo de secciones en el SFTP
     * @param path
     * @return
     * @throws SftpException 
     */
    public String getSectionsFile(String path) throws SftpException{
        String filename = "";
        String extension1 = "";
        String extension2 = "";
        String suffix = "";
        String date = "";    
            Vector<ChannelSftp.LsEntry> v = sftp.ls(path);
            for( LsEntry v1:v ){
                SftpATTRS att = v1.getAttrs();
                if( !att.isDir() ){
                    filename = v1.getFilename();
                    int i = filename.lastIndexOf('.');
                    if (i >= 0) {
                        extension2 = filename.substring(i+1);
                    }
                    extension1 = filename.substring(0,i);
                    String[] t = extension1.split("\\d");
                    suffix = t[0];
                    date = extension1.substring(suffix.length(), extension1.length());
                    SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyMMdd");
                    Date today = Calendar.getInstance().getTime();
                    String formatDate = simpleDateFormat.format(today);
                    
                    if( ( extension2.equalsIgnoreCase(EnvironmentVar.XLS_EXT) 
                            ||  extension2.equalsIgnoreCase(EnvironmentVar.XLSX_EXT)) &&
                            suffix.equalsIgnoreCase(EnvironmentVar.XLS_SUF) && 
                            date.equalsIgnoreCase(formatDate)){
                        return filename;
                    }
                }
            }

        return "";
    }
    
    public boolean getStatusChannel() {
        return sftp.isConnected();
    }
    
    public boolean getStatusSession() {
        return sftpSession.isConnected();
    }
    
    public String getPath() throws SftpException{
        return sftp.pwd();
    }
    
    /**
     * Close all open connections
     */
    public void disconnect() {
        if( sftp != null ) {
            sftp.exit();
            sftp.disconnect();
        }
        if(sftpSession != null) {
            sftpSession.disconnect();
        }
        
    }

    public void reconectSession() throws JSchException {
        sftpSession.connect();
    }
    
    public void reconectChannel() throws JSchException {
        sftp.connect();
    }
}
