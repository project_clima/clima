package utils;

import java.io.ByteArrayOutputStream;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.Map;
import net.sf.jxls.exception.ParsePropertyException;
import net.sf.jxls.transformer.XLSTransformer;
import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.apache.poi.ss.usermodel.Workbook;
import play.Logger;
import play.vfs.VirtualFile;
import play.mvc.Http.Response;
import play.mvc.Scope.RenderArgs;

public class RenderExcel {
    public static final String RA_FILENAME = "__FILE_NAME__";
    public final String template;
    public final Map<String, Object> beans;
    public final String fileName;
    
    public RenderExcel(String templateName) {
        this.template = templateName;        
        this.fileName = fileName_(templateName);
        this.beans    = beans_();
    }
    
    private Map<String, Object> beans_() {
        if (RenderArgs.current().data.containsKey(RA_FILENAME)) {
            RenderArgs.current().data.remove(RA_FILENAME);
        }
        
        return RenderArgs.current().data;
    }
    
    private String fileName_(String path) {
        if (RenderArgs.current().data.containsKey(RA_FILENAME)) 
            return RenderArgs.current().get(RA_FILENAME, String.class);
        int i = path.lastIndexOf("/");
        if (-1 == i)
            return path;
        return path.substring(++i);
    }
    
    public void render()
    throws ParsePropertyException, InvalidFormatException, IOException {
        Response response = Response.current();
        
        VirtualFile templateVirtual = VirtualFile.fromRelativePath(this.template);
        XLSTransformer transformer  = new XLSTransformer();
        
        Workbook workbook = transformer.transformXLS(
            new FileInputStream(templateVirtual.getRealFile()),
            this.beans
        );
        
        ByteArrayOutputStream os = new ByteArrayOutputStream();
        workbook.write(os);
        
        response.setHeader("Content-Type", "application/vnd.ms-excel");
        response.setHeader("Content-disposition", "attachment; filename=" + this.fileName);        
        response.out.write(os.toByteArray());
    }
    
    public void renderWithHide(Integer... remove)
    throws ParsePropertyException, InvalidFormatException, IOException {
        Response response = Response.current();
        
        VirtualFile templateVirtual = VirtualFile.fromRelativePath(this.template);
        XLSTransformer transformer  = new XLSTransformer();
        
        Workbook workbook = transformer.transformXLS(
            new FileInputStream(templateVirtual.getRealFile()),
            this.beans
        );
        
        if (remove.length > 0 && remove[0] != null) {
            try {
                workbook.removeSheetAt(remove[0]);
            } catch (Exception e) {
                Logger.error(e.getMessage());
            }
        }
        
        ByteArrayOutputStream os = new ByteArrayOutputStream();
        workbook.write(os);
        
        response.setHeader("Content-Type", "application/vnd.ms-excel");
        response.setHeader("Content-disposition", "attachment; filename=" + this.fileName);        
        response.out.write(os.toByteArray());
    }
}
