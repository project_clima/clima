package utils;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.mail.MessagingException;

import json.nps.dto.EvaluationDto;
import models.NpsAnswer;
import models.Usuario;
import play.Logger;
import play.Play;

public class NpsMailUtils {

    public static String ROW = "<tr style='height: 25px; %row.style%'>"
        + "<td style='border-spacing:0px; text-align: left; font-size: 11px; ' width='200' >%adviser.name%</td> "
        + "<td style='border-spacing:0px; text-align: center; font-size: 11px; ' width='90'>%evaluations%</td> "
        + "<td style='border-spacing:0px; text-align: center; font-size: 11px; ' width='70'>%evaluations.complaints%</td> "
        + "<td style='border-spacing:0px; text-align: center; font-size: 11px; ; color: #E46C0A;' width='70'>%evaluations.open%</td> "
        + "<td style='border-spacing:0px; text-align: center; font-size: 11px; ; color: #CC9900;' width='85'>%evaluations.tracing%</td> "
        + "<td style='border-spacing:0px; text-align: center; font-size: 11px; ; color: #008000;' width='85'>%evaluations.solved%</td> ";

    public static String prepareHtmlRow(String template, String name, int totalEvaluations, int complaints, int open,
            int tracing, int solved, int row) {

        HashMap<String, Object> vars = new HashMap();

        vars.put("%adviser.name%", name);
        vars.put("%evaluations%", totalEvaluations);
        vars.put("%evaluations.complaints%", complaints);
        vars.put("%evaluations.open%", open);
        vars.put("%evaluations.tracing%", tracing);
        vars.put("%evaluations.solved%", solved);
        vars.put("%row.style%", (row % 2) == 0 ? "" : "background-color:#E8E8E8;");

        if (template != null) {
            for (String key : vars.keySet()) {

                template = template.replace(key, vars.get(key) != null ? vars.get(key).toString() : "");
            }
        }

        return template;
    }

    public static String prepareEmail(String template, String name, String body, String footer) {

        HashMap<String, Object> vars = new HashMap();

        String baseUrl = Play.configuration.getProperty("application.baseUrl");
        String evaluationsUrl = Play.configuration.getProperty("application.evaluationsUrl");

        vars.put("%employee.name%", name);
        vars.put("%table.body%", body);
        vars.put("%table.footer%", footer);
        vars.put("%baseUrl%", baseUrl);
        vars.put("%evaluation.detail%", evaluationsUrl);

        if (template != null) {
            for (String key : vars.keySet()) {

                template = template.replace(key, vars.get(key) != null ? vars.get(key).toString() : "");
            }
        }

        return template;
    }

    public static EvaluationDto addRow(Long idUserChief, List<NpsAnswer> evaluations) {

        int totalEvaluations = evaluations.size();
        int totalComplaints = 0;
        int totalOpen = 0;
        int totalTracing = 0;
        int totalSolved = 0;

        for (NpsAnswer evaluation : evaluations) {
            int serviceScore = NumberConverterUtils.getValuefromText(evaluation.questionsAnswers.get(0).textAnswer);
            int recomendationScore = NumberConverterUtils
                    .getValuefromText(evaluation.questionsAnswers.get(1).textAnswer);

            if (serviceScore <= 8 || recomendationScore <= 8)
                totalComplaints++;

            Long idFollow = evaluation.idFollow;

            // Si no tiene id de seguimiento es porque
            // su calificación es mayor o igual a 9
            if (idFollow != null) {
                switch (idFollow.intValue()) {
                case 1: // Abiertas
                    totalOpen++;
                    break;
                case 2: // En seguimiento
                    totalTracing++;
                    break;
                case 3: // Solventadas
                    totalSolved++;
                    break;
                default:
                    break;
                }
            }
        }

        Usuario user = Usuario.findById(idUserChief);

        String fullName = "-- --";
        if (user != null) {
            fullName = user.apellidos + " " + user.nombre;
        }

        return new EvaluationDto(fullName, totalEvaluations, totalComplaints, totalOpen, totalTracing, totalSolved);
    }

    public static EvaluationDto addRow(Long idUserChief, List<NpsAnswer> evaluations, int complaints) {

        int totalEvaluations = evaluations.size();
        int totalOpen = 0;
        int totalTracing = 0;
        int totalSolved = 0;

        for (NpsAnswer evaluation : evaluations) {

            Long idFollow = evaluation.idFollow;

            if (idFollow != null) {
                switch (idFollow.intValue()) {
                case 1: // Abiertas
                    totalOpen++;
                    break;
                case 2: // En seguimiento
                    totalTracing++;
                    break;
                case 3: // Solventadas
                    totalSolved++;
                    break;
                default:
                    break;
                }
            }
        }

        Usuario user = Usuario.findById(idUserChief);

        String fullName = "-- --";
        if (user != null) {
            fullName = user.apellidos + " " + user.nombre;
        }

        return new EvaluationDto(fullName, totalEvaluations, complaints, totalOpen, totalTracing, totalSolved);
    }

    public static Map<String, Integer> addTotals(Map<String, Integer> map, Integer value, String key) {

        Integer total = map.get(key);
        if (total == null)
            total = 0;

        total = total + value;

        map.put(key, total);

        return map;
    }

    public static void sendMail(String bodyMail, String email) {
        try {
            GmailMailer mailer = new GmailMailer();
            mailer.connect();

            mailer.send(
            		email, // TODO comentar para pruebas
                     //"emartinez@lean4.it", //TODO solo para pruebas
                    "Resumen de evaluaciones", bodyMail, -1, "");
        } catch (MessagingException e) {
            Logger.error("Ocurrió un error al enviar los correos ", e.fillInStackTrace());
            e.printStackTrace();
        }
    }

}
