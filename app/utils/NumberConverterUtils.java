package utils;

public class NumberConverterUtils {

    /**
     * Parse text to number, if NumberFormatException occurs, then return -1
     * 
     * @param text
     * @return value of text, if NumberFormatException occurs return -1
     */
    public static int getValuefromText(String text) {
        int result;
        try {
            result = Integer.valueOf(text);
        } catch (NumberFormatException e) {
            result = -1;
        }

        return result;
    }
}
