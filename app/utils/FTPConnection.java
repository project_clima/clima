package utils;

import com.ibm.as400.access.FTP;
import java.io.IOException;
import java.io.File;

public class FTPConnection {
    private FTP client = null;
    
    public FTPConnection(String host, String user, String password) {
        client = new FTP(host, user, password);
    }
    
    public void changeDirectory(String directory) {
        try {
            client.cd(directory);
            System.out.println("Directory changed: " + directory);
        } catch (IOException e) {
            System.out.println("Ocurrió un error al cambiar de directorio: " + e.getMessage());
        }
    }
    
    public void getFile(String remoteFile, String localFile) {
        try {
            if (client.get(remoteFile, localFile)) {
                System.out.println("Archivo transferido: " + remoteFile);
            } else {
                System.out.println("Archivo no transferido: " + remoteFile);
            }
        } catch (IOException e) {
            System.out.println("Error al transferir el archivo: " + e.getMessage());
        }
    }
    
    public void sendFile(String remoteFile, File localFile) {
        try {
            if (client.put(localFile, remoteFile)) {
                System.out.println("Archivo transferido: " + remoteFile);
            } else {
                System.out.println("Archivo no transferido: " + remoteFile);
            }
        } catch (IOException e) {
            System.out.println("Error al transferir el archivo: " + e.getMessage());
        }
    }
    
    public void disconnect() {
        try {
            client.disconnect();
            System.out.println("Conexión cerrada");
        } catch (IOException e) {
            System.out.println("Error al cerrar la conexión" + e.getMessage());
        }
    }
}
