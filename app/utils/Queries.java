package utils;

import java.math.BigDecimal;
import play.*;
import play.db.DB;
import play.db.jpa.JPA;
import java.util.*;
import javax.persistence.Query;
import java.sql.CallableStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import models.PermisosEspeciales;
import oracle.jdbc.OracleCallableStatement;
import oracle.jdbc.OracleConnection;
import oracle.jdbc.OracleTypes;
import org.springframework.jdbc.support.nativejdbc.C3P0NativeJdbcExtractor;

import controllers.Seguridad;
import oracle.jdbc.OracleTypes;

public class Queries {

    private static final String schema = Play.configuration.getProperty("db.default.default_schema");

    public static List getUserRoles(Long id, String place) {

    	String queryStatement = "";
    	
    	Logger.info("value of place ::::::::: %s", place);
    	
    	if(place != null && place.equalsIgnoreCase("CSI"))
    		queryStatement = getRolesCSI(id);
    	else {
    		queryStatement = getRolesClima(id);	
    	}

    	Logger.info("value of statement ::::::::: %s", queryStatement);
    	
        
        Query query = JPA.em().createNativeQuery(queryStatement);
        return query.getResultList();
    }
    
    private static String getRolesClima(Long id) {
    	return "SELECT R.*, COALESCE(RU.FN_ID_ROLE, 0) TIENE_ROL"
                + " FROM PUL_ROLE R"    			
                + " LEFT JOIN PUL_USER_ROLE RU"
                + " ON RU.FN_ID_ROLE = R.FN_ID_ROLE AND RU.FN_ID_USER = " + id + " where R.FN_ID_MODULE = " + Seguridad.MODULE_CLIMA
                + " ORDER BY R.FC_NAME_ROLE ASC";
    }
    
    private static String getRolesCSI(Long id) {
    	return "SELECT R.*, COALESCE(RU.FN_ID_ROLE, 0) TIENE_ROL"
                + " FROM PUL_ROLE R"                
                + " LEFT JOIN PUL_USER_ROLE RU "
                + " ON RU.FN_ID_ROLE = R.FN_ID_ROLE AND RU.FN_ID_USER = " + id + " where R.FN_ID_MODULE = " + Seguridad.MODULE_CSI                
                + " ORDER BY R.FC_NAME_ROLE ASC";
    }
    

    public static PagingResult getLogs(String conditions, String order, int start, int end) {
        String statement = "SELECT *"
                + " FROM"
                + " ("
                + " SELECT "
                + " TRIM(LOGUSR) LOGUSR, TRIM(LOGDES) LOGDES, LOGUPD, ROWNUM ROW_NUM"
                + " FROM " + schema + ".SELOG WHERE " + conditions
                + ") BATT WHERE ROW_NUM BETWEEN " + start + " AND " + end + " " + order;

        Query oQuery = JPA.em().createNativeQuery(statement);

        return new PagingResult(getCountLogs(conditions), oQuery.getResultList());
    }

    public static long getCountLogs(String conditions) {
        String statement = "SELECT"
                + " COUNT(*) "
                + "FROM " + schema + ".SELOG WHERE " + conditions;

        Query oQuery = JPA.em().createNativeQuery(statement);
        List result = oQuery.getResultList();
        return Long.parseLong(result.get(0).toString());
    }

    public static OracleConnection getOracleConnection() throws SQLException {
        C3P0NativeJdbcExtractor cp30NativeJdbcExtractor = new C3P0NativeJdbcExtractor();
        return (OracleConnection) cp30NativeJdbcExtractor.getNativeConnection(DB.getConnection());
    }

    public static ResultSet planAvance(long userId, long plan) throws SQLException {
        OracleConnection connection = getOracleConnection();
        CallableStatement statement = connection.prepareCall("BEGIN PORTALUNICO.PR_PLAN_AVANCE(?, ?, ?); END;");

        statement.setLong(1, userId);
        statement.setLong(2, plan);
        statement.registerOutParameter(3, OracleTypes.CURSOR);
        statement.execute();

        ResultSet results = ((OracleCallableStatement) statement).getCursor(3);

        return results;
    }

    public static PagingResult getNivelN(String structureDate, String usuario, String nivel, String conditions, String order, int start, int end) {


        String statement="SELECT *\n" +
"  FROM (SELECT (SELECT FC_TITLE\n" +
"                  FROM PUL_EMPLOYEE_CLIMA PEM\n" +
"                 WHERE PEM.FN_USERID = TABLEQ.FN_MANAGER\n" +
"                   AND TO_CHAR(FD_STRUCTURE_DATE, 'MM/YYYY') =\n" +
"                       '" + structureDate+ "') TITLE_MANAGER,\n" +
"               DECODE(FC_DIVISION,\n" +
"                      'CORP',\n" +
"                      FC_TITLE,\n" +
"                      FC_TITLE || ' - ' || FC_DIVISION) FC_TITLE,\n" +
"               S.FC_NAME,\n" +
"               NVL(PORTALUNICO.FN_COUNT_DEPENDENCIES_DATE(TABLEQ.FN_USERID,\n" +
"                                                          '" + structureDate+ "'),\n" +
"                   0) ALI,\n" +
"               NVL(DECODE(S.FN_ID_SURVEY_TYPE,\n" +
"                          1,\n" +
"                          SE.FN_ANSWERED,\n" +
"                          PORTALUNICO.FN_COUNT_DEPENDENCIES_ANS_DATE(TABLEQ.FN_USERID,\n" +
"                                                                     '" + structureDate+ "')),\n" +
"                   0) ANSWERED,\n" +
"               NVL((SELECT (DECODE(S.FN_ID_SURVEY_TYPE,\n" +
"                                  1,\n" +
"                                  SE.FN_ANSWERED,\n" +
"                                  PORTALUNICO.FN_COUNT_DEPENDENCIES_ANS_DATE(TABLEQ.FN_USERID,\n" +
"                                                                             '" + structureDate+ "')) /\n" +
"                          DECODE(S.FN_ID_SURVEY_TYPE,\n" +
"                                  1,\n" +
"                                  SE.FN_PLAN,\n" +
"                                  NVL(PORTALUNICO.FN_COUNT_DEPENDENCIES_DATE(TABLEQ.FN_USERID,\n" +
"                                                                             '" + structureDate+ "'),\n" +
"                                      0))) * 100\n" +
"                     FROM DUAL),\n" +
"                   0) PORCENT,\n" +
"               TABLEQ.FN_LEVEL,\n" +
"               TABLEQ.FN_MANAGER,\n" +
"               SE.FN_ID_EMPLOYEE_CLIMA,\n" +
"               NVL(DECODE(S.FN_ID_SURVEY_TYPE,\n" +
"                          1,\n" +
"                          SE.FN_PLAN,\n" +
"                          (SELECT COUNT(1)\n" +
"                             FROM PUL_EMPLOYEE_CLIMA\n" +
"                            WHERE FN_MANAGER =  " + usuario +")),\n" +
"                   0) FN_PLAN,\n" +
"               TABLEQ.FN_USERID\n" +
"          FROM PUL_SURVEY_EMPLOYEE SE,\n" +
"               PUL_SURVEY S,\n" +
"               (SELECT FN_ID_EMPLOYEE_CLIMA,\n" +
"                       FN_LEVEL,\n" +
"                       FC_TITLE,\n" +
"                       FC_DIVISION,\n" +
"                       FN_USERID,\n" +
"                       FC_LAST_NAME,\n" +
"                       FN_MANAGER\n" +
"                  FROM (SELECT *\n" +
"                          FROM PORTALUNICO.PUL_EMPLOYEE_CLIMA PE\n" +
"                         WHERE PE.FN_LEVEL =\n" +
"                               (SELECT FN_LEVEL + 1\n" +
"                                  FROM PUL_EMPLOYEE_CLIMA\n" +
"                                 WHERE FN_USERID =  " + usuario +"\n" +
"                                   AND TO_CHAR(FD_STRUCTURE_DATE, 'MM/YYYY') =\n" +
"                                       '" + structureDate+ "')\n" +
"                           AND TO_CHAR(FD_STRUCTURE_DATE, 'MM/YYYY') =\n" +
"                               '" + structureDate+ "')\n" +
"                 WHERE FN_MANAGER = " + usuario +") TABLEQ\n" +
"         WHERE SE.FN_ID_EMPLOYEE_CLIMA = TABLEQ.FN_ID_EMPLOYEE_CLIMA\n" +
"           AND SE.FN_ID_SURVEY = S.FN_ID_SURVEY\n" +
"           AND TO_CHAR(S.FD_SURVEY_DATE, 'MM/YYYY') = '" + structureDate+ "'\n" +
"           AND SE.FN_ID_EMPLOYEE_CLIMA NOT IN\n" +
"               (SELECT FN_ID_EMPLOYEE_CLIMA\n" +
"                  FROM (SELECT (SELECT FC_TITLE\n" +
"                                  FROM PUL_EMPLOYEE_CLIMA PEM\n" +
"                                 WHERE PEM.FN_USERID = TABLEQ.FN_MANAGER\n" +
"                                   AND TO_CHAR(FD_STRUCTURE_DATE, 'MM/YYYY') =\n" +
"                                       '" + structureDate+ "') TITLE_MANAGER,\n" +
"                               DECODE(FC_DIVISION,\n" +
"                                      'CORP',\n" +
"                                      FC_TITLE,\n" +
"                                      FC_TITLE || ' - ' || FC_DIVISION) FC_TITLE,\n" +
"                               S.FC_NAME,\n" +
"                               NVL(PORTALUNICO.FN_COUNT_DEPENDENCIES_DATE(TABLEQ.FN_USERID,\n" +
"                                                                          '" + structureDate+ "'),\n" +
"                                   0) ALI,\n" +
"                               NVL(DECODE(S.FN_ID_SURVEY_TYPE,\n" +
"                                          1,\n" +
"                                          SE.FN_ANSWERED,\n" +
"                                          PORTALUNICO.FN_COUNT_DEPENDENCIES_ANS_DATE(TABLEQ.FN_USERID,\n" +
"                                                                                     '" + structureDate+ "')),\n" +
"                                   0) ANSWERED,\n" +
"                               NVL((SELECT (DECODE(S.FN_ID_SURVEY_TYPE,\n" +
"                                                  1,\n" +
"                                                  SE.FN_ANSWERED,\n" +
"                                                  PORTALUNICO.FN_COUNT_DEPENDENCIES_ANS_DATE(TABLEQ.FN_USERID,\n" +
"                                                                                             '" + structureDate+ "')) /\n" +
"                                          NVL(DECODE(S.FN_ID_SURVEY_TYPE,\n" +
"                                                      1,\n" +
"                                                      SE.FN_PLAN,\n" +
"                                                      NVL(PORTALUNICO.FN_COUNT_DEPENDENCIES_DATE(TABLEQ.FN_USERID,\n" +
"                                                                                                 '" + structureDate+ "'),\n" +
"                                                          0)),\n" +
"                                               0)) * 100\n" +
"                                     FROM DUAL),\n" +
"                                   0) PORCENT,\n" +
"                               TABLEQ.FN_LEVEL,\n" +
"                               TABLEQ.FN_MANAGER,\n" +
"                               SE.FN_ID_EMPLOYEE_CLIMA,\n" +
"                               NVL(DECODE(S.FN_ID_SURVEY_TYPE,\n" +
"                                          1,\n" +
"                                          SE.FN_PLAN,\n" +
"                                          (SELECT COUNT(1)\n" +
"                                             FROM PUL_EMPLOYEE_CLIMA\n" +
"                                            WHERE FN_MANAGER =  " + usuario +")),\n" +
"                                   0) FN_PLAN,\n" +
"                               TABLEQ.FN_USERID\n" +
"                          FROM PUL_SURVEY_EMPLOYEE SE,\n" +
"                               PUL_SURVEY S,\n" +
"                               (SELECT FN_ID_EMPLOYEE_CLIMA,\n" +
"                                       FN_LEVEL,\n" +
"                                       FC_TITLE,\n" +
"                                       FC_DIVISION,\n" +
"                                       FN_USERID,\n" +
"                                       FC_LAST_NAME,\n" +
"                                       FN_MANAGER\n" +
"                                  FROM (SELECT *\n" +
"                                          FROM PORTALUNICO.PUL_EMPLOYEE_CLIMA PE\n" +
"                                         WHERE PE.FN_LEVEL =\n" +
"                                               (SELECT FN_LEVEL + 1\n" +
"                                                  FROM PUL_EMPLOYEE_CLIMA\n" +
"                                                 WHERE FN_USERID =  " + usuario +"\n" +
"                                                   AND TO_CHAR(FD_STRUCTURE_DATE,\n" +
"                                                               'MM/YYYY') =\n" +
"                                                       '" + structureDate+ "')\n" +
"                                           AND TO_CHAR(FD_STRUCTURE_DATE,\n" +
"                                                       'MM/YYYY') =\n" +
"                                               '" + structureDate+ "')\n" +
"                                    WHERE FN_MANAGER = " + usuario +") TABLEQ\n" +
"                         WHERE SE.FN_ID_EMPLOYEE_CLIMA =\n" +
"                               TABLEQ.FN_ID_EMPLOYEE_CLIMA\n" +
"                           AND SE.FN_ID_SURVEY = S.FN_ID_SURVEY\n" +
"                           AND TO_CHAR(S.FD_SURVEY_DATE, 'MM/YYYY') =\n" +
"                               '" + structureDate+ "')\n" +
"                 GROUP BY FN_ID_EMPLOYEE_CLIMA\n" +
"                HAVING COUNT(1) > 1)\n" +
"        UNION\n" +
"        SELECT (SELECT FC_TITLE\n" +
"                  FROM PUL_EMPLOYEE_CLIMA PEM\n" +
"                 WHERE PEM.FN_USERID = TABLEQ.FN_MANAGER\n" +
"                   AND TO_CHAR(FD_STRUCTURE_DATE, 'MM/YYYY') =\n" +
"                       '" + structureDate+ "') TITLE_MANAGER,\n" +
"               DECODE(FC_DIVISION,\n" +
"                      'CORP',\n" +
"                      FC_TITLE,\n" +
"                      FC_TITLE || ' - ' || FC_DIVISION) FC_TITLE,\n" +
"               S.FC_NAME,\n" +
"               NVL(PORTALUNICO.FN_COUNT_DEPENDENCIES_DATE(TABLEQ.FN_USERID,\n" +
"                                                          '" + structureDate+ "'),\n" +
"                   0) ALI,\n" +
"               \n" +
"               NVL(DECODE(S.FN_ID_SURVEY_TYPE,\n" +
"                          1,\n" +
"                          SE.FN_ANSWERED,\n" +
"                          PORTALUNICO.FN_COUNT_DEPENDENCIES_ANS_DATE(TABLEQ.FN_USERID,\n" +
"                                                                     '" + structureDate+ "')),\n" +
"                   0) ANSWERED,\n" +
"               NVL((SELECT (DECODE(S.FN_ID_SURVEY_TYPE,\n" +
"                                  1,\n" +
"                                  SE.FN_ANSWERED,\n" +
"                                  PORTALUNICO.FN_COUNT_DEPENDENCIES_ANS_DATE(TABLEQ.FN_USERID,\n" +
"                                                                             '" + structureDate+ "')) /\n" +
"                          NVL(DECODE(S.FN_ID_SURVEY_TYPE,\n" +
"                                      1,\n" +
"                                      SE.FN_PLAN,\n" +
"                                      NVL(PORTALUNICO.FN_COUNT_DEPENDENCIES_DATE(TABLEQ.FN_USERID,\n" +
"                                                                                 '" + structureDate+ "'),\n" +
"                                          0)),\n" +
"                               0)) * 100\n" +
"                     FROM DUAL),\n" +
"                   0) PORCENT,\n" +
"               TABLEQ.FN_LEVEL,\n" +
"               TABLEQ.FN_MANAGER,\n" +
"               SE.FN_ID_EMPLOYEE_CLIMA,\n" +
"               NVL(DECODE(S.FN_ID_SURVEY_TYPE,\n" +
"                          1,\n" +
"                          SE.FN_PLAN,\n" +
"                          (SELECT COUNT(1)\n" +
"                             FROM PUL_EMPLOYEE_CLIMA\n" +
"                            WHERE FN_MANAGER =  " + usuario +")),\n" +
"                   0) FN_PLAN,\n" +
"               TABLEQ.FN_USERID\n" +
"          FROM PUL_SURVEY_EMPLOYEE SE,\n" +
"               PUL_SURVEY S,\n" +
"               (SELECT FN_ID_EMPLOYEE_CLIMA,\n" +
"                       FN_LEVEL,\n" +
"                       FC_TITLE,\n" +
"                       FC_DIVISION,\n" +
"                       FN_USERID,\n" +
"                       FC_LAST_NAME,\n" +
"                       FN_MANAGER\n" +
"                  FROM (SELECT *\n" +
"                          FROM PORTALUNICO.PUL_EMPLOYEE_CLIMA PE\n" +
"                         WHERE PE.FN_LEVEL =\n" +
"                               (SELECT FN_LEVEL + 1\n" +
"                                  FROM PUL_EMPLOYEE_CLIMA\n" +
"                                 WHERE FN_USERID =  " + usuario +"\n" +
"                                   AND TO_CHAR(FD_STRUCTURE_DATE, 'MM/YYYY') =\n" +
"                                       '" + structureDate+ "')\n" +
"                           AND TO_CHAR(FD_STRUCTURE_DATE, 'MM/YYYY') =\n" +
"                               '" + structureDate+ "')\n" +
"                 WHERE FN_MANAGER = " + usuario +") TABLEQ\n" +
"         WHERE SE.FN_ID_EMPLOYEE_CLIMA = TABLEQ.FN_ID_EMPLOYEE_CLIMA\n" +
"           AND SE.FN_ID_SURVEY = S.FN_ID_SURVEY\n" +
"           AND TO_CHAR(S.FD_SURVEY_DATE, 'MM/YYYY') = '" + structureDate+ "'\n" +
"           AND S.FN_ID_SURVEY_TYPE = 1\n" +
"           AND SE.FN_ID_EMPLOYEE_CLIMA IN\n" +
"               (SELECT FN_ID_EMPLOYEE_CLIMA\n" +
"                  FROM (SELECT (SELECT FC_TITLE\n" +
"                                  FROM PUL_EMPLOYEE_CLIMA PEM\n" +
"                                 WHERE PEM.FN_USERID = TABLEQ.FN_MANAGER\n" +
"                                   AND TO_CHAR(FD_STRUCTURE_DATE, 'MM/YYYY') =\n" +
"                                       '" + structureDate+ "') TITLE_MANAGER,\n" +
"                               DECODE(FC_DIVISION,\n" +
"                                      'CORP',\n" +
"                                      FC_TITLE,\n" +
"                                      FC_TITLE || ' - ' || FC_DIVISION) FC_TITLE,\n" +
"                               S.FC_NAME,\n" +
"                               NVL(PORTALUNICO.FN_COUNT_DEPENDENCIES_DATE(TABLEQ.FN_USERID,\n" +
"                                                                          '" + structureDate+ "'),\n" +
"                                   0) ALI,\n" +
"                               NVL(DECODE(S.FN_ID_SURVEY_TYPE,\n" +
"                                          1,\n" +
"                                          SE.FN_ANSWERED,\n" +
"                                          PORTALUNICO.FN_COUNT_DEPENDENCIES_ANS_DATE(TABLEQ.FN_USERID,\n" +
"                                                                                     '" + structureDate+ "')),\n" +
"                                   0) ANSWERED,\n" +
"                               NVL((SELECT (DECODE(S.FN_ID_SURVEY_TYPE,\n" +
"                                                  1,\n" +
"                                                  SE.FN_ANSWERED,\n" +
"                                                  PORTALUNICO.FN_COUNT_DEPENDENCIES_ANS_DATE(TABLEQ.FN_USERID,\n" +
"                                                                                             '" + structureDate+ "')) /\n" +
"                                          NVL(DECODE(S.FN_ID_SURVEY_TYPE,\n" +
"                                                      1,\n" +
"                                                      SE.FN_PLAN,\n" +
"                                                      NVL(PORTALUNICO.FN_COUNT_DEPENDENCIES_DATE(TABLEQ.FN_USERID,\n" +
"                                                                                                 '" + structureDate+ "'),\n" +
"                                                          0)),\n" +
"                                               0)) * 100\n" +
"                                     FROM DUAL),\n" +
"                                   0) PORCENT,\n" +
"                               TABLEQ.FN_LEVEL,\n" +
"                               TABLEQ.FN_MANAGER,\n" +
"                               SE.FN_ID_EMPLOYEE_CLIMA,\n" +
"                               NVL(DECODE(S.FN_ID_SURVEY_TYPE,\n" +
"                                          1,\n" +
"                                          SE.FN_PLAN,\n" +
"                                          (SELECT COUNT(1)\n" +
"                                             FROM PUL_EMPLOYEE_CLIMA\n" +
"                                            WHERE FN_MANAGER =  " + usuario +")),\n" +
"                                   0) FN_PLAN,\n" +
"                               TABLEQ.FN_USERID\n" +
"                          FROM PUL_SURVEY_EMPLOYEE SE,\n" +
"                               PUL_SURVEY S,\n" +
"                               (SELECT FN_ID_EMPLOYEE_CLIMA,\n" +
"                                       FN_LEVEL,\n" +
"                                       FC_TITLE,\n" +
"                                       FC_DIVISION,\n" +
"                                       FN_USERID,\n" +
"                                       FC_LAST_NAME,\n" +
"                                       FN_MANAGER\n" +
"                                  FROM (SELECT *\n" +
"                                          FROM PORTALUNICO.PUL_EMPLOYEE_CLIMA PE\n" +
"                                         WHERE PE.FN_LEVEL =\n" +
"                                               (SELECT FN_LEVEL + 1\n" +
"                                                  FROM PUL_EMPLOYEE_CLIMA\n" +
"                                                 WHERE FN_USERID =  " + usuario +"\n" +
"                                                   AND TO_CHAR(FD_STRUCTURE_DATE,\n" +
"                                                               'MM/YYYY') =\n" +
"                                                       '" + structureDate+ "')\n" +
"                                           AND TO_CHAR(FD_STRUCTURE_DATE,\n" +
"                                                       'MM/YYYY') =\n" +
"                                               '" + structureDate+ "')\n" +
"                                    WHERE FN_MANAGER = "+ usuario+") TABLEQ\n" +
"                         WHERE SE.FN_ID_EMPLOYEE_CLIMA =\n" +
"                               TABLEQ.FN_ID_EMPLOYEE_CLIMA\n" +
"                           AND SE.FN_ID_SURVEY = S.FN_ID_SURVEY\n" +
"                           AND TO_CHAR(S.FD_SURVEY_DATE, 'MM/YYYY') =\n" +
"                               '" + structureDate+ "')\n" +
"                 GROUP BY FN_ID_EMPLOYEE_CLIMA\n" +
"                HAVING COUNT(1) > 1)) TABLAGENERAL\n" +
" WHERE TABLAGENERAL.ALI > 0";
        Query oQuery = JPA.em().createNativeQuery(statement);

        return new PagingResult(getCountN(statement, ""), oQuery.getResultList());
    }

    public static PagingResult getNivelUno(String structureDate, String order, int start, int end) {
        String statement
                = " SELECT (SELECT FC_TITLE  FROM PUL_EMPLOYEE_CLIMA PEM WHERE TO_CHAR (fd_structure_DATE, 'MM/YYYY') = '"+structureDate+"' AND PEM.FN_USERID= TABLEQ.FN_MANAGER) TITLE_MANAGER,TABLEQ.FC_TITLE,S.FC_NAME,"
                + "        nvl(FN_COUNT_DEPENDENCIES_DATE(TABLEQ.FN_USERID,'"+structureDate+"') , 0) ALI,"
                + "        nvl(FN_COUNT_ASWERED_DATE(TABLEQ.FN_USERID,'"+structureDate+"'), 0) ANSWERED,"
                + "        nvl(DECODE(FN_COUNT_DEPENDENCIES_DATE(TABLEQ.FN_USERID,'"+structureDate+"') ,0,0,"
                + "        (FN_COUNT_ASWERED_DATE(TABLEQ.FN_USERID,'"+structureDate+"') / FN_COUNT_DEPENDENCIES_DATE(TABLEQ.FN_USERID,'"+structureDate+"') )*100), 0) PORCENT,"
                + "        TABLEQ.FN_LEVEL, TABLEQ.FN_MANAGER,"
                + "        SE.FN_ID_EMPLOYEE_CLIMA,"
                + "        nvl(SE.FN_PLAN, 0) FN_PLAN,TABLEQ.FN_USERID "
                + " FROM  PUL_SURVEY_EMPLOYEE SE, PUL_SURVEY S,"
                + "      (SELECT FN_ID_EMPLOYEE_CLIMA,FN_LEVEL,FC_TITLE, FN_USERID, FC_LAST_NAME, FN_MANAGER,"
                + "       CONNECT_BY_ROOT FN_USERID AS ROOT_ID  "
                + "       FROM (SELECT * FROM portalunico.PUL_EMPLOYEE_CLIMA  WHERE TO_CHAR (fd_structure_date, 'MM/YYYY') = '"+structureDate+"') " 
                + "       CONNECT BY PRIOR FN_USERID = FN_MANAGER"
                + "       START WITH FN_MANAGER = 50092262) TABLEQ"
                + " WHERE SE.FN_ID_EMPLOYEE_CLIMA = TABLEQ.FN_ID_EMPLOYEE_CLIMA"
                + " AND   SE.FN_ID_SURVEY = S.FN_ID_SURVEY"
                + " AND TABLEQ.FN_LEVEL = (SELECT FN_LEVEL + 1 "
                + "                        FROM   PUL_EMPLOYEE_CLIMA "
                + "                        WHERE  FN_USERID=50092262 AND TO_CHAR (fd_structure_DATE, 'MM/YYYY') = '"+structureDate+"')"
                + " AND  to_char(S.FD_SURVEY_DATE,'MM/YYYY') ='" + structureDate + "'";

        Query oQuery = JPA.em().createNativeQuery(statement);

        return new PagingResult(getCountRows("PUL_EMPLOYEE_CLIMA", "FN_LEVEL=1"), oQuery.getResultList());
    }

    public static PagingResult getZonaUno(String survey, String conditions, String order, int start, int end) {
        String statement = "select ec.FC_AREA AreaId,'Zona '||ec.FC_AREA Area, \n"
                + "       sum(se.FN_PLAN) totalPlanes,\n"
                + "       sum(se.FN_ANSWERED) Contestados,\n"
                + "       DECODE(sum(se.FN_PLAN),0,0,DECODE(sum(se.FN_PLAN),0,0,round((sum(se.FN_ANSWERED)/sum(se.FN_PLAN))*100,4))) Porcentaje\n"
                + " FROM PUL_EMPLOYEE_CLIMA ec, PUL_SURVEY_EMPLOYEE se\n"
                + " WHERE ec.FN_ID_EMPLOYEE_CLIMA = se.FN_ID_EMPLOYEE_CLIMA\n"
                + " AND   se.FN_ID_SURVEY =" + survey + " \n"
                + " GROUP BY ec.FC_AREA\n"
                + " ORDER BY 1";

        Query oQuery = JPA.em().createNativeQuery(statement);

        return new PagingResult(getCountN(statement, ""), oQuery.getResultList());
    }

    public static PagingResult getNivelNZona(String survey, String area, String conditions, String order, int start, int end) {
        String statement = "select ec.FC_DIVISION, \n"
                + "       sum(FN_PLAN) totalPlanes,\n"
                + "       sum(FN_ANSWERED) Contestados,\n"
                + "        DECODE(sum(se.FN_PLAN),0,0,round((sum(se.FN_ANSWERED)/sum(se.FN_PLAN))*100,4)) Porcentaje\n"
                + " FROM PUL_EMPLOYEE_CLIMA ec, PUL_SURVEY_EMPLOYEE se\n"
                + " WHERE ec.FN_ID_EMPLOYEE_CLIMA = se.FN_ID_EMPLOYEE_CLIMA\n"
                + " AND   ec.FC_AREA='" + area + "'\n"
                + " AND   se.FN_ID_SURVEY= " + survey + "\n"
                + " GROUP BY  FC_DIVISION";

        Query oQuery = JPA.em().createNativeQuery(statement);

        return new PagingResult(getCountN(statement, ""), oQuery.getResultList());
    }

    public static PagingResult getNivelZonaDiv(String survey, String area, String conditions, String order, int start, int end) {
        String statement = "select ec.FC_DIVISION, \n"
                + "       sum(FN_PLAN) totalPlanes,\n"
                + "       sum(FN_ANSWERED) Contestados,\n"
                + "       DECODE(sum(FN_PLAN),0,0,round((sum(FN_ANSWERED) / sum(FN_PLAN))*100,4)) Porcentaje\n"
                + " FROM PUL_EMPLOYEE_CLIMA ec, PUL_SURVEY_EMPLOYEE se\n"
                + " WHERE ec.FN_ID_EMPLOYEE_CLIMA = se.FN_ID_EMPLOYEE_CLIMA\n"
                + " AND   ec.FC_AREA='" + area + "'\n"
                + " AND   se.FN_ID_SURVEY= " + survey + "\n"
                + " GROUP BY  FC_DIVISION";

        Query oQuery = JPA.em().createNativeQuery(statement);

        return new PagingResult(getCountN(statement, ""), oQuery.getResultList());
    }

    public static PagingResult getComprasNUno(String survey, String area, String conditions, String order, int start, int end) {
        String statement = "select ec.FC_DIVISION, \n"
                + "       sum(FN_PLAN) totalPlanes,\n"
                + "       sum(FN_ANSWERED) Contestados,\n"
                + "       DECODE(sum(FN_PLAN),0,0,round((sum(FN_ANSWERED) / sum(FN_PLAN))*100,4)) Porcentaje\n"
                + " FROM PUL_EMPLOYEE_CLIMA ec, PUL_SURVEY_EMPLOYEE se\n"
                + "WHERE ec.FN_ID_EMPLOYEE_CLIMA = se.FN_ID_EMPLOYEE_CLIMA\n"
                + "AND   ec.FC_AREA='" + area + "'\n"
                + "AND   se.FN_ID_SURVEY= " + survey + "\n"
                + "GROUP BY  FC_DIVISION";

        Query oQuery = JPA.em().createNativeQuery(statement);

        return new PagingResult(getCountN(statement, ""), oQuery.getResultList());
    }

    public static PagingResult getComprasCorp(String survey, String level, String conditions, String order, int start, int end) {
        String statement = "SELECT TABLEQ.FN_LEVEL||TABLEQ.FN_USERID,TABLEQ.FC_DEPARTMENT,"
                + "       FN_COUNT_DEPENDENCIES(TABLEQ.FN_USERID) Encuestas,\n"
                + "       SE.FN_ANSWERED Contestadas, \n"
                + "       DECODE(FN_COUNT_DEPENDENCIES(TABLEQ.FN_USERID),0,0,round((SE.FN_ANSWERED / FN_COUNT_DEPENDENCIES(TABLEQ.FN_USERID))*100,4)) Porcentajes,\n"
                + " TABLEQ.FN_LEVEL , TABLEQ.FN_MANAGER, \n"
                + "       SE.FN_ID_EMPLOYEE_CLIMA, \n"
                + "       SE.FN_PLAN\n"
                + "FROM   PUL_SURVEY_EMPLOYEE SE, \n"
                + "       PUL_SURVEY S,\n"
                + "       (SELECT FN_ID_EMPLOYEE_CLIMA,\n"
                + "               FN_LEVEL, \n"
                + "               FN_USERID, \n"
                + "               FC_DEPARTMENT,\n"
                + "               FC_LAST_NAME, \n"
                + "               FN_MANAGER,\n"
                + "               CONNECT_BY_ROOT FN_USERID AS ROOT_ID\n"
                + "        FROM PUL_EMPLOYEE_CLIMA\n"
                + "        CONNECT BY PRIOR FN_USERID = FN_MANAGER \n"
                + "        START WITH FN_MANAGER in (50074387, 50969866)) TABLEQ\n"
                + "WHERE SE.FN_ID_EMPLOYEE_CLIMA = TABLEQ.FN_ID_EMPLOYEE_CLIMA\n"
                + "AND S.FN_ID_SURVEY = SE.FN_ID_SURVEY\n"
                + "AND S.FN_ID_SURVEY = " + survey + "\n"
                + "AND TABLEQ.FN_LEVEL =" + level;

        Query oQuery = JPA.em().createNativeQuery(statement);

        return new PagingResult(getCountN(statement, ""), oQuery.getResultList());
    }

    public static PagingResult getNivelDiv(String survey, String div, String conditions, String order, int start, int end) {
        String statement = "select tableq.root_id,\n"
                + "       tableq.FC_DIVISION,\n"
                + "       tableq.FC_DEPARTMENT,\n"
                + "       tableq.FN_LEVEL, \n"
                + "       tableq.FN_MANAGER,\n"
                + "       SE.FN_PLAN Planes,\n"
                + "       SE.FN_ANSWERED Contestados,\n"
                + "       DECODE(se.FN_PLAN,0,0, round((se.FN_ANSWERED / se.FN_PLAN)*100,4)) Porcentaje\n"
                + " from PUL_SURVEY_EMPLOYEE SE,\n"
                + "     (select FN_ID_EMPLOYEE_CLIMA,\n"
                + "             FN_LEVEL, \n"
                + "             FN_USERID,\n"
                + "             FC_DIVISION,\n"
                + "             FC_DEPARTMENT, \n"
                + "             FC_LAST_NAME, \n"
                + "             FN_MANAGER ,\n"
                + "             connect_by_root FN_USERID as root_id\n"
                + "      from PUL_EMPLOYEE_CLIMA\n"
                + "      connect by prior FN_USERID = FN_MANAGER -- down the tree\n"
                + "      start with FN_MANAGER in (50074387) ) tableq \n"
                + "WHERE SE.FN_ID_EMPLOYEE_CLIMA=tableq.FN_ID_EMPLOYEE_CLIMA\n"
                + "AND    tableq.FC_DIVISION = '" + div + "'\n"
                + "AND    SE.FN_ID_SURVEY=" + survey;

        Query oQuery = JPA.em().createNativeQuery(statement);

        return new PagingResult(getCountN(statement, ""), oQuery.getResultList());
    }

    public static List getValidSurveys() {
        String statement = "SELECT * FROM PUL_SURVEY WHERE FD_DATE_END > ADD_MONTHS(SYSDATE, -12)";
        Query oQuery = JPA.em().createNativeQuery(statement);
        return oQuery.getResultList();
    }

    public static int getCountRows(String table, String conditions) {
        String statement = "SELECT"
                + " COUNT(*) "
                + " FROM " + table;
        if (conditions != "") {
            statement += " WHERE " + conditions;

        }

        Query oQuery = JPA.em().createNativeQuery(statement);
        List result = oQuery.getResultList();
        return Integer.parseInt(result.get(0).toString());
    }

    public static int getCountN(String table, String conditions) {
        String statement = "SELECT"
                + " COUNT(*) "
                + " FROM (" + table + ")";
        if (conditions != "") {
            statement += " WHERE " + conditions;

        }

        Query oQuery = JPA.em().createNativeQuery(statement);
        List result = oQuery.getResultList();
        return Integer.parseInt(result.get(0).toString());
    }

    public static List<PermisosEspeciales> getPermisosEspeciales (int userId){
        List<PermisosEspeciales> permisosEspeciales = new ArrayList();
        
        List permisos = JPA.em().createNativeQuery(
                        "SELECT DISTINCT sp.FC_TYPE_PERMIT, sp.FC_VALUE " +
                        " FROM PUL_SPECIAL_PERMISSIONS sp " +
                        " INNER JOIN PUL_ROLE r ON r.FN_ID_ROLE = sp.FN_ID_ROLE " +
                        " INNER JOIN PUL_USER_ROLE ur ON ur.FN_ID_ROLE = r.FN_ID_ROLE " +
                        " WHERE ur.FN_ID_USER = ?").setParameter(1, userId).getResultList();
        
        for (Object permiso : permisos) {
            Object[] permission = (Object[]) permiso;
            PermisosEspeciales special = new PermisosEspeciales();
            special.permitValue = permission[1].toString();
            special.typePermit  = permission[0].toString();
            permisosEspeciales.add(special);            
        }
        
        return permisosEspeciales;
    }

    public static List getParesPermisosEspeciales (int userId, String date) {
        List pares = JPA.em().createNativeQuery(
                        "SELECT decode(fc_division,'CORP', FC_TITLE, FC_TITLE||' - '||FC_DIVISION) FC_TITLE, FN_USERID, FC_DEPARTMENT, " +
                        " COALESCE(SUBSTR(FC_EMAIL , 0, INSTR(FC_EMAIL, '@') - 1), FC_EMAIL) FC_USERNAME " +
                        " FROM PUL_EMPLOYEE_CLIMA ec " +
                        " WHERE ec.FN_MANAGER = (SELECT FN_MANAGER " +
                        "                       FROM PUL_EMPLOYEE_CLIMA " +
                        "                       WHERE FN_USERID = ? and TO_CHAR (ec.FD_STRUCTURE_DATE, 'MM/YYYY') = ? )")
                                                .setParameter(1, userId)
                                                .setParameter(2, date)
                                                .getResultList();
        return pares;
    }

    public static List getLevelPermisosEspeciales (int level, String date) {
        List levels = JPA.em().createNativeQuery(
                        "SELECT decode(fc_desc_location,'Servicios Liverpool'', FC_TITLE, FC_TITLE||' - '||fc_desc_location) FC_TITLE, FN_USERID, FC_DEPARTMENT,\n" +
                        " COALESCE(SUBSTR(FC_EMAIL , 0, INSTR(FC_EMAIL, '@') - 1), FC_EMAIL) FC_USERNAME " +
                        " FROM PUL_EMPLOYEE_CLIMA ec " +
                        " WHERE ec.FN_LEVEL = ? and TO_CHAR (ec.FD_STRUCTURE_DATE, 'MM/YYYY') = ?" +
                        " ORDER BY FC_TITLE ")
                        .setParameter(1, level)
                        .setParameter(2, date)
                        .getResultList();
        return levels;
    }
    
    public static List getEstructuraPermisosEspeciales (Long employeeNumber, String date) {
        List structure = JPA.em().createNativeQuery(
                "SELECT decode(fc_division,'CORP', FC_TITLE, FC_TITLE||' - '||FC_DIVISION) FC_TITLE, FN_USERID, FC_DEPARTMENT, " +
                " COALESCE(SUBSTR(FC_EMAIL , 0, INSTR(FC_EMAIL, '@') - 1), FC_EMAIL) FC_USERNAME " +
                " FROM PUL_EMPLOYEE_CLIMA ec " +
                " WHERE ec.FN_EMPLOYEE_NUMBER = ? and TO_CHAR (ec.FD_STRUCTURE_DATE, 'MM/YYYY') = ? " +
                " ORDER BY FC_TITLE")
                .setParameter(1, employeeNumber)
                .setParameter(2, date).
                getResultList();
        
        return structure;
    }


    //With date:
    public static List getParesPermisosEspecialesAux (int userId, String structureDate) {
        List pares = JPA.em().createNativeQuery(
                        "SELECT (decode(fc_division,'CORP', FC_TITLE, FC_TITLE||' - '||FC_DIVISION))||' - '||FN_EMPLOYEE_NUMBER FC_TITLE, FN_USERID " +
                        " FROM PUL_EMPLOYEE_CLIMA ec " +
                        " WHERE TO_CHAR (FD_STRUCTURE_DATE, 'MM/YYYY') = ?" +
                        " AND ec.FN_MANAGER = (SELECT FN_MANAGER " +
                        "                       FROM PUL_EMPLOYEE_CLIMA " +
                        "                       WHERE FN_USERID = ? )")
                        .setParameter(1, structureDate)
                        .setParameter(2, userId)
                        .getResultList();
        return pares;
    }

    public static List getLevelPermisosEspecialesAux (int level, String structureDate) {
        List levels = JPA.em().createNativeQuery(
                        "SELECT (decode(fc_division,'CORP', FC_TITLE, FC_TITLE||' - '||FC_DIVISION))||' - '||FN_EMPLOYEE_NUMBER FC_TITLE, FN_USERID\n" +
                        " FROM PUL_EMPLOYEE_CLIMA ec " +
                        " WHERE TO_CHAR (FD_STRUCTURE_DATE, 'MM/YYYY') = ?" +
                        " AND ec.FN_LEVEL = ? " +
                        " ORDER BY FC_TITLE ")
                        .setParameter(1, structureDate)
                        .setParameter(2, level)
                        .getResultList();
        return levels;
    }

    public static List getEstructuraPermisosEspecialesAux (Long employeeNumber, String structureDate) {
        List structure = JPA.em().createNativeQuery(
            "SELECT (decode(fc_division,'CORP', FC_TITLE, FC_TITLE||' - '||FC_DIVISION))||' - '||FN_EMPLOYEE_NUMBER FC_TITLE, FN_USERID " +
            " FROM PUL_EMPLOYEE_CLIMA ec " +
            " WHERE TO_CHAR (FD_STRUCTURE_DATE, 'MM/YYYY') = ?" +
            " AND ec.FN_EMPLOYEE_NUMBER = ? " +
            " ORDER BY FC_TITLE")
            .setParameter(1, structureDate)
            .setParameter(2, employeeNumber)
            .getResultList();
        return structure;
    }
}
