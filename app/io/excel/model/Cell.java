package io.excel.model;

public class Cell {
	
	private Integer index;
	private Object value;
	
	public Cell() {}
	
	public Cell(Object value, int index) {
		this.value = value;
		this.index = index;
	}

	/**
	 * @return the value
	 */
	public Object getValue() {
		return value;
	}

	/**
	 * @param value the value to set
	 */
	public void setValue(Object value) {
		this.value = value;
	}

	/**
	 * @return the index
	 */
	public Integer getIndex() {
		return index;
	}

	/**
	 * @param index the index to set
	 */
	public void setIndex(Integer index) {
		this.index = index;
	}
	
	

}
