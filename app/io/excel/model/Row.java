package io.excel.model;

import java.util.List;

public class Row {
   
	private List<Cell> cells;
	private int index;

	public Row() {}
	
	public Row(List<Cell> cells, int index) {
		this.cells = cells;
		this.index = index;
	}
	
	
	/**
	 * @return the cells
	 */
	public List<Cell> getCells() {
		return cells;
	}

	/**
	 * @param cells the cells to set
	 */
	public void setCells(List<Cell> cells) {
		this.cells = cells;
	}

	/**
	 * @return the index
	 */
	public int getIndex() {
		return index;
	}

	/**
	 * @param index the index to set
	 */
	public void setIndex(int index) {
		this.index = index;
	}
  
}
