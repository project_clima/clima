package io.excel.commons;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.DataFormatter;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

import io.excel.commons.exceptions.NotSupportedFileException;
import org.apache.poi.hssf.usermodel.HSSFSheet;

public class ExcelReader {

    /**
     * Reads a excel file with both 2004 & 2007 format
     * 
     * @param file
     * @return List of rows
     * @throws FileNotFoundException,
     *             if the file is not a .xls or xlsx extension
     * @throws IOException
     * @throws NotSupportedFileException
     */
    public List<io.excel.model.Row> readExcelFile(File file)
            throws FileNotFoundException, IOException, NotSupportedFileException {
        String extension = "";
        List<io.excel.model.Row> rows = null;

        if (file != null) {
            String name = file.getName();
            extension = name.substring(name.indexOf("."));

            FileInputStream inputStream = new FileInputStream(file);
            if (extension.equalsIgnoreCase(".xls")) {

                rows = readXlsFile(inputStream); // reads excel file with 2004
                                                 // format
                inputStream.close();

            } else if (extension.equalsIgnoreCase(".xlsx")) {
                rows = readXlsxFile(inputStream); // reads excel file with 2007
                                                  // format
                inputStream.close();
            } else {
                inputStream.close();
                throw new NotSupportedFileException();
            }

        }

        return rows;
    }

    /**
     * Reads a excel file with 2004 format
     * 
     * @param file
     * @return
     */
    public List<io.excel.model.Row> readXlsFile(FileInputStream inputStream) throws FileNotFoundException, IOException {

        HSSFWorkbook  workbook = new HSSFWorkbook (inputStream);
        Sheet firstSheet = workbook.getSheetAt(0);
        Iterator<Row> iterator = firstSheet.iterator();

        List<io.excel.model.Row> rows = new ArrayList<io.excel.model.Row>();
        int index = 0;
        while (iterator.hasNext()) {
            Row nextRow = iterator.next();
            Iterator<Cell> cellIterator = nextRow.cellIterator();
            List<io.excel.model.Cell> cells = new ArrayList<io.excel.model.Cell>();

            while (cellIterator.hasNext()) {
                Cell nextCell = cellIterator.next();
                int columnIndex = nextCell.getColumnIndex();
                cells.add(new io.excel.model.Cell(getCellValue(nextCell), columnIndex));
            }

            rows.add(new io.excel.model.Row(cells, ++index));
        }
        return rows;
    }

    /**
     * Read a excel file with 2007 format
     * 
     * @param file
     *            - excel file
     * @return list of rows
     */
    public List<io.excel.model.Row> readXlsxFile(FileInputStream inputStream)
            throws FileNotFoundException, IOException {

        XSSFWorkbook workbook = new XSSFWorkbook(inputStream);
        XSSFSheet firstSheet = workbook.getSheetAt(0);
        Iterator<Row> iterator = firstSheet.iterator();

        List<io.excel.model.Row> rows = new ArrayList<io.excel.model.Row>();

        int index = 0;
        while (iterator.hasNext()) {
            Row nextRow = iterator.next();
            Iterator<Cell> cellIterator = nextRow.cellIterator();
            List<io.excel.model.Cell> cells = new ArrayList<io.excel.model.Cell>();

            while (cellIterator.hasNext()) {
                Cell nextCell = cellIterator.next();

                int columnIndex = nextCell.getColumnIndex();
                cells.add(new io.excel.model.Cell(getCellValue(nextCell), columnIndex));

            }

            rows.add(new io.excel.model.Row(cells, ++index));

        }
        return rows;
    }

    /**
     * Obtains the cell's value
     * 
     * @param cell
     * @return
     */
    private Object getCellValue(Cell cell) {
        DataFormatter formatter = new DataFormatter();

        return formatter.formatCellValue(cell);

    }
}
