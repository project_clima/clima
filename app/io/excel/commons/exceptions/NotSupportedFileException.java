package io.excel.commons.exceptions;

/**
 * Exception for files not supported
 * @author edge
 *
 */
public class NotSupportedFileException extends Exception {

}
