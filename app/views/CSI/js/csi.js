
//funcion para cuando se actauliza el filtro de la fecha 
function updateInformationWithDateFilters(){
	// SE actualizan los graficos
	graphApi.invokeGraphResults({});
	
	// se reinician las tablas
	var tempCsiType = csiApi.currectLevel0FilterCsiType;
	if (tempCsiType != 0){
		csiApi.currectLevel0FilterCsiType = 100;
		csiApi.setLevel0FilterCsiType(tempCsiType);
	}
	
}

// funcion para cuando se actauliza el filtro de la encusta
function actualizarFiltersCSI(){
	updateInformationWithDateFilters();
		
}
$(document).ready(function() {
	
    $("#corp-areas").css('display', 'none');
    $("#principal-table").css('display', 'none');
    $("#surveyType").change(function(){
    	var csiType = $(this).val();
    	csiApi.setLevel0FilterCsiType(csiType);
    	csiApi.createSurveyList(csiType);
    	filtersAPI.cleanySurveyFilter();
    });
    Table.applyFontColor("#table-results-detail");
	Table.applyFontColor("#corp-areas-table");
	
	$("#table-results").DataTable({
		"oLanguage": {
			"bSort" : false,
			"sZeroRecords": "No hay registros",
			"sInfo": "",
			"sInfoEmpty": "",
			"sPaginatePrevious": "",
			"sProcessing": "Cargando...",
			"sLengthMenu": "",
			"sSearch": "Buscar"
		},
		"bPaginate": false,
		"bLengthChange": false,
		"bFilter" : false
	});
	
	csiApi.inicializarTablas("table-results-detail", {});
    csiApi.inicializarTablas("corp-areas-table", {});	
});

function CsiAPI(){
	this.currectFiltersLevel = 0;	
	this.currectLevel0FilterCsiType = 0;
	this.currectLevel1FilterCorporation = 0;	
	this.currectLevel2FilterManagement = 0;	
	this.currectLevel3FilterWorld = 0;
	this.currectLevel4FilterSection = 0;
	this.currectLevel5FilterEmployee = 0;
	this.currectLevel6FilterRanking = 0;
		
	this.catalogDict = {}; // create an empty array
	this.childrenDict = {};
	
	this.createSurveyList = function(csiType){
		$("#menu-filter-survey-name-lst").empty();
		if(csiType>0 && csiType != "Seleccionar tipo"){
			$.ajax({
		         url: "@{csi.SurveysCSI.surveyListJSON()}",
		         type:'get',
		         async: true,
		         data: {
		             type: csiType
		         },
		         beforeSend: function (){
		         },
		         complete: function (){
		         },
		         success: function (data) {
		             $("#menu-filter-survey-name-lst").append("<option value=''>Selecciona una Opción </option>");
		             for (var i = 0; i < data.length; i++) {
		                 $("#menu-filter-survey-name-lst").append("<option value='" + data[i][0]+"'>"+data[i][1] +"</option>" )
		             };
		         }
		     });
		}	
	}

	this.inicializarTablas = function(idTable,destroy){		
		tableResults = $('#' + idTable).DataTable({
			"bSort" : false,
			"bAutoWidth" : false,
			"oLanguage": {
				"sZeroRecords": "Ningún Registro",
				"sInfo": "Mostrando registros de _START_ al _END_ de _TOTAL_ totales",
				"sInfoEmpty": "0 Eventos",
				"sPaginatePrevious": "Previous page",
				"sProcessing": "Cargando...",
				"sLengthMenu": "Mostrar _MENU_ registros",
				"sSearch": "Buscar",
				"oPaginate": {
					"sFirst":    "<<",
					"sLast":     ">>",
					"sNext":     ">",
					"sPrevious": "<"
				},
				
			},
			destroy
		});
		
		$("#"+ idTable +"_paginate").css("padding", 0);
		$("#"+ idTable +"_info").css("font-size", 12);
		$("#"+ idTable +"_length").css("font-size", 12);
		$("#"+ idTable +"_length select").css({
			paddingTop: "0",
			paddingBottom: "0",
			height: "32px",
			fontSize: "12px"
		});
		
		$("#"+ idTable +"_filter").hide();
	}

	this.limpiarTabla= function (tablaId){
		var leng = $("#"+tablaId+" tbody >tr >td").length;
		for(var j=0; j<leng;j++){
			$("#"+tablaId+" tbody >tr >td").remove();
		}
		leng = $("#"+tablaId+" tbody >tr").length;
		for(var i=0 ; i<leng ; i++){
			$("#"+tablaId+" tbody >tr").remove();
		}

		$("#"+tablaId+"").DataTable().clear();
		$("#"+tablaId+"").DataTable().destroy();
		
		if (tablaId === "corp-areas-table"){
			$("#"+tablaId+" thead >tr").html("<th>Áreas</th>");	
		}
		
		if(tablaId === "table-results-detail"){
			var thead = $("#" + tablaId + " thead >tr >th").length;
			for(var i=5;i<thead;i++){
				$("#" + tablaId + " thead >tr >th:last").remove();
			}
		}
		
		csiApi.inicializarTablas(tablaId,{'bDestroy':true})
	};
	
	this.verComentarios = function(idComentario){
		verComentarios(idComentario);
		return true;
	};
	
	this.agregarEncabezados = function(type, encabezado, final){
		if(type==3){
			if(!$("#table-results-zonab").length) {
				$("#table-results tr:first").append("<th> " + encabezado + " </th>");
				
				if(final) {
					$("#table-results tr:first").append(
							"<th id='table-results-zonab'> Zona Bouetique </th>");
					 $("#table-results").DataTable({
						 "bSort" : false,	
						 "oLanguage": {
								"sZeroRecords": "No hay registros",
								"sInfo": "",
								"sInfoEmpty": "",
								"sPaginatePrevious": "",
								"sProcessing": "Cargando...",
								"sLengthMenu": "",
								"sSearch": "Buscar"
							},
							"bPaginate": false,
							"bLengthChange": false,
							"bFilter" : false,
							"bDestroy" : true,
							"bAutoWidth" : false,
							"columns" : [
							             {"width" : "300%"},
							             {"width" : "80%"},
							             {"width" : "80%"},
							             {"width" : "80%"},
							             {"width" : "80%"},
							             {"width" : "80%"},
							             {"width" : "80%"},
							             {"width" : "80%"},
							             {"width" : "80%"},
							             {"width" : "80%"},
							             {"width" : "80%"},
							             {"width" : "80%"}
							             ]
						});
				}
			}
		}
		if(type==1){
			
			if(!$("#table-results-ver").length) {
				$("#table-results-detail tr:first").append("<th style='width:90%;'> " + encabezado + " </th>");
				
				if(final) {
					$("#table-results-detail tr:first").append(
							"<th tyle='width:90%;' id='table-results-ver'> Comentarios </th>");
					csiApi.inicializarTablas("table-results-detail",{"bDestroy" : true});
				}
			}
			
		} else {
			if(!$("#corp-areas-detalle").length){
				$("#corp-areas-table tr:first").append(
						"<th>" + encabezado + "</th>");
				if(final){
					$("#corp-areas-table tr:first").append(
					"<th id='corp-areas-detalle'> Detalle </th>");
					
					csiApi.inicializarTablas("corp-areas-table",{"bDestroy" : true});
				}
			}
		}
	};
	
	this.agregarCsiHeader = function(idHeader, header, level){
		var signo = "";
	if(level > 1) signo = "&nbsp;&nbsp; > &nbsp;&nbsp;";
		else	$("#csi-header").empty();
		if(!$("#" + idHeader).length){
			$("#csi-header").append(
				"<label id='" + idHeader + "' class='normal-lbl'>" +
					signo + " "+ header + " "  +
				"</label>");
		}
	};
	
	this.borrarCsiHeader = function(level){
		for(var i=level;i<$("#csi-header >label").length;i++){
			$("#csi-header >label:last").remove();
		}
	}

	this.updateResultDetailTable = function(resultDetailDtoList, orders,data, idAgrupador){	
		
		var levelFilter = data['levelFilter'];
		if($("#table-results-detail >tbody >tr").length <= 1){
			// Cuando esta vacia la tabla
			var contenido = [];
			for(var i = resultDetailDtoList.length-1; i >= 0 ; i--) {

				var e = resultDetailDtoList[i];
				var idFechita = e['idAgrupador']+"-"+levelFilter;
					contenido.push($("<tr id='tr-"+ idFechita  +"'>" +//
//							"<td>" +
//								(i + 1) +
//							"</td>" +
							csiApi.getRowResultDetail(e,orders,levelFilter)+
						"</tr>")[0]);
			}
			$("#table-results-detail").DataTable().rows.add(contenido);
			csiApi.inicializarTablas("table-results-detail", {"bDestroy" : true});
		}
		else if (levelFilter > 1){
			var idFechitaPadre = idAgrupador+"-"+(levelFilter-1);
			var children = [];
			for(var i = resultDetailDtoList.length-1; i >= 0 ; i--) {
				var e = resultDetailDtoList[i];

				var idFechita = e['idAgrupador']+"-"+levelFilter;
				
				$("#tr-"+idFechitaPadre).after($("<tr  id='tr-"+ idFechita +"'>" +//
//							"<td>" +
//								(i + 1) +
//							"</td>" +
							csiApi.getRowResultDetail(e,orders,levelFilter)+
						"</tr>")[0]);
				
				children.push(idFechita);
			}
			
			csiApi.childrenDict[idFechitaPadre] = children;
		}
		else {
			alert("No se tienen registros, y es un nivel mas alto" + levelFilter);
			
		}
		
		var tableResults = $('#table-results-detail').DataTable();
		
		Table.applyFontColor("#table-results");
		Table.applyFontColor("#table-results-detail");		
	};
	

	
	this.getRowResultDetail = function (resultDetailDto, orders,levelFilter){		
		var temp = "";
		var titulos = ["", "Apoyo", "Soluciones", "Necesidades", "Herramientas"];
		
		for (var i = 0 ; i < orders.length; i ++) {
			if(resultDetailDto['p' + orders[i]] == undefined || resultDetailDto['p' + orders[i]] == null){}
			else{
				temp = temp + "<td>" + resultDetailDto['p' + orders[i]] + "</td>"; 
				
				if(i == (orders.length-1)) {
					csiApi.agregarEncabezados(1,'P' + orders[i] + " " + (titulos[orders[i]]!=undefined?titulos[orders[i]]:""), true);
				}
				else {
					csiApi.agregarEncabezados(1,'P' + orders[i] + " " + (titulos[orders[i]]!=undefined?titulos[orders[i]]:""), false);
				}
			}
		}
		var espacios = "";
		for (var i = 0 ; i < levelFilter -1; i ++) {
			espacios = espacios + "&nbsp;&nbsp;";
		}
		//

		var flechita = "";
		var idFlechita = resultDetailDto['idAgrupador']+"-"+levelFilter;
		if (levelFilter < 5){
			flechita = "<span id ='span-"+ idFlechita+"' class='glyphicon glyphicon-triangle-right' style='font-size: 12px;' " +
			" 	 onclick='csiApi.zoomNextLevel(\""+idFlechita+"\","+ resultDetailDto['idAgrupador']+","+levelFilter +")'	" +
			"	></span>";
		}
		return "<td style='padding-left:"+ levelFilter*10 + "px;'><span style='font-size: 12px;'>"+espacios+"</span>"+
		
				flechita+	
				csiApi.catalogDict[resultDetailDto['idAgrupador']] +"("+idFlechita+")"+ "</td>" +
				"<td>" 
					+ resultDetailDto['result'].toFixed(2) + "</td>" +
				"<td>" 
					+ resultDetailDto['detractores'].toFixed(2) + "</td>" +
				"<td>" 
					+ resultDetailDto['neutros'].toFixed(2) + "</td>" +
				"<td>" 
					+ resultDetailDto['promotores'].toFixed(2) + "</td>" +
				temp +
				"<td class='view-btn'>" +
						"<span class='glyphicon glyphicon-search' onclick='javascript:csiApi.verComentarios(" + resultDetailDto['idAgrupador'] + ");'> </span> Ver "+
				"</td>" ;
		
	};
	
	this.zoomNextLevel = function(idFlechita, idAgrupador, currentLevelFilter){
		if($('#span-'+idFlechita).attr('class') === "glyphicon glyphicon-triangle-right") {
			// si ya se expandió ese nivel ya mejor los quita
			$('#span-'+idFlechita).removeClass('glyphicon-triangle-right').addClass('glyphicon-triangle-bottom');
			csiApi.extenderRama(idAgrupador,currentLevelFilter);
			csiApi.borrarCsiHeader(currentLevelFilter+1);
		}
		else if($('#span-'+idFlechita).attr('class') === "glyphicon glyphicon-triangle-bottom") {
			$('#span-'+idFlechita).removeClass('glyphicon-triangle-bottom').addClass('glyphicon-triangle-right');
			csiApi.borrarCascada(idFlechita);
			csiApi.borrarCsiHeader(currentLevelFilter);
			csiApi.borrarHeader(currentLevelFilter);
		}
	};
	
	this.borrarHeader = function(currentLevelFilter){
		if(csiApi.currectLevel0FilterCsiType == 1 || csiApi.currectLevel0FilterCsiType == 3) {
			// SE quitan los rows que no se
			
			var leng = $("#table-results tbody >tr").length;
			for(var j=leng-1; j>=0;j--){
				if( (j+1) > currentLevelFilter){
					$('#table-results').DataTable().row($("#table-results tr:last")).remove().draw(false);
				}
			}
		}
	};
	

	this.agregarEncabezadosAreas = function (){
		
		var meses = ['CSI','Enero' ,'Febrero' ,'Marzo' ,'Abril' ,'Mayo' ,'Junio' ,'Julio' ,'Agosto' ,'Septiembre' ,'Octubre' ,'Noviembre' ,'Diciembre'];
		for (var i = 0 ; i <  meses.length  ; i ++){
			var texto = meses[i].substring(0,3);//.toUpperCase();
			csiApi.agregarEncabezados(2,texto, i == meses.length - 1 );
		}

		
	};
	
	this.setLevel0FilterCsiType = function(csiType){
		
		if (csiApi.currectLevel0FilterCsiType != 0 && csiApi.currectLevel0FilterCsiType != csiType){
			csiApi.limpiarTabla("corp-areas-table");
			csiApi.limpiarTabla("table-results-detail");
			csiApi.limpiarTabla("table-results");
		}
		
		csiApi.currectLevel0FilterCsiType = csiType;
		csiApi.currentFiltersLevel = 1;
		
		if (csiType == 1 || csiType == 3) {	
			var header = "";
			if(csiType == 1)	header = "Tienda - Tienda";
			else	header = "Tienda - Corporativo";
			csiApi.agregarCsiHeader("compras-total-header", header, 1);
			
			$("#table-general").css('display', 'inline');
			$("#principal-table").css('display', 'inline');
			
			$("#corp-areas").css('display', 'none');
			
			var data = {'csiType':		csiType, 
				    'levelFilter': 	csiApi.currentFiltersLevel
					};
			csiApi.invokeResultsCSI(data,	// csitype
					'compras-total',            // idFilaHeader
					header,// textoFilaHeader
					1, 							// indexFilaEnHeader
					0 // no se agrupa porque es el primer nivel
			);
		
		}	
		else if (csiType == 2) {	 // Corporativo - Corporativo
			csiApi.agregarCsiHeader("compras-total-header", "Corporativo - Corporativo", 1);
			$("#table-general").css('display', 'none');
			$("#principal-table").css('display', 'none');
			
			$("#corp-areas").css('display', 'inline');

			
			var data = {'csiType':		csiType, 
						'levelFilter': 	csiApi.currentFiltersLevel
						};
			
			csiApi.invokeMatrixReportCSI(data, 	// csitype
					0
//					'csi-corp-address'				// idCatalogoSiguiente 
			);
			
			
		}
		else if (csiType == 4) {	 // Áreas evaluadas periódicamente
			csiApi.agregarCsiHeader("compras-total-header", "Áreas Evaluadas Periódicamente", 1);
			$("#table-general").css('display', 'none');
			$("#principal-table").css('display', 'none');
			
			$("#corp-areas").css('display', 'inline');
			
			

			var data = {
					'csiType': 		csiType,
					'levelFilter':	csiApi.currentFiltersLevel
				};
			
			csiApi.invokeAreaResultsCSI(data,	// Datos para Busqueda
					0
//						'area', 					// Id Fila Header
//						'csi-corp-address' 			// Id Catalog Siguiente
			);
			
			
		}
	};
	
	
	

	// Filtro Corporate Address
	this.setLevel1FilterCorporation = function(idCorporationFilter) {
		// Limpiar los siguientes filtros
		// en caso que el nivel de filtrado sea el anterior, en caso contrario hay que limpiar los otros filtros. 
		csiApi.currentFiltersLevel = 2;
		csiApi.currectLevel1FilterCorporation = idCorporationFilter;
		
		var data = {'csiType': 		csiApi.currectLevel0FilterCsiType ,
					'levelFilter': 	csiApi.currentFiltersLevel,
					'corporation': 	idCorporationFilter 
					
			      };
		
		csiApi.agregarCsiHeader("direccion-corp-header", csiApi.catalogDict[idCorporationFilter], 2);
		// Tienda-Tienda		
		if(csiApi.currectLevel0FilterCsiType == 1 || csiApi.currectLevel0FilterCsiType == 3) {
			csiApi.invokeResultsCSI(data, 						// datos para el
					'direccion-corp',            					// idFilaHeader
					csiApi.catalogDict[idCorporationFilter],		// textoFilaHeader
					2, 												// indexFilaEnHeader
					idCorporationFilter
			);
		}	// Corporativo-Corporativo
		else if (csiApi.currectLevel0FilterCsiType == 2) {		
			$("#table-general").css('display', 'none');
			$("#principal-table").css('display', 'none');
			$("#corp-areas").css('display', 'inline');
			
			$("#corp-areas-table").DataTable().clear();		
			csiApi.invokeMatrixReportCSI(data,
					idCorporationFilter
//					'csi-address'
					);							
		}
		
		else if (csiApi.currectLevel0FilterCsiType == 4) { 
			$("#table-general").css('display', 'none');
			$("#principal-table").css('display', 'none');
			$("#corp-areas").css('display', 'inline');
			
			$("#corp-areas-table").DataTable().clear();
			
			
			csiApi.invokeAreaResultsCSI(data,	// Datos para Busqueda
					idCorporationFilter			// Id Catalog Siguiente
			);
			
		}	
	};
	
	

	// Filtro Address
	this.setLevel2FilterManagement = function(idManagementFilter){		
		csiApi.currentFiltersLevel = 3;
		csiApi.currectLevel2FilterManagement = idManagementFilter;

		var data = { 'csiType': 		csiApi.currectLevel0FilterCsiType,
					 'levelFilter' : 	csiApi.currentFiltersLevel,
					 'corporation': 	csiApi.currectLevel1FilterCorporation,
					 'management': 		idManagementFilter
				   };
		csiApi.agregarCsiHeader("direccion-header", csiApi.catalogDict[idManagementFilter], 3);
		// Tienda-Tienda
		if(csiApi.currectLevel0FilterCsiType == 1 || csiApi.currectLevel0FilterCsiType == 3) {
			csiApi.invokeResultsCSI(data,					// datos para el 
					'direccion',            					// idFilaHeader
					csiApi.catalogDict[idManagementFilter],	// textoFilaHeader
					3, 											// indexFilaEnHeader
					idManagementFilter
			);
			
		} // Corporativo-Corporativo		
		else if (csiApi.currectLevel0FilterCsiType == 2) {	
			$("#table-general").css('display', 'none');
			$("#principal-table").css('display', 'none');
			$("#corp-areas").css('display', 'inline');
			
			$("#corp-areas-table").DataTable().clear();					
			csiApi.invokeMatrixReportCSI(data,
					idManagementFilter
//					'csi-world'
					);							
		} 
		// Areas Evaluadas Periodicamente
		else if (csiApi.currectLevel0FilterCsiType == 4) { 
			$('#table-results').hide();
			$("#table-general").css('display', 'none');
			$("#principal-table").css('display', 'none');
			$("#corp-areas").css('display', 'inline');
			
			$("#corp-areas-table").DataTable().clear();
			
			
			
			csiApi.invokeAreaResultsCSI(data,	// Datos para Busqueda
					idManagementFilter
//						'area', 					// Id Fila Header
//						'csi-world' 			// Id Catalog Siguiente
			);
			
		}
	};
	
	

	// Filtro World
	this.setLevel3FilterWorld = function(idWorldFilter){		
		csiApi.currentFiltersLevel = 4;
		csiApi.currectLevel3FilterWorld = idWorldFilter;

		var data = {'csiType': 		csiApi.currectLevel0FilterCsiType,
					'levelFilter':  csiApi.currentFiltersLevel,
					'corporation': 	csiApi.currectLevel1FilterCorporation,
					'management': 	csiApi.currectLevel2FilterManagement,
					'world': 		idWorldFilter};
		csiApi.agregarCsiHeader("world-header", csiApi.catalogDict[idWorldFilter], 4);
		// Tienda-Tienda
		if(csiApi.currectLevel0FilterCsiType == 1 || csiApi.currectLevel0FilterCsiType == 3) {
			csiApi.invokeResultsCSI(data,				// datos para el 
					'world',            					// idFilaHeader
					csiApi.catalogDict[idWorldFilter],	// textoFilaHeader
					4, 										// indexFilaEnHeader
					idWorldFilter
			);
		} // Corporativo-Corporativo
		else if (csiApi.currectLevel0FilterCsiType == 2) {
			$("#table-general").css('display', 'none');
			$("#principal-table").css('display', 'none');
			$("#corp-areas").css('display', 'inline');
			
			$("#corp-areas-table").DataTable().clear();						
			csiApi.invokeMatrixReportCSI(data, 
//					'csi-section'
					idWorldFilter
			);
		} 
		// Areas Evaluadas Periodicamente
		else if (csiApi.currectLevel0FilterCsiType == 4) {
			$("#table-general").css('display', 'none');
			$("#principal-table").css('display', 'none');
			$("#corp-areas").css('display', 'inline');
			
			$("#corp-areas-table").DataTable().clear();
			
			
			
			
			csiApi.invokeAreaResultsCSI(data,	// Datos para Busqueda
					idWorldFilter
//						'area', 					// Id Fila Header
//						'csi-section' 			// Id Catalog Siguiente
			);
		}
	};
	
	

	// Filtro Section
	this.setLevel4FilterSection = function(idSectionFilter){		
		csiApi.currentFiltersLevel = 5;
		csiApi.currectLevel4FilterSection = idSectionFilter;

		var data = {'csiType': 		csiApi.currectLevel0FilterCsiType,
					'levelFilter':	csiApi.currentFiltersLevel,
					'corporation':	csiApi.currectLevel1FilterCorporation,
					'management':	csiApi.currectLevel2FilterManagement,
					'world':		csiApi.currectLevel3FilterWorld,
					'section':		idSectionFilter};
		csiApi.agregarCsiHeader("section-header", csiApi.catalogDict[idSectionFilter], 5);
		// Tienda-Tienda
		if(csiApi.currectLevel0FilterCsiType == 1 || csiApi.currectLevel0FilterCsiType == 3) {
			csiApi.invokeResultsCSI(data,					// datos para el 
					'section',            						// idFilaHeader
					csiApi.catalogDict[idSectionFilter],	// textoFilaHeader
					5,											// indexFilaEnHeader
					idSectionFilter
			);
		} // Corporativo-Corporativo
		else if (csiApi.currectLevel0FilterCsiType == 2) {
			$("#table-general").css('display', 'none');
			$("#principal-table").css('display', 'none');
			$("#corp-areas").css('display', 'inline');
			
			$("#corp-areas-table").DataTable().clear();
			csiApi.invokeMatrixReportCSI(data, 
//					'csi-area'
					idSectionFilter
					);
		} 
		// Areas Evaluadas Periodicamente
		else if (csiApi.currectLevel0FilterCsiType == 4) {
			$("#table-general").css('display', 'none');
			$("#principal-table").css('display', 'none');
			$("#corp-areas").css('display', 'inline');
			
			var columnas = $("#corp-areas-table tr:first th").length;
			var filas = $("#corp-areas-table tr").length;
			
			$("#corp-areas-table").DataTable().clear();
			
			
			
			
			csiApi.invokeAreaResultsCSI(data,	// Datos para Busqueda
					idSectionFilter
//						'area', 					// Id Fila Header
//						'csi-area' 			// Id Catalog Siguiente
			);
		}
	};
	// Filtro Area
	this.setLevel5FilterEmployee = function(idEmployeeFilter) {		
		csiApi.currentFiltersLevel = 6;
		csiApi.currectLevel5FilterEmployee = idEmployeeFilter;
		csiApi.agregarCsiHeader("area-header", csiApi.catalogDict[idEmployeeFilter], 6);
		var data = {'csiType': 		csiApi.currectLevel0FilterCsiType,
					'levelFilter':	csiApi.currentFiltersLevel,
					'corporation':	csiApi.currectLevel1FilterCorporation,
					'management':	csiApi.currectLevel2FilterManagement,
					'world':		csiApi.currectLevel3FilterWorld,
					'section':		csiApi.currectLevel4FilterSection,
					'evaluated':	csiApi.currectLevel5FilterEmployee};
		
		if(csiApi.currectLevel0FilterCsiType == 1 || csiApi.currectLevel0FilterCsiType == 3) {
			csiApi.invokeResultsCSI(data,				// datos para el 
					'section',            					// idFilaHeader
					csiApi.catalogDict[idEmployeeFilter],	// textoFilaHeader
					6,										// indexFilaEnHeader
					idEmployeeFilter
			);
		}		
		else if (csiApi.currectLevel0FilterCsiType == 2) {
			$("#table-general").css('display', 'none');
			$("#principal-table").css('display', 'none');
			$("#corp-areas").css('display', 'inline');
			
			$("#corp-areas-table").DataTable().clear();
			csiApi.invokeMatrixReportCSI(data, 
//					null
					idEmployeeFilter
					);
		}
		 // Areas Evaluadas Periodicamente
		else if (csiApi.currectLevel0FilterCsiType == 4) {
			$("#table-general").css('display', 'none');
			$("#principal-table").css('display', 'none');
			$("#corp-areas").css('display', 'inline');
			
			$("#corp-areas-table").DataTable().clear();
			
			
			
			
			csiApi.invokeAreaResultsCSI(data,	// Datos para Busqueda
					idEmployeeFilter
//						'area', 					// Id Fila Header
//						null 			// Id Catalog Siguiente
			);
		}
	};
	
	this.setLevel6FilterRanking = function(idRankingFilter) {		
		csiApi.currentFiltersLevel = 7;
		csiApi.currectLevel6FilterRanking = idRankingFilter;

		var data = {'csiType': 		csiApi.currectLevel0FilterCsiType,
					'levelFilter':	csiApi.currentFiltersLevel,
					'corporation': 	csiApi.currectLevel1FilterCorporation,
					'management': 	csiApi.currectLevel2FilterManagement,
					'world': 		csiApi.currectLevel3FilterWorld,
					'section': 		csiApi.currectLevel4FilterSection,
					'evaluated': 	csiApi.currectLevel5FilterEmployee,
					'ranking':		idRankingFilter};
		csiApi.agregarCsiHeader("ranking-header", csiApi.catalogDict[idRankingFilter], 7);
		// Tienda - Tienda
		if(csiApi.currectLevel0FilterCsiType == 1 || csiApi.currectLevel0FilterCsiType == 3) {
			csiApi.invokeResultsCSI(data,					// datos para el 
					'ranking',            						// idFilaHeader
					csiApi.catalogDict[idRankingFilter],	// textoFilaHeader
					7, 											// indexFilaEnHeader
					idRankingFilter
			);
		} // Corporativo-Corporativo
		else if (csiApi.currectLevel0FilterCsiType == 2) {
			$("#table-general").css('display', 'none');
			$("#principal-table").css('display', 'none');
			$("#corp-areas").css('display', 'inline');
			
			$("#corp-areas-table").DataTable().clear();					
			csiApi.invokeMatrixReportCSI(data, 
					idRankingFilter		
//					'none'
					);
		} // Areas Evaluadas Periodicamente
		else if (csiApi.currectLevel0FilterCsiType == 4) {
			
		}
	};
	
	
	
	this.updateNextCatalog = function(nextCatalogValues){

		$.each(nextCatalogValues, function(i, cat) {
			csiApi.catalogDict[cat['id']] = cat['desc'];
		});         		
	};
	// ---------------------------------------------
	// ------ Funciones que Invocan Servicios ------
	
	
	// ---------------------------------------------
	// TODO implementar ls llamadas conservando los filtos y así 
	this.invokeResultsCSI = function (data,idFilaHeader,textoFilaHeader, indexFilaEnHeader, idAgrupador){//,idFilaHeader,textoFilaHeader, invokeResultsCSI, idNextCatalog){
		$.ajax({    	
	        url: '@{csi.ResultsCSI.getTableReportJSON}',
	        type: 'POST',//'POST',	        
	        cache: false,	  
	        data: data,
	        success: function (response) {	        		
	        	
	        	if(response['nextCatalog'].length > 0) {
	        		csiApi.updateNextCatalog(response['nextCatalog']);
	    		}
	        	csiApi.addRowHeader(idFilaHeader,textoFilaHeader, response['resultHeaderDtoList'], indexFilaEnHeader);
	        	csiApi.updateResultDetailTable(response['resultDetailDtoList'], response['orders'], data, idAgrupador);	        	
	        },
	        beforeSend: function () {
	        	Liverpool.loading("show");
	        },
	        complete: function () {
	        	Liverpool.loading("hide");
	        	//$('#menu-filter-date-lst').css('display', 'none');
	        },
	        error: function () {
	            Liverpool.setMessage($('.inner-message'), 'No se pudo aplicar el filtro. Por favor, intente nuevamente.', 'warning');
	        }
	    });
	};
	
	this.addRowHeader = function (idFilaHeader,textoFilaHeader, resultHeaderDtoList, indexFilaEnHeader) {
		// Verifica que exista la fila, si no existe la cream si existe la edita
		var resultHeaderDto = resultHeaderDtoList[0];
		var contenidoRow = "";
		
		var tableResults;
		if($("#" + idFilaHeader).length && (textoFilaHeader != "Tienda - Tienda" || textoFilaHeader != "Tienda - Corporativo" )) {		
			var quitar = true;
				
			while(quitar) {
				if($("#table-results tr").length > indexFilaEnHeader) {
					$('#table-results').DataTable().row($("#table-results tr:last")).remove().draw(false);
				}
				else {
					quitar = false;
				}
			}
		}
			
		var length =  $("#table-results tr").length;
			
		if(textoFilaHeader == "Tienda - Tienda" || textoFilaHeader == "Tienda - Corporativo" ) {
			length = 1;
			csiApi.agregarEncabezados(3, "Calificación CSI", false);
			csiApi.agregarEncabezados(3, "Zona 1A", false);
			csiApi.agregarEncabezados(3, "Zona 1B", false);
			csiApi.agregarEncabezados(3, "Zona 2", false);
			csiApi.agregarEncabezados(3, "Zona 3", false);
			csiApi.agregarEncabezados(3, "Zona 4", false);
			csiApi.agregarEncabezados(3, "Zona 5", false);
			csiApi.agregarEncabezados(3, "Zona 6", false);
			csiApi.agregarEncabezados(3, "Zona 7", false);
			csiApi.agregarEncabezados(3, "Zona 8", true);
			csiApi.inicializarTablas("table-results", {"bDestroy" : true});
		}
			
			
		contenidoRow="<tr id='" + idFilaHeader + "'>" +
//			"<td> " + length + "</td>" +
				csiApi.getRowHeaderTD(textoFilaHeader, resultHeaderDto) +
			"</tr>"
			
		$('#table-results').DataTable().row.add($(contenidoRow)[0]).draw(false);
		
		Table.applyFontColor('#table-results-detail');
		Table.applyFontColor("#corp-areas-table");
		Table.applyFontColor('#table-results');
	};
	
	this.getRowHeaderTD = function (textoFilaHeader, resultHeaderDto) {
		
		return "<td>" + textoFilaHeader + "</td>" +
				"<td>" + resultHeaderDto["result"].toFixed(2) + "</td>" +
				"<td>" + resultHeaderDto["countZ1A"].toFixed(2) + "</td>" +
				"<td>" + resultHeaderDto["countZ1B"].toFixed(2) + "</td>" +
				"<td>" + resultHeaderDto["countZ02"].toFixed(2) + "</td>" +
				"<td>" + resultHeaderDto["countZ03"].toFixed(2) + "</td>" +
				"<td>" + resultHeaderDto["countZ04"].toFixed(2) + "</td>" +
				"<td>" + resultHeaderDto["countZ05"].toFixed(2) + "</td>" +
				"<td>" + resultHeaderDto["countZ06"].toFixed(2) + "</td>" +
				"<td>" + resultHeaderDto["countZ07"].toFixed(2) + "</td>" +
				"<td>" + resultHeaderDto["countZ08"].toFixed(2) + "</td>" +
				"<td>" + resultHeaderDto["countZBT"].toFixed(2) + "</td>" ;
	};

	
	this.invokeMatrixReportCSI = function (data, idAgrupador) {	
		$.ajax({    	
	        url: '@{csi.ResultsCSI.getMatrixReportJSON}',
	        type: 'POST',//'POST',	        
	        cache: false,	  
	        data: data,
	        success: function (response) {		        	
	        	if(response['nextCatalog'].length > 0){
	        		csiApi.updateNextCatalog(response['nextCatalog']);
//	        		csiApi.mostrarFiltros(data.levelFilter);
	        	}
	        	var matrixReport = response['matrixReport'];
	        	
	        	csiApi.processMatrixReport(matrixReport,idAgrupador,data);
	        },
	        beforeSend: function () {
	        	Liverpool.loading("show");
	        },
	        complete: function () {
	        	Liverpool.loading("hide");
	        	//$('#menu-filter-date-lst').css('display', 'none');
	        },
	        error: function () {
	            Liverpool.setMessage($('.inner-message'), 'No se pudo aplicar el filtro. Por favor, intente nuevamente.', 'warning');
	        }
	    });
	};
	
	
	
	this.invokeAreaResultsCSI = function(data, idAgrupador) {
		$.ajax({
			url : '@{csi.ResultsCSI.getTableReportAreaJSON}',
			type : 'POST',// 'POST',
			cache : false,
			data : data,
			success : function(response) {
				csiApi.agregarEncabezadosAreas();
				// Actualiza Tabla
				csiApi.addRowHeaderArea(response['resultHeaderAreaDtoList'],idAgrupador,data);
				// Actualiza Filtros
				if(response['nextCatalog'].length > 0) {
	        		var nombre = "";
	        		csiApi.updateNextCatalog(response['nextCatalog']);
	    		}
			},
			beforeSend : function() {
				Liverpool.loading("show");
			},
			complete : function() {
				Liverpool.loading("hide");
			},
			error : function() {
				Liverpool.setMessage(
						$('.inner-message'),
						'No se pudo aplicar el filtro. Por favor, intente nuevamente.',
						'warning');
			}
		});
	};
	
	

	this.processMatrixReport = function(matrixReport,idAgrupador, data){
		//resultsAPI.updateNextCatalog(idNextCatalog,response['nextCatalog']);    	
    	var countColumns = matrixReport['countColumns'];
    	var colNames = matrixReport['colNames'];
    	var colNamesKeys = Object.keys(colNames);

		var levelFilter = data['levelFilter'];
    	

    	if (levelFilter == 1){
	    	csiApi.agregarEncabezados(2,'CSI (Calificación total)', false);		
	    	
			for (var i = 0 ; i < countColumns; i++){
	    		var idColumn = colNamesKeys[i];
	    		// la ultima columna debe ser true para que se agregue la ultima columna de total 
	    		csiApi.agregarEncabezados(2,colNames[idColumn], i==countColumns-1);	
	    	}    	
			csiApi.fillDataMatrixReport(matrixReport,idAgrupador,data);
		}
    	else if (levelFilter > 1){
    		csiApi.fillDataMatrixReport(matrixReport,idAgrupador,data);
    	}
    	
    	/*
		// TODO pasar los datos reales
		contentCorp();
		*/
		Table.applyFontColor('#table-results-detail');
		Table.applyFontColor('#table-results');
		Table.applyFontColor('#corp-areas-table');
	};
	
	this.fillDataMatrixReport = function(matrixReport,idAgrupador,data) {
		var levelFilter = data['levelFilter'];
		var countRows = matrixReport['countRows'];
    	var rowNames = matrixReport['rowNames'];
    	var rowNamesKeys = Object.keys(rowNames);
    	var rows = matrixReport['rows'];
    	
    	// se copia del contentcorp
		var filas = $("#corp-areas-table >tbody >tr").length;
		if( filas > 0)	$("#corp-areas-table").DataTable().clear();

		var espacios = "";
		for (var i = 0 ; i < levelFilter -1; i ++) {
			espacios = espacios + "&nbsp;&nbsp;";
		}
		
		var idFlechitaPadre = idAgrupador+"-"+(levelFilter-1);
		
		
		var children = [];
		for(var i = 0; i < countRows; i++) {
			var idRow = rowNamesKeys[i];			
			var row = rows[idRow];			
			var flechita = "";
			var idFlechita = idRow +"-"+ levelFilter;
			if (levelFilter < 6){
				flechita = "<span id ='span-"+ idFlechita+"' class='glyphicon glyphicon-triangle-right' style='font-size: 12px;' " +
				" 	 onclick='csiApi.zoomMatrixNextLevel(\""+ idFlechita+"\","+ idRow+","+levelFilter +")'	" +
				"	></span>";
			}		
			var trValue = "<tr id ='tr-"+ idFlechita+"'>" +
							"<td style='font-weight: bold;'>" +
							"<span style='font-size: 12px;'>"+espacios+"</span>"+
							flechita+
							rowNames[idRow] + //"(" +idFlechita+")"+
						"</td>" +
						 csiApi.getDataMatrixRowReport(idRow,row,matrixReport) +
						"<td class='view-btn'>" +
							"<span class='glyphicon glyphicon-search' onclick='javascript:csiApi.verComentarios(" + idRow + ");'> </span> Ver" +
						"</td>" +
					"</tr>";
			if (levelFilter == 1){
				$("#corp-areas-table").DataTable().row.add($(
						trValue)[0]
					).draw(false);
			}
			else {
				
				$("#tr-"+idFlechitaPadre).after($(trValue)[0]);
			}

			
		
			children.push(idFlechita);
			
		}
		
		csiApi.childrenDict[idFlechitaPadre] = children;

		Table.applyFontColor("#corp-areas-table");
		Table.applyFontColor("#table-results-detail");
	};
	


	this.zoomMatrixNextLevel = function(idFlechita,idRow, currentLevelFilter){
		
		//var idFlechita = idRow;
		if($('#span-'+idFlechita).attr('class') === "glyphicon glyphicon-triangle-right") {
			// si ya se expandió ese nivel ya mejor los quita
			$('#span-'+idFlechita).removeClass('glyphicon-triangle-right').addClass('glyphicon-triangle-bottom');
			csiApi.extenderRama(idRow,currentLevelFilter);
		}
		else if($('#span-'+idFlechita).attr('class') === "glyphicon glyphicon-triangle-bottom") {
			$('#span-'+idFlechita).removeClass('glyphicon-triangle-bottom').addClass('glyphicon-triangle-right');

			csiApi.borrarCascada(idFlechita);
			csiApi.borrarHeader(currentLevelFilter);
			
		}
		
		
	};
	
	this.extenderRama = function(idRow,currentLevelFilter){
		
		
		if (currentLevelFilter == 1){
			csiApi.setLevel1FilterCorporation(idRow);
		}
		else if (currentLevelFilter == 2){
			csiApi.setLevel2FilterManagement(idRow);
		}
		else if (currentLevelFilter == 3){
			csiApi.setLevel3FilterWorld(idRow);
		}
		else if (currentLevelFilter == 4){
			csiApi.setLevel4FilterSection(idRow);
		}
		else if (currentLevelFilter == 5){
			csiApi.setLevel5FilterEmployee(idRow);
		}
		else if (currentLevelFilter == 6){
			csiApi.setLevel6FilterRanking(idRow);
		}
		if(csiApi.currectLevel0FilterCsiType == 1 || csiApi.currectLevel0FilterCsiType == 3) {
			for (var key in csiApi.childrenDict) {
				var index = key.lastIndexOf("-");
				var keyLevelStr = key.substring(index+1, key.length);
				var keyLevel = parseInt(keyLevelStr);
				if (keyLevel >= currentLevelFilter){
					
					if (key !== (idRow+"-"+currentLevelFilter)){
						csiApi.borrarCascada(key);
						$('#span-'+key).removeClass('glyphicon-triangle-bottom').addClass('glyphicon-triangle-right');	
					}
					
				}
			}
			
		}
		
	};
	
	
	this.borrarCascada = function(idFlechita){
		
		if (idFlechita in csiApi.childrenDict && csiApi.childrenDict[idFlechita] != undefined){
			var children = csiApi.childrenDict[idFlechita];
			
			for (var i = 0 ; i < children.length ; i ++){
				//children[i]
				var idFlechitaHijo  = children[i] ;  
				csiApi.borrarCascada(idFlechitaHijo);
				$("#tr-"+idFlechitaHijo).remove();
			}
		
			csiApi.childrenDict[idFlechita] = undefined;
		}
	};
	
	
	
	this.getDataMatrixRowReport= function(idRow,row,matrixReport){		
    	var countColumns = matrixReport['countColumns'];
    	
    	var colNames = matrixReport['colNames'];
    	var colNamesKeys = Object.keys(colNames);
		
		var rowHTML = "<td>" + row['total'] + "</td>";
		
		for (var i = 0 ; i < countColumns ; i++ ){
			var idCol = colNamesKeys[i];
			
			if((row['values'][idCol])) {
				rowHTML = rowHTML + 
				"<td style='text-align: center;'>" +				
					row['values'][idCol]['total']+
				"</td>";
			} 
			else {
				rowHTML = rowHTML + "<td style='text-align: center;'> -- </td>";
			}
		}
		return rowHTML;
	};
	
	
	
	


	// Para Areas Evaluadas Periodicamente
	this.addRowHeaderArea = function(resultHeaderAreaDtoList,idAgrupador,data) {
		var contenido = [];
		
		var levelFilter = data['levelFilter'];

    	// se copia del contentcorp
//		var filas = $("#corp-areas-table >tbody >tr").length;
//		if( filas > 0)	$("#corp-areas-table").DataTable().clear();

		var espacios = "";
		for (var i = 0 ; i < levelFilter -1; i ++) {
			espacios = espacios + "&nbsp;&nbsp;";
		}
		var idFlechitaPadre = idAgrupador+"-"+(levelFilter-1);
		var children = [];

		for(var i = 0; i < resultHeaderAreaDtoList.length; i++) {
			var e = resultHeaderAreaDtoList[i];
			

			var idManager =  e['idManager'];
			
			var idFlechita = idManager+"-"+(levelFilter);
			
			var flechita ="";
			if (levelFilter < 6){
				flechita = "<span id ='span-"+ idFlechita+"' class='glyphicon glyphicon-triangle-right' style='font-size: 12px;' " +
				" 	 onclick='csiApi.zoomAreaNextLevel(\""+ idFlechita+"\","+ idManager+","+levelFilter +")' ></span>";
			}		
			
			var trValue = "<tr  id='tr-"+idFlechita+"'>" +
					"<td>" +
						"<span style='font-size: 12px;'>"+espacios+"</span>" +
						flechita +
						e["nombreArea"] + "</td>" +
						
						 csiApi.getRowHeaderAreaTD(e) +
						 "<td class='view-btn'>"+
							"<span class='glyphicon glyphicon-search' onclick='javascript:csiApi.verComentarios(" +idManager + ");'> </span> Ver" + 
						"</td>"
				+ "</tr>";
			
			
				if (levelFilter == 1){
					$("#corp-areas-table").DataTable().row.add(
							$(trValue)[0]
						).draw(false);	
				}
				else {
					
					$("#tr-"+idFlechitaPadre).after($(trValue)[0]);
				}
			
				children.push(idFlechita);
		}
		
		csiApi.childrenDict[idFlechitaPadre] = children;
		
//		$("#corp-areas-table").DataTable().rows.add(contenido).draw(false);
		Table.applyFontColor('#table-results-detail');
		Table.applyFontColor('#table-results');
		Table.applyFontColor('#corp-areas-table');
		
	};
	// Para Areas Evaluadas Periodicamente
	this.getRowHeaderAreaTD = function(resultHeaderAreaDto) {
		
		return   "<td>" + resultHeaderAreaDto["result"].toFixed(2) + "</td>" 
				+"<td>" + resultHeaderAreaDto["enero"].toFixed(2) + "</td>" 
				+ "<td>" + resultHeaderAreaDto["febrero"].toFixed(2) + "</td>" 
				+ "<td>" + resultHeaderAreaDto["marzo"].toFixed(2) + "</td>" 
				+ "<td>" + resultHeaderAreaDto["abril"].toFixed(2) + "</td>" 
				+ "<td>" + resultHeaderAreaDto["mayo"].toFixed(2) + "</td>" 
				+ "<td>" + resultHeaderAreaDto["junio"].toFixed(2) + "</td>" 
				+ "<td>" + resultHeaderAreaDto["julio"].toFixed(2) + "</td>" 
				+ "<td>" + resultHeaderAreaDto["agosto"].toFixed(2) + "</td>" 
				+ "<td>" + resultHeaderAreaDto["septiembre"].toFixed(2) + "</td>" 
				+ "<td>" + resultHeaderAreaDto["octubre"].toFixed(2) + "</td>"
				+ "<td>" + resultHeaderAreaDto["noviembre"].toFixed(2) + "</td>"
				+ "<td>" + resultHeaderAreaDto["diciembre"].toFixed(2) + "</td>";
	};
	
	this.zoomAreaNextLevel = function(idFlechita,idManager, currentLevelFilter){
		
		//var idFlechita = idRow;
		if($('#span-'+idFlechita).attr('class') === "glyphicon glyphicon-triangle-right") {
			// si ya se expandió ese nivel ya mejor los quita
			$('#span-'+idFlechita).removeClass('glyphicon-triangle-right').addClass('glyphicon-triangle-bottom');
			csiApi.extenderRama(idManager,currentLevelFilter);
		}
		else if($('#span-'+idFlechita).attr('class') === "glyphicon glyphicon-triangle-bottom") {
			$('#span-'+idFlechita).removeClass('glyphicon-triangle-bottom').addClass('glyphicon-triangle-right');

			csiApi.borrarCascada(idFlechita);			
			csiApi.borrarHeader(currentLevelFilter);
		}
	};
	
}

var csiApi = new CsiAPI();