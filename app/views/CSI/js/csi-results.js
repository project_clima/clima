var dataTableResults = [];

var globalVariable = null;

//funcion para cuando se actauliza el filtro de la fecha 
function updateInformationWithDateFilters(){
	// SE actualizan los graficos
//	alert("update");
	graphApi.invokeGraphResults({});
	// se reinician las tablas
	var tempCsiType = resultsAPI.currectLevel0FilterCsiType;
	if (tempCsiType != 0){
		resultsAPI.currectLevel0FilterCsiType = 100;
		resultsAPI.setLevel0FilterCsiType(tempCsiType);
	}
	
}

// funcion para cuando se actauliza el filtro de la encusta
function actualizarFiltersCSI(){
	updateInformationWithDateFilters();
		
}

//------------------------------------
// ------ Eventos de Cada Filtro ------
// ------------------------------------
var bindEvents = function() {
	
	$("#csi-type").change(function(e){
		
		var valueSelected = this.value;
		$("#select-corp-adress").css('display', 'none');
		$("#select-adress").css('display', 'none');
		$("#select-world").css('display', 'none');
		$("#select-section").css('display', 'none');
		$("#select-area").css('display', 'none');
		
		globalVariable = valueSelected;
		resultsAPI.setLevel0FilterCsiType(valueSelected);
		resultsAPI.createSurveyList(valueSelected);
		if(filtersAPI.isSurveyFilter != "no"){
			filtersAPI.cleanySurveyFilter();
		}
		
		if(globalVariable == "Seleccionar tipo"){
			filtersAPI.cleanCSITypeFilter();
		}else{
			filtersAPI.applyCSITypeFilter(globalVariable);
		}
	});
	
	
	
	$('#csi-corp-address').change(function() {
		$("#select-adress").css('display', 'none');
		$("#select-world").css('display', 'none');
		$("#select-section").css('display', 'none');
		$("#select-area").css('display', 'none');
		var idCorporationFilter = $(this).val();
		globalVariable = idCorporationFilter;
		
		if(idCorporationFilter == ""  || idCorporationFilter == null || idCorporationFilter == undefined ||
				$("#csi-type").val() == ""  || $("#csi-type").val() == null || $("#csi-type").val() == undefined ){
			filtersAPI.cleanCorporationFilter();
		}else{
			resultsAPI.setLevel1FilterCorporation(idCorporationFilter);
			filtersAPI.applyCorporationFilter(idCorporationFilter);
		}
	});

	$('#csi-address').change(function() {
		$("#select-world").css('display', 'none');
		$("#select-section").css('display', 'none');
		$("#select-area").css('display', 'none');
		var idManagementFilter = $(this).val();
		globalVariable = idManagementFilter;
		
		if(idManagementFilter == ""  || idManagementFilter == null || idManagementFilter == undefined ){
			filtersAPI.cleanManagementFilter();
		}else{
			resultsAPI.setLevel2FilterManagement(idManagementFilter);
			filtersAPI.applyManagementFilter(globalVariable);
		}
	});

	$('#csi-world').change(function() {
		$("#select-section").css('display', 'none');
		$("#select-area").css('display', 'none');
		var idWorldFilter = $(this).val();
		globalVariable = idWorldFilter;
		
		if(idWorldFilter == ""  || idWorldFilter == null || idWorldFilter == undefined ){
			filtersAPI.cleanWorldFilter();
		}else{
			resultsAPI.setLevel3FilterWorld(idWorldFilter);
			filtersAPI.applyWorldFilter(globalVariable);
		}
	});

	$('#csi-section').change(function() {
		$("#select-area").css('display', 'none');
		var idSectionFilter = $(this).val();
		globalVariable = idSectionFilter;
		
		if(idSectionFilter == ""  || idSectionFilter == null || idSectionFilter == undefined ){
			filtersAPI.cleanSectionFilter();
		}else{
			resultsAPI.setLevel4FilterSection(idSectionFilter);
			filtersAPI.applySectionFilter(globalVariable);
		}
	});
	
	
	$('#csi-area').change(function() {
		var idEvaluatedEmployee = $(this).val();
		globalVariable = idEvaluatedEmployee
		
		if(idEvaluatedEmployee == ""  || idEvaluatedEmployee == null || idEvaluatedEmployee == undefined ){
			filtersAPI.cleanEvaluatedFilter();
		}else{
			resultsAPI.setLevel5FilterEmployee(idEvaluatedEmployee);
			filtersAPI.applyEvaluatedFilter(globalVariable);
		}
	});
	
	$('#csi-ranking').change(function() {
		var idRankingFilter = $(this).val();
		globalVariable = idRankingFilter
		
		if(idRankingFilter == ""  || idRankingFilter == null || idRankingFilter == undefined ){}else{
			resultsAPI.setLevel6FilterRanking(idRankingFilter);
		}
	});
};

$(document).ready(function() {
	$("#nav-environment").hide();
	$("#nav-csi").fadeIn(200);
	$("#corp-areas").css('display', 'none');
	$("#select-corp-adress").css('display', 'none');
	$("#select-adress").css('display', 'none');
	$("#select-world").css('display', 'none');
	$("#select-section").css('display', 'none');
	$("#select-area").css('display', 'none');

	Table.applyFontColor('#table-results-detail');
	Table.applyFontColor("#corp-areas-table");
	Table.applyFontColor('#table-results');
	
	if(filtersAPI.isCsiFilter != "no"){
		filtersAPI.cleanCSITypeFilter();
	}
	
	if(filtersAPI.isCorporationFilter != "no"){
		filtersAPI.cleanCorporationFilter();
	}
	
	if(filtersAPI.isManagemetFilter != "no"){
		filtersAPI.cleanManagementFilter();
	}
	
	if(filtersAPI.isWorldFilter != "no"){
		filtersAPI.cleanWorldFilter();
	}
	
	if(filtersAPI.isSectionFilter != "no"){
		filtersAPI.cleanSectionFilter();
	}
	
	if(filtersAPI.isEvaluatedFilter != "no"){
		filtersAPI.cleanEvaluatedFilter();
	}
	
	$('#table-results').DataTable({
		"oLanguage": {
			"sZeroRecords": "No hay registros",
			"sInfo": "",
			"sInfoEmpty": "",
			"sPaginatePrevious": "",
			"sProcessing": "Cargando...",
			"sLengthMenu": "",
			"sSearch": "Buscar"
		},
		"bPaginate": false,
		"bLengthChange": false,
		"bFilter" : false
	});
	
	resultsAPI.inicializarTabla("table-results-detail", {});
	resultsAPI.inicializarTabla("corp-areas-table", {});
	
	bindEvents();
});

$('#download').click(function() {
	if(resultsAPI.currectLevel0FilterCsiType == 1 || resultsAPI.currectLevel0FilterCsiType == 3){
		location.href = '@{csi.ResultsDownloadCSI.downloadTienda()}?csiType=' + resultsAPI.currectLevel0FilterCsiType +
			"&levelFilter=" + resultsAPI.currentFiltersLevel +
			"&corporation=" + resultsAPI.currectLevel1FilterCorporation +
			"&management=" + resultsAPI.currectLevel2FilterManagement +
			"&world=" + resultsAPI.currectLevel3FilterWorld +
			"&section=" + resultsAPI.currectLevel4FilterSection +
			"&evaluated=" + resultsAPI.currectLevel5FilterEmployee;
	}else{
		if(resultsAPI.currectLevel0FilterCsiType == 2){
			location.href = '@{csi.ResultsDownloadCSI.downloadCorporativo()}?csiType=' + resultsAPI.currectLevel0FilterCsiType +
			"&levelFilter=" + resultsAPI.currentFiltersLevel +
			"&corporation=" + resultsAPI.currectLevel1FilterCorporation +
			"&management=" + resultsAPI.currectLevel2FilterManagement +
			"&world=" + resultsAPI.currectLevel3FilterWorld +
			"&section=" + resultsAPI.currectLevel4FilterSection +
			"&evaluated=" + resultsAPI.currectLevel5FilterEmployee;
		}else{
			if(resultsAPI.currectLevel0FilterCsiType == 4){
				location.href = '@{csi.ResultsDownloadCSI.downloadAreas()}?csiType=' + resultsAPI.currectLevel0FilterCsiType +
					"&levelFilter=" + resultsAPI.currentFiltersLevel +
					"&corporation=" + resultsAPI.currectLevel1FilterCorporation +
					"&management=" + resultsAPI.currectLevel2FilterManagement +
					"&world=" + resultsAPI.currectLevel3FilterWorld +
					"&section=" + resultsAPI.currectLevel4FilterSection +
					"&evaluated=" + resultsAPI.currectLevel5FilterEmployee;
			}
		}
	}
});

$('#btnDownloadBD').click(function() {
	Liverpool.loading("show");
	location.href = '@{csi.ResultsDownloadCSI.downloadBD()}?management=' + resultsAPI.currectLevel2FilterManagement;
	setTimeout ("Liverpool.loading('hide')", 9000); 
});

function printResults() {
	var divToPrint = document.getElementById("table-results-detail");
	
	newWin = window.open("");
	newWin.document.title = "Resultados";
	newWin.document.write(divToPrint.outerHTML);
	newWin.print();
	newWin.close();
}

function ResultsAPI() {
	//  ----------------------------------------------
	//  ------   Funciones para Afectar la Tabla 
	//  ----------------------------------------------
	
	//Inicializar o refrescar tablas
	this.inicializarTabla = function(idTable, destroy){
		tableResults = $('#' + idTable).DataTable({
			"bAutoWidth" : false,
			"oLanguage": {
				"sZeroRecords": "Ningún Registro",
				"sInfo": "Mostrando registros de _START_ al _END_ de _TOTAL_ totales",
				"sInfoEmpty": "0 Eventos",
				"sPaginatePrevious": "Previous page",
				"sProcessing": "Cargando...",
				"sLengthMenu": "Mostrar _MENU_ registros",
				"sSearch": "Buscar",
				"oPaginate": {
					"sFirst":    "<<",
					"sLast":     ">>",
					"sNext":     ">",
					"sPrevious": "<"
				}
			},
			destroy
		});

		$("#" + idTable + "_paginate").css("padding", 0);
		$("#" + idTable + "_info").css("font-size", 12);
		$("#" + idTable + "_length").css("font-size", 12);
		$("#" + idTable + "_filter").hide();
		$("#" + idTable + "_length select").css({
			paddingTop: "0",
			paddingBottom: "0",
			height: "32px",
			fontSize: "12px"
		});
	}
	
	this.createSurveyList = function(csiType){
		$("#menu-filter-survey-name-lst").empty();
		if(csiType>0&&csiType!="Sleccionar tipo"){
			$.ajax({
		         url: "@{csi.SurveysCSI.surveyListJSON()}",
		         type:'get',
		         async: true,
		         data: {
		             type: csiType
		         },
		         beforeSend: function (){
		         },
		         complete: function (){
		         },
		         success: function (data) {
		             $("#menu-filter-survey-name-lst").append("<option value=''>Selecciona una Opción </option>");
		             for (var i = 0; i < data.length; i++) {
		                 $("#menu-filter-survey-name-lst").append("<option value='" + data[i][0]+"'>"+data[i][1] +"</option>" )
		             };
		         }
		     });
		}	
	}

	
	this.agregarCsiHeader = function(idHeader, header, level){
		var signo = "";
		if(level > 1) signo = "&nbsp;&nbsp; > &nbsp;&nbsp;";
		else	$("#csi-header").empty();
		if(!$("#" + idHeader).length){
			$("#csi-header").append(
				"<label id='" + idHeader + "' class='normal-lbl'>" +
					signo + " "+ header + " "  +
				"</label>");
		}
	};
	
	this.borrarCsiHeader = function(level){
		for(var i=level;i<$("#csi-header >label").length;i++){
			$("#csi-header >label:last").remove();
		}
	}
	
	this.agregarEncabezados = function(type, encabezado, final) {
		if(type == 1) {
			if(!$("#table-results-ver").length) {
				$("#table-results-detail tr:first").append("<th> " + encabezado + " </th>");
				if(final) {
					$("#table-results-detail tr:first").append(
							"<th id='table-results-ver'> Comentarios </th>");
					resultsAPI.inicializarTabla("table-results-detail", {"bDestroy":true});
				}
			}
		}
		else {
			if(!$("#corp-areas-ver").length){
				$("#corp-areas-table tr:first").append("<th>" + encabezado + "</th>");
				
				if(final) {
					$("#corp-areas-table tr:first").append("<th id='corp-areas-ver'> Detalle </th>");
					resultsAPI.inicializarTabla("corp-areas-table", {"bDestroy":true});
				}
			}
		}
	}
	

	this.limpiarTabla = function(tablaId){
		var leng = $("#"+tablaId+" tbody >tr >td").length;
		for(var j=0; j<leng;j++){
			$("#"+tablaId+" tbody >tr >td").remove();
		}
		leng = $("#"+tablaId+" tbody >tr").length;
		for(var i=0 ; i<leng ; i++){
			$("#"+tablaId+" tbody >tr").remove();
		}
		
		$("#"+tablaId+"").DataTable().clear();
		$("#"+tablaId+"").DataTable().destroy();
		
		if (tablaId === "corp-areas-table"){
			$("#"+tablaId+" thead >tr").html("<th>Áreas</th>");	
		}
		
		if(tablaId === "table-results-detail"){
			var thead = $("#" + tablaId + " thead >tr >th").length;
			for(var i=6;i<thead;i++){
				$("#" + tablaId + " thead >tr >th:last").remove();
			}
		}
		
		resultsAPI.inicializarTabla(tablaId, {"bDestroy":true});
	};

	this.getRowResultDetail = function (resultDetailDto, orders){		
		var temp = "";
		var titulos = ["", "Apoyo", "Soluciones", "Necesidades", "Herramientas"];
		for (var i = 0 ; i < orders.length; i ++) {			
			if(resultDetailDto['p' + orders[i]] == undefined || resultDetailDto['p' + orders[i]] == null ){}
			else{
				temp = temp + "<td>" + resultDetailDto['p' + orders[i]] + "</td>"; 
				
				if(i == (orders.length-1)) {
					resultsAPI.agregarEncabezados(1,'P' + orders[i] + " " + (titulos[orders[i]]!=undefined?titulos[orders[i]]:""), true);
				}
				else {
					resultsAPI.agregarEncabezados(1,'P' + orders[i] + " " + (titulos[orders[i]]!=undefined?titulos[orders[i]]:""), false);
				}
			}
		}
		//resultsAPI.inicializarTabla("table-results-detail", {"bDestroy" : true});
		
		if((resultsAPI.currectLevel0FilterCsiType == 1 || resultsAPI.currectLevel0FilterCsiType == 3) && (resultsAPI.currectLevel5FilterEmployee != 0 )){
			//resultsAPI.currectLevel5FilterEmployee = 0;
			return "<td>" + resultsAPI.catalogDict[resultDetailDto['idAgrupador']] + "</td>" +
			"<td>" 
				+ resultDetailDto['result'].toFixed(2) + "</td>" +
			"<td>" 
				+ resultDetailDto['detractores'].toFixed(2) + "</td>" +
			"<td>" 
				+ resultDetailDto['neutros'].toFixed(2) + "</td>" +
			"<td>" 
				+ resultDetailDto['promotores'].toFixed(2) + "</td>" +
			temp +
			"<td class='view-btn'>" +
					"<span class='glyphicon glyphicon-search' onclick='see(" + resultDetailDto['idAgrupador'] + ")'></span> Ver"+
			"</td>" ;
		}else{
			
		
			return "<td>" + resultsAPI.catalogDict[resultDetailDto['idAgrupador']]  + "</td>" +
					"<td>" 
						+ resultDetailDto['result'].toFixed(2) + "</td>" +
					"<td>" 
						+ resultDetailDto['detractores'].toFixed(2) + "</td>" +
					"<td>" 
						+ resultDetailDto['neutros'].toFixed(2) + "</td>" +
					"<td>" 
						+ resultDetailDto['promotores'].toFixed(2) + "</td>" +
					temp +
					"<td class='view-btn'>" +
							"<span class='glyphicon glyphicon-search' onclick='see(" + resultDetailDto['idAgrupador'] + ")'></span> Ver"+
					"</td>" ;
		}
	}
	
	this.updateResultDetailTable = function (resultDetailDtoList, orders) {		
		var contenido = [];
		for(var i = 0; i < resultDetailDtoList.length; i++) {
			var e = resultDetailDtoList[i];
				contenido.push($("<tr>" +
						"<td>" +
							(i + 1) +
						"</td>" +
						resultsAPI.getRowResultDetail(e,orders)+
					"</tr>")[0]);
		}
		
		// Se borran las filas de la tabla
		$("#table-results-detail").DataTable().clear();		
		
		//  Se recarga el objeto data table
		$("#table-results-detail").DataTable().rows.add(contenido).draw(false);
		
		Table.applyFontColor('#table-results-detail');
		Table.applyFontColor('#table-results');
		Table.applyFontColor('#corp-areas-table');
	}
	
	this.getRowHeaderTD = function (textoFilaHeader, resultHeaderDto) {
		
		return "<td>" + (textoFilaHeader) + "</td>" +
				"<td>" + resultHeaderDto["result"].toFixed(2) + "</td>" +
				"<td>" + resultHeaderDto["countZ1A"].toFixed(2) + "</td>" +
				"<td>" + resultHeaderDto["countZ1B"].toFixed(2) + "</td>" +
				"<td>" + resultHeaderDto["countZ02"].toFixed(2) + "</td>" +
				"<td>" + resultHeaderDto["countZ03"].toFixed(2) + "</td>" +
				"<td>" + resultHeaderDto["countZ04"].toFixed(2) + "</td>" +
				"<td>" + resultHeaderDto["countZ05"].toFixed(2) + "</td>" +
				"<td>" + resultHeaderDto["countZ06"].toFixed(2) + "</td>" +
				"<td>" + resultHeaderDto["countZ07"].toFixed(2) + "</td>" +
				"<td>" + resultHeaderDto["countZ08"].toFixed(2) + "</td>" +
				"<td>" + resultHeaderDto["countZBT"].toFixed(2) + "</td>" ;
	};


	this.invokeRefuse = function (value) {

        swal({
            title: "",
            text: "¿Esta seguro de impugnar esta calificación?",
            type: "warning", 
            showCancelButton: true,
            confirmButtonText: "Aceptar",
            cancelButtonText: "Cancelar",
            closeOnConfirm: false
        },
        function(){
        	resultsAPI.invokeSendRefuseMailCSI(value);         	
        });

	}
	
	
	this.invokeSendRefuseMailCSI = function (value,data, idNextCatalog) {
		$.ajax({    	
	        url: '@{csi.ResultsCSI.sendRefuseMail}',
	        type: 'POST',//'POST',	        
	        cache: false,	  
	        data: {val: value},
	        success: function (response) {	
	        	swal("Impugnación enviada!", "Recibirás respuesta en los próximos 5 días hábiles.", "success");
	        },
	        beforeSend: function () {
	        	Liverpool.loading("show");
	        },
	        complete: function () {
	        	Liverpool.loading("hide");	        	
	        },
	        error: function () {
	        	swal("Error!", "Ocurrió un error al enviar la impugnación, por favor intente nuevamente!.", "error");
	        }
	    });
	};
	
	
	this.impugnaciones = function () {
		$.ajax({    	
	        url: '@{csi.ResultsCSI.sendSurvey}',
	        type: 'POST',//'POST',	        
	        cache: false,
	        success: function (response) {
	        	if(response.length > 0){
	        		swal({
		                title: "Tiene las siguientes impugnaciones pendientes: ",
		                text: "" + response,
		                type: "warning", 
		                showCancelButton: true,
		                confirmButtonText: "Aceptar",
		                cancelButtonText: "Cancelar",
		                closeOnConfirm: false
		            }, function (isConfirm) {
		                if (!isConfirm) return;
		                $.ajax({
		                	url: '@{csi.ResultsCSI.eliminarImpugnar}',        	
		                    type: "POST",
		                    cache: false,
		                    success: function (response) {
		                    	swal("Impugnaciónes canceladas!","", "success");
		                    },
		                    error: function (xhr, ajaxOptions, thrownError) {
		                        swal("Ocurrió un error", "error");
		                    }
		                });
		            });
	        	}
	        }
	    });
		
        /*swal({
            title: "",
            text: "Tiene las siguientes impugnaciones pendientes: ",
            type: "warning", 
            showCancelButton: true,
            confirmButtonText: "Aceptar",
            cancelButtonText: "Cancelar",
            closeOnConfirm: false
        });*/
	}
	
	this.addRowHeader = function (idFilaHeader,textoFilaHeader, resultHeaderDtoList, indexFilaEnHeader) {
		// Verifica que exista la fila, si no existe la cream si existe la edita
		var resultHeaderDto = resultHeaderDtoList[0];
		var contenidoRow = "";
		var tableResults;
		if($("#" + idFilaHeader).length && (textoFilaHeader != "Tienda - Tienda" || textoFilaHeader != "Tienda - Corporativo" )) {		
			var quitar = true;
				
			while(quitar) {
				if($("#table-results tr").length > indexFilaEnHeader) {
					$('#table-results').DataTable().row($("#table-results tr:last")).remove().draw(false);
				}
				else {
					quitar = false;
				}
			}
		}
			
		var length =  $("#table-results tr").length;
			
		if(textoFilaHeader == "Tienda - Tienda" || textoFilaHeader == "Tienda - Corporativo"){
			length = 1;
			resultsAPI.inicializarTabla("table-results", {"bDestroy" : true});
		}
			
			
		contenidoRow="<tr id='" + idFilaHeader + "'>" +
			"<td> " + length + "</td>" +
				resultsAPI.getRowHeaderTD(textoFilaHeader, resultHeaderDto) +
			"</tr>"
			
		$('#table-results').DataTable().row.add($(contenidoRow)[0]).draw(false);
		
		Table.applyFontColor('#table-results-detail');
		Table.applyFontColor("#corp-areas-table");
		Table.applyFontColor('#table-results');
	};
	
	// Para Areas Evaluadas Periodicamente
	this.getRowHeaderAreaTD = function(resultHeaderAreaDto) {
		return "<td>" + resultHeaderAreaDto["nombreArea"] + "</td>" 		
				+ "<td>" + resultHeaderAreaDto["result"].toFixed(2) + "</td>" 
				+ "<td>" + resultHeaderAreaDto["enero"].toFixed(2) + "</td>" 
				+ "<td>" + resultHeaderAreaDto["febrero"].toFixed(2) + "</td>" 
				+ "<td>" + resultHeaderAreaDto["marzo"].toFixed(2) + "</td>" 
				+ "<td>" + resultHeaderAreaDto["abril"].toFixed(2) + "</td>" 
				+ "<td>" + resultHeaderAreaDto["mayo"].toFixed(2) + "</td>" 
				+ "<td>" + resultHeaderAreaDto["junio"].toFixed(2) + "</td>" 
				+ "<td>" + resultHeaderAreaDto["julio"].toFixed(2) + "</td>" 
				+ "<td>" + resultHeaderAreaDto["agosto"].toFixed(2) + "</td>" 
				+ "<td>" + resultHeaderAreaDto["septiembre"].toFixed(2) + "</td>" 
				+ "<td>" + resultHeaderAreaDto["octubre"].toFixed(2) + "</td>"
				+ "<td>" + resultHeaderAreaDto["noviembre"].toFixed(2) + "</td>"
				+ "<td>" + resultHeaderAreaDto["diciembre"].toFixed(2) + "</td>";
	};
	
	// Para Areas Evaluadas Periodicamente
	this.addRowHeaderArea = function(resultHeaderAreaDtoList, indexFilaEnHeaderArea) {
		var contenido = [];
		for(var i = 0; i < resultHeaderAreaDtoList.length; i++) {
			var e = resultHeaderAreaDtoList[i];
				contenido.push($("<tr>"						
						+ resultsAPI.getRowHeaderAreaTD(e) 
						+ "<td class='view-btn'>"+
							"<span class='glyphicon glyphicon-search' onclick='see(" + e['idManager'] + ")'></span> Ver" + 
						"</td>"
					+ "</tr>")[0]);
		}
		
		$("#corp-areas-table").DataTable().rows.add(contenido).draw(false);
		Table.applyFontColor('#table-results-detail');
		Table.applyFontColor('#table-results');
		Table.applyFontColor('#corp-areas-table');
	}
		
	this.updateNextCatalog = function(idNextCatalog,nextCatalogValues){
		$('#'+idNextCatalog).html('<option value="">Selecciona una Opción</option>');
		var $option = null;
		
		$.each(nextCatalogValues, function(i, cat) {
			$option = $('<option>');
			$option.attr('value', cat['id']);
//			$option.attr('data-type', address[2])
			$option.text(cat['desc']);
			
			$('#' + idNextCatalog).append($option);
			
			resultsAPI.catalogDict[cat['id']] = cat['desc'];
		});         		
	};
	
	// --------------------------------------------------
	// ------ Fin Funciones para Afectar la Tabla ------
	// --------------------------------------------------
	this.currectFiltersLevel = 0;

	this.currectLevel0FilterCsiType = 0;
	this.currectLevel1FilterCorporation = 0;	
	this.currectLevel2FilterManagement = 0;	
	this.currectLevel3FilterWorld = 0;
	this.currectLevel4FilterSection = 0;
	this.currectLevel5FilterEmployee = 0;
	this.currectLevel6FilterRanking = 0;
		
	this.catalogDict = {}; // create an empty array

	
	// ---------------------------------------------
	// ------ Funciones que Invocan Servicios ------
	// ---------------------------------------------
	// TODO implementar ls llamadas conservando los filtos y así 
	this.invokeResultsCSI = function (data,idFilaHeader,textoFilaHeader, indexFilaEnHeader, idNextCatalog){
		$.ajax({    	
	        url: '@{csi.ResultsCSI.getTableReportJSON}',
	        type: 'POST',//'POST',	        
	        cache: false,	  
	        data: data,
	        success: function (response) {	        		
	        	if(response['nextCatalog'].length > 0) {
	        		var nombre = "";
	        		
	        		resultsAPI.updateNextCatalog(idNextCatalog,response['nextCatalog']);
	        		resultsAPI.mostrarFiltros(data.levelFilter);
	        		
	        		if(idNextCatalog == "csi-corp-address"){
	        			nombre = "Dirección Corporativa";
	        		}else if(idNextCatalog == "csi-address"){
	        			nombre = "Dirección";
	        		}else if(idNextCatalog == "csi-world"){
	        			nombre = "Gerencia / Mundo";
	        		}else if(idNextCatalog == "csi-section"){
	        			nombre = "Comprador / Coordinador";
	        		}else if(idNextCatalog == "csi-area"){
	        			nombre = "Evaluado";
	        		}
	        		
	        		$("#result-header").html(nombre);
	    		}
	        	
	        	resultsAPI.addRowHeader(idFilaHeader,textoFilaHeader, response['resultHeaderDtoList'], indexFilaEnHeader);
	        	resultsAPI.updateResultDetailTable(response['resultDetailDtoList'], response['orders']);	        	
	        },
	        beforeSend: function () {
	        	Liverpool.loading("show");
	        },
	        complete: function () {
	        	Liverpool.loading("hide");
	        	//$('#menu-filter-date-lst').css('display', 'none');
	        },
	        error: function () {
	            Liverpool.setMessage($('.inner-message'), 'No se pudo aplicar el filtro. Por favor, intente nuevamente.', 'warning');
	        }
	    });
	};
	
	this.invokeAreaResultsCSI = function(data, invokeResultsCSI, idNextCatalog) {
		$.ajax({
			url : '@{csi.ResultsCSI.getTableReportAreaJSON}',
			type : 'POST',// 'POST',
			cache : false,
			data : data,
			success : function(response) {
				// Actualiza Tabla
				resultsAPI.addRowHeaderArea(response['resultHeaderAreaDtoList'], invokeResultsCSI);
				// Actualiza Filtros
				if(response['nextCatalog'].length > 0) {
	        		var nombre = "";
	        		
	        		resultsAPI.updateNextCatalog(idNextCatalog,
							response['nextCatalog']);
	        		resultsAPI.mostrarFiltros(data.levelFilter);
	        		
	        		if(idNextCatalog == "csi-corp-address"){
	        			nombre = "Dirección Corporativa";
	        		}else if(idNextCatalog == "csi-address"){
	        			nombre = "Dirección";
	        		}else if(idNextCatalog == "csi-world"){
	        			nombre = "Gerencia / Mundo";
	        		}else if(idNextCatalog == "csi-section"){
	        			nombre = "Comprador / Coordinador";
	        		}else if(idNextCatalog == "csi-area"){
	        			nombre = "Evaluado";
	        		}
	        		
	        		$("#result-header").html(nombre);
	    		}
			},
			beforeSend : function() {
				Liverpool.loading("show");
			},
			complete : function() {
				Liverpool.loading("hide");
			},
			error : function() {
				Liverpool.setMessage(
						$('.inner-message'),
						'No se pudo aplicar el filtro. Por favor, intente nuevamente.',
						'warning');
			}
		});
	};
	
	this.mostrarFiltros = function(nivel) {
		switch(nivel) {
		case 1:
			$("#select-corp-adress").css('display', 'inline');
			break;
		case 2:
			$("#select-adress").css('display', 'inline');
			break;
		case 3:
			$("#select-world").css('display', 'inline');
			break;
		case 4:
			$("#select-section").css('display', 'inline');
			break;
		case 5:
			$("#select-area").css('display', 'inline');
			break;
		case 6:
			$("#select-ranking").css('display', 'inline');
			break;
		}
	}

	this.processMatrixReport = function(matrixReport){
		//resultsAPI.updateNextCatalog(idNextCatalog,response['nextCatalog']);    	
    	var countColumns = matrixReport['countColumns'];
    	var colNames = matrixReport['colNames'];
    	var colNamesKeys = Object.keys(colNames);
    	
		resultsAPI.agregarEncabezados(2,'CSI (Calificación total)', false);	
    	
		for (var i = 0 ; i < countColumns; i++){
    		var idColumn = colNamesKeys[i];
    		// la ultima columna debe ser true para que se agregue la ultima columna de total 
    		resultsAPI.agregarEncabezados(2,colNames[idColumn], i==countColumns-1);	
    	}    	
    	resultsAPI.fillDataMatrixReport(matrixReport);
    	/*
		// TODO pasar los datos reales
		contentCorp();
		*/
		Table.applyFontColor('#table-results-detail');
		Table.applyFontColor('#table-results');
		Table.applyFontColor('#corp-areas-table');
	};
	
	this.fillDataMatrixReport = function(matrixReport) {
		var countRows = matrixReport['countRows'];
    	var rowNames = matrixReport['rowNames'];
    	var rowNamesKeys = Object.keys(rowNames);
    	var rows = matrixReport['rows'];
    	
		var filas = $("#corp-areas-table >tbody >tr").length;
		
		if( filas > 0) {
			$("#corp-areas-table").DataTable().clear();
		}
		
		for(var i = 0; i < countRows; i++) {
			var idRow = rowNamesKeys[i];			
			var row = rows[idRow];			
			$("#corp-areas-table").DataTable().row.add($(
				"<tr>" +
					"<td style='font-weight: bold;'>" +
						rowNames[idRow] +
					"</td>" +
					 resultsAPI.getDataMatrixRowReport(idRow,row,matrixReport) +
					"<td class='view-btn' style='text-align: center;'>" +
						"<span class='glyphicon glyphicon-search' onclick='see(" + idRow + ")'></span> Ver" +
					"</td>" +
				"</tr>")[0]).draw(false);
		}
			/*
			for(var i = 0; i < 10; i++) {
				$("#corp-areas-table tbody").append(
					"<tr>" +
						"<td style='cursor: pointer;'>" +
							"<span class='glyphicon glyphicon-triangle-right' style='font-size: 12px;'></span>" +
							" Dirección " + (i + 1) +
						"</td>" +
						"<td style='text-align: center;'>" +
							(Math.random() * 25 + 75).toFixed(2) +
						"</td>" +
						"<td style='text-align: center;'>" +
							(Math.random() * 25 + 75).toFixed(2) +
						"</td>" +
						"<td style='text-align: center;'>" +
							(Math.random() * 25 + 75).toFixed(2) +
						"</td>" +
						"<td style='text-align: center;'>" +
							(Math.random() * 25 + 75).toFixed(2) +
						"</td>" +
						"<td style='text-align: center;'>" +
							"<button class='btn btn-link btn-see-detail'>Ver detalle</button>" +
						"</td>" +
					"</tr>"
				);
			}*/
	};
	
	this.getDataMatrixRowReport= function(idRow,row,matrixReport){		
    	var countColumns = matrixReport['countColumns'];
    	
    	var colNames = matrixReport['colNames'];
    	var colNamesKeys = Object.keys(colNames);
		
		var rowHTML = "<td>" + row['total'] + "</td>";
		
		for (var i = 0 ; i < countColumns ; i++ ){
			var idCol = colNamesKeys[i];
			
			if((row['values'][idCol])) {
				rowHTML = rowHTML + 
				"<td style='text-align: center;'>" +				
					row['values'][idCol]['total']+
				"</td>";
			} 
			else {
				rowHTML = rowHTML + "<td style='text-align: center;'> -- </td>";
			}
		}
		return rowHTML;
	};
	

	this.invokeMatrixReportCSI = function (data, idNextCatalog) {	
		$.ajax({    	
	        url: '@{csi.ResultsCSI.getMatrixReportJSON}',
	        type: 'POST',//'POST',	        
	        cache: false,	  
	        data: data,
	        success: function (response) {		        	
	        	if(response['nextCatalog'].length > 0 && idNextCatalog != null){
	        		resultsAPI.updateNextCatalog(idNextCatalog,response['nextCatalog']);
	        		resultsAPI.mostrarFiltros(data.levelFilter);
	        	}
	        	var matrixReport = response['matrixReport'];
	        	
	        	resultsAPI.processMatrixReport(matrixReport);
	        },
	        beforeSend: function () {
	        	Liverpool.loading("show");
	        },
	        complete: function () {
	        	Liverpool.loading("hide");
	        	//$('#menu-filter-date-lst').css('display', 'none');
	        },
	        error: function () {
	            Liverpool.setMessage($('.inner-message'), 'No se pudo aplicar el filtro. Por favor, intente nuevamente.', 'warning');
	        }
	    });
	};
	
	this.getCurrentFiltersLevel = function() {
		return resultsAPI.currentFiltersLevel;
	}

	this.cleanFilters = function(csiType) {
		// TODO Implementar el limpiado de todos los filtros 
		resultsAPI.currentFiltersLevel = 0;
		resultsAPI.currectLevel0FilterCsiType = 0;
		resultsAPI.currectLevel1FilterCorporation = 0;
		resultsAPI.currectLevel2FilterManagement = 0;
		resultsAPI.currectLevel3FilterWorld = 0;
		resultsAPI.currectLevel4FilterSection = 0;
		resultsAPI.currectLevel5FilterEmployee = 0;
		resultsAPI.currectLevel6FilterRanking = 0;
	}
	// -------------------------------------------------
	// ------ Fin Funciones que Invocan Servicios ------
	// -------------------------------------------------
	
	// -------------------------------------------------------
	// ------ Funciones que se Invocan en cada Filtrado ------ 
	// -------------------------------------------------------
	this.setLevel0FilterCsiType = function(csiType) {
		
		if (resultsAPI.currectLevel0FilterCsiType != 0 && resultsAPI.currectLevel0FilterCsiType != csiType){
			resultsAPI.limpiarTabla("corp-areas-table");
			resultsAPI.limpiarTabla("table-results-detail");
			resultsAPI.limpiarTabla("table-results");
		}
		
		resultsAPI.currectLevel0FilterCsiType = csiType;
		resultsAPI.currentFiltersLevel = 1;
		
		// Tienda-Tienda o Tienda-Corporativo
		if (csiType == 1 || csiType == 3) {
			var header = "";
			if(csiType == 1)	header = "Tienda - Tienda";
			else	header = "Tienda - Corporativo";
			resultsAPI.agregarCsiHeader("compras-total-header", header, 1);
			
			$("#corp-areas").css('display', 'none');
			$("#table-general").css('display', 'inline');
			$("#principal-table").css('display', 'inline');
			
//			$("#table-results").empty();
//			resultsAPI.inicializarTabla("table-results", {})
			
			var data = {'csiType':		csiType, 
					    'levelFilter': 	resultsAPI.currentFiltersLevel
						};
			
			resultsAPI.invokeResultsCSI(data,	// csitype
					'compras-total',            // idFilaHeader
					header,					// textoFilaHeader
					1, 							// indexFilaEnHeader
					'csi-corp-address' 			// idCatalogoSiguiente 
			);
		} // Corporativo-Corporativo
		else if (csiType == 2) {
			resultsAPI.agregarCsiHeader("compras-total-header", "Corporativo - Corporativo", 1);
			
			$("#table-general").css('display', 'none');
			$("#principal-table").css('display', 'none');
			$("#corp-areas").css('display', 'inline');
			
//			$("#table-results").empty();
//			resultsAPI.inicializarTabla("table-results", {});
//			resultsAPI.limpiarTabla("corp-areas-table");
			var data = {'csiType':		csiType, 
						'levelFilter': 	resultsAPI.currentFiltersLevel
						};
			
			resultsAPI.invokeMatrixReportCSI(data, 	// csitype
					'csi-corp-address'				// idCatalogoSiguiente 
			);
			
		
		} 		
		
		//4	Áreas evaluadas periódicamente
		else if (csiType == 4) { 
			resultsAPI.agregarCsiHeader("compras-total-header", "Áreas Evaluadas Periódicamente", 1);
			
			$("#table-general").css('display', 'none');
			$("#principal-table").css('display', 'none');
			$("#corp-areas").css('display', 'inline');
			
//			$("#table-results").empty();
//			resultsAPI.inicializarTabla("table-results", {});
//			resultsAPI.limpiarTabla("corp-areas-table");
			//resultsAPI.inicializarTabla("corp-areas-table", {});
			
			resultsAPI.agregarEncabezados(2,'CSI', false);
			resultsAPI.agregarEncabezados(2,'Enero', false);
			resultsAPI.agregarEncabezados(2,'Febrero', false);
			resultsAPI.agregarEncabezados(2,'Marzo', false);
			resultsAPI.agregarEncabezados(2,'Abril', false);
			resultsAPI.agregarEncabezados(2,'Mayo', false);
			resultsAPI.agregarEncabezados(2,'Junio', false);
			resultsAPI.agregarEncabezados(2,'Julio', false);
			resultsAPI.agregarEncabezados(2,'Agosto', false);
			resultsAPI.agregarEncabezados(2,'Septiembre', false);
			resultsAPI.agregarEncabezados(2,'Octubre', false);
			resultsAPI.agregarEncabezados(2,'Noviembre', false);
			resultsAPI.agregarEncabezados(2,'Diciembre', true);
			
			var data = {
					'csiType': 		csiType,
					'levelFilter':	resultsAPI.currentFiltersLevel
				};
			
			resultsAPI.invokeAreaResultsCSI(data,	// Datos para Busqueda
						'area', 					// Id Fila Header
						'csi-corp-address' 			// Id Catalog Siguiente
			);
			
		}
		else {
//			$("#table-results").empty();
//			resultsAPI.inicializarTabla("table-results", {});
//			$("#corp-areas-table").DataTable().clear();
		}
	};
	
	// Filtro Corporate Address
	this.setLevel1FilterCorporation = function(idCorporationFilter) {
		// Limpiar los siguientes filtros
		// en caso que el nivel de filtrado sea el anterior, en caso contrario hay que limpiar los otros filtros. 
		resultsAPI.currentFiltersLevel = 2;
		resultsAPI.currectLevel1FilterCorporation = idCorporationFilter;
		
		var data = {'csiType': 		resultsAPI.currectLevel0FilterCsiType ,
					'levelFilter': 	resultsAPI.currentFiltersLevel,
					'corporation': 	idCorporationFilter 					
			      };
		resultsAPI.borrarCsiHeader(1);
		resultsAPI.agregarCsiHeader("direccion-corp-header", $('#csi-corp-address option:selected').text(),2);
		// Tienda-Tienda	
		if(resultsAPI.currectLevel0FilterCsiType == 1 || resultsAPI.currectLevel0FilterCsiType == 3) {			
			resultsAPI.limpiarTabla("table-results-detail");
			resultsAPI.invokeResultsCSI(data,						// datos para el 
					'direccion-corp',            					// idFilaHeader
					$('#csi-corp-address option:selected').text(),	// textoFilaHeader
					2, 												// indexFilaEnHeader
					'csi-address' 									// idCatalogoSiguiente 
			);
		}	// Corporativo-Corporativo
		else if (resultsAPI.currectLevel0FilterCsiType == 2) {			
			$("#table-general").css('display', 'none');
			$("#principal-table").css('display', 'none');
			$("#corp-areas").css('display', 'inline');
			
			resultsAPI.limpiarTabla("corp-areas-table");
			resultsAPI.invokeMatrixReportCSI(data, 'csi-address');							
		}
		
		else if (resultsAPI.currectLevel0FilterCsiType == 4) { 
			$("#table-general").css('display', 'none');
			$("#principal-table").css('display', 'none');
			$("#corp-areas").css('display', 'inline');
			

			resultsAPI.limpiarTabla("corp-areas-table");
			
			resultsAPI.agregarEncabezados(2,'CSI', false);
			resultsAPI.agregarEncabezados(2,'Enero', false);
			resultsAPI.agregarEncabezados(2,'Febrero', false);
			resultsAPI.agregarEncabezados(2,'Marzo', false);
			resultsAPI.agregarEncabezados(2,'Abril', false);
			resultsAPI.agregarEncabezados(2,'Mayo', false);
			resultsAPI.agregarEncabezados(2,'Junio', false);
			resultsAPI.agregarEncabezados(2,'Julio', false);
			resultsAPI.agregarEncabezados(2,'Agosto', false);
			resultsAPI.agregarEncabezados(2,'Septiembre', false);
			resultsAPI.agregarEncabezados(2,'Octubre', false);
			resultsAPI.agregarEncabezados(2,'Noviembre', false);
			resultsAPI.agregarEncabezados(2,'Diciembre', true);
			
			
			resultsAPI.invokeAreaResultsCSI(data,	// Datos para Busqueda
						'area', 					// Id Fila Header
						'csi-address' 			// Id Catalog Siguiente
			);
			
		}	
	};
	
	// Filtro Address
	this.setLevel2FilterManagement = function(idManagementFilter){		
		resultsAPI.currentFiltersLevel = 3;
		resultsAPI.currectLevel2FilterManagement = idManagementFilter;
		resultsAPI.borrarCsiHeader(2);
		resultsAPI.agregarCsiHeader("direccion-header", $('#csi-address option:selected').text(),3);
		var data = { 'csiType': 		resultsAPI.currectLevel0FilterCsiType,
					 'levelFilter' : 	resultsAPI.currentFiltersLevel,
					 'corporation': 	resultsAPI.currectLevel1FilterCorporation,
					 'management': 		idManagementFilter
				   };
		
		// Tienda-Tienda
		if(resultsAPI.currectLevel0FilterCsiType == 1 || resultsAPI.currectLevel0FilterCsiType == 3) {			
			resultsAPI.limpiarTabla("table-results-detail");
			resultsAPI.invokeResultsCSI(data,					// datos para el 
					'direccion',            					// idFilaHeader
					$('#csi-address option:selected').text(),	// textoFilaHeader
					3, 											// indexFilaEnHeader
					'csi-world' 								// idCatalogoSiguiente 
			);
		} // Corporativo-Corporativo		
		else if (resultsAPI.currectLevel0FilterCsiType == 2) {			
			$("#table-general").css('display', 'none');
			$("#principal-table").css('display', 'none');
			$("#corp-areas").css('display', 'inline');
			
			resultsAPI.limpiarTabla("corp-areas-table");
			resultsAPI.invokeMatrixReportCSI(data, 'csi-world');							
		} 
		// Areas Evaluadas Periodicamente
		else if (resultsAPI.currectLevel0FilterCsiType == 4) { 
			$("#table-general").css('display', 'none');
			$("#principal-table").css('display', 'none');
			$("#corp-areas").css('display', 'inline');
		
			
			resultsAPI.limpiarTabla("corp-areas-table");
			resultsAPI.agregarEncabezados(2,'CSI', false);
			resultsAPI.agregarEncabezados(2,'Enero', false);
			resultsAPI.agregarEncabezados(2,'Febrero', false);
			resultsAPI.agregarEncabezados(2,'Marzo', false);
			resultsAPI.agregarEncabezados(2,'Abril', false);
			resultsAPI.agregarEncabezados(2,'Mayo', false);
			resultsAPI.agregarEncabezados(2,'Junio', false);
			resultsAPI.agregarEncabezados(2,'Julio', false);
			resultsAPI.agregarEncabezados(2,'Agosto', false);
			resultsAPI.agregarEncabezados(2,'Septiembre', false);
			resultsAPI.agregarEncabezados(2,'Octubre', false);
			resultsAPI.agregarEncabezados(2,'Noviembre', false);
			resultsAPI.agregarEncabezados(2,'Diciembre', true);
			
			
			resultsAPI.invokeAreaResultsCSI(data,	// Datos para Busqueda
						'area', 					// Id Fila Header
						'csi-world' 			// Id Catalog Siguiente
			);
			
		}
	};
	
	// Filtro World
	this.setLevel3FilterWorld = function(idWorldFilter){		
		resultsAPI.currentFiltersLevel = 4;
		resultsAPI.currectLevel3FilterWorld = idWorldFilter;
		resultsAPI.borrarCsiHeader(3);
		resultsAPI.agregarCsiHeader("world-header", $('#csi-world option:selected').text(),4);
		var data = {'csiType': 		resultsAPI.currectLevel0FilterCsiType,
					'levelFilter': resultsAPI.currentFiltersLevel,
					'corporation': 	resultsAPI.currectLevel1FilterCorporation,
					'management': 	resultsAPI.currectLevel2FilterManagement,
					'world': 		idWorldFilter};
		
		// Tienda-Tienda
		if(resultsAPI.currectLevel0FilterCsiType == 1 || resultsAPI.currectLevel0FilterCsiType == 3) {
			resultsAPI.limpiarTabla("table-results-detail");
			resultsAPI.invokeResultsCSI(data,				// datos para el 
					'world',            					// idFilaHeader
					$('#csi-world option:selected').text(),	// textoFilaHeader
					4, 										// indexFilaEnHeader
					'csi-section' 							// idCatalogoSiguiente 
			);
		} // Corporativo-Corporativo
		else if (resultsAPI.currectLevel0FilterCsiType == 2) {
			$("#table-general").css('display', 'none');
			$("#principal-table").css('display', 'none');
			$("#corp-areas").css('display', 'inline');
			
			resultsAPI.limpiarTabla("corp-areas-table");
			resultsAPI.invokeMatrixReportCSI(data, 'csi-section');
		} 
		// Areas Evaluadas Periodicamente
		else if (resultsAPI.currectLevel0FilterCsiType == 4) {
			$("#table-general").css('display', 'none');
			$("#principal-table").css('display', 'none');
			$("#corp-areas").css('display', 'inline');
			
			resultsAPI.limpiarTabla("corp-areas-table");
			resultsAPI.agregarEncabezados(2,'CSI', false);
			resultsAPI.agregarEncabezados(2,'Enero', false);
			resultsAPI.agregarEncabezados(2,'Febrero', false);
			resultsAPI.agregarEncabezados(2,'Marzo', false);
			resultsAPI.agregarEncabezados(2,'Abril', false);
			resultsAPI.agregarEncabezados(2,'Mayo', false);
			resultsAPI.agregarEncabezados(2,'Junio', false);
			resultsAPI.agregarEncabezados(2,'Julio', false);
			resultsAPI.agregarEncabezados(2,'Agosto', false);
			resultsAPI.agregarEncabezados(2,'Septiembre', false);
			resultsAPI.agregarEncabezados(2,'Octubre', false);
			resultsAPI.agregarEncabezados(2,'Noviembre', false);
			resultsAPI.agregarEncabezados(2,'Diciembre', true);
			
			
			resultsAPI.invokeAreaResultsCSI(data,	// Datos para Busqueda
						'area', 					// Id Fila Header
						'csi-section' 			// Id Catalog Siguiente
			);
		}
	};
	
	// Filtro Section
	this.setLevel4FilterSection = function(idSectionFilter){		
		resultsAPI.currentFiltersLevel = 5;
		resultsAPI.currectLevel4FilterSection = idSectionFilter;
		resultsAPI.borrarCsiHeader(4);
		resultsAPI.agregarCsiHeader("section-header", $('#csi-section option:selected').text(),5);
		var data = {'csiType': 		resultsAPI.currectLevel0FilterCsiType,
					'levelFilter':	resultsAPI.currentFiltersLevel,
					'corporation':	resultsAPI.currectLevel1FilterCorporation,
					'management':	resultsAPI.currectLevel2FilterManagement,
					'world':		resultsAPI.currectLevel3FilterWorld,
					'section':		idSectionFilter};
		
		// Tienda-Tienda
		if(resultsAPI.currectLevel0FilterCsiType == 1 || resultsAPI.currectLevel0FilterCsiType == 3) {
			resultsAPI.limpiarTabla("table-results-detail");
			resultsAPI.invokeResultsCSI(data,					// datos para el 
					'section',            						// idFilaHeader
					$('#csi-section option:selected').text(),	// textoFilaHeader
					5,											// indexFilaEnHeader
					'csi-area'									// idCatalogoSiguiente 
			);
		} // Corporativo-Corporativo
		else if (resultsAPI.currectLevel0FilterCsiType == 2) {
			$("#table-general").css('display', 'none');
			$("#principal-table").css('display', 'none');
			$("#corp-areas").css('display', 'inline');
			
			resultsAPI.limpiarTabla("corp-areas-table");
			resultsAPI.invokeMatrixReportCSI(data, 'csi-area');
		} 
		// Areas Evaluadas Periodicamente
		else if (resultsAPI.currectLevel0FilterCsiType == 4) {
			$("#table-general").css('display', 'none');
			$("#principal-table").css('display', 'none');
			$("#corp-areas").css('display', 'inline');
			
			resultsAPI.limpiarTabla("corp-areas-table");
			resultsAPI.agregarEncabezados(2,'CSI', false);
			resultsAPI.agregarEncabezados(2,'Enero', false);
			resultsAPI.agregarEncabezados(2,'Febrero', false);
			resultsAPI.agregarEncabezados(2,'Marzo', false);
			resultsAPI.agregarEncabezados(2,'Abril', false);
			resultsAPI.agregarEncabezados(2,'Mayo', false);
			resultsAPI.agregarEncabezados(2,'Junio', false);
			resultsAPI.agregarEncabezados(2,'Julio', false);
			resultsAPI.agregarEncabezados(2,'Agosto', false);
			resultsAPI.agregarEncabezados(2,'Septiembre', false);
			resultsAPI.agregarEncabezados(2,'Octubre', false);
			resultsAPI.agregarEncabezados(2,'Noviembre', false);
			resultsAPI.agregarEncabezados(2,'Diciembre', true);
			
			
			resultsAPI.invokeAreaResultsCSI(data,	// Datos para Busqueda
						'area', 					// Id Fila Header
						'csi-area' 			// Id Catalog Siguiente
			);
		}
	};
	
	// Filtro Area
	this.setLevel5FilterEmployee = function(idEmployeeFilter) {		
		resultsAPI.currentFiltersLevel = 6;
		resultsAPI.currectLevel5FilterEmployee = idEmployeeFilter;
		resultsAPI.borrarCsiHeader(5);
		resultsAPI.agregarCsiHeader("area-header", $('#csi-area option:selected').text(),6);
		var data = {'csiType': 		resultsAPI.currectLevel0FilterCsiType,
					'levelFilter':	resultsAPI.currentFiltersLevel,
					'corporation':	resultsAPI.currectLevel1FilterCorporation,
					'management':	resultsAPI.currectLevel2FilterManagement,
					'world':		resultsAPI.currectLevel3FilterWorld,
					'section':		resultsAPI.currectLevel4FilterSection,
					'evaluated':	resultsAPI.currectLevel5FilterEmployee};
		
		if(resultsAPI.currectLevel0FilterCsiType == 1 || resultsAPI.currectLevel0FilterCsiType == 3) {
			resultsAPI.limpiarTabla("table-results-detail");
			resultsAPI.invokeResultsCSI(data,				// datos para el 
					'section',            					// idFilaHeader
					$('#csi-area option:selected').text(),	// textoFilaHeader
					6,										// indexFilaEnHeader
					'csi-ranking'							// idCatalogoSiguiente 
			);
		}		
		else if (resultsAPI.currectLevel0FilterCsiType == 2) {
			$("#table-general").css('display', 'none');
			$("#principal-table").css('display', 'none');
			$("#corp-areas").css('display', 'inline');
			
			resultsAPI.limpiarTabla("corp-areas-table");
			resultsAPI.invokeMatrixReportCSI(data, null);
		}
		 // Areas Evaluadas Periodicamente
		else if (resultsAPI.currectLevel0FilterCsiType == 4) {
			$("#table-general").css('display', 'none');
			$("#principal-table").css('display', 'none');
			$("#corp-areas").css('display', 'inline');
	
			
			resultsAPI.limpiarTabla("corp-areas-table");
			resultsAPI.agregarEncabezados(2,'CSI', false);
			resultsAPI.agregarEncabezados(2,'Enero', false);
			resultsAPI.agregarEncabezados(2,'Febrero', false);
			resultsAPI.agregarEncabezados(2,'Marzo', false);
			resultsAPI.agregarEncabezados(2,'Abril', false);
			resultsAPI.agregarEncabezados(2,'Mayo', false);
			resultsAPI.agregarEncabezados(2,'Junio', false);
			resultsAPI.agregarEncabezados(2,'Julio', false);
			resultsAPI.agregarEncabezados(2,'Agosto', false);
			resultsAPI.agregarEncabezados(2,'Septiembre', false);
			resultsAPI.agregarEncabezados(2,'Octubre', false);
			resultsAPI.agregarEncabezados(2,'Noviembre', false);
			resultsAPI.agregarEncabezados(2,'Diciembre', true);
			
			
			resultsAPI.invokeAreaResultsCSI(data,	// Datos para Busqueda
						'area', 					// Id Fila Header
						null 			// Id Catalog Siguiente
			);
		}
	};
	
	this.setLevel6FilterRanking = function(idRankingFilter) {		
		resultsAPI.currentFiltersLevel = 7;
		resultsAPI.currectLevel6FilterRanking = idRankingFilter;
		resultsAPI.borrarCsiHeader(6);
		resultsAPI.agregarCsiHeader("ranking-header", $('#csi-ranking option:selected').text(),7);
		var data = {'csiType': 		resultsAPI.currectLevel0FilterCsiType,
					'levelFilter':	resultsAPI.currentFiltersLevel,
					'corporation': 	resultsAPI.currectLevel1FilterCorporation,
					'management': 	resultsAPI.currectLevel2FilterManagement,
					'world': 		resultsAPI.currectLevel3FilterWorld,
					'section': 		resultsAPI.currectLevel4FilterSection,
					'evaluated': 	resultsAPI.currectLevel5FilterEmployee,
					'ranking':		idRankingFilter};
		
		// Tienda - Tienda
		if(resultsAPI.currectLevel0FilterCsiType == 1) {
			resultsAPI.limpiarTabla("table-results-detail");
			resultsAPI.invokeResultsCSI(data,					// datos para el 
					'ranking',            						// idFilaHeader
					$('#csi-ranking option:selected').text(),	// textoFilaHeader
					7, 											// indexFilaEnHeader
					null 										// idCatalogoSiguiente 
			);
		} // Corporativo-Corporativo
		else if (resultsAPI.currectLevel0FilterCsiType == 2) {
			$("#table-general").css('display', 'none');
			$("#principal-table").css('display', 'none');
			$("#corp-areas").css('display', 'inline');
			
			resultsAPI.limpiarTabla("corp-areas-table");
			resultsAPI.invokeMatrixReportCSI(data, 'none');
		} // Tienda-Corporativo
		else if (resultsAPI.currectLevel0FilterCsiType == 3) {
			
		} // Areas Evaluadas Periodicamente
		else if (resultsAPI.currectLevel0FilterCsiType == 4) {
			
		}
	};
};

var resultsAPI = new ResultsAPI();


