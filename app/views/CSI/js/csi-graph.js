var circleGraph;
var linesGraph;



function CsiGraphAPI(){
	
	
	
	
	this.drawCSIResumeFromResponse = function(resume){
		var generalNPS = resume['general'];
		var plan = resume['plan'];
		var promotores = resume['promotores'];
		var neutros = resume['neutros'];
		var destractores = resume['detractores'];
		
		graphApi.drawCSIResume(generalNPS, plan, promotores, neutros, destractores);
			
	};
	this.drawCSIResume = function(generalNPS, plan, promotores, neutros, destractores){
//		var plan = Math.floor(Math.random() * 100);
//		var generalNPS = 85.5;

		circleGraph = Highcharts.chart('circle-graph', {
	        chart: {
	            type: 'solidgauge',
	            marginTop: 50,
	            marginLeft: 25,
	            height: 330
	        },
	        title: {
	            text: 'CSI',
	            x: 10,
	            style: {
	                fontSize: '18px'
	            }
	        },
	        tooltip: {
			    formatter: function() {
			        return "PLAN " + plan;
			    },
			    positioner: function() {
			    	return {
			    		x: 100,
			    		y: 210
			    	}
			    }
			},
	        pane: {
	        	center: ['52%', '45%'],
	            startAngle: 0,
	            endAngle: 360,
	            y: 0,
	            background: [{ // Track for Move
	                outerRadius: '75%',
	                innerRadius: '60%',
	                backgroundColor: "#DDDADB",
	                borderWidth: 0
	            }]
	        },
	        yAxis: {
	            min: 0,
	            max: 100,
	            lineWidth: 0,
	            tickPositions: []
	        },
	        plotOptions: {
	            solidgauge: {
	                outerRadius: '75%',
	                innerRadius: '60%',
	                dataLabels: {
	                    enabled: false
	                },
	                linecap: 'square',
	                stickyTracking: false
	            }
	        },
	        series: [{
	            name: 'NPS',
	            borderColor: "#4D9311",
	            data: [{
	                color: "#4D9311",
	                radius: '75%',
	                y: generalNPS
	            }]
//	        },{
//	            name: 'PLAN',
//	            borderColor: "#337AB7",
//	            data: [{
//	                color: "#337AB7",
//	                radius: '75%',
//	                y: plan
//        	}]
//	        },{
//	            name: 'destractores',
//	            borderColor: "#337AB7",
//	            data: [{
//	                color: "#D36900",
//	                radius: '45%',
//	                y: promotores + neutros + destractores
//    			}]
//	        },{
//	            name: 'neutros',
//	            borderColor: "#337AB7",
//	            data: [{
//	                color: "#EBBD3F",
//	                radius: '45%',
//	                y: promotores + neutros 
//	           
//    			}]
//	        },{
//	            name: 'promotores',
//	            borderColor: "#337AB7",
//	            data: [{
//	                color: "#4D9311	",
//	                radius: '45%',
//	                y: promotores
//	            
//    			}]
	        }]
	    },
	    function(circleGraph) {
	    	//circleGraph.renderer.label("sdfhsadkkkkkkk").add();
	        
			var table = circleGraph.renderer.html(
				"<table class='resume-circle-table'>" +
					"<tbody>" +
						"<tr>" +
							"<td class='quant'>" +
							promotores+
							"</td>" +
							"<td class='quant'>" +
							 neutros+
							"</td>" +
							"<td class='quant'>" +
							destractores+
							"</td>" +
						"</tr>" +
						"<tr>" +
							"<td class='cell-green'>" +
							"</td>" +
							"<td class='cell-yellow'>" +
							"</td>" +
							"<td class='cell-orange'>" +
							"</td>" +
						"</tr>" +
						"<tr>" +
							"<td class='text-label'>" +
								"Promotores" +
							"</td>" +
							"<td class='text-label'>" +
								"Neutros" +
							"</td>" +
							"<td class='text-label'>" +
								"Detractores" +
							"</td>" +
						"</tr>" +
					"</tbody>" +
				"</table>"
			).add();
	        
			var tablePercent = circleGraph.renderer.html(
				"<table class='percent-circle-table'>" +
					"<tbody>" +
						"<tr>" +
							"<td style='color: #337AB7;'>" +
								generalNPS+"<span class='span-percent-circle'>%</span>" +
							"</td>" +
						"</tr>" +
					"</tbody>" +
				"</table>"
			).add();
	        
			table.align(Highcharts.extend(table.getBBox(), {
	            align: 'center',
	            x: 13, // offset
	            verticalAlign: 'bottom',
	            y: 0 // offset
	        }), null, 'spacingBox');
	        
	        tablePercent.align(Highcharts.extend(tablePercent.getBBox(), {
	            align: 'center',
	            x: 16, // offset
	            verticalAlign: 'middle',
	            y: 16 // offset
	        }), null, 'spacingBox');
	    });
		
	};
	

	this.drawDotsGraphFromResponse = function(seriesResponse, categories){
		
		
		
		var colors =  seriesResponse['colors'];
		var series =  seriesResponse['series'];
	    if(categories.length<1){
	    	categories.push('Ene');
	    	categories.push('Feb');
	    	categories.push('Mar');
	    	categories.push('Abr');
	    	categories.push('May');
	    	categories.push('Jun');
	    	categories.push('Jul');
	    	categories.push('Ago');
	    	categories.push('Sep');
	    	categories.push('Oct');
	    	categories.push('Nov');
	    	categories.push('Dic');
	    }
		graphApi.drawDotsGraph(series,colors, categories);
	    
	};
	
	
	this.drawDotsGraph = function(series, colors, categories){


	    linesGraph = Highcharts.chart('lines-graph', {
	        chart: {
	            type: 'column',
	           	height: 430
	        },
	        colors: colors,
	        title: {
	            text: 'Histórico CSI'
	        },
	        xAxis: {
	            categories: categories,
	            color: "#337AB7"
	        },
	        yAxis: {
	            title: {
	                text: '',
	                style: {
		            	"font-size": "20px"
		            }
	            },
	            max: 100,
	            min: 70//lowest
	        },
	        plotOptions: {
	            line: {
	                dataLabels: {
	                    enabled: true
	                },
	                enableMouseTracking: true
	            }
	        },
	        tooltip: {
	        	enabled: true,
		    	formatter: function() {
			        return "<div style='color: #008800;'>" + this.y + "</div> "
			    }
			},
	        series: series
	    });
		
	};
	
	// ---------------------------------------------
	// TODO implementar ls llamadas conservando los filtos y así 
	this.invokeGraphResults = function (){
		$.ajax({    	
	        url: '@{csi.ApplicationCSI.getGraphJSON}',
	        type: 'POST',//'POST',	        
	        cache: false,	  
	        success: function (response) {	        		
	        	var resume = response['resume'];
	        	graphApi.drawCSIResumeFromResponse(resume);   		
	        	var series = response['series'];
	        	graphApi.drawDotsGraphFromResponse(series, response['catalogos']);
	        },
	        beforeSend: function () {
	        	Liverpool.loading("show");
	        },
	        complete: function () {
	        	Liverpool.loading("hide");
	        	//$('#menu-filter-date-lst').css('display', 'none');
	        },
	        error: function () {
	            Liverpool.setMessage($('.inner-message'), 'No se pudo obtener la información para grficar .', 'warning');
	        }
	    });
	};
	
	
	
};

$(document).ready(function() {
    //Circle graph:
	graphApi.invokeGraphResults({});
  
});


var graphApi = new CsiGraphAPI();