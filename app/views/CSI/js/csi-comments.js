var globalVariable;
var typingTimer;
var doneTypingInterval = 1000;
var search;
var idQuestion;
var id;

function see(value) {
	globalVariable = value;
	Liverpool.loading("show");
	$.get('@{csi.CommentsController.comments()}', {
		id : value
	}, function(data) {
		$('#mdl-comments').empty().html(data).modal('show');
		var type = "";
		var corpAddress = "";
		var address = "";
		var world = "";
		var section = "";
		var employee = "";
		if(resultsAPI.currectLevel0FilterCsiType != 0){
			type = $('#csi-type option:selected').text()
			$("#title-comments").html("" + type);
			if(resultsAPI.currectLevel1FilterCorporation != 0){
				corpAddress = $('#csi-corp-address option:selected').text();
				$("#title-comments").html(" " + type + " / " + corpAddress);
				if(resultsAPI.currectLevel2FilterManagement != 0){
					address = $('#csi-address option:selected').text();
					$("#title-comments").html(" " + type + " / " + corpAddress + " / " + address);
					if(resultsAPI.currectLevel3FilterWorld != 0){
						world = $('#csi-world option:selected').text();
						$("#title-comments").html(" " + type + " / " + corpAddress + " / " + address + " / " + world);
						if(resultsAPI.currectLevel4FilterSection != 0){
							section = $('#csi-section option:selected').text();
							$("#title-comments").html(" " + type + " / " + corpAddress + " / " + address + " / " + world + " / " + section);
							if(resultsAPI.currectLevel5FilterEmployee != 0){
								employee = $('#csi-area option:selected').text();
								$("#title-comments").html(" " + type + " / " + corpAddress + " / " + address + " / " + world + " / " + section + " / " + employee);
							}
						}
					}
				}
			}
		}
		$('#myModal').is(':visible');
		Liverpool.loading("hide");
	});
}

function verComentarios(value){
	globalVariable = value;
	Liverpool.loading("show");
	$.get('@{csi.CommentsController.comments()}', {
		id : value
	}, function(data) {
		$('#mdl-comments').empty().html(data).modal('show');
		$("#title-comments").html("" + csiApi.catalogDict[value]);
		$('#myModal').is(':visible');
		Liverpool.loading("hide");
	});
}

function printComment() {

	var divToPrint = document.getElementById("comments-table");
	newWin = window.open("");
	newWin.document.write(divToPrint.outerHTML);
	newWin.print();
	newWin.close();

}

function dataTable(datos) {	
	var tableExam = $('#comments-table')
			.DataTable(
					{
						data : datos,
						"bDestroy" : true,
						"bSort" : false,
						"oLanguage" : {
							"sZeroRecords" : "Ningún Comentario",
							"sInfo" : "Mostrando comentarios de _START_ al _END_ de _TOTAL_ totales",
							"sInfoEmpty" : "0 Comentarios",
							"sPaginatePrevious" : "Previous page",
							"sProcessing" : "Cargando...",
							"sSearch" : "Buscar",
							"sLengthMenu" : "",
							"oPaginate" : {
								"sFirst" : "<<",
								"sLast" : ">>",
								"sNext" : ">",
								"sPrevious" : "<"
							}
						}
					});	
	$("#comments-table_paginate").css("padding", 0);
	$("#comments-table_info").css("font-size", 12);
	$("#comments-table_filter").css("font-size", 12);
	$("#comments-table_length").css("font-size", 12);
	$("#comments-table_length").css("padding-left","15px");
	$("#comments-table_length select").css({
		paddingTop : "0",
		paddingBottom : "0",
		height : "32px",
		fontSize : "12px"
	});
}

$(document).ready(function() {	
	$("#comment-type").on('change',function(e) {
		
		if($("#comment-type").val() == 3){//Detractores
			$("#comment-type-message").html("<label class='normal-lbl'>Muestra todos los comentarios de preguntas calificadas entre 0 y 6</label>");
		}else if($("#comment-type").val() == 4){//Promotores
			$("#comment-type-message").html("<label class='normal-lbl'>Muestra todos los comentarios de preguntas calificadas entre 9 y 10</label>");
		}else if($("#comment-type").val() == 5){//Pasivos
			$("#comment-type-message").html("<label class='normal-lbl'>Muestra todos los comentarios de preguntas calificadas entre 7 y 8</label>");
		}else{
			$("#comment-type-message").html('');
		}
		$("#comments-table").dataTable().fnClearTable();	
		$("#graficaPorClave").hide();
		id = this.value;
		if (id == 2) {
			$("#search").hide();
			$("#comments-table").show();
			$("#comments-table_paginate").css("padding", 0);
			$("#comments-table_info").css("font-size", 12);
			$("#comments-table_length").css("font-size", 12);
			$("#comments-table_length select").css({
				paddingTop : "0",
				paddingBottom : "0",
				height : "32px",
				fontSize : "12px"
			});			
			$("#comments-table_filter").css("font-size", 12);
			$.ajax({
				url : "@{csi.CommentsController.getQuestions()}",
				type : 'POST',
				data : {id : globalVariable},
				success : function(data) {
					$("#questions-select").html('<option value="">Seleccione una pregunta</option>');
					var questions = data['questions'];
					
					for (var i = 0; i < questions.length; i++) {
						$("#questions-select").append("<option value= " + questions[i]['id'] + ">" + questions[i]['description'] + "</option>");
					}

					$("#questions-select").show();
				},
				beforeSend : function() {
					Liverpool.loading("show");
				},
				complete : function() {
					Liverpool.loading("hide");
				}
			});
			
			
			} else if (id == 6) {			
				Liverpool.loading("show");
				$("#questions-select").hide();
				$("#comments-table").hide();
				$("#comments-table_filter").hide();
				$("#comments-table_paginate").hide();
				$("#comments-table_info").hide();
				$("#comments-table_length").hide();
				$("#comments-table_length select").hide();
				$("#search").show();
				$.get('@{csi.CommentsController.getAllPorClave()}',{id : globalVariable},function(data) {
					var size = data.length;
					for(var j=0;j<size;j++){data[j] = data[j].toUpperCase();}
					Liverpool.loading("hide");
					$('#search').keyup(function() {
						clearTimeout(typingTimer);
						if ($('#search').val) {
							typingTimer = setTimeout(function() {
								search = $("#search").val().toUpperCase();								
								var found = jQuery.grep(data, function(value, i) {
									return value.indexOf(search) != -1
								}).length;
								if (found > 0 && search.length > 0) {
									generaGrafica(size, search, found);
								}else {
									generaGrafica(size, search, 0);
								}
							},
							doneTypingInterval);
						}
					});
				});
								
			}else {
				
				$("#questions-select").hide();
				$("#search").hide();
				$("#comments-table").show();
				
				$.ajax({
					url : "@{csi.CommentsController.getComments()}",
					type : 'POST',
					data : {id : id, csiId : globalVariable},success : function(data) {
						dataTable(data);
					},
					beforeSend : function() {
						Liverpool.loading("show");
					},
					complete : function() {
						Liverpool.loading("hide");
					}
				});
			}		
		});
	
	$("#questions-select").on('change',function(e) {
		$("#comments-table").dataTable().fnClearTable();			
			idQuestion = this.value;
			$.ajax({
				url : "@{csi.CommentsController.getCommetsQuestions()}",
				type : 'POST',
				data : {id : idQuestion, csiId : globalVariable}, 
				success : function(data) {
					dataTable(data);
				},
				beforeSend : function() {
					Liverpool.loading("show");
				},
				complete : function() {
					Liverpool.loading("hide");
				}
			}); 
			dataTable();
			
		});
		dataTable();		
});

$('#download-comments').click(function() {
	if(id == 2 && idQuestion != null){
		location.href = '@{csi.CommentsController.downloadQuestions()}?id=' + idQuestion + "&csiId=" + globalVariable;
	}else{
		if(id == 6 && search != null){
			location.href = '@{csi.CommentsController.downloadWord()}?word=' + search + "&csiId=" + globalVariable;
		}else{
			location.href = '@{csi.CommentsController.download()}?id=' + id + "&csiId=" + globalVariable;	
		}	
	}
}); 

function generaGrafica(total, palabraClave, coincidencias) {
	var porcentaje = (coincidencias * 100) / total;
	palabraClave = palabraClave.toUpperCase();
	$("#graficaPorClave").show();
	//GRAFICA PARA FILTRO POR PALABRA CLAVE
	Highcharts
			.chart(
					'graficaPorClave',
					{
						chart : {
							type : 'bar'
						},
						title : {
							text : 'Porcentaje de respuestas por palabra'
						},
						subtitle : {
							text : null
						},
						xAxis : {
							categories : [ '' ],
							title : {
								text : palabraClave
							}
						},
						yAxis : {
							min : 0,
							title : {
								text : '%',
								align : 'high'
							},
							labels : {
								overflow : 'justify'
							}
						},
						tooltip : {
							headerFormat : '<span style="font-size:11px">{series.name}</span><br>',
							pointFormat : '<span style="color:{point.color}">{point.name}</span>:<b>{point.y:.2f} %</b><br/>'
						},
						plotOptions : {
							bar : {
								dataLabels : {
									enabled : false
								}
							}
						},
						legend : {
							layout : 'vertical',
							align : 'right',
							verticalAlign : 'top',
							x : -40,
							y : 80,
							floating : true,
							borderWidth : 1,
							backgroundColor : ((Highcharts.theme && Highcharts.theme.legendBackgroundColor) || '#FFFFFF'),
							shadow : true,
							enabled : false
						},
						credits : {
							enabled : false
						},
						series : [ {
							name : palabraClave,
							data : [ porcentaje ]
						} ]
					});
}
