
// SE crea un objeto para manejar los filtros del CSI
function FiltersAPI(){

		this.debug = true;
		this.currentQuarter = "${session.get(session.id + '-filter-date-quarter-quarter')?:'Q1'}";

		this.isDateFilter = "${session.get(session.id + '-filter-date-type')?:'no'}";
		this.isSurveyFilter = "${session.get(session.id + '-filter-encuesta')?:'no'}";
		this.isCsiFilter = "${session.get(session.id+'-filter-csi-type')?:'no'}";
		this.isCorporationFilter = "${session.get(session.id+'-filter-corporation')?:'no'}";
		this.isManagemetFilter = "${session.get(session.id+'-filter-management')?:'no'}";
		this.isWorldFilter = "${session.get(session.id+'-filter-world')?:'no'}";
		this.isSectionFilter = "${session.get(session.id+'-filter-section')?:'no'}";
		this.isEvaluatedFilter = "${session.get(session.id+'-filter-evaluated')?:'no'}";

		this.changeQuarter = function (q){
			filtersAPI.currentQuarter = q;
		};
		this.getCurrentQuarter = function (){
			if (filtersAPI.currentQuarter == null || filtersAPI.currentQuarter == undefined || filtersAPI.currentQuarter === "" || filtersAPI.currentQuarter === "null" || filtersAPI.currentQuarter === "undefined"){
				filtersAPI.currentQuarter = "Q1";
			}
			return filtersAPI.currentQuarter;
		};


		this.stringIsEmpty = function(s){
			return s == undefined || s === ""|| s === "-";
		}

		this.cleanySurveyFilter = function(){
			$("#menu-filter-survey-name-lst").val("");
			filtersAPI.applyFilterEncuestaRequest( '@{Filters.cleanySurveyFilter}', 'Limpiar Filtro Encuestas');

		}
		
		this.cleanCSITypeFilter = function(){
			filtersAPI.applyFilterCSITypeRequest('@{Filters.cleanCSITypeFilter}');
		}
		
		this.applyCSITypeFilter = function(id){
			filtersAPI.applyFilterCSITypeRequest('@{Filters.applyCSITypeFilter}?id='+id);
		}
		
		this.cleanCorporationFilter = function(){
			filtersAPI.applyFilterCSITypeRequest('@{Filters.cleanCorporationFilter}');
		}
		
		this.applyCorporationFilter = function(id){
			filtersAPI.applyFilterCSITypeRequest('@{Filters.applyCorporationFilter}?id='+id);
		}
		
		this.cleanManagementFilter = function(){
			filtersAPI.applyFilterCSITypeRequest('@{Filters.cleanManagementFilter}');
		}
		
		this.applyManagementFilter = function(id){
			filtersAPI.applyFilterCSITypeRequest('@{Filters.applyManagementFilter}?id='+id);
		}
		
		this.cleanWorldFilter = function(){
			filtersAPI.applyFilterCSITypeRequest('@{Filters.cleanWorldFilter}');
		}
		
		this.applyWorldFilter = function(id){
			filtersAPI.applyFilterCSITypeRequest('@{Filters.applyWorldFilter}?id='+id);
		}
		
		this.cleanSectionFilter = function(){
			filtersAPI.applyFilterCSITypeRequest('@{Filters.cleanSectionFilter}');
		}
		
		this.applySectionFilter = function(id){
			filtersAPI.applyFilterCSITypeRequest('@{Filters.applySectionFilter}?id='+id);
		}
		
		this.cleanEvaluatedFilter = function(){
			filtersAPI.applyFilterCSITypeRequest('@{Filters.cleanEvaluatedFilter}');
		}
		
		this.applyEvaluatedFilter = function(id){
			filtersAPI.applyFilterCSITypeRequest('@{Filters.applyEvaluatedFilter}?id='+id);
		}
		
		this.applyFilterEncuesta = function (id){
			$('#menu-filter-dates-lst').css('display', 'inline');
			$("#date-year").val("");
			$("#date-and-month").val("");
			$("#date-from").val("");
			$("#date-to").val("");
			filtersAPI.currentQuarter = "Q1";
			$('#clean-filter-dates').attr('value', "Limpiar Filtro Fecha" );
			$('#clean-filter-dates').css('display', 'none');
			filtersAPI.applyFilterEncuestaRequest( '@{Filters.applySurveyFilter}?id='+id, 'X ('+id+')');
		}
		
		this.applyFilterCSITypeRequest = function (url){
			 $.ajax({
		        url: url,
		        type: 'GET',//'POST',
		        contentType: false,
		        cache: false,
		        processData: false,
		        success: function (response) {
		        	//Aplica filtro
		        	graphApi.invokeGraphResults({});
		        }
		    });
		};
		
		this.applyFilterEncuestaRequest = function (url,texto){
			 $.ajax({
		        url: url,
		        type: 'GET',//'POST',
		        contentType: false,
		        cache: false,
		        processData: false,
		        success: function (response) {
		        	// Se obtiene la respuesta
			        try {
		        		actualizarFiltersCSI();
		        	}
		        	catch (e){
		        		if(filtersAPI.debug){
		        			throw e;
		        		}
		        	}
		        	finally{
			        	if (url === '@{Filters.cleanySurveyFilter}'){
		        			$('#menu-filter-survey-name-lst').css('display', 'inline');
//		        			$('#menu-filter-survey-name-lst').val("");
		        			$('#clean-filter-surveys').attr('value', texto );
		        			$('#clean-filter-surveys').css('display', 'none');
		        			$("#menu-date-filter").show();
		        		}
		        		else {
		        			$("#menu-date-filter").hide();
		        			$('#menu-filter-survey-name-lst').css('display', 'none');
		        			$('#clean-filter-surveys').attr('value', texto );
		        			$('#clean-filter-surveys').css('display', 'inline');
		        		}
			        }
		        },
		        beforeSend: function () {
		        	Liverpool.loading("show");
		        },
		        complete: function () {
		        	Liverpool.loading("hide");
		        	if (url === '@{Filters.cleanDateFilter}'){
		        		$('#menu-filter-survey-lst').css('display', 'inline');
		        	}else{
		        		$('#menu-filter-survey-lst').css('display', 'none');
		        	}
		        },
		        error: function () {
		            Liverpool.setMessage($('.inner-message'), 'No se pudo aplicar el filtro. Por favor, intente nuevamente.', 'warning');
		        }
		    });
		};

		this.applyDateFilter = function (url, texto){
			$.ajax({
		        url: url,
		        type: 'GET',//'POST',
		        contentType: false,
		        cache: false,
		        processData: false,
		        success: function (response) {
		        	// Se obtiene la respuesta
		        	try {
		        		// Si se quiere usar este filtro se deben de leer de la session los datos que se tiene  guardados y
		        		// se debe declarar esta funcion en la pagina donde se requiera usar
		        		updateInformationWithDateFilters();

		        	}
		        	catch (e){
		        		if(filtersAPI.debug){
		        			throw e;
		        		}
		        	//  en caso que no se tenga la funcion
		        	}
		        	finally{
			        	if (url === '@{Filters.cleanDateFilter}'){
		        			$('#menu-filter-dates-lst').css('display', 'inline');
		        			$("#date-year").val("");
		        			$("#date-and-month").val("");
		        			$("#date-from").val("");
		        			$("#date-to").val("");
		        			filtersAPI.currentQuarter = "Q1";
		        			$('#clean-filter-dates').attr('value', texto );
		        			$('#clean-filter-dates').css('display', 'none');
		        			$("#btn-surveys-name").show();
		        		}
		        		else {
		        			$("#btn-surveys-name").hide();
		        			$('#menu-filter-dates-lst').css('display', 'none');
		        			$('#clean-filter-dates').attr('value', texto );
		        			$('#clean-filter-dates').css('display', 'inline');
		        		}
		        	}
		        },
		        beforeSend: function () {
		        	Liverpool.loading("show");
		        },
		        complete: function () {
		        	Liverpool.loading("hide");
		        	if (url === '@{Filters.cleanDateFilter}'){
		        		$('#menu-filter-date-lst').css('display', 'inline');
		        	}else{
		        		$('#menu-filter-date-lst').css('display', 'none');
		        	}
		        },
		        error: function () {
		            Liverpool.setMessage($('.inner-message'), 'No se pudo aplicar el filtro. Por favor, intente nuevamente.', 'warning');
		        }
		    });
		};

		this.applyDateQuarterFilter = function (year, quarter){
			$('#menu-filter-survey-name-lst').css('display', 'inline');
			$('#clean-filter-surveys').attr('value', 'Limpiar Filtro Encuestas' );
			$('#clean-filter-surveys').css('display', 'none');
			filtersAPI.applyDateFilter('@{Filters.applyDateQuarterFilter}?year='+year+'&quarter='+quarter,'T:Y='+year+',Q='+quarter);
		};
		this.applyDateMonthFilter = function (month){
			$('#menu-filter-survey-name-lst').css('display', 'inline');
			$('#clean-filter-surveys').attr('value', 'Limpiar Filtro Encuestas' );
			$('#clean-filter-surveys').css('display', 'none');
			filtersAPI.applyDateFilter('@{Filters.applyDateMonthFilter}?month='+month,'M:M='+month);
		};
		this.applyDatePeriodFilter = function ( dateFrom, dateTo){
			$('#menu-filter-survey-name-lst').css('display', 'inline');
			$('#clean-filter-surveys').attr('value', 'Limpiar Filtro Encuestas' );
			$('#clean-filter-surveys').css('display', 'none');
			filtersAPI.applyDateFilter('@{Filters.applyDatePeriodFilter}?dateFrom='+dateFrom+'&dateTo=' + dateTo,'P:F='+dateFrom +',T='+dateTo);
		};
		this.cleanDateFilter = function(){
			filtersAPI.applyDateFilter('@{Filters.cleanDateFilter}',"Limpiar Filtro Fecha");
		};
	};

var filtersAPI = new FiltersAPI();


function getRandomQual() {
	var qual = Math.random() * 35 + 65;
	var cls = "detractor";

	if(qual >= 70) {
		cls = "neutral";

		if(qual >= 80) {
			cls = "promoter";
		}
	}

	return "<div class='label-qual " + cls + "'>" +
		"<div>" + qual.toFixed(2) + "</div>" +
	"</div>";
}

// Inicializa todo cuando la pagina esta cargada
$(document).ready(function() {
	//Modify behavior of NPS filters to fit CSI:
	$("#nav-environment").hide();
	$("#nav-csi").fadeIn(200);
	$("#side-bar-nps").fadeIn(200);
	$("#btn-section").hide();

	$(".dropdown-zone .w-500").css("width", "490px");
	$(".dropdown-zone #row-zones div.col-xs-3").css("margin-bottom", "3px");
	$("#btn-surveys-name").show();
	$("#btn-surveys-name .menu-side-nps li:first-child").css("margin", 0);
	$("#btn-zone-bar").hide();


	// se agregan los eventos cuando se da click en el boton aplicar
	$("#apply-date").click(function(e) {
		// e detine la proagación del evento

		e.stopImmediatePropagation();
		//http://fantom.org/doc/fwt/Tab
		// Se obtiene el objeto del tab activo
		var active = $('.fwt-tab-active-up');
		var selected = undefined;
		if (active.length > 0) selected = active[0].textContent;

		if (selected === "Trimestral"){
			// Si es Trimestral se invoca un metodo
			var quarter = filtersAPI.getCurrentQuarter();

			// SE validan que las variables no esten vacias
			if (filtersAPI.stringIsEmpty(quarter)){
				return false;
			}
			var year = $('#date-year').val();
			if (filtersAPI.stringIsEmpty(year)){
				return false;
			}
			//se aplica el filtro con el controlador
			filtersAPI.applyDateQuarterFilter(year,quarter);
		}
		else if (selected === "Mensual"){
			// Si es Mensual se invoca un metodo
			var month = $("#date-and-month").val();
			// SE validan que las variables no esten vacias
			if (filtersAPI.stringIsEmpty(month)){
				return false;
			}
			//se aplica el filtro con el controlador
			filtersAPI.applyDateMonthFilter(month);
		}
		else if (selected === "Periodo"){
			// Si es Periodo se invoca un metodo
			// SE validan que las variables no esten vacias
			var dateFrom = $("#date-from").val();
			if (filtersAPI.stringIsEmpty(dateFrom)){
				return false;
			}
			var dateTo = $("#date-to").val();
			if (filtersAPI.stringIsEmpty(dateTo)){
				return false;
			}
			//se aplica el filtro con el controlador
			filtersAPI.applyDatePeriodFilter(dateFrom,dateTo);
		}
		else{
			Liverpool.setMessage($('.inner-message'), 'No se pudo seleccionar el tipo de periodo. Por favor, intente nuevamente.', 'warning');
		}

	    $('.dropdown-date').removeClass('open');
	});



	//$('#menu-filter-survey-name-lst').on('click', function(e){
		// si se da click en el boton se esconde o se muestra el filtro de encuestas

		//$("#menu-filter-survey-name-lst").toggle();
		//e.stopImmediatePropagation();

	    // SE hace la solicitud para que se muestra la lista de encuestas
//	     $.ajax({
//	        url: '@{csi.SurveysCSI.surveylist}',
//	        type: 'GET',//'POST',
//	        contentType: false,
//	        cache: false,
//	        processData: false,
//	        success: function (response) {
//	        	$('#menu-filter-survey-name-lst').html(response);
//	        },
//	        beforeSend: function () {
//	        	Liverpool.loading("show");
//	            //$('#menu-filter-survey-name-btn', $(this)).attr('disabled', 'disabled');
//	        },
//	        complete: function () {
//	        	Liverpool.loading("hide");
//	            //$('#menu-filter-survey-name-btn', $(this)).removeAttr('disabled');
//	        },
//	        error: function () {
//	        	Liverpool.setMessage($('.inner-message'), 'La encuestas no pudieron ser cargadas. Por favor, intente nuevamente.', 'warning');
//	        }
//	    });

	     $('#menu-filter-survey-name-lst').change(function(e) {
		 	var idSurveyLst = $(this).val();
		 	if(idSurveyLst == undefined ||idSurveyLst == null || idSurveyLst == ""){}else{
											 		filtersAPI
													.applyFilterEncuesta(idSurveyLst);
		 	}
		 	e.stopImmediatePropagation();
		 });
	//});


	// si se da click en el boton se esconde o se muestra el filtro de fechas
//	$('#menu-filter-dates-btn').on('click', function(e){
//		$('#menu-filter-date-lst').toggle();
//		e.stopImmediatePropagation();
//	});

	//En caso que el filtro de fechas se haya seleccionado, se debe mostrar el boton de limpiar
	 if (filtersAPI.isDateFilter === 'no'){
		$('#menu-filter-date-lst').css('display', 'inline');
		$('#clean-filter-dates').css('display', 'none');
	} else {
		$('#menu-filter-date-lst').css('display', 'none');
		$('#clean-filter-dates').css('display', 'inline');
	}

	 // En caso que el filtro de encuestas se haya seleccionado, se debe mostrar el boton de limpiar
	 if (filtersAPI.isSurveyFilter === 'no'){
		$('#menu-filter-survey-name-lst').css('display', 'inline');
		$('#clean-filter-surveys').css('display', 'none');


	}
	else {


		$('#menu-filter-survey-name-lst').css('display', 'none');
		$('#menu-filter-survey-name-btn').css('display', 'none');
		$('#clean-filter-surveys').css('display', 'inline');
	}
 });
