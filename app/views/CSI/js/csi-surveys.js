var idSurvey = ''
var oTable = null;
var CsiSurveys = function() {

    var structure  = null;
    var deselected = [];
    
	var bindEvents = function() {		
        $(function () {
        	$('#add-csi-survey-btn').click(function () {
                $.get('@{csi.SurveysCSI.addSurvey()}', function (data) {
                    showForm(data);
                });
            });
        	        
        });               
	};
    


    var showForm = function (data) {
        $('#add-csi-survey-form').trigger('reset');
        $('#mdl-add-edit').empty().html(data).modal('show');

        $(".date-input").datepicker({
            format: "dd/mm/yyyy",
            language: 'es',
            todayHighlight: true,
            autoclose: true
        });
        

        validate();
    };

    var validate = function() {
        $('#add-csi-survey-form').validate({
            'ignore': '',
            'rules': {
                'month': {
                  required: true,
                  min: 1
                },
                'year': {
                    required: true,
                    min: 1
                },
                'goal': {
                	required: true,
                	number: true
                },
                'surveyType': {
                    required: true,
                    min: 1
                } 
              },
            'messages': {
                'nameSurvey': {
                    'required': 'Por favor, ingrese el nombre de la encuesta'
                },
                'validityFrom': {
                    'required': 'Por favor, ingrese la fecha inicial'
                },
                'validityTo': {
                    'required': 'Por favor, ingrese la fecha final'
                },
                'month': {
                    'required': 'Por favor, seleccione el mes',
                    'min': 'Por favor, seleccione el mes'
                    
                },
                'year': {
                    'required': 'Por favor, seleccione el año', 
                	'min': 'Por favor, seleccione el año'
                },
                'goal': {
                    'required': 'Por favor, ingrese la meta',
                    'number': 'Por favor, solo ingrese números'
                },
                'surveyType': {
                    'required': 'Por favor, seleccione un tipo de encuesta',
                    'min': 'Por favor, seleccione un tipo de encuesta'
                },
                'area': {
                    'required': 'Por favor, ingrese un área'                    
                }
            },
            errorPlacement: function(error, element) {
                if (element.next().is('.input-group-addon')) {
                    error.insertAfter(element.next().parent());
                } else {
                    error.insertAfter(element);
                }
            },
            submitHandler: function (form) {
                save(form);
            }
        });
    };
    
    
    var save = function(form) {
    	var data = new FormData(this);    	
        $.ajax({
        	
            url: '@{csi.SurveysCSI.add()}',
            data: data,
            type: 'post',
            contentType: false,
            
            success: function (response) {
                if (response.error) {
                    Liverpool.setMessage($('.inner-message'), response.message, 'danger');
                } else {                    
                    //oTable.api().ajax.reload();
                    $('#mdl-add-edit').modal('hide');                                            
                }
            },
            beforeSend: function () {
                $('button', $(form)).attr('disabled', 'disabled');
            },
            complete: function () {
                $('button', $(form)).removeAttr('disabled');
            },
            error: function () {
                Liverpool.setMessage($('.inner-message'), 'La encuesta no pudo ser guardada. Por favor, intente nuevamente.', 'warning');
            }
        });
    };
    
   
    
    var init = function() {
        bindEvents();               
    };

    init();
}();


function loadEditModal(id) {
	// TODO hacer un archivo de urls 
	$.get('@{csi.SurveysCSI.editSurvey()}', function (data) {
		
		idSurvey = id;
		$('#mdl-edit').empty().html(data).modal('show');
		
    });
};

function loadUploadModal(id) {
	$.get('@{csi.SurveysCSI.uploadResults()}', function (data) {
		idSurvey = id;
		$('#mdl-upload').empty().html(data).modal('show');
	 });

};

function loadDeleteModal(id) {
	invokeRefuse();
//	$.get('@{csi.SurveysCSI.deleteSurvey()}?', function (data) {
//		idSurvey = id;
//		alert(idSurvey);
//		$('#mdl-delete').empty().html(data).modal('show');
//		
//    });
};

this.invokeRefuse = function (id) {
    swal({
        title: "",
        text: "¿Esta seguro de eliminar esta encuesta?",
        type: "warning", 
        showCancelButton: true,
        confirmButtonText: "Aceptar",
        cancelButtonText: "Cancelar",
        closeOnConfirm: false
    },
    function(){
    	invokeDeleteSurvey(id);         	
    });

}

this.invokeDeleteSurvey = function (id) {
	$.ajax({    	
        url: '@{csi.SurveysCSI.delete()}',
        type: 'POST',//'POST',	        
        cache: false,	  
        data: {id: id},
        success: function (data) {
        	if(data == 'ok'){
        		swal("Encuesta eliminada", "La encuesta ha sido eliminada satisfactoriamente.", "success");
        		location.reload();
        	}else{
        		swal("Error!", "No se puede eliminar la encuesta!.", "error");
        	}
        },
        beforeSend: function () {
        	Liverpool.loading("show");
        },
        complete: function () {
        	Liverpool.loading("hide");	        	
        }
    });
};



function validateEditSurvey() {
	
    $('#edit-csi-survey-form').validate({
        'ignore': '',
        'rules': {
            'month': {
              required: true,
              min: 1
            },
            'year': {
                required: true,
                min: 1
            },
            'goal': {
            	required: true,
            	number: true
            } 
          },
        'messages': {
            'name': {
                'required': 'Por favor, ingrese el nombre de la encuesta'
            },
            'initDate': {
                'required': 'Por favor, ingrese la fecha inicial'
            },
            'endDate': {
                'required': 'Por favor, ingrese la fecha final'
            },
            'month': {
                'required': 'Por favor, seleccione el mes',
                'min': 'Por favor, seleccione el mes'
                
            },
            'year': {
                'required': 'Por favor, seleccione el año', 
            	'min': 'Por favor, seleccione el año'
            },
            'goal': {
                'required': 'Por favor, ingrese la meta',
                'number': 'Por favor, solo ingrese números'
            }
        },
        errorPlacement: function(error, element) {
            if (element.next().is('.input-group-addon')) {
                error.insertAfter(element.next().parent());
            } else {
                error.insertAfter(element);
            }
        }
       }).form();
    
    if($('#edit-csi-survey-form').valid()) {    	
		return true; 			
	} else {
		return false;
	}
};


function validateUpload() {	
    $('#survey-upload-form').validate({
        'ignore': '',
        'rules': {
            'typeload': {
              required: true             
            },
            'csiFile': {
                required: true                
            }            
          },
        'messages': {
            'typeload': {
                'required': 'Por favor, Seleccione un tipo de carga'
            },
            'csiFile': {
                'required': 'Por favor, Seleccione un archivo'
            }
        }
    }).form();
    
    if($("#survey-upload-form").valid()) {
		return true; 			
	} else {
		return false;
	}
};


function uploadFile(form) {
	
	var TYPELOAD_HEADERS = {
        	'1':['Evaluador','Evaluado'],
        	'2':['Evaluado','Resultado']};
	
	var data = new FormData(form);
    $.ajax({
        url: '@{csi.FileController.uploadFile()}', //this is the submit URL
        type: 'post',
        contentType: false,
        data: data,
        success: function(data){
        	// se limpian los encabezados y los datos
        	$("#table-evaluations tbody").empty();
        	$("#table-evaluations thead").empty();
        	
       		var typeload = data['loadtype'];
       		var cargaRows = data['cargaRows'];
       		// se ponen los nuevos encbezados
       		$("#table-evaluations thead").append(
                 "<tr>" +
                     "<th>"+TYPELOAD_HEADERS[typeload][0]+"</th>" +
                     "<th>"+TYPELOAD_HEADERS[typeload][1]+"</th>" +
                 "</tr>"
             );
       		// se ponen los nuevos datos
        	 for(var i = 0; i < cargaRows.length; i++) {
                 $("#table-evaluations tbody").append(
                     "<tr>" +
                         "<td>"+cargaRows[i]['key']+"</td>" +
                         "<td>"+cargaRows[i]['value']+"</td>" +
                     "</tr>"
                 );
             }
       		// actualizamos la presentación de la tabla
        	 ut.refresh("table-evaluations");
        }
    });
}

function getFormattedDate(date) {
  var year = date.getFullYear();
  var month = (1 + date.getMonth()).toString();
  month = month.length > 1 ? month : '0' + month;
  var day = date.getDate().toString();
  day = day.length > 1 ? day : '0' + day;
  return day + '/' + month + '/' + year;
}

