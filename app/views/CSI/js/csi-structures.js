var sorted = {};
var entrar = true;
$(document).ready(function(){
    
    $("#userName").focus(function (){
        $("#userNumber").val('');
    });

    $("#userNumber").focus(function (){
        $("#userName").val('');
    });
    
    $("#employeeName").focus(function (){
        $("#employeeName").val('');
    });
    
    ocultarFiltros(1);
    /*
     * Inicialización de la tabla
     */
     Liverpool.loading("show");
     $.ajax({
         url: "@{csi.StructureCSI.getDatesStructures()}",
         type:'get',
         beforeSend: function (){
         	Liverpool.loading("show");
         },
         complete: function (){
         	Liverpool.loading("hide");
         },
         success: function (data) {        	 
             var selectLocation = $("#structureDate");
             for (var i = 0; i < data.length; i++) {
                 var opt = document.createElement("option");
                 opt.text = data[i];
                 opt.value = data[i];
                 selectLocation.append(opt);
             };
             
             $.fn.dataTable.ext.errMode = 'none';
             
            oStructureTable = $('#table-structure').dataTable({
                "iDisplayLength": 10,
                "bFilter": false,
                "bLengthChange": false,
                "bAutoWidth": false,
                "bDestroy": true,
                "oLanguage": {
                    "sZeroRecords": "Ningún Registro",
                    "sInfo": "Mostrando Eventos de _START_ al _END_ de _TOTAL_ totales",
                    "sInfoEmpty": "0 Eventos",
                     "sPaginatePrevious": "Previous page",
                     "sProcessing": "Cargando...",
                     "oPaginate": {
                        "sFirst":    "<<",
                        "sLast":     ">>",
                        "sNext":     ">",
                        "sPrevious": "<"
                    }
                },
                "columnDefs": [ {
                    "visible": false,
                    "searchable": false,
                  }
                  ],
                "bProcessing": false,
                "bStateSave" : false,
                "bServerSide": true,
                "sAjaxSource": "@{csi.StructureCSI.getStructureData()}",
                "fnServerParams": function ( aoData ) {
                    var array = jQuery($('#filter-form')).serializeArray();
                    jQuery.each(array, function() {
                        aoData.push({'name': this.name, 'value': this.value});
                    });
                },
                "pagingType": "full_numbers",
                //"aaSorting": [[ 4, "ASC" ]],
                "createdRow": function(row, data, index){
                	$(row).attr("data-area", data[0]);
                    $(row).attr("data-employee-number", data[1]);
                    $(row).attr("data-user", data[2]);
                    $(row).attr("data-name", data[3]);
                    $(row).attr("data-funcion", data[4]);
                    $(row).attr("data-seccion", data[5]);
                    $(row).attr('onclick', "openModalStructure(this)");
                },
                "drawCallback" : function(){
                	if(sorted.iDisStart != undefined){
                		oStructureTable.fnSettings()._iDisplayStart = sorted.iDisStart;
                		var aux = sorted.page;
			    		sorted = {};
			    		
			    		oStructureTable.fnPageChange(aux);
                	}                	
                }
            });
            
            $("#table-structure_paginate").css("padding", 0);
            $("#table-structure_info").css("font-size", 12);
		    $("#table-structure_length").css("font-size", 12);
		    $("#table-structure_filter").css("font-size", 12);
		    $("#table-structure select").css({
		    paddingTop: "0",
		            paddingBottom: "0",
		            height: "32px",
		            fontSize: "12px"
		    });		
		    
		    $('#table-structure_paginate').on('click', function(){
		    	var info = oStructureTable.DataTable().page.info();
		    	$("#iDisplayStart").val(oStructureTable.fnSettings()._iDisplayStart);
		    	oStructureTable.fnPageChange(info.page);
		    });
		    
		  //Filtrado
            $('#filter-form').submit(function(){
            	var vacios = 0;
            	
            	if($("#userNumber").val() == null || $("#userNumber").val() == 0)	vacios++;
            	if($("#userName").val() == "" || $("#userName").val() == null)	vacios++;    
            	if($("#employeeName").val() == "" || $("#employeeName").val() == null)	vacios++;
            	if($("#location").val() < 0 || $("#location").val() == null || $("#location").val() == undefined) vacios++;
            	if($("#selectSection").val() < 0 || $("#selectSection").val() == null || $("#selectSection").val() == undefined) vacios++;
            	
            	if(vacios == 5){
            		$("#message").show();
            		Liverpool.setMessage($('.inner-message'), "Se debe elegir al menos un filtro", 'warning');
            	}else{
            		$("#message").hide();
            		Liverpool.loading("show");
                    oStructureTable.fnPageChange(0);
                    Liverpool.loading("hide");
            	}
                return false;
            }); 
          	
          //Select location 
            $.ajax({
                url: "@{csi.StructureCSI.getLocation()}",
                type:'get',
                beforeSend: function (){
                	Liverpool.loading("show");
                },
                complete: function (){
                	Liverpool.loading("hide");
                },
                success: function (data) {
                    var selectLocation = document.createElement("select");
                    selectLocation.name = "location";
                    selectLocation.id = "location";
                    selectLocation.setAttribute("class","form-control");
                    //selectLocation.setAttribute("onchange", "setDepartments(this);filterRemove(this);");
                    var opt = document.createElement("option");
                    opt.text = "Seleccione una opción";
                    opt.value = "-1";
                    selectLocation.add(opt);
                    for (var i = 0; i < data.length; i++) {
                        opt = document.createElement("option");
                        opt.text = data[i];
                        opt.value = data[i];
                        selectLocation.add(opt, null);
                    };
                    $("#div_select_location"). append(selectLocation);                    
                }
            });
           
          //Select Sections
            $.ajax({
            	           	
                url: "@{csi.StructureCSI.getSection()}",
                type:'get',
                beforeSend: function (){
                	Liverpool.loading("show");
                },
                complete: function (){
                	Liverpool.loading("hide");
                },
                success: function (data) {
                    var selectSection = document.createElement("select");
                    selectSection.name = "section";
                    selectSection.id = "selectSection";
                    selectSection.setAttribute("class","form-control");
                    //selectSection.setAttribute("onchange", "setDepartments(this);filterRemove(this);");
                    var opt = document.createElement("option");
                    opt.text = "Seleccione una opción";
                    opt.value = "-1";
                    selectSection.add(opt);
                    for (var i = 0; i < data.length; i++) {
                        opt = document.createElement("option");
                        opt.text = data[i][1];
                        opt.value = data[i][0];
                        selectSection.add(opt, null);
                    };
                    $("#div_select_section"). append(selectSection);
                }
            });
          
//            $.ajax({
//                url: "@{csi.StructureCSI.getLevels1()}",
//                type:'get',
//                async: true,
//                data: {
//                    level: 1
//                },
//                beforeSend: function (){
//                	Liverpool.loading("show");
//                },
//                complete: function (){
//                	Liverpool.loading("hide");
//                },
//                success: function (data) {
//                    var selectLevel = document.createElement("select");
//                    selectLevel.name = "level1";
//                    selectLevel.id = "level1";
//                    selectLevel.setAttribute("class","form-control");
//                    var opt = document.createElement("option");
//                    opt.text = "Seleccione una opción";
//                    opt.value = "-1";
//                    selectLevel.add(opt);
//                    selectLevel.setAttribute("onchange", "searchNextLevel(this);filterRemove(this);");
//                    selectLevel.setAttribute("data-level", 1);
//                    for (var i = 0; i < data.length; i++) {
//                        opt = document.createElement("option");
//                        opt.text = data[i][0];
//                        opt.value = data[i][1];
//                        selectLevel.add(opt, null);
//                    };
//                    $("#div_select_level_1"). append(selectLevel);
//                }
//            });

            //Se crean los demas combo
            for (var i = 2; i < 9; i++) {
                var selectLevel = document.createElement("select");
                selectLevel.name = "level" + i;
                selectLevel.id = "level" + i;
                selectLevel.setAttribute("class","form-control");
                var opt = document.createElement("option");
                opt.text = "Seleccione una opción";
                opt.value = "-1";
                selectLevel.add(opt);
                selectLevel.setAttribute("onchange", "searchNextLevel(this)");
                selectLevel.setAttribute("data-level", i);
                $("#div_select_level_"+i).append(selectLevel);
                
            }
          
            Liverpool.loading("hide");
         }   
    	  
     });
    
     $("#btn_manager").click(function () {
    	Liverpool.loading("show");
         $("#modalManagerNumber").val("");
         $("#modalManagerName").val("");
         $("#btn_change_mamager").attr("disabled", true);
         $("#modal-manager").modal("show");
         $("#alert-user").hide();
         Liverpool.loading("hide");         
     }); 
     
     $("#btn_validate_mamager").click(function (){
         var managerId = $("#modalManagerNumber").val().trim();
         var managerName = $("#modalManagerName").val().trim();
         $("#alert-user").hide();
         

         if(managerId.length == 0 && managerName.length == 0) {
             $("#msg-alert-user").html("Por favor especifíque al menos un campo");
             $("#alert-user").show(200);
         } else {
             Liverpool.loading("show");
             $.ajax({
                 url: "@{csi.StructureCSI.validateManager()}",
                 type:'get',
                 async: true,
                 data: {
                     managerId: managerId,
                     managerName: managerName,
                     employeeNumber: $("#modalEmployeeNumber").val()
                 },
                 beforeSend: function (){
                 	Liverpool.loading("show");
                 },
                 complete: function (){
                 	Liverpool.loading("hide");
                 },
                 success: function (data) {
                     if (data.length > 0) {
                         $("#modalManagerNumber").val(data[0][11]);
                         $("#modalManagerName").val(data[0][32]);
                         $("#btn_change_mamager").removeAttr("disabled");
                     } else {
                         $("#msg-alert-user").html("No se encontró a la persona especificada");
                         $("#btn_change_mamager").attr("disabled", "disabled");
                         $("#alert-user").show(200);
                     }
                     Liverpool.loading("hide");
                 },
                 error: function(data) {
                     Liverpool.loading("hide");
                 }
             });
         }
     });
    
     
//////CAmbio de Jefe
     $("#btn_change_mamager").click(function () {
         $("#alert-user").hide();
         swal({
             title: "Mensaje",
             text: "Al realizar esta acción se recalcularán todos los planes automáticamente." +
                 "\nCualquier cambio manual de Plan realizado anteriormente se perderá. ¿Desea continuar?",
             type: "warning",
             showCancelButton: true,
             confirmButtonColor: "#DD6B55",
             confirmButtonText: "Aceptar",
             cancelButtonText: "Cancelar",
             closeOnConfirm: true
         },
         function(){
             var userId = $("#modalEmployeeNumber").val();
             var newManagerId = $("#modalManagerNumber").val();
             $.ajax({
                 url: "@{csi.StructureCSI.changeManager()}",
                 type:'get',
                 async: true,
                 data: {
                     userId: userId,
                     newManagerId: newManagerId
                 },
                 beforeSend: function (){
                 	Liverpool.loading("show");
                 },
                 complete: function (){
                 	Liverpool.loading("hide");
                 },
                 success: function (data) {
                     if (!data.error) {
                         $("#modal-manager").modal("hide");
                         $("#modal-structure").modal("hide");
                         oStructureTable.fnPageChange(0);
                     } else {
                         swal({
                             title: "",
                             text: data.message,
                             type: "error"
                         });
                     }
                 }
             });
         });
     });
     
     
     $("#btn_function").click(function () {
         $("#btn_change_function").attr("disabled", true);
         $.ajax({
             url: "@{csi.StructureCSI.getFunction()}",
             type:'get',
             async: true,
             beforeSend: function (){
             	Liverpool.loading("show");
             },
             complete: function (){
             	Liverpool.loading("hide");
             },
             success: function (data) {
                 var oldFunction = $("#modalFunction").val();
                 var functionSelect = $("#function-select");
                 for (var i = 0; i < data.length; i++) {
                     var opt = document.createElement("option");
                     opt.value = data[i];
                     opt.text = data[i];

                     if (oldFunction == data[i]) {
                         opt.selected = true;
                     }
                     functionSelect.append(opt);
                 }
             }
         });
         $("#modal-function").modal("show");
     });
     
    
     $("#table-structure tbody tr").css("cursor", "pointer");
});

function ocultarFiltros(level){
	for(var i=level;i<8;i++){
		$("#level-"+(i+1)).hide();
	}
}

function mostrarFiltros(level){
	for(var i=1;i<=level+1;i++){
		$("#level-"+i).show();
	}
}

function setOrder(column){
	$("#iSortCol_0").val(column);
	var info = oStructureTable.DataTable().page.info();
	
	sorted.iDisStart = oStructureTable.fnSettings()._iDisplayStart;
	sorted.page = info.page;
	entrar = true;
	
	if(!$("#iDisplayStart").length){
		$("#filter-form").append("<input type='hidden' name='iDisplayStart' id='iDisplayStart' value='"+sorted.iDisStart+"'/>");
	}else{
		$("#iDisplayStart").val(sorted.iDisStart);
	}
}

function activa(value){
    $("#btn_change_function").removeAttr("disabled").after('');
}

///Cambio de function
$("#btn_change_function").click(function () {
    var newFunction = $("#function-select option:selected").val();
    var userId = $("#modalEmployeeNumber").val();
    $.ajax({
        url: "@{csi.StructureCSI.changeFunction()}",
        type:'get',
        async: true,
        data: {
            userId: userId,
            newFunction: newFunction
        },
        beforeSend: function (){
        	Liverpool.loading("show");
        },
        complete: function (){
        	Liverpool.loading("hide");
        },
        success: function (data) {
            if (data != 0) {
                $("#modal-function").modal("hide");
                $("#modal-structure").modal("hide");
                swal("Mensage", "La función se actualizo con éxito", "success");
                $('#search').click();
            }
        }
    });
});


function openModalStructure(data) {
    //$("#modalSection").val($(data).attr("data-seccion"));
    $("#modalEmployeeNumber").val($(data).attr("data-employee-number"));
    $("#modalUserName").val($(data).attr("data-user"));
    
    showSections($("#modalEmployeeNumber").val());
    $.ajax({
        url: "@{csi.StructureCSI.getInfoModal()}",
        type:'get',
        async: true,        
        data: {
            userId: $("#modalEmployeeNumber").val()
        },
        cache: false,
        beforeSend: function (){
        	Liverpool.loading("show");
        },
        success: function (data) {
        	var location;
            for (var i = 0; i < data.length; i++) {
                $("#modalEmail").val(data[i][0]);
                $("#modalUserName").val(data[i][1]);
                $("#modalFunction").val(data[i][2]);
                $("#modalManager").val(data[i][3]);             
                $("#modalFirstName").val(data[i][4]);
                $("#modalLastName").val(data[i][5]);
                location = data[i][6];
            }
            showLocation(location);
        }
    });
    $("#modal-structure").modal("show");
}

function showSections(empNumber){
	$('#modalSection').empty();
	var arrSelect = new Array();
	$.ajax({
       	
        url: "@{csi.StructureCSI.getSectionsSelect()}",
        type:'get',
        data: {
            numEmployee: empNumber
        },
        beforeSend: function (){
        	Liverpool.loading("show");
        },
        success: function (data) {
        	var sections = data['sections'];
        	var sectionSelect = data['sectionSelected'];
        	for(var i= 0; i < sections.length; i++){
        		$('#modalSection').append('<option value='+ sections[i]['id'] + '>'+ sections[i]['nombreSeccion'] +'</option>');
        	}
        	$('#modalSection').multiselect({
        		includeSelectAllOption: true,
        		maxHeight: 450
        	});
        	if(sectionSelect.length > 0){
        		for(var j = 0; j<sectionSelect.length; j++){
            		arrSelect.push(sectionSelect[j]['id']);
            	}
        	}
        	
        	$('#modalSection').val(arrSelect);
        	$("#modalSection").multiselect("rebuild");
        }
    });
}

function showLocation(location){
	var selectLocation = 0;
	$.ajax({
        url: "@{csi.StructureCSI.getLocation()}",
        type:'get',
        cache: false,
        beforeSend: function (){
        	Liverpool.loading("show");
        },
        complete: function (){
        	Liverpool.loading("hide");
        },
        success: function (data) {
            $("#location-select").html('<option value=' + location + '>' + location + '</option>');
            for (var i = 0; i < data.length; i++) {
            	$("#location-select").append("<option value= " + data[i] + ">" + data[i] + "</option>");
            };
        }
    });
}



function filterRemove(value){

    var element = value.getAttribute("name");

    if(element =="userNumber" ||
        element =="userName" ||
        element =="location" ||
        element =="section" ||
        element =='employeeName'){       
    }else{
        $("#userNumber").val("");
        $("#userName").val("");
        $("#employeeName").val("");
        $("#location").val(-1);
        $("#selectSection").val(-1); 
    }
}

function searchNextLevel(element) {

    Liverpool.loading("show");
    level = Number(element.getAttribute("data-level"));
    nextLevel = level + 1;
    department = element.options[element.options.selectedIndex].value;
    for (var i = nextLevel; i < 9; i++) {
        var opt = document.createElement("option");
        opt.text = "Seleccione una opción";
        opt.value = "-1";
        $("#level"+i).html('');
        $("#level"+i).append(opt);
     }
    mostrarFiltros(level);
    
    $.ajax({
        url: "@{csi.StructureCSI.getLevels()}",
        type:'get',
        async: true,
        data: {
            "department": department,
            "date": $("#structureDate").val()
        },
        beforeSend: function (){
        	Liverpool.loading("show");
        },
        complete: function (){
        	Liverpool.loading("hide");
        },
        success: function (data) {
            var selectLevel = $("#level"+nextLevel);
            selectLevel.html('');
            var opt = document.createElement("option");
            opt.text = "Seleccione una opción";
            opt.value = "-1";
            selectLevel.append(opt);
            if(data.length<=0){
            	ocultarFiltros(level);
            }
            for (var i = 0; i < data.length; i++) {
                opt = document.createElement("option");
                opt.text = data[i][0];
                opt.value = data[i][1];
                selectLevel.append(opt);
            };
        }
    });

    Liverpool.loading("hide");
}

$("#btn_structure_guardar").click(function () {
    var email = $("#modalEmail").val();
    var ubication = $("#location-select option:selected").text();
    var section = $("#modalSection").val();
    var structureDate = $("#structureDate").val();
    if(email.trim() != "" || ubication.trim() != "" || section.trim() != ""){
        if(validarEmail(email)){    		    
		    var userId = $("#modalEmployeeNumber").val();		    
		    $.ajax({
		        url: "@{csi.StructureCSI.setPlanEmail()}",
		        type:'get',
		        async: true,
		        data: {
		            email: email,		            
		            idEmployee: userId,
		            ubication: ubication,
		            section: section,
                            structureDate: structureDate 
		        },
		        beforeSend: function (){
	            	Liverpool.loading("show");
	            },
	            complete: function (){
	            	Liverpool.loading("hide");
	            },
		        success: function (data) {
		            if (data != 0) {
		                location.reload();
		            }
		        }
		    });
		}else{
		    alert("Formato invalido");
		}
	}else {
	    alert("El campo email es requerido");
	}	
});

function validarEmail(valor) {
    var correcto = false;
  if (/^(([^<>()[\]\.,;:\s@\"]+(\.[^<>()[\]\.,;:\s@\"]+)*)|(\".+\"))@(([^<>()[\]\.,;:\s@\"]+\.)+[^<>()[\]\.,;:\s@\"]{2,})$/i.test(valor)){
   correcto= true;
  } 
  return correcto;
}
$('#export').click(function() {
	Liverpool.loading("show");
    var array = jQuery($('#filter-form')).serializeArray();
    var param = "?";
    jQuery.each(array, function() {
        param += this.name + "=" + this.value +"&";
    });
    param = param.substring(0, param.length-1);
    location.href = '@{csi.StructureCSI.export()}' + param;
    Liverpool.loading("hide");
});
	