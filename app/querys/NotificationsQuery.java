package querys;

import java.sql.CallableStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import play.*;
import play.db.jpa.JPA;
import javax.persistence.Query;
import java.util.*;
import static utils.Queries.getOracleConnection;
import utils.PagingResult;

public class NotificationsQuery {
    public static List needsReview(String userId) {

        String statement =
            " SELECT COUNT(1) FROM PUL_PLAN_ACCION PA, PUL_EMPLOYEE_CLIMA EC" +
            " WHERE PA.FN_ID_EMPLOYEE_CLIMA = EC.FN_ID_EMPLOYEE_CLIMA" +
            " AND EC.FN_MANAGER = " +  userId +
            " AND PA.FC_STATUS_REVIEW IS NULL";

        Query oQuery = JPA.em().createNativeQuery(statement);
        return oQuery.getResultList();
    }

    public static List hasRejected(String userId) {

        String statement =
            " SELECT COUNT(1) FROM PUL_PLAN_ACCION PA, PUL_EMPLOYEE_CLIMA EC" +
            " WHERE PA.FN_ID_EMPLOYEE_CLIMA = EC.FN_ID_EMPLOYEE_CLIMA" +
            " AND EC.FN_MANAGER = " + userId +
            " AND FC_STATUS_PLAN = 'RECHAZADO'";

        Query oQuery = JPA.em().createNativeQuery(statement);
        return oQuery.getResultList();
    }
}
