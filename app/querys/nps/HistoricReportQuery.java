/**
 * @(#) HistoricReportQuery 5/07/2017
 */

package querys.nps;

import java.sql.CallableStatement;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import oracle.jdbc.OracleCallableStatement;
import oracle.jdbc.OracleConnection;
import oracle.jdbc.OracleTypes;
import play.Logger;
import static utils.Queries.getOracleConnection;

/**
 *
 * @author Rodolfo Miranda -- Qualtop
 */
public class HistoricReportQuery {

    public static HashMap<String, List> getData(Integer userId, Integer idQuestion,
                                    Integer groupId, Integer businessId, String area,
                                    String zone, Integer store,Integer manager,
                                    Integer chief, Integer section, String startDate,
                                    String endDate, String option) {
        HashMap<String, List> lists = new HashMap();
        List resultsList        = null;
        List resultsListAcum    = null;
        
        try {
            OracleConnection connection = getOracleConnection();
            
            String query = "BEGIN\n" +
                            "  PORTALUNICO.PR_GET_NPS_HISTORIC(\n" +
                            "    P_USERID => %s,\n" +
                            "    PN_ID_QUESTION => %s,\n" +                      
                            "    PN_GROUP_ID => %s,\n" +
                            "    PN_BUSINESS_ID => %s,\n" +
                            "    PC_AREA => %s,\n" +
                            "    PC_ZONE => %s,\n" +
                            "    PN_STORE => %s,\n" +
                            "    PN_USERID_MANAGER => %s,\n" +
                            "    PN_USERID_CHIEF => %s,\n" +
                            "    PN_SECTION => %s,\n" +
                            "    PD_INI_DATE => %s,\n" +
                            "    PD_END_DATE => %s,\n" +
                            "    PN_OPTION => %s,\n" +
                            "    CUR_NPS => ?,\n" +
                            "    CUR_NPS_ACUM => ?\n" +
                            "  );\n" +
                            "END;";
            
            query = String.format(query,
                setIntParameter(userId),
                setIntParameter(idQuestion),    
                setIntParameter(groupId),
                setIntParameter(businessId),
                setStringParameter(area),
                setStringParameter(zone),
                setIntParameter(store),
                setIntParameter(manager),
                setIntParameter(chief),
                setIntParameter(section),
                setStringParameter(startDate),
                setStringParameter(endDate),
                setStringParameter(option)
            );
            
            CallableStatement statement = connection.prepareCall(query);        
            
            statement.registerOutParameter(1, OracleTypes.CURSOR);
            statement.registerOutParameter(2, OracleTypes.CURSOR);
            
            statement.execute();
            
            ResultSet results = ((OracleCallableStatement) statement).getCursor(1);
            ResultSet resultsAcum = ((OracleCallableStatement) statement).getCursor(2);
            
            ResultSetMetaData metaData = results.getMetaData();
            ResultSetMetaData metaData2 = resultsAcum.getMetaData();
            
            int columnsCount = metaData.getColumnCount();
            
            Object[] data;
            resultsList = new ArrayList();
            
            while (results.next()) {
                data = new Object[columnsCount];
                
                for (int i = 1; i <= columnsCount; i++) {
                    data[i - 1] = results.getObject(i);
                }
                
                resultsList.add(data);
            }
            
            columnsCount = metaData2.getColumnCount();
            
            Object[] data2;
            resultsListAcum = new ArrayList();
            
            while (resultsAcum.next()) {
                data2 = new Object[columnsCount];
                
                for (int i = 1; i <= columnsCount; i++) {
                    data2[i - 1] = resultsAcum.getObject(i);
                }
                
                resultsListAcum.add(data2);
            }
            
            lists.put("months", resultsList);
            lists.put("acum", resultsListAcum);
            
            statement.close();
            results.close();
        } catch (SQLException e) {
            Logger.error(e.getMessage());
        }
        
        return lists;
    }
    
    public static HashMap<String, List> getBusinessDetail(Integer userId,
            Integer idQuestion, Integer groupId, Integer businessId,
            String area, String startDate, String endDate, String option) {
        HashMap<String, List> detail = new HashMap();
        
        try {
            OracleConnection connection = getOracleConnection();
            
            String query = "BEGIN\n" +
                            "  PORTALUNICO.PR_GET_NPS_HISTORIC_DET(\n" +
                            "    P_USERID => %s,\n" +
                            "    PN_ID_QUESTION => %s,\n" +
                            "    PN_GROUP_ID => %s,\n" +
                            "    PN_BUSINESS_ID => %s,\n" +
                            "    PC_AREA => %s,\n" +
                            "    PD_INI_DATE => %s,\n" +
                            "    PD_END_DATE => %s,\n" +
                            "    PN_OPTION => %s,\n" +
                            "    CUR_NPS => ?,\n" +
                            "    CUR_NPS_ACUM => ?\n" +
                            "  );\n" +
                            "END;";
            
            query = String.format(query,
                setIntParameter(userId),
                setIntParameter(idQuestion),
                setIntParameter(groupId),
                setIntParameter(businessId),
                setStringParameter(area),
                setStringParameter(startDate),
                setStringParameter(endDate),
                setStringParameter(option)
            );
            
            CallableStatement statement = connection.prepareCall(query);        
            
            statement.registerOutParameter(1, OracleTypes.CURSOR);
            statement.registerOutParameter(2, OracleTypes.CURSOR);
            
            statement.execute();
            
            ResultSet stores = ((OracleCallableStatement) statement).getCursor(1);
            ResultSet resultsAcum = ((OracleCallableStatement) statement).getCursor(2);
                        
            setDetail(detail, "stores", stores);
            ResultSetMetaData metaData2 = resultsAcum.getMetaData();
            
            int columnsCount = metaData2.getColumnCount();
            
            Object[] data2;
            List resultsListAcum = new ArrayList();
            
            while (resultsAcum.next()) {
                data2 = new Object[columnsCount];
                
                for (int i = 1; i <= columnsCount; i++) {
                    data2[i - 1] = resultsAcum.getObject(i);
                }
                
                resultsListAcum.add(data2);
            }
            detail.put("acum", resultsListAcum);
            statement.close();
            
            stores.close();
            
        } catch (SQLException e) {
            Logger.error(e.getMessage());
        }
        
        return detail;
    }
    
    public static Float getAcumulado(Integer userId, Integer idQuestion,
                                    Integer groupId, Integer businessId, String area,
                                    String zone, Integer store,Integer manager,
                                    Integer chief, Integer section, String startDate,
                                    String endDate, String option) {
        Float acumulado = 0f;
        
        try {
            OracleConnection connection = getOracleConnection();
            
            String query = "BEGIN\n" +
                            "  PORTALUNICO.PR_GET_NPS_HISTORIC_ACUM(\n" +
                            "    P_USERID => %s,\n" +
                            "    PN_ID_QUESTION => %s,\n" +                      
                            "    PN_GROUP_ID => %s,\n" +
                            "    PN_BUSINESS_ID => %s,\n" +
                            "    PC_AREA => %s,\n" +
                            "    PC_ZONE => %s,\n" +
                            "    PN_STORE => %s,\n" +
                            "    PN_USERID_MANAGER => %s,\n" +
                            "    PN_USERID_CHIEF => %s,\n" +
                            "    PN_SECTION => %s,\n" +
                            "    PD_INI_DATE => %s,\n" +
                            "    PD_END_DATE => %s,\n" +
                            "    PN_OPTION => %s,\n" +
                            "    p_pct_total_promt => ?,\n" +
                            "    p_pct_total_neut => ?,\n" +
                            "    p_pct_total_detra => ?,\n" +
                            "    p_pct_total_eva => ?\n" +
                            "  );\n" +
                            "END;";
            
            query = String.format(query,
                setIntParameter(userId),
                setIntParameter(idQuestion),    
                setIntParameter(groupId),
                setIntParameter(businessId),
                setStringParameter(area),
                setStringParameter(zone),
                setIntParameter(store),
                setIntParameter(manager),
                setIntParameter(chief),
                setIntParameter(section),
                setStringParameter(startDate),
                setStringParameter(endDate),
                setStringParameter(option)
            );
            
            CallableStatement statement = connection.prepareCall(query);        
            
            statement.registerOutParameter(1, OracleTypes.FLOAT);
            statement.registerOutParameter(2, OracleTypes.FLOAT);
            statement.registerOutParameter(3, OracleTypes.FLOAT);
            statement.registerOutParameter(4, OracleTypes.FLOAT);
            
            statement.execute();
            
            acumulado = statement.getFloat(4);
            
            statement.close();
           
        } catch (SQLException e) {
            Logger.error(e.getMessage());
        }
        
        return acumulado;
    }
    
    public static void setDetail(HashMap detail, String name, ResultSet results) throws SQLException {
        ResultSetMetaData metaData = results.getMetaData();
        int columnsCount = metaData.getColumnCount();
            
        List data = new ArrayList();
        
        Object[] row;

        while (results.next()) {
            row = new Object[columnsCount];
            
            for (int i = 1; i <= columnsCount; i++) {
                row[i - 1] = results.getObject(i);
            }

            data.add(row);
        }
        
        detail.put(name, data);
    }
    
    public static String setIntParameter(Integer value) {
        if (value == null) {
            return "null";
        }
        
        return Integer.toString(value);
    }
    
    public static String setStringParameter(String value) {
        if (value == null || value.isEmpty()) {
            return "null";
        }
        
        return "'" + value + "'";
    }
    
    public static String setArrayParameter(String value) {
        if (value == null) {
            return "";
        }
        
        return "'" + value + "'";
    }
}
