/**
 * @(#) EvaluationDetailsQuery 3/04/2017
 */

package querys.nps;

import java.io.File;
import java.sql.Array;
import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.persistence.Query;
import models.nps.ChallengeUtil;
import oracle.jdbc.OracleConnection;
import oracle.jdbc.internal.OracleTypes;
import play.Play;
import play.db.jpa.JPA;
import utils.nps.connectionDB.DbConnection;
import utils.nps.model.Evaluation;
import utils.nps.model.EvaluationDetail;
import utils.nps.model.EvaluationsModel;

/**
 *
 * @author Rodolfo Miranda -- Qualtop
 */
public class EvaluationDetailsQuery {

    private static final EvaluationDetailsQuery LOCK_1 = new EvaluationDetailsQuery() {};
    
    public static void getDetailsEvaluation(){
        
    }
    
    public static void getChallengesDetail(){
        
    }
    
    public static void getTracingDetail(){  
        
    }
    
    public static EvaluationsModel getEvaluations(String userId,
            String firstDate, String lastDate, String []store, String []manager,
            String []chief, String []section, String zone, String group,String business,
            int ini, int end, int flag, int column, String order, String employeeId)
            throws SQLException, ParseException{
        
        List<Evaluation> evaluations = new ArrayList<>();
        EvaluationsModel model = new EvaluationsModel();
        String query = 
           "{call PORTALUNICO.PR_GET_EVAL_DETAIL(?, ?, ?, ?, ?, ? , ?,?,?"
                                            + " ,? ,? ,? ,? ,?, ? , ?, ?, ?)}";
        
        CallableStatement call = null;
        Connection conn = null;
        ResultSet rs = null;
        
        try{
            conn = DbConnection.getDBConnection();
            
             Array array1 = ((OracleConnection) conn).
                     createOracleArray("PORTALUNICO.ARRAY_STRING_TABLE", store);
             
             Array array2 = ((OracleConnection) conn).
                     createOracleArray("PORTALUNICO.ARRAY_STRING_TABLE", manager);
             
             Array array3 = ((OracleConnection) conn).
                     createOracleArray("PORTALUNICO.ARRAY_STRING_TABLE", chief);
             
             Array array4 = ((OracleConnection) conn).
                     createOracleArray("PORTALUNICO.ARRAY_STRING_TABLE", section);
                                
            if( conn != null ){
                call = conn.prepareCall(query);
                
                call.setString(1, userId );         //User id
                call.setString(2, group );
                call.setString(3, business );
                call.setString(4, zone );
                call.setArray(5, array1);
                call.setArray(6, array2);
                call.setArray(7, array3);
                call.setArray(8, array4);
                call.setString(9, firstDate);       //Date in
                call.setString(10, lastDate);        //Date out
                call.setInt(11,ini);
                call.setInt(12,end);
                call.setInt(13,flag);
                call.setInt(14,column);
                call.setString(15,order);
                call.setString(16,employeeId);
                call.registerOutParameter(17, OracleTypes.CURSOR);           //Chief    
                call.registerOutParameter(18, OracleTypes.INTEGER);           //Chief    
                
                call.execute();
                
                rs = (ResultSet)call.getObject(17);
                
                List<String> columnNames = new ArrayList<>();
                ResultSetMetaData rsmd = rs.getMetaData();
                for (int i = 1; i <= rsmd.getColumnCount(); i++) {
                    columnNames.add(rsmd.getColumnLabel(i));
                }
                List<Object> list = new ArrayList<>();
                while( rs.next() ){
                    Object [] columns = new Object[19];
                    //Evaluation e = new Evaluation();
                    columns[0]=  rs.getString(2)== null ? " " : rs.getString(2) ;
                    columns[1]=  rs.getString(3) == null ? " " : rs.getString(3) ;
                    columns[2]=  rs.getString(4)== null ? " " :rs.getString(4);
                    columns[3]=  rs.getString(5)== null ? " " :rs.getString(5);
                    columns[4]=  rs.getString(6)== null ? " " :rs.getString(6) ;
                    columns[5]=  rs.getString(7)== null ? " " :rs.getString(7);
                    columns[6]=  rs.getString(8)== null ? " " : rs.getString(8);
                    columns[7]=  rs.getString(9)== null ? " " : rs.getString(9);
                    columns[8]=  rs.getString(10)== null ? " " : rs.getString(10);
                    columns[9]=  rs.getString(11)== null ? " " : rs.getString(11);
                    if(rs.getString(12) != null && !rs.getString(12).equals("")){
                        if(getRecordAudio(rs.getString(12).trim())){
                            columns[10]= rs.getString(12).trim()+"|SI";
                        }else {
                            columns[10]= rs.getString(12).trim()+"|NO";
                        }
                        
                    }else {
                        columns[10]= "|NO";
                    }
                    
                    columns[11]=  rs.getString(13)== null ? " " : rs.getString(13);
                    //e.setIdTracking(rs.getString("SEGUIMIENTO_ID") );
                    columns[12]=  rs.getString(14)== null ? " " : rs.getString(14);
                    //e.setIdChallenge(rs.getSti5g("IMPUGNACIONES_ID") );
                    columns[13]=  rs.getString(15)== null ? " " : rs.getString(15);
                    columns[14]=  rs.getString(16)== null ? " " : rs.getString(16);
                    columns[15]=  rs.getString(17)== null ? " " :rs.getString(17);
                    columns[16]=  rs.getString(18)== null ? " " :rs.getString(18);
                    columns[17]=  rs.getString(19)== null ? " " :rs.getString(19);
                    columns[18]=  rs.getString(20)== null ? " " :rs.getString(20);
                    list.add(columns);
                }
                
                int count = (int) call.getObject(18);
                
                model.setEvaluations( list );
                model.setCount( count );
                
            }
        } catch (SQLException ex) {
            Logger.getLogger(EvaluationDetailsQuery.class.getName()).log(Level.SEVERE, null, ex);
        }finally {
            if (call != null) {
		call.close();
            }
            if (conn != null) {
		conn.close();
            }
            if (rs != null) {
                rs.close();
            }
	}
        return model;
    }
    
    public static List getEvaluationsXLS(String userId,
            String firstDate, String lastDate, String []store, String []manager,
            String []chief, String []section, String zone, String group,String business,
            int flag,String search)throws SQLException, ParseException{
    
        String query = 
           "{call PORTALUNICO.PR_GET_EVAL_DETAIL_XLS(?, ?, ?, ?"
                                            + " ,? ,? ,? ,? ,?,?,?,?,?)}"; //agregar un ?
        
        CallableStatement call = null;
        Connection conn = null;
        ResultSet rs = null;
        List<Object> list = new ArrayList<>();
        try{
            conn = DbConnection.getDBConnection();
            
             Array array1 = ((OracleConnection) conn).
                     createOracleArray("PORTALUNICO.ARRAY_STRING_TABLE", store);
             
             Array array2 = ((OracleConnection) conn).
                     createOracleArray("PORTALUNICO.ARRAY_STRING_TABLE", manager);
             
             Array array3 = ((OracleConnection) conn).
                     createOracleArray("PORTALUNICO.ARRAY_STRING_TABLE", chief);
             
             Array array4 = ((OracleConnection) conn).
                     createOracleArray("PORTALUNICO.ARRAY_STRING_TABLE", section);
                                
            if( conn != null ){
                call = conn.prepareCall(query);
                
                call.setString(1, userId );         //User id
                call.setString(2, group );
                call.setString(3, business );
                call.setString(4, zone );
                call.setArray(5, array1);
                call.setArray(6, array2);
                call.setArray(7, array3);
                call.setArray(8, array4);
                call.setString(9, firstDate);       //Date in
                call.setString(10, lastDate);        //Date out
                call.setInt(11,flag);
                call.setString(12,search);
                call.registerOutParameter(13, OracleTypes.CURSOR);           //Chief    
                
                call.execute();
                
                rs = (ResultSet)call.getObject(13);
                
                while( rs.next() ){
                    Object [] columns = null;
                    if( flag == 0 ){
                        columns = new Object[21];
                    }else if( flag == 1 ){
                        columns = new Object[48];
                    }
                    
                    for( int i = 0; i < columns.length; i++ ){
                        columns[i]=  rs.getString(i+1)== null ? "" : rs.getString(i+1) ;
                    }
                    list.add(columns);
                }
                
            }
        } catch (SQLException ex) {
            Logger.getLogger(EvaluationDetailsQuery.class.getName()).log(Level.SEVERE, null, ex);
        }finally {
            if (call != null) {
		call.close();
            }
            if (conn != null) {
		conn.close();
            }
            if (rs != null) {
                rs.close();
            }

	}
        return list;
    }
    
    public static EvaluationDetail getEvaluationDetail(String idAnswer){
        EvaluationDetail ev = new EvaluationDetail();
        ev.setId(idAnswer);
        String statement = "select FN_ID_QUESTION, fc_text_answer\n" +
                            "from portalunico.pul_nps_question_answer\n" +
                            "where fn_id_answer = "+Integer.parseInt(idAnswer);
        String statement2 = "select a.fc_id_category_1_1,a.fc_id_category_2_1,"
                + "b.FC_DESC_CATEGORY_1 Des_cat1_1 ,b.FC_DESC_CATEGORY_1 Des_cat2_1\n" +
                    "from portalunico.pul_nps_answer a,\n" +
                    "pul_nps_category_1 b\n" +
                    "where fn_id_answer ="+Integer.parseInt(idAnswer)+"\n" +
                    "and a.fc_id_category_1_1 = b.FN_ID_CATEGORY_1\n" +
                    "and a.FC_ID_CATEGORY_2_1 = b.FN_ID_CATEGORY_1";
        
        Query query = JPA.em().
                    createNativeQuery(statement);
     
        List<Object[]> result = query.getResultList();
        for( Object[] o : result ){
            String tmp = "";
           if(o[1] != null){
               tmp = o[1].toString();
           }
            if( o[0].toString().equals("201") ){
                ev.setAnswer1( tmp );
            }
            if( o[0].toString().equals("202") ){
                ev.setAnswer2( tmp );
            }
            if( o[0].toString().equals("203") ){
                ev.setAnswer1_1(tmp );
            }
            if( o[0].toString().equals("204") ){
                ev.setAnswer2_1(tmp );
            }
        }
        
        query = JPA.em().
                    createNativeQuery(statement2);
        result = query.getResultList();
        for( Object[] o : result ){
            ev.setCat_answer1_1( o[2].toString() );
            ev.setCat_answer2_1( o[3].toString() );
        }
        return ev;
    }
    
     public static String getTitleUserID(String userId){
        String statement = "select FC_TITLE\n" +
                        "from pul_structure\n" +
                        "where FN_USERID = :id";
        
        Query query = JPA.em().
                    createNativeQuery(statement);
        
        query.setParameter("id", Integer.parseInt(userId));
        return (String) query.getSingleResult();
    }
     
    public static int getNextValTracing(){
        int myId = 0;

        try {
            Connection conn = DbConnection.getDBConnection();
            String sqlIdentifier = "select PORTALUNICO.SQ_NPS_ID_FOLLOWUP.NEXTVAL from dual";
            PreparedStatement pst = conn.prepareStatement(sqlIdentifier);
            
            synchronized ( LOCK_1 ) {
                ResultSet rs = pst.executeQuery();
                if(rs.next())
                    myId = rs.getInt(1);
            }
        } catch (SQLException ex) {
            Logger.getLogger(EvaluationDetailsQuery.class.getName()).log(Level.SEVERE, null, ex);
        }
        return myId;
    }
    
    public static Long getNextValChallenge(){
        Long myId = 0l;

        try {
            Connection conn = DbConnection.getDBConnection();
            String sqlIdentifier = "select PORTALUNICO.SQ_NPS_ID_IMPUGN.NEXTVAL from dual";
            PreparedStatement pst = conn.prepareStatement(sqlIdentifier);
            
            synchronized ( LOCK_1 ) {
                ResultSet rs = pst.executeQuery();
                if(rs.next())
                    myId = rs.getLong(1);
            }
        } catch (SQLException ex) {
            Logger.getLogger(EvaluationDetailsQuery.class.getName()).log(Level.SEVERE, null, ex);
        }
        return myId;
    }
    
    public static Long getNextValFileLoad(){
        Long myId = 0l;

        try {
            Connection conn = DbConnection.getDBConnection();
            String sqlIdentifier = "select PORTALUNICO.SQ_FILE_LOAD.NEXTVAL from dual";
            PreparedStatement pst = conn.prepareStatement(sqlIdentifier);
            
            synchronized ( LOCK_1 ) {
                ResultSet rs = pst.executeQuery();
                if(rs.next())
                    myId = rs.getLong(1);
            }
        } catch (SQLException ex) {
            java.util.logging.Logger.getLogger(EvaluationDetailsQuery.class.getName()).log(Level.SEVERE, null, ex);
        }
        return myId;
    }
    
    public static ChallengeUtil getInfoChangeUserId(String empId){
        
        String query = 
           "{call PORTALUNICO.PR_GET_REASIGN_EMP (?, ?, ?, ?"
                                            + " ,? ,? ,? ,? ,?,?)}"; //agregar un ?
        
        CallableStatement call = null;
        Connection conn = null;
        ChallengeUtil util = new ChallengeUtil();
        try{
            conn = DbConnection.getDBConnection();
                                           
            if( conn != null ){
                call = conn.prepareCall(query);
                
                call.setString(1, empId );         //User id
                call.registerOutParameter(2, OracleTypes.INTEGER);
                call.registerOutParameter(3, OracleTypes.VARCHAR);
                call.registerOutParameter(4, OracleTypes.INTEGER);
                call.registerOutParameter(5, OracleTypes.INTEGER);
                call.registerOutParameter(6, OracleTypes.INTEGER);
                call.registerOutParameter(7, OracleTypes.INTEGER);
                call.registerOutParameter(8, OracleTypes.INTEGER);
                call.registerOutParameter(9, OracleTypes.INTEGER);
                call.registerOutParameter(10, OracleTypes.VARCHAR);
                
                call.execute();
                
                String error = call.getString(10);
                util.setError(error);
                
                if( error == null ){
                    util.setEmpId(Long.parseLong(empId));
                    util.setIdStore(call.getLong(2));
                    util.setZone(call.getString(3));
                    util.setUserId(call.getLong(4));
                    util.setChiefId(call.getLong(5));
                    util.setManagerId(call.getLong(6));
                    util.setDirectorId(call.getLong(7));
                    util.setUserZone(call.getLong(8));
                    util.setSection(call.getInt(9));
                }
                
                
            }
        } catch (SQLException ex) {
            Logger.getLogger(EvaluationDetailsQuery.class.getName()).log(Level.SEVERE, null, ex);
        }finally {
            if (call != null) {
                try {
                    call.close();
                } catch (SQLException ex) {
                    Logger.getLogger(EvaluationDetailsQuery.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
            if (conn != null) {
                try {
                    conn.close();
                } catch (SQLException ex) {
                    Logger.getLogger(EvaluationDetailsQuery.class.getName()).log(Level.SEVERE, null, ex);
                }
            }

	}
        return util;
    }
    
    public static boolean getRecordAudio(String filename) {
        String mediaPath = Play.configuration.getProperty("media.folder");
        String replace   = File.separator.equals("/") ? "/$" : "\\\\$";        
        String filePath  = mediaPath.replaceAll(replace, "") + File.separator ;
        
        File audioFile = new File(filePath + filename);

        if (audioFile.exists()) {
            return true;
        }
        
        return false;
    }
}