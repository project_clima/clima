package querys.nps;

import java.sql.CallableStatement;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import play.*;
import oracle.jdbc.OracleCallableStatement;
import oracle.jdbc.OracleConnection;
import oracle.jdbc.OracleTypes;
import static utils.Queries.getOracleConnection;

public class TrackingReportQuery {
    public static List getData(Integer userId, Integer groupId, Integer businessId,
                              String zone, String store, String manager, String startDate,
                              String endDate) {
        List resultsList = null;
        
        try {
            OracleConnection connection = getOracleConnection();
            
            String query = "BEGIN\n" +
                            "  PORTALUNICO.PR_GET_NPS_FOLLOWUP(\n" +
                            "    P_USERID => %s,\n" +
                            "    PN_GROUP_ID => %s,\n" +
                            "    PN_BUSINESS_ID => %s,\n" +
                            "    PC_ZONE => %s,\n" +
                            "    PL_STORE => PORTALUNICO.ARRAY_STRING_TABLE(%s),\n" +
                            "    PL_MANAGER => PORTALUNICO.ARRAY_STRING_TABLE(%s),\n" +
                            "    PD_INI_DATE => %s,\n" +
                            "    PD_END_DATE => %s,\n" +
                            "    CUR_NPS => ?\n" +
                            "  );\n" +
                            "END;";
            
            query = String.format(query,
                setIntParameter(userId),
                setIntParameter(groupId),
                setIntParameter(businessId),
                setStringParameter(zone),
                setArrayParameter(store),
                setArrayParameter(manager),
                setStringParameter(startDate),
                setStringParameter(endDate)
            );
            
            CallableStatement statement = connection.prepareCall(query);        
            
            statement.registerOutParameter(1, OracleTypes.CURSOR);
            statement.execute();
            
            ResultSet results = ((OracleCallableStatement) statement).getCursor(1);
            ResultSetMetaData metaData = results.getMetaData();
            int columnsCount = metaData.getColumnCount();
            
            Object[] data;
            resultsList = new ArrayList();
            
            while (results.next()) {
                data = new Object[columnsCount];
                
                for (int i = 1; i <= columnsCount; i++) {
                    data[i - 1] = results.getObject(i);
                }
                
                resultsList.add(data);
            }
            
            statement.close();
            results.close();
        } catch (SQLException e) {
            Logger.error(e.getMessage());
        }
        
        return resultsList;
    }
    
    public static HashMap<String, List> getBusinessDetail(Integer userId, Integer groupId, Integer businessId,
                                            String startDate, String endDate) {
        HashMap<String, List> detail = new HashMap();
        
        try {
            OracleConnection connection = getOracleConnection();
            
            String query = "BEGIN\n" +
                            "  PORTALUNICO.PR_GET_NPS_FOLLOWUP_DET(\n" +
                            "    P_USERID => %s,\n" +
                            "    PN_GROUP_ID => %s,\n" +
                            "    PN_BUSINESS_ID => %s,\n" +
                            "    PD_INI_DATE => %s,\n" +
                            "    PD_END_DATE => %s,\n" +
                            "    CUR_STORE => ?,\n" +
                            "    CUR_MANAGER => ?\n" +
                            "  );\n" +
                            "END;";
            
            query = String.format(query,
                setIntParameter(userId),
                setIntParameter(groupId),
                setIntParameter(businessId),
                setStringParameter(startDate),
                setStringParameter(endDate)
            );
            
            CallableStatement statement = connection.prepareCall(query);        
            
            statement.registerOutParameter(1, OracleTypes.CURSOR);
            statement.registerOutParameter(2, OracleTypes.CURSOR);
            statement.execute();
            
            ResultSet stores = ((OracleCallableStatement) statement).getCursor(1);
            ResultSet managers = ((OracleCallableStatement) statement).getCursor(2);
            
            setDetail(detail, "stores", stores);
            setDetail(detail, "managers", managers);
            
            statement.close();
            
            stores.close();
            managers.close();
        } catch (SQLException e) {
            Logger.error(e.getMessage());
        }
        
        return detail;
    }
    
    public static void setDetail(HashMap detail, String name, ResultSet results) throws SQLException {
        ResultSetMetaData metaData = results.getMetaData();
        int columnsCount = metaData.getColumnCount();
            
        List data = new ArrayList();
        
        Object[] row;

        while (results.next()) {
            row = new Object[columnsCount];
            
            for (int i = 1; i <= columnsCount; i++) {
                row[i - 1] = results.getObject(i);
            }

            data.add(row);
        }
        
        detail.put(name, data);
    }
    
    public static String setIntParameter(Integer value) {
        if (value == null) {
            return "null";
        }
        
        return Integer.toString(value);
    }
    
    public static String setStringParameter(String value) {
        if (value == null || value.isEmpty()) {
            return "null";
        }
        
        return "'" + value + "'";
    }
    
    public static String setArrayParameter(String value) {
        if (value == null) {
            return "";
        }
        
        return "'" + value + "'";
    }
}
