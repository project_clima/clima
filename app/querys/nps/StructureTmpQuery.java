/**
 * @(#) StructureTmpQuery 21/03/2017
 */

package querys.nps;

import controllers.csi.StructureCSI;
import static controllers.csi.StructureCSI.getOracleConnection;
import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Types;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.persistence.Query;
import oracle.jdbc.OracleCallableStatement;
import oracle.jdbc.OracleConnection;
import oracle.jdbc.OracleTypes;
import org.hibernate.internal.SessionImpl;
import play.db.jpa.JPA;
import utils.nps.vars.EnvironmentVar;
import utils.nps.connectionDB.DbConnection;

/**
 *
 * @author Rodolfo Miranda -- Qualtop
 */
public class StructureTmpQuery {
    
    private static final StructureTmpQuery LOCK_1 = new StructureTmpQuery() {};

    public static Integer truncateTableRH(){
        Integer result = 0;
        OracleConnection connection = null;        
        CallableStatement statement = null;

        try {
            connection = getOracleConnection();
            statement = connection.prepareCall("{ ? = call PORTALUNICO.FN_DEL_STRUCTURE_TMP() }");

            statement.registerOutParameter(1, OracleTypes.INTEGER);
            statement.execute();

            result = ((OracleCallableStatement) statement).getInt(1);

        } catch (SQLException e) {
            Logger.getLogger(StructureCSI.class.getName()).log(Level.SEVERE, null, e);
        } finally {
            try {
                if (statement != null && !statement.isClosed()) {
                    statement.close();
                }
            } catch (SQLException e) {
                Logger.getLogger(StructureCSI.class.getName()).log(Level.SEVERE, null, e);
            }
        }
        return result;
    }
    
    public static Integer truncateTableAns(){
        Integer result = 0;
        OracleConnection connection = null;        
        CallableStatement statement = null;

        try {
            connection = getOracleConnection();
            statement = connection.prepareCall("{ ? = call PORTALUNICO.FN_DEL_ANSWER_TMP() }");

            statement.registerOutParameter(1, OracleTypes.INTEGER);
            statement.execute();

            result = ((OracleCallableStatement) statement).getInt(1);

        } catch (SQLException e) {
            Logger.getLogger(StructureCSI.class.getName()).log(Level.SEVERE, null, e);
        } finally {
            try {
                if (statement != null && !statement.isClosed()) {
                    statement.close();
                }
            } catch (SQLException e) {
                Logger.getLogger(StructureCSI.class.getName()).log(Level.SEVERE, null, e);
            }
        }
        return result;
    }
    
    public static String getUser(){
        String statement = "select SYS_CONTEXT( 'USERENV', 'CURRENT_USER' "
                            + " ) from dual";
        Query query = JPA.em().
                    createNativeQuery(statement);
        String user = (String) query.getSingleResult();
        
        return user;
    }
    
    public static int callStoreProcedureLoadStructure(int ID_FILE, 
            String operation, String fill) throws SQLException{
        int error = 0;
        CallableStatement callableStatement = null;
       
            OracleConnection oracleConnection = null;
            SessionImpl sessionImpl = ((SessionImpl) JPA.em().getDelegate());
            Connection connection = sessionImpl.connection();
            if (connection.isWrapperFor(OracleConnection.class)){
                oracleConnection= connection.unwrap(OracleConnection.class);  
            }else{
                // recover, not an oracle connection
            }
            if(oracleConnection != null){
                if(operation.equals(EnvironmentVar.INS_NPS_STRUCT)){

                    callableStatement = oracleConnection.
                            prepareCall("{ ? = call PORTALUNICO."
                                    + "PK_CARGA_ESTRUCTURA."+fill+"(?, ?) }");
                    callableStatement.registerOutParameter(1, Types.INTEGER);
                    callableStatement.setInt(2, ID_FILE);
                    callableStatement.setString(3, EnvironmentVar.DB_PLSQL);


                    callableStatement.execute();

                    error = callableStatement.getInt(1);


                }else if(operation.equals(EnvironmentVar.INS_NPS_ANSWER)){
                    callableStatement = oracleConnection.
                            prepareCall("{ ? = call PORTALUNICO."
                                    + "PK_CARGA_ESTRUCTURA.FN_INS_NPS_ANSWER(?, ?) }");
                    callableStatement.registerOutParameter(1, Types.INTEGER);
                    callableStatement.setInt(2, ID_FILE);
                    callableStatement.setString(3, EnvironmentVar.DB_PLSQL);


                    callableStatement.execute();

                    error = callableStatement.getInt(1);
                }
                if (callableStatement != null) {
                    callableStatement.close();
                }
            }else{

            }        
        return error;
    }
    
    public static int getNextValChallenge(){
        int myId = 0;

        try {
            Connection conn = DbConnection.getDBConnection();
            String sqlIdentifier = "select PORTALUNICO.SQ_NPS_ID_ANWER_TMP.NEXTVAL from dual";
            PreparedStatement pst = conn.prepareStatement(sqlIdentifier);
            
            synchronized ( LOCK_1 ) {
                ResultSet rs = pst.executeQuery();
                if(rs.next())
                    myId = rs.getInt(1);
            }
        } catch (SQLException ex) {
            Logger.getLogger(EvaluationDetailsQuery.class.getName()).log(Level.SEVERE, null, ex);
        }
        return myId;
    }
}
