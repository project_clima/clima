/**
 * @(#) NpsHomeQuery 30/03/2017
 */
package querys.nps;

import java.sql.Array;
import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Types;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.persistence.Query;
import oracle.jdbc.OracleConnection;
import oracle.jdbc.OracleTypes;
import play.db.jpa.JPA;
import utils.nps.model.Area;
import utils.nps.model.Business;
import utils.nps.model.Chief;
import utils.nps.model.Group;
import utils.nps.model.Manager;
import utils.nps.model.Section;
import utils.nps.model.Seller;
import utils.nps.model.Store;
import utils.nps.model.TopNps;
import utils.nps.model.UtilFolloupModel;
import utils.nps.model.Zone;

/**
 *
 * @author Rodolfo Miranda -- Qualtop
 */
public class NpsHomeQuery {

    public static List<String> getZonesId(String idUser) {

        String statement
                = "select distinct  fc_sub_div \n"
                + "from  pul_Store a\n"
                + "where a.fc_sub_div not in ('BT','CM','CO','AD','BD')\n"
                + "  and a.fn_id_store in "
                + "(select b.fn_id_store\n"
                + " from pul_structure b\n"
                + " where b.fn_userid = nvl(" + idUser + " ,b.fn_userid)\n"
                + " )";

        List<String> idZones = new ArrayList<>();

        Query query = JPA.em().
                createNativeQuery(statement);

        idZones = (List<String>) query.getResultList();
        return idZones;
    }

    public static List<Store> getStores(String userId, String idZone) {
        List<Store> stores = new ArrayList<>();
        String statement = "select FC_SUB_DIV, FN_ID_STORE, FC_DESC_STORE\n"
                + "  from  pul_Store a\n"
                + " where a.fc_sub_div not in ('BT','CM','CO','AD','BD')\n"
                + "   and a.fc_sub_div = '" + idZone + "' \n"
                + "   and a.fn_id_store in (select b.fn_id_store\n"
                + " from pul_structure b\n"
                + " where b.fn_userid = nvl(" + userId + " ,b.fn_userid)\n"
                + " )";

        Query query = JPA.em().
                createNativeQuery(statement);

        List<Object[]> result = query.getResultList();
        for (Object[] o : result) {
            Store store = new Store();
            store.setId(o[1].toString());
            store.setDesc(o[2].toString());
            store.setIdParent(o[0].toString());
            stores.add(store);
        }

        //stores = (List<Store>) query.getResultList();
        return stores;
    }

    public static float[] getNpsCompany(String userId, String firstDate,
            String lastDate, String[] strs, String[] mngs, String[] chfs,
            String[] sctns, int op, String zone, String group, String business,
            Connection oracleConnection)
            throws SQLException {
        float results[] = new float[4];
        CallableStatement callableStatement = null;

        float pTotal = 0f, pNeutro = 0f, pDetra = 0f, pPromp = 0f;
        try {

            Array array1 = ((OracleConnection) oracleConnection).
                    createOracleArray("PORTALUNICO.ARRAY_STRING_TABLE", strs);

            Array array2 = ((OracleConnection) oracleConnection).
                    createOracleArray("PORTALUNICO.ARRAY_STRING_TABLE", mngs);

            Array array3 = ((OracleConnection) oracleConnection).
                    createOracleArray("PORTALUNICO.ARRAY_STRING_TABLE", chfs);

            Array array4 = ((OracleConnection) oracleConnection).
                    createOracleArray("PORTALUNICO.ARRAY_STRING_TABLE", sctns);
            if (oracleConnection != null) {
                callableStatement = oracleConnection.
                        prepareCall("call PORTALUNICO."
                                + "pk_nps_home.pr_get_nps(?, ?,?,?,?, ?, ?, ?, "
                                + "?, ?, ?, ?, ?,?,?) ");
                callableStatement.setString(1, userId); //userid
                callableStatement.setString(2, group);      //group
                callableStatement.setString(3, business);      //business
                callableStatement.setString(4, zone);   //zone
                callableStatement.setArray(5, array1);  //store
                callableStatement.setArray(6, array2);  //manager
                callableStatement.setArray(7, array3);  //chief
                callableStatement.setArray(8, array4);  //section
                callableStatement.setString(9, firstDate);
                callableStatement.setString(10, lastDate);
                callableStatement.setInt(11, op);
                callableStatement.registerOutParameter(12, Types.FLOAT);
                callableStatement.registerOutParameter(13, Types.FLOAT);
                callableStatement.registerOutParameter(14, Types.FLOAT);
                callableStatement.registerOutParameter(15, Types.FLOAT);

                callableStatement.execute();

                pPromp = callableStatement.getFloat(12);
                pNeutro = callableStatement.getFloat(13);
                pDetra = callableStatement.getFloat(14);
                pTotal = callableStatement.getFloat(15);

                results[3] = pTotal;
                results[0] = pPromp;
                results[1] = pNeutro;
                results[2] = pDetra;

                callableStatement.close();
            }

        } catch (SQLException ex) {
            Logger.getLogger(NpsHomeQuery.class.getName()).log(Level.SEVERE,
                    null, ex);
        } finally {
            if (callableStatement != null) {
                callableStatement.close();
            }
        }
        return results;
    }

    public static List<Zone> getZoneHome(String business, String group, String
            userId)
            throws SQLException {
        List<Zone> zones = new ArrayList<>();
        //List<String> zonesArray = NpsHomeQuery.getZonesId(userId);
        String statement = "SELECT answ.FC_ZONE, \n" +
            "       answ.FN_ID_STORE, \n" +
            "       --(select str.FC_DESC_STORE from pul_store str where answ.fn_id_store = str.fn_id_store) FC_DESC_STORE\n" +
            "       str.FC_DESC_STORE\n" +
            "FROM   portalunico.PUL_NPS_ANSWER answ\n" +
            "JOIN   portalunico.PUL_STORE str on  answ.fn_id_store = str.fn_id_store\n" +
            "WHERE    answ.FN_STORE_BUSINESS_ID = "+business+"\n" +
            "AND      answ.FN_STORE_GROUP_ID = "+group+"\n" +
            "AND ((answ.FN_USERID          = "+userId+" OR "+userId+" IS NULL) \n" +
                " OR  (answ.FN_USERID_CHIEF    = "+ userId+" OR "+ userId+" IS NULL)\n" +
                " OR  (answ.FN_USERID_MANAGER  = "+ userId+" OR "+ userId+" IS NULL) \n" +
                " OR  (answ.FN_USERID_DIRECTOR = "+ userId+" OR "+ userId+" IS NULL)\n" +
                " OR  (answ.FN_USERID_ZONE     = "+ userId+" OR "+ userId+" IS NULL) )" +
            "GROUP BY answ.FC_ZONE,answ.FN_ID_STORE, str.FC_DESC_STORE\n" +
            "HAVING   answ.FC_ZONE IS NOT NULL\n" +
            "order by answ.FC_ZONE, answ.FN_ID_STORE";

        Query query = JPA.em().
                createNativeQuery(statement);
        List<Object[]> result = query.getResultList();

        if (result != null && !result.isEmpty()) {
            String idZone = "";
            String id;
            Zone g2 = null;
            for (Object[] o : result) {
                id = o[0] != null ? o[0].toString() : "";
                if (!idZone.equals(id)) {
                    Zone g = new Zone();
                    g2 = g;
                    idZone = o[0] != null ? o[0].toString() : "";
                    g.setId(o[0] != null ? o[0].toString() : "");
                    g.setDesc((o[0] != null ? o[0].toString() : ""));
                    Store store = new Store();
                    List<Store> stores = new ArrayList<>();
                    store.setId(o[1] != null ? o[1].toString() : "");
                    store.setDesc(o[2] != null ? o[2].toString() : "");
                    store.setIdParent(g.getId());
                    stores.add(store);
                    g.setStores(stores);
                    zones.add(g);
                } else {
                    Store store = new Store();
                    store.setId(o[1] != null ? o[1].toString() : "");
                    store.setDesc(o[2] != null ? o[2].toString() : "");
                    store.setIdParent(g2.getId());
                    g2.getStores().add(store);
                }
            }
        }
        return zones;
    }

    public static Float[] getNpsMonths(String userId, String firstDate,
            String lastDate, String[] strs, String[] mngs, String[] chfs,
            String[] sctns, int op, String zone, String group, String business,
            Connection oracleConnection)
            throws SQLException {
        Float month[] = new Float[12];

        CallableStatement callableStatement = null;
        ResultSet rs = null;
        try {
            Array array1 = ((OracleConnection) oracleConnection).
                    createOracleArray("PORTALUNICO.ARRAY_STRING_TABLE", strs);

            Array array2 = ((OracleConnection) oracleConnection).
                    createOracleArray("PORTALUNICO.ARRAY_STRING_TABLE", mngs);
            Array array3 = ((OracleConnection) oracleConnection).
                    createOracleArray("PORTALUNICO.ARRAY_STRING_TABLE", chfs);

            Array array4 = ((OracleConnection) oracleConnection).
                    createOracleArray("PORTALUNICO.ARRAY_STRING_TABLE", sctns);

            if (oracleConnection != null) {
                callableStatement = oracleConnection.
                        prepareCall("call PORTALUNICO.pk_nps_home."
                                + "pr_get_nps_mes(?, ?, ?, ?, ?,"
                                + " ?, ?, ?, ?, ?,?,?) ");

                callableStatement.setString(1, userId);  // userId
                callableStatement.setString(2, group);  // userId
                callableStatement.setString(3, business);  // userId
                callableStatement.setString(4, zone);  // userId
                callableStatement.setArray(5, array1);  // idZone
                callableStatement.setArray(6, array2);  // idStore
                callableStatement.setArray(7, array3);  // idZone
                callableStatement.setArray(8, array4);  // idStore
                callableStatement.setString(9, firstDate);  // date in
                callableStatement.setString(10, lastDate);  // date out
                callableStatement.setInt(11, op);  // userId
                callableStatement.registerOutParameter(12, OracleTypes.CURSOR);

                callableStatement.execute();

                rs = (ResultSet) callableStatement.getObject(12);

                while (rs.next()) {
                    month[rs.getInt(1) - 1] = rs.getFloat(2);
                }
            }
        } catch (SQLException ex) {
            Logger.getLogger(NpsHomeQuery.class.getName()).log(Level.SEVERE,
                    null, ex);
        } finally {
            if (callableStatement != null) {
                callableStatement.close();
            }
            if (rs != null) {
                rs.close();
            }
        }

        return month;
    }

    public static Float[] getNpsTriMonths(String userId, String firstDate,
            String lastDate, String[] strs, String[] mngs, String[] chfs,
            String[] sctns, int op, String zone, String group, String business,
            Connection oracleConnection)
            throws SQLException {
        Float month[] = new Float[4];

        CallableStatement callableStatement = null;

        try {

            Array array1 = ((OracleConnection) oracleConnection).
                    createOracleArray("PORTALUNICO.ARRAY_STRING_TABLE", strs);

            Array array2 = ((OracleConnection) oracleConnection).
                    createOracleArray("PORTALUNICO.ARRAY_STRING_TABLE", mngs);
            Array array3 = ((OracleConnection) oracleConnection).
                    createOracleArray("PORTALUNICO.ARRAY_STRING_TABLE", chfs);

            Array array4 = ((OracleConnection) oracleConnection).
                    createOracleArray("PORTALUNICO.ARRAY_STRING_TABLE", sctns);

            if (oracleConnection != null) {
                callableStatement = oracleConnection.
                        prepareCall("call PORTALUNICO.pk_nps_home."
                                + "PR_GET_NPS_TRIMESTRE(?, ?, ?, ?, ?,"
                                + " ?, ?, ?, ?, ?, ?, ?) ");

                callableStatement.setString(1, userId);  // userId
                callableStatement.setString(2, group);  // userId
                callableStatement.setString(3, business);  // userId
                callableStatement.setString(4, zone);  // userId
                callableStatement.setArray(5, array1);  // idZone
                callableStatement.setArray(6, array2);  // idStore
                callableStatement.setArray(7, array3);  // idZone
                callableStatement.setArray(8, array4);  // idStore
                callableStatement.setString(9, firstDate);  // date in
                callableStatement.setString(10, lastDate);  // date out
                callableStatement.setInt(11, op);  // userId
                callableStatement.registerOutParameter(12, OracleTypes.CURSOR);

                callableStatement.execute();

                ResultSet rs = (ResultSet) callableStatement.getObject(12);

                while (rs.next()) {
                    if(rs.getString(2) != null)
                        month[rs.getInt(1) - 1] = rs.getFloat(2);
                }
                
                rs.close();
            }
        } catch (SQLException ex) {
            Logger.getLogger(NpsHomeQuery.class.getName()).log(Level.SEVERE,
                    null, ex);
        } finally {
            if (callableStatement != null) {
                callableStatement.close();
            }
        }

        return month;
    }

    public static List<Area> getNpsGlobalArea(String userId, String firstDate,
            String lastDate, String[] strs, String[] mngs, String[] chfs,
            String[] sctns, int op, String zone, String group, String business,
            Connection oracleConnection)
            throws SQLException {

        List<Area> zareas = new ArrayList<>();

        CallableStatement callableStatement = null;

        try {

            Array array1 = ((OracleConnection) oracleConnection).
                    createOracleArray("PORTALUNICO.ARRAY_STRING_TABLE", strs);

            Array array2 = ((OracleConnection) oracleConnection).
                    createOracleArray("PORTALUNICO.ARRAY_STRING_TABLE", mngs);
            Array array3 = ((OracleConnection) oracleConnection).
                    createOracleArray("PORTALUNICO.ARRAY_STRING_TABLE", chfs);

            Array array4 = ((OracleConnection) oracleConnection).
                    createOracleArray("PORTALUNICO.ARRAY_STRING_TABLE", sctns);
            if (oracleConnection != null) {
                callableStatement = oracleConnection.
                        prepareCall("call PORTALUNICO.pk_nps_home."
                                + "pr_get_nps_ranking(?, ?, ?, ?, ?,"
                                + "?, ?, ?, ?, ?, ?, ?) ");

                callableStatement.setString(1, userId);  // userId
                callableStatement.setString(2, group);  // userId
                callableStatement.setString(3, business);  // userId
                callableStatement.setString(4, zone);  // idZone
                callableStatement.setArray(5, array1);  // idStore
                callableStatement.setArray(6, array2);  // idStore
                callableStatement.setArray(7, array3);  // idStore
                callableStatement.setArray(8, array4);  // idStore
                callableStatement.setString(9, firstDate);  // date in
                callableStatement.setString(10, lastDate);  // date out
                callableStatement.setInt(11, op);  // userId
                callableStatement.registerOutParameter(12, OracleTypes.CURSOR);

                callableStatement.execute();

                ResultSet rs = (ResultSet) callableStatement.getObject(12);

                while (rs.next()) {
                    Area zArea = new Area();
//                   zArea.setIdZone(rs.getString(1));
                    zArea.setId(rs.getString(1));
                    zArea.setNps(rs.getFloat(3));
                    zArea.setDesc(rs.getString(2));
//                    zArea.setAreas(areas);
                    zareas.add(zArea);
                }
                
                rs.close();
            }
        } catch (SQLException ex) {
            Logger.getLogger(NpsHomeQuery.class.getName()).log(Level.SEVERE,
                    null, ex);
        } finally {
            if (callableStatement != null) {
                callableStatement.close();
            }
        }

        return zareas;
    }

    public static List<Zone> getNpsArea(String userId, String firstDate,
            String lastDate, String[] strs, String[] mngs, String[] chfs,
            String[] sctns, int op, String zone, String group, String business,
            Connection oracleConnection)
            throws SQLException {
        List<Zone> zareas = new ArrayList<>();

        CallableStatement callableStatement = null;

        try {

            Array array1 = ((OracleConnection) oracleConnection).
                    createOracleArray("PORTALUNICO.ARRAY_STRING_TABLE", strs);

            Array array2 = ((OracleConnection) oracleConnection).
                    createOracleArray("PORTALUNICO.ARRAY_STRING_TABLE", mngs);
            Array array3 = ((OracleConnection) oracleConnection).
                    createOracleArray("PORTALUNICO.ARRAY_STRING_TABLE", chfs);

            Array array4 = ((OracleConnection) oracleConnection).
                    createOracleArray("PORTALUNICO.ARRAY_STRING_TABLE", sctns);
            if (oracleConnection != null) {
                callableStatement = oracleConnection.
                        prepareCall("call PORTALUNICO.pk_nps_home."
                                + "pr_get_nps_zone_area(?, ?, ?, ?, ?,"
                                + "?, ?, ?, ?, ?, ?, ?, ?, ?) ");

                callableStatement.setString(1, userId);  // userId
                callableStatement.setString(2, group);  // userId
                callableStatement.setString(3, business);  // userId
                callableStatement.setString(4, zone);  // idZone
                callableStatement.setArray(5, array1);  // idStore
                callableStatement.setArray(6, array2);  // idStore
                callableStatement.setArray(7, array3);  // idStore
                callableStatement.setArray(8, array4);  // idStore
                callableStatement.setString(9, firstDate);  // date in
                callableStatement.setString(10, lastDate);  // date out
                callableStatement.setInt(11, op);  // userId
                callableStatement.registerOutParameter(12, OracleTypes.CURSOR);
                callableStatement.registerOutParameter(13, OracleTypes.CURSOR);
                callableStatement.registerOutParameter(14, OracleTypes.CURSOR);

                callableStatement.execute();

                ResultSet rs = (ResultSet) callableStatement.getObject(12);

                while (rs.next()) {
                    Zone zArea = new Zone();
                    List<Area> areas = new ArrayList<>();
                    zArea.setId(rs.getString(1));
                    zArea.setNps(rs.getFloat(2));
                    zArea.setAreas(areas);
                    zareas.add(zArea);
                }

                rs = (ResultSet) callableStatement.getObject(13);
                
                while (rs.next()) {
                    Zone z = containsZone(zareas,rs.getString(1));
                    if( z != null ) {
                        z.setNpsP(rs.getFloat(2));
                    }
                }
                
                rs = (ResultSet) callableStatement.getObject(14);
                while (rs.next()) {
                    Zone zArea = containsArea(zareas, rs.getString(1));
                    Area area = new Area();
                    area.setIdZone(rs.getString(1));
                    area.setId(rs.getString(2));
                    area.setDesc(rs.getString(3));
                    area.setNps(rs.getFloat(4));
                    zArea.getAreas().add(area);
                }
                
                rs.close();
            }
        } catch (SQLException ex) {
            Logger.getLogger(NpsHomeQuery.class.getName()).log(Level.SEVERE,
                    null, ex);
        } finally {
            if (callableStatement != null) {
                callableStatement.close();
            }
        }

        return zareas;
    }

    public static TopNps getNpsTop(String userId, String firstDate,
            String lastDate, String order, int op, String zone, String group,
            String business, Connection oracleConnection) throws SQLException {

        TopNps top = new TopNps();
        List<Store> stores = new ArrayList<>();
        List<Manager> manager = new ArrayList<>();
        List<Section> sections = new ArrayList<>();
        CallableStatement callableStatement = null;

        try {

            if (oracleConnection != null) {
                callableStatement = oracleConnection.
                        prepareCall("call PORTALUNICO.pk_nps_home."
                                + "PR_GET_NPS_TOP(?, ?, ?, ?,"
                                + "?, ?, ?, ?, ?, ?) ");

                callableStatement.setString(1, userId);  // userId
                callableStatement.setString(2, group);  // idStore
                callableStatement.setString(3, business);  // idStore
                callableStatement.setString(4, firstDate);  // date in
                callableStatement.setString(5, lastDate);  // idStore
                callableStatement.setInt(6, op);  // date in
                callableStatement.setString(7, order);  // idStore

                callableStatement.registerOutParameter(8, OracleTypes.CURSOR);
                callableStatement.registerOutParameter(9, OracleTypes.CURSOR);
                callableStatement.registerOutParameter(10, OracleTypes.CURSOR);
                
                callableStatement.execute();

                ResultSet rs1 = (ResultSet) callableStatement.getObject(8);
                ResultSet rs2 = (ResultSet) callableStatement.getObject(9);
                ResultSet rs3 = (ResultSet) callableStatement.getObject(10);
                
                while (rs1.next()) {
                    Store store = new Store();
                    store.setId(rs1.getString(1));
                    store.setDesc(rs1.getString(2) != null ? rs1.getString(2) : "");
                    store.setNps(Float.toString(rs1.getFloat(3)));
                    stores.add(store);
                }
                rs1.close();
                
                                
                while (rs3.next()) {
                    Section section = new Section();
                    section.setId(rs3.getString(1));
                    section.setDesc(rs3.getString(2) != null ? rs3.getString(2) : "");
                    section.setNps(Float.toString(rs3.getFloat(3)));
                    sections.add(section);
                }
                rs3.close();
                                
                while (rs2.next()) {
                    Manager man = new Manager();
                    man.setId(rs2.getString(1));
                    man.setDesc(rs2.getString(2) != null ? rs2.getString(2) : "");
                    man.setNps(Float.toString(rs2.getFloat(3)));
                    manager.add(man);
                }
                rs2.close();
            }
            top.setStores(stores);
            top.setSections(sections);
            top.setManagers(manager);
        } catch (SQLException ex) {
            Logger.getLogger(NpsHomeQuery.class.getName()).log(Level.SEVERE,
                    null, ex);
        } finally {
            if (callableStatement != null) {
                callableStatement.close();
            }
        }

        return top;
    }

    public static String[] getNpsGoal(String userId, String firstDate,
            String lastDate, int goal, int op, String zone, String group,
            String business, Connection oracleConnection) throws SQLException {
        TopNps top = new TopNps();
        String[] goals = new String[4];
        CallableStatement callableStatement = null;

        try {

            if (oracleConnection != null) {
                callableStatement = oracleConnection.
                        prepareCall("call PORTALUNICO.pk_nps_home."
                                + "PR_GET_NPS_GOAL(?, ?, ?, ?,"
                                + "?, ?, ?, ?, ?, ?, ?) ");

                callableStatement.setString(1, userId);  // userId
                callableStatement.setString(2, group);  // idStore
                callableStatement.setString(3, business);  // idStore
                callableStatement.setString(4, firstDate);  // date in
                callableStatement.setString(5, lastDate);  // idStore
                callableStatement.setInt(6, op);  // date in
                callableStatement.setInt(7, goal);  // idStore

                callableStatement.registerOutParameter(8, Types.FLOAT);
                callableStatement.registerOutParameter(9, Types.FLOAT);
                callableStatement.registerOutParameter(10, Types.FLOAT);
                callableStatement.registerOutParameter(11, Types.FLOAT);

                callableStatement.execute();

                goals[0] = Float.toString(callableStatement.getFloat(8));
                goals[1] = Integer.toString(callableStatement.getInt(9));
                goals[2] = Float.toString(callableStatement.getFloat(10));
                goals[3] = Integer.toString(callableStatement.getInt(11));
            }
        } catch (SQLException ex) {
            Logger.getLogger(NpsHomeQuery.class.getName()).log(Level.SEVERE,
                    null, ex);
        } finally {
            if (callableStatement != null) {
                callableStatement.close();
            }
        }

        return goals;
    }

    public static List<Manager> getManagers(String userId, String idStore) {
        List<Manager> managers = new ArrayList<>();

        String statement
                = "SELECT distinct answ.fn_userid_manager,\n" +
                  "(decode(fn_userid_manager,'7777777777','Gerente dummy', "
                + "nvl((select fc_firstname||' '||fc_lastname from pul_structure "
                + "         where fn_userid = answ.fn_userid_manager),"
                + "'Nombre no encontrado'))) nombre, \n" +
                  "(decode(fn_userid_manager,'7777777777','Gerente dummy', "
                + "nvl((select fc_title from pul_structure "
                + "where fn_userid = answ.fn_userid_manager),'Puesto no encontrado')))title\n" +
                "FROM   pul_nps_answer answ\n" +
                "WHERE  answ.fn_id_store ="+ Integer.parseInt(idStore)+"\n" +
                "AND ((answ.FN_USERID          = "+ userId+" OR "+ userId+" IS NULL) \n" +
                " OR  (answ.FN_USERID_CHIEF    = "+ userId+" OR "+ userId+" IS NULL)\n" +
                " OR  (answ.FN_USERID_MANAGER  = "+ userId+" OR "+ userId+" IS NULL) \n" +
                " OR  (answ.FN_USERID_DIRECTOR = "+ userId+" OR "+ userId+" IS NULL)\n" +
                " OR  (answ.FN_USERID_ZONE     = "+ userId+" OR "+ userId+" IS NULL) )" ;

        Query query = JPA.em().
                createNativeQuery(statement);

        List<Object[]> result = query.getResultList();
        for (Object[] o : result) {
            Manager man = new Manager();

            man.setId(o[0].toString());
            man.setTitle(o[2].toString());
            man.setDesc(o[1].toString());

            managers.add(man);
        }

        return managers;
    }

    public static List<Chief> getChiefs(String userId, String idStore) {
        List<Chief> chiefs = new ArrayList<>();

        String statement
                = "select distinct answ.fn_userid_chief, \n"
                + " (decode(fn_userid_chief,'6666666666','Jefe dummy',\n"
                + "     nvl((select fc_firstname||' '||fc_lastname \n"
                + "     from    pul_structure \n"
                + "     where   fn_userid = answ.fn_userid_chief),'Nombre no encontrado') \n"
                + "  )) nombre, \n"
                + "  (decode(fn_userid_chief,'6666666666','Jefe dummy',\n"
                + "     nvl((select fc_title \n"
                + "     from pul_structure \n"
                + "     where fn_userid = answ.fn_userid_chief),'Puesto no encontrado'\n"
                + "  ))) title\n"
                + "from   pul_nps_answer answ\n"
                + "where  answ.FN_USERID_MANAGER = " + Long.parseLong(userId) + " and \n"
                + "answ.fn_id_store = " + Integer.parseInt(idStore);

        Query query = JPA.em().
                createNativeQuery(statement);

        List<Object[]> result = query.getResultList();
        for (Object[] o : result) {
            Chief man = new Chief();

            man.setId(o[0].toString());
            man.setDesc(o[2].toString());
            man.setTitle(o[1].toString());

            chiefs.add(man);
        }

        return chiefs;
    }

    public static String getFullNameUserID(String userId) {
        String statement = " select s.FC_FIRSTNAME || ' ' || s.FC_LASTNAME \n" +
                           " from pul_structure s\n" +
                           " where FN_USERID = :id";

        Query query = JPA.em().
                createNativeQuery(statement);

        query.setParameter("id", Integer.parseInt(userId));
        
        String name = "";
        List results = query.getResultList();
        
        if (!results.isEmpty())
           //newCall = (T03CallsLog) results.get(0);
            name = (String) results.get(0);
        else
            name = "";
        
        
        return name;
    }

    public static String getTitleUserID(String userId) {
        String statement = "select FC_TITLE\n"
                + "from pul_structure\n"
                + "where FN_USERID = :id";

        Query query = JPA.em().
                createNativeQuery(statement);

        query.setParameter("id", Integer.parseInt(userId));
        String title = ""; //(String) query.getSingleResult();
        
        List results = query.getResultList();
        
        if (!results.isEmpty())
           //newCall = (T03CallsLog) results.get(0);
            title = (String) results.get(0);
        else
            title = "";
        
        return title;
    }

    public static List<String> getZones() {
        String statement = "select distinct(fc_sub_div)\n"
                + "from pul_store\n"
                + "where fc_sub_div not in ('BT','CM','CO','AD','BD')" //"union \n" +
                //"Select 'Duty Free'\n" +
                //"from dual"
                ;

        Query query = JPA.em().
                createNativeQuery(statement);

        List<String> zones = (List<String>) query.getResultList();
        return zones;
    }

    public static List<UtilFolloupModel> getTracings(String stores, String zones,
            String firstDate, String lastDate) {
        List<UtilFolloupModel> tracings = new ArrayList<>();
        DecimalFormat df = new DecimalFormat("#.##");
        //df.setMinimumFractionDigits(2);
        String statement2 = "";

        String statement = "select answ.FC_ZONE as zone,\n"
                + "  count(case when TO_NUMBER(quest.FC_TEXT_ANSWER) >= 9"
                + " then 1 end) as Promotoras, \n"
                + "  count(case when TO_NUMBER(quest.FC_TEXT_ANSWER) < 9 then"
                + " 1 end) as Detractoras_pasivas,\n"
                + "  count(case when TO_NUMBER(quest.FC_TEXT_ANSWER) >= 0 "
                + "then 1 end) as Respuestas,\n"
                + "  count(case when lower(answ.FC_TITLE) like ('gerente%') "
                + "then 1 end) / 2 as Gerentes,\n"
                + "  count(case when follow.FN_ID_FOLLOWUP_STATUS = 2 then 1 end)"
                + " / 2 as Seguimiento,\n"
                + " count(case when follow.FN_ID_FOLLOWUP_STATUS is null and "
                + "TO_NUMBER(quest.FC_TEXT_ANSWER) < 9 then 1 end) "
                + "as Sin_Seguimiento,\n"
                + " count(case when follow.FN_ID_FOLLOWUP_STATUS = 3 then 1 end)"
                + " / 2 as Cerrado \n"
                + " from PORTALUNICO.PUL_NPS_ANSWER answ\n"
                + " inner join PORTALUNICO.PUL_NPS_QUESTION_ANSWER quest "
                + "on answ.FN_ID_ANSWER = quest.FN_ID_ANSWER \n"
                + " full outer join PORTALUNICO.PUL_NPS_FOLLOWUP follow "
                + "on answ.FN_ID_ANSWER = follow.FN_ID_ANSWER\n"
                + " where "
                + " ((quest.FN_ID_QUESTION = 201 and quest.FN_ID_QUESTION "
                + "is not null) or\n"
                + "             (quest.FN_ID_QUESTION = 202 "
                + "and quest.FN_ID_QUESTION is not null)) \n"
                + " and to_date(to_char(lpad(answ.fn_day,2,0)||"
                + "lpad(answ.fn_month,2,0)||answ.fn_year),'dd/mm/yy')\n"
                + " between nvl(to_date('" + firstDate + "','dd/mm/yy'),"
                + " to_date(to_char(lpad(answ.fn_day,2,0)"
                + "||lpad(answ.fn_month,2,0)||answ.fn_year),'dd/mm/yy') )\n"
                + " and nvl(to_date('" + lastDate + "','dd/mm/yy'),"
                + " to_date(to_char(lpad(answ.fn_day,2,0)||lpad(answ.fn_month,2,0)"
                + "||answ.fn_year),'dd/mm/yy') )";
        if (!stores.equals("(") && !zones.equals("(")) {
            statement2 = " and answ.fc_zone in " + zones + "\n"
                    + "             and answ.FN_ID_STORE in " + stores;
            statement = statement + statement2;
        }
        statement
                += " group by answ.fc_zone\n"
                + " having answ.fc_zone is not null\n"
                + " and answ.fc_zone not in('BT','CO','BD')\n"
                + " order by answ.fc_zone asc";

        Query query = JPA.em().createNativeQuery(statement);

        List<Object[]> result = query.getResultList();
        for (Object[] o : result) {
            UtilFolloupModel follow = new UtilFolloupModel();
            follow.setZone(o[0].toString());
            follow.setPromotoras(o[1].toString() != null ? Integer.parseInt(o[1].
                    toString()) : 0);
            follow.setDetra_pasivas(o[2].toString() != null
                    ? Integer.parseInt(o[2].toString()) : 0);
            follow.setTotalRespuestas(o[3].toString() != null
                    ? Integer.parseInt(o[3].toString()) : 0);
            follow.setNumGerentes(o[4].toString() != null
                    ? Integer.parseInt(o[4].toString()) : 0);
            follow.setSeguimiento(o[5].toString() != null
                    ? Integer.parseInt(o[5].toString()) : 0);
            follow.setSin_seguimiento(o[6].toString() != null
                    ? Integer.parseInt(o[6].toString()) : 0);
            follow.setClosed(o[7].toString() != null
                    ? Integer.parseInt(o[7].toString()) : 0);
            follow.setPercentPromo(df.format((float) follow.getPromotoras()
                    * 100 / follow.getTotalRespuestas()));
            follow.setPercentDetra(df.format((float) follow.getDetra_pasivas()
                    * 100 / follow.getTotalRespuestas()));
            follow.setPercentSeg(df.format((float) follow.getSeguimiento()
                    * 100 / follow.getDetra_pasivas()));
            follow.setPercentSinS(df.format((float) follow.getSin_seguimiento()
                    * 100 / follow.getDetra_pasivas()));
            follow.setPercentCd(df.format((float) follow.getClosed()
                    * 100 / follow.getDetra_pasivas()));
            tracings.add(follow);
        }

        return tracings;
    }

    public static List<Group> getGroups(String firstDate, String lastDate,
            String userId,Connection oracleConnection) {
        List<Group> groups = new ArrayList<>();

        CallableStatement callableStatement = null;

        try {

            if (oracleConnection != null) {
                callableStatement = oracleConnection.
                        prepareCall("call PORTALUNICO.pk_nps_home."
                                + "PR_GET_NPS_GROUP_BUSINESS(?, ?, ?, ?,"
                                + "?) ");

                callableStatement.setString(1, userId);  // userId
                callableStatement.setString(2, null);  // idStore
                callableStatement.setString(3, firstDate);  // idStore
                callableStatement.setString(4, lastDate);  // date in
                
                callableStatement.registerOutParameter(5, OracleTypes.CURSOR);

                callableStatement.execute();
                ResultSet rs = (ResultSet) callableStatement.getObject(5);

                while (rs.next()) {
                    Group g = new Group();
                    g.setId( rs.getString(1) );
                    g.setDesc(rs.getString(2));
                    groups.add(g);
                }
                rs.close();
            }
        } catch (SQLException ex) {
            Logger.getLogger(NpsHomeQuery.class.getName()).log(Level.SEVERE,
                    null, ex);
        } finally {
            if (callableStatement != null) {
                try {
                    callableStatement.close();
                } catch (SQLException ex) {
                    Logger.getLogger(NpsHomeQuery.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        }
        return groups;
    }

    public static List<Business> getBusiness(String group,String firstDate,
            String lastDate,String userId,Connection oracleConnection) {
        List<Business> business = new ArrayList<>();

        CallableStatement callableStatement = null;

        try {

            if (oracleConnection != null) {
                callableStatement = oracleConnection.
                        prepareCall("call PORTALUNICO.pk_nps_home."
                                + "PR_GET_NPS_GROUP_BUSINESS(?, ?, ?, ?,"
                                + "?) ");

                callableStatement.setString(1, userId);  // userId
                callableStatement.setString(2, group);  // idStore
                callableStatement.setString(3, firstDate);  // idStore
                callableStatement.setString(4, lastDate);  // date in
                
                callableStatement.registerOutParameter(5, OracleTypes.CURSOR);

                callableStatement.execute();
                ResultSet rs = (ResultSet) callableStatement.getObject(5);

                while (rs.next()) {
                    Business b = new Business();
                    b.setId( rs.getString(1) );
                    b.setDesc(rs.getString(2));
                    business.add(b);
                }        
                rs.close();
            }
        } catch (SQLException ex) {
            Logger.getLogger(NpsHomeQuery.class.getName()).log(Level.SEVERE,
                    null, ex);
        } finally {
            if (callableStatement != null) {
                try {
                    callableStatement.close();
                } catch (SQLException ex) {
                    Logger.getLogger(NpsHomeQuery.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        }


        return business;
    }

    public static String[] getNpsAsnswers() {
        return new String[]{"201", "202"};
    }

    private static Zone containsArea(List<Zone> zareas, String idZone) {
        for (Zone area : zareas) {
            if (area.getId().equals(idZone)) {
                return area;
            }
        }
        return null;
    }

    public static List<Store> getNpsStore(String userId, String firstDate,
            String lastDate, String[] strs, String[] mngs, String[] chfs,
            String[] sctns, int op, String zone, String group, String business,
            Connection oracleConnection)
            throws SQLException {
        List<Store> stores = new ArrayList<>();

        CallableStatement callableStatement = null;

        try {
            Array array1 = ((OracleConnection) oracleConnection).
                    createOracleArray("PORTALUNICO.ARRAY_STRING_TABLE", strs);

            Array array2 = ((OracleConnection) oracleConnection).
                    createOracleArray("PORTALUNICO.ARRAY_STRING_TABLE", mngs);
            Array array3 = ((OracleConnection) oracleConnection).
                    createOracleArray("PORTALUNICO.ARRAY_STRING_TABLE", chfs);

            Array array4 = ((OracleConnection) oracleConnection).
                    createOracleArray("PORTALUNICO.ARRAY_STRING_TABLE", sctns);
            if (oracleConnection != null) {
                callableStatement = oracleConnection.
                        prepareCall("call PORTALUNICO.pk_nps_home."
                                + "pr_get_nps_zone_area(?, ?, ?, ?, ?,"
                                + "?, ?, ?, ?, ?, ?, ?, ?, ?) ");

                callableStatement.setString(1, userId);  // userId
                callableStatement.setString(2, group);  // userId
                callableStatement.setString(3, business);  // userId
                callableStatement.setString(4, zone);  // idZone
                callableStatement.setArray(5, array1);  // idStore
                callableStatement.setArray(6, array2);  // idStore
                callableStatement.setArray(7, array3);  // idStore
                callableStatement.setArray(8, array4);  // idStore
                callableStatement.setString(9, firstDate);  // date in
                callableStatement.setString(10, lastDate);  // date out
                callableStatement.setInt(11, op);  // userId
                callableStatement.registerOutParameter(12, OracleTypes.CURSOR);
                callableStatement.registerOutParameter(13, OracleTypes.CURSOR);
                callableStatement.registerOutParameter(14, OracleTypes.CURSOR);

                callableStatement.execute();

                ResultSet rs = (ResultSet) callableStatement.getObject(12);

                while (rs.next()) {
                    Store store = new Store();
                    List<Area> areas = new ArrayList<>();
                    store.setIdParent(zone);
                    store.setId(rs.getString(1));
                    store.setNps(Float.toString(rs.getFloat(2)));
                    store.setAreas(areas);
                    stores.add(store);
                }

                rs = (ResultSet) callableStatement.getObject(13);
                while (rs.next()) {
                    Store store = containsStore(stores, rs.getString(1));
                    if(store != null) {
                        store.setNpsP(rs.getString(2));
                    }
                }
                
                rs = (ResultSet) callableStatement.getObject(14);
                while (rs.next()) {
                    Store store = containsStore(stores, rs.getString(1));
                    Area area = new Area();
                    store.setDesc(rs.getString(2));
                    area.setId(rs.getString(3));
                    area.setDesc(rs.getString(4));
                    area.setNps(rs.getFloat(5));
                    store.getAreas().add(area);
                }
                
                rs.close();
            }
        } catch (SQLException ex) {
            Logger.getLogger(NpsHomeQuery.class.getName()).log(Level.SEVERE,
                    null, ex);
        } finally {
            if (callableStatement != null) {
                callableStatement.close();
            }
        }

        return stores;
    }

    public static List<Manager> getNpsManager(String userId, String firstDate,
            String lastDate, String[] strs, String[] mngs, String[] chfs,
            String[] sctns, int op, String zone, String group, String business,
            Connection oracleConnection)
            throws SQLException {
        List<Manager> managers = new ArrayList<>();

        CallableStatement callableStatement = null;

        try {
            Array array1 = ((OracleConnection) oracleConnection).
                    createOracleArray("PORTALUNICO.ARRAY_STRING_TABLE", strs);

            Array array2 = ((OracleConnection) oracleConnection).
                    createOracleArray("PORTALUNICO.ARRAY_STRING_TABLE", mngs);
            Array array3 = ((OracleConnection) oracleConnection).
                    createOracleArray("PORTALUNICO.ARRAY_STRING_TABLE", chfs);

            Array array4 = ((OracleConnection) oracleConnection).
                    createOracleArray("PORTALUNICO.ARRAY_STRING_TABLE", sctns);
            if (oracleConnection != null) {
                callableStatement = oracleConnection.
                        prepareCall("call PORTALUNICO.pk_nps_home."
                                + "pr_get_nps_zone_area(?, ?, ?, ?, ?,"
                                + "?, ?, ?, ?, ?, ?, ?, ?, ?) ");

                callableStatement.setString(1, userId);  // userId
                callableStatement.setString(2, group);  // userId
                callableStatement.setString(3, business);  // userId
                callableStatement.setString(4, zone);  // idZone
                callableStatement.setArray(5, array1);  // idStore
                callableStatement.setArray(6, array2);  // idStore
                callableStatement.setArray(7, array3);  // idStore
                callableStatement.setArray(8, array4);  // idStore
                callableStatement.setString(9, firstDate);  // date in
                callableStatement.setString(10, lastDate);  // date out
                callableStatement.setInt(11, op);  // userId
                callableStatement.registerOutParameter(12, OracleTypes.CURSOR);
                callableStatement.registerOutParameter(13, OracleTypes.CURSOR);
                callableStatement.registerOutParameter(14, OracleTypes.CURSOR);
                
                callableStatement.execute();

                ResultSet rs = (ResultSet) callableStatement.getObject(12);

                while (rs.next()) {
                    Manager man = new Manager();
                    List<Area> areas = new ArrayList<>();
                    man.setIdParent(rs.getString(1));
                    man.setParent(rs.getString(2));
                    man.setId(rs.getString(3));
                    man.setNps(Float.toString(rs.getFloat(4)));
                    man.setAreas(areas);
                    managers.add(man);
                }

                 rs = (ResultSet) callableStatement.getObject(13);
                while (rs.next()) {
                    Manager man = containsManager(managers, rs.getString(3));
                    if(man != null) {
                        man.setNpsP(rs.getString(4));
                    }
                }
                
                rs = (ResultSet) callableStatement.getObject(14);
                while (rs.next()) {
                    Manager man = containsManager(managers, rs.getString(3));
                    Area area = new Area();
                    man.setDesc(rs.getString(4));
                    area.setId(rs.getString(5));
                    area.setDesc(rs.getString(6));
                    area.setNps(rs.getFloat(7));
                    man.getAreas().add(area);
                }
                
                rs.close();
            }
        } catch (SQLException ex) {
            Logger.getLogger(NpsHomeQuery.class.getName()).log(Level.SEVERE,
                    null, ex);
        } finally {
            if (callableStatement != null) {
                callableStatement.close();
            }
        }

        return managers;
    }

    public static List<Chief> getNpsChief(String userId, String firstDate,
            String lastDate, String[] strs, String[] mngs, String[] chfs,
            String[] sctns, int op, String zone, String group, String business,
            Connection oracleConnection)
            throws SQLException {
        List<Chief> chiefs = new ArrayList<>();

        CallableStatement callableStatement = null;

        try {

            Array array1 = ((OracleConnection) oracleConnection).
                    createOracleArray("PORTALUNICO.ARRAY_STRING_TABLE", strs);

            Array array2 = ((OracleConnection) oracleConnection).
                    createOracleArray("PORTALUNICO.ARRAY_STRING_TABLE", mngs);
            Array array3 = ((OracleConnection) oracleConnection).
                    createOracleArray("PORTALUNICO.ARRAY_STRING_TABLE", chfs);

            Array array4 = ((OracleConnection) oracleConnection).
                    createOracleArray("PORTALUNICO.ARRAY_STRING_TABLE", sctns);
            if (oracleConnection != null) {
                callableStatement = oracleConnection.
                        prepareCall("call PORTALUNICO.pk_nps_home."
                                + "pr_get_nps_zone_area(?, ?, ?, ?, ?,"
                                + "?, ?, ?, ?, ?, ?, ?, ?, ?) ");

                callableStatement.setString(1, userId);  // userId
                callableStatement.setString(2, group);  // userId
                callableStatement.setString(3, business);  // userId
                callableStatement.setString(4, zone);  // idZone
                callableStatement.setArray(5, array1);  // idStore
                callableStatement.setArray(6, array2);  // idStore
                callableStatement.setArray(7, array3);  // idStore
                callableStatement.setArray(8, array4);  // idStore
                callableStatement.setString(9, firstDate);  // date in
                callableStatement.setString(10, lastDate);  // date out
                callableStatement.setInt(11, op);  // userId
                callableStatement.registerOutParameter(12, OracleTypes.CURSOR);
                callableStatement.registerOutParameter(13, OracleTypes.CURSOR);
                callableStatement.registerOutParameter(14, OracleTypes.CURSOR);
                
                callableStatement.execute();

                ResultSet rs = (ResultSet) callableStatement.getObject(12);

                while (rs.next()) {
                    Chief chf = new Chief();
                    List<Area> areas = new ArrayList<>();
                    chf.setIdStore(rs.getString(1));
                    chf.setDescStore(rs.getString(2));
                    chf.setIdParent(rs.getString(3));
                    chf.setId(rs.getString(4));
                    chf.setNps(Float.toString(rs.getFloat(5)));
                    chf.setAreas(areas);
                    chiefs.add(chf);
                }

                rs = (ResultSet) callableStatement.getObject(13);
                while (rs.next()) {
                    Chief chf = containsChief(chiefs, rs.getString(4));
                    if(chf != null) {
                        chf.setNpsP(rs.getString(5));
                    }
                }
                
                rs = (ResultSet) callableStatement.getObject(14);
                while (rs.next()) {
                    Chief chf = containsChief(chiefs, rs.getString(4));
                    Area area = new Area();
                    chf.setDesc(rs.getString(5));
                    area.setId(rs.getString(6));
                    area.setDesc(rs.getString(7));
                    area.setNps(rs.getFloat(8));
                    chf.getAreas().add(area);
                }
                
                rs.close();
            }
        } catch (SQLException ex) {
            Logger.getLogger(NpsHomeQuery.class.getName()).log(Level.SEVERE,
                    null, ex);
        } finally {
            if (callableStatement != null) {
                callableStatement.close();
            }
        }

        return chiefs;
    }

    public static List<Section> getNpsSections(String userId, String firstDate,
            String lastDate, String[] strs, String[] mngs, String[] chfs,
            String[] sctns, int op, String zone, String group, String business,
            Connection oracleConnection)
            throws SQLException {
        List<Section> sections = new ArrayList<>();

        CallableStatement callableStatement = null;
        try {

            Array array1 = ((OracleConnection) oracleConnection).
                    createOracleArray("PORTALUNICO.ARRAY_STRING_TABLE", strs);

            Array array2 = ((OracleConnection) oracleConnection).
                    createOracleArray("PORTALUNICO.ARRAY_STRING_TABLE", mngs);
            Array array3 = ((OracleConnection) oracleConnection).
                    createOracleArray("PORTALUNICO.ARRAY_STRING_TABLE", chfs);

            Array array4 = ((OracleConnection) oracleConnection).
                    createOracleArray("PORTALUNICO.ARRAY_STRING_TABLE", sctns);
            if (oracleConnection != null) {
                callableStatement = oracleConnection.
                        prepareCall("call PORTALUNICO.pk_nps_home."
                                + "pr_get_nps_zone_area(?, ?, ?, ?, ?,"
                                + "?, ?, ?, ?, ?, ?, ?, ?, ?) ");

                callableStatement.setString(1, userId);  // userId
                callableStatement.setString(2, group);  // userId
                callableStatement.setString(3, business);  // userId
                callableStatement.setString(4, zone);  // idZone
                callableStatement.setArray(5, array1);  // idStore
                callableStatement.setArray(6, array2);  // idStore
                callableStatement.setArray(7, array3);  // idStore
                callableStatement.setArray(8, array4);  // idStore
                callableStatement.setString(9, firstDate);  // date in
                callableStatement.setString(10, lastDate);  // date out
                callableStatement.setInt(11, op);  // userId
                callableStatement.registerOutParameter(12, OracleTypes.CURSOR);
                callableStatement.registerOutParameter(13, OracleTypes.CURSOR);
                callableStatement.registerOutParameter(14, OracleTypes.CURSOR);
                
                callableStatement.execute();

                ResultSet rs = (ResultSet) callableStatement.getObject(12);

                while (rs.next()) {
                    Section sec = new Section();
                    List<Area> areas = new ArrayList<>();
                    sec.setIdStore(rs.getString(1));
                    sec.setStoreDesc(rs.getString(2));
                    sec.setId(rs.getString(3));
                    sec.setNps(Float.toString(rs.getFloat(4)));
                    sec.setAreas(areas);
                    sections.add(sec);
                }

                rs = (ResultSet) callableStatement.getObject(13);
                while (rs.next()) {
                    Section sec = containsSection(sections,rs.getString(3));
                    if(sec != null) {
                        sec.setNpsP(rs.getString(4));
                    }
                }
                
                rs = (ResultSet) callableStatement.getObject(14);
                while (rs.next()) {
                    Section sec = containsSection(sections,
                            rs.getString(3));
                    Area area = new Area();
                    sec.setDesc(rs.getString(4));
                    area.setId(rs.getString(5));
                    area.setDesc(rs.getString(6));
                    area.setNps(rs.getFloat(7));
                    sec.getAreas().add(area);
                }
                
                rs.close();
            }
        } catch (SQLException ex) {
            Logger.getLogger(NpsHomeQuery.class.getName()).log(Level.SEVERE,
                    null, ex);
        } finally {
            if (callableStatement != null) {
                callableStatement.close();
            }
        }

        return sections;
    }

    public static List<Section> getSections(String id, String store) {
        List<Section> sections = new ArrayList<>();
        String statement = "select distinct answ.FC_SECTION_ID, \n"
                + "                nvl((select FC_DESC_SECTION \n"
                + "                     from    PORTALUNICO.PUL_SECTIONS\n"
                + "                     where   FN_ID_SECTION = "
                + "                       answ.FC_SECTION_ID),'SIN DESCRIPCIÓN') \n"
                + "                nombre, \n"
                + "                (decode(fn_userid_chief,'6666666666','Jefe dummy',\n"
                + "                  nvl((select fc_title \n"
                + "                  from PORTALUNICO.pul_structure \n"
                + "                  where fn_userid = answ.fn_userid_chief),'Puesto no encontrado'\n"
                + "                  ))) title"
                + " from   pul_nps_answer answ\n"
                + " where answ.FN_USERID_CHIEF = " + Long.parseLong(id)
                + " and answ.FN_ID_STORE=" + Integer.parseInt(store);

        Query query = JPA.em().
                createNativeQuery(statement);

        List<Object[]> result = query.getResultList();
        if (result != null && !result.isEmpty()) {
            for (Object[] o : result) {
                Section sec = new Section();
                sec.setId(o[0] != null ? o[0].toString() : "");
                sec.setDesc(o[1] != null ? o[1].toString() : "");
                sec.setParent(o[2] != null ? o[2].toString() : "");
                sections.add(sec);
            }
        }
        return sections;
    }

    public static List<Seller> getNpsSellers(String userId, String firstDate,
            String lastDate, String[] strs, String[] mngs, String[] chfs,
            String[] sctns, int op, String zone, String group, String business,
            Connection oracleConnection)
            throws SQLException {
        List<Seller> sellers = new ArrayList<>();

        CallableStatement callableStatement = null;

        try {

            Array array1 = ((OracleConnection) oracleConnection).
                    createOracleArray("PORTALUNICO.ARRAY_STRING_TABLE", strs);

            Array array2 = ((OracleConnection) oracleConnection).
                    createOracleArray("PORTALUNICO.ARRAY_STRING_TABLE", mngs);
            Array array3 = ((OracleConnection) oracleConnection).
                    createOracleArray("PORTALUNICO.ARRAY_STRING_TABLE", chfs);

            Array array4 = ((OracleConnection) oracleConnection).
                    createOracleArray("PORTALUNICO.ARRAY_STRING_TABLE", sctns);
            if (oracleConnection != null) {
                callableStatement = oracleConnection.
                        prepareCall("call PORTALUNICO.pk_nps_home."
                                + "pr_get_nps_zone_area(?, ?, ?, ?, ?,"
                                + "?, ?, ?, ?, ?, ?, ?, ?, ?) ");

                callableStatement.setString(1, userId);  // userId
                callableStatement.setString(2, group);  // userId
                callableStatement.setString(3, business);  // userId
                callableStatement.setString(4, zone);  // idZone
                callableStatement.setArray(5, array1);  // idStore
                callableStatement.setArray(6, array2);  // idStore
                callableStatement.setArray(7, array3);  // idStore
                callableStatement.setArray(8, array4);  // idStore
                callableStatement.setString(9, firstDate);  // date in
                callableStatement.setString(10, lastDate);  // date out
                callableStatement.setInt(11, op);  // userId
                callableStatement.registerOutParameter(12, OracleTypes.CURSOR);
                callableStatement.registerOutParameter(13, OracleTypes.CURSOR);
                callableStatement.registerOutParameter(14, OracleTypes.CURSOR);
                
                callableStatement.execute();

                ResultSet rs = (ResultSet) callableStatement.getObject(12);

                while (rs.next()) {
                    Seller sell = new Seller();
                    sell.setIdStore(rs.getString(1));
                    sell.setDescStore(rs.getString(2));
                    List<Area> areas = new ArrayList<>();
                    sell.setId(rs.getString(3));
                    sell.setNps(Float.toString(rs.getFloat(4)));
                    sell.setAreas(areas);
                    sellers.add(sell);
                }

                rs = (ResultSet) callableStatement.getObject(13);
                while (rs.next()) {
                    Seller sell = containsSeller(sellers, rs.getString(3));
                    if(sell != null) {
                        sell.setNpsP(rs.getString(4));
                    }
                }
                
                rs = (ResultSet) callableStatement.getObject(14);
                while (rs.next()) {
                    Seller sell = containsSeller(sellers,
                            rs.getString(3));
                    Area area = new Area();
                    sell.setDesc(rs.getString(4));
                    area.setId(rs.getString(5));
                    area.setDesc(rs.getString(6));
                    area.setNps(rs.getFloat(7));
                    sell.getAreas().add(area);
                }
                
                rs.close();
            }
        } catch (SQLException ex) {
            Logger.getLogger(NpsHomeQuery.class.getName()).log(Level.SEVERE,
                    null, ex);
        } finally {
            if (callableStatement != null) {
                callableStatement.close();
            }
        }

        return sellers;
    }

    public static List<Store> getNpsRankingForStore(String userId, String firstDate,
            String lastDate, String[] strs, String[] mngs, String[] chfs,
            String[] sctns, int op, String zone[], String group, String business,
            Connection oracleConnection, String rankear)
            throws SQLException {

        List<Store> stores = new ArrayList<>();

        CallableStatement callableStatement = null;

        try {
            Array array1 = ((OracleConnection) oracleConnection).
                    createOracleArray("PORTALUNICO.ARRAY_STRING_TABLE", zone);

            Array array2 = ((OracleConnection) oracleConnection).
                    createOracleArray("PORTALUNICO.ARRAY_STRING_TABLE", strs);
            
            Array array3 = ((OracleConnection) oracleConnection).
                    createOracleArray("PORTALUNICO.ARRAY_STRING_TABLE", mngs);
            
            Array array4 = ((OracleConnection) oracleConnection).
                    createOracleArray("PORTALUNICO.ARRAY_STRING_TABLE", chfs);
            
            Array array5 = ((OracleConnection) oracleConnection).
                    createOracleArray("PORTALUNICO.ARRAY_STRING_TABLE", sctns);

            if (oracleConnection != null) {
                callableStatement = oracleConnection.
                        prepareCall("call PORTALUNICO.pk_nps_home."
                                + "pr_get_ranking_for(?, ?, ?, ?, ?,"
                                + "?, ?, ?, ?, ?, ?, ?, ?, ?, ?)");

                callableStatement.setString(1, userId);  // userId
                callableStatement.setString(2, group);  // userId
                callableStatement.setString(3, business);  // userId
                callableStatement.setArray(4, array1);  // idzone
                callableStatement.setArray(5, array2);  // idStore
                callableStatement.setArray(6, array3);  // idmanager
                callableStatement.setArray(7, array4);  // idchiefs
                callableStatement.setArray(8, array5);  // idsections
                callableStatement.setString(9, rankear);  // idStore
                callableStatement.setString(10, firstDate);  // date in
                callableStatement.setString(11, lastDate);  // date out
                callableStatement.setInt(12, op);  // userId
                callableStatement.registerOutParameter(13, OracleTypes.CURSOR);
                callableStatement.registerOutParameter(14, OracleTypes.CURSOR);
                callableStatement.registerOutParameter(15, OracleTypes.CURSOR);

                callableStatement.execute();

                ResultSet rs = (ResultSet) callableStatement.getObject(13);

                while (rs.next()) {
                    Store store = new Store();
                    List<Area> areas = new ArrayList<>();
                    store.setId(rs.getString(1));
                    store.setDesc(rs.getString(2));
                    store.setNps(Float.toString(rs.getFloat(3)));
                    store.setAreas(areas);
                    stores.add(store);
                }

                rs = (ResultSet) callableStatement.getObject(14);
                while (rs.next()) {
                    Store store = containsStore(stores, rs.getString(1));
                    if(store != null){
                        store.setNpsP(rs.getString(3));
                    }
                }
                
                rs = (ResultSet) callableStatement.getObject(15);
                while (rs.next()) {
                    Store store = containsStore(stores, rs.getString(1));
                    Area area = new Area();

                    area.setId(rs.getString(3));
                    area.setDesc(rs.getString(4));
                    area.setNps(rs.getFloat(5));
                    store.getAreas().add(area);
                }
                
                rs.close();
            }
        } catch (SQLException ex) {
            Logger.getLogger(NpsHomeQuery.class.getName()).log(Level.SEVERE,
                    null, ex);
        } finally {
            if (callableStatement != null) {
                callableStatement.close();
            }
        }

        return stores;
    }

    public static List<Manager> getNpsRankingForManager(String userId, String firstDate,
            String lastDate, String[] strs, String[] mngs, String[] chfs,
            String[] sctns, int op, String zone[], String group, String business,
            Connection oracleConnection, String rankear)
            throws SQLException {

        List<Manager> managers = new ArrayList<>();;

        CallableStatement callableStatement = null;

        try {
            Array array1 = ((OracleConnection) oracleConnection).
                    createOracleArray("PORTALUNICO.ARRAY_STRING_TABLE", zone);

            Array array2 = ((OracleConnection) oracleConnection).
                    createOracleArray("PORTALUNICO.ARRAY_STRING_TABLE", strs);

            Array array3 = ((OracleConnection) oracleConnection).
                    createOracleArray("PORTALUNICO.ARRAY_STRING_TABLE", mngs);
            
            Array array4 = ((OracleConnection) oracleConnection).
                    createOracleArray("PORTALUNICO.ARRAY_STRING_TABLE", chfs);
            
            Array array5 = ((OracleConnection) oracleConnection).
                    createOracleArray("PORTALUNICO.ARRAY_STRING_TABLE", sctns);
            
            if (oracleConnection != null) {
                callableStatement = oracleConnection.
                        prepareCall("call PORTALUNICO.pk_nps_home."
                                + "pr_get_ranking_for(?, ?, ?, ?, ?,"
                                + "?, ?, ?, ?, ?, ?, ?, ? ,? ,?) ");
                
                callableStatement.setString(1, userId);  // userId
                callableStatement.setString(2, group);  // userId
                callableStatement.setString(3, business);  // userId
                callableStatement.setArray(4, array1);  // idStore
                callableStatement.setArray(5, array2);  // idStore
                callableStatement.setArray(6, array3);  // idmanager
                callableStatement.setArray(7, array4);  // idchiefs
                callableStatement.setArray(8, array5);  // idsections
                callableStatement.setString(9, rankear);  // idStore
                callableStatement.setString(10, firstDate);  // date in
                callableStatement.setString(11, lastDate);  // date out
                callableStatement.setInt(12, op);  // userId
                callableStatement.registerOutParameter(13, OracleTypes.CURSOR);
                callableStatement.registerOutParameter(14, OracleTypes.CURSOR);
                callableStatement.registerOutParameter(15, OracleTypes.CURSOR);

                callableStatement.execute();

                ResultSet rs = (ResultSet) callableStatement.getObject(13);

                while (rs.next()) {
                    Manager store = new Manager();
                    List<Area> areas = new ArrayList<>();
                    store.setId(rs.getString(1));
                    store.setDesc(rs.getString(2));
                    store.setNps(Float.toString(rs.getFloat(3)));
                    store.setAreas(areas);
                    managers.add(store);
                }

                rs = (ResultSet) callableStatement.getObject(14);
                while (rs.next()) {
                    Manager store = containsManager(managers, rs.getString(1));
                    if(store != null){
                        store.setNpsP(rs.getString(3));
                    }
                }
                
                rs = (ResultSet) callableStatement.getObject(15);
                while (rs.next()) {
                    Manager store = containsManager(managers, rs.getString(1));
                    Area area = new Area();

                    area.setId(rs.getString(3));
                    area.setDesc(rs.getString(4));
                    area.setNps(rs.getFloat(5));
                    store.getAreas().add(area);
                }
                
                rs.close();
            }
        } catch (SQLException ex) {
            Logger.getLogger(NpsHomeQuery.class.getName()).log(Level.SEVERE,
                    null, ex);
        } finally {
            if (callableStatement != null) {
                callableStatement.close();
            }
        }

        return managers;
    }

    public static List<Chief> getNpsRankingForChief(String userId, String firstDate,
            String lastDate, String[] strs, String[] mngs, String[] chfs,
            String[] sctns, int op, String zone[], String group, String business,
            Connection oracleConnection, String rankear)
            throws SQLException {

        List<Chief> chiefs = new ArrayList<>();;

        CallableStatement callableStatement = null;

        try {
            Array array1 = ((OracleConnection) oracleConnection).
                    createOracleArray("PORTALUNICO.ARRAY_STRING_TABLE", zone);

            Array array2 = ((OracleConnection) oracleConnection).
                    createOracleArray("PORTALUNICO.ARRAY_STRING_TABLE", strs);

            Array array3 = ((OracleConnection) oracleConnection).
                    createOracleArray("PORTALUNICO.ARRAY_STRING_TABLE", mngs);
            
            Array array4 = ((OracleConnection) oracleConnection).
                    createOracleArray("PORTALUNICO.ARRAY_STRING_TABLE", chfs);
            
            Array array5 = ((OracleConnection) oracleConnection).
                    createOracleArray("PORTALUNICO.ARRAY_STRING_TABLE", sctns);
            
            if (oracleConnection != null) {
                callableStatement = oracleConnection.
                        prepareCall("call PORTALUNICO.pk_nps_home."
                                + "pr_get_ranking_for(?, ?, ?, ?, ?,"
                                + "?, ?, ?, ?, ?, ?, ?, ?, ?, ?) ");

                callableStatement.setString(1, userId);  // userId
                callableStatement.setString(2, group);  // userId
                callableStatement.setString(3, business);  // userId
                callableStatement.setArray(4, array1);  // idStore
                callableStatement.setArray(5, array2);  // idStore
                callableStatement.setArray(6, array3);  // idmanager
                callableStatement.setArray(7, array4);  // idchiefs
                callableStatement.setArray(8, array5);  // idsections
                callableStatement.setString(9, rankear);  // idStore
                callableStatement.setString(10, firstDate);  // date in
                callableStatement.setString(11, lastDate);  // date out
                callableStatement.setInt(12, op);  // userId
                callableStatement.registerOutParameter(13, OracleTypes.CURSOR);
                callableStatement.registerOutParameter(14, OracleTypes.CURSOR);
                callableStatement.registerOutParameter(15, OracleTypes.CURSOR);

                callableStatement.execute();

                ResultSet rs = (ResultSet) callableStatement.getObject(13);

                while (rs.next()) {
                    Chief store = new Chief();
                    List<Area> areas = new ArrayList<>();
                    store.setId(rs.getString(1));
                    store.setDesc(rs.getString(2));
                    store.setNps(Float.toString(rs.getFloat(3)));
                    store.setAreas(areas);
                    chiefs.add(store);
                }

                rs = (ResultSet) callableStatement.getObject(14);
                while (rs.next()) {
                    Chief store = containsChief(chiefs, rs.getString(1));
                    if(store != null){
                        store.setNpsP(rs.getString(3));
                    }
                }
                
                rs = (ResultSet) callableStatement.getObject(15);
                while (rs.next()) {
                    Chief store = containsChief(chiefs, rs.getString(1));
                    Area area = new Area();

                    area.setId(rs.getString(3));
                    area.setDesc(rs.getString(4));
                    area.setNps(rs.getFloat(5));
                    store.getAreas().add(area);
                }
                
                rs.close();
            }
        } catch (SQLException ex) {
            Logger.getLogger(NpsHomeQuery.class.getName()).log(Level.SEVERE,
                    null, ex);
        } finally {
            if (callableStatement != null) {
                callableStatement.close();
            }
        }

        return chiefs;
    }

    public static List<Seller> getNpsRankingForSeller(String userId, String firstDate,
            String lastDate, String[] strs, String[] mngs, String[] chfs,
            String[] sctns, int op, String zone[], String group, String business,
            Connection oracleConnection, String rankear)
            throws SQLException {

        List<Seller> sellers = new ArrayList<>();;

        CallableStatement callableStatement = null;

        try {
            Array array1 = ((OracleConnection) oracleConnection).
                    createOracleArray("PORTALUNICO.ARRAY_STRING_TABLE", zone);

            Array array2 = ((OracleConnection) oracleConnection).
                    createOracleArray("PORTALUNICO.ARRAY_STRING_TABLE", strs);

            Array array3 = ((OracleConnection) oracleConnection).
                    createOracleArray("PORTALUNICO.ARRAY_STRING_TABLE", mngs);
            
            Array array4 = ((OracleConnection) oracleConnection).
                    createOracleArray("PORTALUNICO.ARRAY_STRING_TABLE", chfs);
            
            Array array5 = ((OracleConnection) oracleConnection).
                    createOracleArray("PORTALUNICO.ARRAY_STRING_TABLE", sctns);
            
            if (oracleConnection != null) {
                callableStatement = oracleConnection.
                        prepareCall("call PORTALUNICO.pk_nps_home."
                                + "pr_get_ranking_for(?, ?, ?, ?, ?,"
                                + "?, ?, ?, ?, ?, ?, ?, ?, ?, ?) ");

                callableStatement.setString(1, userId);  // userId
                callableStatement.setString(2, group);  // group
                callableStatement.setString(3, business);  // business
                callableStatement.setArray(4, array1);  // array1
                callableStatement.setArray(5, array2);  // array2
                callableStatement.setArray(6, array3);  // idmanager
                callableStatement.setArray(7, array4);  // idchiefs
                callableStatement.setArray(8, array5);  // idsections
                callableStatement.setString(9, rankear);  // idStore
                callableStatement.setString(10, firstDate);  // date in
                callableStatement.setString(11, lastDate);  // date out
                callableStatement.setInt(12, op);  // userId
                callableStatement.registerOutParameter(13, OracleTypes.CURSOR);
                callableStatement.registerOutParameter(14, OracleTypes.CURSOR);
                callableStatement.registerOutParameter(15, OracleTypes.CURSOR);

                callableStatement.execute();

                ResultSet rs = (ResultSet) callableStatement.getObject(13);

                while (rs.next()) {
                    Seller store = new Seller();
                    List<Area> areas = new ArrayList<>();
                    store.setId(rs.getString(1));
                    store.setDesc(rs.getString(2));
                    store.setNps(Float.toString(rs.getFloat(3)));
                    store.setAreas(areas);
                    sellers.add(store);
                }

                rs = (ResultSet) callableStatement.getObject(14);
                while (rs.next()) {
                    Seller store = containsSeller(sellers, rs.getString(1));
                    if(store != null){
                        store.setNpsP(rs.getString(3));
                    }
                }
                
                rs = (ResultSet) callableStatement.getObject(15);
                while (rs.next()) {
                    Seller store = containsSeller(sellers, rs.getString(1));
                    Area area = new Area();

                    area.setId(rs.getString(3));
                    area.setDesc(rs.getString(4));
                    area.setNps(rs.getFloat(5));
                    store.getAreas().add(area);
                }
                
                rs.close();
            }
        } catch (SQLException ex) {
            Logger.getLogger(NpsHomeQuery.class.getName()).log(Level.SEVERE,
                    null, ex);
        } finally {
            if (callableStatement != null) {
                callableStatement.close();
            }
        }

        return sellers;
    }
    
    private static Store containsStore(List<Store> stores, String idStore) {
        for (Store store : stores) {
            if (store.getId().equals(idStore)) {
                return store;
            }
        }
        return null;
    }
    
    private static Store containsStoreName(List<Store> stores, String idStore) {
        for (Store store : stores) {
            if (store.getDesc().equals(idStore)) {
                return store;
            }
        }
        return null;
    }

    private static Manager containsManager(List<Manager> mans, String idMan) {
        for (Manager man : mans) {
            if (man.getId().equals(idMan)) {
                return man;
            }
        }
        return null;
    }
    
    private static Manager containsManagerName(List<Manager> mans, String idMan) {
        for (Manager man : mans) {
            if (man.getDesc().equals(idMan)) {
                return man;
            }
        }
        return null;
    }

    private static Chief containsChief(List<Chief> chiefs, String idChf) {
        for (Chief chf : chiefs) {
            if (chf.getId().equals(idChf)) {
                return chf;
            }
        }
        return null;
    }

    private static Section containsSection(List<Section> sections, String idSct) {
        for (Section sec : sections) {
            if (sec.getId().equals(idSct)) {
                return sec;
            }
        }
        return null;
    }

    private static Section containsSectionName(List<Section> sections, String idSct) {
        for (Section sec : sections) {
            if (sec.getDesc().equals(idSct)) {
                return sec;
            }
        }
        return null;
    }
    
    private static Seller containsSeller(List<Seller> sellers, String idSell) {
        for (Seller sell : sellers) {
            if (sell.getId().equals(idSell)) {
                return sell;
            }
        }
        return null;
    }

    private static Zone containsZone(List<Zone> zareas, String string) {
        for(Zone z : zareas) {
            if(z.getId().equalsIgnoreCase(string)) {
                return z;
            }
        }
        return null;
    }
}
