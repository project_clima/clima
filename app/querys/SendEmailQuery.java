package querys;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.sql.CallableStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.*;
import json.entities.MailingList;
import oracle.jdbc.OracleCallableStatement;
import oracle.jdbc.OracleConnection;
import play.vfs.VirtualFile;
import static utils.Queries.getOracleConnection;
import org.apache.commons.lang3.StringUtils;

public class SendEmailQuery {
    
    public static List<MailingList> getEmployeesList(int surveyId, String employeesList)
    throws SQLException {        
        String query = "select\n" +
                       "ec.FN_USERID,\n" +
                       "lower(ec.FC_EMAIL) FC_EMAIL,\n" +
                       "ec.FC_FIRST_NAME FC_FIRST_NAME,\n" +
                       "ec.FC_LAST_NAME FC_LAST_NAME,\n" +
                       "decode(su.FN_ID_SURVEY_TYPE, 1,SUBSTR(se.FC_NOMENCLATURA, 0, INSTR(se.FC_NOMENCLATURA, '/')-1) ,se.FC_USERNAME) USERNAME,\n" +
                       "decode(su.FN_ID_SURVEY_TYPE, 1,SUBSTR(se.FC_NOMENCLATURA, INSTR(se.FC_NOMENCLATURA, '/')+1), se.FC_PASSWORD) FC_PASSWORD\n" +
                       "from PUL_EMPLOYEE_CLIMA ec\n" +
                       "INNER JOIN PUL_SURVEY_EMPLOYEE se ON se.FN_ID_EMPLOYEE_CLIMA = ec.FN_ID_EMPLOYEE_CLIMA\n" +
                       "INNER JOIN PUL_SURVEY su ON su.FN_ID_SURVEY = se.FN_ID_SURVEY\n" +
                       "where su.FN_ID_SURVEY = " + surveyId + "\n" +
                       "AND TO_CHAR(ec.FD_STRUCTURE_DATE, 'MM/YYYY') = TO_CHAR(su.FD_SURVEY_DATE, 'MM/YYYY') \n" +
                       //"AND (FN_LEVEL <> 0 AND FN_LEVEL <> 1) \n" +
                       addionalConditionsEmployees(employeesList);
        
        OracleConnection connection = getOracleConnection();
        CallableStatement statement = connection.prepareCall(query);
        statement.execute();

        ResultSet results = ((OracleCallableStatement) statement).getResultSet();
        
        List<MailingList> dataList = new ArrayList();
        MailingList mail;
        
        while (results.next()) {
            mail = new MailingList();
            
            mail.setUserId(results.getInt("FN_USERID"));
            mail.setFirstName(results.getString("FC_FIRST_NAME"));
            mail.setLastName(results.getString("FC_LAST_NAME"));
            mail.setEmail(results.getString("FC_EMAIL"));
            mail.setUserName(results.getString("USERNAME"));
            mail.setPassword(results.getString("FC_PASSWORD"));
            
            dataList.add(mail);
        }
        
        return dataList;
    }
    
    public static String addionalConditionsEmployees(String employeesList) {
        String condition = "";
        
        if (employeesList != null && !employeesList.equals("")) {
            condition = "AND FN_USERID IN (" + employeesList + ")";
        }
        
        return condition;
    }
    
    public static List<MailingList> getEmployeesStore(int surveyId, String storeTypes, 
            String zones, List<String> stores, Date surveyDate)
    throws SQLException, IOException {
        String query = getQueryStore(surveyId, storeTypes, zones, stores, surveyDate);

        OracleConnection connection = getOracleConnection();
        CallableStatement statement = connection.prepareCall(query);
        statement.execute();

        ResultSet results = ((OracleCallableStatement) statement).getResultSet();
        
        List<MailingList> dataList = new ArrayList();
        MailingList mail;
        
        while (results.next()) {
            mail = new MailingList();
            
            mail.setUserId(results.getInt("FN_USERID"));
            mail.setUserName(results.getString("USUARIO"));
            mail.setPassword(results.getString("CONTRASENA"));
            mail.setDepartment(results.getString("DEMOGRAFICO"));
            mail.setManager(results.getString("FC_MANAGER"));
            mail.setDivision(results.getString("FC_DIVISION"));
            mail.setPlan(results.getInt("FN_PLAN"));
            dataList.add(mail);
        }
        
        statement.close();
        
        return dataList;
    }
    
    public static String getQueryStore(int surveyId, String storeTypes, String zones, 
        List<String> stores, Date surveyDate)
    throws IOException {
        String baseQuery = getBaseStoreQuery(surveyId, surveyDate);
        String andWhere  = "";
        
        if (storeTypes != null && !storeTypes.equals("all")) {
            andWhere = "AND LOWER(st.FC_DESC_SUBDIV) LIKE LOWER('%" + storeTypes + "%')\n";
        }
        
        if (zones != null && !zones.equals("all")) {
            andWhere = "AND st.FC_SUB_DIV = '" + zones + "'\n";
        }
        
        if (stores != null && stores.size() > 0) {
            andWhere = "AND st.FN_ID_STORE IN (" + StringUtils.join(stores, ",")  + ")\n";
        }
        
        return baseQuery + andWhere + "ORDER BY st.FC_DESC_SUBDIV ASC";
    }
    
    public static String getBaseStoreQuery(int surveyId, Date surveyDate) {
        SimpleDateFormat sdf = new SimpleDateFormat("MM/yyyy");
        
        String query = "SELECT EC.FN_USERID,\n" +
                        "EC.FN_MANAGER,\n" +
                        "EC.FC_TITLE DEMOGRAFICO,\n" +
                        "decode(1, 1,SUBSTR(se.FC_NOMENCLATURA, 0, INSTR(se.FC_NOMENCLATURA, '/')-1) ,se.FC_USERNAME) USUARIO,\n" +
                        "decode(1, 1,SUBSTR(se.FC_NOMENCLATURA, INSTR(se.FC_NOMENCLATURA, '/')+1), se.FC_PASSWORD) CONTRASENA,\n" +
                        "EC.FC_FIRST_NAME||' '||EC.FC_LAST_NAME as FC_MANAGER,\n" +
                        "st.FN_ID_STORE FC_DIVISION,\n" +
                        "ec.FC_EMAIL,\n" +
                        "se.FN_PLAN,\n" +
                        "st.FN_ID_STORE,\n" +
                        "st.FC_DESC_STORE,\n" +
                        "st.FC_SUB_DIV\n" +
                        "FROM PUL_EMPLOYEE_CLIMA ec\n" +
                        "LEFT JOIN PUL_SURVEY_EMPLOYEE SE ON SE.FN_ID_EMPLOYEE_CLIMA = EC.FN_ID_EMPLOYEE_CLIMA\n" +
                        "LEFT JOIN PUL_STORE st ON (st.FN_ID_STORE = ec.FN_ID_STORE AND TO_CHAR(st.FD_STRUCTURE_DATE, 'MM/YYYY') ='"+ sdf.format(surveyDate) +"')\n" +
                        "WHERE  SE.FN_ID_SURVEY = " + surveyId + "\n" +
                        "AND    SE.FN_PLAN <> 0\n" +
                        "AND    SE.FN_ID_SURVEY in (select s.FN_ID_SURVEY from pul_survey s where TO_CHAR(s.FD_SURVEY_DATE, 'MM/YYYY') = '" + sdf.format(surveyDate) + "')\n" +
                        "AND    TO_CHAR(ec.FD_STRUCTURE_DATE, 'MM/YYYY') = '" + sdf.format(surveyDate) + "'\n";
        
        return query;
    }
    
    public static List<MailingList> getZoneDirectors(int surveyId, String storeTypes, 
            String zones, List<String> stores, Date surveyDate)
    throws SQLException, IOException {
        String query = getQueryZoneDirectors(surveyId, storeTypes, zones, stores, surveyDate);

        OracleConnection connection = getOracleConnection();
        CallableStatement statement = connection.prepareCall(query);
        statement.execute();

        ResultSet results = ((OracleCallableStatement) statement).getResultSet();
        
        List<MailingList> dataList = new ArrayList();
        MailingList mail;
        
        while (results.next()) {
            mail = new MailingList();
            
            mail.setUserName(results.getString("USUARIO"));
            mail.setPassword(results.getString("CONTRASENA"));
            mail.setFirstName(results.getString("MANAGER"));
            mail.setLastName(results.getString("MANAGER"));
            mail.setManager(results.getString("MANAGER"));
            mail.setDepartment(results.getString("DEMOGRAFICO"));
            mail.setEmail(results.getString("EMAIL"));
            mail.setEmailCC(results.getString("EMAILCC"));
            mail.setDivision(results.getString("FC_DESC_STORE"));
            
            dataList.add(mail);
        }
        
        statement.close();
        
        return dataList;
    }
    
    public static String getQueryZoneDirectors(int surveyId, String storeTypes,
        String zones, List<String> stores, Date surveyDate)
    throws IOException {
         SimpleDateFormat sdf = new SimpleDateFormat("MM/yyyy");
        String andWhere  = "AND TO_CHAR(st.FD_STRUCTURE_DATE, 'MM/YYYY') = '" + sdf.format(surveyDate) + "' \n";
        
        if (storeTypes != null && !storeTypes.equals("all")) {
            andWhere = "\n AND LOWER(st.FC_DESC_SUBDIV) LIKE LOWER('%" + storeTypes + "%')\n";
        }
        
        if (zones != null && !zones.equals("all")) {
            andWhere = "\n AND st.FC_SUB_DIV = '" + zones + "'\n";
        }
        
        if (stores != null && stores.size() > 0) {
            andWhere = "\n AND st.FN_ID_STORE IN (" + StringUtils.join(stores, ",")  + ")\n";
        }
        
        return getBaseZoneDirectorQuery(surveyId, andWhere);
    }
    
    public static String getBaseZoneDirectorQuery(int surveyId, String andWhere) throws IOException {
        VirtualFile sqlFile = VirtualFile.fromRelativePath("sql/zone-directors.sql");
        String query        = new String(Files.readAllBytes(Paths.get(sqlFile.getRealFile().getPath())));
        
        return query + andWhere;
    }
    
    public static List<MailingList> getDependantsZoneDirector(int surveyId, int userId, Date surveyDate)
    throws SQLException {
        String query = getBaseStoreQuery(surveyId, surveyDate) + "\nAND ec.FN_MANAGER = " + userId + "\n";
        
        OracleConnection connection = getOracleConnection();
        CallableStatement statement = connection.prepareCall(query);
        statement.execute();

        ResultSet results = ((OracleCallableStatement) statement).getResultSet();
        
        List<MailingList> dataList = new ArrayList();
        MailingList mail;
        
        while (results.next()) {
            mail = new MailingList();
            
            mail.setEmail(results.getString("FC_EMAIL"));
            mail.setFirstName(results.getString("FC_MANAGER"));
            mail.setLastName(results.getString("FC_MANAGER"));
            mail.setDepartment(results.getString("DEMOGRAFICO"));
            dataList.add(mail);
        }
        
        statement.close();
        
        return dataList;
    }
    
    public static List<MailingList> getQueryZoneDirectors2(int surveyId)
    throws IOException, SQLException {
        VirtualFile sqlFile = VirtualFile.fromRelativePath("sql/zone-directors.sql");
        String query        = new String(Files.readAllBytes(Paths.get(sqlFile.getRealFile().getPath())));
            
        OracleConnection connection = getOracleConnection();
        CallableStatement statement = connection.prepareCall(query);
        statement.execute();

        ResultSet results = ((OracleCallableStatement) statement).getResultSet();
        
        List<MailingList> dataList = new ArrayList();
        MailingList mail;
        
        while (results.next()) {
            mail = new MailingList();
            
            mail.setUserName(results.getString("USUARIO"));
            mail.setPassword(results.getString("CONTRASENA"));
            mail.setFirstName(results.getString("MANAGER"));
            mail.setLastName(results.getString("MANAGER"));
            mail.setManager(results.getString("MANAGER"));
            mail.setDepartment(results.getString("DEMOGRAFICO"));
            mail.setEmail(results.getString("EMAIL"));
            mail.setEmailCC(results.getString("EMAILCC"));
            mail.setDivision(results.getString("FC_DESC_STORE"));
            dataList.add(mail);
        }
        
        statement.close();
        
        return dataList;
    }
}
