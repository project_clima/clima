package querys;

import java.math.BigDecimal;
import java.sql.Array;
import java.sql.CallableStatement;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import javax.persistence.Query;
import json.entities.TreeData;
import models.Survey;
import play.*;
import oracle.jdbc.OracleCallableStatement;
import oracle.jdbc.OracleConnection;
import oracle.jdbc.OracleTypes;
import oracle.sql.ARRAY;
import oracle.sql.ArrayDescriptor;
import play.db.jpa.JPA;
import static utils.Queries.getOracleConnection;

/**
 * Clase de apoyo para la ejecucion de los querys de Encuestas
 * @author Rodolfo Miranda -- Qualtop
 */
public class SurveysGeneratorQuery {
    /**
     * Metodo para obtener los porcentajes por factores de las calificaciones
     * @param surveyId
     * @return 
     */
    public static int calculatePercentageFactors(int surveyId) {
        int result = 0;

        try {
            OracleConnection connection = getOracleConnection();
            CallableStatement statement = connection.prepareCall("{ ? = call PORTALUNICO.FN_CALCULA_PORCENTAJE_FACTORES(?) }");

            statement.setLong(2, surveyId);
            statement.registerOutParameter(1, OracleTypes.NUMBER);
            statement.execute();

            result = ((OracleCallableStatement) statement).getInt(1);
            statement.close();
        } catch (SQLException e) {
            Logger.error(e.getMessage());
        }

        return result;
    }
    
    /**
     * Metodo para obtener el arbol de estuctura por usuario, fecha de estructura
     * @param userId
     * @param type
     * @param month
     * @param year
     * @param surveyId
     * @return 
     */
    public static List<TreeData> structure(String userId, String type, String month, String year, int surveyId) {        
        String title = "CASE WHEN FN_LEVEL >= 3 \n" +
                       "THEN FC_TITLE || ' - ' || FC_DIVISION \n" +
                       "ELSE FC_TITLE END FC_TITLE";
        
        String strucutureDateCondition = "";
        
        if (month != null && year != null && !month.equals("") && !year.equals("")) {
            strucutureDateCondition = "to_char(FD_STRUCTURE_DATE, 'MMYYYY') = '" + month + year + "'\n";
        }
        
        String statement = "SELECT DISTINCT FN_USERID, FN_MANAGER, FC_TITLE, Manager.Manager, SE.SurveyEmployee FROM\n" +
                            "(\n" +
                            "  SELECT FN_USERID, FN_MANAGER, " + title + ", FN_ID_EMPLOYEE_CLIMA\n" +
                            "  FROM PUL_EMPLOYEE_CLIMA WHERE " + strucutureDateCondition +
                            ") Employees\n" +
                            "\n" +
                            "LEFT JOIN (\n" +
                            "  SELECT FN_MANAGER Manager\n" +
                            "  FROM PUL_EMPLOYEE_CLIMA WHERE " + strucutureDateCondition +
                            ") Manager ON Manager.Manager = Employees.FN_USERID\n" +
                            "LEFT JOIN (\n" +
                            "  SELECT FN_ID_EMPLOYEE_CLIMA SurveyEmployee, FN_ID_SURVEY Survey\n" +
                            "  FROM PUL_SURVEY_EMPLOYEE se1\n" +
                            "  WHERE  se1.FN_ID_SURVEY in (select s.FN_ID_SURVEY from pul_survey s where TO_CHAR(s.FD_SURVEY_DATE, 'MMYYYY')= '" + month + year + "')\n" +
                            ") SE ON SE.SurveyEmployee = Employees.FN_ID_EMPLOYEE_CLIMA AND Survey = " + surveyId + "\n" +
                            "WHERE FN_MANAGER = " + userId;
        
        Query oQuery = JPA.em().createNativeQuery(statement);
        List results = oQuery.getResultList();
        List<TreeData> dataTree = new ArrayList();
        
        TreeData node;
        boolean hasChildren;
        boolean selected;
        
        for (Object result : results) {
            Object[] row = (Object[]) result;    
            
            if (row[0] == null) {
                continue;
            }
            
            if (row[1] == null) {
                row[1] = "#";
            }
            
            hasChildren = row[3] != null;
            selected    = row[4] != null;
            node = new TreeData(row[0].toString(), row[1].toString(), row[2].toString(), hasChildren, selected);
            dataTree.add(node);
        }
        
        return dataTree;
    }
    
    /**
     * Metodo para obtener el inicio del arbol por encuesta
     * @param surveyId
     * @param month
     * @param year
     * @return 
     */
    public static List<TreeData> getRoot(int surveyId, String month, String year) {
        String dateCondition = "";
        
        if (month != null && year != null && !month.equals("") && !year.equals("")) {
            dateCondition = "AND to_char(FD_STRUCTURE_DATE, 'MMYYYY') = '" + month + year + "'\n";
        }
        
        String statement = "SELECT FN_USERID, '#' AS parentNode, FC_TITLE, SurveyEmployee\n" +
                           "FROM PUL_EMPLOYEE_CLIMA Employees\n" +
                           "LEFT JOIN (\n" +
                            "  SELECT FN_ID_EMPLOYEE_CLIMA SurveyEmployee, FN_ID_SURVEY Survey\n" +
                            "  FROM PUL_SURVEY_EMPLOYEE se1\n" +
                            "  WHERE  se1.FN_ID_SURVEY in (select s.FN_ID_SURVEY from pul_survey s where TO_CHAR(s.FD_SURVEY_DATE, 'MMYYYY')= '" + month + year + "')\n" +
                            ") SE ON SE.SurveyEmployee = Employees.FN_ID_EMPLOYEE_CLIMA AND Survey = " + surveyId + "\n" +
                           "WHERE FN_LEVEL = 1 \n" + dateCondition +
                           "ORDER BY FN_USERID";
        
        Query oQuery = JPA.em().createNativeQuery(statement);
        List results = oQuery.getResultList();
        
        List<TreeData> dataTree = new ArrayList();
        boolean selected;
        TreeData node;
        
        for (Object result : results) {
            Object[] row = (Object[]) result;
            
            selected = row[3] != null;
            node = new TreeData(row[0].toString(), "#", row[2].toString(), true, selected);
            dataTree.add(node);
        }
        
        return dataTree;
    }
    
    /**
     * Metodo de apoyo en el query cuando es coorporativo
     * @return 
     */
    public static String whereCorporative() {
        String where = "WHERE  FN_STATUS_EMPLOYEE = 1\n" +
                        "AND    FN_KEY_PLANT IN ('A','B') \n" +
                        "AND   FC_MANAGEMENT    IS NOT NULL\n" +
                        "AND   upper(FC_MANAGEMENT)    NOT LIKE upper('%Dirección Corporativa Compras Hard-Line%')\n" +
                        "AND   upper(FC_MANAGEMENT)    NOT LIKE upper('%Dirección Corporativa Compras Soft-Line%')\n" +
                        "AND  (upper(FC_DIVISION)      like upper('%Servicios Liverpool%') \n" +
                        "OR    upper(FC_DIVISION)      like upper('%CORP%')\n" +
                        "OR    upper(FC_DIVISION)      like upper('%Centro de Atención Telefónico%'))\n" +
                        "AND  (upper(FC_DESC_LOCATION) like upper('%Servicios Liverpool%')\n" +
                        "OR    upper(FC_DESC_LOCATION) like upper('%Centro de Atención Telefónico%'))\n";
        
        return where;
    }
    
    /**
     * Metodo de apoyo en el query cuando es de tienda
     * @return 
     */
    public static String whereStore() {
        String where = "WHERE  FN_STATUS_EMPLOYEE = 1\n" +
                        "AND    FN_KEY_PLANT in ('A','B') \n" +
                        "  OR UPPER(FC_DIVISION)  like UPPER('%Adcon%')\n" +
                        "  OR UPPER(FC_DIVISION)  like UPPER('%Bodega%') \n" +
                        "  OR  UPPER(FC_DIVISION)  like UPPER('%Fabricas%') \n" +
                        "  OR  UPPER(FC_DIVISION)  like UPPER('%Liverpool%')\n" +
                        "  OR  UPPER(FC_DIVISION)  IN (\n" +
                        "    SELECT UPPER(fc_desc_store) \n" +
                        "    FROM   PUL_STORE \n" +
                        "    WHERE  FC_SUB_DIV LIKE UPPER('%BT%')\n" +
                        "  )\n" +
                        "AND   (FC_AREA NOT LIKE '%VAD%' OR FC_AREA IS NULL)";
        
        return where;
    }
    
    /**
     * MEtodo de apoyo cuando la encuesta es de compra
     * @return 
     */
    public static String wherePurchase() {
        String where = "WHERE  FN_STATUS_EMPLOYEE = 1 \n" +
                        "AND    FN_KEY_PLANT in ('A','B') \n" +
                        "AND    UPPER(FC_DESC_LOCATION) like UPPER('%Servicios Liverpool%')\n" +
                        "AND   (UPPER(FC_DIVISION)      like UPPER('%Servicios Liverpool%') \n" +
                        "OR     UPPER(FC_DIVISION)      like UPPER('%CORP%'))\n" +
                        "AND   (UPPER(FC_MANAGEMENT)    like UPPER('%Dirección Corporativa Compras Hard-Line%')\n" +
                        "OR     UPPER(FC_MANAGEMENT)    like UPPER('%Dirección Corporativa Compras Soft-Line%'))";

        return where;
    }
    
    /**
     * MEtodo de apoyo par las condiciones "cuando"
     * @param type
     * @param month
     * @param year
     * @return 
     */
    public static String getWhereCondition(String type, String month, String year) {
        String strucutureDateCondition = "";
        String date = "";
        
        if (month != null && year != null && !month.equals("") && !year.equals("")) {
            date = month + year;
            strucutureDateCondition = "\nAND to_char(FD_STRUCTURE_DATE, 'MMYYYY') = '" + date + "'";
        }
        
        if (type.equals("1")) {
            return whereStore() + strucutureDateCondition;
        }
        
        if (type.equals("2")) {
            return whereCorporative() + strucutureDateCondition;
        }
        
        if (type.equals("3")) {
            return wherePurchase() + strucutureDateCondition;
        }
        
        return "";
    }
    
    /**
     * MEtodo de utileria para saber el año de la encuesta
     * @param type
     * @return 
     */
    public static int getYearStructure(String type) {
        String aggregate = type.equals("max") ? "MAX" : "MIN";
        String statement = "SELECT " + aggregate + "(EXTRACT(year FROM FD_STRUCTURE_DATE)) FROM PUL_EMPLOYEE_CLIMA";
        
        Query oQuery   = JPA.em().createNativeQuery(statement);
        BigDecimal result = (BigDecimal) oQuery.getSingleResult();
        
        return result.intValue();
    }
    
    /**
     * 
     * @param type
     * @return 
     */
    public static int getYearCsiStructure(String type) {
        String aggregate = type.equals("max") ? "MAX" : "MIN";
        String statement = "SELECT " + aggregate + "(EXTRACT(year FROM FD_STRUCTURE_DATE)) FROM PORTALUNICO.PUL_EMPLOYEE_CSI";
        
        Query oQuery   = JPA.em().createNativeQuery(statement);
        BigDecimal result = (BigDecimal) oQuery.getSingleResult();
        
        return result.intValue();
    }
    
    /**
     * Metodo para guardar los participantes por encuesta
     */
    public static int saveSurveyEmployees(Survey survey, String userIds, String month, String year) {
        int result = 0;
        SimpleDateFormat sdf = new SimpleDateFormat("MMyyyy");
        String period;
        
        if (month == null || month.equals("") || year == null || year.equals("")) {
            period = sdf.format(survey.surveyDate != null ? survey.surveyDate : new Date());
        } else {
            period = month + year;
        }
        
        try {
            OracleConnection connection = getOracleConnection();
            CallableStatement statement = connection.prepareCall("{ ? = call PORTALUNICO.FN_INS_SURVEY_EMPLOYEE_SEL(?, ?, ?) }");
        
            Array IDs = new ARRAY(
                ArrayDescriptor.createDescriptor("PORTALUNICO.ARRAY_STRING_TABLE", connection),
                connection,
                userIds.split(",")
            );

            statement.setInt(2, survey.id);
            statement.setArray(3, IDs);
            statement.setString(4, period);
            statement.registerOutParameter(1, OracleTypes.NUMBER);
            statement.execute();

            result = ((OracleCallableStatement) statement).getInt(1);
            statement.close();
        } catch (SQLException e) {
            Logger.error(e.getMessage());
        }

        return result;
    }
    
    /**
     * Metodo para eliminar empleado de una encuesta
     * @param surveyId
     * @param userIds
     * @return 
     */
    public static int deleteSurveyEmployees(int surveyId, String userIds) {
        if (userIds == null || userIds.equals("")) {
            return 0;
        }
        
        String statement = "DELETE FROM PUL_SURVEY_EMPLOYEE WHERE FN_ID_SURVEY = ?1 AND FN_ID_EMPLOYEE_CLIMA IN (\n" +
                            "  SELECT DISTINCT FN_ID_EMPLOYEE_CLIMA FROM PUL_EMPLOYEE_CLIMA WHERE FN_USERID IN (\n" +
                            userIds + " \n" +
                            "  )\n" +
                            ")";
        
        Query oQuery   = JPA.em().createNativeQuery(statement);
        oQuery.setParameter(1, surveyId);
        
        return oQuery.executeUpdate();
    }
    
    /**
     * Metodo para actualizar los subfactores de una encuesta
     * @param surveyId
     * @param originalSubfactor
     * @param newSubfactor
     * @return 
     */
    public static int updateSubfactors(int surveyId, int originalSubfactor, int newSubfactor) {
        String statement = "UPDATE PUL_QUESTION SET FN_ID_SUBFACTOR = ?1 WHERE FN_ID_SURVEY = ?2 AND FN_ID_SUBFACTOR = ?3";
        
        Query oQuery   = JPA.em().createNativeQuery(statement);
        oQuery.setParameter(1, newSubfactor);
        oQuery.setParameter(2, surveyId);
        oQuery.setParameter(3, originalSubfactor);
        
        return oQuery.executeUpdate();
    }
    
    /**
     * Metodo para copiar empleados de una encuestas a otra
     * @param originalId
     * @param copiedId
     * @param username
     * @return 
     */
    public static int copySurveyEmployees(int originalId, int copiedId, String username) {
        int result = 0;
        
        try {
            OracleConnection connection = getOracleConnection();
            CallableStatement statement = connection.prepareCall("{ ? = call PORTALUNICO.FN_COPY_SURVEY_EMPLOYEE(?, ?, ?) }");

            statement.setInt(2, originalId);
            statement.setInt(3, copiedId);
            statement.setString(4, username);
            statement.registerOutParameter(1, OracleTypes.NUMBER);
            statement.execute();

            result = ((OracleCallableStatement) statement).getInt(1);
            statement.close();
        } catch (SQLException e) {
            Logger.error(e.getMessage());
        }

        return result;
    }
    
    /**
     * MEtodo para eliminar los resultados de una evaluacion por subfactores
     * @param surveyId
     * @return 
     */
    public static int deleteSubfactorScore(int surveyId) {        
        String statement = "DELETE FROM PUL_SUBFACTOR_SCORE WHERE FN_ID_SURVEY = ?1";
        
        Query oQuery   = JPA.em().createNativeQuery(statement);
        oQuery.setParameter(1, surveyId);
        
        return oQuery.executeUpdate();
    }
}
