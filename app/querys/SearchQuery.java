package querys;

import java.sql.CallableStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import play.*;
import play.db.jpa.JPA;
import java.util.*;
import java.util.logging.Level;
import javax.persistence.Query;
import json.entities.FortalezasList;
import json.entities.ResumeTable;
import models.Survey;
import oracle.jdbc.OracleCallableStatement;
import oracle.jdbc.OracleConnection;
import oracle.jdbc.OracleTypes;
import org.apache.commons.lang3.StringUtils;
import play.mvc.Scope;
import utils.Queries;
import static utils.Queries.getOracleConnection;

/**
 * Metodo de utileria para ejecucion de querys de la Busqueda de Resultados
 * @author Rodolfo Miranda -- Qualtop
 */
public class SearchQuery {
    
    /**
     * Metodo que ejecuta el query para obtener la busqueda de resultados
     * @param search
     * @param date
     * @param start
     * @param end
     * @param paginate
     * @return 
     */
    public static List search(String search, String date, int start, int end, boolean paginate) {
        String where     = paginate ? "where rn between " + start + " AND " + end : "";
        
        String statement = "select * from\n" +
                            "   (\n" +
                "select a1.fn_employee_number                      as NUM_EMP,\n" +
                            "       a1.nombre                                  as NOMBRE,\n" +
                            "       a1.fc_title                                as PUESTO,\n" +
                            "       ecs.FN_TODO_SURVEY                         as NUM_ENCUESTAS,\n" +
                            "       ecs.FN_TREE_SCORE                          as CALIFICACION_CLIMA,\n" +
                            "       ecs.FN_TREE_LEADER_SCORE                   as CALIFICACION_LIDERAZGO,\n" +
                            "       ROW_NUMBER() OVER (ORDER BY null) rn\n" +
                            "from (select fc_first_name ||' '||fc_last_name nombre,\n" +
                            "             fc_title, fc_email, fn_userid, fn_id_employee_clima, fn_employee_number, fn_manager, fd_structure_date\n" +
                            "      from portalunico.pul_employee_clima where to_char(fd_structure_date,'MM/YYYY') = '"+date+"') a1\n" +
                            "join pul_employee_clima_score ecs on ecs.fn_id_employee_clima = a1.fn_id_employee_clima\n" +
                            "where (TO_CHAR(a1.fn_employee_number) = nvl('" + search + "', TO_CHAR(a1.fn_employee_number)) \n" +
                            "or ( a1.fn_userid in (select fn_id_user from pul_user where UPPER(fc_username) like UPPER('" + search + "')) OR '" + search + "' IS NULL)) \n" +
                            "and to_char(ecs.fd_structure_date,'MM/YYYY') = '"+date+"'\n" +
                            "and nvl(ecs.FN_TODO_SURVEY,0) > 0 \n" +
                            "order by 2\n" +                          
                            ")tablepaginate \n" + 
                            where;
        
        Query oQuery = JPA.em().createNativeQuery(statement);
        
        return oQuery.getResultList();        
    }
    
    /**
     * Metodo para obtener el total de resultados segun filtros seleccionados
     * @param search
     * @param date
     * @return 
     */
    public static long count(String search, String date) {
        String query = "select count(1)\n" +
                        "  from (select a.fn_userid                               as USERID,\n" +
                        "               portalunico.fn_get_final_score(a.fn_userid, '09/2016')    as CALIFICACION_CLIMA,\n" +
                        "               portalunico.fn_get_final_leadership_score(a.fn_userid, '09/2016', 'Liderazgo') as CALIFICACION_LIDERAZGO\n" +
                        "          from portalunico.pul_employee_clima a\n" +
                        "         where a.fn_employee_number = nvl('" + search + "', a.fn_employee_number)\n" +
                        "           or substr(a.fc_email,0,instr(a.fc_email,'@') - 1) = nvl('" + search + "', substr(a.fc_email,0,instr(a.fc_email,'@') - 1) )\n" +
                        "         start with a.fn_userid = 50092262\n" +
                        "       connect by prior a.fn_userid = a.fn_manager\n" +
                        "       ) tab\n" +
                        "       where tab.calificacion_clima is not null\n" +
                        "       and tab.calificacion_liderazgo is not null\n" +
                        "       and portalunico.fn_get_todo_survey('" + date + "', tab.userid) > 0\n" +
                        "       and portalunico.fn_get_survey_resolved('" + date + "', tab.userid) > 0\n";
        
        Query oQuery = JPA.em().createNativeQuery(query);
        List result = oQuery.getResultList();
        return Long.parseLong(result.get(0).toString());
    }
    
    /**
     * Metodo para obtener la calificacion global de Clima, segun fecha de 
     * estructura y nombre de usuario
     * @param structureDate
     * @param username
     * @return 
     */
    public static int climaScore(String structureDate, String username) {
        int result = 0;
        try {
            OracleConnection connection = Queries.getOracleConnection();
            
            CallableStatement statement = 
                    connection.prepareCall("{? = call PORTALUNICO.FN_INS_EMPLOYEE_CLIMA_SCORE(?, ?) }");
            
            statement.setString(2, structureDate);
            statement.setString(3, username);
            statement.registerOutParameter(1, OracleTypes.NUMBER);
            statement.execute();
            
            result = ((OracleCallableStatement) statement).getInt(1);
            statement.close();
            
            
        } catch (SQLException ex) {
            Logger.info(ex.getMessage());
        }
        return result;
    }
}
