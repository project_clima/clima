package querys;

import play.*;
import play.db.jpa.JPA;
import java.util.*;
import javax.persistence.Query;
import models.Survey;

/**
 * Clase de utileria para querys especiales
 * @author Rodolfo Miranda -- Qualtop
 */
public class SpecialQuery {
    /**
     * Metodo para obtener los niveles de la estructura de clima
     * @return 
     */
    public static List getLevels() {
        String statement = "SELECT FN_LEVEL FROM PUL_EMPLOYEE_CLIMA GROUP BY FN_LEVEL ORDER BY FN_LEVEL ASC";
        
        Query oQuery = JPA.em().createNativeQuery(statement);

        return oQuery.getResultList();
    }

    /**
     * Metodo de utileria para obtner la llave especial y nombre especial
     * @return 
     */
     public static List getVie() {
        String statement = "SELECT FN_KEY_SPECIAL,FC_TYPE_SPECIAL_NAME FROM PUL_TYPE_SPECIAL order by FN_ORDER";
        
        Query oQuery = JPA.em().createNativeQuery(statement);

        return oQuery.getResultList();
    }
}
