package querys;

import java.math.BigDecimal;
import java.sql.CallableStatement;
import java.sql.SQLException;
import play.*;
import play.db.jpa.JPA;
import java.util.*;
import javax.persistence.Query;
import oracle.jdbc.OracleCallableStatement;
import oracle.jdbc.OracleConnection;
import oracle.jdbc.OracleTypes;
import utils.PagingResult;
import static utils.Queries.getOracleConnection;
import play.mvc.Scope.Session;

/**
 * Clase de utileria para ejecucion de querys en el manejo de la estructura de Clima
 * @author Rodolfo Miranda -- Qualtop
 */
public class StructureQuery {

    /**
     * Metodo para exportar los resultados en la estructura
     * @param conditions
     * @param structureDate
     * @return 
     */
    public static List getExport(String conditions, String structureDate) {

        String statement = " SELECT TBL.FN_EMPLOYEE_NUMBER,\n" +
            "       U.FC_USERNAME,\n" +
            "       TBL.FC_FIRST_NAME, \n" +
            "       TBL.FC_LAST_NAME, \n" +
            "       TBL.FC_FUNCTION, \n" +
            "       TBL.FC_DEPARTMENT, \n" +
            "       TBL.FN_LEVEL, \n" +
            "       RE.FC_FIRST_NAME ||' '|| RE.FC_LAST_NAME, \n" +
            "       SE.FN_PLAN , SE.FN_ANSWERED\n" +
            "FROM (\n" +
            "      select distinct rownum renglon,\n" +
            "             FN_EMPLOYEE_NUMBER, \n" +
            "             FC_FIRST_NAME, \n" +
            "             FC_LAST_NAME, \n" +
            "             FC_FUNCTION, \n" +
            "             FC_DEPARTMENT, \n" +
            "             FN_LEVEL, \n" +
            "             FN_MANAGER, \n" +
            "             FN_ID_EMPLOYEE_CLIMA, \n" +
            "             FD_STRUCTURE_DATE,\n" +
            "             FN_USERID,\n" +
            "            connect_by_root FN_USERID as root_id \n" +
            "      from (select FN_EMPLOYEE_NUMBER, FC_FIRST_NAME, FC_LAST_NAME, FC_FUNCTION, FC_DEPARTMENT, FN_LEVEL, FN_MANAGER, FN_ID_EMPLOYEE_CLIMA, FD_STRUCTURE_DATE, FN_USERID \n" +
            "            from PUL_EMPLOYEE_CLIMA where to_char(fd_structure_date,'MM/YYYY') = :FD_DATE) emp       \n" +
            "      connect by prior FN_USERID = FN_MANAGER\n" +
            "      start with FN_USERID in (select FN_USERID from pul_employee_clima where FN_LEVEL = 1 and to_char(fd_structure_date,'MM/YYYY') = :FD_DATE)      \n" +
            "      )TBL \n" +
            "JOIN PUL_EMPLOYEE_CLIMA RE ON RE.FN_USERID = TBL.FN_MANAGER AND RE.FD_STRUCTURE_DATE = TBL.FD_STRUCTURE_DATE\n" +
            "JOIN PUL_USER U  ON TBL.FN_USERID = U.FN_ID_USER      \n" +
            "LEFT JOIN PUL_SURVEY_EMPLOYEE SE ON SE.FN_ID_EMPLOYEE_CLIMA = TBL.FN_ID_EMPLOYEE_CLIMA  \n" +
            "ORDER by TBL.renglon";

        Query oQuery = JPA.em().createNativeQuery(statement);
        oQuery.setParameter("FD_DATE", structureDate);
        return oQuery.getResultList();
    }

    /**
     * Metodo que obtiene la estructura completa segun filtros seleccionados
     * @param conditions
     * @param order
     * @param start
     * @param end
     * @param structureDate
     * @return 
     */
    public static PagingResult getStructure(String conditions, String order, int start, int end, String structureDate) {
        String statement =
            " SELECT TBL.renglon, TBL.FN_EMPLOYEE_NUMBER, U.FC_USERNAME, TBL.FC_FIRST_NAME, TBL.FC_LAST_NAME, TBL.FC_FUNCTION, TBL.FC_DEPARTMENT, TBL.FN_LEVEL, " +
            "nvl((select FC_FIRST_NAME ||' '|| FC_LAST_NAME from pul_employee_clima where fn_userid = TBL.FN_MANAGER and to_Char(fd_structure_date,'MM/YYYY') = '"+ structureDate +"') ,'NO_MANAGER') NOM_JEFE, "+
            " (SELECT SE.FN_PLAN " +
            "FROM PUL_SURVEY_EMPLOYEE SE " +
            "WHERE SE.FN_ID_EMPLOYEE_CLIMA= TBL.FN_ID_EMPLOYEE_CLIMA " +
            "AND ROWNUM                   < 2), " +
            "(SELECT SE.FN_ANSWERED " +
            "FROM PUL_SURVEY_EMPLOYEE SE " +
            "WHERE SE.FN_ID_EMPLOYEE_CLIMA= TBL.FN_ID_EMPLOYEE_CLIMA " +
            "AND ROWNUM                   < 2), " +
            "(SELECT SE.FN_ID_SURVEY " +
            "FROM PUL_SURVEY_EMPLOYEE SE " +
            "WHERE SE.FN_ID_EMPLOYEE_CLIMA= TBL.FN_ID_EMPLOYEE_CLIMA " +
            "AND ROWNUM                   < 2)"+
            " FROM ( " +
            " select rownum as renglon, FN_EMPLOYEE_NUMBER, FC_FIRST_NAME, FC_LAST_NAME, FC_FUNCTION," +
            " FC_DEPARTMENT, FN_LEVEL, FN_MANAGER, FN_ID_EMPLOYEE_CLIMA, FN_USERID" +
            " from (SELECT * FROM PUL_EMPLOYEE_CLIMA" +
            " WHERE TO_CHAR (fd_structure_date, 'MM/YYYY') = '" + structureDate + "') emp" +
            " start with emp.FN_USERID in ( " +
            " select FN_USERID " +
            " from pul_employee_clima where " + conditions +
            " ) connect by prior emp.FN_USERID = emp.FN_MANAGER)TBL " +
            " JOIN PUL_USER U ON TBL.FN_USERID = U.FN_ID_USER " +
            "  where TBL.renglon BETWEEN " + start + " AND " + end +
            "  order by TBL.renglon asc ";

        Query oQuery = JPA.em().createNativeQuery(statement);
        return new PagingResult(getCountRows(conditions, structureDate), oQuery.getResultList());
    }

    /**
     * Metodo para obtener los empleados, segun las condiciones seleccionadas
     * @param conditions
     * @param order
     * @param start
     * @param end
     * @return 
     */
    public static PagingResult getEmployees(String conditions, String order, int start, int end) {
        String statement = "select * " +
        " from (  select  rownum as renglon, ec.FN_EMPLOYEE_NUMBER, ec.FC_FIRST_NAME, ec.FC_LAST_NAME, ec.FC_FUNCTION, ec.FC_DEPARTMENT, ec.FN_LEVEL, em.FC_FIRST_NAME || ' ' || em.FC_LAST_NAME as manager" +
        " from pul_employee_clima ec " +
        " inner join pul_employee_clima em on ec.FN_MANAGER = em.FN_USERID ";
        if (conditions != "") {
            statement += " where " + conditions;
        }
        statement += ") where renglon BETWEEN " + start + " AND " + end + " " + order;

        Query oQuery = JPA.em().createNativeQuery(statement);

        return new PagingResult(getCountRows(conditions, ""), oQuery.getResultList());
    }


    /**
     * Metodo para obtener el total de registros por fecha de estructura
     * @param conditions
     * @param structureDate
     * @return 
     */
    public static int getCountRows(String conditions, String structureDate) {
        String statement =  " select count(1) from (" +
                            " select rownum as renglon, FN_USERID, FC_FIRST_NAME, FC_LAST_NAME, FC_FUNCTION, FC_DEPARTMENT, FN_LEVEL, FN_MANAGER" +
                            //" connect_by_root FN_USERID as root_id " +
                            " from (SELECT * FROM PUL_EMPLOYEE_CLIMA" +
                            " WHERE TO_CHAR (fd_structure_date, 'MM/YYYY') = '" + structureDate + "') " +
                            " connect by prior FN_USERID = FN_MANAGER " +
                            " start with FN_USERID in ( " +
                                " select FN_USERID " +
                                " from pul_employee_clima where " + conditions +
                            " ))";

        Query oQuery = JPA.em().createNativeQuery(statement);
        List result = oQuery.getResultList();
        return Integer.parseInt(result.get(0).toString());
    }

    /**
     * Metodo de utileria para el ordenamiento
     * @param sSortDir_0
     * @param iSortCol_0
     * @return 
     */
    public static String getOrder(String sSortDir_0, int iSortCol_0) {
        String order;

        switch (iSortCol_0) {
            case 0 : order = "ORDER BY FN_LEVEL " + sSortDir_0;
                break;
            case 1 : order = "ORDER BY FN_EMPLOYEE_NUMBER " + sSortDir_0;
                break;
            case 2 : order = "ORDER BY FC_FIRST_NAME " + sSortDir_0;
                break;
            case 3 : order = "ORDER BY FC_LAST_NAME " + sSortDir_0;
                break;
            case 4 : order = "ORDER BY FC_FUNCTION " + sSortDir_0;
                break;
            case 5 : order = "ORDER BY FC_DEPARTMENT " + sSortDir_0;
                break;
            case 6 : order = "ORDER BY FN_LEVEL " + sSortDir_0;
                break;
            case 7 : order = "ORDER BY manager " + sSortDir_0;
                break;
            default : order = "ORDER BY FN_LEVEL " + sSortDir_0;
                break;
        }
        return order;
    }

    /**
     * Metodo para obtener las ubicaciones por fecha de estructura
     * @param date
     * @return 
     */
    public static List getLocation(String date) {
        String statement = " select  distinct FC_DESC_LOCATION " +
                            " from pul_employee_clima ORDER by FC_DESC_LOCATION";

        if(date != null && date.length() > 0) {
            statement =
                " select  distinct FC_DESC_LOCATION " +
                " from " +
                " (SELECT * FROM PUL_EMPLOYEE_CLIMA" +
                " WHERE TO_CHAR (fd_structure_date, 'MM/YYYY') = '" + date + "')" +
                "ORDER by FC_DESC_LOCATION";
        }

        Query oQuery = JPA.em().createNativeQuery(statement);

        return oQuery.getResultList();
    }

    /**
     * Metodo para obtener los departamentos por ubicaciones en clima
     * @param location
     * @param date
     * @return 
     */
    public static List getDepartments(String location, String date) {
        String statement = " select distinct FC_DEPARTMENT " +
                            " from PUL_EMPLOYEE_CLIMA " +
                            " where FC_DESC_LOCATION = '" + location + "' order by FC_DEPARTMENT";
        if(date != null && date.length() > 0) {
            statement =
            " select distinct FC_DEPARTMENT " +
            " from " +
            " (SELECT * FROM PUL_EMPLOYEE_CLIMA" +
            " WHERE TO_CHAR (fd_structure_date, 'MM/YYYY') = '" + date + "')" +
            " where FC_DESC_LOCATION = '" + location + "' order by FC_DEPARTMENT";
        }

        Query oQuery = JPA.em().createNativeQuery(statement);

        return oQuery.getResultList();
    }

    /**
     * Metodo para obtener los departamentos para el nivel 1 de los filtros
     * @param level
     * @param date
     * @return 
     */
    public static List getDepartmentsByLevel1(int level, String date) {
        String statement = "select FC_TITLE, FN_USERID, FC_DEPARTMENT, " +
                           " COALESCE(SUBSTR(FC_EMAIL , 0, INSTR(FC_EMAIL, '@') - 1), FC_EMAIL) FC_USERNAME " +
                           " from PUL_EMPLOYEE_CLIMA " +
                           " where FN_LEVEL = " + level + "\n" +
                           " AND TO_CHAR (fd_structure_date, 'MM/YYYY') = '" + date + "'\n" +
                           " order by FC_TITLE";

        Query oQuery = JPA.em().createNativeQuery(statement);

        return oQuery.getResultList();
    }

    /**
     * Metodo para obtener los departamentos segun filtros seleccionados
     * @param userId
     * @param date
     * @param withDependants
     * @return 
     */
    public static List getDepartmentsByLevel(int userId, String date, boolean withDependants) {
        String statement = "select decode(TBL.fc_desc_location,'Servicios Liverpool',TBL.FC_TITLE, TBL.FC_TITLE||' - '||TBL.fc_desc_location) FC_TITLE, FN_USERID, FC_DEPARTMENT " +
                            " from " +
                            " (select  FC_TITLE, FC_DIVISION, FC_DESC_LOCATION, FN_USERID, FC_DEPARTMENT " +
                            " from PUL_EMPLOYEE_CLIMA " +
                            " where   FN_MANAGER = " + userId +
                            " ) TBL order by FC_TITLE";
        
        String andWhere = withDependants == true ? 
                            "  WHERE ((SELECT COUNT(1) FROM PUL_EMPLOYEE_CLIMA SEC WHERE SEC.FN_MANAGER = TBL.FN_USERID) > 0 OR\n" +
                            "  PORTALUNICO.FN_GET_SURVEY_RESOLVED('" + date + "', TBL.FN_USERID) > 2 )\n" : "";
        
        if(date != null && !date.equals("null") && !date.isEmpty()) {
            statement = "select \n" +
                        "  decode(TBL.fc_desc_location, 'Servicios Liverpool', TBL.FC_TITLE, TBL.FC_TITLE || ' - ' || TBL.fc_desc_location) FC_TITLE,\n" +
                        "  FN_USERID,\n" +
                        "  FC_DEPARTMENT, \n" +
                        "  FC_USERNAME \n" +
                        "from (\n" +
                        "  SELECT\n" +
                        "    EC.FC_TITLE, EC.FC_DIVISION, EC.FC_DESC_LOCATION, EC.FN_USERID, EC.FC_DEPARTMENT,\n" +
                        "    COALESCE(SUBSTR(FC_EMAIL , 0, INSTR(FC_EMAIL, '@') - 1), FC_EMAIL) FC_USERNAME \n" +
                        "  FROM \n" +
                        "    PUL_EMPLOYEE_CLIMA EC\n" +
                        "  WHERE TO_CHAR (fd_structure_date, 'MM/YYYY') = '" + date + "' AND FN_MANAGER = " + userId +"\n" +
                        ") TBL \n" +
                        andWhere +
                        "order by FC_TITLE";
        }

        Query oQuery = JPA.em().createNativeQuery(statement);

        return oQuery.getResultList();
    }
    
    public static List getDepartmentsByLevelAllowed(int userId, String date) {
        String statement = "";
        
        if(date != null && !date.equals("null") && !date.isEmpty()) {
            String isAdmin  = Session.current().get("isAdmin");
            String andWhere = "  AND ((PORTALUNICO.FN_GET_TODO_SURVEY('" + date + "', EC.FN_USERID) > 2\n" +
                              "           AND  PORTALUNICO.FN_GET_SURVEY_RESOLVED('" + date + "', EC.FN_USERID) > 2 )\n" +
                              "            OR PORTALUNICO.FN_GET_SURVEY_RESOLVED('" + date + "', EC.FN_USERID) > 2  )\n";
            
            if (isAdmin.equals("true")) {
                andWhere = "";
            }
            
            statement = "select \n" +
                            "  decode(TBL.fc_desc_location, 'Servicios Liverpool', TBL.FC_TITLE, TBL.FC_TITLE || ' - ' || TBL.fc_desc_location) FC_TITLE,\n" +
                            "  FN_USERID,\n" +
                            "  FC_DEPARTMENT, FC_USERNAME, FN_SURVEYS, FN_SURVEYS_ANSWERED \n" +
                            "from (\n" +
                            "  SELECT\n" +
                            "    EC.FC_TITLE, EC.FC_DIVISION, EC.FC_DESC_LOCATION, EC.FN_USERID, EC.FC_DEPARTMENT,\n" +
                            "    COALESCE(SUBSTR(FC_EMAIL , 0, INSTR(FC_EMAIL, '@') - 1), FC_EMAIL) FC_USERNAME,\n" +
                            "    PORTALUNICO.FN_GET_TODO_SURVEY('" + date + "', FN_USERID) FN_SURVEYS,\n" +
                            "    PORTALUNICO.FN_GET_SURVEY_RESOLVED('" + date + "', FN_USERID) FN_SURVEYS_ANSWERED\n" +
                            "  FROM \n" +
                            "    PUL_EMPLOYEE_CLIMA EC\n" +
                            "  WHERE TO_CHAR (fd_structure_date, 'MM/YYYY') = '" + date + "' AND FN_MANAGER = " + userId + "\n" +
                            "  AND PORTALUNICO.FN_GET_TODO_SURVEY('" + date + "', EC.FN_USERID) > 0 \n" +
                            andWhere +
                            "  ) TBL order by FC_TITLE";
        }

        Query oQuery = JPA.em().createNativeQuery(statement);
        
        return oQuery.getResultList();
    }

    /**
     * MEtodo para obtener mensaje de correo y usuario
     * @param userId
     * @return 
     */
    public static List getInfoModal(Long userId){
        String statement = "select ec.fc_email, pu.FC_USERNAME " +
                            " from PUL_EMPLOYEE_CLIMA ec " +
                            " inner join PUL_USER pu on pu.FN_ID_USER = ec.FN_USERID " +
                            " where ec.FN_EMPLOYEE_NUMBER = " + userId;

        Query oQuery = JPA.em().createNativeQuery(statement);

        return oQuery.getResultList();
    }

    /**
     * Metodo para obtener los gerentes por empleado, nombre de usuario y id de gerente
     * @param managerId
     * @param managerName
     * @param userIdEmployee
     * @return 
     */
    public static List getManagers(Long managerId, String managerName, int userIdEmployee) {
        String cond1 = " ec.FN_EMPLOYEE_NUMBER = " + managerId, cond2 = "1 > 0";

        if (managerName != null && managerName.length() > 0) {
            cond2 = " pu.FC_USERNAME = '" + managerName + "'";
            cond1 = "1 > 0";
        }

        String statement =
            " select ec.*, pu.FC_USERNAME " +
            " from PUL_EMPLOYEE_CLIMA ec" +
            " inner join PUL_USER pu on pu.FN_ID_USER = ec.FN_USERID " +
            " where " + cond1 + " AND " + cond2 + " AND ec.fn_manager != " + userIdEmployee;

        Query oQuery = JPA.em().createNativeQuery(statement);

        return oQuery.getResultList();
    }

    /**
     * Metodo para el cambio de gerente
     * @param userId
     * @param newManagerId
     * @param sessionUser
     * @param structureDate
     * @return
     * @throws SQLException 
     */
    public static int setManager(Integer userId, Integer newManagerId, 
            String sessionUser, String structureDate) throws SQLException{
//        int managerId = getUserIdByEmployeeId(newManagerId);
//
//        String statement = " Update PUL_EMPLOYEE_CLIMA set FD_MOD_DATE = SYSDATE, FC_MOD_FOR = '" + sessionUser + "', FN_MANAGER = " + managerId + " where FN_EMPLOYEE_NUMBER = " + userId;
//        return JPA.em().createNativeQuery(statement).executeUpdate();
        
        OracleConnection connection = getOracleConnection();

        String sql="begin ?:= PORTALUNICO.FN_UPD_LEVEL_PLAN(?, ?, ?, ?); end;";
        CallableStatement stmt = connection.prepareCall(sql);
        stmt.registerOutParameter(1, OracleTypes.INTEGER);
        stmt.setInt(2,userId);
        stmt.setInt(3, newManagerId);
        stmt.setString(4, structureDate);
        stmt.setString(5, sessionUser);
        stmt.execute();
        int recalcLevel = stmt.getInt(1);
        stmt.close();
        return recalcLevel;
    }

    /**
     * Metodo para la busqueda de empleado por id
     * @param employeeNumber
     * @return 
     */
    public static int getUserIdByEmployeeId(Long employeeNumber){
        String statement = " select FN_USERID from " +
                            " PUL_EMPLOYEE_CLIMA " +
                            " where FN_EMPLOYEE_NUMBER = ? ";

        BigDecimal userId =  (BigDecimal) JPA.em().createNativeQuery(statement)
                .setParameter(1, employeeNumber)
                .getSingleResult();

        return userId.intValue();
    }

    /**
     * MEtodo para obtener listado de funciones
     * @return 
     */
    public static List getFunctions(){
        String statement = " select DISTINCT FC_FUNCTION " +
                            " from PUL_EMPLOYEE_CLIMA order by FC_FUNCTION";

        Query oQuery = JPA.em().createNativeQuery(statement);

        return oQuery.getResultList();
    }

    /**
     * Metodo para almacenar nueva funcion por id de usuario
     * @param userId
     * @param newFunction
     * @param sessionUser
     * @return 
     */
    public static int setFunction(Long userId, String newFunction, String sessionUser) {
        String statement = " Update PUL_EMPLOYEE_CLIMA set FD_MOD_DATE = SYSDATE, FC_MOD_FOR= ?, FC_FUNCTION = ? where FN_EMPLOYEE_NUMBER = ?";
        return JPA.em().createNativeQuery(statement)
                .setParameter(1, sessionUser)
                .setParameter(2, newFunction)
                .setParameter(3, userId)
                .executeUpdate();
    }

    /**
     * Metodo para obtener la busqueda por username
     * @param userName
     * @return 
     */
    public static List getUserIdByUsername (String userName){
        String statement = " select FN_ID_USER from " +
                            " PUL_USER " +
                            " where FC_USERNAME like '%" + userName + "%' ";

        List userId =  JPA.em().createNativeQuery(statement)
                .getResultList();

        return userId;
    }


    public static int getUserByUsername (String userName){ 
        String statement = " select FN_ID_USER from " + 
                            " PUL_USER " + 
                            " where FC_USERNAME like '%" + userName + "%' "; 

        BigDecimal userId =  (BigDecimal) JPA.em().createNativeQuery(statement) 
                .getSingleResult(); 

        return userId.intValue(); 
    } 


    /**
     * Metodo para obtener las diferentes fechas de estructura
     * @return 
     */
    public static List getDatesStructures (){
        String statement = "SELECT to_char(STRDATE,'MM/YYYY')\n" +
                "FROM (\n" +
                "    select   to_date(to_char(FD_STRUCTURE_DATE, 'MM/YYYY'),'MM/YYYY')   STRDATE\n" +
                "    from     PUL_EMPLOYEE_CLIMA\n" +
                "    group by to_date(to_char(FD_STRUCTURE_DATE, 'MM/YYYY'),'MM/YYYY') \n" +
                "    order by to_date(to_char(FD_STRUCTURE_DATE, 'MM/YYYY'),'MM/YYYY')  DESC \n" +
                "    )";

        return JPA.em().createNativeQuery(statement).getResultList();
    }
    
    /**
     * Metodo para guardar el plan de clima en las encuestas 
     * @param email
     * @param plan
     * @param idEmployee
     * @param sessionUser
     * @param idSurvey
     * @param date
     * @return 
     */
    public static int setPlanEmail(String email, int plan, Long idEmployee, 
            String sessionUser, int idSurvey, String date) {
        int update = 0;
        
        String statement = " Update PUL_EMPLOYEE_CLIMA set FD_MOD_DATE = SYSDATE, FC_MOD_FOR= ?, fc_email = ? where FN_EMPLOYEE_NUMBER = ?";
        
        int updateEmail = JPA.em().createNativeQuery(statement)
                .setParameter(1, sessionUser)
                .setParameter(2, email)
                .setParameter(3, idEmployee)
                .executeUpdate();

        if (updateEmail > 0) {
            statement = "update PUL_SURVEY_EMPLOYEE set  FD_MOD_DATE = SYSDATE,\n" + 
                        "FC_MOD_FOR= ?, FN_PLAN = ? where FN_ID_EMPLOYEE_CLIMA = \n" +
                        "(SELECT FN_ID_EMPLOYEE_CLIMA FROM PUL_EMPLOYEE_CLIMA WHERE FN_EMPLOYEE_NUMBER = ? " +
                        "AND TO_CHAR(FD_STRUCTURE_DATE,'mm/yyyy') = ?)\n" +
                        "AND FN_ID_SURVEY = ?";
            
            update = JPA.em().createNativeQuery(statement)
                .setParameter(1, sessionUser)
                .setParameter(2, plan)
                .setParameter(3, idEmployee)
                .setParameter(4, date)
                .setParameter(5, idSurvey)
                .executeUpdate();
            
            if (update > 0) {
                changePlan(idEmployee, idSurvey, plan, sessionUser);
            }
        }
        
        return update;
    }

    /**
     * Metodo para recalcular las calificaciones despues del cambio del plan de 
     * acciones
     * @param structureDate
     * @return
     * @throws SQLException 
     */
    public static int recalcLevel(String structureDate) throws SQLException {
        OracleConnection connection = getOracleConnection();

        String sql="begin ?:= PORTALUNICO.FN_UPD_LEVEL_PLAN(?); end;";
        CallableStatement stmt = connection.prepareCall(sql);
        stmt.registerOutParameter(1, OracleTypes.INTEGER);
        stmt.setString(2,structureDate);
        stmt.execute();
        int recalcLevel = stmt.getInt(1);
        stmt.close();
        return recalcLevel;
   }
    
    /**
     * Metodo para modificar el plan de accion 
     * @param employeeNumber
     * @param surveyId
     * @param newPlan
     * @param userName
     * @return 
     */
    public static int changePlan(Long employeeNumber, int surveyId, int newPlan, String userName) {
        int result = 0;

        try {
            OracleConnection connection = getOracleConnection();
            CallableStatement statement = connection.prepareCall("{ ? = call PORTALUNICO.FN_CHANGE_PLAN(?, ?, ?, ?) }");

            statement.setLong(2, employeeNumber);
            statement.setInt(3, surveyId);
            statement.setInt(4, newPlan);
            statement.setString(5, userName);
            statement.registerOutParameter(1, OracleTypes.NUMBER);
            statement.execute();

            result = ((OracleCallableStatement) statement).getInt(1);
            statement.close();
        } catch (SQLException e) {
            Logger.error(e.getMessage());
        }

        return result;
    }

    /**
     * Metodo para saber si la ubicacion es tienda
     * @param userId
     * @param structureDate
     * @return
     * @throws SQLException 
     */
   public static int isStore(int userId, String structureDate) throws SQLException {
       OracleConnection connection = getOracleConnection();

       String sql="begin ?:= PORTALUNICO.fn_is_store(?, ?); end;";
       CallableStatement stmt = connection.prepareCall(sql);
       stmt.registerOutParameter(1, OracleTypes.INTEGER);
       stmt.setInt(2,userId);
       stmt.setString(3, structureDate);
       stmt.execute();
       int isStore = stmt.getInt(1);
       stmt.close();
       return isStore;
  }


   /**
    * Metodo para obtener el nombre del departamento por fecha de estructura
    * @param userId
    * @param date
    * @return 
    */
   public static String[] getDepartmentName(int userId, String date) {
        String statement = 
                            "SELECT decode(fc_desc_location, 'Servicios Liverpool', FC_TITLE, FC_TITLE || ' - ' || fc_desc_location) as title, FC_DEPARTMENT,\n" +
                           "COALESCE(SUBSTR(FC_EMAIL , 0, INSTR(FC_EMAIL, '@') - 1), FC_EMAIL) FC_USERNAME, FN_USERID,\n" +
                           "PORTALUNICO.FN_GET_TODO_SURVEY(:PN_DATE, FN_USERID) FN_SURVEYS,\n" +
                           "PORTALUNICO.FN_GET_SURVEY_RESOLVED(:PN_DATE, FN_USERID) FN_SURVEYS_ANSWERED\n" +
                           "FROM PUL_EMPLOYEE_CLIMA WHERE FN_USERID = :PN_USERID AND to_char(FD_STRUCTURE_DATE, 'mm/yyyy') = :PN_DATE";

        Query oQuery = JPA.em().createNativeQuery(statement);
        oQuery.setParameter("PN_USERID", userId)
            .setParameter("PN_DATE", date);
        
        try {
            Object result = oQuery.getSingleResult();
            Object[] deparment = (Object[]) result;

            return new String[]{
                deparment[0].toString(),
                deparment[1].toString(),
                deparment[2].toString(),
                deparment[3].toString(),
                deparment[4].toString(),
                deparment[5].toString()
            };
        } catch (Exception e) {
            Logger.error(e.getMessage());
        }
        
        return new String[]{"", "", "", "", "", ""};
    }
}
