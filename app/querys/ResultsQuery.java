package querys;

import java.sql.ResultSet;
import java.sql.SQLException;
import play.*;
import play.db.jpa.JPA;
import java.util.*;
import javax.persistence.Query;
import models.Survey;
import oracle.jdbc.OracleConnection;
import oracle.jdbc.OracleTypes;
import static utils.Queries.getOracleConnection;
import utils.PagingResult;

/**
 * Clase de utileria para ejecutar los querys del avance de Clima
 * @author Rodolfo Miranda -- Qualtop
 */
public class ResultsQuery {

    /**
     * MEtodo para la ejecucion del query el cual obtiene el primer nivel de la estructura
     * @param level
     * @return 
     */
    public static List getDepartmentsByLevel1(int level) {
        String statement = "select FC_TITLE,FN_USERID from PUL_EMPLOYEE_CLIMA "
                + " where FN_LEVEL = " + level + " order by FC_TITLE";

        Query oQuery = JPA.em().createNativeQuery(statement);

        return oQuery.getResultList();
    }

    /**
     * Metodo para obtener los niveles dinamicamente por filtros seleccionados
     * @param level
     * @param department
     * @return 
     */
    public static List getDepartmentsByLevel(int level, String department) {
        String statement = "select decode(fc_division,'CORP',FC_TITLE, FC_TITLE||' - '||FC_DIVISION) FC_TITLE,FN_USERID from PUL_EMPLOYEE_CLIMA "
                + " where FN_MANAGER in ( "
                + "   select FN_USERID from PUL_EMPLOYEE_CLIMA "
                + "   where FN_LEVEL = " + level
                + "   and FN_USERID = '" + department + "'"
                + " ) order by FC_TITLE";

        Query oQuery = JPA.em().createNativeQuery(statement);

        return oQuery.getResultList();
    }
    
    /**
     * Metodo para obtener los departamentos por fecha de estructura, y departamento
     * anterior
     * @param level
     * @param department
     * @param date
     * @return 
     */
    public static List getDepartmentsByLevel(int level, String department, String date) {
        String statement = "select decode(fc_division,'CORP',FC_TITLE, FC_TITLE||' - '||FC_DIVISION) FC_TITLE,FN_USERID from PUL_EMPLOYEE_CLIMA "
            + " where FN_MANAGER in ( "
            + "   select FN_USERID from PUL_EMPLOYEE_CLIMA "
            + "   where FN_LEVEL = " + level
            + "   and FN_USERID = '" + department + "'"
            + "   and TO_CHAR (fd_structure_date, 'MM/YYYY') = '" + date + "'"
            + " ) order by FC_TITLE";

        Query oQuery = JPA.em().createNativeQuery(statement);

        return oQuery.getResultList();
    }

    /**
     * Metodo para obtener las fechas de estructuras actuales en base de datos
     * @return 
     */
    public static List getDatesStructures() {
        String statement = "select distinct to_char(FD_SURVEY_DATE, 'mm/yyyy')  from PUL_SURVEY ";
        return JPA.em().createNativeQuery(statement).getResultList();
    }

}
