package querys;

import java.sql.CallableStatement;
import java.sql.SQLException;
import models.ScoreTask;
import oracle.jdbc.OracleConnection;
import play.Logger;
import utils.Queries;


public class ScoreEvaluation {
    public static void evaluate(String structureDate, String username, Long id) {
        ScoreTask task = task = ScoreTask.findById(id);
        try {
            OracleConnection connection = Queries.getOracleConnection();
            CallableStatement statement = connection.prepareCall("BEGIN PORTALUNICO.PR_SURVEY_EVALUATION(?, ?); END;");
            
            statement.setString(1, structureDate);
            statement.setString(2, username);
            statement.execute();
            Logger.info("Calculating scores complete");
            
            task.status = "S";
            task.save();
            statement.close();
        } catch (SQLException e) {
            Logger.error("Survey evaluation error: " + e.getMessage());
            task.status = "E";
            task.save();
        }        
    }
}
