package querys;

import java.sql.CallableStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import play.*;
import play.db.jpa.JPA;
import java.util.*;
import javax.persistence.Query;
import json.entities.FortalezasList;
import json.entities.ResumeTable;
import models.Survey;
import oracle.jdbc.OracleCallableStatement;
import oracle.jdbc.OracleConnection;
import oracle.jdbc.OracleTypes;
import org.apache.commons.lang3.StringUtils;
import play.mvc.Scope;
import static utils.Queries.getOracleConnection;

/**
 * Clase de utilidad para el manejo de las encuestas.
 * @author Rodolfo Miranda -- Qualtop
 */
public class SurveysQuery {
    
    /**
     * Metodo para obtener todas las encuestas
     * @return 
     */
    public static List getSurveys() {
        String statement = "SELECT * FROM PUL_SURVEY";

        Query oQuery = JPA.em().createNativeQuery(statement);

        return oQuery.getResultList();
    }

    /**
     * Metodo para obtener las preguntas por encuesta
     * @param surveyId
     * @param typeQuestionId
     * @return 
     */
    public static List getQuestionsByType(int surveyId, int typeQuestionId){
        String statement = "SELECT FN_ID_QUESTION, FC_QUESTION "
                + "FROM PUL_QUESTION "
                + "WHERE  FN_ID_SURVEY = ? "
                + "AND FN_ID_TYPE_QUESTION = ?";

        Query oQuery = JPA.em().createNativeQuery(statement)
                        .setParameter(1, surveyId)
                        .setParameter(2, typeQuestionId);

        return oQuery.getResultList();
    }

    /**
     * Metodo para obtener las respuestas de una pregunta abierta
     * @param questionId
     * @return 
     */
    public static List getOpenAnswersByQuestion(int questionId) {
        String statement = "SELECT rownum, FC_TEXT_ANSWER " +
                            "FROM   PUL_QUESTION_ANSWER qa " +
                            "WHERE  qa.FN_ID_QUESTION = ? " +
                            " and trim(qa.fc_text_answer) is not null";
                            
        Query oQuery = JPA.em().createNativeQuery(statement)
                        .setParameter(1, questionId);

        return oQuery.getResultList();
    }

    /**
     * Metodo para obtener el numero de respuesta por pregunta abierta
     * @param questionId
     * @return 
     */
    public static List getOpenAnswersCount(int questionId) {
        String statement =
        " SELECT count(1) FROM" +
           " (" +
              " SELECT" +
                 " rownum uno," +
                 " tabla1.* " +
              " FROM" +
                 " (" +
                    " SELECT" +
                       " FC_TEXT_ANSWER " +
                    " FROM" +
                       " PUL_QUESTION_ANSWER qa " +
                    " WHERE" +
                       " qa.FN_ID_QUESTION = ?" +
                       " AND trim(qa.fc_text_answer) IS NOT NULL " +
                 " )" +
                 " tabla1 " +
           " )" +
           " tabla12";
                            
        Query oQuery = JPA.em().createNativeQuery(statement)
                        .setParameter(1, questionId);

        return oQuery.getResultList();
    }

    /**
     * Metodo para obtener respuestas a una pregunta abierta por rango
     * @param questionId
     * @param rowStart
     * @param rowEnd
     * @return 
     */
    public static List getOpenAnswersByQuestionByRange(int questionId, int rowStart, int rowEnd) {
        String statement =
        " SELECT * FROM" +
           " (" +
              " SELECT" +
                 " rownum uno," +
                 " tabla1.* " +
              " FROM" +
                 " (" +
                    " SELECT" +
                       " FC_TEXT_ANSWER " +
                    " FROM" +
                       " PUL_QUESTION_ANSWER qa " +
                    " WHERE" +
                       " qa.FN_ID_QUESTION = ?" +
                       " AND trim(qa.fc_text_answer) IS NOT NULL " +
                 " )" +
                 " tabla1 " +
           " )" +
           " tabla12 " +
        " WHERE" +
           " tabla12.uno >= " + rowStart +
           " AND tabla12.uno <= " + rowEnd;
                            
        Query oQuery = JPA.em().createNativeQuery(statement)
                        .setParameter(1, questionId);

        return oQuery.getResultList();
    }

    /**
     * Metodo para obtner total de respuestas por pregunta
     * @param questionId
     * @return 
     */
    public static List getMultiAnswersByQuestion(int questionId) {
        String statement = "select cha.FN_ID_CHOICE_ANSWER, CHA.FC_CHOICE_TEXT, " +
                        "        (SELECT  count(qa.FC_TEXT_ANSWER) totRespuesta  " +
                        "        FROM   PUL_QUESTION_ANSWER qa, PUL_CHOICE_ANSWER ca " +
                        "        WHERE  qa.FN_ID_QUESTION = " + questionId +
                        "        AND    ca.FN_ID_CHOICE_ANSWER = qa.FN_ID_CHOICE_ANSWER " +
                        "        AND    qa.FN_ID_CHOICE_ANSWER = cha.FN_ID_CHOICE_ANSWER) Contestadas, " +
                        "        nvl((SELECT " +
                        "               (select sum(count(qa.FC_TEXT_ANSWER)) " +
                        "                from pul_question_answer " +
                        "                where FN_ID_QUESTION = " + questionId +
                        "                group by FC_TEXT_ANSWER) SUMA_TOTAL " +
                        "        FROM   PUL_QUESTION_ANSWER qa, PUL_CHOICE_ANSWER ca " +
                        "        WHERE  qa.FN_ID_QUESTION = "+ questionId +
                        "        AND    ca.FN_ID_CHOICE_ANSWER = qa.FN_ID_CHOICE_ANSWER " +
                        "        AND    qa.FN_ID_CHOICE_ANSWER = cha.FN_ID_CHOICE_ANSWER " +
                        "        GROUP BY FC_TEXT_ANSWER), 0) SUMA_TOTAL, " +
                        "        nvl((SELECT round(((count(qa.FC_TEXT_ANSWER) / (select sum(count(qa.FC_TEXT_ANSWER)) " +
                        "                                            from pul_question_answer " +
                        "                                            where FN_ID_QUESTION = "+ questionId +
                        "                                            group by FC_TEXT_ANSWER)) *100),4) " +
                        "        FROM   PUL_QUESTION_ANSWER qa, PUL_CHOICE_ANSWER ca " +
                        "        WHERE  qa.FN_ID_QUESTION = "+ questionId +
                        "        AND    ca.FN_ID_CHOICE_ANSWER = qa.FN_ID_CHOICE_ANSWER " +
                        "        AND    qa.FN_ID_CHOICE_ANSWER = cha.FN_ID_CHOICE_ANSWER " +
                        "        GROUP BY FC_TEXT_ANSWER),0) PORCENTAJE_CONTESTADO " +
                        " FROM PUL_CHOICE_ANSWER cha " +
                        " WHERE cha.FN_ID_QUESTION =" + questionId +
                        " ORDER by cha.FN_ID_CHOICE_ANSWER";


        Query oQuery = JPA.em().createNativeQuery(statement);

        return oQuery.getResultList();
    }

    /**
     * Metodo para insertar calificacion por pregunta y nombre de usuario
     * @param surveyId
     * @param employeeId
     * @param username
     * @return 
     */
    public static int insertSingleScore(int surveyId, int employeeId, String username) {
        int result = 0;

        try {
            OracleConnection connection = getOracleConnection();
            CallableStatement statement = connection.prepareCall("{ ? = call PORTALUNICO.FN_INS_SINGLE_SCORE(?, ?, ?) }");

            statement.setLong(2, surveyId);
            statement.setLong(3, employeeId);
            statement.setString(4, username);
            statement.registerOutParameter(1, OracleTypes.NUMBER);
            statement.execute();

            result = ((OracleCallableStatement) statement).getInt(1);
            statement.close();
        } catch (SQLException e) {
            Logger.error(e.getMessage());
        }

        return result;
    }

    /**
     * MEtodo para obtener resumen de calificaciones por encuesta
     * @param surveyId
     * @return 
     */
    public static ResumeTable getResume(int surveyId) {
        ResumeTable resume = new ResumeTable();
        try {
            String query = "SELECT encuestas, contestadas, porcentaje, " +
                           " (select ROUND(AVG(calificacion), 2) " +
                           " from (SELECT SUM (fn_tree_score) calificacion, FN_ID_EMPLOYEE_CLIMA " +
                           " FROM PUL_SUBFACTOR_SCORE " +
                           " WHERE FN_ID_SURVEY = tbl1.FN_ID_SURVEY " +
                           " AND FN_TREE_SCORE IS NOT NULL " +
                           " group by FN_ID_EMPLOYEE_CLIMA) " +
                           "  ) calificacion " +
                           " FROM (SELECT COUNT(1) encuestas, SUM (FN_ANSWERED) contestadas,ROUND ( (SUM (FN_ANSWERED) / COUNT(1)) * 100, 2) porcentaje,FN_ID_SURVEY " +
                           " FROM PUL_SURVEY_EMPLOYEE " +
                           " WHERE FN_ID_SURVEY = " + surveyId +
                           " GROUP BY FN_ID_SURVEY) tbl1";


            OracleConnection connection = getOracleConnection();

            CallableStatement statement = connection.prepareCall(query);
            statement.execute();

            ResultSet results = ((OracleCallableStatement) statement).getResultSet();
            if (results.next()) {
                resume.setSurveys(results.getInt("encuestas"));
                resume.setAnswers(results.getInt("contestadas"));
                resume.setPercentage(results.getFloat("porcentaje"));
                resume.setScore(results.getFloat("calificacion"));
            }
            results.close();
            statement.close();
        } catch (SQLException e) {
            Logger.error(e.getMessage());
        }
        return resume;
    }

    /**
     * Metodo para exportar a formato Excel los resultados mostrados en pantalla
     * @param surveyId
     * @return
     * @throws SQLException 
     */
    public List getExport(int surveyId) throws SQLException{
        String query =  " SELECT FC_QUESTION, FC_DESC_FACTOR, FC_DESC_SUBFACTOR, score " +
                        " FROM ( " +
                        "   SELECT q.FC_QUESTION, f.FC_DESC_FACTOR, sf.FC_DESC_SUBFACTOR, round((DECODE (qa.FN_WEIGH,0,0,(sum(qa.FN_SINGLE_SCORE)/count(1))/qa.FN_WEIGH))*100, 2) score " +
                        "   FROM PUL_QUESTION_ANSWER qa " +
                        "   INNER JOIN PUL_QUESTION q ON q.FN_ID_QUESTION = qa.FN_ID_QUESTION " +
                        "   INNER JOIN PUL_EMPLOYEE_CLIMA ec ON ec.FN_ID_EMPLOYEE_CLIMA = qa.FN_ID_EMPLOYEE_CLIMA " +
                        "   INNER JOIN PUL_SUBFACTOR sf ON sf.FN_ID_SUBFACTOR = q.FN_ID_SUBFACTOR " +
                        "   INNER JOIN PUL_FACTOR f ON f.FN_ID_FACTOR = sf.FN_ID_FACTOR " +
                        "   WHERE q.fn_id_survey = " + surveyId +
                        "   AND qa.FN_WEIGH IS NOT NULL " +
                        "   AND qa.FN_SINGLE_SCORE IS NOT NULL " +
                        "   GROUP BY q.FC_QUESTION, qa.FN_WEIGH, sf.FC_DESC_SUBFACTOR, f.FC_DESC_FACTOR " +
                        "   ORDER BY score asc) " +
                        " WHERE rownum BETWEEN 1 AND 10";

        OracleConnection connection = getOracleConnection();

        CallableStatement statement = connection.prepareCall(query);
        statement.execute();

        ResultSet results = ((OracleCallableStatement) statement).getResultSet();

        List<FortalezasList> dataList = new ArrayList();

        while (results .next()) {
            FortalezasList fortaleza =  new FortalezasList();

            fortaleza.setQuestion(results.getString("FC_QUESTION"));
            fortaleza.setFactor(results.getString("FC_DESC_FACTOR"));
            fortaleza.setSubfactor(results.getString("FC_DESC_SUBFACTOR"));
            fortaleza.setScore(results.getFloat("score"));

            dataList.add(fortaleza);
        }
        results.close();
        statement.close();
        return dataList;
    }
    
    /**
     * Metodo para obtener las preguntas abiertas por fecha de estructura, 
     * userId
     * @param userId
     * @param structureDate
     * @param firstRequest
     * @return 
     */
    public static List getOpenQuestions(String userId, String structureDate, boolean firstRequest) {
        String where = "(ec.FN_USERID IN (SELECT  ec01.FN_USERID\n" +
                       "                    FROM PUL_EMPLOYEE_CLIMA ec01\n" +
                       "                    JOIN PUL_SURVEY_EMPLOYEE se01 on se01.FN_ID_EMPLOYEE_CLIMA = ec01.FN_ID_EMPLOYEE_CLIMA \n" +
                       "                    JOIN PUL_SURVEY s01  ON s01.FN_ID_SURVEY = se01.FN_ID_SURVEY\n" +
                       "                    WHERE FN_MANAGER = (:PN_USERID)\n" +
                       "                    AND s01.FN_ID_SURVEY_TYPE <> 1 \n" +
                       "                     ) \n" +
                       "OR ec.FN_USERID IN (SELECT  ec01.FN_USERID\n" +
                       "                    FROM PUL_EMPLOYEE_CLIMA ec01\n" +
                       "                    JOIN PUL_SURVEY_EMPLOYEE se01 on se01.FN_ID_EMPLOYEE_CLIMA = ec01.FN_ID_EMPLOYEE_CLIMA \n" +
                       "                    JOIN PUL_SURVEY s01  ON s01.FN_ID_SURVEY = se01.FN_ID_SURVEY\n" +
                       "                    WHERE FN_USERID = (:PN_USERID)\n" +
                       "                    AND s01.FN_ID_SURVEY_TYPE = 1 \n" +
                       "                    )\n" +
                       ")\n";
        
        String isAdmin = Scope.Session.current().get("isAdmin");
        
        if (firstRequest && (isAdmin != null && isAdmin.equals("true"))) {
            where  = "1 = :PN_USERID\n";
            userId = "1";
        }
        
        String query = "SELECT q.FC_QUESTION, q.FN_ID_SURVEY\n" +
                        "FROM PUL_QUESTION         q\n" +
                        "JOIN PUL_QUESTION_ANSWER qa ON q.FN_ID_QUESTION = qa.FN_ID_QUESTION \n" +
                        "JOIN PUL_EMPLOYEE_CLIMA  ec ON ec.FN_ID_EMPLOYEE_CLIMA = qa.FN_ID_EMPLOYEE_CLIMA\n" +
                        "WHERE " + where +
                        "AND TO_CHAR(ec.FD_STRUCTURE_DATE, 'MM/YYYY') = :PN_DATE\n" +
                        "AND qa.FN_SURVEY_ID in (select s.FN_ID_SURVEY from pul_survey s where TO_CHAR(s.FD_SURVEY_DATE, 'MM/YYYY')= :PN_DATE)\n" +
                        "AND q.FN_ID_TYPE_QUESTION = 3\n" +
                        "GROUP BY q.FC_QUESTION, q.FN_ID_SURVEY";

        Query oQuery = JPA.em().createNativeQuery(query)
            .setParameter("PN_USERID", Integer.valueOf(userId))
            .setParameter("PN_DATE", structureDate);
        
        
        return oQuery.getResultList();        
    }
    
    /**
     * Metodo para obtener las preguntas respuestas por encuestas y fecha de
     * estructura
     * @param surveys
     * @param question
     * @param userId
     * @param structureDate
     * @param start
     * @param end
     * @param direct
     * @return 
     */
    public static List getQuestionAnswers(String surveys, String question, String userId,
                                          String structureDate, int start, int end, boolean direct) {        
        String query = "SELECT * FROM (\n" +
                        "    SELECT\n" +
                        "        answer.FC_TEXT_ANSWER, answer.FN_ID_QUESTION_ANSWER,\n" +
                        "        ROW_NUMBER() OVER (ORDER BY answer.FN_ID_QUESTION_ANSWER) row_number\n" +
                        "    FROM\n" +
                        "        pul_question_answer answer\n" +
                        "    INNER JOIN\n" +
                        "        pul_question question ON question.FN_ID_QUESTION = answer.FN_ID_QUESTION\n" +
                        "    LEFT JOIN PUL_EMPLOYEE_CLIMA employee ON employee.FN_ID_EMPLOYEE_CLIMA = answer.FN_ID_EMPLOYEE_CLIMA\n" +
                        "    WHERE\n" +
                        "        question.FN_ID_TYPE_QUESTION = 3\n" +
                        "        AND TRIM(answer.FC_TEXT_ANSWER) IS NOT NULL\n" +
                        "        AND TRIM(question.FC_QUESTION) = TRIM(:PN_QUESTION)\n" +
                        "   and    answer.fn_id_employee_clima in (SELECT emp.FN_ID_EMPLOYEE_CLIMA\n" +
                        "   FROM (SELECT FN_ID_EMPLOYEE_CLIMA,FN_USERID, FN_MANAGER FROM portalunico.PUL_EMPLOYEE_CLIMA WHERE TO_CHAR(fd_structure_date, 'MM/YYYY') = :PN_DATE) emp\n" +
                        "   START WITH emp.FN_USERID IN (:PN_USERID)\n" +
                        "   CONNECT BY PRIOR emp.FN_USERID = emp.FN_MANAGER)\n" +
                        ") questionAnswers\n" +
                        "WHERE row_number BETWEEN :PN_START AND :PN_END";
        
        if (direct) {
            query = "SELECT * \n" +
                    "FROM (  SELECT  ANSWER.FC_TEXT_ANSWER, \n" +
                    "                ANSWER.FN_ID_QUESTION_ANSWER,\n" +
                    "                ROW_NUMBER() OVER (ORDER BY answer.FN_ID_QUESTION_ANSWER) row_number\n" +
                    "        FROM PUL_QUESTION_ANSWER ANSWER\n" +
                    "        JOIN PUL_QUESTION QUESTION ON QUESTION.FN_ID_QUESTION = ANSWER.FN_ID_QUESTION\n" +
                    "        LEFT JOIN PUL_EMPLOYEE_CLIMA EMPLOYEE ON EMPLOYEE.FN_ID_EMPLOYEE_CLIMA = ANSWER.FN_ID_EMPLOYEE_CLIMA\n" +
                    "        WHERE QUESTION.FN_ID_TYPE_QUESTION = 3\n" +
                    "        AND TRIM(ANSWER.FC_TEXT_ANSWER) IS NOT NULL\n" +
                    "        AND TRIM(QUESTION.FC_QUESTION) = TRIM(:PN_QUESTION)\n" +
                    "        AND (EMPLOYEE.FN_USERID IN (SELECT  ec01.FN_USERID\n" +
                    "                                    FROM PUL_EMPLOYEE_CLIMA ec01\n" +
                    "                                    JOIN PUL_SURVEY_EMPLOYEE se01 on se01.FN_ID_EMPLOYEE_CLIMA = ec01.FN_ID_EMPLOYEE_CLIMA \n" +
                    "                                    JOIN PUL_SURVEY s01  ON s01.FN_ID_SURVEY = se01.FN_ID_SURVEY\n" +
                    "                                    WHERE FN_MANAGER = (:PN_USERID)\n" +
                    "                                    AND s01.FN_ID_SURVEY in (select s.FN_ID_SURVEY from pul_survey s where TO_CHAR(s.FD_SURVEY_DATE, 'MM/YYYY')= :PN_DATE)\n" +
                    "                                    AND s01.FN_ID_SURVEY_TYPE <> 1 \n" +
                    "                                    ) \n" +
                    "          OR EMPLOYEE.FN_USERID IN (SELECT  ec01.FN_USERID\n" +
                    "                                    FROM PUL_EMPLOYEE_CLIMA ec01\n" +
                    "                                    JOIN PUL_SURVEY_EMPLOYEE se01 on se01.FN_ID_EMPLOYEE_CLIMA = ec01.FN_ID_EMPLOYEE_CLIMA \n" +
                    "                                    JOIN PUL_SURVEY s01  ON s01.FN_ID_SURVEY = se01.FN_ID_SURVEY\n" +
                    "                                    WHERE FN_USERID = (:PN_USERID)\n" +
                    "                                    AND s01.FN_ID_SURVEY in (select s.FN_ID_SURVEY from pul_survey s where TO_CHAR(s.FD_SURVEY_DATE, 'MM/YYYY')= :PN_DATE)\n" +
                    "                                    AND s01.FN_ID_SURVEY_TYPE = 1 \n" +
                    "                                    ) \n" +
                    "            )            \n" +
                    ") questionAnswers\n" +
                    "WHERE row_number BETWEEN :PN_START AND :PN_END";
        }
        
        Query oQuery = JPA.em().createNativeQuery(query)
            .setParameter("PN_USERID", userId)
            .setParameter("PN_QUESTION", question)
            .setParameter("PN_START", start)
            .setParameter("PN_END", end)
            .setParameter("PN_DATE", structureDate);
        
        return oQuery.getResultList();
    }
    
    /**
     * Metodo de apoyo para la pagincacion
     * @param surveys
     * @param question
     * @param userId
     * @param structureDate
     * @param direct
     * @return 
     */
    public static long getCountQuestionAnswers(String surveys, String question, 
                                               String userId, String structureDate,
                                               boolean direct) {        
        String query = "SELECT COUNT(1)\n" +
                        "    FROM\n" +
                        "        pul_question_answer answer\n" +
                        "    INNER JOIN\n" +
                        "        pul_question question ON question.FN_ID_QUESTION = answer.FN_ID_QUESTION\n" +
                        "    LEFT JOIN PUL_EMPLOYEE_CLIMA employee ON employee.FN_ID_EMPLOYEE_CLIMA = answer.FN_ID_EMPLOYEE_CLIMA\n" +
                        "    WHERE\n" +
                        "        question.FN_ID_TYPE_QUESTION = 3\n" +
                        "        AND TRIM(answer.FC_TEXT_ANSWER) IS NOT NULL\n" +
                        "        AND TRIM(question.FC_QUESTION) = TRIM(:PN_QUESTION)\n" +
                        "        and answer.fn_id_employee_clima in (SELECT emp.FN_ID_EMPLOYEE_CLIMA\n" +
                        "   FROM (SELECT FN_ID_EMPLOYEE_CLIMA,FN_USERID, FN_MANAGER FROM portalunico.PUL_EMPLOYEE_CLIMA WHERE TO_CHAR(fd_structure_date, 'MM/YYYY') = :PN_DATE) emp\n" +
                        "   START WITH emp.FN_USERID IN (:PN_USERID)\n" +
                        "   CONNECT BY PRIOR emp.FN_USERID = emp.FN_MANAGER)";
        
        if (direct) {
            query = "SELECT COUNT(1)\n" +
                    "FROM PUL_QUESTION_ANSWER ANSWER\n" +
                    "JOIN PUL_QUESTION QUESTION ON QUESTION.FN_ID_QUESTION = ANSWER.FN_ID_QUESTION\n" +
                    "LEFT JOIN PUL_EMPLOYEE_CLIMA EMPLOYEE ON EMPLOYEE.FN_ID_EMPLOYEE_CLIMA = ANSWER.FN_ID_EMPLOYEE_CLIMA\n" +
                    "WHERE QUESTION.FN_ID_TYPE_QUESTION = 3\n" +
                    "AND TRIM(ANSWER.FC_TEXT_ANSWER) IS NOT NULL\n" +
                    "AND TRIM(QUESTION.FC_QUESTION) = TRIM(:PN_QUESTION)\n" +
                    "AND (EMPLOYEE.FN_USERID IN (SELECT  ec01.FN_USERID\n" +
                    "	FROM PUL_EMPLOYEE_CLIMA ec01\n" +
                    "	JOIN PUL_SURVEY_EMPLOYEE se01 on se01.FN_ID_EMPLOYEE_CLIMA = ec01.FN_ID_EMPLOYEE_CLIMA \n" +
                    "	JOIN PUL_SURVEY s01  ON s01.FN_ID_SURVEY = se01.FN_ID_SURVEY\n" +
                    "	WHERE FN_MANAGER = (:PN_USERID)\n" +
                    "	AND s01.FN_ID_SURVEY_TYPE <> 1 \n" +
                    ") \n" +
                    "OR EMPLOYEE.FN_USERID IN (SELECT  ec01.FN_USERID\n" +
                    "	FROM PUL_EMPLOYEE_CLIMA ec01\n" +
                    "	JOIN PUL_SURVEY_EMPLOYEE se01 on se01.FN_ID_EMPLOYEE_CLIMA = ec01.FN_ID_EMPLOYEE_CLIMA \n" +
                    "	JOIN PUL_SURVEY s01  ON s01.FN_ID_SURVEY = se01.FN_ID_SURVEY\n" +
                    "	WHERE FN_USERID = (:PN_USERID)\n" +
                    "	AND s01.FN_ID_SURVEY_TYPE = 1 \n" +
                    "))";
        }

        Query oQuery = JPA.em().createNativeQuery(query)
            .setParameter("PN_USERID", userId)
            .setParameter("PN_QUESTION", question);
        
        if (!direct) {
            oQuery.setParameter("PN_DATE", structureDate);
        }
        
        List result = oQuery.getResultList();
        return Long.parseLong(result.get(0).toString());
    }
    
    /**
     * MEtodo para obtener los ids de encuestas
     * @return 
     */
    public static Long getIdBySurvey() {
        Long result = 0L;

        try {
            OracleConnection connection = getOracleConnection();
            CallableStatement statement = connection.prepareCall("{ ? = call PORTALUNICO.FN_GET_ID_BY_SURVEY() }");
            
            statement.registerOutParameter(1, OracleTypes.NUMBER);
            statement.execute();

            result = ((OracleCallableStatement) statement).getLong(1);
            statement.close();
        } catch (SQLException e) {
            Logger.error(e.getMessage());
        }

        return result;
    }
}
