package querys;

import java.util.*;
import javax.persistence.Query;
import play.db.jpa.JPA;

/**
 * Clase de utileria para ejecutar los querys del avance
 * @author Rodolfo Miranda -- Qualtop
 */
public class ProgressQuery {
    
    /**
     * MEtodo para ejecutar query de avance por fecha y usuario
     * @param userId
     * @param date
     * @return 
     */
    public static List getProgress(int userId, String date) {
        String query = "SELECT  TB1.TITLE_MANAGER, \n" +
                        "        TB1.FC_TITLE, \n" +
                        "        TB1.FC_NAME,\n" +
                        "        TB1.ALI,\n" +
                        "        TB1.ANSWERED,\n" +
                        "        DECODE(TB1.ANSWERED, 0, 0, ROUND((TB1.ANSWERED / TB1.ALI) * 100, 2)) PORCENT,\n" +
                        "        TB1.FN_LEVEL,\n" +
                        "        TB1.FN_ID_EMPLOYEE_CLIMA,\n" +
                        "        TB1.FN_USERID\n" +
                        "FROM (\n" +
                        "    SELECT (SELECT FC_TITLE\n" +
                        "            FROM   PUL_EMPLOYEE_CLIMA PEM\n" +
                        "            WHERE  TO_CHAR (fd_structure_date, 'MM/YYYY') = '"+date+"'\n" +
                        "            AND PEM.FN_USERID = TABLEQ.FN_MANAGER) TITLE_MANAGER,\n" +
                        "            TABLEQ.FC_TITLE,\n" +
                        "            (select FC_NAME from pul_survey s where fn_id_survey= se.FN_ID_SURVEY) FC_NAME,\n" +
                        "            FN_GET_TODO_SURVEY ('"+date+"', TABLEQ.FN_USERID) ALI,\n" +
                        "            FN_GET_SURVEY_RESOLVED ('"+date+"', TABLEQ.FN_USERID) ANSWERED,                \n" +
                        "            TABLEQ.FN_LEVEL,\n" +
                        "            TABLEQ.FN_ID_EMPLOYEE_CLIMA,\n" +
                        "            TABLEQ.FN_USERID\n" +
                        "    FROM (SELECT  FN_ID_EMPLOYEE_CLIMA,\n" +
                        "                  FN_LEVEL,\n" +
                        "                  FC_TITLE ||' - '||FC_DESC_LOCATION FC_TITLE,\n" +
                        "                  FN_USERID,\n" +
                        "                  FC_LAST_NAME,\n" +
                        "                  FN_MANAGER,\n" +
                        "                  FD_STRUCTURE_date\n" +
                        "          FROM (SELECT * FROM PORTALUNICO.PUL_EMPLOYEE_CLIMA WHERE TO_CHAR (FD_STRUCTURE_date, 'MM/YYYY') = '"+date+"') \n" +
                        "          CONNECT BY PRIOR FN_USERID = FN_MANAGER\n" +
                        "          START WITH FN_MANAGER = '"+userId+"') TABLEQ \n" +
                        "    JOIN  PUL_SURVEY_EMPLOYEE SE ON SE.FN_ID_EMPLOYEE_CLIMA = TABLEQ.FN_ID_EMPLOYEE_CLIMA\n" +
                        "    WHERE TO_CHAR(FD_STRUCTURE_date,'MM/YYYY') = '"+date+"'\n" +
                        "    AND   se.FN_ID_SURVEY = (SELECT MAX(FN_ID_SURVEY) FROM PUL_SURVEY_EMPLOYEE WHERE FN_ID_EMPLOYEE_CLIMA = TABLEQ.FN_ID_EMPLOYEE_CLIMA)\n" +
                        "    AND  (SE.FN_ID_SURVEY in (select s.FN_ID_SURVEY from pul_survey s where TO_CHAR(s.FD_SURVEY_date, 'MM/YYYY')= '"+date+"') OR  SE.FN_ID_EMPLOYEE_CLIMA IS NULL)\n" +
                        "    AND   TABLEQ.FN_MANAGER = '"+userId+"'\n" +
                        "    AND  TO_CHAR (FD_STRUCTURE_date, 'MM/YYYY') = '"+date+"') TB1";
        
        
        Query oQuery = JPA.em().createNativeQuery(query);
        return oQuery.getResultList();
    }
    
    /**
     * Metodo para obtener el avance principañ por fecha de estructura
     * @param date
     * @return
     * @throws Exception 
     */
    public static List getInitialProgress(String date) throws Exception{
        String query = "SELECT  T1.TITLE_MANAGER,\n" +
                        "        T1.FC_TITLE,\n" +
                        "        T1.FC_NAME,\n" +
                        "        T1.ALI,\n" +
                        "        T1.ANSWERED,\n" +
                        "        DECODE(T1.ANSWERED, 0, 0,ROUND((T1.ANSWERED / T1.ALI) * 100, 2)) PORCENT,        \n" +
                        "        FN_LEVEL,\n" +
                        "        FN_ID_EMPLOYEE_CLIMA,\n" +
                        "        FN_USERID     \n" +
                        "FROM ( \n" +
                        "    SELECT (SELECT FC_TITLE\n" +
                        "            FROM   PUL_EMPLOYEE_CLIMA PEM\n" +
                        "            WHERE  TO_CHAR (fd_structure_DATE, 'MM/YYYY') = '" + date + "'\n" +
                        "            AND    PEM.FN_USERID = TABLEQ.FN_MANAGER) TITLE_MANAGER,\n" +
                        "            TABLEQ.FC_TITLE,\n" +
                        "            (select FC_NAME from pul_survey s where fn_id_survey= se.FN_ID_SURVEY) FC_NAME,\n" +
                        "            FN_GET_TODO_SURVEY ('" + date + "', TABLEQ.FN_USERID) ALI,\n" +
                        "            FN_GET_SURVEY_RESOLVED ('" + date + "', TABLEQ.FN_USERID) ANSWERED,         \n" +
                        "            TABLEQ.FN_LEVEL,\n" +
                        "            TABLEQ.FN_ID_EMPLOYEE_CLIMA,\n" +
                        "            TABLEQ.FN_USERID\n" +
                        "    FROM (SELECT FN_ID_EMPLOYEE_CLIMA,\n" +
                        "                 FN_LEVEL,\n" +
                        "                 FC_TITLE,\n" +
                        "                 FN_USERID,\n" +
                        "                 FC_LAST_NAME,\n" +
                        "                 FN_MANAGER,\n" +
                        "                 FD_STRUCTURE_DATE\n" +
                        "          FROM PORTALUNICO.PUL_EMPLOYEE_CLIMA \n" +
                        "          WHERE TO_CHAR (FD_STRUCTURE_DATE, 'MM/YYYY') = '" + date + "' AND FN_LEVEL =1) TABLEQ\n" +
                        "    LEFT JOIN  PUL_SURVEY_EMPLOYEE SE ON SE.FN_ID_EMPLOYEE_CLIMA = TABLEQ.FN_ID_EMPLOYEE_CLIMA\n" +
                        "    WHERE TO_CHAR(FD_STRUCTURE_DATE,'MM/YYYY') ='" + date + "') T1";
        
        Query oQuery = JPA.em().createNativeQuery(query);
        return oQuery.getResultList();
    }
}
