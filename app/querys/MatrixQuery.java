package querys;

import java.sql.CallableStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import play.*;
import play.db.jpa.JPA;
import java.util.*;
import javax.persistence.Query;
import json.entities.ResultMatrixList;
import json.entities.ResumeTable;
import models.Survey;
import oracle.jdbc.OracleCallableStatement;
import oracle.jdbc.OracleConnection;
import oracle.jdbc.OracleTypes;
import org.apache.commons.lang3.StringUtils;
import static utils.Queries.getOracleConnection;
import utils.PagingResult;
import static utils.Queries.getOracleConnection;

public class MatrixQuery {

    public static List<ResultMatrixList> getSubFactor(String[] departments, String structureDate) throws SQLException {
        String where = departments != null ? "ec.FN_USERID IN (" + StringUtils.join(departments, ",") + ")" : "1 = 0";
        
        String query = "SELECT  FC_DESC_FACTOR, FC_DESC_SUBFACTOR, FC_TITLE, ROUND(avg(tbl.score), 2) FN_SINGLE_SCORE\n" +
                        "FROM \n" +
                        "(SELECT f.FC_DESC_FACTOR, sf.FC_DESC_SUBFACTOR, decode(ec.fc_desc_location,'Servicios Liverpool',ec.FC_TITLE, ec.FC_TITLE||' - '||ec.fc_desc_location) ||'-'||ec.FN_USERID FC_TITLE, \n" +
                        "        ec.FN_USERID, avg(ss.FN_TREE_SCORE) score\n" +
                        " FROM      PUL_EMPLOYEE_CLIMA  ec       \n" +
                        " LEFT JOIN PUL_SUBFACTOR_SCORE ss ON ec.FN_ID_EMPLOYEE_CLIMA = ss.FN_ID_EMPLOYEE_CLIMA\n" +
                        " LEFT JOIN PUL_SUBFACTOR sf ON sf.FC_DESC_SUBFACTOR = ss.FC_DESC_SUBFACTOR\n" +
                        " LEFT JOIN PUL_FACTOR     f ON f.FN_ID_FACTOR = sf.FN_ID_FACTOR       \n" +
                        " WHERE " + where + " AND   ss.FN_TREE_SCORE IS NOT NULL \n" +
                        " AND   TO_CHAR(ec.FD_STRUCTURE_DATE, 'MM/YYYY') = '" + structureDate + "'\n" +
                        " AND   f.FN_ID_SURVEY in (select s.FN_ID_SURVEY from pul_survey s where TO_CHAR(s.FD_SURVEY_DATE, 'MM/YYYY')= '" + structureDate + "') \n" +
                        " GROUP BY f.FC_DESC_FACTOR, sf.FC_DESC_SUBFACTOR, ec.fc_desc_location,ec.FC_TITLE, \n" +
                        "          ec.FN_USERID  \n" +
                        ") tbl\n" +
                        "WHERE (SELECT COUNT(1) FROM PUL_EMPLOYEE_CLIMA SEC WHERE SEC.FN_MANAGER = tbl.FN_USERID) > 0 \n" +
                        "   OR  PORTALUNICO.FN_GET_SURVEY_RESOLVED('" + structureDate + "', tbl.FN_USERID) > 2   \n" +
                        "GROUP BY FC_DESC_FACTOR, FC_DESC_SUBFACTOR, FC_TITLE";

        OracleConnection connection = getOracleConnection();

        CallableStatement statement = connection.prepareCall(query);
        statement.execute();

        ResultSet results = ((OracleCallableStatement) statement).getResultSet();

        List<ResultMatrixList> dataList = new ArrayList();

        while (results.next()) {
            ResultMatrixList matrix = new ResultMatrixList();

            matrix.setFactor(results.getString("FC_DESC_FACTOR"));
            matrix.setSubFactor(results.getString("FC_DESC_SUBFACTOR"));
            matrix.setScore(results.getFloat("FN_SINGLE_SCORE"));
            matrix.setDepartment(results.getString("FC_TITLE"));
            dataList.add(matrix);
        }
        results.close();
        statement.close();
        return dataList;
    }

    public static List<ResultMatrixList> getFactores(String[] departments, String structureDate) throws SQLException {
        String where = departments != null ? "ec.FN_USERID IN (" + StringUtils.join(departments, ",") + ")" : "1 = 0";
        
        String query = "SELECT  FC_DESC_FACTOR, \n" +
                        "        FC_TITLE,\n" +
                        "        ROUND(AVG(tbl.score), 2) FN_SINGLE_SCORE\n" +
                        "FROM \n" +
                        "    (SELECT f.FC_DESC_FACTOR, sf.FN_ID_SUBFACTOR, sf.FN_POINTS,  \n" +
                        "            decode(ec.fc_desc_location,'Servicios Liverpool',ec.FC_TITLE, ec.FC_TITLE||' - '||ec.fc_desc_location) ||'-'||ec.FN_USERID FC_TITLE, \n" +
                        "            ec.FN_USERID, avg(ss.FN_TREE_SCORE)  score \n" +
                        "     FROM  PUL_EMPLOYEE_CLIMA  ec       \n" +
                        "     LEFT JOIN  PUL_SUBFACTOR_SCORE ss ON ec.FN_ID_EMPLOYEE_CLIMA = ss.FN_ID_EMPLOYEE_CLIMA           \n" +
                        "     LEFT JOIN PUL_SUBFACTOR sf ON sf.FC_DESC_SUBFACTOR = ss.FC_DESC_SUBFACTOR \n" +
                        "     LEFT JOIN PUL_FACTOR     f ON f.FN_ID_FACTOR = sf.FN_ID_FACTOR        \n" +
                        "     WHERE " + where + "\n" +
                        "     AND   ss.FN_TREE_SCORE IS NOT NULL \n" +
                        "     AND   TO_CHAR(ec.FD_STRUCTURE_DATE, 'MM/YYYY') = '" + structureDate + "'\n" +
                        "     AND   f.FN_ID_SURVEY in (select s.FN_ID_SURVEY from pul_survey s where TO_CHAR(s.FD_SURVEY_DATE, 'MM/YYYY')= '" + structureDate + "') \n" +
                        "     AND   ss.FN_TREE_SCORE <> 0 \n" +
                        "     GROUP BY f.FC_DESC_FACTOR, sf.FN_ID_SUBFACTOR, sf.FN_POINTS, ec.fc_desc_location,ec.FC_TITLE, ec.FN_USERID                \n" +
                        "    ) tbl\n" +
                        "WHERE FC_DESC_FACTOR IS NOT NULL\n" +
                        "GROUP BY FC_DESC_FACTOR, FC_TITLE";

        OracleConnection connection = getOracleConnection();
   
        CallableStatement statement = connection.prepareCall(query);
        statement.execute();

        ResultSet results = ((OracleCallableStatement) statement).getResultSet();

        List<ResultMatrixList> dataList = new ArrayList();

        while (results.next()) {
            ResultMatrixList matrix = new ResultMatrixList();

            matrix.setDepartment(results.getString("FC_TITLE"));
            matrix.setFactor(results.getString("FC_DESC_FACTOR"));
            matrix.setScore(results.getFloat("FN_SINGLE_SCORE"));
            dataList.add(matrix);
        }
        results.close();
        statement.close();
        return dataList;
    }

    public static List getQuestion(String[] departments, String structureDate) throws SQLException {
        String andWhere = departments != null ? "ec.FN_USERID IN (" + StringUtils.join(departments, ",") + ")" : "1 = 0";
        
        String query = "SELECT distinct qas.FC_DESC_SUBFACTOR, qas.FC_QUESTION,\n" +
                        "      case when ec.fc_desc_location = 'Servicios Liverpool' then\n" +
                        "		ec.fc_title\n" +
                        "	else\n" +
                        "		ec.FC_TITLE||' - '||ec.fc_desc_location \n" +
                        "	end fc_title,\n" + 
                        "      round(qas.FN_TREE_SCORE_PERCENT, 4) SCORE \n" +
                        "      FROM   PORTALUNICO.PUL_QUESTION_ANSWER_SCORE qas\n" +
                        "      JOIN PUL_EMPLOYEE_CLIMA ec ON ec.FN_ID_EMPLOYEE_CLIMA = qas.FN_ID_EMPLOYEE_CLIMA\n" +
                        "      JOIN   PORTALUNICO.PUL_SUBFACTOR sf ON sf.FC_DESC_SUBFACTOR = qas.FC_DESC_SUBFACTOR \n" +
                        "      JOIN   PORTALUNICO.PUL_FACTOR f ON f.FN_ID_FACTOR = sf.FN_ID_FACTOR\n" +
                        "      WHERE  TO_CHAR(qas.FD_STRUCTURE_DATE, 'MM/YYYY') = '" + structureDate + "'\n" +
                        "      AND    TO_CHAR(ec.FD_STRUCTURE_DATE, 'MM/YYYY') = '" + structureDate + "'\n" +
                        "      AND    f.FN_ID_SURVEY in (select s.FN_ID_SURVEY from pul_survey s where TO_CHAR(s.FD_SURVEY_DATE, 'MM/YYYY')= '" + structureDate + "')\n" +
                        "      AND " + andWhere;

        OracleConnection connection = getOracleConnection();
        CallableStatement statement = connection.prepareCall(query);
        statement.execute();

        ResultSet results = ((OracleCallableStatement) statement).getResultSet();

        List<ResultMatrixList> dataList = new ArrayList();

        while (results.next()) {
            ResultMatrixList matrix = new ResultMatrixList();
            matrix.setQuestion(results.getString("FC_QUESTION"));
            matrix.setSubFactor(results.getString("FC_DESC_SUBFACTOR"));
            matrix.setScore(results.getFloat("score"));
            matrix.setDepartment(results.getString("FC_TITLE"));
            dataList.add(matrix);
        }
        results.close();
        statement.close();
        return dataList;

   }

    public static ResumeTable getResume(int userId, String structureDate) {
        ResumeTable resume = new ResumeTable();
        try {
            String query = "select encuestas,\n" +
                            "       contestadas,\n" +
                            "       porcentaje,\n" +
                            "       calificacion \n" +
                            "from (\n" +
                            "SELECT  fn_id_survey,\n" +
                            "        encuestas,\n" +
                            "        contestadas,\n" +
                            "        case when encuestas = 0 then\n" +
                            "                0\n" +
                            "             else\n" +
                            "                ROUND((nvl(contestadas, 0) / encuestas) * 100, 2) \n" +
                            "        end porcentaje,\n" +
                            "        round(FN_GET_FINAL_DIRECT_SCORE (:PN_USERID, :PN_DATE), 2) calificacion           \n" +
                            "FROM (SELECT  DECODE(SU.FN_ID_SURVEY_TYPE,1,SE.FN_PLAN,(select count(1) \n" +
                            "                                                        from   pul_employee_clima \n" +
                            "                                                        where  fn_manager=:PN_USERID \n" +
                            "                                                        and    to_char(FD_STRUCTURE_DATE, 'MM/YYYY') = :PN_DATE)) encuestas,\n" +
                            "              PORTALUNICO.FN_GET_DIRECT_SURVEY_RESOLVED (:PN_DATE, :PN_USERID) contestadas,\n" +
                            "              EC.FN_ID_EMPLOYEE_CLIMA,\n" +
                            "              EC.FN_USERID,\n" +
                            "              nvl(SU.FN_ID_SURVEY_TYPE, 0) FN_ID_SURVEY_TYPE,\n" +
                            "              su.FN_ID_SURVEY \n" +
                            "      FROM  PUL_EMPLOYEE_CLIMA EC \n" +
                            "      LEFT JOIN  PUL_SURVEY_EMPLOYEE SE ON SE.FN_ID_EMPLOYEE_CLIMA = EC.FN_ID_EMPLOYEE_CLIMA\n" +
                            "      LEFT JOIN  PUL_SURVEY SU ON SE.FN_ID_SURVEY  = SU.FN_ID_SURVEY\n" +
                            "      WHERE EC.FN_USERID                           = :PN_USERID\n" +
                            "      AND TO_CHAR(ec.FD_STRUCTURE_DATE, 'MM/YYYY') = :PN_DATE\n" +
                            "      AND   (SU.FN_ID_SURVEY in (select s.FN_ID_SURVEY from pul_survey s where TO_CHAR(s.FD_SURVEY_DATE, 'MM/YYYY')=:PN_DATE)\n" +
                            "        OR  SE.FN_ID_EMPLOYEE_CLIMA IS NULL) \n" +
                            "      GROUP BY SU.FN_ID_SURVEY_TYPE, SE.FN_PLAN, FN_ANSWERED, EC.FN_ID_EMPLOYEE_CLIMA, EC.FN_USERID, SU.FN_ID_SURVEY, ec.FD_STRUCTURE_DATE) tbl1\n" +
                            "ORDER BY calificacion desc\n" +
                            ") tb\n" +
                            "WHERE ROWNUM = 1";
            
            OracleConnection connection = getOracleConnection();
            
            OracleCallableStatement statement = (OracleCallableStatement)connection.prepareCall(query);
            statement.setIntAtName("PN_USERID", userId);
            statement.setStringAtName("PN_DATE", structureDate);
        
            statement.execute();
            
            ResultSet results = ((OracleCallableStatement) statement).getResultSet();

            if (results.next()) {
                resume.setSurveys(results.getInt("encuestas"));
                resume.setAnswers(results.getInt("contestadas"));
                resume.setPercentage(results.getFloat("porcentaje"));
                resume.setScore(results.getFloat("calificacion"));
            }
            results.close();
            statement.close();
        } catch (SQLException e) {
            Logger.error(e.getMessage());
        }

        return resume;
    }
    
    public static ResumeTable getResumeTree(int userId, String structureDate) {
        ResumeTable resume = new ResumeTable();
        try {
            String query = "select encuestas,\n" +
                            "       contestadas,\n" +
                            "       porcentaje,\n" +
                            "       calificacion \n" +
                            "from (\n" +
                            "SELECT  fn_id_survey,\n" +
                            "        encuestas,\n" +
                            "        contestadas,\n" +
                            "        case when encuestas = 0 then\n" +
                            "                0\n" +
                            "             else\n" +
                            "                ROUND((nvl(contestadas, 0) / encuestas) * 100, 2) \n" +
                            "        end porcentaje,\n" +
                            "        FN_GET_FINAL_SCORE(:PN_USERID, :PN_DATE) calificacion          \n" +
                            "FROM ( SELECT  PORTALUNICO.FN_GET_TODO_SURVEY(:PN_DATE, :PN_USERID) encuestas,\n" +
                            "               PORTALUNICO.FN_GET_SURVEY_RESOLVED(:PN_DATE, :PN_USERID) contestadas,\n" +
                            "              EC.FN_ID_EMPLOYEE_CLIMA,\n" +
                            "              EC.FN_USERID,\n" +
                            "              nvl(SU.FN_ID_SURVEY_TYPE, 0) FN_ID_SURVEY_TYPE,\n" +
                            "              su.FN_ID_SURVEY \n" +
                            "      FROM  PUL_EMPLOYEE_CLIMA EC \n" +
                            "      LEFT JOIN  PUL_SURVEY_EMPLOYEE SE ON SE.FN_ID_EMPLOYEE_CLIMA = EC.FN_ID_EMPLOYEE_CLIMA\n" +
                            "      LEFT JOIN  PUL_SURVEY SU ON SE.FN_ID_SURVEY  = SU.FN_ID_SURVEY\n" +
                            "      WHERE EC.FN_USERID                           = :PN_USERID\n" +
                            "      AND TO_CHAR(ec.FD_STRUCTURE_DATE, 'MM/YYYY') = :PN_DATE\n" +
                            "      GROUP BY SU.FN_ID_SURVEY_TYPE, SE.FN_PLAN, FN_ANSWERED, EC.FN_ID_EMPLOYEE_CLIMA, EC.FN_USERID, SU.FN_ID_SURVEY\n" +
                            "      ) tbl1\n" +
                            "ORDER BY calificacion desc\n" +
                            ") tb\n" +
                            "WHERE ROWNUM = 1";
            
            OracleConnection connection = getOracleConnection();

            OracleCallableStatement statement = (OracleCallableStatement)connection.prepareCall(query);
            statement.setIntAtName("PN_USERID", userId);
            statement.setStringAtName("PN_DATE", structureDate);
            
            statement.execute();
            
            ResultSet results = ((OracleCallableStatement) statement).getResultSet();

            if (results.next()) {
                resume.setSurveys(results.getInt("encuestas"));
                resume.setAnswers(results.getInt("contestadas"));
                resume.setPercentage(results.getFloat("porcentaje"));
                resume.setScore(results.getFloat("calificacion"));
            }
            results.close();
            statement.close();
        } catch (SQLException e) {
            Logger.error(e.getMessage());
        }

        return resume;
    }
}
