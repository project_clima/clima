package querys;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import play.*;
import play.db.jpa.JPA;
import java.util.*;
import java.util.logging.Level;
import javax.persistence.Query;
import oracle.jdbc.OracleCallableStatement;
import oracle.jdbc.OracleConnection;
import oracle.jdbc.OracleTypes;
import static utils.Queries.getOracleConnection;
import utils.PagingResult;
import utils.nps.connectionDB.DbConnection;

/**
 * Clase de utileria para ejecutar los querys del Ranking
 * @author Rodolfo Miranda -- Qualtop
 */
public class RankingQuery {

    /**
     * Metodo para clasificar las opciones de los querys
     * @param option
     * @return 
     */
    public static String getCondition(String option) {
        String cond = "";

        if(option.equals("warehouse")) {
            cond =
                "(ec1.FC_DESC_LOCATION like 'Liverpool%' or " +
                "ec1.FC_DESC_LOCATION like 'Fábricas%'  or " +
                "ec1.FC_DESC_LOCATION like 'Fabricas%')";
        } else if(option.equals("cellar")) {
            cond = "ec1.FC_DESC_LOCATION like 'Bodega%'";
        } else if(option.equals("distCenter")) {
            cond =
                " tabla1.FC_DIVISION in(" +
                " SELECT" +
                " DISTINCT FC_DIVISION" +
                " FROM PORTALUNICO.PUL_EMPLOYEE_CLIMA" +
                " WHERE FC_DEPARTMENT like '%Centro Distribucion%'" +
                " AND FC_DIVISION  <> 'CORP')";
        } else if(option.equals("adcon")) {
            cond = "tabla1.FC_DIVISION like '%Adcon%'";
        }

        return cond;
    }

    /**
     * Query para obtener el ranking por usuario
     * @param idEmployye
     * @param fecha
     * @return 
     */
    public static List getRankingByUser(String idEmployye, String fecha) {
        String statement = "SELECT FC_TITLE, " +
                        "  PORTALUNICO.FN_FACTOR_BY_EMPLOYEE_EMP(FN_ID_EMPLOYEE_CLIMA ,1) Organizacion, " +
                        "  PORTALUNICO.FN_FACTOR_BY_EMPLOYEE_EMP(FN_ID_EMPLOYEE_CLIMA ,2) DesarrolloCrecimiento, " +
                        "  PORTALUNICO.FN_FACTOR_BY_EMPLOYEE_EMP(FN_ID_EMPLOYEE_CLIMA ,3) Clientes, " +
                        "  PORTALUNICO.FN_FACTOR_BY_EMPLOYEE_EMP(FN_ID_EMPLOYEE_CLIMA ,4) TrabajoEquipo, " +
                        "  PORTALUNICO .FN_FACTOR_BY_EMPLOYEE_EMP(FN_ID_EMPLOYEE_CLIMA ,5) Liderazgo " +
                        "FROM " +
                        "  (SELECT " +
                        "    (SELECT FC_TITLE " +
                        "    FROM PUL_EMPLOYEE_CLIMA PEM " +
                        "    WHERE PEM.FN_USERID                        = TABLEQ.FN_MANAGER " +
                        "    AND TO_CHAR (fd_structure_date, 'MM/YYYY') = '"+fecha+"' " +
                        "    ) TITLE_MANAGER, " +
                        "    DECODE(fc_division,'CORP',FC_TITLE, FC_TITLE " +
                        "    ||' - ' " +
                        "    ||FC_DIVISION) FC_TITLE, " +
                        "    S.FC_NAME, " +
                        "    NVL(FN_COUNT_DEPENDENCIES_DATE(TABLEQ.FN_USERID,'"+fecha+"') , 0) ALI, " +
                        "    NVL(FN_COUNT_ASWERED_DATE(TABLEQ.FN_USERID,'"+fecha+"'), 0) ANSWERED, " +
                        "    NVL(DECODE(FN_COUNT_DEPENDENCIES_DATE(TABLEQ.FN_USERID,'"+fecha+"') ,0,0,(FN_COUNT_ASWERED_DATE(TABLEQ.FN_USERID,'"+fecha+"') / FN_COUNT_DEPENDENCIES_DATE(TABLEQ.FN_USERID,'"+fecha+"') )*100), 0) PORCENT, " +
                        "    TABLEQ.FN_LEVEL, " +
                        "    TABLEQ.FN_MANAGER, " +
                        "    SE.FN_ID_EMPLOYEE_CLIMA, " +
                        "    NVL(SE.FN_PLAN, 0) FN_PLAN, " +
                        "    TABLEQ.FN_USERID " +
                        "  FROM PUL_SURVEY_EMPLOYEE SE, " +
                        "    PUL_SURVEY S, " +
                        "    (SELECT FN_ID_EMPLOYEE_CLIMA, " +
                        "      FN_LEVEL, " +
                        "      FC_TITLE, " +
                        "      FC_DIVISION, " +
                        "      FN_USERID, " +
                        "      FC_LAST_NAME, " +
                        "      FN_MANAGER, " +
                        "      CONNECT_BY_ROOT FN_USERID AS ROOT_ID " +
                        "    FROM " +
                        "      (SELECT * " +
                        "      FROM portalunico.PUL_EMPLOYEE_CLIMA pe " +
                        "      WHERE pe.FN_LEVEL = " +
                        "        (SELECT FN_LEVEL + 1 " +
                        "        FROM PUL_EMPLOYEE_CLIMA " +
                        "        WHERE FN_USERID                            ="+idEmployye+
                        "        AND TO_CHAR (fd_structure_date, 'MM/YYYY') = '"+fecha+"' " +
                        "        ) " +
                        "      AND TO_CHAR (fd_structure_date, 'MM/YYYY') = '"+fecha+"' " +
                        "      ) " +
                        "      CONNECT BY PRIOR FN_USERID = FN_MANAGER " +
                        "      START WITH FN_MANAGER      = "+idEmployye+
                        "    ) TABLEQ " +
                        "  WHERE SE.FN_ID_EMPLOYEE_CLIMA           = TABLEQ.FN_ID_EMPLOYEE_CLIMA " +
                        "  AND SE.FN_ID_SURVEY                     = S.FN_ID_SURVEY " +
                        "  AND TO_CHAR(S.FD_SURVEY_DATE,'MM/YYYY') ='"+fecha+"' " +
                        "  AND se.FN_ID_EMPLOYEE_CLIMA NOT        IN " +
                        "    (SELECT FN_ID_EMPLOYEE_CLIMA " +
                        "    FROM " +
                        "      (SELECT " +
                        "        (SELECT FC_TITLE " +
                        "        FROM PUL_EMPLOYEE_CLIMA PEM " +
                        "        WHERE PEM.FN_USERID                        = TABLEQ.FN_MANAGER " +
                        "        AND TO_CHAR (fd_structure_date, 'MM/YYYY') = '"+fecha+"' " +
                        "        ) TITLE_MANAGER, " +
                        "        DECODE(fc_division,'CORP',FC_TITLE, FC_TITLE " +
                        "        ||' - ' " +
                        "        ||FC_DIVISION) FC_TITLE, " +
                        "        S.FC_NAME, " +
                        "        NVL(FN_COUNT_DEPENDENCIES_DATE(TABLEQ.FN_USERID,'"+fecha+"') , 0) ALI, " +
                        "        NVL(FN_COUNT_ASWERED_DATE(TABLEQ.FN_USERID,'"+fecha+"'), 0) ANSWERED, " +
                        "        NVL(DECODE(FN_COUNT_DEPENDENCIES_DATE (TABLEQ.FN_USERID,'"+fecha+"') ,0,0,(FN_COUNT_ASWERED_DATE(TABLEQ.FN_USERID,'"+fecha+"') / FN_COUNT_DEPENDENCIES_DATE(TABLEQ.FN_USERID,'"+fecha+"') )*100), 0) PORCENT , " +
                        "        TABLEQ.FN_LEVEL, " +
                        "        TABLEQ.FN_MANAGER, " +
                        "        SE.FN_ID_EMPLOYEE_CLIMA, " +
                        "        NVL(SE.FN_PLAN, 0) FN_PLAN, " +
                        "        TABLEQ.FN_USERID " +
                        "      FROM PUL_SURVEY_EMPLOYEE SE, " +
                        "        PUL_SURVEY S, " +
                        "        (SELECT FN_ID_EMPLOYEE_CLIMA, " +
                        "          FN_LEVEL, " +
                        "          FC_TITLE, " +
                        "          FC_DIVISION, " +
                        "          FN_USERID, " +
                        "          FC_LAST_NAME, " +
                        "          FN_MANAGER, " +
                        "          CONNECT_BY_ROOT FN_USERID AS ROOT_ID " +
                        "        FROM " +
                        "          (SELECT * " +
                        "          FROM portalunico.PUL_EMPLOYEE_CLIMA pe " +
                        "          WHERE pe.FN_LEVEL = " +
                        "            (SELECT FN_LEVEL + 1 " +
                        "            FROM PUL_EMPLOYEE_CLIMA " +
                        "            WHERE FN_USERID                            ="+idEmployye+" " +
                        "            AND TO_CHAR (fd_structure_date, 'MM/YYYY') = '"+fecha+"' " +
                        "            ) " +
                        "          AND TO_CHAR (fd_structure_date, 'MM/YYYY') = '"+fecha+"' " +
                        "          ) " +
                        "          CONNECT BY PRIOR FN_USERID = FN_MANAGER " +
                        "          START WITH FN_MANAGER      = "+idEmployye+
                        "        ) TABLEQ " +
                        "      WHERE SE.FN_ID_EMPLOYEE_CLIMA           = TABLEQ.FN_ID_EMPLOYEE_CLIMA " +
                        "      AND SE.FN_ID_SURVEY                     = S.FN_ID_SURVEY " +
                        "      AND TO_CHAR(S.FD_SURVEY_DATE,'MM/YYYY') ='"+fecha+"' " +
                        "      ) " +
                        "    GROUP BY FN_ID_EMPLOYEE_CLIMA " +
                        "    HAVING COUNT(1) >1 " +
                        "    ) " +
                        "  UNION " +
                        "  SELECT " +
                        "    (SELECT FC_TITLE " +
                        "    FROM PUL_EMPLOYEE_CLIMA PEM " +
                        "    WHERE PEM.FN_USERID                        = TABLEQ.FN_MANAGER " +
                        "    AND TO_CHAR (fd_structure_date, 'MM/YYYY') = '"+fecha+"' " +
                        "    ) TITLE_MANAGER, " +
                        "    DECODE(fc_division,'CORP',FC_TITLE, FC_TITLE " +
                        "    ||' - ' " +
                        "    ||FC_DIVISION) FC_TITLE, " +
                        "    S.FC_NAME, " +
                        "    NVL(FN_COUNT_DEPENDENCIES_DATE(TABLEQ.FN_USERID,'"+fecha+"') , 0) ALI, " +
                        "    NVL(FN_COUNT_ASWERED_DATE(TABLEQ.FN_USERID,'"+fecha+"'), 0) ANSWERED, " +
                        "    NVL(DECODE(FN_COUNT_DEPENDENCIES_DATE(TABLEQ.FN_USERID,'"+fecha+"') ,0,0,(FN_COUNT_ASWERED_DATE(TABLEQ.FN_USERID,'"+fecha+"') / FN_COUNT_DEPENDENCIES_DATE(TABLEQ .FN_USERID,'"+fecha+"') )*100), 0) PORCENT, " +
                        "    TABLEQ.FN_LEVEL, " +
                        "    TABLEQ.FN_MANAGER , " +
                        "    SE.FN_ID_EMPLOYEE_CLIMA, " +
                        "    NVL(SE.FN_PLAN, 0) FN_PLAN, " +
                        "    TABLEQ.FN_USERID " +
                        "  FROM PUL_SURVEY_EMPLOYEE SE, " +
                        "    PUL_SURVEY S, " +
                        "    (SELECT FN_ID_EMPLOYEE_CLIMA, " +
                        "      FN_LEVEL, " +
                        "      FC_TITLE, " +
                        "      FC_DIVISION, " +
                        "      FN_USERID, " +
                        "      FC_LAST_NAME, " +
                        "      FN_MANAGER, " +
                        "      CONNECT_BY_ROOT FN_USERID AS ROOT_ID " +
                        "    FROM " +
                        "      (SELECT * " +
                        "      FROM portalunico.PUL_EMPLOYEE_CLIMA pe " +
                        "      WHERE pe.FN_LEVEL = " +
                        "        (SELECT FN_LEVEL + 1 " +
                        "        FROM PUL_EMPLOYEE_CLIMA " +
                        "        WHERE FN_USERID                            ="+idEmployye+" " +
                        "        AND TO_CHAR (fd_structure_date, 'MM/YYYY') = '"+fecha+"' " +
                        "        ) " +
                        "      AND TO_CHAR (fd_structure_date, 'MM/YYYY') = '"+fecha+"' " +
                        "      ) " +
                        "      CONNECT BY PRIOR FN_USERID = FN_MANAGER " +
                        "      START WITH FN_MANAGER      = "+idEmployye+
                        "    ) TABLEQ " +
                        "  WHERE SE.FN_ID_EMPLOYEE_CLIMA           = TABLEQ.FN_ID_EMPLOYEE_CLIMA " +
                        "  AND SE.FN_ID_SURVEY                     = S.FN_ID_SURVEY " +
                        "  AND TO_CHAR(S.FD_SURVEY_DATE,'MM/YYYY') ='"+fecha+"' " +
                        "  AND S.FN_ID_SURVEY_TYPE                 = 1 " +
                        "  AND SE.FN_ID_EMPLOYEE_CLIMA            IN " +
                        "    (SELECT FN_ID_EMPLOYEE_CLIMA " +
                        "    FROM " +
                        "      (SELECT " +
                        "        (SELECT FC_TITLE " +
                        "        FROM PUL_EMPLOYEE_CLIMA PEM " +
                        "        WHERE PEM.FN_USERID                        = TABLEQ.FN_MANAGER " +
                        "        AND TO_CHAR (fd_structure_date, 'MM/YYYY') = '"+fecha+"' " +
                        "        ) TITLE_MANAGER, " +
                        "        DECODE(fc_division,'CORP',FC_TITLE, FC_TITLE " +
                        "        ||' - ' " +
                        "        ||FC_DIVISION) FC_TITLE, " +
                        "        S.FC_NAME, " +
                        "        NVL(FN_COUNT_DEPENDENCIES_DATE(TABLEQ.FN_USERID,'"+fecha+"') , 0) ALI, " +
                        "        NVL(FN_COUNT_ASWERED_DATE(TABLEQ.FN_USERID,'"+fecha+"'), 0) ANSWERED, " +
                        "        NVL(DECODE(NVL(FN_COUNT_DEPENDENCIES_DATE(TABLEQ.FN_USERID,'"+fecha+"') , 0),0,0,(FN_COUNT_ASWERED_DATE(TABLEQ.FN_USERID,'"+fecha+"') / NVL(FN_COUNT_DEPENDENCIES_DATE(TABLEQ.FN_USERID,'"+fecha+"') , 0))* 100), 0) PORCENT, " +
                        "        TABLEQ.FN_LEVEL, " +
                        "        TABLEQ.FN_MANAGER, " +
                        "        SE.FN_ID_EMPLOYEE_CLIMA, " +
                        "        NVL(SE.FN_PLAN, 0) FN_PLAN, " +
                        "        TABLEQ.FN_USERID " +
                        "      FROM PUL_SURVEY_EMPLOYEE SE, " +
                        "        PUL_SURVEY S, " +
                        "        (SELECT FN_ID_EMPLOYEE_CLIMA, " +
                        "          FN_LEVEL, " +
                        "          FC_TITLE, " +
                        "          FC_DIVISION, " +
                        "          FN_USERID, " +
                        "          FC_LAST_NAME, " +
                        "          FN_MANAGER, " +
                        "          CONNECT_BY_ROOT FN_USERID AS ROOT_ID " +
                        "        FROM " +
                        "          (SELECT * " +
                        "          FROM portalunico.PUL_EMPLOYEE_CLIMA pe " +
                        "          WHERE pe.FN_LEVEL = " +
                        "            (SELECT FN_LEVEL + 1 " +
                        "            FROM PUL_EMPLOYEE_CLIMA " +
                        "            WHERE FN_USERID                            ="+idEmployye+
                        "            AND TO_CHAR (fd_structure_date, 'MM/YYYY') = '"+fecha+"' " +
                        "            ) " +
                        "          AND TO_CHAR (fd_structure_date, 'MM/YYYY') = '"+fecha+"' " +
                        "          ) " +
                        "          CONNECT BY PRIOR FN_USERID = FN_MANAGER " +
                        "          START WITH FN_MANAGER      = "+idEmployye+
                        "        ) TABLEQ " +
                        "      WHERE SE.FN_ID_EMPLOYEE_CLIMA           = TABLEQ.FN_ID_EMPLOYEE_CLIMA " +
                        "      AND SE.FN_ID_SURVEY                     = S.FN_ID_SURVEY " +
                        "      AND TO_CHAR(S.FD_SURVEY_DATE,'MM/YYYY') ='"+fecha+"' " +
                        "      ) " +
                        "    GROUP BY FN_ID_EMPLOYEE_CLIMA " +
                        "    HAVING COUNT(1) >1 " +
                        "    ) " +
                        "  ) tablaGeneral " +
                        "WHERE tablaGeneral.ALI >0";

        Query oQuery = JPA.em().createNativeQuery(statement);
        return oQuery.getResultList();
    }

    /**
     * Metodo para ejecutar el query de ranking por opcion
     * @param option
     * @param date
     * @return 
     */
    public static List getRankingByOption(String option, Date firstDate, 
                                    Date endDate) throws SQLException {
        Connection oracleConnection = DbConnection.getDBConnection();
       
       CallableStatement callable = null;
       ResultSet results = null;
       
       try{
           if(oracleConnection != null){
               callable = oracleConnection.prepareCall("BEGIN PORTALUNICO.PR_GET_RANKING_REPORT(?, ?, ?, ?); END;");
        
                callable.setDate(1, new java.sql.Date(firstDate.getTime()));
                callable.setDate(2, new java.sql.Date(endDate.getTime()));
                callable.setString(3, option);
                callable.registerOutParameter(4, OracleTypes.CURSOR);
                callable.execute();
                
                results = ((OracleCallableStatement)callable).getCursor(4);
                
                ResultSetMetaData metaData = results.getMetaData();

                int columnsCount = metaData.getColumnCount();

                Object[] data;
                List resultsList = new ArrayList();

                while (results.next()) {
                    data = new Object[columnsCount];

                    for (int i = 1; i <= columnsCount; i++) {
                        data[i - 1] = results.getObject(i);
                    }

                    resultsList.add(data);
                }
                
                return resultsList;
           }
       } catch (SQLException ex) {
            java.util.logging.Logger.getLogger(RankingQuery.class.getName()).log(Level.SEVERE, null, ex);
        }finally {
            if (callable != null) {
                callable.close();
            }
            if (results != null) {
                results.close();
            }
        }
       return null;
    }
}