package querys;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.sql.CallableStatement;
import java.sql.SQLException;
import javax.persistence.Query;
import play.*;
import oracle.jdbc.OracleCallableStatement;
import oracle.jdbc.OracleConnection;
import oracle.jdbc.OracleTypes;
import play.db.jpa.JPA;
import static utils.Queries.getOracleConnection;

/**
 * Clase de utileria para ejecucion de querys para la creacion de fotos en Clima
 * @author Rodolfo Miranda -- Qualtop
 */
public class SnapshotQuery {
    /**
     * MEtodo para crear fotos en Clima
     * @param date
     * @param user
     * @return 
     */
    public static int createSnapshot(String date, String user) {
        int result = 0;

        try {
            OracleConnection connection = getOracleConnection();
            CallableStatement statement = connection.prepareCall("{ ? = call PORTALUNICO.FN_INS_CONGELAR_FOTO(?, ?) }");

            statement.setString(2, date);
            statement.setString(3, user);
            statement.registerOutParameter(1, OracleTypes.NUMBER);
            statement.execute();

            result = ((OracleCallableStatement) statement).getInt(1);
            statement.close();
            Logger.info("Foto terminanda ");
        } catch (SQLException e) {
            Logger.error("Foto terminada " + e.getMessage());
        }

        return result;
    }
    
    /**
     * Metodo para la actualizacion de las fotos en clima
     * @param newPeriod
     * @param oldPeriod
     * @param user
     * @return 
     */
    public static int updateSnapshot(String newPeriod, String oldPeriod, String user) {
        String statement = "UPDATE PUL_EMPLOYEE_CLIMA SET FD_STRUCTURE_DATE = ?1, FC_MOD_FOR = ?2 WHERE to_char(FD_STRUCTURE_DATE,'MM/YYYY') = ?3";
        
        Query oQuery   = JPA.em().createNativeQuery(statement);
        oQuery.setParameter(1, newPeriod);
        oQuery.setParameter(2, user);
        oQuery.setParameter(3, oldPeriod);
        
        return oQuery.executeUpdate();
    }
    
    /**
     * Metodo para obtener el total de empleados por periodo de foto
     * @param period
     * @return 
     */
    public static int getEmployeesNumber(String period) {
        String statement = "SELECT COUNT(*) FROM PUL_SURVEY_EMPLOYEE SE\n" +
                           "INNER JOIN PUL_EMPLOYEE_CLIMA EC\n" +
                           "ON EC.FN_ID_EMPLOYEE_CLIMA = SE.FN_ID_EMPLOYEE_CLIMA\n" +
                           "WHERE to_char(EC.FD_STRUCTURE_DATE,'MM/YYYY') = ?1";
        
        Query oQuery   = JPA.em().createNativeQuery(statement);
        oQuery.setParameter(1, period);
        
        return ((BigDecimal) oQuery.getSingleResult()).intValue();
    }
}
