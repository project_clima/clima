package querys;

import java.sql.Array;
import java.sql.CallableStatement;
import java.sql.SQLException;
import java.util.List;
import javax.persistence.Query;
import play.*;
import oracle.jdbc.OracleCallableStatement;
import oracle.jdbc.OracleConnection;
import oracle.jdbc.OracleTypes;
import play.db.jpa.JPA;
import static utils.Queries.getOracleConnection;

/**
 * Clase de utileria para ejecucion de querys para el manejo de duplicados y vacantes
 * @author Rodolfo Miranda -- Qualtop
 */
public class DuplicatesQuery {
    /**
     * Metodo para guardar empleados duplicados
     * @param period
     * @param userId
     * @param position
     * @param location
     * @param function
     * @param dependants
     * @param action
     * @param username
     * @param managerId
     * @return 
     */
    public static int saveDuplicate(String period, Integer userId, String position,
                                    Integer location, String function, Integer[][] dependants,
                                    String action, String username, Integer managerId) {        
        int result = 0;
        
        try {
            OracleConnection connection = getOracleConnection();
            
            CallableStatement statement = (OracleCallableStatement)connection.prepareCall(
                "{ ? = call PORTALUNICO.FN_INS_EMPLOYEE_CLIMA_DOUBLE(?, ?, ?, ?, ?, ?, ?, ?, ?) }"
            );
            
            Array subordinates = connection.createOracleArray("PORTALUNICO.TY_DEPENDENCIES", dependants);

            statement.setString(2, period);
            statement.setInt(3, userId);
            statement.setString(4, position);
            statement.setString(5, function);
            statement.setInt(6, location);
            statement.setString(7, username);
            statement.setArray(8, subordinates);
            statement.setString(9, action);
            statement.setInt(10, managerId);
            statement.registerOutParameter(1, OracleTypes.NUMBER);
            statement.executeUpdate();
            
            result = 1;
            statement.close();
        } catch (SQLException e) {
            Logger.error(e.getMessage());
        }
        
        return result;
    }
    
    /**
     * Metodo para gurdar empleados vacantes
     * @param period
     * @param managerId
     * @param position
     * @param location
     * @param function
     * @param dependants
     * @param action
     * @param userId
     * @param username
     * @return 
     */
    public static int saveVacant(String period, Integer managerId, String position,
                                 Integer location, String function, Integer[][] dependants,
                                 String action, Integer userId, String username) {        
        int result = 0;
        
        try {
            OracleConnection connection = getOracleConnection();
            
            CallableStatement statement = connection.prepareCall(
                "{ ? = call PORTALUNICO.FN_INS_EMPLOYEE_CLIMA_VACANT(?, ?, ?, ?, ?, ?, ?, ?, ?) }"
            );
            
            Array subordinates = connection.createOracleArray(
                "PORTALUNICO.TY_DEPENDENCIES", dependants
            );
            
            statement.setString(2, period);
            statement.setString(3, position);
            statement.setString(4, function);
            statement.setLong(5, location);
            statement.setLong(6, managerId);
            statement.setArray(7, subordinates);
            statement.setString(8, username);            
            statement.setString(9, action);
            
            if (userId == null) {
                statement.setNull(10, OracleTypes.NUMBER);
            } else {
                statement.setInt(10, userId);
            }
            
            statement.registerOutParameter(1, OracleTypes.NUMBER);
            statement.execute();
            
            result = 1;
        } catch (SQLException e) {
            Logger.error(e.getMessage());
        }
        
        return result;
    }
    
    /**
     * Metodo que ejecuta el query para obtener los duplicados por fecha de estructura y
     * palabra de busqueda
     * @param structureDate
     * @param search
     * @param start
     * @param end
     * @return 
     */
    public static List getDuplicates(String structureDate, String search, int start, int end) {
        String query = "SELECT * FROM (\n" +
                        "SELECT\n" +
"                                EmployeeClima.FN_EMPLOYEE_NUMBER,\n" +
"                                EmployeeClima.FC_EMPLOYEE_NAME,\n" +
"                                EmployeeClima.FC_DESC_LOCATION,\n" +
"                                EmployeeClima.FC_TITLE,\n" +
"                                (select max (FC_FIRST_NAME || ' ' || FC_LAST_NAME) from PUL_EMPLOYEE_CLIMA WHERE FN_USERID = EmployeeClima.FN_MANAGER\n" +
"                                and  TO_CHAR(FD_STRUCTURE_DATE,'MM/YYYY') = :PN_DATE) FC_MANAGER_NAME,\n" +
"                                EmployeeClima.FN_ID_EMPLOYEE_CLIMA,\n" +
"                                EmployeeClima.FN_USERID,\n" +
"                                ROW_NUMBER() OVER (ORDER BY FC_EMPLOYEE_NAME) row_number\n" +
"                            FROM (\n" +
"                                SELECT FN_EMPLOYEE_NUMBER\n" +
"                                FROM   PORTALUNICO.PUL_EMPLOYEE_CLIMA\n" +
"                                WHERE  TO_CHAR(FD_STRUCTURE_DATE,'MM/YYYY') = :PN_DATE\n" +
"                                GROUP BY FN_EMPLOYEE_NUMBER  \n" +
"                                HAVING COUNT(FN_EMPLOYEE_NUMBER) > 1\n" +
"                            ) Duplicates\n" +
"                            JOIN (\n" +
"                                SELECT\n" +
"                                    FN_EMPLOYEE_NUMBER, FC_FIRST_NAME || ' ' || FC_LAST_NAME FC_EMPLOYEE_NAME, FC_DESC_LOCATION,\n" +
"                                    FC_TITLE, FN_ID_EMPLOYEE_CLIMA, FN_USERID, FD_STRUCTURE_DATE, FN_MANAGER\n" +
"                                FROM\n" +
"                                    PORTALUNICO.PUL_EMPLOYEE_CLIMA\n" +
"                                WHERE\n" +
"                                    TO_CHAR(FD_STRUCTURE_DATE,'MM/YYYY') = :PN_DATE\n" +
"                            ) EmployeeClima ON EmployeeClima.FN_EMPLOYEE_NUMBER = Duplicates.FN_EMPLOYEE_NUMBER\n" +
"                            WHERE EmployeeClima.FN_ID_EMPLOYEE_CLIMA > (\n" +
"                                SELECT\n" +
"                                    FN_ID_EMPLOYEE_CLIMA FROM PORTALUNICO.PUL_EMPLOYEE_CLIMA EC\n" +
"                                WHERE \n" +
"                                    EC.FN_EMPLOYEE_NUMBER = EmployeeClima.FN_EMPLOYEE_NUMBER\n" +
"                                    AND TO_CHAR(FD_STRUCTURE_DATE, 'MM/YYYY') = :PN_DATE\n" +
"                                    AND (LOWER(EmployeeClima.FC_EMPLOYEE_NAME) LIKE '%' || LOWER(:PN_SEARCH) || '%'\n" +
"                                    OR LOWER(EmployeeClima.FN_EMPLOYEE_NUMBER) LIKE '%' || LOWER(:PN_SEARCH) || '%')\n" +
"                                    ORDER BY FN_ID_EMPLOYEE_CLIMA ASC\n" +
"                                    FETCH FIRST 1 ROWS ONLY\n" +
"                            )\n" +
"                            ORDER BY FC_EMPLOYEE_NAME ASC" +
                        ") WHERE row_number BETWEEN :PN_START AND :PN_END";
        
        Query oQuery = JPA.em().createNativeQuery(query)
            .setParameter("PN_DATE", structureDate)
            .setParameter("PN_SEARCH", search)
            .setParameter("PN_START", start)
            .setParameter("PN_END", end);
        
        return oQuery.getResultList();
    }
    
    /**
     * Metodo para ayuda de la paginacion
     * @param structureDate
     * @param search
     * @return 
     */
    public static long countDuplicates(String structureDate, String search) {
        String query = "SELECT\n" +
                        "    count(1)\n" +
                        "FROM (\n" +
                        "    SELECT FN_EMPLOYEE_NUMBER\n" +
                        "    FROM   PORTALUNICO.PUL_EMPLOYEE_CLIMA\n" +
                        "    WHERE  TO_CHAR(FD_STRUCTURE_DATE,'MM/YYYY') = :PN_DATE\n" +
                        "    GROUP BY FN_EMPLOYEE_NUMBER  \n" +
                        "    HAVING COUNT(FN_EMPLOYEE_NUMBER) > 1\n" +
                        ") Duplicates\n" +
                        "LEFT JOIN (\n" +
                        "    SELECT\n" +
                        "        FN_EMPLOYEE_NUMBER, FC_FIRST_NAME || ' ' || FC_LAST_NAME FC_EMPLOYEE_NAME, FC_DESC_LOCATION, FC_TITLE, FN_ID_EMPLOYEE_CLIMA\n" +
                        "    FROM\n" +
                        "        PORTALUNICO.PUL_EMPLOYEE_CLIMA\n" +
                        "    WHERE\n" +
                        "        TO_CHAR(FD_STRUCTURE_DATE,'MM/YYYY') = :PN_DATE\n" +
                        ") EmployeeClima ON EmployeeClima.FN_EMPLOYEE_NUMBER = Duplicates.FN_EMPLOYEE_NUMBER\n" +
                        "WHERE FN_ID_EMPLOYEE_CLIMA > (\n" +
                        "    SELECT\n" +
                        "        FN_ID_EMPLOYEE_CLIMA FROM PORTALUNICO.PUL_EMPLOYEE_CLIMA EC\n" +
                        "    WHERE \n" +
                        "        EC.FN_EMPLOYEE_NUMBER = EmployeeClima.FN_EMPLOYEE_NUMBER\n" +
                        "        AND TO_CHAR(FD_STRUCTURE_DATE, 'MM/YYYY') = :PN_DATE\n" +
                        "            AND (LOWER(EmployeeClima.FC_EMPLOYEE_NAME) LIKE '%' || LOWER(:PN_SEARCH) || '%'\n" +
                        "            OR LOWER(EmployeeClima.FN_EMPLOYEE_NUMBER) LIKE '%' || LOWER(:PN_SEARCH) || '%')\n" +
                        "        ORDER BY FN_ID_EMPLOYEE_CLIMA ASC\n" +
                        "        FETCH FIRST 1 ROWS ONLY\n" +
                        ")";
        
        Query oQuery = JPA.em().createNativeQuery(query)
            .setParameter("PN_DATE", structureDate)
            .setParameter("PN_SEARCH", search);
        
        List result = oQuery.getResultList();
        return Long.parseLong(result.get(0).toString());
    }
    
    /**
     * Metodo que obtiene los empleados vacantes
     * @param structureDate
     * @param search
     * @param start
     * @param end
     * @return 
     */
    public static List getVacancies(String structureDate, String search, int start, int end) {
        String query = "SELECT * FROM (\n" +
                        "    SELECT\n" +                        
                        "        EmployeeClima.FC_EMPLOYEE_NAME,\n" +
                        "        EmployeeClima.FC_DESC_LOCATION,\n" +
                        "        EmployeeClima.FC_TITLE,\n" +
                        "        ManagerEC.FC_FIRST_NAME || ' ' || ManagerEC.FC_LAST_NAME FC_MANAGER_NAME,\n" +
                        "        EmployeeClima.FC_EMAIL,\n" +
                        "        EmployeeClima.FN_ID_EMPLOYEE_CLIMA,\n" +
                        "        EmployeeClima.FN_USERID,\n" +
                        "        EmployeeClima.FN_EMPLOYEE_NUMBER,\n" +
                        "        ROW_NUMBER() OVER (ORDER BY FC_EMPLOYEE_NAME) row_number\n" +
                        "    FROM (\n" +
                        "        SELECT FN_EMPLOYEE_NUMBER\n" +
                        "        FROM   PUL_EMPLOYEE_CLIMA\n" +
                        "        WHERE  TO_CHAR(FD_STRUCTURE_DATE,'MM/YYYY') = :PN_DATE\n" +
                        "        AND    FN_EMPLOYEE_NUMBER > 9000000000\n" +
                        "    ) Duplicates\n" +
                        "    LEFT JOIN (\n" +
                        "        SELECT\n" +
                        "            FN_EMPLOYEE_NUMBER, FC_FIRST_NAME || ' ' || FC_LAST_NAME FC_EMPLOYEE_NAME,\n" +
                        "            FC_DESC_LOCATION, FC_TITLE, FN_ID_EMPLOYEE_CLIMA, FN_USERID, FN_MANAGER,\n" +
                        "            FD_STRUCTURE_DATE, FC_EMAIL\n" +
                        "        FROM\n" +
                        "            PORTALUNICO.PUL_EMPLOYEE_CLIMA\n" +
                        "        WHERE\n" +
                        "            TO_CHAR(FD_STRUCTURE_DATE,'MM/YYYY') = :PN_DATE\n" +                        
                        "    ) EmployeeClima ON EmployeeClima.FN_EMPLOYEE_NUMBER = Duplicates.FN_EMPLOYEE_NUMBER\n" +
                        "    LEFT JOIN PORTALUNICO.PUL_EMPLOYEE_CLIMA ManagerEC ON ManagerEC.FN_USERID = EmployeeClima.FN_MANAGER\n" +
                        "    AND TO_CHAR(ManagerEC.FD_STRUCTURE_DATE,'MM/YYYY') = TO_CHAR(EmployeeClima.FD_STRUCTURE_DATE,'MM/YYYY')\n" +
                        "    WHERE LOWER(FC_EMPLOYEE_NAME) LIKE '%' || LOWER(:PN_SEARCH) || '%'\n" +
                        "    ORDER BY FC_EMPLOYEE_NAME ASC\n" +
                        ") WHERE row_number BETWEEN :PN_START AND :PN_END";
        
        Query oQuery = JPA.em().createNativeQuery(query)
            .setParameter("PN_DATE", structureDate)
            .setParameter("PN_SEARCH", search)
            .setParameter("PN_START", start)
            .setParameter("PN_END", end);
        
        return oQuery.getResultList();
    }
    
    /**
     * Metodo para la paginacion de los empleados vacantes
     * @param structureDate
     * @param search
     * @return 
     */
    public static long countVacancies(String structureDate, String search) {
        String query = "SELECT\n" +                        
                        "        COUNT(1)\n" +
                        "    FROM (\n" +
                        "        SELECT FN_EMPLOYEE_NUMBER\n" +
                        "        FROM   PUL_EMPLOYEE_CLIMA\n" +
                        "        WHERE  TO_CHAR(FD_STRUCTURE_DATE,'MM/YYYY') = :PN_DATE\n" +
                        "        AND    FN_EMPLOYEE_NUMBER > 9000000000\n" +
                        "    ) Duplicates\n" +
                        "    LEFT JOIN (\n" +
                        "        SELECT\n" +
                        "            FN_EMPLOYEE_NUMBER, FC_FIRST_NAME || ' ' || FC_LAST_NAME FC_EMPLOYEE_NAME,\n" +
                        "            FC_DESC_LOCATION, FC_TITLE, FN_ID_EMPLOYEE_CLIMA, FN_USERID, FN_MANAGER,\n" +
                        "            FD_STRUCTURE_DATE, FC_EMAIL\n" +
                        "        FROM\n" +
                        "            PORTALUNICO.PUL_EMPLOYEE_CLIMA\n" +
                        "        WHERE\n" +
                        "            TO_CHAR(FD_STRUCTURE_DATE,'MM/YYYY') = :PN_DATE\n" +                        
                        "    ) EmployeeClima ON EmployeeClima.FN_EMPLOYEE_NUMBER = Duplicates.FN_EMPLOYEE_NUMBER\n" +
                        "    LEFT JOIN PORTALUNICO.PUL_EMPLOYEE_CLIMA ManagerEC ON ManagerEC.FN_USERID = EmployeeClima.FN_MANAGER\n" +
                        "    AND TO_CHAR(ManagerEC.FD_STRUCTURE_DATE,'MM/YYYY') = TO_CHAR(EmployeeClima.FD_STRUCTURE_DATE,'MM/YYYY')\n" +
                        "    WHERE LOWER(FC_EMPLOYEE_NAME) LIKE '%' || LOWER(:PN_SEARCH) || '%'\n";
        
        Query oQuery = JPA.em().createNativeQuery(query)
            .setParameter("PN_DATE", structureDate)
            .setParameter("PN_SEARCH", search);
        
        List result = oQuery.getResultList();
        return Long.parseLong(result.get(0).toString());
    }
}
