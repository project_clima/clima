package querys;

import java.sql.CallableStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import play.*;
import play.db.jpa.JPA;
import java.util.*;
import javax.persistence.Query;
import json.entities.ResultMatrixList;
import json.entities.ResumeTable;
import models.Survey;
import oracle.jdbc.OracleCallableStatement;
import oracle.jdbc.OracleConnection;
import oracle.jdbc.OracleTypes;
import org.apache.commons.lang3.StringUtils;
import static utils.Queries.getOracleConnection;
import utils.PagingResult;
import static utils.Queries.getOracleConnection;

public class MatrixQueryDirect {

    public static List<ResultMatrixList> getSubfactors(String userId, String structureDate) throws SQLException {
        String where = userId != null && !"".equals(userId) ? "ec.FN_USERID IN (" + userId + ")" : "1 = 0";
        
        String query = "SELECT  FC_DESC_FACTOR, FC_DESC_SUBFACTOR, FC_TITLE, ROUND(avg(tbl.score), 2) FN_SINGLE_SCORE\n" +
                        "FROM \n" +
                        "    (SELECT f.FC_DESC_FACTOR, sf.FC_DESC_SUBFACTOR,             \n" +
                        "            case when ec.fc_desc_location = 'Servicios Liverpool' then\n" +
                        "                ec.fc_title\n" +
                        "            else\n" +
                        "                ec.FC_TITLE||' - '||ec.fc_desc_location \n" +
                        "            end fc_title,  \n" +
                        "            ec.FN_USERID, avg(ss.FN_DIRECT_SCORE) score\n" +
                        "     FROM      PUL_EMPLOYEE_CLIMA  ec       \n" +
                        "     LEFT JOIN PUL_SUBFACTOR_SCORE ss ON ec.FN_ID_EMPLOYEE_CLIMA = ss.FN_ID_EMPLOYEE_CLIMA\n" +
                        "     LEFT JOIN PUL_SUBFACTOR sf ON sf.FC_DESC_SUBFACTOR = ss.FC_DESC_SUBFACTOR\n" +
                        "     LEFT JOIN PUL_FACTOR     f ON f.FN_ID_FACTOR = sf.FN_ID_FACTOR       \n" +
                        "     WHERE "+ where +"\n" +
                        "     AND   ss.FN_DIRECT_SCORE IS NOT NULL \n" +
                        "     AND   ss.FN_DIRECT_SCORE <> 0 \n" +
                        "     AND   TO_CHAR(ec.FD_STRUCTURE_DATE, 'MM/YYYY') = '"+ structureDate +"'\n" +
                        "     AND   f.FN_ID_SURVEY in (select s.FN_ID_SURVEY from pul_survey s where TO_CHAR(s.FD_SURVEY_DATE, 'MM/YYYY')= '"+ structureDate +"') \n" +
                        "     GROUP BY f.FC_DESC_FACTOR, sf.FC_DESC_SUBFACTOR, ec.fc_desc_location,ec.FC_TITLE, ec.FN_USERID     \n" +
                        "    ) tbl\n" +
                        "WHERE PORTALUNICO.FN_GET_TODO_SURVEY('"+ structureDate +"', tbl.FN_USERID) > 0 \n" +
                        "GROUP BY FC_DESC_FACTOR, FC_DESC_SUBFACTOR, FC_TITLE";

        OracleConnection connection = getOracleConnection();

        CallableStatement statement = connection.prepareCall(query);
        statement.execute();

        ResultSet results = ((OracleCallableStatement) statement).getResultSet();

        List<ResultMatrixList> dataList = new ArrayList();

        while (results.next()) {
            ResultMatrixList matrix = new ResultMatrixList();

            matrix.setFactor(results.getString("FC_DESC_FACTOR"));
            matrix.setSubFactor(results.getString("FC_DESC_SUBFACTOR"));
            matrix.setScore(results.getFloat("FN_SINGLE_SCORE"));
            matrix.setDepartment(results.getString("FC_TITLE"));
            dataList.add(matrix);
        }
        results.close();
        statement.close();
        return dataList;
    }

    public static List<ResultMatrixList> getFactors(String userId, String structureDate) throws SQLException {
        String where = userId != null && !"".equals(userId) ? "ec.FN_USERID IN (" + userId + ")" : "1 = 0";
        
        String query = "SELECT  FC_DESC_FACTOR, \n" +
                        "        FC_TITLE,\n" +
                        "        ROUND(AVG(tbl.score), 2) FN_SINGLE_SCORE\n" +
                        "FROM \n" +
                        "    ( \n" +
                        "     SELECT f.FC_DESC_FACTOR, sf.FN_ID_SUBFACTOR, sf.FN_POINTS,  \n" +
                        "            case when ec.fc_desc_location = 'Servicios Liverpool' then\n" +
                        "                ec.fc_title\n" +
                        "            else\n" +
                        "                ec.FC_TITLE||' - '||ec.fc_desc_location \n" +
                        "            end fc_title, \n" +
                        "            ec.FN_USERID, avg(ss.FN_DIRECT_SCORE)  score\n" +
                        "     FROM  PUL_EMPLOYEE_CLIMA  ec       \n" +
                        "     LEFT JOIN  PUL_SUBFACTOR_SCORE ss ON ec.FN_ID_EMPLOYEE_CLIMA = ss.FN_ID_EMPLOYEE_CLIMA           \n" +
                        "     LEFT JOIN PUL_SUBFACTOR sf ON sf.FC_DESC_SUBFACTOR = ss.FC_DESC_SUBFACTOR \n" +
                        "     LEFT JOIN PUL_FACTOR     f ON f.FN_ID_FACTOR = sf.FN_ID_FACTOR        \n" +
                        "     WHERE "+ where +"\n" +
                        "     AND   ss.FN_DIRECT_SCORE IS NOT NULL \n" +
                        "     AND   TO_CHAR(ec.FD_STRUCTURE_DATE, 'MM/YYYY') = '"+ structureDate +"'\n" +
                        "     AND   f.FN_ID_SURVEY in (select s.FN_ID_SURVEY from pul_survey s where TO_CHAR(s.FD_SURVEY_DATE, 'MM/YYYY')= '"+ structureDate +"') \n" +
                        "     AND   ss.FN_DIRECT_SCORE <> 0 \n" +
                        "     GROUP BY f.FC_DESC_FACTOR, sf.FN_ID_SUBFACTOR, sf.FN_POINTS, ec.fc_desc_location,ec.FC_TITLE, ec.FN_USERID \n" +
                        "    ) tbl\n" +
                        "WHERE PORTALUNICO.FN_GET_TODO_SURVEY('"+ structureDate +"', tbl.FN_USERID) > 0 AND FC_DESC_FACTOR IS NOT NULL\n" +
                        "GROUP BY FC_DESC_FACTOR, FC_TITLE";

        OracleConnection connection = getOracleConnection();
   
        CallableStatement statement = connection.prepareCall(query);
        statement.execute();

        ResultSet results = ((OracleCallableStatement) statement).getResultSet();

        List<ResultMatrixList> dataList = new ArrayList();

        while (results.next()) {
            ResultMatrixList matrix = new ResultMatrixList();

            matrix.setDepartment(results.getString("FC_TITLE"));
            matrix.setFactor(results.getString("FC_DESC_FACTOR"));
            matrix.setScore(results.getFloat("FN_SINGLE_SCORE"));
            dataList.add(matrix);
        }
        results.close();
        statement.close();
        return dataList;
    }

    public static List getQuestions(String userId, String structureDate) throws SQLException {
        String andWhere = userId != null && !"".equals(userId) ? "ec.FN_USERID IN (" + userId + ")" : "1 = 0";
        
        String query = 
                        "SELECT  distinct qas.FC_DESC_SUBFACTOR, qas.FC_QUESTION,\n" +
                        "        case when ec.fc_desc_location = 'Servicios Liverpool' then\n" +
                        "            ec.fc_title\n" +
                        "        else\n" +
                        "            ec.FC_TITLE||' - '||ec.fc_desc_location \n" +
                        "        end fc_title,  \n" +
                        "       round(qas.FN_DIRECT_SCORE_PERCENT, 4) SCORE \n" +
                        "FROM   PORTALUNICO.PUL_QUESTION_ANSWER_SCORE qas\n" +
                        "JOIN   PORTALUNICO.PUL_EMPLOYEE_CLIMA ec ON ec.FN_ID_EMPLOYEE_CLIMA = qas.FN_ID_EMPLOYEE_CLIMA\n" +
                        "JOIN   PORTALUNICO.PUL_SUBFACTOR sf ON sf.FC_DESC_SUBFACTOR = qas.FC_DESC_SUBFACTOR \n" +
                        "JOIN   PORTALUNICO.PUL_FACTOR f ON f.FN_ID_FACTOR = sf.FN_ID_FACTOR\n" +
                        "WHERE  TO_CHAR(qas.FD_STRUCTURE_DATE, 'MM/YYYY') = '" + structureDate + "'\n" +
                        "AND    TO_CHAR(ec.FD_STRUCTURE_DATE, 'MM/YYYY') = '" + structureDate + "'\n" +
                        "AND    f.FN_ID_SURVEY in (select s.FN_ID_SURVEY from pul_survey s where TO_CHAR(s.FD_SURVEY_DATE, 'MM/YYYY')= '" + structureDate + "')\n" +
                        "AND " + andWhere;
        OracleConnection connection = getOracleConnection();
        CallableStatement statement = connection.prepareCall(query);
        statement.execute();

        ResultSet results = ((OracleCallableStatement) statement).getResultSet();

        List<ResultMatrixList> dataList = new ArrayList();

        while (results.next()) {
            ResultMatrixList matrix = new ResultMatrixList();
            matrix.setQuestion(results.getString("FC_QUESTION"));
            matrix.setSubFactor(results.getString("FC_DESC_SUBFACTOR"));
            matrix.setScore(results.getFloat("score"));
            matrix.setDepartment(results.getString("FC_TITLE"));
            dataList.add(matrix);
        }
        results.close();
        statement.close();
        return dataList;

   }
}
