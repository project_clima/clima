package querys;

import java.sql.CallableStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import play.*;
import play.db.jpa.JPA;
import javax.persistence.Query;
import java.util.*;
import models.Goal;
import oracle.jdbc.OracleCallableStatement;
import oracle.jdbc.OracleConnection;
import oracle.jdbc.OracleTypes;
import static utils.Queries.getOracleConnection;
import utils.PagingResult;
import static utils.Queries.getOracleConnection;

/**
 * Clase de apoyo para el manejo del plan de accion a nivel base
 * @author Rodolfo Miranda -- Qualtop
 */
public class ActionPlanQuery {
    /**
     * 
     * @param userId
     * @param date
     * @param notCaptured
     * @return 
     */
    public static List openToCapture(String userId, String date, Boolean notCaptured) {
        String year = date.split("/")[1];
        
        Goal goal = Goal.find(
            "year = ?1 and lower(description) like '%satisfactorio%'",
            Integer.valueOf(year)
        ).first();

        Float plan = goal != null ? goal.maxValue : 86;
        
        String query = "SELECT distinct \n" +
                        "    FC_QUESTION, FC_DESC_FACTOR,\n" +
                        "    FC_DESC_SUBFACTOR,\n" +
                        "    SCORE,\n" +
                        "    tabla.FC_COMMENTS,\n" +
                        "    tabla.FC_HISTORIC,\n" +
                        "    tabla.FC_RESPONSIBLES,\n" +
                        "    tabla.FC_STATUS_PLAN,\n" +
                        "    tabla.FD_COMMIT_DATE,\n" +
                        "    tabla.FN_ID_EMPLOYEE_CLIMA,\n" +
                        "    tabla.FN_ID_PLAN_ACCION,\n" +
                        "    tabla.FC_CATEGORY_PLAN,\n" +
                        "    tabla.FC_APPROVAL_HISTORY, \n" +
                        "    tabla.FD_CRE_DATE\n" +
                        "FROM (\n" +
                        "      SELECT distinct  qas.FC_QUESTION,\n" +
                        "              f.FC_DESC_FACTOR,\n" +
                        "              sf.FC_DESC_SUBFACTOR,\n" +
                        "              avg(qas.FN_TREE_SCORE_PERCENT) SCORE,\n" +
                        "              pa.FC_COMMENTS,\n" +
                        "              pa.FC_HISTORIC,\n" +
                        "              pa.FC_RESPONSIBLES,\n" +
                        "              pa.FC_STATUS_PLAN,\n" +
                        "              pa.FD_COMMIT_DATE,\n" +
                        "              pa.FN_ID_EMPLOYEE_CLIMA,\n" +
                        "              pa.FN_ID_PLAN_ACCION,\n" +
                        "              pa.FC_CATEGORY_PLAN,\n" +
                        "              pa.FC_APPROVAL_HISTORY,\n" +
                        "              pa.FD_CRE_DATE\n" +
                        "      FROM   PORTALUNICO.PUL_QUESTION_ANSWER_SCORE qas\n" +
                        "      JOIN   PORTALUNICO.PUL_SUBFACTOR sf ON sf.FC_DESC_SUBFACTOR = qas.FC_DESC_SUBFACTOR \n" +
                        "      JOIN   PORTALUNICO.PUL_FACTOR f ON f.FN_ID_FACTOR = sf.FN_ID_FACTOR\n" +
                        "      LEFT   JOIN PUL_PLAN_ACCION pa ON qas.FC_QUESTION = pa.FC_QUESTION AND pa.FN_ID_EMPLOYEE_CLIMA = :PN_ID_EMPLOYEE_CLIMA\n" +
                        "      WHERE  TO_CHAR(qas.FD_STRUCTURE_DATE, 'MM/YYYY') = :PN_STRUCTURE_DATE\n" +
                        "      AND (qas.FN_USERID IN (SELECT  distinct FN_MANAGER\n" +
                        "                             FROM PUL_EMPLOYEE_CLIMA ec01\n" +
                        "                             JOIN PUL_SURVEY_EMPLOYEE se01 on se01.FN_ID_EMPLOYEE_CLIMA = ec01.FN_ID_EMPLOYEE_CLIMA \n" +
                        "                             JOIN PUL_SURVEY s01  ON s01.FN_ID_SURVEY = se01.FN_ID_SURVEY\n" +
                        "                             WHERE FN_MANAGER = (SELECT FN_USERID FROM PUL_EMPLOYEE_CLIMA WHERE FN_ID_EMPLOYEE_CLIMA = :PN_ID_EMPLOYEE_CLIMA)\n" +
                        "                             AND FN_ID_SURVEY_TYPE <> 1 \n" +
                        "                             ) \n" +
                        "          OR qas.FN_USERID IN (SELECT  FN_USERID\n" +
                        "                              FROM PUL_EMPLOYEE_CLIMA ec01\n" +
                        "                              JOIN PUL_SURVEY_EMPLOYEE se01 on se01.FN_ID_EMPLOYEE_CLIMA = ec01.FN_ID_EMPLOYEE_CLIMA \n" +
                        "                              JOIN PUL_SURVEY s01  ON s01.FN_ID_SURVEY = se01.FN_ID_SURVEY\n" +
                        "                              WHERE FN_USERID = (SELECT FN_USERID FROM PUL_EMPLOYEE_CLIMA WHERE FN_ID_EMPLOYEE_CLIMA = :PN_ID_EMPLOYEE_CLIMA)\n" +
                        "                              AND FN_ID_SURVEY_TYPE = 1 \n" +
                        "                          ))     \n" +
                        "      AND    qas.FN_TREE_SCORE_PERCENT IS NOT NULL      \n" +
                        "      GROUP BY qas.FC_QUESTION, f.FC_DESC_FACTOR, sf.FC_DESC_SUBFACTOR,                    \n" +
                        "              pa.FC_COMMENTS, pa.FC_HISTORIC, pa.FC_RESPONSIBLES,\n" +
                        "              pa.FC_STATUS_PLAN, pa.FD_COMMIT_DATE,\n" +
                        "              pa.FN_ID_EMPLOYEE_CLIMA, pa.FN_ID_PLAN_ACCION,\n" +
                        "              pa.FC_CATEGORY_PLAN, pa.FC_APPROVAL_HISTORY, pa.FD_CRE_DATE      \n" +
                        "      ORDER BY SCORE, FC_QUESTION asc      \n" +
                        "     ) tabla\n" +
                        "WHERE tabla.score < :PN_SCORE \n" +
                        "AND rownum BETWEEN 1 AND 10 \n" +
                        "ORDER BY score ASC, FC_QUESTION asc";

    	Query oQuery = JPA.em().createNativeQuery(query)
            .setParameter("PN_ID_EMPLOYEE_CLIMA", userId)
            .setParameter("PN_STRUCTURE_DATE", date)
            .setParameter("PN_SCORE", plan);
        
        return oQuery.getResultList();
    }

    /**
     * MEtodo para el manejo de la tabla principal del plan de accion
     * @param userId
     * @param date
     * @param statusPlan
     * @param statusReview
     * @param statusSelector
     * @param start
     * @param end
     * @param search
     * @return 
     */
    public static List openMainTable(String userId, String date, String statusPlan,
                                     String statusReview, String statusSelector,
                                     int start, int end, String search) {
        String year = date.split("/")[1];
        
        Goal goal = Goal.find(
            "year = ?1 and lower(description) like '%satisfactorio%'",
            Integer.valueOf(year)
        ).first();
        
        Float plan   = goal != null ? goal.maxValue : 86;
        String reviewCondition = statusReview == null || statusReview.isEmpty() ?
                                    "      AND   tablaGeneral.ESTATUS_REVIEW LIKE '%' || :PN_STATUS_REVIEW || '%'\n" :
                                    "      AND   tablaGeneral.ESTATUS_REVIEW LIKE :PN_STATUS_REVIEW\n";
        
        String paginationCondition = end > -1 ? "WHERE row_number BETWEEN :START AND :END" : "";
        
        String query = "SELECT * \n" +
                        "FROM\n" +
                        "(\n" +
                        "SELECT\n" +
                        "    FULL_NAME,\n" +
                        "    FN_EMPLOYEE_NUMBER,\n" +
                        "    FC_TITLE,\n" +
                        "    FN_ID_EMPLOYEE_CLIMA,\n" +
                        "    CALIFICACION,\n" +
                        "    ESTATUS_PLAN,\n" +
                        "    ESTATUS_REVIEW,\n" +
                        "    ESTATUS_TRACING,\n" +
                        "    LIDERAZGO,\n" +
                        "    ORGANIZACION,\n" +
                        "    DESARROLLO,\n" +
                        "    CLIENTES,\n" +
                        "    TEQUIPO,\n" +
                        "    FN_ANSWERED,\n" +
                        "    FN_PLAN,\n" +
                        "    ROW_NUMBER() OVER (ORDER BY NULL) row_number\n" +
                        "FROM (SELECT FULL_NAME,\n" +
                        "             FN_EMPLOYEE_NUMBER,\n" +
                        "             DECODE (FC_DIVISION,'CORP', FC_TITLE, FC_TITLE || ' - ' || FC_DIVISION) FC_TITLE, \n" +
                        "             TABLEQ.FN_ID_EMPLOYEE_CLIMA,\n" +
                        "             TABLEQ.ESTATUS_PLAN,\n" +
                        "             TABLEQ.ESTATUS_REVIEW,\n" +
                        "             TABLEQ.ESTATUS_TRACING,\n" +
                        "             TABLEQ.FN_USERID,\n" +
                        "             (CASE WHEN LOWER(TABLEQ.FC_TITLE) LIKE '%director%' OR LOWER(TABLEQ.FC_TITLE) LIKE '%gerente%' THEN\n" +
                        "                nvl((SELECT ROUND(AVG(FN_SINGLE_SCORE), 2) \n" +
                        "                    FROM (SELECT  FN_ID_SUBFACTOR, \n" +
                        "                                   FC_TITLE,\n" +
                        "                                   AVG(tbl1.score) FN_SINGLE_SCORE\n" +
                        "                           FROM \n" +
                        "                               (SELECT sf2.FN_ID_SUBFACTOR,   \n" +
                        "                                       decode(ec2.fc_division,'CORP',ec2.FC_TITLE, ec2.FC_TITLE||' - '||ec2.FC_DIVISION) FC_TITLE, \n" +
                        "                                       ec2.FN_USERID, avg(ss2.FN_DIRECT_SCORE)  score\n" +
                        "                                FROM  PUL_EMPLOYEE_CLIMA  ec2       \n" +
                        "                                LEFT JOIN  PUL_SUBFACTOR_SCORE ss2 ON ec2.FN_ID_EMPLOYEE_CLIMA = ss2.FN_ID_EMPLOYEE_CLIMA           \n" +
                        "                                LEFT JOIN PUL_SUBFACTOR sf2 ON sf2.FC_DESC_SUBFACTOR = ss2.FC_DESC_SUBFACTOR         \n" +
                        "                                WHERE ec2.FN_USERID IN (TABLEQ.FN_USERID)\n" +
                        "                                AND   ss2.FN_DIRECT_SCORE IS NOT NULL \n" +
                        "                                AND   TO_CHAR(ec2.FD_STRUCTURE_DATE, 'MM/YYYY') = :PN_DATE\n" +
                        "                                AND   ss2.FN_DIRECT_SCORE <> 0 \n" +
                        "                                GROUP BY sf2.FN_ID_SUBFACTOR, decode(ec2.fc_division,'CORP',ec2.FC_TITLE, ec2.FC_TITLE||' - '||ec2.FC_DIVISION), ec2.FN_USERID              \n" +
                        "                               ) tbl1\n" +
                        "                           WHERE FN_ID_SUBFACTOR IS NOT NULL\n" +
                        "                           GROUP BY FN_ID_SUBFACTOR, FC_TITLE)\n" +
                        "                    ), 0)    \n" +
                        "             ELSE\n" +
                        "                nvl((SELECT ROUND(AVG(FN_SINGLE_SCORE), 2) \n" +
                        "                    FROM (SELECT  FN_ID_SUBFACTOR, \n" +
                        "                                   FC_TITLE,\n" +
                        "                                   AVG(tbl.score) FN_SINGLE_SCORE\n" +
                        "                           FROM (SELECT sf2.FN_ID_SUBFACTOR,   \n" +
                        "                                       decode(ec2.fc_division,'CORP',ec2.FC_TITLE, ec2.FC_TITLE||' - '||ec2.FC_DIVISION) FC_TITLE, \n" +
                        "                                       ec2.FN_USERID, avg(ss2.FN_TREE_SCORE)  score\n" +
                        "                                FROM  PUL_EMPLOYEE_CLIMA  ec2       \n" +
                        "                                LEFT JOIN  PUL_SUBFACTOR_SCORE ss2 ON ec2.FN_ID_EMPLOYEE_CLIMA = ss2.FN_ID_EMPLOYEE_CLIMA           \n" +
                        "                                LEFT JOIN PUL_SUBFACTOR sf2 ON sf2.FC_DESC_SUBFACTOR = ss2.FC_DESC_SUBFACTOR         \n" +
                        "                                WHERE ec2.FN_USERID IN (TABLEQ.FN_USERID)\n" +
                        "                                AND   ss2.FN_TREE_SCORE IS NOT NULL \n" +
                        "                                AND   TO_CHAR(ec2.FD_STRUCTURE_DATE, 'MM/YYYY') = :PN_DATE\n" +
                        "                                AND   ss2.FN_TREE_SCORE <> 0 \n" +
                        "                                GROUP BY sf2.FN_ID_SUBFACTOR, decode(ec2.fc_division,'CORP',ec2.FC_TITLE, ec2.FC_TITLE||' - '||ec2.FC_DIVISION), ec2.FN_USERID              \n" +
                        "                               ) tbl\n" +
                        "                           WHERE FN_ID_SUBFACTOR IS NOT NULL\n" +
                        "                           GROUP BY FN_ID_SUBFACTOR, FC_TITLE)\n" +
                        "                    ), 0)\n" +
                        "             END)   CALIFICACION,\n" +
                        "             nvl((SELECT ROUND(AVG(FN_SINGLE_SCORE), 2) \n" +
                        "                  FROM (SELECT  FC_DESC_FACTOR, \n" +
                        "                                FC_TITLE,\n" +
                        "                                AVG(tbl.score) FN_SINGLE_SCORE\n" +
                        "                        FROM \n" +
                        "                            (SELECT f_3.FC_DESC_FACTOR,  \n" +
                        "                                    decode(ec_3.fc_division,'CORP',ec_3.FC_TITLE, ec_3.FC_TITLE||' - '||ec_3.FC_DIVISION) FC_TITLE, \n" +
                        "                                    ec_3.FN_USERID, avg(ss_3.FN_TREE_SCORE)  score\n" +
                        "                             FROM  PUL_EMPLOYEE_CLIMA  ec_3       \n" +
                        "                             LEFT JOIN  PUL_SUBFACTOR_SCORE ss_3 ON ec_3.FN_ID_EMPLOYEE_CLIMA = ss_3.FN_ID_EMPLOYEE_CLIMA           \n" +
                        "                             LEFT JOIN PUL_SUBFACTOR sf_3 ON sf_3.FC_DESC_SUBFACTOR = ss_3.FC_DESC_SUBFACTOR \n" +
                        "                             LEFT JOIN PUL_FACTOR     f_3 ON f_3.FN_ID_FACTOR = sf_3.FN_ID_FACTOR        \n" +
                        "                             WHERE ec_3.FN_USERID IN (TABLEQ.FN_USERID)\n" +
                        "                             AND   ss_3.FN_TREE_SCORE IS NOT NULL \n" +
                        "                             AND   f_3.FC_DESC_FACTOR LIKE 'Liderazgo'\n" +
                        "                             AND   TO_CHAR(ec_3.FD_STRUCTURE_DATE, 'MM/YYYY') = :PN_DATE\n" +
                        "                             AND   ss_3.FN_TREE_SCORE <> 0 \n" +
                        "                             GROUP BY f_3.FC_DESC_FACTOR, decode(ec_3.fc_division,'CORP',ec_3.FC_TITLE, ec_3.FC_TITLE||' - '||ec_3.FC_DIVISION), ec_3.FN_USERID              \n" +
                        "                             ) tbl\n" +
                        "                        WHERE FC_DESC_FACTOR IS NOT NULL\n" +
                        "                        GROUP BY FC_DESC_FACTOR, FC_TITLE)\n" +
                        "                 ), 0) LIDERAZGO,\n" +
                        "             nvl((SELECT ROUND(AVG(FN_SINGLE_SCORE), 2) \n" +
                        "                  FROM (SELECT  FC_DESC_FACTOR, \n" +
                        "                                FC_TITLE,\n" +
                        "                                AVG(tbl.score) FN_SINGLE_SCORE\n" +
                        "                        FROM \n" +
                        "                            (SELECT f_3.FC_DESC_FACTOR,  \n" +
                        "                                    decode(ec_3.fc_division,'CORP',ec_3.FC_TITLE, ec_3.FC_TITLE||' - '||ec_3.FC_DIVISION) FC_TITLE, \n" +
                        "                                    ec_3.FN_USERID, avg(ss_3.FN_TREE_SCORE)  score\n" +
                        "                             FROM  PUL_EMPLOYEE_CLIMA  ec_3       \n" +
                        "                             LEFT JOIN  PUL_SUBFACTOR_SCORE ss_3 ON ec_3.FN_ID_EMPLOYEE_CLIMA = ss_3.FN_ID_EMPLOYEE_CLIMA           \n" +
                        "                             LEFT JOIN PUL_SUBFACTOR sf_3 ON sf_3.FC_DESC_SUBFACTOR = ss_3.FC_DESC_SUBFACTOR \n" +
                        "                             LEFT JOIN PUL_FACTOR     f_3 ON f_3.FN_ID_FACTOR = sf_3.FN_ID_FACTOR        \n" +
                        "                             WHERE ec_3.FN_USERID IN (TABLEQ.FN_USERID)\n" +
                        "                             AND   ss_3.FN_TREE_SCORE IS NOT NULL \n" +
                        "                             AND   f_3.FC_DESC_FACTOR LIKE 'Organización'\n" +
                        "                             AND   TO_CHAR(ec_3.FD_STRUCTURE_DATE, 'MM/YYYY') = :PN_DATE\n" +
                        "                             AND   ss_3.FN_TREE_SCORE <> 0 \n" +
                        "                             GROUP BY f_3.FC_DESC_FACTOR, decode(ec_3.fc_division,'CORP',ec_3.FC_TITLE, ec_3.FC_TITLE||' - '||ec_3.FC_DIVISION), ec_3.FN_USERID              \n" +
                        "                             ) tbl\n" +
                        "                        WHERE FC_DESC_FACTOR IS NOT NULL\n" +
                        "                        GROUP BY FC_DESC_FACTOR, FC_TITLE)\n" +
                        "                 ), 0)  ORGANIZACION,\n" +
                        "             nvl((SELECT ROUND(AVG(FN_SINGLE_SCORE), 2) \n" +
                        "                  FROM (SELECT  FC_DESC_FACTOR, \n" +
                        "                                FC_TITLE,\n" +
                        "                                AVG(tbl.score) FN_SINGLE_SCORE\n" +
                        "                        FROM \n" +
                        "                            (SELECT f_3.FC_DESC_FACTOR,  \n" +
                        "                                    decode(ec_3.fc_division,'CORP',ec_3.FC_TITLE, ec_3.FC_TITLE||' - '||ec_3.FC_DIVISION) FC_TITLE, \n" +
                        "                                    ec_3.FN_USERID, avg(ss_3.FN_TREE_SCORE)  score\n" +
                        "                             FROM  PUL_EMPLOYEE_CLIMA  ec_3       \n" +
                        "                             LEFT JOIN  PUL_SUBFACTOR_SCORE ss_3 ON ec_3.FN_ID_EMPLOYEE_CLIMA = ss_3.FN_ID_EMPLOYEE_CLIMA           \n" +
                        "                             LEFT JOIN PUL_SUBFACTOR sf_3 ON sf_3.FC_DESC_SUBFACTOR = ss_3.FC_DESC_SUBFACTOR \n" +
                        "                             LEFT JOIN PUL_FACTOR     f_3 ON f_3.FN_ID_FACTOR = sf_3.FN_ID_FACTOR        \n" +
                        "                             WHERE ec_3.FN_USERID IN (TABLEQ.FN_USERID)\n" +
                        "                             AND   ss_3.FN_TREE_SCORE IS NOT NULL \n" +
                        "                             AND   f_3.FC_DESC_FACTOR LIKE 'Desarrollo y crecimiento'\n" +
                        "                             AND   TO_CHAR(ec_3.FD_STRUCTURE_DATE, 'MM/YYYY') = :PN_DATE\n" +
                        "                             AND   ss_3.FN_TREE_SCORE <> 0 \n" +
                        "                             GROUP BY f_3.FC_DESC_FACTOR, decode(ec_3.fc_division,'CORP',ec_3.FC_TITLE, ec_3.FC_TITLE||' - '||ec_3.FC_DIVISION), ec_3.FN_USERID              \n" +
                        "                             ) tbl\n" +
                        "                        WHERE FC_DESC_FACTOR IS NOT NULL\n" +
                        "                        GROUP BY FC_DESC_FACTOR, FC_TITLE)\n" +
                        "                 ), 0) DESARROLLO,\n" +
                        "             nvl((SELECT ROUND(AVG(FN_SINGLE_SCORE), 2) \n" +
                        "                  FROM (SELECT  FC_DESC_FACTOR, \n" +
                        "                                FC_TITLE,\n" +
                        "                                AVG(tbl.score) FN_SINGLE_SCORE\n" +
                        "                        FROM \n" +
                        "                            (SELECT f_3.FC_DESC_FACTOR,  \n" +
                        "                                    decode(ec_3.fc_division,'CORP',ec_3.FC_TITLE, ec_3.FC_TITLE||' - '||ec_3.FC_DIVISION) FC_TITLE, \n" +
                        "                                    ec_3.FN_USERID, avg(ss_3.FN_TREE_SCORE)  score\n" +
                        "                             FROM  PUL_EMPLOYEE_CLIMA  ec_3       \n" +
                        "                             LEFT JOIN  PUL_SUBFACTOR_SCORE ss_3 ON ec_3.FN_ID_EMPLOYEE_CLIMA = ss_3.FN_ID_EMPLOYEE_CLIMA           \n" +
                        "                             LEFT JOIN PUL_SUBFACTOR sf_3 ON sf_3.FC_DESC_SUBFACTOR = ss_3.FC_DESC_SUBFACTOR \n" +
                        "                             LEFT JOIN PUL_FACTOR     f_3 ON f_3.FN_ID_FACTOR = sf_3.FN_ID_FACTOR        \n" +
                        "                             WHERE ec_3.FN_USERID IN (TABLEQ.FN_USERID)\n" +
                        "                             AND   ss_3.FN_TREE_SCORE IS NOT NULL \n" +
                        "                             AND   f_3.FC_DESC_FACTOR LIKE 'Clientes'\n" +
                        "                             AND   TO_CHAR(ec_3.FD_STRUCTURE_DATE, 'MM/YYYY') = :PN_DATE\n" +
                        "                             AND   ss_3.FN_TREE_SCORE <> 0 \n" +
                        "                             GROUP BY f_3.FC_DESC_FACTOR, decode(ec_3.fc_division,'CORP',ec_3.FC_TITLE, ec_3.FC_TITLE||' - '||ec_3.FC_DIVISION), ec_3.FN_USERID              \n" +
                        "                             ) tbl\n" +
                        "                        WHERE FC_DESC_FACTOR IS NOT NULL\n" +
                        "                        GROUP BY FC_DESC_FACTOR, FC_TITLE)\n" +
                        "                 ), 0)   CLIENTES,\n" +
                        "             nvl((SELECT ROUND(AVG(FN_SINGLE_SCORE), 2) \n" +
                        "                  FROM (SELECT  FC_DESC_FACTOR, \n" +
                        "                                FC_TITLE,\n" +
                        "                                AVG(tbl.score) FN_SINGLE_SCORE\n" +
                        "                        FROM \n" +
                        "                            (SELECT f_3.FC_DESC_FACTOR,  \n" +
                        "                                    decode(ec_3.fc_division,'CORP',ec_3.FC_TITLE, ec_3.FC_TITLE||' - '||ec_3.FC_DIVISION) FC_TITLE, \n" +
                        "                                    ec_3.FN_USERID, avg(ss_3.FN_TREE_SCORE)  score\n" +
                        "                             FROM  PUL_EMPLOYEE_CLIMA  ec_3       \n" +
                        "                             LEFT JOIN  PUL_SUBFACTOR_SCORE ss_3 ON ec_3.FN_ID_EMPLOYEE_CLIMA = ss_3.FN_ID_EMPLOYEE_CLIMA           \n" +
                        "                             LEFT JOIN PUL_SUBFACTOR sf_3 ON sf_3.FC_DESC_SUBFACTOR = ss_3.FC_DESC_SUBFACTOR \n" +
                        "                             LEFT JOIN PUL_FACTOR     f_3 ON f_3.FN_ID_FACTOR = sf_3.FN_ID_FACTOR        \n" +
                        "                             WHERE ec_3.FN_USERID IN (TABLEQ.FN_USERID)\n" +
                        "                             AND   ss_3.FN_TREE_SCORE IS NOT NULL \n" +
                        "                             AND   f_3.FC_DESC_FACTOR LIKE 'Trabajo en equipo'\n" +
                        "                             AND   TO_CHAR(ec_3.FD_STRUCTURE_DATE, 'MM/YYYY') = :PN_DATE\n" +
                        "                             AND   ss_3.FN_TREE_SCORE <> 0 \n" +
                        "                             GROUP BY f_3.FC_DESC_FACTOR, decode(ec_3.fc_division,'CORP',ec_3.FC_TITLE, ec_3.FC_TITLE||' - '||ec_3.FC_DIVISION), ec_3.FN_USERID              \n" +
                        "                             ) tbl\n" +
                        "                        WHERE FC_DESC_FACTOR IS NOT NULL\n" +
                        "                        GROUP BY FC_DESC_FACTOR, FC_TITLE)\n" +
                        "                 ), 0)  TEQUIPO,\n" +
                        "             CASE WHEN LOWER(TABLEQ.fc_title) like '%director%' OR LOWER(TABLEQ.fc_title) like '%gerente%' THEN\n" +
                        "                PORTALUNICO.FN_GET_DIRECT_SURVEY_RESOLVED(:PN_DATE, TABLEQ.FN_USERID)\n" +
                        "             ELSE\n" +
                        "                PORTALUNICO.FN_GET_SURVEY_RESOLVED (:PN_DATE, TABLEQ.FN_USERID)\n" +
                        "             END FN_ANSWERED,\n" +
                        "             CASE WHEN LOWER(TABLEQ.fc_title) like '%director%' OR LOWER(TABLEQ.fc_title) like '%gerente%' THEN\n" +
                        "                PORTALUNICO.FN_GET_TODO_SURVEY_DIRECT(:PN_DATE, TABLEQ.FN_USERID)\n" +
                        "             ELSE\n" +
                        "                PORTALUNICO.FN_GET_TODO_SURVEY (:PN_DATE, TABLEQ.FN_USERID)\n" +
                        "             END FN_PLAN                        \n" +
                        "      FROM (SELECT FN_ID_EMPLOYEE_CLIMA,\n" +
                        "                   FN_LEVEL,\n" +
                        "                   FC_TITLE,\n" +
                        "                   FC_DIVISION,\n" +
                        "                   FN_USERID,\n" +
                        "                   FC_FIRST_NAME||' '||FC_LAST_NAME FULL_NAME,\n" +
                        "                   FN_EMPLOYEE_NUMBER,\n" +
                        "                   FN_MANAGER,                   \n" +
                        "                   NVL ((SELECT LISTAGG (tal.FC_STATUS_PLAN, '|') WITHIN GROUP (ORDER BY tal.FC_STATUS_PLAN)\n" +
                        "                         FROM (SELECT DISTINCT FC_STATUS_PLAN FROM PORTALUNICO.PUL_PLAN_ACCION PA WHERE PA.FN_ID_EMPLOYEE_CLIMA   = ecTable.FN_ID_EMPLOYEE_CLIMA) tal), 'PENDIENTE') ESTATUS_PLAN,\n" +
                        "                   NVL ((SELECT LISTAGG (tabla2.FC_STATUS_REVIEW, '|') WITHIN GROUP (ORDER BY tabla2.FC_STATUS_REVIEW)\n" +
                        "                         FROM (SELECT DISTINCT FC_STATUS_REVIEW FROM PORTALUNICO.PUL_PLAN_ACCION PA WHERE PA.FN_ID_EMPLOYEE_CLIMA = ecTable.FN_ID_EMPLOYEE_CLIMA) tabla2), 'PENDIENTE') ESTATUS_REVIEW,\n" +
                        "                   NVL ((SELECT LISTAGG (tabla3.FC_STATUS_TRACING, '|') WITHIN GROUP (ORDER BY tabla3.FC_STATUS_TRACING)\n" +
                        "                         FROM (SELECT DISTINCT FC_STATUS_TRACING FROM PORTALUNICO.PUL_PLAN_ACCION PA WHERE PA.FN_ID_EMPLOYEE_CLIMA = ecTable.FN_ID_EMPLOYEE_CLIMA) tabla3), 'PENDIENTE') ESTATUS_TRACING                                                  \n" +
                        "            FROM (select * from PUL_EMPLOYEE_CLIMA where TO_CHAR(FD_STRUCTURE_DATE,'MM/YYYY')= :PN_DATE) ecTable\n" +
                        "            CONNECT BY PRIOR ecTable.FN_USERID = ecTable.FN_MANAGER\n" +
                        "            START WITH ecTable.FN_USERID IN (SELECT FN_USERID \n" +
                        "                                             FROM   PUL_EMPLOYEE_CLIMA  \n" +
                        "                                             WHERE  FN_USERID = (:PN_USERID)  \n" +
                        "                                             AND    to_char(FD_STRUCTURE_DATE,'MM/YYYY') = (:PN_DATE))  \n" +                
                        "            ) TABLEQ    \n" +
                        "      )  tablaGeneral\n" +
                        "      WHERE tablaGeneral.ESTATUS_PLAN LIKE '%' || :PN_STATUS_PLAN || '%'\n" +
                        "      AND   tablaGeneral.ESTATUS_REVIEW LIKE '%' || :PN_STATUS_REVIEW || '%'\n" +
                        "      AND   tablaGeneral.ESTATUS_REVIEW LIKE '%' || :PN_STATUS_REVIEW || '%'\n" +
                        "      AND   TO_CHAR(tablaGeneral.FN_EMPLOYEE_NUMBER) LIKE '%' || :PN_EMPLOYEE_NUMBER || '%' \n" +
                        "      AND (tablaGeneral.CALIFICACION < :PN_SCORE  OR tablaGeneral.LIDERAZGO < :PN_SCORE OR  tablaGeneral.ORGANIZACION < :PN_SCORE OR tablaGeneral.DESARROLLO < :PN_SCORE OR tablaGeneral.CLIENTES < :PN_SCORE OR  tablaGeneral.TEQUIPO < :PN_SCORE)\n" +
                        "      and   tablaGeneral.FN_ANSWERED > 2\n" +
                        "      and   tablaGeneral.FN_PLAN > 2\n" +
                        ") tblPaginated\n" +
                        paginationCondition;

     	Query oQuery = JPA.em().createNativeQuery(query)
            .setParameter("PN_USERID", userId)
            .setParameter("PN_DATE", date)
            .setParameter("PN_SCORE", plan)
            .setParameter("PN_STATUS_PLAN", statusPlan)
            .setParameter("PN_STATUS_REVIEW", statusReview)
            .setParameter("PN_EMPLOYEE_NUMBER", search);
        
        if (end > -1) {
            oQuery.setParameter("START", start);
            oQuery.setParameter("END", end);
        }
        
        return oQuery.getResultList();
    }
    
    /**
     * Query para la caificacion por usuario y fecha de estructura
     * @param date
     * @param userId
     * @return 
     */
    public static Float getUserScore(String date, int userId) {
        Float result = 0f;

        try {
            OracleConnection connection = getOracleConnection();
            CallableStatement statement = connection.prepareCall("{ ? = call PORTALUNICO.FN_GET_FINAL_SCORE(?, ?) }");
            
            statement.registerOutParameter(1, OracleTypes.NUMBER);
            statement.setInt(2, userId);
            statement.setString(3, date);
            statement.execute();

            result = ((OracleCallableStatement) statement).getFloat(1);
            statement.close();
        } catch (SQLException e) {
            Logger.error(e.getMessage());
        }

        return result;
    }
    
    /**
     * Query para obtener los empleados directos por fecha de estructura y
     * id de usuario
     * @param date
     * @param userId
     * @return 
     */
    public static Float getUserScoreDirect(String date, int userId) {
        Float result = 0f;

        try {
            OracleConnection connection = getOracleConnection();
            CallableStatement statement = connection.prepareCall("{ ? = call PORTALUNICO.FN_GET_FINAL_DIRECT_SCORE(?, ?) }");
            
            statement.registerOutParameter(1, OracleTypes.NUMBER);
            statement.setInt(2, userId);
            statement.setString(3, date);
            statement.execute();

            result = ((OracleCallableStatement) statement).getFloat(1);
            statement.close();
        } catch (SQLException e) {
            Logger.error(e.getMessage());
        }

        return result;
    }
    
    /**
     * Query para obtener el total de participantes y calificaciones por plan
     * @param userId
     * @param date
     * @param statusPlan
     * @param statusReview
     * @param search
     * @return 
     */
    public static long getPlanCount(String userId, String date, String statusPlan,
                                    String statusReview, String search) {
        String year = date.split("/")[1];
        
        Goal goal = Goal.find(
            "year = ?1 and lower(description) like '%satisfactorio%'",
            Integer.valueOf(year)
        ).first();
        
        Float plan   = goal != null ? goal.maxValue : 86;
        String reviewCondition = statusReview == null || statusReview.isEmpty() ?
                                    "      AND   tablaGeneral.ESTATUS_REVIEW LIKE '%' || :PN_STATUS_REVIEW || '%'\n" :
                                    "      AND   tablaGeneral.ESTATUS_REVIEW LIKE :PN_STATUS_REVIEW\n";
        
        String statement = "SELECT count(1)\n" +
                            "FROM (SELECT  TABLEQ.FN_ID_EMPLOYEE_CLIMA,\n" +
                            "              TABLEQ.FN_EMPLOYEE_NUMBER,\n" +
                            "              TABLEQ.FC_TITLE,\n" +
                            "              TABLEQ.FN_USERID,\n" +
                            "              TABLEQ.ESTATUS_PLAN,\n" +
                            "              TABLEQ.ESTATUS_REVIEW,\n" +
                            "              (CASE WHEN lower(TABLEQ.FC_TITLE) LIKE '%director%' OR lower(TABLEQ.FC_TITLE) LIKE '%gerente%' THEN\n" +
                            "               (SELECT ROUND(AVG(FN_SINGLE_SCORE), 2) \n" +
                            "                 FROM (SELECT  FN_ID_SUBFACTOR, \n" +
                            "                               FC_TITLE,\n" +
                            "                               AVG(tbl1.score) FN_SINGLE_SCORE\n" +
                            "                       FROM \n" +
                            "                           (SELECT sf2.FN_ID_SUBFACTOR,   \n" +
                            "                                   decode(ec2.fc_division,'CORP',ec2.FC_TITLE, ec2.FC_TITLE||' - '||ec2.FC_DIVISION) FC_TITLE, \n" +
                            "                                   ec2.FN_USERID, avg(ss2.FN_DIRECT_SCORE)  score\n" +
                            "                            FROM  PUL_EMPLOYEE_CLIMA  ec2       \n" +
                            "                            LEFT JOIN  PUL_SUBFACTOR_SCORE ss2 ON ec2.FN_ID_EMPLOYEE_CLIMA = ss2.FN_ID_EMPLOYEE_CLIMA           \n" +
                            "                            LEFT JOIN PUL_SUBFACTOR sf2 ON sf2.FC_DESC_SUBFACTOR = ss2.FC_DESC_SUBFACTOR         \n" +
                            "                            WHERE ec2.FN_USERID IN (TABLEQ.FN_USERID)\n" +
                            "                            AND   ss2.FN_DIRECT_SCORE IS NOT NULL \n" +
                            "                            AND   TO_CHAR(ec2.FD_STRUCTURE_DATE, 'MM/YYYY') = :PN_DATE\n" +
                            "                            AND   ss2.FN_DIRECT_SCORE <> 0 \n" +
                            "                            GROUP BY sf2.FN_ID_SUBFACTOR, decode(ec2.fc_division,'CORP',ec2.FC_TITLE, ec2.FC_TITLE||' - '||ec2.FC_DIVISION), ec2.FN_USERID              \n" +
                            "                           ) tbl1\n" +
                            "                       WHERE FN_ID_SUBFACTOR IS NOT NULL\n" +
                            "                       GROUP BY FN_ID_SUBFACTOR, FC_TITLE)\n" +
                            "                )      \n" +
                            "             ELSE\n" +
                            "                (SELECT ROUND(AVG(FN_SINGLE_SCORE), 2) \n" +
                            "                 FROM (SELECT  FN_ID_SUBFACTOR, \n" +
                            "                               FC_TITLE,\n" +
                            "                               AVG(tbl.score) FN_SINGLE_SCORE\n" +
                            "                       FROM (SELECT sf2.FN_ID_SUBFACTOR,   \n" +
                            "                                   decode(ec2.fc_division,'CORP',ec2.FC_TITLE, ec2.FC_TITLE||' - '||ec2.FC_DIVISION) FC_TITLE, \n" +
                            "                                   ec2.FN_USERID, avg(ss2.FN_TREE_SCORE)  score\n" +
                            "                            FROM  PUL_EMPLOYEE_CLIMA  ec2       \n" +
                            "                            LEFT JOIN  PUL_SUBFACTOR_SCORE ss2 ON ec2.FN_ID_EMPLOYEE_CLIMA = ss2.FN_ID_EMPLOYEE_CLIMA           \n" +
                            "                            LEFT JOIN PUL_SUBFACTOR sf2 ON sf2.FC_DESC_SUBFACTOR = ss2.FC_DESC_SUBFACTOR         \n" +
                            "                            WHERE ec2.FN_USERID IN (TABLEQ.FN_USERID)\n" +
                            "                            AND   ss2.FN_TREE_SCORE IS NOT NULL \n" +
                            "                            AND   TO_CHAR(ec2.FD_STRUCTURE_DATE, 'MM/YYYY') = :PN_DATE\n" +
                            "                            AND   ss2.FN_TREE_SCORE <> 0 \n" +
                            "                            GROUP BY sf2.FN_ID_SUBFACTOR, decode(ec2.fc_division,'CORP',ec2.FC_TITLE, ec2.FC_TITLE||' - '||ec2.FC_DIVISION), ec2.FN_USERID              \n" +
                            "                           ) tbl\n" +
                            "                       WHERE FN_ID_SUBFACTOR IS NOT NULL\n" +
                            "                       GROUP BY FN_ID_SUBFACTOR, FC_TITLE)\n" +
                            "                )\n" +
                            "             END) CALIFICACION,\n" +
                            "             CASE WHEN LOWER(TABLEQ.fc_title) like '%director%' OR LOWER(TABLEQ.fc_title) like '%gerente%' THEN\n" +
                            "                PORTALUNICO.FN_GET_DIRECT_SURVEY_RESOLVED(:PN_DATE, TABLEQ.FN_USERID)\n" +
                            "             ELSE\n" +
                            "                PORTALUNICO.FN_GET_SURVEY_RESOLVED (:PN_DATE, TABLEQ.FN_USERID)\n" +
                            "             END FN_ANSWERED,             \n" +
                            "             CASE WHEN LOWER(TABLEQ.fc_title) like '%director%' OR LOWER(TABLEQ.fc_title) like '%gerente%' THEN\n" +
                            "                PORTALUNICO.FN_GET_TODO_SURVEY_DIRECT(:PN_DATE, TABLEQ.FN_USERID)\n" +
                            "             ELSE\n" +
                            "                PORTALUNICO.FN_GET_TODO_SURVEY (:PN_DATE, TABLEQ.FN_USERID)\n" +
                            "             END FN_PLAN,                        \n" +
                            "             nvl((SELECT ROUND(AVG(FN_SINGLE_SCORE), 2) \n" +
                            "                  FROM (SELECT  FC_DESC_FACTOR, \n" +
                            "                                FC_TITLE,\n" +
                            "                                AVG(tbl.score) FN_SINGLE_SCORE\n" +
                            "                        FROM \n" +
                            "                            (SELECT f_3.FC_DESC_FACTOR,  \n" +
                            "                                    decode(ec_3.fc_division,'CORP',ec_3.FC_TITLE, ec_3.FC_TITLE||' - '||ec_3.FC_DIVISION) FC_TITLE, \n" +
                            "                                    ec_3.FN_USERID, avg(ss_3.FN_TREE_SCORE)  score\n" +
                            "                             FROM  PUL_EMPLOYEE_CLIMA  ec_3       \n" +
                            "                             LEFT JOIN  PUL_SUBFACTOR_SCORE ss_3 ON ec_3.FN_ID_EMPLOYEE_CLIMA = ss_3.FN_ID_EMPLOYEE_CLIMA           \n" +
                            "                             LEFT JOIN PUL_SUBFACTOR sf_3 ON sf_3.FC_DESC_SUBFACTOR = ss_3.FC_DESC_SUBFACTOR \n" +
                            "                             LEFT JOIN PUL_FACTOR     f_3 ON f_3.FN_ID_FACTOR = sf_3.FN_ID_FACTOR        \n" +
                            "                             WHERE ec_3.FN_USERID IN (TABLEQ.FN_USERID)\n" +
                            "                             AND   ss_3.FN_TREE_SCORE IS NOT NULL \n" +
                            "                             AND   f_3.FC_DESC_FACTOR LIKE 'Liderazgo'\n" +
                            "                             AND   TO_CHAR(ec_3.FD_STRUCTURE_DATE, 'MM/YYYY') = :PN_DATE\n" +
                            "                             AND   ss_3.FN_TREE_SCORE <> 0 \n" +
                            "                             GROUP BY f_3.FC_DESC_FACTOR, decode(ec_3.fc_division,'CORP',ec_3.FC_TITLE, ec_3.FC_TITLE||' - '||ec_3.FC_DIVISION), ec_3.FN_USERID              \n" +
                            "                             ) tbl\n" +
                            "                        WHERE FC_DESC_FACTOR IS NOT NULL\n" +
                            "                        GROUP BY FC_DESC_FACTOR, FC_TITLE)\n" +
                            "                 ), 0) LIDERAZGO,\n" +
                            "             nvl((SELECT ROUND(AVG(FN_SINGLE_SCORE), 2) \n" +
                            "                  FROM (SELECT  FC_DESC_FACTOR, \n" +
                            "                                FC_TITLE,\n" +
                            "                                AVG(tbl.score) FN_SINGLE_SCORE\n" +
                            "                        FROM \n" +
                            "                            (SELECT f_3.FC_DESC_FACTOR,  \n" +
                            "                                    decode(ec_3.fc_division,'CORP',ec_3.FC_TITLE, ec_3.FC_TITLE||' - '||ec_3.FC_DIVISION) FC_TITLE, \n" +
                            "                                    ec_3.FN_USERID, avg(ss_3.FN_TREE_SCORE)  score\n" +
                            "                             FROM  PUL_EMPLOYEE_CLIMA  ec_3       \n" +
                            "                             LEFT JOIN  PUL_SUBFACTOR_SCORE ss_3 ON ec_3.FN_ID_EMPLOYEE_CLIMA = ss_3.FN_ID_EMPLOYEE_CLIMA           \n" +
                            "                             LEFT JOIN PUL_SUBFACTOR sf_3 ON sf_3.FC_DESC_SUBFACTOR = ss_3.FC_DESC_SUBFACTOR \n" +
                            "                             LEFT JOIN PUL_FACTOR     f_3 ON f_3.FN_ID_FACTOR = sf_3.FN_ID_FACTOR        \n" +
                            "                             WHERE ec_3.FN_USERID IN (TABLEQ.FN_USERID)\n" +
                            "                             AND   ss_3.FN_TREE_SCORE IS NOT NULL \n" +
                            "                             AND   f_3.FC_DESC_FACTOR LIKE 'Organización'\n" +
                            "                             AND   TO_CHAR(ec_3.FD_STRUCTURE_DATE, 'MM/YYYY') = :PN_DATE\n" +
                            "                             AND   ss_3.FN_TREE_SCORE <> 0 \n" +
                            "                             GROUP BY f_3.FC_DESC_FACTOR, decode(ec_3.fc_division,'CORP',ec_3.FC_TITLE, ec_3.FC_TITLE||' - '||ec_3.FC_DIVISION), ec_3.FN_USERID              \n" +
                            "                             ) tbl\n" +
                            "                        WHERE FC_DESC_FACTOR IS NOT NULL\n" +
                            "                        GROUP BY FC_DESC_FACTOR, FC_TITLE)\n" +
                            "                 ), 0)  ORGANIZACION,\n" +
                            "             nvl((SELECT ROUND(AVG(FN_SINGLE_SCORE), 2) \n" +
                            "                  FROM (SELECT  FC_DESC_FACTOR, \n" +
                            "                                FC_TITLE,\n" +
                            "                                AVG(tbl.score) FN_SINGLE_SCORE\n" +
                            "                        FROM \n" +
                            "                            (SELECT f_3.FC_DESC_FACTOR,  \n" +
                            "                                    decode(ec_3.fc_division,'CORP',ec_3.FC_TITLE, ec_3.FC_TITLE||' - '||ec_3.FC_DIVISION) FC_TITLE, \n" +
                            "                                    ec_3.FN_USERID, avg(ss_3.FN_TREE_SCORE)  score\n" +
                            "                             FROM  PUL_EMPLOYEE_CLIMA  ec_3       \n" +
                            "                             LEFT JOIN  PUL_SUBFACTOR_SCORE ss_3 ON ec_3.FN_ID_EMPLOYEE_CLIMA = ss_3.FN_ID_EMPLOYEE_CLIMA           \n" +
                            "                             LEFT JOIN PUL_SUBFACTOR sf_3 ON sf_3.FC_DESC_SUBFACTOR = ss_3.FC_DESC_SUBFACTOR \n" +
                            "                             LEFT JOIN PUL_FACTOR     f_3 ON f_3.FN_ID_FACTOR = sf_3.FN_ID_FACTOR        \n" +
                            "                             WHERE ec_3.FN_USERID IN (TABLEQ.FN_USERID)\n" +
                            "                             AND   ss_3.FN_TREE_SCORE IS NOT NULL \n" +
                            "                             AND   f_3.FC_DESC_FACTOR LIKE 'Desarrollo y crecimiento'\n" +
                            "                             AND   TO_CHAR(ec_3.FD_STRUCTURE_DATE, 'MM/YYYY') = :PN_DATE\n" +
                            "                             AND   ss_3.FN_TREE_SCORE <> 0 \n" +
                            "                             GROUP BY f_3.FC_DESC_FACTOR, decode(ec_3.fc_division,'CORP',ec_3.FC_TITLE, ec_3.FC_TITLE||' - '||ec_3.FC_DIVISION), ec_3.FN_USERID              \n" +
                            "                             ) tbl\n" +
                            "                        WHERE FC_DESC_FACTOR IS NOT NULL\n" +
                            "                        GROUP BY FC_DESC_FACTOR, FC_TITLE)\n" +
                            "                 ), 0) DESARROLLO,\n" +
                            "             nvl((SELECT ROUND(AVG(FN_SINGLE_SCORE), 2) \n" +
                            "                  FROM (SELECT  FC_DESC_FACTOR, \n" +
                            "                                FC_TITLE,\n" +
                            "                                AVG(tbl.score) FN_SINGLE_SCORE\n" +
                            "                        FROM \n" +
                            "                            (SELECT f_3.FC_DESC_FACTOR,  \n" +
                            "                                    decode(ec_3.fc_division,'CORP',ec_3.FC_TITLE, ec_3.FC_TITLE||' - '||ec_3.FC_DIVISION) FC_TITLE, \n" +
                            "                                    ec_3.FN_USERID, avg(ss_3.FN_TREE_SCORE)  score\n" +
                            "                             FROM  PUL_EMPLOYEE_CLIMA  ec_3       \n" +
                            "                             LEFT JOIN  PUL_SUBFACTOR_SCORE ss_3 ON ec_3.FN_ID_EMPLOYEE_CLIMA = ss_3.FN_ID_EMPLOYEE_CLIMA           \n" +
                            "                             LEFT JOIN PUL_SUBFACTOR sf_3 ON sf_3.FC_DESC_SUBFACTOR = ss_3.FC_DESC_SUBFACTOR \n" +
                            "                             LEFT JOIN PUL_FACTOR     f_3 ON f_3.FN_ID_FACTOR = sf_3.FN_ID_FACTOR        \n" +
                            "                             WHERE ec_3.FN_USERID IN (TABLEQ.FN_USERID)\n" +
                            "                             AND   ss_3.FN_TREE_SCORE IS NOT NULL \n" +
                            "                             AND   f_3.FC_DESC_FACTOR LIKE 'Clientes'\n" +
                            "                             AND   TO_CHAR(ec_3.FD_STRUCTURE_DATE, 'MM/YYYY') = :PN_DATE\n" +
                            "                             AND   ss_3.FN_TREE_SCORE <> 0 \n" +
                            "                             GROUP BY f_3.FC_DESC_FACTOR, decode(ec_3.fc_division,'CORP',ec_3.FC_TITLE, ec_3.FC_TITLE||' - '||ec_3.FC_DIVISION), ec_3.FN_USERID              \n" +
                            "                             ) tbl\n" +
                            "                        WHERE FC_DESC_FACTOR IS NOT NULL\n" +
                            "                        GROUP BY FC_DESC_FACTOR, FC_TITLE)\n" +
                            "                 ), 0)   CLIENTES,\n" +
                            "             nvl((SELECT ROUND(AVG(FN_SINGLE_SCORE), 2) \n" +
                            "                  FROM (SELECT  FC_DESC_FACTOR, \n" +
                            "                                FC_TITLE,\n" +
                            "                                AVG(tbl.score) FN_SINGLE_SCORE\n" +
                            "                        FROM \n" +
                            "                            (SELECT f_3.FC_DESC_FACTOR,  \n" +
                            "                                    decode(ec_3.fc_division,'CORP',ec_3.FC_TITLE, ec_3.FC_TITLE||' - '||ec_3.FC_DIVISION) FC_TITLE, \n" +
                            "                                    ec_3.FN_USERID, avg(ss_3.FN_TREE_SCORE)  score\n" +
                            "                             FROM  PUL_EMPLOYEE_CLIMA  ec_3       \n" +
                            "                             LEFT JOIN  PUL_SUBFACTOR_SCORE ss_3 ON ec_3.FN_ID_EMPLOYEE_CLIMA = ss_3.FN_ID_EMPLOYEE_CLIMA           \n" +
                            "                             LEFT JOIN PUL_SUBFACTOR sf_3 ON sf_3.FC_DESC_SUBFACTOR = ss_3.FC_DESC_SUBFACTOR \n" +
                            "                             LEFT JOIN PUL_FACTOR     f_3 ON f_3.FN_ID_FACTOR = sf_3.FN_ID_FACTOR        \n" +
                            "                             WHERE ec_3.FN_USERID IN (TABLEQ.FN_USERID)\n" +
                            "                             AND   ss_3.FN_TREE_SCORE IS NOT NULL \n" +
                            "                             AND   f_3.FC_DESC_FACTOR LIKE 'Trabajo en equipo'\n" +
                            "                             AND   TO_CHAR(ec_3.FD_STRUCTURE_DATE, 'MM/YYYY') = :PN_DATE\n" +
                            "                             AND   ss_3.FN_TREE_SCORE <> 0 \n" +
                            "                             GROUP BY f_3.FC_DESC_FACTOR, decode(ec_3.fc_division,'CORP',ec_3.FC_TITLE, ec_3.FC_TITLE||' - '||ec_3.FC_DIVISION), ec_3.FN_USERID              \n" +
                            "                             ) tbl\n" +
                            "                        WHERE FC_DESC_FACTOR IS NOT NULL\n" +
                            "                        GROUP BY FC_DESC_FACTOR, FC_TITLE)\n" +
                            "                 ), 0)  TEQUIPO\n" +
                            "      FROM (SELECT FN_ID_EMPLOYEE_CLIMA,\n" +
                            "                   FN_EMPLOYEE_NUMBER,\n" +
                            "                   FC_TITLE,\n" +
                            "                   FN_USERID,\n" +
                            "                   FN_MANAGER,\n" +
                            "                   NVL ((SELECT LISTAGG (tal.FC_STATUS_PLAN, '|') WITHIN GROUP (ORDER BY tal.FC_STATUS_PLAN)\n" +
                            "                         FROM (SELECT DISTINCT FC_STATUS_PLAN FROM PORTALUNICO.PUL_PLAN_ACCION PA WHERE PA.FN_ID_EMPLOYEE_CLIMA   = ecTable.FN_ID_EMPLOYEE_CLIMA) tal), 'PENDIENTE') ESTATUS_PLAN,\n" +
                            "                   NVL ((SELECT LISTAGG (tabla2.FC_STATUS_REVIEW, '|') WITHIN GROUP (ORDER BY tabla2.FC_STATUS_REVIEW)\n" +
                            "                         FROM (SELECT DISTINCT FC_STATUS_REVIEW FROM PORTALUNICO.PUL_PLAN_ACCION PA WHERE PA.FN_ID_EMPLOYEE_CLIMA = ecTable.FN_ID_EMPLOYEE_CLIMA) tabla2), 'PENDIENTE') ESTATUS_REVIEW                         \n" +
                            "            FROM PORTALUNICO.PUL_EMPLOYEE_CLIMA ecTable\n" +
                            "            WHERE TO_CHAR (ecTable.FD_STRUCTURE_DATE, 'MM/YYYY') = :PN_DATE \n" +
                            "            START WITH ecTable.FN_USERID = :PN_USERID\n" +
                            "            CONNECT BY PRIOR ecTable.FN_USERID = ecTable.FN_MANAGER\n" +
                            "            ) TABLEQ \n" +
                            "      )  tablaGeneral\n" +
                            "      WHERE tablaGeneral.ESTATUS_PLAN LIKE '%' || :PN_STATUS_PLAN || '%'\n" + reviewCondition +
                            "      AND   TO_CHAR(tablaGeneral.FN_EMPLOYEE_NUMBER) LIKE '%' || :PN_EMPLOYEE_NUMBER || '%'\n" +
                            "      AND   (tablaGeneral.CALIFICACION < :PN_SCORE  OR tablaGeneral.LIDERAZGO < :PN_SCORE OR  tablaGeneral.ORGANIZACION < :PN_SCORE OR tablaGeneral.DESARROLLO < :PN_SCORE OR tablaGeneral.CLIENTES < :PN_SCORE OR  tablaGeneral.TEQUIPO < :PN_SCORE)\n" +
                            "      and   tablaGeneral.FN_ANSWERED > 2\n" +
                            "      and   tablaGeneral.FN_PLAN > 2";
                            
        Query oQuery = JPA.em().createNativeQuery(statement)
            .setParameter("PN_USERID", userId)
            .setParameter("PN_DATE", date)
            .setParameter("PN_SCORE", plan)
            .setParameter("PN_STATUS_PLAN", statusPlan)
            .setParameter("PN_STATUS_REVIEW", statusReview)
            .setParameter("PN_EMPLOYEE_NUMBER", search);

        List result = oQuery.getResultList();
        return Long.parseLong(result.get(0).toString());
    }
    
    /**
     * Metodo de ejecucion del query para obtener los dependientes por empleado
     * @param userId
     * @param date
     * @return 
     */
    public static int getEmployeeDepedants(int userId, String date) {
        int result = 0;

        try {
            OracleConnection connection = getOracleConnection();
            CallableStatement statement = connection.prepareCall("{ ? = call PORTALUNICO.FN_GET_TODO_SURVEY_DIRECT(?, ?) }");

            statement.setInt(2, userId);
            statement.setString(3, date);
            statement.registerOutParameter(1, OracleTypes.NUMBER);
            statement.execute();

            result = ((OracleCallableStatement) statement).getInt(1);
            statement.close();
        } catch (SQLException e) {
            Logger.error(e.getMessage());
        }

        return result;
    }
}
