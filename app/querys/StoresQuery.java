package querys;

import play.db.jpa.JPA;
import javax.persistence.Query;
import java.util.*;

public class StoresQuery {
    public static List getStores(String search, String period, int start, int end) {
        String query = "SELECT * FROM\n" +
                        "(\n" +
                        "    SELECT\n" +
                        "        S.FN_ID_STORE, S.FC_DESC_STORE, S.FC_COUNTRY, S.FC_DESC_SUBDIV, S.FN_HUMAN_RESOURCES,\n" +
                        "        S.FN_STORE_HEAD, S.FN_OTHER,\n" +
                        "        HR.FC_FIRST_NAME || ' ' || HR.FC_LAST_NAME HR_NAME, HR.FN_EMPLOYEE_NUMBER HR_EMPLOYEE_NUMBER,\n" +
                        "        SH.FC_FIRST_NAME || ' ' || SH.FC_LAST_NAME SH_NAME, SH.FN_EMPLOYEE_NUMBER SH_EMPLOYEE_NUMBER,\n" +
                        "        OTH.FC_FIRST_NAME || ' ' || OTH.FC_LAST_NAME OTH_NAME, OTH.FN_EMPLOYEE_NUMBER OTH_EMPLOYEE_NUMBER,\n" +
                        "        S.FN_SEQ_STORE,\n" +
                        "        ROW_NUMBER() OVER (ORDER BY S.FN_ID_STORE ASC) row_number\n" +
                        "    FROM \n" +
                        "        PORTALUNICO.PUL_STORE S\n" +
                        "    LEFT JOIN\n" +
                        "        (\n" +
                        "            SELECT FN_USERID, FC_FIRST_NAME, FC_LAST_NAME, FN_EMPLOYEE_NUMBER, TO_CHAR(FD_STRUCTURE_DATE, 'mm/yyyy') FC_STRUCTURE_DATE FROM PUL_EMPLOYEE_CLIMA\n" +
                        "        ) HR ON HR.FN_USERID = S.FN_HUMAN_RESOURCES AND HR.FC_STRUCTURE_DATE = :PN_PERIOD\n" +
                        "    LEFT JOIN\n" +
                        "        (\n" +
                        "            SELECT FN_USERID, FC_FIRST_NAME, FC_LAST_NAME, FN_EMPLOYEE_NUMBER, TO_CHAR(FD_STRUCTURE_DATE, 'mm/yyyy') FC_STRUCTURE_DATE FROM PUL_EMPLOYEE_CLIMA\n" +
                        "        ) SH ON SH.FN_USERID = S.FN_STORE_HEAD AND SH.FC_STRUCTURE_DATE = :PN_PERIOD\n" +
                        "    LEFT JOIN\n" +
                        "        (\n" +
                        "            SELECT FN_USERID, FC_FIRST_NAME, FC_LAST_NAME, FN_EMPLOYEE_NUMBER, TO_CHAR(FD_STRUCTURE_DATE, 'mm/yyyy') FC_STRUCTURE_DATE FROM PUL_EMPLOYEE_CLIMA\n" +
                        "        ) OTH ON OTH.FN_USERID = S.FN_OTHER AND OTH.FC_STRUCTURE_DATE = :PN_PERIOD\n" +
                        "    WHERE (TO_CHAR(S.FN_ID_STORE) LIKE '%' || :PN_SEARCH || '%' OR LOWER(S.FC_DESC_STORE) LIKE '%' || LOWER(:PN_SEARCH) || '%')\n" +
                        "    AND TO_CHAR(FD_STRUCTURE_DATE, 'mm/yyyy') = :PN_PERIOD\n" +
                        "    ORDER BY S.FN_ID_STORE ASC\n" +
                        ") TBL_STORE\n" +
                        "WHERE row_number BETWEEN :START AND :END";

     	Query oQuery = JPA.em().createNativeQuery(query)
            .setParameter("PN_SEARCH", search)
            .setParameter("PN_PERIOD", period)
            .setParameter("START", start)
            .setParameter("END", end);
        
        return oQuery.getResultList();
    }
    
    public static List getStructureDates() {
        String statement = "SELECT DISTINCT TO_CHAR(FD_STRUCTURE_DATE, 'mm/yyyy')\n" +
                           "FROM PORTALUNICO.PUL_STORE\n" + 
                           "ORDER BY TO_DATE('15' || TO_CHAR(FD_STRUCTURE_DATE, 'mm/yyyy'), 'dd/mm/yyyy') DESC";

        return JPA.em().createNativeQuery(statement).getResultList();
    }
}
