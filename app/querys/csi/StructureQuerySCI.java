package querys.csi;

import static utils.Queries.getOracleConnection;

import java.math.BigDecimal;
import java.sql.CallableStatement;
import java.sql.SQLException;
import java.util.List;

import javax.persistence.Query;

import oracle.jdbc.OracleConnection;
import oracle.jdbc.OracleTypes;
import play.db.jpa.JPA;
import utils.PagingResult;

public class StructureQuerySCI {

	/**
	 * Obtienen las fechas para la estructura csi
	 * @return List 
	 */
    public static List getDatesStructures() {
        String statement = "select distinct to_char(FD_STRUCTURE_DATE, 'mm/yyyy') "
                + " from PORTALUNICO.PUL_EMPLOYEE_CSI ";

        return JPA.em().createNativeQuery(statement).getResultList();
    }

    /**
     * Obtiene los datos paginados de la estructura CSI 
     * @param conditions
     * @param order
     * @param start
     * @param end
     * @param structureDate
     * @param oneSection
     * @param length
     * @return PagingResult
     */
    public static PagingResult getStructure(String conditions, String order, int start, int end, String structureDate,
            String oneSection, int length) {

        String statement = "SELECT " + "TBL.FC_DESC_LOCATION, " + "TBL.FN_EMPLOYEE_NUMBER, " + "U.FC_USERNAME, "
                + "TBL.FC_FIRST_NAME || ' ' ||" + "TBL.FC_LAST_NAME, " + "TBL.FC_FUNCTION, " + "TBL.FC_DESC_SECTION, "
                + "TBL.FN_ID_EMPLOYEE_CSI, " + "TBL.renglon " + "FROM " + "(SELECT rownum AS renglon, "
                + "  FC_DESC_SECTION, " + "  FC_DESC_LOCATION, " + "  FN_EMPLOYEE_NUMBER, " + "  FN_USERID, "
                + "  FC_FIRST_NAME, " + "  FC_LAST_NAME, " + "  FC_FUNCTION, " + "  FN_MANAGER, "
                + "  FN_ID_EMPLOYEE_CSI " + "  from "
                + "  (SELECT PEC.FN_USERID, PEC.FC_DESC_LOCATION, PEC.FN_EMPLOYEE_NUMBER, PEC.FC_FIRST_NAME, PEC.FC_LAST_NAME, "
                + "   PEC.FC_FUNCTION, PEC.FN_MANAGER,  LISTAGG(PSE.FN_ID_SECTION ||' '|| PSE.FC_DESC_SECTION, '/ ') WITHIN GROUP (ORDER BY PSE.FC_DESC_SECTION) FC_DESC_SECTION, "
                + "    FN_ID_EMPLOYEE_CSI" + "		FROM PORTALUNICO.PUL_EMPLOYEE_CSI PEC "
                + "		  left JOIN PORTALUNICO.PUL_EMPLOYEE_SECTIONS PES ON "
                + "			  PEC.FN_USERID = PES.FN_USERID " + "       left JOIN PORTALUNICO.PUL_SECTIONS PSE ON "
                + "       PES.FN_ID_SECTION = PSE.FN_ID_SECTION " + oneSection
                + "     WHERE TO_CHAR (PEC.fd_structure_date, 'MM/YYYY') = '" + structureDate + "'"
                + "     GROUP BY PEC.FN_USERID, PEC.FC_DESC_LOCATION, PEC.FN_EMPLOYEE_NUMBER, PEC.FC_FIRST_NAME, PEC.FC_LAST_NAME, PEC.FN_MANAGER, FN_ID_EMPLOYEE_CSI, PEC.FC_FUNCTION "
                + "  ) " + "  CONNECT BY prior FN_USERID = FN_MANAGER " + "  START WITH FN_USERID IN "
                + "  ( SELECT FN_USERID FROM PORTALUNICO.pul_employee_csi " + conditions + " ) " + ")TBL "
                + " INNER JOIN PORTALUNICO.PUL_EMPLOYEE_CSI RE " + " ON RE.FN_USERID = TBL.FN_MANAGER "
                + " AND TO_CHAR (fd_structure_date, 'MM/YYYY') = '" + structureDate + "'" + " INNER JOIN PUL_USER U "
                + " ON TBL.FN_USERID = U.FN_ID_USER " + " where TBL.renglon BETWEEN  " + start + " AND " + end + order;
      
        Query oQuery = JPA.em().createNativeQuery(statement);
        return new PagingResult(getCountRows(conditions, structureDate, oneSection), oQuery.getResultList());
    }

    /**
     * Query para ordenar los datos de la consulta
     * @param sSortDir_0
     * @param iSortCol_0
     * @return String
     */
    public static String getOrder(String sSortDir_0, int iSortCol_0) {
        String order;

        switch (iSortCol_0) {
        // case 0 : order = " ORDER BY TBL.renglon " + sSortDir_0;
        // break;
        case 1:
            order = " ORDER BY TBL.FC_DESC_LOCATION " + sSortDir_0;
            break;
        case 2:
            order = " ORDER BY TBL.FN_EMPLOYEE_NUMBER " + sSortDir_0;
            break;
        case 3:
            order = " ORDER BY U.FC_USERNAME " + sSortDir_0;
            break;
        case 4:
            order = " ORDER BY TBL.FC_FIRST_NAME || ' ' || TBL.FC_LAST_NAME " + sSortDir_0;
            break;
        case 5:
            order = " ORDER BY TBL.FC_FUNCTION " + sSortDir_0;
            break;
        case 6:
            order = " ORDER BY TBL.FC_DESC_SECTION " + sSortDir_0;
            break;
        default:
            order = " ORDER BY TBL.FC_DESC_LOCATION ASC";
            break;
        }
        return order;
    }

    /**
     * Obtiene el número de filas totales
     * @param conditions
     * @param structureDate
     * @param oneSection
     * @return int
     */
    public static int getCountRows(String conditions, String structureDate, String oneSection) {
        String statement = " select count(1) from ("
                + " SELECT PEC.FN_USERID, PEC.FC_DESC_LOCATION, PEC.FN_EMPLOYEE_NUMBER, PEC.FC_FIRST_NAME, PEC.FC_LAST_NAME, "
                + "   PEC.FN_MANAGER,  LISTAGG(PSE.FN_ID_SECTION ||' '|| PSE.FC_DESC_SECTION, '/ ') WITHIN GROUP (ORDER BY PSE.FC_DESC_SECTION) FC_DESC_SECTION, FN_ID_EMPLOYEE_CSI "
                + "		FROM PORTALUNICO.PUL_EMPLOYEE_CSI PEC "
                + "		  left JOIN PORTALUNICO.PUL_EMPLOYEE_SECTIONS PES ON "
                + "			  PEC.FN_USERID = PES.FN_USERID " + "       left JOIN PORTALUNICO.PUL_SECTIONS PSE ON "
                + "       PES.FN_ID_SECTION = PSE.FN_ID_SECTION " + oneSection
                + "     WHERE TO_CHAR (PEC.fd_structure_date, 'MM/YYYY') = '" + structureDate + "'"
                + "     GROUP BY PEC.FN_USERID, PEC.FC_DESC_LOCATION, PEC.FN_EMPLOYEE_NUMBER, PEC.FC_FIRST_NAME, PEC.FC_LAST_NAME, PEC.FN_MANAGER, FN_ID_EMPLOYEE_CSI "
                + "  ) " + "  CONNECT BY prior FN_USERID = FN_MANAGER " + "  START WITH FN_USERID      IN "
                + "  ( SELECT FN_USERID FROM PORTALUNICO.pul_employee_csi " + conditions + " )";

        Query oQuery = JPA.em().createNativeQuery(statement);
        List result = oQuery.getResultList();
        return Integer.parseInt(result.get(0).toString());
    }

    /**
     * Obtiene las ubicaciones en la estructura CSI
     * @param date
     * @return List
     */
    public static List getLocation(String date) {
        String statement = " select  distinct FC_DESC_LOCATION "
                + " from portalunico.pul_employee_csi ORDER by FC_DESC_LOCATION";

        if (date != null && date.length() > 0) {
            statement = " select  distinct FC_DESC_LOCATION " + " from "
                    + " (SELECT * FROM portalunico.PUL_EMPLOYEE_CSI"
                    + " WHERE TO_CHAR (fd_structure_date, 'MM/YYYY') = '" + date + "')" + "ORDER by FC_DESC_LOCATION";
        }

        Query oQuery = JPA.em().createNativeQuery(statement);

        return oQuery.getResultList();
    }

    /**
     * Obtiene las secciones
     * @return List
     */
    public static List getSection() {
        String statement = " select  FN_ID_SECTION, FN_ID_SECTION || ' ' ||FC_DESC_SECTION "
                + " from portalunico.pul_sections ORDER by FC_DESC_SECTION";

        Query oQuery = JPA.em().createNativeQuery(statement);

        return oQuery.getResultList();
    }

    /**
     * Obtiene los departamentos del nivel 1
     * @param level
     * @return List
     */
    public static List getDepartmentsByLevel1(int level) {
        String statement = "select FC_TITLE, FN_USERID, FC_DEPARTMENT, "
                + " COALESCE(SUBSTR(FC_EMAIL , 0, INSTR(FC_EMAIL, '@') - 1), FC_EMAIL) FC_USERNAME "
                + " from PORTALUNICO.PUL_EMPLOYEE_CSI " + " where FN_LEVEL = " + level + " order by FC_TITLE";

        Query oQuery = JPA.em().createNativeQuery(statement);

        return oQuery.getResultList();
    }

    /**
     * Obtiene los departamentos según el nivel
     * @param userId
     * @param date
     * @param withDependants
     * @return List
     */
    public static List getDepartmentsByLevel(int userId, String date, boolean withDependants) {
        String statement = "select decode(TBL.fc_division,'CORP',TBL.FC_TITLE, TBL.FC_TITLE||' - '||TBL.FC_DIVISION) FC_TITLE, FN_USERID, FC_DEPARTMENT "
                + " from " + " (select  FC_TITLE, FC_DIVISION, FC_DESC_LOCATION, FN_USERID, FC_DEPARTMENT "
                + " from PORTALUNICO.PUL_EMPLOYEE_CSI " + " where   FN_MANAGER = " + userId
                + " ) TBL order by FC_TITLE";

        String andWhere = withDependants == true
                ? "  WHERE ((SELECT COUNT(1) FROM PORTALUNICO.PUL_EMPLOYEE_CSI SEC WHERE SEC.FN_MANAGER = TBL.FN_USERID) > 0 OR\n"
                        + "  PORTALUNICO.FN_GET_SURVEY_RESOLVED('" + date + "', TBL.FN_USERID) > 2 )\n"
                : "";

        if (date != null && !date.equals("null") && !date.isEmpty()) {
            statement = "select \n"
                    + "  decode(TBL.fc_division, 'CORP', TBL.FC_TITLE, TBL.FC_TITLE || ' - ' || TBL.FC_DIVISION) FC_TITLE,\n"
                    + "  FN_USERID,\n" + "  FC_DEPARTMENT, \n" + "  FC_USERNAME \n" + "from (\n" + "  SELECT\n"
                    + "    EC.FC_TITLE, EC.FC_DIVISION, EC.FC_DESC_LOCATION, EC.FN_USERID, EC.FC_DEPARTMENT,\n"
                    + "    COALESCE(SUBSTR(FC_EMAIL , 0, INSTR(FC_EMAIL, '@') - 1), FC_EMAIL) FC_USERNAME \n"
                    + "  FROM \n" + "    PORTALUNICO.PUL_EMPLOYEE_CSI EC\n"
                    + "  WHERE TO_CHAR (fd_structure_date, 'MM/YYYY') = '" + date + "' AND FN_MANAGER = " + userId
                    + "\n" + ") TBL \n" + andWhere + "order by FC_TITLE";
        }

        Query oQuery = JPA.em().createNativeQuery(statement);

        return oQuery.getResultList();
    }

    /**
     * Obtiene la información del usuario seleccionado
     * @param userId Número de empleado
     * @return List
     */
    public static List getInfoModal(int userId) {
        String statement = "select ec.fc_email, pu.FC_USERNAME, ec.FC_FUNCTION, RE.FC_FIRST_NAME || ' ' || RE.FC_LAST_NAME, ec.FC_FIRST_NAME, ec.FC_LAST_NAME, ec.FC_DESC_LOCATION"
                + " from PORTALUNICO.PUL_EMPLOYEE_CSI ec " + " inner join PUL_USER pu on pu.FN_ID_USER = ec.FN_USERID "
                + " INNER JOIN PORTALUNICO.PUL_EMPLOYEE_CSI RE ON RE.FN_USERID = EC.FN_MANAGER "
                + " where ec.FN_EMPLOYEE_NUMBER = " + userId;

        Query oQuery = JPA.em().createNativeQuery(statement);

        return oQuery.getResultList();
    }

    /**
     * Actualiza el email y ubicación del empleado
     * @param email
     * @param idEmployee
     * @param sessionUser
     * @param ubication
     * @return int
     */
    public static int setPlanEmail(String email, int idEmployee, String sessionUser, String ubication) {

        String statement = " Update PORTALUNICO.PUL_EMPLOYEE_CSI set FD_MOD_DATE = SYSDATE, FC_MOD_FOR= ?, fc_email = ?, fc_desc_location = ? where FN_EMPLOYEE_NUMBER = ?";

        int updateEmail = JPA.em().createNativeQuery(statement).setParameter(1, sessionUser).setParameter(2, email)
                .setParameter(3, ubication).setParameter(4, idEmployee).executeUpdate();
        return updateEmail;
    }

    /**
     * Obtiene los managers del empleado
     * @param managerId
     * @param managerName
     * @param userIdEmployee
     * @return List
     */
    public static List getManagers(int managerId, String managerName, int userIdEmployee) {
        String cond1 = " ec.FN_EMPLOYEE_NUMBER = " + managerId, cond2 = "1 > 0";

        if (managerName != null && managerName.length() > 0) {
            cond2 = " pu.FC_USERNAME = '" + managerName + "'";
            cond1 = "1 > 0";
        }

        String statement = " select ec.*, pu.FC_USERNAME " + " from PORTALUNICO.PUL_EMPLOYEE_CSI ec"
                + " inner join PUL_USER pu on pu.FN_ID_USER = ec.FN_USERID " + " where " + cond1 + " AND " + cond2
                + " AND ec.fn_manager != " + userIdEmployee;

        Query oQuery = JPA.em().createNativeQuery(statement);

        return oQuery.getResultList();
    }

    /**
     * Actualiza el manager del empleado
     * @param userId
     * @param newManagerId
     * @param sessionUser
     * @return int Número de entidades actualizadas
     */
    public static int setManager(int userId, int newManagerId, String sessionUser) {
        int managerId = getUserIdByEmployeeId(newManagerId);

        String statement = " Update PORTALUNICO.PUL_EMPLOYEE_CSI set FD_MOD_DATE = SYSDATE, FC_MOD_FOR = '"
                + sessionUser + "', FN_MANAGER = " + managerId + " where FN_EMPLOYEE_NUMBER = " + userId;
        return JPA.em().createNativeQuery(statement).executeUpdate();
    }

    /**
     * Obtiene el id de usuario a partir del número de empleado
     * @param employeeNumber
     * @return int
     */
    public static int getUserIdByEmployeeId(int employeeNumber) {
        String statement = " select FN_USERID from " + " PORTALUNICO.PUL_EMPLOYEE_CSI "
                + " where FN_EMPLOYEE_NUMBER = ? ";

        BigDecimal userId = (BigDecimal) JPA.em().createNativeQuery(statement).setParameter(1, employeeNumber)
                .getSingleResult();

        return userId.intValue();
    }

    /**
     * Obtiene las funciones de los empleados
     * @return List
     */
    public static List getFunctions() {
        String statement = " select DISTINCT FC_FUNCTION " + " from PORTALUNICO.PUL_EMPLOYEE_CSI order by FC_FUNCTION";

        Query oQuery = JPA.em().createNativeQuery(statement);

        return oQuery.getResultList();
    }

    /**
     * Cambia la función de un empleado
     * @param userId
     * @param newFunction
     * @param sessionUser
     * @return int Número de entidades actualizadas
     */
    public static int setFunction(int userId, String newFunction, String sessionUser) {
        String statement = " Update PORTALUNICO.PUL_EMPLOYEE_CSI set FD_MOD_DATE = SYSDATE, FC_MOD_FOR= ?, FC_FUNCTION = ? where FN_EMPLOYEE_NUMBER = ?";
        return JPA.em().createNativeQuery(statement).setParameter(1, sessionUser).setParameter(2, newFunction)
                .setParameter(3, userId).executeUpdate();
    }

    /**
     * Obtiene los datos de la estrucutra CSI
     * @param conditions
     * @param structureDate
     * @param oneSection
     * @return List
     */
    public static List export(String conditions, String structureDate, String oneSection) {

        String statement = "SELECT TBL.renglon, " + "TBL.FC_DESC_SECTION, " + "TBL.FC_DESC_LOCATION, "
                + "TBL.FN_EMPLOYEE_NUMBER, " + "TBL.FN_USERID, " + "TBL.FC_FIRST_NAME || ' ' ||" + "TBL.FC_LAST_NAME, "
                + "TBL.FN_ID_EMPLOYEE_CSI " + "FROM " + "(SELECT rownum AS renglon, " + "  FC_DESC_SECTION, "
                + "  FC_DESC_LOCATION, " + "  FN_EMPLOYEE_NUMBER, " + "  FN_USERID, " + "  FC_FIRST_NAME, "
                + "  FC_LAST_NAME, " + "  FN_MANAGER, " + "  FN_ID_EMPLOYEE_CSI " + "  from "
                + "  (SELECT PEC.FN_USERID, PEC.FC_DESC_LOCATION, PEC.FN_EMPLOYEE_NUMBER, PEC.FC_FIRST_NAME, PEC.FC_LAST_NAME, "
                + "   PEC.FN_MANAGER,  LISTAGG(PSE.FN_ID_SECTION ||' '|| PSE.FC_DESC_SECTION, '/ ') WITHIN GROUP (ORDER BY PSE.FC_DESC_SECTION) FC_DESC_SECTION, "
                + "    FN_ID_EMPLOYEE_CSI" + "		FROM PORTALUNICO.PUL_EMPLOYEE_CSI PEC "
                + "		  left JOIN PORTALUNICO.PUL_EMPLOYEE_SECTIONS PES ON "
                + "			  PEC.FN_USERID = PES.FN_USERID " + "       left JOIN PORTALUNICO.PUL_SECTIONS PSE ON "
                + "       PES.FN_ID_SECTION = PSE.FN_ID_SECTION " + oneSection
                + "     WHERE TO_CHAR (PEC.fd_structure_date, 'MM/YYYY') = '" + structureDate + "'"
                + "     GROUP BY PEC.FN_USERID, PEC.FC_DESC_LOCATION, PEC.FN_EMPLOYEE_NUMBER, PEC.FC_FIRST_NAME, PEC.FC_LAST_NAME, PEC.FN_MANAGER, FN_ID_EMPLOYEE_CSI "
                + "  ) " + "  CONNECT BY prior FN_USERID = FN_MANAGER " + "  START WITH FN_USERID      IN "
                + "  ( SELECT FN_USERID FROM PORTALUNICO.pul_employee_csi " + conditions + " ) " + ")TBL "
                + "INNER JOIN PORTALUNICO.PUL_EMPLOYEE_CSI RE " + "ON RE.FN_USERID = TBL.FN_MANAGER "
                + "AND TO_CHAR (fd_structure_date, 'MM/YYYY') = '" + structureDate + "'" + "INNER JOIN PUL_USER U "
                + "ON TBL.FN_USERID = U.FN_ID_USER " + "ORDER BY TBL.renglon ASC ";

        Query oQuery = JPA.em().createNativeQuery(statement);
        return oQuery.getResultList();
    }

    /**
     * Obtiene los departamentos según su ubicación
     * @param location
     * @param date
     * @return List
     */
    public static List getDepartments(String location, String date) {
        String statement = " select distinct FC_DEPARTMENT " + " from PUL_EMPLOYEE_CSI " + " where FC_DESC_LOCATION = '"
                + location + "' order by FC_DEPARTMENT";
        if (date != null && date.length() > 0) {
            statement = " select distinct FC_DEPARTMENT " + " from " + " (SELECT * FROM PUL_EMPLOYEE_CSI"
                    + " WHERE TO_CHAR (fd_structure_date, 'MM/YYYY') = '" + date + "')" + " where FC_DESC_LOCATION = '"
                    + location + "' order by FC_DEPARTMENT";
        }

        Query oQuery = JPA.em().createNativeQuery(statement);

        return oQuery.getResultList();
    }

}
