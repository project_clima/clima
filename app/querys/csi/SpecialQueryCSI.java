package querys.csi;

import java.util.List;

import javax.persistence.Query;

import play.db.jpa.JPA;

public class SpecialQueryCSI {
	/**
	 * Obtiene los niveles de los empleados
	 * @return List
	 */
	public static List getLevels() {
        String statement = "SELECT FN_LEVEL FROM PUL_EMPLOYEE_CLIMA GROUP BY FN_LEVEL ORDER BY FN_LEVEL ASC";
        
        Query oQuery = JPA.em().createNativeQuery(statement);

        return oQuery.getResultList();
    }

	
	/**
	 * Obtiene la clave y el tipo de la tabla tipo especial
	 * @return List
	 */
     public static List getVie() {
        String statement = "SELECT FN_KEY_SPECIAL,FC_TYPE_SPECIAL_NAME FROM PUL_TYPE_SPECIAL order by FN_ORDER";
        
        Query oQuery = JPA.em().createNativeQuery(statement);

        return oQuery.getResultList();
    }
}
