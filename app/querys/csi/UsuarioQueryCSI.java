package querys.csi;

import java.util.List;

import javax.persistence.Query;

import play.db.jpa.JPA;
import utils.PagingResult;

public class UsuarioQueryCSI {
	/**
	 * Obtiene los datos de la estrcuctura
	 * @param conditions
	 * @param start
	 * @param end
	 * @param structureDate
	 * @param userName
	 * @return List
	 */
    public static List getStructure(
            String conditions, int start, int end, String structureDate, String userName) {
                String statement="SELECT * FROM (" +
                                    "SELECT  ROW_NUMBER() OVER (ORDER BY TABLEROW.FN_LEVEL ASC) AS renglon,\n" +
                                    "        TABLEROW.FN_EMPLOYEE_NUMBER, \n" +
                                    "        TABLEROW.FC_FIRST_NAME, \n" +
                                    "        TABLEROW.FC_LAST_NAME, \n" +
                                    "        TABLEROW.FC_FUNCTION, \n" +
                                    "        TABLEROW.FC_DEPARTMENT,\n" +
                                    "        TABLEROW.FC_DESC_LOCATION, \n" +
                                    "        TABLEROW.FN_LEVEL, \n" +
                                    "        (SELECT FC_FIRST_NAME||' '||FC_LAST_NAME FROM PORTALUNICO.PUL_EMPLOYEE_CSI WHERE FN_USERID = TABLEROW.FN_MANAGER AND TO_CHAR(FD_STRUCTURE_DATE, 'MM/YYYY' ) = '"+ structureDate+"') RESPONSABLE, \n" +
                                    "        TABLEROW.FN_USERID,\n" +
                                    "        TABLEROW.FC_USERNAME\n" +
                                    "FROM (SELECT TBL.*, US.FC_USERNAME\n" +
                                    "      FROM (SELECT * FROM PORTALUNICO.PUL_EMPLOYEE_CSI WHERE TO_CHAR(FD_STRUCTURE_DATE, 'MM/YYYY' ) = '"+ structureDate+"')TBL \n" +
                                    "      JOIN PUL_USER US ON US.FN_ID_USER = TBL.FN_USERID \n" +
                                    "	   AND FC_USERNAME LIKE '%"+userName+"%' \n" +
                                    "      AND TO_CHAR(TBL.FD_STRUCTURE_DATE, 'MM/YYYY') = '"+ structureDate+"') TABLEROW\n" +
                                    "      \n" +
                                    "WHERE  "+ conditions +") WHERE renglon BETWEEN "+ start +" AND "+ end ;
                    
           
            Query oQuery = JPA.em().createNativeQuery(statement);

            return  oQuery.getResultList();
        }

    	/**
    	 * Obtiene todos los empleados paginados
    	 * @param conditions
    	 * @param order
    	 * @param start
    	 * @param end
    	 * @return PagingResult
    	 */
        public static PagingResult getEmployees(String conditions, String order, int start, int end) {
            String statement = "select * " +
            " from (  select  rownum as renglon, ec.FN_EMPLOYEE_NUMBER, ec.FC_FIRST_NAME, ec.FC_LAST_NAME, ec.FC_FUNCTION, ec.FC_DEPARTMENT, ec.FN_LEVEL, em.FC_FIRST_NAME || ' ' || em.FC_LAST_NAME as manager" +
            " from PORTALUNICO.PUL_EMPLOYEE_CSI ec " +
            " inner join PORTALUNICO.PUL_EMPLOYEE_CSI em on ec.FN_MANAGER = em.FN_USERID ";
            if (conditions != "") {
                statement += " where " + conditions;
            }
            statement += ") where renglon BETWEEN " + start + " AND " + end + " " + order;

            Query oQuery = JPA.em().createNativeQuery(statement);

            return new PagingResult(getCountRows(conditions, ""), oQuery.getResultList());
        }


        /**
         * Regresa el número de filas totales
         * @param conditions
         * @param date
         * @return int
         */
        public static int getCountRows(String conditions, String date) {
            String statement =  " select count(1) from (" +
                                " select rownum as renglon, FN_USERID, FC_FIRST_NAME, FC_LAST_NAME, FC_FUNCTION, FC_DEPARTMENT, FN_LEVEL, FN_MANAGER,  " +
                                " connect_by_root FN_USERID as root_id " +
                                " from PORTALUNICO.PUL_EMPLOYEE_CSI " +
                                " connect by prior FN_USERID = FN_MANAGER " +
                                " start with FN_MANAGER in ( " +
                                    " select FN_USERID " +
                                    " from PORTALUNICO.PUL_EMPLOYEE_CSI where " + conditions +
                                " ))";
            if(date != null && date.length() > 0) {
                statement =
                    " select count(1) from (" +
                    " select rownum as renglon, FN_USERID, FC_FIRST_NAME, FC_LAST_NAME, FC_FUNCTION, FC_DEPARTMENT, FN_LEVEL, FN_MANAGER,  " +
                    " connect_by_root FN_USERID as root_id " +
                    " from" +
                    " (SELECT * FROM PORTALUNICO.PUL_EMPLOYEE_CSI" +
                    " WHERE TO_CHAR (fd_structure_date, 'MM/YYYY') = '" + date + "')" +
                    " connect by prior FN_USERID = FN_MANAGER " +
                    " start with FN_MANAGER in ( " +
                        " select FN_USERID " +
                        " from PORTALUNICO.PUL_EMPLOYEE_CSI where " + conditions +
                        " AND TO_CHAR (fd_structure_date, 'MM/YYYY') = '" + date + "'" +
                    " ))";
            } 

            Query oQuery = JPA.em().createNativeQuery(statement);
            List result = oQuery.getResultList();
            return Integer.parseInt(result.get(0).toString());
        }

        /**
         * 
         * @param s
         * @return
         */
        public static int getCountRowsAux(String s) {
            String statement = 
                " select count(1) from (" + s + ") ";

            Query oQuery = JPA.em().createNativeQuery(statement);
            List result = oQuery.getResultList();
            return Integer.parseInt(result.get(0).toString());
        }
        
        public static long getCountRowsUsers(String conditions, String structureDate, String userName){
            String statement =  "select count(1) from ("+
                    "SELECT  ROW_NUMBER() OVER (ORDER BY TABLEROW.FN_LEVEL ASC) AS renglon,\n" +
                    "        TABLEROW.FN_EMPLOYEE_NUMBER, \n" +
                    "        TABLEROW.FC_FIRST_NAME, \n" +
                    "        TABLEROW.FC_LAST_NAME, \n" +
                    "        TABLEROW.FC_FUNCTION, \n" +
                    "        TABLEROW.FC_DEPARTMENT,\n" +
                    "        TABLEROW.FC_DESC_LOCATION, \n" +
                    "        TABLEROW.FN_LEVEL, \n" +
                    "        (SELECT FC_FIRST_NAME||' '||FC_LAST_NAME FROM PORTALUNICO.PUL_EMPLOYEE_CSI WHERE FN_USERID = TABLEROW.FN_MANAGER) RESPONSABLE, \n" +
                    "        TABLEROW.FN_USERID,\n" +
                    "        TABLEROW.FC_USERNAME\n" +
                    "FROM (SELECT TBL.*, US.FC_USERNAME\n" +
                    "      FROM (SELECT * FROM PORTALUNICO.PUL_EMPLOYEE_CSI WHERE TO_CHAR(FD_STRUCTURE_DATE, 'MM/YYYY' ) = '"+ structureDate+"')TBL \n" +
                    "      JOIN PUL_USER US ON US.FN_ID_USER = TBL.FN_USERID \n" +
                    "	   AND FC_USERNAME LIKE '%"+userName+"%' \n" +
                    "      AND TO_CHAR(TBL.FD_STRUCTURE_DATE, 'MM/YYYY') = '"+ structureDate+"') TABLEROW\n" +
                    "      \n" +
                    "WHERE  "+ conditions +")";
            Query oQuery = JPA.em().createNativeQuery(statement);
            List result = oQuery.getResultList();
            return Long.parseLong(result.get(0).toString());
        }

        public static List getUsersIdByName(String userName, String structureDate){
            String statement =" SELECT FN_ID_USER FROM "
                    + "(SELECT pu.FN_ID_USER, pu.FC_USERNAME FROM PUL_USER pu INNER JOIN PORTALUNICO.PUL_EMPLOYEE_CSI pec "
                    + "ON pu.FN_ID_USER = pec.FN_USERID WHERE TO_CHAR(pec.FD_STRUCTURE_DATE, 'MM/YYYY') = '"+ structureDate +"') "
                    + "WHERE FC_USERNAME LIKE '%"+userName +"%'";

            List userId =  JPA.em().createNativeQuery(statement)
                    .getResultList();

            return userId;
        }
        
        public static String getOrder(String sSortDir_0, int iSortCol_0) {
            String order;

            switch (iSortCol_0) {
                case 0 : order = "ORDER BY renglon " + sSortDir_0;
                    break;
                case 1 : order = "ORDER BY FN_EMPLOYEE_NUMBER " + sSortDir_0;
                    break;
                case 2 : order = "ORDER BY FC_FIRST_NAME " + sSortDir_0;
                    break;
                case 3 : order = "ORDER BY FC_LAST_NAME " + sSortDir_0;
                    break;
                case 4 : order = "ORDER BY FC_FUNCTION " + sSortDir_0;
                    break;
                case 5 : order = "ORDER BY FC_DEPARTMENT " + sSortDir_0;
                    break;
                case 6 : order = "ORDER BY FN_LEVEL " + sSortDir_0;
                    break;
                case 7 : order = "ORDER BY manager " + sSortDir_0;
                    break;
                default : order = "ORDER BY renglon " + sSortDir_0;
                    break;
            }
            return order;
        }
}
