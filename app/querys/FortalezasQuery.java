package querys;

import java.sql.CallableStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import play.*;
import play.db.jpa.JPA;
import java.util.*;
import javax.persistence.Query;
import json.entities.FortalezasList;
import oracle.jdbc.OracleCallableStatement;
import oracle.jdbc.OracleConnection;
import oracle.jdbc.OracleTypes;
import static utils.Queries.getOracleConnection;

/**
 * Metodo de utiliria para obtener las fortalezas y debilidades
 * @author Rodolfo Miranda -- Qualtop
 */
public class FortalezasQuery {

    /**
     * MEtodo que obtiene las fortalezas o debilidades para las tiendas
     * @param userId
     * @param structureDate
     * @param order
     * @return
     * @throws SQLException 
     */
    public static List<FortalezasList> getFortalezasTienda(int userId, String structureDate, String order) throws SQLException{
        String query = "SELECT FC_QUESTION, FC_DESC_FACTOR, FC_DESC_SUBFACTOR, score \n" +
                        "FROM (\n" +
                        "  SELECT distinct f.FC_DESC_FACTOR, qas.FC_DESC_SUBFACTOR, qas.FC_QUESTION, round(qas.FN_TREE_SCORE_PERCENT, 4) score\n" +
                        "  FROM   PORTALUNICO.PUL_QUESTION_ANSWER_SCORE qas\n" +
                        "  JOIN   PORTALUNICO.PUL_SUBFACTOR sf ON sf.FC_DESC_SUBFACTOR = qas.FC_DESC_SUBFACTOR \n" +
                        "  JOIN   PORTALUNICO.PUL_FACTOR f ON f.FN_ID_FACTOR = sf.FN_ID_FACTOR\n" +
                        "  AND    TO_CHAR(qas.FD_STRUCTURE_DATE, 'MM/YYYY') = '"+ structureDate +"'\n" +
                        "  WHERE  qas.FN_USERID = "+  userId+"\n" +
                        "  ORDER BY score " + order + "\n" +
                        ")\n" +
                        "WHERE rownum BETWEEN 1 AND 10";

        OracleConnection connection = getOracleConnection();

        CallableStatement statement = connection.prepareCall(query);
        statement.execute();

        ResultSet results = ((OracleCallableStatement) statement).getResultSet();

        List<FortalezasList> dataList = new ArrayList();

        while (results .next()) {
            FortalezasList fortaleza =  new FortalezasList();

            fortaleza.setQuestion(results.getString("FC_QUESTION"));
            fortaleza.setFactor(results.getString("FC_DESC_FACTOR"));
            fortaleza.setSubfactor(results.getString("FC_DESC_SUBFACTOR"));
            fortaleza.setScore(results.getFloat("score"));

            dataList.add(fortaleza);
        }
        results.close();
        statement.close();
        return dataList;
    }

    /**
     * MEtodo que obtiene las fortalezas o debilidades a nivel coorporativo
     * @param userId
     * @param structureDate
     * @param order
     * @param type
     * @return
     * @throws SQLException 
     */
    public static List<FortalezasList> getFortalezasCoorp(
        int userId, String structureDate, String order, String type
    ) throws SQLException {
        String field = type.equals("tree") ? "FN_TREE_SCORE_PERCENT" : "FN_DIRECT_SCORE_PERCENT";
        
        String query = "SELECT FC_QUESTION, FC_DESC_FACTOR, FC_DESC_SUBFACTOR, score \n" +
                        "FROM (\n" +
                        "  SELECT distinct f.FC_DESC_FACTOR, qas.FC_DESC_SUBFACTOR, qas.FC_QUESTION, round(qas." + field + ", 4) score\n" +
                        "  FROM   PORTALUNICO.PUL_QUESTION_ANSWER_SCORE qas\n" +
                        "  JOIN   PORTALUNICO.PUL_SUBFACTOR sf ON sf.FC_DESC_SUBFACTOR = qas.FC_DESC_SUBFACTOR \n" +
                        "  JOIN   PORTALUNICO.PUL_FACTOR f ON f.FN_ID_FACTOR = sf.FN_ID_FACTOR\n" +
                        "  AND    TO_CHAR(qas.FD_STRUCTURE_DATE, 'MM/YYYY') = '"+ structureDate +"'\n" +
                        "  AND   f.FN_ID_SURVEY in (select s.FN_ID_SURVEY from pul_survey s where TO_CHAR(s.FD_SURVEY_DATE, 'MM/YYYY')= '"+ structureDate +"')\n" +
                        "  WHERE  qas.FN_USERID = "+  userId+"\n" +
                        "  ORDER BY score " + order + ", FC_QUESTION ASC\n" +
                        ")\n" +
                        "WHERE rownum BETWEEN 1 AND 10";

        OracleConnection connection = getOracleConnection();

        CallableStatement statement = connection.prepareCall(query);
        statement.execute();

        ResultSet results = ((OracleCallableStatement) statement).getResultSet();

        List<FortalezasList> dataList = new ArrayList();

        while (results .next()) {
            FortalezasList fortaleza =  new FortalezasList();

            fortaleza.setQuestion(results.getString("FC_QUESTION"));
            fortaleza.setFactor(results.getString("FC_DESC_FACTOR"));
            fortaleza.setSubfactor(results.getString("FC_DESC_SUBFACTOR"));
            fortaleza.setScore(results.getFloat("score"));

            dataList.add(fortaleza);
        }
        results.close();
        statement.close();
        return dataList;
    }
}
