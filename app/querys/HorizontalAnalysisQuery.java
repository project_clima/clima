package querys;

import java.sql.ResultSet;
import java.sql.SQLException;
import play.*;
import play.db.jpa.JPA;
import java.util.*;
import javax.persistence.Query;
import oracle.jdbc.OracleConnection;
import oracle.jdbc.OracleTypes;
import static utils.Queries.getOracleConnection;
import utils.PagingResult;

/**
 * Clase de utileria para el manejo del Analisis Horizontal
 * @author Rodolfo Miranda -- Qualtop
 */
public class HorizontalAnalysisQuery {
    
    /**
     * Metodo para obtener los gerentes por ubicacion y fecha de estructura
     * @param date
     * @return 
     */
    public static List getByStoreManager(String date) {
            String statement =
                    " select tabla.FC_DIVISION val,'Director ' || tabla.FC_DIVISION nivel" +
                    " from(Select ec.FC_DIVISION" +
                    " FROM PUL_EMPLOYEE_CLIMA ec, PUL_SURVEY_EMPLOYEE se" +
                    " WHERE ec.FN_ID_EMPLOYEE_CLIMA = se.FN_ID_EMPLOYEE_CLIMA" +
                    " AND FC_FUNCTION like  '%Director Almacen%'" +
                    " AND   se.FN_ID_SURVEY in (" +
                    //listo:
                    " select DISTINCT FN_ID_SURVEY FROM PUL_SURVEY_EMPLOYEE s,PUL_EMPLOYEE_CLIMA m where  " +
                    " m.FN_ID_EMPLOYEE_CLIMA = s.FN_ID_EMPLOYEE_CLIMA AND" +
                    " TO_CHAR(m.FD_STRUCTURE_DATE, 'mm/yyyy') = '" + date + "'" +
                    " )" +
                    " GROUP BY  FC_DIVISION" +
                    " ) tabla";

                    Query oQuery = JPA.em().createNativeQuery(statement);
                    return oQuery.getResultList();
    }

    /**
     * MEtodo para obtener el director por direccion. Filtros fecha de estrucura,
     * area y texto de busqueda
     * @param area
     * @param text
     * @param date
     * @return 
     */
    public static List getByDirectorOrDireccion(String area, String text, String date) {
            String statement =
                    " select nivel,clima,lidereazgo,ecuenstas from (" +
                    " select (SELECT" +
                      " ltrim(FC_SUB_DIV, '0')" +
                    " FROM PORTALUNICO.PUL_STORE" +
                    " WHERE FC_DESC_STORE=tabla.FC_DIVISION) area,'" + text + " ' || tabla.FC_DIVISION nivel," +
                    " FN_SCORE_BY_DIVISION_EMP('" + date + "',tabla.FC_DIVISION)  clima," +
                    " FN_FACTOR_BY_DIVISION_EMP('" + date + "',tabla.FC_DIVISION,5) lidereazgo," +
                    " (    select sum(FN_ANSWERED) Contestados" +
                          " from(select tableq.FN_LEVEL," +
                                " tableq.FN_USERID," +
                                " tableq.FN_MANAGER," +
                                " SE.FN_ID_EMPLOYEE_CLIMA," +
                                " SE.FN_PLAN," +
                                " SE.FN_ANSWERED" +
                          " from   PORTALUNICO.PUL_SURVEY_EMPLOYEE SE," +
                              " (select FN_ID_EMPLOYEE_CLIMA," +
                                      " FN_LEVEL," +
                                      " FN_USERID," +
                                      " FC_LAST_NAME," +
                                      " FN_MANAGER" +
                               " from PORTALUNICO.PUL_EMPLOYEE_CLIMA" +
                               " WHERE FC_DIVISION = tabla.FC_DIVISION" +
                               " ) tableq" +
                         " WHERE SE.FN_ID_EMPLOYEE_CLIMA = tableq.FN_ID_EMPLOYEE_CLIMA)) ecuenstas" +
                    " from(" +
                    " Select ec.FC_DIVISION" +
                    " FROM PUL_EMPLOYEE_CLIMA ec, PUL_SURVEY_EMPLOYEE se" +
                    " WHERE ec.FN_ID_EMPLOYEE_CLIMA = se.FN_ID_EMPLOYEE_CLIMA" +
                    " AND FC_FUNCTION like  '%" + text + " Almacen%'" +
                    " AND   se.FN_ID_SURVEY in (" +
                    //listo:
                            " select DISTINCT FN_ID_SURVEY FROM PUL_SURVEY_EMPLOYEE s,PUL_EMPLOYEE_CLIMA m where  " +
                            " m.FN_ID_EMPLOYEE_CLIMA = s.FN_ID_EMPLOYEE_CLIMA AND" +
                            " TO_CHAR(m.FD_STRUCTURE_DATE, 'mm/yyyy') = '" + date + "'" +
                    " )" +
                    " GROUP BY  FC_DIVISION) tabla) tabla2 where tabla2.area = '" + area + "'";

                    Query oQuery = JPA.em().createNativeQuery(statement);
                    return oQuery.getResultList();
    }

    /**
     * MEtodo para obtener las areas por empleado
     * @return 
     */
    public static List getByOperationsManager() {
            String statement =
                    " select ec.FC_AREA AreaId,'Zona '||ec.FC_AREA Area" +
                    " FROM PUL_EMPLOYEE_CLIMA ec, PUL_SURVEY_EMPLOYEE se" +
                    " WHERE ec.FN_ID_EMPLOYEE_CLIMA = se.FN_ID_EMPLOYEE_CLIMA" +
                    " AND ec.FC_AREA is not null AND ec.FC_AREA in ('1A'," +
                    " '1B','2','3','4','5','6','7')" +
                    " GROUP BY ec.FC_AREA" +
                    " ORDER BY 1";

            Query oQuery = JPA.em().createNativeQuery(statement);
            return oQuery.getResultList();
    }

    /**
     * MEtodo para obtener las regiones por area y ubicacion
     * @return 
     */
    public static List getComboRegion() {
            String statement =
                    " select 'sur','Gerencia Centros Comerciales Sur' from dual" +
                    " union" +
                    " select 'norte','Gerencia Centros Comerciales Norte' from dual" +
                    " union" +
                    " select 'DIRECCION-'||ec.FC_AREA AreaId,'Dirección Operaciones Zona '||ec.FC_AREA Area" +
                    " FROM PUL_EMPLOYEE_CLIMA ec, PUL_SURVEY_EMPLOYEE se" +
                    " WHERE ec.FN_ID_EMPLOYEE_CLIMA = se.FN_ID_EMPLOYEE_CLIMA" +
                    " AND ec.FC_AREA is not null" +
                    " GROUP BY ec.FC_AREA" +
                    " union" +
                    " select 'DIRECTOR-'||ec.FC_AREA AreaId,'Director Operaciones Zona '||ec.FC_AREA Area" +
                    " FROM PUL_EMPLOYEE_CLIMA ec, PUL_SURVEY_EMPLOYEE se" +
                    " WHERE ec.FN_ID_EMPLOYEE_CLIMA = se.FN_ID_EMPLOYEE_CLIMA" +
                    " AND ec.FC_AREA is not null" +
                    " GROUP BY ec.FC_AREA";

                    Query oQuery = JPA.em().createNativeQuery(statement);
                    return oQuery.getResultList();
    }

    /**
     * Metodo para obtener los gerentes por fecha de estructura
     * @param date
     * @return 
     */
    public static List getByManagement(String date) {
            String statement =
                    " select tabla.FC_DIVISION val,'Gerente ' || tabla.FC_DIVISION nivel" +
                    " from(Select ec.FC_DIVISION" +
                    " FROM PUL_EMPLOYEE_CLIMA ec, PUL_SURVEY_EMPLOYEE se" +
                    " WHERE ec.FN_ID_EMPLOYEE_CLIMA = se.FN_ID_EMPLOYEE_CLIMA" +
                    " AND FC_FUNCTION like  '%Gerente Almacen%'" +
                    " AND   se.FN_ID_SURVEY in (" +
                            //listo:
                            " select DISTINCT FN_ID_SURVEY FROM PUL_SURVEY_EMPLOYEE s,PUL_EMPLOYEE_CLIMA m where  " +
                            " m.FN_ID_EMPLOYEE_CLIMA = s.FN_ID_EMPLOYEE_CLIMA AND" +
                            " TO_CHAR(m.FD_STRUCTURE_DATE, 'mm/yyyy') = '" + date + "'" +
                    " )" +
                    " GROUP BY  FC_DIVISION" +
                    " ) tabla";

            Query oQuery = JPA.em().createNativeQuery(statement);
            return oQuery.getResultList();
    }

    /**
     * MEtodo para obtener las ubicaciones y calificaciones
     * @param date
     * @return 
     */
    public static List getTableMalls(String date) {
            String statement =
                    " select tabla.FC_DIVISION nivel," +
                    " FN_SCORE_BY_DIVISION_EMP('" + date + "',tabla.FC_DIVISION)  clima," +
                    " FN_FACTOR_BY_DIVISION_EMP('" + date + "',tabla.FC_DIVISION,5) lidereazgo," +
                    " (    select sum(FN_ANSWERED) Contestados" +
                        " from" +
                        " (" +
                         " select tableq.FN_LEVEL," +
                                " tableq.FN_USERID," +
                                " tableq.FN_MANAGER," +
                                " SE.FN_ID_EMPLOYEE_CLIMA," +
                                " SE.FN_PLAN," +
                                " SE.FN_ANSWERED" +
                         " from   PORTALUNICO.PUL_SURVEY_EMPLOYEE SE," +
                              " (select FN_ID_EMPLOYEE_CLIMA," +
                                      " FN_LEVEL," +
                                      " FN_USERID," +
                                      " FC_LAST_NAME," +
                                      " FN_MANAGER" +
                               " from PORTALUNICO.PUL_EMPLOYEE_CLIMA" +
                               " WHERE FC_DIVISION = tabla.FC_DIVISION" +
                               " ) tableq" +
                         " WHERE SE.FN_ID_EMPLOYEE_CLIMA = tableq.FN_ID_EMPLOYEE_CLIMA)) ecuenstas" +
                    " from" +
                    " (" +
                    " Select ec.FC_DIVISION" +
                    " FROM PUL_EMPLOYEE_CLIMA ec, PUL_SURVEY_EMPLOYEE se" +
                    " WHERE ec.FN_ID_EMPLOYEE_CLIMA = se.FN_ID_EMPLOYEE_CLIMA" +
                    " AND   ec.FC_AREA in (" +
                            " select ec.FC_AREA AreaId" +
                            " FROM PUL_EMPLOYEE_CLIMA ec, PUL_SURVEY_EMPLOYEE se" +
                            " WHERE ec.FN_ID_EMPLOYEE_CLIMA = se.FN_ID_EMPLOYEE_CLIMA" +
                            //listo:
                            " AND   se.FN_ID_SURVEY in (" +
                                            " select DISTINCT FN_ID_SURVEY FROM PUL_SURVEY_EMPLOYEE s,PUL_EMPLOYEE_CLIMA m where  " +
                                            " m.FN_ID_EMPLOYEE_CLIMA = s.FN_ID_EMPLOYEE_CLIMA AND" +
                                            " TO_CHAR(m.FD_STRUCTURE_DATE, 'mm/yyyy') = '" + date + "'" +
                            " ) AND" +
                            " ec.FC_AREA is not null" +
                            " AND ec.FC_DIVISION like '%Adcon%'" +
                            " GROUP BY ec.FC_AREA" +
                    " )" +
                    " AND   se.FN_ID_SURVEY in (" +
                    //listo:
                            " select DISTINCT FN_ID_SURVEY FROM PUL_SURVEY_EMPLOYEE s,PUL_EMPLOYEE_CLIMA m where  " +
                            " m.FN_ID_EMPLOYEE_CLIMA = s.FN_ID_EMPLOYEE_CLIMA AND" +
                            " TO_CHAR(m.FD_STRUCTURE_DATE, 'mm/yyyy') = '" + date + "'" +
                    " )" +
                    " GROUP BY  FC_DIVISION" +
                    " ) tabla";

            Query oQuery = JPA.em().createNativeQuery(statement);
            return oQuery.getResultList();
    }

    /**
     * Metodo para obtener las diferentes areas por gerentes
     * @param area
     * @param date
     * @return 
     */
    public static List getTableOperationManager(String area, String date) {
            String statement =
                    " select 'Director Operaciones Zona ' || area,sum(clima),sum(lidereazgo),sum(ecuenstas) from ( " +
                    " select  tabla.FC_AREA area,tabla.FC_DIVISION nivel," +
                    " FN_SCORE_BY_DIVISION_EMP('" + date + "',tabla.FC_DIVISION)  clima," +
                    " FN_FACTOR_BY_DIVISION_EMP('" + date + "',tabla.FC_DIVISION,5) lidereazgo," +
                    " (select sum(FN_ANSWERED) Contestados" +
                            " from(select tableq.FN_LEVEL," +
                                " tableq.FN_USERID," +
                                " tableq.FN_MANAGER," +
                                " SE.FN_ID_EMPLOYEE_CLIMA," +
                                " SE.FN_PLAN," +
                                " SE.FN_ANSWERED" +
                         " from   PORTALUNICO.PUL_SURVEY_EMPLOYEE SE," +
                              " (select FN_ID_EMPLOYEE_CLIMA," +
                                      " FN_LEVEL," +
                                      " FN_USERID," +
                                      " FC_LAST_NAME," +
                                      " FN_MANAGER" +
                               " from PORTALUNICO.PUL_EMPLOYEE_CLIMA" +
                               " WHERE FC_DIVISION = tabla.FC_DIVISION" +
                               " ) tableq" +
                         " WHERE SE.FN_ID_EMPLOYEE_CLIMA = tableq.FN_ID_EMPLOYEE_CLIMA)) ecuenstas" +
                    " from(Select ec.FC_AREA,ec.FC_DIVISION" +
                    " FROM PUL_EMPLOYEE_CLIMA ec, PUL_SURVEY_EMPLOYEE se" +
                    " WHERE ec.FN_ID_EMPLOYEE_CLIMA = se.FN_ID_EMPLOYEE_CLIMA" +
                    " AND ec.FC_AREA  in('" + area + "')" +
                    " AND se.FN_ID_SURVEY in (" +
                    //listo:
                            " select DISTINCT FN_ID_SURVEY FROM PUL_SURVEY_EMPLOYEE s,PUL_EMPLOYEE_CLIMA m where  " +
                            " m.FN_ID_EMPLOYEE_CLIMA = s.FN_ID_EMPLOYEE_CLIMA AND" +
                            " TO_CHAR(m.FD_STRUCTURE_DATE, 'mm/yyyy') = '" + date + "'" +
                    " )" +
                    " GROUP BY  FC_DIVISION,ec.FC_AREA" +
                    " ) tabla)tablac GROUP BY  tablac.area";

                    Query oQuery = JPA.em().createNativeQuery(statement);
                    return oQuery.getResultList();
    }

    /**
     * MEtodo para obtener la tabla principal del analisis horizontal
     * @param date
     * @return 
     */
    public static List getDefaultTable(String date) {
        String statement = "SELECT * FROM (SELECT tbl.PUESTO, \n" +
                            "       ROUND(AVG(tbl.CLIMA), 2) CLIMA,\n" +
                            "       ROUND(AVG(tbl.LIDERAZGO), 2) LIDERAZGO,\n" +
                            "       SUM(tbl.CONTESTADAS) CONTESTADAS\n" +
                            "FROM (SELECT 'Gerentes de Almacen' PUESTO,\n" +
                            "             FN_GET_FINAL_SCORE(ec.FN_USERID, '" + date + "') CLIMA,\n" +
                            "             FN_GET_FINAL_LEADERSHIP_SCORE(ec.FN_USERID, '" + date + "', 'Liderazgo') LIDERAZGO,\n" +
                            "             FN_GET_SURVEY_RESOLVED('" + date + "', ec.FN_USERID) as CONTESTADAS\n" +
                            "      FROM   PUL_EMPLOYEE_CLIMA ec\n" +
                            "      WHERE  TO_CHAR (FD_STRUCTURE_DATE, 'MM/YYYY') = '" + date + "'\n" +
                            "      AND    upper(FC_FUNCTION) LIKE upper('Gerente Almacén%')\n" +
                            "      ) tbl\n" +
                            "GROUP BY tbl.PUESTO      \n" +
                            "UNION\n" +
                            "SELECT tbl.PUESTO, \n" +
                            "       ROUND(AVG(tbl.CLIMA), 2) CLIMA,\n" +
                            "       ROUND(AVG(tbl.LIDERAZGO), 2) LIDERAZGO,\n" +
                            "       SUM(tbl.CONTESTADAS) CONTESTADAS\n" +
                            "FROM (SELECT 'Director de Almacen' PUESTO,\n" +
                            "             FN_GET_FINAL_SCORE(ec.FN_USERID,'" + date + "') CLIMA,\n" +
                            "             FN_GET_FINAL_LEADERSHIP_SCORE(ec.FN_USERID, '" + date + "', 'Liderazgo') LIDERAZGO,\n" +
                            "             FN_GET_SURVEY_RESOLVED('" + date + "', ec.FN_USERID) as CONTESTADAS\n" +
                            "      FROM   PUL_EMPLOYEE_CLIMA ec\n" +
                            "      WHERE  TO_CHAR (FD_STRUCTURE_DATE, 'MM/YYYY') = '" + date + "'\n" +
                            "      AND    (upper(FC_FUNCTION) LIKE upper('%25)')\n" +
                            "      OR      upper(FC_FUNCTION) LIKE upper('%51519080)')\n" +
                            "      OR      upper(FC_FUNCTION) LIKE upper('%51519081)')\n" +
                            "      OR      upper(FC_FUNCTION) LIKE upper('%51519082)'))\n" +
                            "      ) tbl\n" +
                            "GROUP BY tbl.PUESTO\n" +
                            "UNION \n" +
                            "SELECT tbl.PUESTO, \n" +
                            "       ROUND(AVG(tbl.CLIMA), 2) CLIMA,\n" +
                            "       ROUND(AVG(tbl.LIDERAZGO), 2) LIDERAZGO,\n" +
                            "       SUM(tbl.CONTESTADAS) CONTESTADAS\n" +
                            "FROM (SELECT 'Jefaturas' PUESTO,\n" +
                            "             FN_GET_FINAL_SCORE(ec.FN_USERID, '" + date + "') CLIMA,\n" +
                            "             FN_GET_FINAL_LEADERSHIP_SCORE(ec.FN_USERID, '" + date + "', 'Liderazgo') LIDERAZGO,\n" +
                            "             FN_GET_SURVEY_RESOLVED('" + date + "', ec.FN_USERID) as CONTESTADAS\n" +
                            "      FROM   PUL_EMPLOYEE_CLIMA ec\n" +
                            "      WHERE  TO_CHAR (FD_STRUCTURE_DATE, 'MM/YYYY') = '" + date + "'\n" +
                            "      AND    upper(FC_FUNCTION) LIKE upper('Jefe%')\n" +
                            "      ) tbl\n" +
                            "GROUP BY tbl.PUESTO) TBL_ANALYSIS ORDER BY CLIMA DESC";

            Query oQuery = JPA.em().createNativeQuery(statement);
            return oQuery.getResultList();
    }

    /**
     * MEtodo para obtener la calificacion por jefe
     * @param par1
     * @param par2
     * @return 
     */
    public static List getByBoss(String par1, String par2) {
            String cond1 = "1 > 0", cond2 = "1 > 0";

            if(par1.length() > 0) {
                    cond1 = "FC_DEPARTMENT   like  '%" + par1 + "%'";
            }

            if(par2.length() > 0) {
                    cond2 = "FC_FUNCTION   like  '%" + par2 + "%'";
            }

            String statement =
                    " SELECT" +
                    " FC_DEPARTMENT," +
                    " (select sumas from(" +
                         " SELECT FN_ID_EMPLOYEE_CLIMA,SUM(FN_TREE_SCORE) sumas" +
                          " FROM PUL_SUBFACTOR_SCORE" +
                          " where FN_ID_EMPLOYEE_CLIMA =ec.FN_ID_EMPLOYEE_CLIMA" +
                          " group by FN_ID_EMPLOYEE_CLIMA" +
                          " )) sumsa," +
                    " (select sumas from(" +
                          " SELECT FN_ID_EMPLOYEE_CLIMA,SUM(FN_TREE_SCORE) sumas" +
                          " FROM PUL_SUBFACTOR_SCORE" +
                          " where FN_ID_EMPLOYEE_CLIMA =ec.FN_ID_EMPLOYEE_CLIMA" +
                          " AND FN_ID_FACTOR = 5" +
                          " group by FN_ID_EMPLOYEE_CLIMA" +
                          " )) sumsa2," +
                          " (    select sum(FN_ANSWERED) Contestados" +
                    " from(select tableq.FN_LEVEL," +
                            " tableq.FN_USERID," +
                            " tableq.FN_MANAGER," +
                            " SE.FN_ID_EMPLOYEE_CLIMA," +
                            " SE.FN_PLAN," +
                            " SE.FN_ANSWERED" +
                    " from   PORTALUNICO.PUL_SURVEY_EMPLOYEE SE," +
                          " (select s.FN_ID_EMPLOYEE_CLIMA," +
                                  " s.FN_LEVEL," +
                                  " s.FN_USERID," +
                                  " s.FC_LAST_NAME," +
                                  " s.FN_MANAGER" +
                           " from PORTALUNICO.PUL_EMPLOYEE_CLIMA s" +
                           " WHERE s.FC_DIVISION = ec.FC_DIVISION" +
                           " ) tableq" +
                     " WHERE SE.FN_ID_EMPLOYEE_CLIMA = tableq.FN_ID_EMPLOYEE_CLIMA)) ecuenstas" +
                    " FROM PORTALUNICO.PUL_EMPLOYEE_CLIMA ec" +
                    " WHERE " + cond1 + " AND " + cond2;

            Query oQuery = JPA.em().createNativeQuery(statement);
            return oQuery.getResultList();
    }

    /**
     * Metodo para obtener las calificaciones por ubicacion-gerente
     * @param area
     * @param date
     * @return 
     */
    public static List getTableOperationManagerAll(String area, String date) {
            String statement =
                    " select nivel,clima,lidereazgo,ecuenstas from (" +
                    " select (SELECT" +
                      " ltrim(FC_SUB_DIV, '0')" +
                    " FROM PORTALUNICO.PUL_STORE" +
                    " WHERE FC_DESC_STORE=tabla.FC_DIVISION) area,'Director ' || tabla.FC_DIVISION nivel," +
                    " FN_SCORE_BY_DIVISION_EMP('" + date + "',tabla.FC_DIVISION)  clima," +
                    " FN_FACTOR_BY_DIVISION_EMP('" + date + "',tabla.FC_DIVISION,5) lidereazgo," +
                    " (    select sum(FN_ANSWERED) Contestados" +
                        " from" +
                        " (" +
                         " select tableq.FN_LEVEL," +
                                " tableq.FN_USERID," +
                                " tableq.FN_MANAGER," +
                                " SE.FN_ID_EMPLOYEE_CLIMA," +
                                " SE.FN_PLAN," +
                                " SE.FN_ANSWERED" +
                         " from   PORTALUNICO.PUL_SURVEY_EMPLOYEE SE," +
                              " (select FN_ID_EMPLOYEE_CLIMA," +
                                      " FN_LEVEL," +
                                      " FN_USERID," +
                                      " FC_LAST_NAME," +
                                      " FN_MANAGER" +
                               " from PORTALUNICO.PUL_EMPLOYEE_CLIMA" +
                               " WHERE FC_DIVISION = tabla.FC_DIVISION" +
                               " ) tableq" +
                         " WHERE SE.FN_ID_EMPLOYEE_CLIMA = tableq.FN_ID_EMPLOYEE_CLIMA)) ecuenstas" +
                    " from" +
                    " (" +
                    " Select ec.FC_DIVISION" +
                    " FROM PUL_EMPLOYEE_CLIMA ec, PUL_SURVEY_EMPLOYEE se" +
                    " WHERE ec.FN_ID_EMPLOYEE_CLIMA = se.FN_ID_EMPLOYEE_CLIMA" +
                    " AND FC_FUNCTION like  '%Director Almacen%'" +
                    " AND   se.FN_ID_SURVEY in (" +
                    //listo:
                            " select DISTINCT FN_ID_SURVEY FROM PUL_SURVEY_EMPLOYEE s,PUL_EMPLOYEE_CLIMA m where  " +
                            " m.FN_ID_EMPLOYEE_CLIMA = s.FN_ID_EMPLOYEE_CLIMA AND" +
                            " TO_CHAR(m.FD_STRUCTURE_DATE, 'mm/yyyy') = '" + date + "'" +
                    " )" +
                    " GROUP BY  FC_DIVISION" +
                    " ) tabla" +
                    " ) tabla2 where tabla2.area = '" + area + "'" +
                    " union" +
                    " select nivel,clima,lidereazgo,ecuenstas from (" +
                    " select (SELECT" +
                      " ltrim(FC_SUB_DIV, '0')" +
                    " FROM PORTALUNICO.PUL_STORE" +
                    " WHERE FC_DESC_STORE=tabla.FC_DIVISION) area,'Gerente ' || tabla.FC_DIVISION nivel," +
                    " FN_SCORE_BY_DIVISION_EMP('" + date + "',tabla.FC_DIVISION)  clima," +
                    " FN_FACTOR_BY_DIVISION_EMP('" + date + "',tabla.FC_DIVISION,5) lidereazgo," +
                    " (    select sum(FN_ANSWERED) Contestados" +
                        " from" +
                        " (" +
                         " select tableq.FN_LEVEL," +
                                " tableq.FN_USERID," +
                                " tableq.FN_MANAGER," +
                                " SE.FN_ID_EMPLOYEE_CLIMA," +
                                " SE.FN_PLAN," +
                                " SE.FN_ANSWERED" +
                         " from   PORTALUNICO.PUL_SURVEY_EMPLOYEE SE," +
                              " (select FN_ID_EMPLOYEE_CLIMA," +
                                      " FN_LEVEL," +
                                      " FN_USERID," +
                                      " FC_LAST_NAME," +
                                      " FN_MANAGER" +
                               " from PORTALUNICO.PUL_EMPLOYEE_CLIMA" +
                               " WHERE FC_DIVISION = tabla.FC_DIVISION" +
                               " ) tableq" +
                         " WHERE SE.FN_ID_EMPLOYEE_CLIMA = tableq.FN_ID_EMPLOYEE_CLIMA)) ecuenstas" +
                    " from" +
                    " (" +
                    " Select ec.FC_DIVISION" +
                    " FROM PUL_EMPLOYEE_CLIMA ec, PUL_SURVEY_EMPLOYEE se" +
                    " WHERE ec.FN_ID_EMPLOYEE_CLIMA = se.FN_ID_EMPLOYEE_CLIMA" +
                    " AND FC_FUNCTION like  '%Gerente Almacen%'" +
                    //listo:
                    " AND   se.FN_ID_SURVEY in (" +
                            " select DISTINCT FN_ID_SURVEY FROM PUL_SURVEY_EMPLOYEE s,PUL_EMPLOYEE_CLIMA m where  " +
                            " m.FN_ID_EMPLOYEE_CLIMA = s.FN_ID_EMPLOYEE_CLIMA AND" +
                            " TO_CHAR(m.FD_STRUCTURE_DATE, 'mm/yyyy') = '" + date + "'" +
                    " )" +
                    " GROUP BY  FC_DIVISION" +
                    " ) tabla" +
                    " ) tabla2 where tabla2.area = '" + area + "'";

            Query oQuery = JPA.em().createNativeQuery(statement);
            return oQuery.getResultList();
    }

    /**
     * Metodo paraobtener los resultados por area
     * @param area
     * @return 
     */
    public static List getTableByArea(String area) {
            String statement =
                    " select 'Dirección Operaciones Zona  ' ||area,sum(organizacional),sum(desarrolloCrecimiento)," +
                    " sum(clientes),sum(trabajoEquipo),sum(lidereazgo) from ( select  tabla.FC_AREA area,tabla.FC_DIVISION direccionOperacion," +
                    " FN_FACTOR_BY_DIVISION((select to_char(extract(year from sysdate)) from dual),tabla.FC_DIVISION,1) organizacional," +
                    " FN_FACTOR_BY_DIVISION((select to_char(extract(year from sysdate)) from dual),tabla.FC_DIVISION,2) desarrolloCrecimiento," +
                    " FN_FACTOR_BY_DIVISION((select to_char(extract(year from sysdate)) from dual),tabla.FC_DIVISION,3) clientes," +
                    " FN_FACTOR_BY_DIVISION((select to_char(extract(year from sysdate)) from dual),tabla.FC_DIVISION,4) trabajoEquipo," +
                    " FN_FACTOR_BY_DIVISION((select to_char(extract(year from sysdate)) from dual),tabla.FC_DIVISION,5) lidereazgo" +
                    " from(Select ec.FC_AREA,ec.FC_DIVISION" +
                    " FROM PUL_EMPLOYEE_CLIMA ec, PUL_SURVEY_EMPLOYEE se" +
                    " WHERE ec.FN_ID_EMPLOYEE_CLIMA = se.FN_ID_EMPLOYEE_CLIMA" +
                    " AND ec.FC_AREA  in(" +
                    " select ec.FC_AREA AreaId" +
                    " FROM PUL_EMPLOYEE_CLIMA ec, PUL_SURVEY_EMPLOYEE se" +
                    " WHERE ec.FN_ID_EMPLOYEE_CLIMA = se.FN_ID_EMPLOYEE_CLIMA" +
                    " AND   se.FN_ID_SURVEY =1" +
                    " AND ec.FC_AREA is not null" +
                    " GROUP BY ec.FC_AREA" +
                    " )" +
                    " AND se.FN_ID_SURVEY in (" +
                            " select FN_ID_SURVEY FROM PUL_SURVEY" +
                            " where extract(year from FD_DATE_INITIAL" +
                            " )=(select to_char(extract(year from sysdate)) from dual)" +
                    " )" +
                    " GROUP BY  FC_DIVISION,ec.FC_AREA" +
                    " ) tabla) tablac where area in('" + area + "')" +
                    " GROUP BY  tablac.area";

            Query oQuery = JPA.em().createNativeQuery(statement);
            return oQuery.getResultList();
    }

    /**
     * MEtodo para obtener el analisis por jefes
     * @param zone
     * @param date
     * @return 
     */
    public static List getBossTable(String zone, String date) {
            String statement =
                    " select fc_department,sum(t.clima),sum(t.liderazgo),sum(t.answ) from(" +
                    " select e.fc_department,tabla.clima,tabla1.liderazgo,tabla.answ" +
                       " from PUL_EMPLOYEE_CLIMA e," +
                    " (SELECT" +
                      " SS.FN_ID_EMPLOYEE_CLIMA,AVG (FN_TREE_SCORE) clima, SUM(FN_ANSWERED) answ" +
                    " FROM PORTALUNICO.PUL_EMPLOYEE_CLIMA EC,PORTALUNICO.PUL_STORE,PUL_SUBFACTOR_SCORE SS,PUL_SURVEY_EMPLOYEE SE" +
                    " where FC_DESC_STORE=FC_DIVISION" +
                    " AND EC.FN_ID_EMPLOYEE_CLIMA =SS.FN_ID_EMPLOYEE_CLIMA" +
                    " and ltrim(FC_SUB_DIV, '0') = '" + zone + "'" +
                      " and SE .FN_ID_EMPLOYEE_CLIMA = EC.FN_ID_EMPLOYEE_CLIMA" +
                    " group by SS.FN_ID_EMPLOYEE_CLIMA) tabla," +
                    " (SELECT" +
                      " SS.FN_ID_EMPLOYEE_CLIMA,AVG (FN_TREE_SCORE) liderazgo" +
                    " FROM PORTALUNICO.PUL_EMPLOYEE_CLIMA EC,PORTALUNICO.PUL_STORE,PUL_SUBFACTOR_SCORE SS,PUL_SURVEY_EMPLOYEE SE" +
                    " where FC_DESC_STORE=FC_DIVISION" +
                    " AND EC.FN_ID_EMPLOYEE_CLIMA =SS.FN_ID_EMPLOYEE_CLIMA" +
                    " AND FN_ID_FACTOR ='5'" +
                    " and ltrim(FC_SUB_DIV, '0') = '" + zone + "'" +
                    " and SE.FN_ID_EMPLOYEE_CLIMA = EC.FN_ID_EMPLOYEE_CLIMA" +
                    " group by SS.FN_ID_EMPLOYEE_CLIMA) tabla1" +
                    " where tabla.FN_ID_EMPLOYEE_CLIMA = e.FN_ID_EMPLOYEE_CLIMA" +
                    " and tabla1.FN_ID_EMPLOYEE_CLIMA = e.FN_ID_EMPLOYEE_CLIMA) t group by t.fc_department";

                    Query oQuery = JPA.em().createNativeQuery(statement);
                    return oQuery.getResultList();
    }
    
    /**
     * MEtodo para obtener la tabla de analisis default
     * @param date
     * @param zone
     * @param position
     * @return 
     */
    public static List getAnalysisTable(String date, String zone, String position) {
        String query = "SELECT FC_DIVISION || ' - ' || FC_TITLE  PUESTO,\n" +
                        "       PORTALUNICO.FN_GET_FINAL_SCORE(ec.FN_USERID, '" + date + "') CLIMA,\n" +
                        "       PORTALUNICO.FN_GET_FINAL_LEADERSHIP_SCORE(ec.FN_USERID, '" + date + "', 'Liderazgo') LIDERAZGO,\n" +
                        "       PORTALUNICO.FN_GET_SURVEY_RESOLVED('" + date + "', ec.FN_USERID) as CONTESTADAS\n" +
                        "FROM   PUL_EMPLOYEE_CLIMA ec\n" +
                        "JOIN   PUL_SURVEY_EMPLOYEE se on se.FN_ID_EMPLOYEE_CLIMA = ec.FN_ID_EMPLOYEE_CLIMA\n" +
                        "JOIN   PUL_STORE st on fn_compare_string(st.FC_DESC_STORE) = fn_compare_string(ec.FC_DIVISION) AND TO_CHAR(st.FD_STRUCTURE_DATE, 'MM/YYYY') =  TO_CHAR(ec.FD_STRUCTURE_DATE, 'MM/YYYY')\n" +
                        "WHERE  TO_CHAR (ec.FD_STRUCTURE_DATE, 'MM/YYYY') = '" + date + "'\n" +
                        "AND    se.FN_PLAN <> 0\n" +
                        "AND    upper(FC_TITLE) like upper('%'||('" + position + "')||'%')\n" +
                        "AND    PORTALUNICO.FN_GET_SURVEY_RESOLVED('" + date + "', ec.FN_USERID) > 2\n" + 
                        "ORDER BY 2 desc";

        Query oQuery = JPA.em().createNativeQuery(query);
        return oQuery.getResultList();
    }
}
