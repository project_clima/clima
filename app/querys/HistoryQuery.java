package querys;

import java.sql.CallableStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import play.*;
import play.db.jpa.JPA;
import java.util.*;
import javax.persistence.Query;
import json.entities.HistoricoList;
import oracle.jdbc.OracleCallableStatement;
import oracle.jdbc.OracleConnection;
import oracle.jdbc.OracleTypes;
import org.springframework.jdbc.support.nativejdbc.C3P0NativeJdbcExtractor;
import play.db.DB;
import static utils.Queries.getOracleConnection;
import utils.PagingResult;
import static utils.Survey.templates;

/**
 * Clase de utilieria para ejecucion de querys del reporte historico
 * @author Rodolfo Miranda -- Qualtop
 */
public class HistoryQuery {

    /**
     * MEtodo para obtener los resultados del reporte historico de clima
     * @param option
     * @param twoYearsAgo
     * @return
     * @throws SQLException 
     */
   public static LinkedHashMap<String, HistoricoList> getEnvironment(String option, String twoYearsAgo) throws SQLException {
        OracleConnection connection = getOracleConnection();
        CallableStatement statement = connection.prepareCall("BEGIN PORTALUNICO.PR_SCORE_BY_YEAR(?, ?); END;");
        
        statement.setString(1, option);
        statement.registerOutParameter(2, OracleTypes.CURSOR);
        statement.execute();
        
        LinkedHashMap<String, HistoricoList> historicoList = new LinkedHashMap();
        List dataList = new ArrayList();
        ResultSet results = ((OracleCallableStatement)statement).getCursor(2);      
        
        while(results.next()){
             HistoricoList obHistorico= new HistoricoList();
           
             obHistorico.setDisivion(results.getString("FC_DIVISION"));
             
             if(historicoList.containsKey(results.getString("FC_DIVISION"))){
                 HistoricoList ob= historicoList.get(results.getString("FC_DIVISION"));
                 if(twoYearsAgo.equals(results.getString("FN_YEAR"))){
                     ob.setFirstPercent(results.getFloat("FN_PORCENTAJE"));
                     ob.setFirstYear(results.getString("FN_YEAR"));
                 }else{
                     ob.setSecondPercent(results.getFloat("FN_PORCENTAJE"));
                     ob.setSecondYear(results.getString("FN_YEAR"));
                 }   
             }else{
                 if(twoYearsAgo.equals(results.getString("FN_YEAR"))){
                     obHistorico.setFirstPercent(results.getFloat("FN_PORCENTAJE"));
                     obHistorico.setFirstYear(results.getString("FN_YEAR"));
                 }else{
                     obHistorico.setSecondPercent(results.getFloat("FN_PORCENTAJE"));
                     obHistorico.setSecondYear(results.getString("FN_YEAR"));
                 } 
                 historicoList.put(results.getString("FC_DIVISION"), obHistorico);
             }    
        }
        results.close();
        statement.close();
        return historicoList;
    }


/**
 * Metodo para obtener los resultados del reporte historico para liderazgo
 * @param option
 * @param twoYearsAgo
 * @return
 * @throws SQLException 
 */
    public static LinkedHashMap<String, HistoricoList> getLeadership(String option, String twoYearsAgo) throws SQLException {
        OracleConnection connection= getOracleConnection();
        CallableStatement statement= connection.prepareCall("BEGIN PORTALUNICO.PR_SCORE_BY_YEAR_LEADERSHIP(?,?); END;");
        
        statement.setString(1, option);
        statement.registerOutParameter(2, OracleTypes.CURSOR);
        statement.execute();
        
        LinkedHashMap<String, HistoricoList> historicoList = new LinkedHashMap();
        List<HistoricoList> dataList = new ArrayList();
        
        ResultSet results = ((OracleCallableStatement)statement).getCursor(2);
        
        while(results.next()){
            HistoricoList obHistorico= new HistoricoList();
           
             obHistorico.setDisivion(results.getString("FC_DIVISION"));
             
             if(historicoList.containsKey(results.getString("FC_DIVISION"))){
                 HistoricoList ob= historicoList.get(results.getString("FC_DIVISION"));
                 if(twoYearsAgo.equals(results.getString("FN_YEAR"))){
                     ob.setFirstPercent(results.getFloat("FN_PORCENTAJE"));
                     ob.setFirstYear(results.getString("FN_YEAR"));
                 }else{
                     ob.setSecondPercent(results.getFloat("FN_PORCENTAJE"));
                     ob.setSecondYear(results.getString("FN_YEAR"));
                 }   
             }else{
                 if(twoYearsAgo.equals(results.getString("FN_YEAR"))){
                     obHistorico.setFirstPercent(results.getFloat("FN_PORCENTAJE"));
                     obHistorico.setFirstYear(results.getString("FN_YEAR"));
                 }else{
                     obHistorico.setSecondPercent(results.getFloat("FN_PORCENTAJE"));
                     obHistorico.setSecondYear(results.getString("FN_YEAR"));
                 } 
                 historicoList.put(results.getString("FC_DIVISION"), obHistorico);
             } 
        }
        
        results.close();
        statement.close();
        return historicoList;
    }
   
}
