package querys;

import java.io.File;
import java.io.IOException;
import java.math.BigDecimal;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.sql.CallableStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import play.*;
import play.db.jpa.JPA;
import java.util.*;
import javax.persistence.Query;
import json.entities.MailingList;
import oracle.jdbc.OracleCallableStatement;
import oracle.jdbc.OracleConnection;
import oracle.jdbc.OracleTypes;
import play.vfs.VirtualFile;
import utils.Queries;
import static utils.Queries.getOracleConnection;


public class EmailQuery {

    public static String getEmailManager(int userId) {
        String statement = " select  fc_email " +
                            " from pul_employee_clima where FN_USERID = " + userId;

        Query oQuery = JPA.em().createNativeQuery(statement);
        String email;
        
        try {
            email = (String) oQuery.getSingleResult();
        } catch (Exception e) {
            email = "";
        }
        
        return email;
    }

    public static List<MailingList> sendEmails(int survey) throws SQLException {
        Queries query = new Queries();
        OracleConnection connection = query.getOracleConnection();
        CallableStatement statement = connection.prepareCall("BEGIN PORTALUNICO.PR_MAILING_LIST(?, ?); END;");
        
        statement.setInt(1, survey);
        statement.registerOutParameter(2, OracleTypes.CURSOR);
        statement.execute();

        List<MailingList> dataList = new ArrayList();

        ResultSet results = ((OracleCallableStatement)statement).getCursor(2);

        while (results.next()) {
            MailingList mail = new MailingList();
            mail.setUserId(results.getInt("FN_USERID"));
            mail.setEmail(results.getString("FC_EMAIL"));
            mail.setUserName(results.getString("USERNAME"));
            mail.setPassword(results.getString("FC_PASSWORD"));
            mail.setHumanResources(results.getInt("FN_HUMAN_RESOURCES"));
            mail.setSurveyType(results.getInt("FN_SURVEY_TYPE"));
            dataList.add(mail);
        }

        return dataList;
    }
    
    public static List<MailingList> getEmployees(int surveyType) throws SQLException {
        String query = "SELECT ec.FN_USERID, \n" +
                       "               lower(ec.FC_EMAIL) FC_EMAIL, \n" +
                       "               ec.FC_FIRST_NAME FC_FIRST_NAME, \n" +
                       "               ec.FC_LAST_NAME FC_LAST_NAME, \n" +
                       "               decode(" + surveyType + ", 1,SUBSTR(se.FC_NOMENCLATURA, 0, INSTR(se.FC_NOMENCLATURA, '/')-1) ,se.FC_USERNAME) USERNAME,\n" +
                       "               decode(" + surveyType + ", 1,SUBSTR(se.FC_NOMENCLATURA, INSTR(se.FC_NOMENCLATURA, '/')+1), se.FC_PASSWORD) FC_PASSWORD,\n" +
                       "               ec.FN_HUMAN_RESOURCES,\n" +
                       "               " + surveyType + " AS FN_SURVEY_TYPE,\n" +
                       "               ec.FC_DEPARTMENT FC_DEPARTMENT,\n" +
                       "               ecm.FC_FIRST_NAME || ' ' || ecm.FC_LAST_NAME FC_MANAGER,\n" +
                       "               se.FN_ID_EMPLOYEE_CLIMA FN_ID_EMPLOYEE_CLIMA\n" +
                       "        FROM PUL_EMPLOYEE_CLIMA ec\n" +
                       "        INNER JOIN PUL_SURVEY_EMPLOYEE se ON se.FN_ID_EMPLOYEE_CLIMA = ec.FN_ID_EMPLOYEE_CLIMA\n" +
                       "        INNER JOIN PUL_SURVEY s ON s.FN_ID_SURVEY = se.FN_ID_SURVEY \n" +
                       "        LEFT JOIN PUL_EMPLOYEE_CLIMA ecm ON ecm.FN_USERID = ec.FN_MANAGER \n" +
                       "        WHERE  s.FN_ID_SURVEY_TYPE = " + surveyType + "\n" +
                       "        AND    SE.FN_PLAN <> 0\n" +
                       "        ORDER BY FN_HUMAN_RESOURCES";
        
        OracleConnection connection = getOracleConnection();
        CallableStatement statement = connection.prepareCall(query);
        statement.execute();

        ResultSet results = ((OracleCallableStatement) statement).getResultSet();
        
        List<MailingList> dataList = new ArrayList();
        MailingList mail;
        
        while (results.next()) {
            mail = new MailingList();
            
            mail.setUserId(results.getInt("FN_USERID"));
            mail.setFirstName(results.getString("FC_FIRST_NAME"));
            mail.setLastName(results.getString("FC_LAST_NAME"));
            mail.setEmail(results.getString("FC_EMAIL"));
            mail.setUserName(results.getString("USERNAME"));
            mail.setPassword(results.getString("FC_PASSWORD"));
            mail.setHumanResources(results.getInt("FN_HUMAN_RESOURCES"));
            mail.setSurveyType(results.getInt("FN_SURVEY_TYPE"));
            mail.setDepartment(results.getString("FC_DEPARTMENT"));
            mail.setManager(results.getString("FC_MANAGER"));
            mail.setIdEmployee(results.getInt("FN_ID_EMPLOYEE_CLIMA"));
            
            dataList.add(mail);
        }
        
        return dataList;
    }
    
    public static List<MailingList> getEmployeesTest(int surveyId) throws SQLException {
        String query = "select\n" +
                       "ec.FN_USERID,\n" +
                       "lower(ec.FC_EMAIL) FC_EMAIL,\n" +
                       "ec.FC_FIRST_NAME FC_FIRST_NAME,\n" +
                       "ec.FC_LAST_NAME FC_LAST_NAME,\n" +
                       "decode(su.FN_ID_SURVEY_TYPE, 1,SUBSTR(se.FC_NOMENCLATURA, 0, INSTR(se.FC_NOMENCLATURA, '/')-1) ,se.FC_USERNAME) USERNAME,\n" +
                       "decode(su.FN_ID_SURVEY_TYPE, 1,SUBSTR(se.FC_NOMENCLATURA, INSTR(se.FC_NOMENCLATURA, '/')+1), se.FC_PASSWORD) FC_PASSWORD\n" +
                       "from PUL_EMPLOYEE_CLIMA ec\n" +
                       "INNER JOIN PUL_SURVEY_EMPLOYEE se ON se.FN_ID_EMPLOYEE_CLIMA = ec.FN_ID_EMPLOYEE_CLIMA\n" +
                       "INNER JOIN PUL_SURVEY su ON su.FN_ID_SURVEY = se.FN_ID_SURVEY\n" +
                       "where FN_USERID IN(\n" +
                       "  SELECT FN_USERID\n" +
                       "    FROM (\n" +
                       "      SELECT LEVEL  NIVEL, st.FN_USERID\n" +
                       "      FROM PUL_EMPLOYEE_CLIMA st\n" +
                       "      CONNECT BY PRIOR st.FN_USERID = st.FN_MANAGER\n" +
                       "      START WITH st.FN_USERID = 50092399\n" +
                       "   )\n" +
                       ")\n" +
                        "AND su.FN_ID_SURVEY = " + surveyId;
        
        OracleConnection connection = getOracleConnection();
        CallableStatement statement = connection.prepareCall(query);
        statement.execute();

        ResultSet results = ((OracleCallableStatement) statement).getResultSet();
        
        List<MailingList> dataList = new ArrayList();
        MailingList mail;
        
        while (results.next()) {
            mail = new MailingList();
            
            mail.setUserId(results.getInt("FN_USERID"));
            mail.setFirstName(results.getString("FC_FIRST_NAME"));
            mail.setLastName(results.getString("FC_LAST_NAME"));
            mail.setEmail(results.getString("FC_EMAIL"));
            mail.setUserName(results.getString("USERNAME"));
            mail.setPassword(results.getString("FC_PASSWORD"));
            
            dataList.add(mail);
        }
        
        return dataList;
    }
    
    public static List<MailingList> getEmployeesStoreTest(int surveyId, int zona) throws SQLException {
        String query = "SELECT\n" +
                        "  EC.FN_USERID,\n" +
                        "  EC.FC_DIVISION, \n" +
                        "  EC.FC_TITLE DEMOGRAFICO,\n" +
                        "  decode(1, 1,SUBSTR(se.FC_NOMENCLATURA, 0, INSTR(se.FC_NOMENCLATURA, '/')-1) ,se.FC_USERNAME) USUARIO,\n" +
                        "  decode(1, 1,SUBSTR(se.FC_NOMENCLATURA, INSTR(se.FC_NOMENCLATURA, '/')+1), se.FC_PASSWORD) CONTRASENA,\n" +
                        "  EC.FC_FIRST_NAME||' '||EC.FC_LAST_NAME as FC_MANAGER,\n" +
                        "  SE.FN_PLAN,\n" +
                        "  EC.FC_AREA,\n" +
                        "  ec.FN_HUMAN_RESOURCES\n" +
                        "FROM   PUL_EMPLOYEE_CLIMA ec\n" +
                        "LEFT   JOIN PUL_SURVEY_EMPLOYEE SE ON SE.FN_ID_EMPLOYEE_CLIMA = EC.FN_ID_EMPLOYEE_CLIMA \n" +
                        "WHERE  SE.FN_ID_SURVEY = " + surveyId + "\n" +
                        "AND    SE.FN_PLAN <> 0\n" +
                        "AND   ((select upper(fc_desc_store) from pul_store where fn_id_store = EC.FN_ID_STORE) like upper('%Fábricas Lago de Guadalupe%')\n" +
                        "OR     (select upper(fc_desc_store) from pul_store where fn_id_store = EC.FN_ID_STORE) like upper('%Fábricas Plaza Central%'))\n" +
                        "AND  EXISTS (select PORTALUNICO.FN_COMPARE_STRING(fc_desc_store) from portalunico.pul_store where FC_SUB_DIV= lpad(" + zona + ",2,'0') )";

        OracleConnection connection = getOracleConnection();
        CallableStatement statement = connection.prepareCall(query);
        statement.execute();

        ResultSet results = ((OracleCallableStatement) statement).getResultSet();
        
        List<MailingList> dataList = new ArrayList();
        MailingList mail;
        
        while (results.next()) {
            mail = new MailingList();
            
            mail.setUserId(results.getInt("FN_USERID"));
            mail.setUserName(results.getString("USUARIO"));
            mail.setPassword(results.getString("CONTRASENA"));
            mail.setHumanResources(results.getInt("FN_HUMAN_RESOURCES"));
            mail.setDepartment(results.getString("DEMOGRAFICO"));
            mail.setManager(results.getString("FC_MANAGER"));
            mail.setDivision(results.getString("FC_DIVISION"));
            mail.setPlan(results.getInt("FN_PLAN"));
            dataList.add(mail);
        }
        
        return dataList;
    }
    
    public static List<MailingList> getEmployeesStoreTest(int surveyId, String store, int zona) throws SQLException {
        String query = "SELECT EC.FN_USERID,\n" +
                        "EC.FN_MANAGER,\n" +
                        "EC.FC_TITLE DEMOGRAFICO,\n" +
                        "decode(1, 1,SUBSTR(se.FC_NOMENCLATURA, 0, INSTR(se.FC_NOMENCLATURA, '/')-1) ,se.FC_USERNAME) USUARIO,\n" +
                        "decode(1, 1,SUBSTR(se.FC_NOMENCLATURA, INSTR(se.FC_NOMENCLATURA, '/')+1), se.FC_PASSWORD) CONTRASENA,\n" +
                        "EC.FC_FIRST_NAME||' '||EC.FC_LAST_NAME as FC_MANAGER,\n" +
                        "ec.FC_DIVISION,\n" +
                        "se.FN_PLAN\n" +
                        "FROM PUL_EMPLOYEE_CLIMA ec\n" +
                        "LEFT JOIN PUL_SURVEY_EMPLOYEE SE ON SE.FN_ID_EMPLOYEE_CLIMA = EC.FN_ID_EMPLOYEE_CLIMA\n" +
                        "LEFT JOIN PUL_STORE st ON st.FN_ID_STORE = ec.FN_ID_STORE\n" +
                        "WHERE  SE.FN_ID_SURVEY = " + surveyId + "\n" +
                        "AND    SE.FN_PLAN <> 0\n" +
                        "AND    UPPER(FN_COMPARE_STRING(ec.FC_DIVISION)) like UPPER(FN_COMPARE_STRING('" + store + "'))";

        OracleConnection connection = getOracleConnection();
        CallableStatement statement = connection.prepareCall(query);
        statement.execute();

        ResultSet results = ((OracleCallableStatement) statement).getResultSet();
        
        List<MailingList> dataList = new ArrayList();
        MailingList mail;
        
        while (results.next()) {
            mail = new MailingList();
            
            mail.setUserId(results.getInt("FN_USERID"));
            mail.setUserName(results.getString("USUARIO"));
            mail.setPassword(results.getString("CONTRASENA"));
            mail.setDepartment(results.getString("DEMOGRAFICO"));
            mail.setManager(results.getString("FC_MANAGER"));
            mail.setDivision(results.getString("FC_DIVISION"));
            mail.setPlan(results.getInt("FN_PLAN"));
            dataList.add(mail);
        }
        
        statement.close();
        
        return dataList;
    }
    
    public static MailingList getAreaDirector(int surveyId, int zona) throws SQLException {
        String query = "SELECT\n" +
                        "  EC.FN_USERID,\n" +
                        "  EC.FC_DIVISION,\n" +
                        "  EC.FC_TITLE DEMOGRAFICO,\n" +
                        "  decode(1, 1,SUBSTR(se.FC_NOMENCLATURA, 0, INSTR(se.FC_NOMENCLATURA, '/') - 1) ,se.FC_USERNAME) USUARIO,\n" +
                        "  decode(1, 1,SUBSTR(se.FC_NOMENCLATURA, INSTR(se.FC_NOMENCLATURA, '/') + 1), se.FC_PASSWORD) CONTRASENA,\n" +
                        "  EC.FC_FIRST_NAME||' '||EC.FC_LAST_NAME as FC_MANAGER,\n" +
                        "  FN_PLAN,\n" +
                        "  EC.FC_AREA,\n" +
                        "  ec.FN_HUMAN_RESOURCES\n" +
                        "FROM   PUL_EMPLOYEE_CLIMA ec\n" +
                        "LEFT  JOIN PUL_SURVEY_EMPLOYEE SE ON SE.FN_ID_EMPLOYEE_CLIMA = EC.FN_ID_EMPLOYEE_CLIMA\n" +
                        "WHERE  SE.FN_ID_SURVEY = " + surveyId + "\n" +
                        "AND    SE.FN_PLAN <> 0\n" +
                        "AND    upper(EC.FC_TITLE) like upper('%Director%zona%'||" + zona + "||'%')";
        
        OracleConnection connection = getOracleConnection();
        CallableStatement statement = connection.prepareCall(query);
        statement.execute();

        ResultSet results = ((OracleCallableStatement) statement).getResultSet();
        
        MailingList mail = new MailingList();
        
        while (results.next()) {
            mail.setUserId(results.getInt("FN_USERID"));
            mail.setUserName(results.getString("USUARIO"));
            mail.setPassword(results.getString("CONTRASENA"));
            mail.setHumanResources(results.getInt("FN_HUMAN_RESOURCES"));
            mail.setDepartment(results.getString("DEMOGRAFICO"));
            mail.setManager(results.getString("FC_MANAGER"));
            mail.setDivision(results.getString("FC_DIVISION"));
            mail.setPlan(results.getInt("FN_PLAN"));
        }
        
        return mail;
    }
    
    public static MailingList getAreaDirector(int surveyId, String store, int zona) throws SQLException {
        String query = "SELECT EC.FN_USERID,\n" +
                        "EC.FN_MANAGER,\n" +
                        "EC.FC_TITLE DEMOGRAFICO,\n" +
                        "SUBSTR(se.FC_NOMENCLATURA, 0, INSTR(se.FC_NOMENCLATURA, '/')-1) USUARIO,\n" +
                        "SUBSTR(se.FC_NOMENCLATURA, INSTR(se.FC_NOMENCLATURA, '/')+1)    CONTRASENA,\n" +
                        "EC.FC_FIRST_NAME||' '||EC.FC_LAST_NAME as FC_MANAGER,\n" +
                        "ec.FC_DIVISION,\n" +
                        "se.FN_PLAN\n" +
                        "FROM   PUL_EMPLOYEE_CLIMA ec\n" +
                        "LEFT  JOIN PUL_SURVEY_EMPLOYEE SE ON SE.FN_ID_EMPLOYEE_CLIMA = EC.FN_ID_EMPLOYEE_CLIMA\n" +
                        "WHERE  SE.FN_ID_SURVEY = " + surveyId + " \n" +
                        "AND    SE.FN_PLAN <> 0 \n" +
                        "AND    ec.FN_USERID = (SELECT EC.FN_MANAGER     \n" +
                        "FROM   PUL_EMPLOYEE_CLIMA ec\n" +
                        "LEFT   JOIN PUL_SURVEY_EMPLOYEE SE ON SE.FN_ID_EMPLOYEE_CLIMA = EC.FN_ID_EMPLOYEE_CLIMA \n" +
                        "WHERE  SE.FN_ID_SURVEY = " + surveyId + "\n" +
                        "AND    SE.FN_PLAN <> 0\n" +
                        "AND    UPPER(FN_COMPARE_STRING(ec.FC_DIVISION)) like UPPER(FN_COMPARE_STRING('" + store + "'))\n" +
                        "AND    EC.FN_MANAGER in (SELECT FN_USERID FROM PUL_EMPLOYEE_CLIMA WHERE upper(FC_TITLE) LIKE upper('%Director%zona%') FETCH FIRST 1 ROWS ONLY)\n" +
                        ")";
        
        OracleConnection connection = getOracleConnection();
        CallableStatement statement = connection.prepareCall(query);
        statement.execute();

        ResultSet results = ((OracleCallableStatement) statement).getResultSet();
        
        MailingList mail = new MailingList();
        
        while (results.next()) {
            mail.setUserId(results.getInt("FN_USERID"));
            mail.setUserName(results.getString("USUARIO"));
            mail.setPassword(results.getString("CONTRASENA"));
            mail.setDepartment(results.getString("DEMOGRAFICO"));
            mail.setManager(results.getString("FC_MANAGER"));
            mail.setDivision(results.getString("FC_DIVISION"));
            mail.setPlan(results.getInt("FN_PLAN"));
        }
        
        statement.close();
        
        return mail;
    }
    
    public static List<MailingList> getEmployeesStoreTest(int surveyId) throws SQLException {
        String query = "SELECT\n" +
                        "  ec.FC_AREA,\n" +
                        "  ec.FC_DIVISION,\n" +
                        "  ec.FN_USERID, \n" +
                        "  lower(ec.FC_EMAIL) FC_EMAIL, \n" +
                        "  ec.FC_FIRST_NAME FC_FIRST_NAME, \n" +
                        "  ec.FC_LAST_NAME FC_LAST_NAME, \n" +
                        "  decode(1, 1,SUBSTR(se.FC_NOMENCLATURA, 0, INSTR(se.FC_NOMENCLATURA, '/')-1) ,se.FC_USERNAME) USERNAME,\n" +
                        "  decode(1, 1,SUBSTR(se.FC_NOMENCLATURA, INSTR(se.FC_NOMENCLATURA, '/')+1), se.FC_PASSWORD) FC_PASSWORD,\n" +
                        "  ec.FN_HUMAN_RESOURCES,\n" +
                        "  s.FN_ID_SURVEY_TYPE AS FN_SURVEY_TYPE,\n" +
                        "  ec.FC_DEPARTMENT FC_DEPARTMENT,\n" +
                        "  ecm.FC_FIRST_NAME || ' ' || ecm.FC_LAST_NAME FC_MANAGER,\n" +
                        "  se.FN_ID_EMPLOYEE_CLIMA FN_ID_EMPLOYEE_CLIMA\n" +
                        "FROM PUL_EMPLOYEE_CLIMA ec\n" +
                        "INNER JOIN PUL_SURVEY_EMPLOYEE se ON se.FN_ID_EMPLOYEE_CLIMA = ec.FN_ID_EMPLOYEE_CLIMA\n" +
                        "INNER JOIN PUL_SURVEY s ON s.FN_ID_SURVEY = se.FN_ID_SURVEY \n" +
                        "LEFT JOIN PUL_EMPLOYEE_CLIMA ecm ON ecm.FN_USERID = ec.FN_MANAGER \n" +
                        "WHERE \n" +
                        "  s.FN_ID_SURVEY_TYPE = 1\n" +
                        "  AND    se.FN_PLAN <> 0\n" +
                        "  AND    upper(EC.FC_DIVISION) = upper('Liverpool Zapopan')\n" +
                        "  AND    s.FN_ID_SURVEY = " + surveyId + "\n" +
                        "ORDER BY ec.FC_AREA, ec.FC_DIVISION, FN_HUMAN_RESOURCES";
        
        OracleConnection connection = getOracleConnection();
        CallableStatement statement = connection.prepareCall(query);
        statement.execute();

        ResultSet results = ((OracleCallableStatement) statement).getResultSet();
        
        List<MailingList> dataList = new ArrayList();
        MailingList mail;
        
        while (results.next()) {
            mail = new MailingList();
            
            mail.setUserId(results.getInt("FN_USERID"));
            mail.setFirstName(results.getString("FC_FIRST_NAME"));
            mail.setLastName(results.getString("FC_LAST_NAME"));
            mail.setEmail(results.getString("FC_EMAIL"));
            mail.setUserName(results.getString("USERNAME"));
            mail.setPassword(results.getString("FC_PASSWORD"));
            mail.setHumanResources(results.getInt("FN_HUMAN_RESOURCES"));
            mail.setSurveyType(results.getInt("FN_SURVEY_TYPE"));
            mail.setDepartment(results.getString("FC_DEPARTMENT"));
            mail.setManager(results.getString("FC_MANAGER"));
            mail.setIdEmployee(results.getInt("FN_ID_EMPLOYEE_CLIMA"));
            
            dataList.add(mail);
        }
        
        statement.close();
        
        return dataList;
    }
    
    public static MailingList getEmailStore(String store, Date surveyDate)  throws SQLException {
        SimpleDateFormat sdf = new SimpleDateFormat("MM/yyyy");
        String structureDate = sdf.format(surveyDate);
        
        String query = "SELECT st.FN_HUMAN_RESOURCES HR, st.FN_OTHER other, \n" +
                        "(select FC_EMAIL from PUL_EMPLOYEE_CLIMA where FN_USERID = st.FN_HUMAN_RESOURCES AND TO_CHAR(FD_STRUCTURE_DATE, 'MM/YYYY') = :structureDate) EMAIL,\n" +
                        "(select FC_EMAIL from PUL_EMPLOYEE_CLIMA where FN_USERID = st.FN_OTHER AND TO_CHAR(FD_STRUCTURE_DATE, 'MM/YYYY') = :structureDate) EMAIL_CC,\n" +
                        "(select FC_FIRST_NAME || ' ' || FC_LAST_NAME from PUL_EMPLOYEE_CLIMA where FN_USERID = st.FN_HUMAN_RESOURCES AND TO_CHAR(FD_STRUCTURE_DATE, 'MM/YYYY') = :structureDate) FC_MANAGER,\n" +
                        "st.FC_DESC_STORE FC_DIVISION\n" +
                        "FROM  PUL_STORE st\n" +
                        "WHERE  st.FN_ID_STORE = :store and TO_CHAR(st.FD_STRUCTURE_DATE, 'MM/YYYY') = :structureDate";
        
        OracleConnection connection = getOracleConnection();
        OracleCallableStatement statement = (OracleCallableStatement)connection.prepareCall(query);
        statement.setStringAtName("structureDate", structureDate);
        statement.setStringAtName("store", store);
        statement.execute();

        ResultSet results = ((OracleCallableStatement) statement).getResultSet();
        MailingList mail = new MailingList();
        
        while (results.next()) {
            mail.setHumanResources(results.getInt("HR"));
            mail.setManager(results.getString("FC_MANAGER"));
            mail.setEmail(results.getString("EMAIL"));
            mail.setEmailCC(results.getString("EMAIL_CC"));
            mail.setDivision(results.getString("FC_DIVISION"));
        }
        
        statement.close();
        
        return mail;        
    }
    
    public static List<String> getStoreList() throws IOException, SQLException {
        String query;
        
        VirtualFile sqlFile = VirtualFile.fromRelativePath("sql/stores.sql");
        File fileSql = sqlFile.getRealFile();
        
        query = new String(Files.readAllBytes(Paths.get(fileSql.getPath())));
        
        OracleConnection connection = getOracleConnection();
        CallableStatement statement = connection.prepareCall(query);
        statement.execute();

        ResultSet results = ((OracleCallableStatement) statement).getResultSet();
        List<String> stores = new ArrayList();
        
        while (results.next()) {
            stores.add(results.getString("FC_DESC_STORE"));
        }
        
        statement.close();
        
        return stores;
    }
}
