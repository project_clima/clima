package concurrent.csi;

import java.util.List;

import javax.persistence.EntityManager;

import play.db.jpa.JPA;

public class SaveThread<T> implements Runnable {

	private List<T> results;
	
	
	public SaveThread(List<T> results) {
		 this.results = results;
	 }
	
	
	public void run() {
		EntityManager em = JPA.createEntityManager();
		if(em.getTransaction().isActive()) {
			em.getTransaction().commit();
		}
		
		int i = 1;		
		em.getTransaction().begin();	
		for (T csiEvaluation2Save : results) {		
			em.merge(csiEvaluation2Save);
			flushEm(em, i);
			
			i++;						
		}
		if(em.getTransaction().isActive()) {
			em.getTransaction().commit();
		}
		
		if(em.isOpen())
			em.close();
    }
	
	private void flushEm(EntityManager em, int i) {	
		if(i <= 1000000) {             
			if ((i % 200) == 0) {
				em.getTransaction().commit();
				em.clear();          
				em.getTransaction().begin();
			}
		}
	}

	/**
	 * @return the results
	 */
	public List<T> getResults() {
		return results;
	}

	/**
	 * @param results the results to set
	 */
	public void setResults(List<T> results) {
		this.results = results;
	}
}
