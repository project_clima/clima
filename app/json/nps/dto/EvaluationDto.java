package json.nps.dto;

public class EvaluationDto {

    private String fullName;
    private int totalEvaluations;
    private int totalComplaints;
    private int totalOpen;
    private int totalTracing;
    private int totalSolved;

    public EvaluationDto(String fullName, int totalEvaluations, int totalComplaints, int totalOpen, int totalTracing,
            int totalSolved) {
        this.fullName = fullName;
        this.totalEvaluations = totalEvaluations;
        this.totalComplaints = totalComplaints;
        this.totalOpen = totalOpen;
        this.totalTracing = totalTracing;
        this.totalSolved = totalSolved;
    }

    /**
     * @return the fullName
     */
    public String getFullName() {
        return fullName;
    }

    /**
     * @param fullName
     *            the fullName to set
     */
    public void setFullName(String fullName) {
        this.fullName = fullName;
    }

    /**
     * @return the totalEvaluations
     */
    public int getTotalEvaluations() {
        return totalEvaluations;
    }

    /**
     * @param totalEvaluations
     *            the totalEvaluations to set
     */
    public void setTotalEvaluations(int totalEvaluations) {
        this.totalEvaluations = totalEvaluations;
    }

    /**
     * @return the totalComplaints
     */
    public int getTotalComplaints() {
        return totalComplaints;
    }

    /**
     * @param totalComplaints
     *            the totalComplaints to set
     */
    public void setTotalComplaints(int totalComplaints) {
        this.totalComplaints = totalComplaints;
    }

    /**
     * @return the totalOpen
     */
    public int getTotalOpen() {
        return totalOpen;
    }

    /**
     * @param totalOpen
     *            the totalOpen to set
     */
    public void setTotalOpen(int totalOpen) {
        this.totalOpen = totalOpen;
    }

    /**
     * @return the totalTracing
     */
    public int getTotalTracing() {
        return totalTracing;
    }

    /**
     * @param totalTracing
     *            the totalTracing to set
     */
    public void setTotalTracing(int totalTracing) {
        this.totalTracing = totalTracing;
    }

    /**
     * @return the totalSolved
     */
    public int getTotalSolved() {
        return totalSolved;
    }

    /**
     * @param totalSolved
     *            the totalSolved to set
     */
    public void setTotalSolved(int totalSolved) {
        this.totalSolved = totalSolved;
    }
}
