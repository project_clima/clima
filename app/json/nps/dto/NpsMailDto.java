package json.nps.dto;

import models.NpsMailLog;

public class NpsMailDto {

    private NpsMailLog mailLog;
    private String mailBody;
    private String subject;
    private int survey;
    private String email;

    public NpsMailDto(NpsMailLog mailLog, String mailBody, String subject, int survey, String email) {
        this.mailLog = mailLog;
        this.mailBody = mailBody;
        this.subject = subject;
        this.survey = survey;
        this.email = email;
    }

    /**
     * @return the mailLog
     */
    public NpsMailLog getMailLog() {
        return mailLog;
    }

    /**
     * @param mailLog
     *            the mailLog to set
     */
    public void setMailLog(NpsMailLog mailLog) {
        this.mailLog = mailLog;
    }

    /**
     * @return the mailBody
     */
    public String getMailBody() {
        return mailBody;
    }

    /**
     * @param mailBody
     *            the mailBody to set
     */
    public void setMailBody(String mailBody) {
        this.mailBody = mailBody;
    }

    /**
     * @return the subject
     */
    public String getSubject() {
        return subject;
    }

    /**
     * @param subject
     *            the subject to set
     */
    public void setSubject(String subject) {
        this.subject = subject;
    }

    /**
     * @return the survey
     */
    public int getSurvey() {
        return survey;
    }

    /**
     * @param survey
     *            the survey to set
     */
    public void setSurvey(int survey) {
        this.survey = survey;
    }

    /**
     * @return the email
     */
    public String getEmail() {
        return email;
    }

    /**
     * @param email
     *            the email to set
     */
    public void setEmail(String email) {
        this.email = email;
    }

}
