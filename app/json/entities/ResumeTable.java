package json.entities;

public class ResumeTable {
    private int surveys;
    private int answers;
    private float percentage;
    private float score;

    /**
     * @return the surveys
     */
    public int getSurveys() {
        return surveys;
    }

    /**
     * @param surveys the surveys to set
     */
    public void setSurveys(int surveys) {
        this.surveys = surveys;
    }

    /**
     * @return the answers
     */
    public int getAnswers() {
        return answers;
    }

    /**
     * @param answers the answers to set
     */
    public void setAnswers(int answers) {
        this.answers = answers;
    }

    /**
     * @return the percentage
     */
    public float getPercentage() {
        return percentage;
    }

    /**
     * @param percentage the percentage to set
     */
    public void setPercentage(float percentage) {
        this.percentage = percentage;
    }

    /**
     * @return the score
     */
    public float getScore() {
        return score;
    }

    /**
     * @param score the score to set
     */
    public void setScore(float score) {
        this.score = score;
    }
    
    
}
