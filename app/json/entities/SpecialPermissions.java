package json.entities;

import java.util.List;
import models.EmployeeClima;

public class SpecialPermissions {
    private int level;
    private List data;
    private EmployeeClima employee;

    /**
     * @return the level
     */
    public int getLevel() {
        return level;
    }

    /**
     * @param level the level to set
     */
    public void setLevel(int level) {
        this.level = level;
    }

    /**
     * @return the data
     */
    public List getData() {
        return data;
    }

    /**
     * @param data the data to set
     */
    public void setData(List data) {
        this.data = data;
    }

    /**
     * @return the employee
     */
    public EmployeeClima getEmployee() {
        return employee;
    }

    /**
     * @param employee the employee to set
     */
    public void setEmployee(EmployeeClima employee) {
        this.employee = employee;
    }
    
    
}
