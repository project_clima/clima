package json.entities;

public class HistoryResults {
    
    private String level;
    
    // Environment
    private String envFirstYear;
    private float envFirstPercent;    
    private float envSecondPercent;
    private String envSecondYear;
    
    // Leadership
    private String leadFirstYear;
    private float leadFirstPercent;    
    private float leadSecondPercent;
    private String leadSecondYear;

    /**
     * @return the level
     */
    public String getLevel() {
        return level;
    }

    /**
     * @param level the level to set
     */
    public void setLevel(String level) {
        this.level = level;
    }

    /**
     * @return the envFirstYear
     */
    public String getEnvFirstYear() {
        return envFirstYear;
    }

    /**
     * @param envFirstYear the envFirstYear to set
     */
    public void setEnvFirstYear(String envFirstYear) {
        this.envFirstYear = envFirstYear;
    }

    /**
     * @return the envFirstPercent
     */
    public float getEnvFirstPercent() {
        return envFirstPercent;
    }

    /**
     * @param envFirstPercent the envFirstPercent to set
     */
    public void setEnvFirstPercent(float envFirstPercent) {
        this.envFirstPercent = envFirstPercent;
    }

    /**
     * @return the envSecondPercent
     */
    public float getEnvSecondPercent() {
        return envSecondPercent;
    }

    /**
     * @param envSecondPercent the envSecondPercent to set
     */
    public void setEnvSecondPercent(float envSecondPercent) {
        this.envSecondPercent = envSecondPercent;
    }

    /**
     * @return the envSecondYear
     */
    public String getEnvSecondYear() {
        return envSecondYear;
    }

    /**
     * @param envSecondYear the envSecondYear to set
     */
    public void setEnvSecondYear(String envSecondYear) {
        this.envSecondYear = envSecondYear;
    }

    /**
     * @return the leadFirstYear
     */
    public String getLeadFirstYear() {
        return leadFirstYear;
    }

    /**
     * @param leadFirstYear the leadFirstYear to set
     */
    public void setLeadFirstYear(String leadFirstYear) {
        this.leadFirstYear = leadFirstYear;
    }

    /**
     * @return the leadFirstPercent
     */
    public float getLeadFirstPercent() {
        return leadFirstPercent;
    }

    /**
     * @param leadFirstPercent the leadFirstPercent to set
     */
    public void setLeadFirstPercent(float leadFirstPercent) {
        this.leadFirstPercent = leadFirstPercent;
    }

    /**
     * @return the leadSecondPercent
     */
    public float getLeadSecondPercent() {
        return leadSecondPercent;
    }

    /**
     * @param leadSecondPercent the leadSecondPercent to set
     */
    public void setLeadSecondPercent(float leadSecondPercent) {
        this.leadSecondPercent = leadSecondPercent;
    }

    /**
     * @return the leadSecondYear
     */
    public String getLeadSecondYear() {
        return leadSecondYear;
    }

    /**
     * @param leadSecondYear the leadSecondYear to set
     */
    public void setLeadSecondYear(String leadSecondYear) {
        this.leadSecondYear = leadSecondYear;
    }
}
