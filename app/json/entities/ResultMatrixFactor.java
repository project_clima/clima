package json.entities;

import java.util.List;

public class ResultMatrixFactor {
    private String factor;
    private float score;
    private List<ResultMatrixList> subfactors;

    /**
     * @return the factor
     */
    public String getFactor() {
        return factor;
    }

    /**
     * @param factor the factor to set
     */
    public void setFactor(String factor) {
        this.factor = factor;
    }

    /**
     * @return the score
     */
    public float getScore() {
        return score;
    }

    /**
     * @param score the score to set
     */
    public void setScore(float score) {
        this.score = score;
    }

    /**
     * @return the subfactors
     */
    public List<ResultMatrixList> getSubfactors() {
        return subfactors;
    }

    /**
     * @param subfactors the subfactors to set
     */
    public void setSubfactors(List<ResultMatrixList> subfactors) {
        this.subfactors = subfactors;
    }

    
}
