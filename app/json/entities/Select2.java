package json.entities;

import java.util.ArrayList;
import java.util.List;

public class Select2 {
    public long total_count;
    public boolean incomplete_results;
    public List<AutoComplete> items;
    
    public Select2() {
        this.incomplete_results = false;
        this.items = new ArrayList();
    }
}
