package json.entities;

import java.util.ArrayList;
import java.util.List;

public class TreeData {
    public String id;
    public String parent;
    public String text;
    public String icon;
    public Boolean children;
    public State state;
    
    public TreeData(String id, String parent, String text) {
        this.id     = id;
        this.parent = parent;
        this.text   = text;
        this.icon   = "public/assets/img/user-24x24.png";
    }
    
    public TreeData(String id, String parent, String text, Boolean children, Boolean selected) {
        this.id     = id;
        this.parent = parent;
        this.text   = text;
        this.icon   = "public/assets/img/user-24x24.png";
        this.children = children;
        this.state    = new State(selected);
    }
}
