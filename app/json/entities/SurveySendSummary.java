package json.entities;

public class SurveySendSummary {
    public String date;
    public long sent;
    public long notSent;
    
    public SurveySendSummary(String date, long sent, long notSent) {
        this.date    = date;
        this.sent    = sent;
        this.notSent = notSent;
    }
}
