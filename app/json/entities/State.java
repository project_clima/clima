package json.entities;

/**
 *
 * @author Jorge
 */
public class State {
    public Boolean selected;
    
    public State(Boolean selected) {
        this.selected = selected;
    }
}
