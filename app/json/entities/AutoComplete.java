package json.entities;

public class AutoComplete {
    public int id;
    public String text;
    
    public AutoComplete(int id, String text) {
        this.id   = id;
        this.text = text;
    }
}
