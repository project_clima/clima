package json.entities;

public class FortalezasList {
    private String question;
    private String factor;
    private String subfactor;
    private float score;


    /**
    * Returns value of question
    * @return
    */
    public String getQuestion() {
        return question;
    }

    /**
    * Sets new value of question
    * @param
    */
    public void setQuestion(String question) {
        this.question = question;
    }

    /**
    * Returns value of factor
    * @return
    */
    public String getFactor() {
        return factor;
    }

    /**
    * Sets new value of factor
    * @param
    */
    public void setFactor(String factor) {
        this.factor = factor;
    }

    /**
    * Returns value of subfactor
    * @return
    */
    public String getSubfactor() {
        return subfactor;
    }

    /**
    * Sets new value of subfactor
    * @param
    */
    public void setSubfactor(String subfactor) {
        this.subfactor = subfactor;
    }

    /**
    * Returns value of score
    * @return
    */
    public float getScore() {
        return score;
    }

    /**
    * Sets new value of score
    * @param
    */
    public void setScore(float score) {
        this.score = score;
    }
}
