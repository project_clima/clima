package json.entities;

import java.math.BigDecimal;

public class MailingList {

    private Integer userId;
    private String email;
    private String firstName;
    private String LastName;
    private String userName;
    private String password;
    private Integer humanResources;
    private Integer surveyType;
    private String department;
    private String manager;
    private Integer idEmployee;
    private String division;
    private Integer plan;
    private String emailCC;

    /**
     * @return the userId
     */
    public Integer getUserId() {
        return userId;
    }

    /**
     * @param userId the userId to set
     */
    public void setUserId(Integer userId) {
        this.userId = userId;
    }

    /**
     * @return the email
     */
    public String getEmail() {
        return email;
    }

    /**
     * @param email the email to set
     */
    public void setEmail(String email) {
        this.email = email;
    }

    /**
     * @return the firstName
     */
    public String getFirstName() {
        return firstName;
    }

    /**
     * @param firstName the firstName to set
     */
    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    /**
     * @return the LastName
     */
    public String getLastName() {
        return LastName;
    }

    /**
     * @param LastName the LastName to set
     */
    public void setLastName(String LastName) {
        this.LastName = LastName;
    }

    /**
     * @return the userName
     */
    public String getUserName() {
        return userName;
    }

    /**
     * @param userName the userName to set
     */
    public void setUserName(String userName) {
        this.userName = userName;
    }

    /**
     * @return the password
     */
    public String getPassword() {
        return password;
    }

    /**
     * @param password the password to set
     */
    public void setPassword(String password) {
        this.password = password;
    }

    /**
     * @return the humanResources
     */
    public Integer getHumanResources() {
        return humanResources;
    }

    /**
     * @param humanResources the humanResources to set
     */
    public void setHumanResources(Integer humanResources) {
        this.humanResources = humanResources;
    }

    /**
     * @return the surveyType
     */
    public Integer getSurveyType() {
        return surveyType;
    }

    /**
     * @param surveyType the surveyType to set
     */
    public void setSurveyType(Integer surveyType) {
        this.surveyType = surveyType;
    }

    /**
     * @return the department
     */
    public String getDepartment() {
        return department;
    }

    /**
     * @param department the department to set
     */
    public void setDepartment(String department) {
        this.department = department;
    }

    /**
     * @return the manager
     */
    public String getManager() {
        return manager;
    }

    /**
     * @param manager the manager to set
     */
    public void setManager(String manager) {
        this.manager = manager;
    }

    /**
     * @return the idEmployee
     */
    public Integer getIdEmployee() {
        return idEmployee;
    }

    /**
     * @param idEmployee the idEmployee to set
     */
    public void setIdEmployee(Integer idEmployee) {
        this.idEmployee = idEmployee;
    }
    
    /**
     * @return the idEmployee
     */
    public String getDivision() {
        return division;
    }

    /**
     * @param division the division to set
     */
    public void setDivision(String division) {
        this.division = division;
    }
    
    /**
     * @return the plan
     */
    public Integer getPlan() {
        return plan;
    }

    /**
     * @param plan the plan to set
     */
    public void setPlan(Integer plan) {
        this.plan = plan;
    }
    
    /**
     * @return the fullName
     */
    public String getFullName() {
        return this.firstName + " " + this.LastName;
    }

    /**
     * @return the emailCC
     */
    public String getEmailCC() {
        return emailCC;
    }

    /**
     * @param emailCC the emailCC to set
     */
    public void setEmailCC(String emailCC) {
        this.emailCC = emailCC;
    }
}
