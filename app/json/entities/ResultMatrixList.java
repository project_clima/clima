package json.entities;

public class ResultMatrixList {
    private String factor;
    private String subFactor;
    private float  score;
    private String department;
    private String question;

    /**
    * Returns value of factor
    * @return
    */
    public String getFactor() {
        return factor;
    }

    /**
    * Sets new value of factor
    * @param
    */
    public void setFactor(String factor) {
        this.factor = factor;
    }

    /**
    * Returns value of subFactor
    * @return
    */
    public String getSubFactor() {
        return subFactor;
    }

    /**
    * Sets new value of subFactor
    * @param
    */
    public void setSubFactor(String subFactor) {
        this.subFactor = subFactor;
    }

    /**
    * Returns value of score
    * @return
    */
    public float getScore() {
        return score;
    }

    /**
    * Sets new value of score
    * @param
    */
    public void setScore(float score) {
        this.score = score;
    }

    /**
    * Returns value of department
    * @return
    */
    public String getDepartment() {
        return department;
    }

    /**
    * Sets new value of department
    * @param
    */
    public void setDepartment(String department) {
        this.department = department;
    }

    /**
    * Returns value of question
    * @return
    */
    public String getQuestion() {
        return question;
    }

    /**
    * Sets new value of question
    * @param
    */
    public void setQuestion(String question) {
        this.question = question;
    }
}
