/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package json.entities;

/**
 *
 * @author QualtopGroup
 */
public class HistoricoList {
    
    private String division;
    private float firstPercent = 0f;
    private float secondPercent = 0f;
    private String firstYear;
    private String secondYear;
    
    public float getFirstPercent() {
        return firstPercent;
    }

    public void setFirstPercent(float firstPercent) {
        this.firstPercent = firstPercent;
    }

    public float getSecondPercent() {
        return secondPercent;
    }

    public void setSecondPercent(float secondPercent) {
        this.secondPercent = secondPercent;
    }

    public String getFirstYear() {
        return firstYear;
    }

    public void setFirstYear(String firstYear) {
        this.firstYear = firstYear;
    }

    public String getSecondYear() {
        return secondYear;
    }

    public void setSecondYear(String secondYear) {
        this.secondYear = secondYear;
    }
    
    
    public String getDivision(){
        return this.division;
    }
    
    public void setDisivion(String division){
        this.division=division;
    }
    
    
}
