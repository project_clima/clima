package json.entities;

public class ResultMatrixTitles {
    private float score;
    private String titles;

    /**
     * @return the score
     */
    public float getScore() {
        return score;
    }

    /**
     * @param score the score to set
     */
    public void setScore(float score) {
        this.score = score;
    }

    /**
     * @return the titles
     */
    public String getTitles() {
        return titles;
    }

    /**
     * @param titles the titles to set
     */
    public void setTitles(String titles) {
        this.titles = titles;
    }
    
    
}
