package json.csi.dto;

import java.util.ArrayList;
import java.util.List;

public class GraphSeriesResponseDto {

	public List<GraphSerieDto> series =  new ArrayList<>();
	public List<String> colors =  new ArrayList<>();
	
	public GraphSeriesResponseDto(){
		super();
	}

	public List<GraphSerieDto> getSeries() {
		return series;
	}

	public void setSeries(List<GraphSerieDto> series) {
		this.series = series;
	}

	public List<String> getColors() {
		return colors;
	}

	public void setColors(List<String> colors) {
		this.colors = colors;
	}
	
	
}
