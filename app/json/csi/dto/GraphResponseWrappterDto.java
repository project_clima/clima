package json.csi.dto;

import java.util.List;

public class GraphResponseWrappterDto {
	public GraphResumeDto resume ;
	GraphSeriesResponseDto series;
	List<String> catalogos;
	


	public GraphResponseWrappterDto(GraphResumeDto resume, GraphSeriesResponseDto series, List<String >catalogos) {
		super();
		this.resume = resume;
		this.series = series;
		this.catalogos = catalogos;
	}

	public List<String> getCatalogos() {
		return catalogos;
	}

	public void setCatalogos(List<String> catalogos) {
		this.catalogos = catalogos;
	}

	public GraphSeriesResponseDto getSeries() {
		return series;
	}

	public void setSeries(GraphSeriesResponseDto series) {
		this.series = series;
	}

	public GraphResumeDto getResume() {
		return resume;
	}

	public void setResume(GraphResumeDto resume) {
		this.resume = resume;
	}
	
	
	
}
