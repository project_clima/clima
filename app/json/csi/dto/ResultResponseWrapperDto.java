package json.csi.dto;

import java.util.List;

public class ResultResponseWrapperDto {

	List<ResultHeaderDto> resultHeaderDtoList;
	List<ResultDetailDto> resultDetailDtoList;
	List<Integer> orders;
	List<ResultCatalogDto> nextCatalog;
	public ResultResponseWrapperDto(List<ResultHeaderDto> resultHeaderDtoList,
			List<ResultDetailDto> resultDetailDtoList, 
			List<Integer> orders,List<ResultCatalogDto> nextCatalog) {
		super();
		this.resultHeaderDtoList = resultHeaderDtoList;
		this.resultDetailDtoList = resultDetailDtoList;
		this.orders = orders;
		this.nextCatalog = nextCatalog;
	}
	public ResultResponseWrapperDto(){
		
	}
	
	public List<ResultHeaderDto> getResultHeaderDtoList() {
		return resultHeaderDtoList;
	}
	public void setResultHeaderDtoList(List<ResultHeaderDto> resultHeaderDtoList) {
		this.resultHeaderDtoList = resultHeaderDtoList;
	}
	public List<ResultDetailDto> getResultDetailDtoList() {
		return resultDetailDtoList;
	}
	public void setResultDetailDtoList(List<ResultDetailDto> resultDetailDtoList) {
		this.resultDetailDtoList = resultDetailDtoList;
	}
	public List<Integer> getOrders() {
		return orders;
	}
	public void setOrders(List<Integer> orders) {
		this.orders = orders;
	}
	public List<ResultCatalogDto> getNextCatalog() {
		return nextCatalog;
	}
	public void setNextCatalog(List<ResultCatalogDto> nextCatalog) {
		this.nextCatalog = nextCatalog;
	}

	
	
	
}
