package json.csi.dto;

import java.util.Date;

public class MatrixRawAreaLineResultDto {

	private Date date;
	private String evaluatedArea;
	private Integer idEvaluatedManager;
	private Double rawResult;
	private Double totalRawResult;
	private Double result;
	
	public MatrixRawAreaLineResultDto(Date date, Integer idEvaluatedManager, String evaluatedArea, Double rawResult, Double totalRawResult, Double result) {
		this.date = date;
		this.evaluatedArea = evaluatedArea;
		this.rawResult = rawResult;
		this.totalRawResult = totalRawResult;
		this.idEvaluatedManager = idEvaluatedManager;
		this.result = result;
	}
	
	
	public Double getResult() {
		return result;
	}


	public void setResult(Double result) {
		this.result = result;
	}


	/**
	 * @return the date
	 */
	public Date getDate() {
		return date;
	}
	/**
	 * @param date the date to set
	 */
	public void setDate(Date date) {
		this.date = date;
	}
	/**
	 * @return the evaluatedArea
	 */
	public String getEvaluatedArea() {
		return evaluatedArea;
	}
	/**
	 * @param evaluatedArea the evaluatedArea to set
	 */
	public void setEvaluatedArea(String evaluatedArea) {
		this.evaluatedArea = evaluatedArea;
	}
	/**
	 * @return the rawResult
	 */
	public Double getRawResult() {
		return rawResult;
	}
	/**
	 * @param rawResult the rawResult to set
	 */
	public void setRawResult(Double rawResult) {
		this.rawResult = rawResult;
	}
	/**
	 * @return the totalRawResult
	 */
	public Double getTotalRawResult() {
		return totalRawResult;
	}
	/**
	 * @param totalRawResult the totalRawResult to set
	 */
	public void setTotalRawResult(Double totalRawResult) {
		this.totalRawResult = totalRawResult;
	}


	/**
	 * @return the idEvaluatedManager
	 */
	public Integer getIdEvaluatedManager() {
		return idEvaluatedManager;
	}


	/**
	 * @param idEvaluatedManager the idEvaluatedManager to set
	 */
	public void setIdEvaluatedManager(Integer idEvaluatedManager) {
		this.idEvaluatedManager = idEvaluatedManager;
	}
	
}
