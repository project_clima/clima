package json.csi.dto;

import java.util.ArrayList;
import java.util.List;

import models.Sections;

public class SectionsResponse {

    private List<Sections> sections = new ArrayList<>();
    private List<SectionSelect> sectionSelected = new ArrayList<>();

    public List<Sections> getSections() {
        return sections;
    }

    public void setSections(List<Sections> sections) {
        this.sections = sections;
    }

    public List<SectionSelect> getSectionSelected() {
        return sectionSelected;
    }

    public void setSectionSelected(List<SectionSelect> sectionSelected) {
        this.sectionSelected = sectionSelected;
    }

}
