package json.csi.dto;

import java.util.List;

public class MatrixReportResponseWrapperDto {

	MatrixReportResultDto matrixReport;
	private List<ResultCatalogDto> nextCatalog;
	
	public MatrixReportResponseWrapperDto(MatrixReportResultDto matrixReport, List<ResultCatalogDto> nextCatalog) {
		super();
		this.matrixReport = matrixReport;
		this.nextCatalog = nextCatalog;		
	}
	public MatrixReportResultDto getMatrixReport() {
		return matrixReport;
	}
	public void setMatrixReport(MatrixReportResultDto matrixReport) {
		this.matrixReport = matrixReport;
	}
	/**
	 * @return the nextCatalog
	 */
	public List<ResultCatalogDto> getNextCatalog() {
		return nextCatalog;
	}
	/**
	 * @param nextCatalog the nextCatalog to set
	 */
	public void setNextCatalog(List<ResultCatalogDto> nextCatalog) {
		this.nextCatalog = nextCatalog;
	}
	
		
	
}
