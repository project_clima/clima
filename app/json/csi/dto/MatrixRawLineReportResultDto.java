package json.csi.dto;
import static json.csi.dto.RoundHelper.*;
public class MatrixRawLineReportResultDto {

	Integer idDireccionManager3Evaluado;
	String direccionManager3Evaluado;
	
	Integer idDireccionManager3Evaluador;
	String direccionManager3Evaluador;
	
	Double promMenosDetra;
	Double total;

	public MatrixRawLineReportResultDto(Integer idDireccionManager3Evaluado,String direccionManager3Evaluado,
			
			Integer idDireccionManager3Evaluador, String direccionManager3Evaluador,
			
			Double promMenosDetra, Double total) {
		super();
		this.idDireccionManager3Evaluado = idDireccionManager3Evaluado;
		this.idDireccionManager3Evaluador = idDireccionManager3Evaluador;
		this.direccionManager3Evaluado = direccionManager3Evaluado;
		this.direccionManager3Evaluador = direccionManager3Evaluador;
		
		this.promMenosDetra		= roundDouble2Decimales(promMenosDetra);
		this.total 				= roundDouble2Decimales(total);
	}

	public Integer getIdDireccionManager3Evaluado() {
		return idDireccionManager3Evaluado;
	}

	public void setIdDireccionManager3Evaluado(Integer idDireccionManager3Evaluado) {
		this.idDireccionManager3Evaluado = idDireccionManager3Evaluado;
	}

	public Integer getIdDireccionManager3Evaluador() {
		return idDireccionManager3Evaluador;
	}

	public void setIdDireccionManager3Evaluador(Integer idDireccionManager3Evaluador) {
		this.idDireccionManager3Evaluador = idDireccionManager3Evaluador;
	}

	public String getDireccionManager3Evaluado() {
		return direccionManager3Evaluado;
	}

	public void setDireccionManager3Evaluado(String direccionManager3Evaluado) {
		this.direccionManager3Evaluado = direccionManager3Evaluado;
	}

	public String getDireccionManager3Evaluador() {
		return direccionManager3Evaluador;
	}

	public void setDireccionManager3Evaluador(String direccionManager3Evaluador) {
		this.direccionManager3Evaluador = direccionManager3Evaluador;
	}

	public Double getPromMenosDetra() {
		return promMenosDetra;
	}

	public void setPromMenosDetra(Double promMenosDetra) {
		this.promMenosDetra = promMenosDetra;
	}

	public Double getTotal() {
		return total;
	}

	public void setTotal(Double total) {
		this.total = total;
	}

}
