package json.csi.dto;

public class GraphSerieDto {
	
	String name;
	Double[] data;

	public GraphSerieDto() {
		this.data = new Double[]{0d,0d,0d,0d,0d,0d,0d,0d,0d,0d,0d,0d};
	}
	
	public GraphSerieDto(int size) {
		this.data = new Double[size];
	}
	public GraphSerieDto(String name, Double[] data) {
		super();
		this.name = name;
		this.data = data;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public Double[] getData() {
		return data;
	}
	public void setData(Double[] data) {
		this.data = data;
	}

	
	
	
}
