package json.csi.dto;

public class EmployeeSections {
	
	private String userId;
	private String sections;
	private String structureDate;
	public String getUserId() {
		return userId;
	}
	public void setUserId(String userId) {
		this.userId = userId;
	}
	public String getSections() {
		return sections;
	}
	public void setSections(String sections) {
		this.sections = sections;
	}
	public String getStructureDate() {
		return structureDate;
	}
	public void setStructureDate(String structureDate) {
		this.structureDate = structureDate;
	}
	
	
}
