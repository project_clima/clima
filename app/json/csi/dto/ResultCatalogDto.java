package json.csi.dto;

public class ResultCatalogDto {

  Integer id;
  String desc;
  
  
  public ResultCatalogDto(Integer id, String desc) {
		super();
		this.id = id;
		this.desc = desc;
		
		if (desc == null || desc.trim().isEmpty()){
			this.desc = "" + id;
		}
	}
	  
public Integer getId() {
	return id;
}
public void setId(Integer id) {
	this.id = id;
}
public String getDesc() {
	if (desc == null || desc.trim().isEmpty()){
		this.desc = "" + id;
	}
	return desc;
}
public void setDesc(String desc) {
	this.desc = desc;
}

  
	  
  

}
