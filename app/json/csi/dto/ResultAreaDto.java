package json.csi.dto;

public class ResultAreaDto {
	Integer idManager;
	
	String nombreArea;
	
	Double result;
	Double enero;
	Double febrero;
	Double marzo;
	Double abril;
	Double mayo;
	Double junio;
	Double julio;
	Double agosto;
	Double septiembre;
	Double octubre;
	Double noviembre;
	Double diciembre;

	public ResultAreaDto() {
		super();
		
		this.idManager = 0;
		
		this.nombreArea = "";
		
		this.result = 0.0;
		this.enero = 0.0;
		this.febrero = 0.0;
		this.marzo = 0.0;
		this.abril = 0.0;
		this.mayo = 0.0;
		this.junio = 0.0;
		this.julio = 0.0;
		this.agosto = 0.0;
		this.septiembre = 0.0;
		this.octubre = 0.0;
		this.noviembre = 0.0;
		this.diciembre = 0.0;
	}
	
	public ResultAreaDto(Integer idManager, String nombreArea, Double result, Double enero, Double febrero, Double marzo,
			Double abril, Double mayo, Double junio, Double julio, Double agosto, Double septiembre,
			Double octubre, Double noviembre, Double diciembre) {
		super();
		
		this.idManager = idManager;
		
		this.nombreArea = nombreArea;
		
		this.result = Math.round(result * 100.0) / 100.0;
		this.enero = Math.round(enero * 100.0) / 100.0;
		this.febrero = Math.round(febrero * 100.0) / 100.0;
		this.marzo = Math.round(marzo * 100.0) / 100.0;
		this.abril = Math.round(abril * 100.0) / 100.0;
		this.mayo = Math.round(mayo * 100.0) / 100.0;
		this.junio = Math.round(junio * 100.0) / 100.0;
		this.julio = Math.round(julio * 100.0) / 100.0;
		this.agosto = Math.round(agosto * 100.0) / 100.0;
		this.septiembre = Math.round(septiembre * 100.0) / 100.0;
		this.octubre = Math.round(octubre * 100.0) / 100.0;
		this.noviembre = Math.round(noviembre * 100.0) / 100.0;
		this.diciembre = Math.round(diciembre * 100.0) / 100.0;
	}

	public Integer getIdManager() {
		return idManager;
	}

	public void setIdManager(Integer idManager) {
		this.idManager = idManager;
	}

	public String getNombreArea() {
		return nombreArea;
	}

	public void setNombreArea(String nombreArea) {
		this.nombreArea = nombreArea;
	}

	public Double getResult() {
		return result;
	}

	public void setResult(Double result) {
		this.result = result;
	}

	public Double getEnero() {
		return enero;
	}

	public void setEnero(Double enero) {
		this.enero = enero;
	}

	public Double getFebrero() {
		return febrero;
	}

	public void setFebrero(Double febrero) {
		this.febrero = febrero;
	}

	public Double getMarzo() {
		return marzo;
	}

	public void setMarzo(Double marzo) {
		this.marzo = marzo;
	}

	public Double getAbril() {
		return abril;
	}

	public void setAbril(Double abril) {
		this.abril = abril;
	}

	public Double getMayo() {
		return mayo;
	}

	public void setMayo(Double mayo) {
		this.mayo = mayo;
	}

	public Double getJunio() {
		return junio;
	}

	public void setJunio(Double junio) {
		this.junio = junio;
	}

	public Double getJulio() {
		return julio;
	}

	public void setJulio(Double julio) {
		this.julio = julio;
	}

	public Double getAgosto() {
		return agosto;
	}

	public void setAgosto(Double agosto) {
		this.agosto = agosto;
	}

	public Double getSeptiembre() {
		return septiembre;
	}

	public void setSeptiembre(Double septiembre) {
		this.septiembre = septiembre;
	}

	public Double getOctubre() {
		return octubre;
	}

	public void setOctubre(Double octubre) {
		this.octubre = octubre;
	}

	public Double getNoviembre() {
		return noviembre;
	}

	public void setNoviembre(Double noviembre) {
		this.noviembre = noviembre;
	}

	public Double getDiciembre() {
		return diciembre;
	}

	public void setDiciembre(Double diciembre) {
		this.diciembre = diciembre;
	}
}
