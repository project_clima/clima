package json.csi.dto;

public class CsiDBRepor {

	private Integer order;
	private Integer result;
	private String comments;

	public CsiDBRepor() {

	}

	public CsiDBRepor(Integer order, Integer result, String comments) {
		super();
		this.order = order;
		this.result = result;
		this.comments = comments;
	}

	public Integer getOrder() {
		return order;
	}

	public Integer getResult() {
		return result;
	}

	public void setResult(Integer result) {
		this.result = result;
	}

	public void setOrder(Integer order) {
		this.order = order;
	}

	public String getComments() {
		if (comments == null) {
			comments = " - ";
		}
		return comments;
	}

	public void setComments(String comments) {
		this.comments = comments;
	}

}
