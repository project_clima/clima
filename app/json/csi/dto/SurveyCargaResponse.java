package json.csi.dto;

import java.util.ArrayList;
import java.util.List;

public class SurveyCargaResponse {
	String loadtype;
	int idResponse;
	String errorMessage;
	
	List<SurveyCargaRow> cargaRows =  new ArrayList<>();
	
	public String getLoadtype() {
		return loadtype;
	}
	public void setLoadtype(String loadtype) {
		this.loadtype = loadtype;
	}
	public List<SurveyCargaRow> getCargaRows() {
		return cargaRows;
	}
	public void setCargaRows(List<SurveyCargaRow> cargaRows) {
		this.cargaRows = cargaRows;
	}
	/**
	 * @return the idResponse
	 */
	public int getIdResponse() {
		return idResponse;
	}
	/**
	 * @param idResponse the idResponse to set
	 */
	public void setIdResponse(int idResponse) {
		this.idResponse = idResponse;
	}
	/**
	 * @return the errorMessage
	 */
	public String getErrorMessage() {
		return errorMessage;
	}
	/**
	 * @param errorMessage the errorMessage to set
	 */
	public void setErrorMessage(String errorMessage) {
		this.errorMessage = errorMessage;
	}

	
	
}
