package json.csi.dto;

public class CsiDBReportDTO {

	private Integer idEvaluated = 0;

	private String lastName = "-";

	private String firstName = "-";

	private String email = "-";

	private String direcciones = "-";

	private String mundo = "-";

	private String seccion = "";

	private Double total = 0.0;

	private Double detractoras = 0.0;

	private Double neutras = 0.0;

	private Double promotoras = 0.0;

	private Integer numEmpleado = 0;

	private Double totalCSI = 0.0;
	
	private Integer cal0 = 0;
	private Integer cal1 = 0;
	private Integer cal2 = 0;
	private Integer cal3 = 0;
	private Integer cal4 = 0;
	private Integer cal5 = 0;
	private Integer cal6 = 0;
	private Integer cal7 = 0;
	private Integer cal8 = 0;
	private Integer cal9 = 0;
	private Integer cal10 = 0;

	public CsiDBReportDTO() {
	}

	public CsiDBReportDTO(
			Integer idEvaluated, String lastName, String firstName, String email, Integer numEmpleado,
			String direcciones, String mundo, String seccion
			, Double detractoras, Double neutras, Double promotoras
			, Long cal0, Long cal1, Long cal2, Long cal3, Long cal4, Long cal5, Long cal6, Long cal7, Long cal8, Long cal9, Long cal10
			) {
		super();
		this.idEvaluated = idEvaluated;
		this.lastName = lastName;
		this.firstName = firstName;
		this.email = email;
		this.numEmpleado = numEmpleado;
		this.direcciones = direcciones;
		this.mundo = mundo;
		this.detractoras = detractoras;
		this.neutras = neutras;
		this.promotoras = promotoras;
		this.seccion = seccion;
		
		this.cal0 = cal0.intValue();
		this.cal1 = cal1.intValue();
		this.cal2 = cal2.intValue();
		this.cal3 = cal3.intValue();
		this.cal4 = cal4.intValue();
		this.cal5 = cal5.intValue();
		this.cal6 = cal6.intValue();
		this.cal7 = cal7.intValue();
		this.cal8 = cal8.intValue();
		this.cal9 = cal9.intValue();
		this.cal10 = cal10.intValue();
	}

	public Integer getIdEvaluated() {
		return idEvaluated;
	}

	public void setIdEvaluated(Integer idEvaluated) {
		this.idEvaluated = idEvaluated;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getDirecciones() {
		return direcciones == null ? "" : direcciones;
	}

	public void setDirecciones(String direcciones) {
		this.direcciones = direcciones;
	}

	public String getMundo() {
		return mundo == null ? "" : mundo;
	}

	public void setMundo(String mundo) {
		this.mundo = mundo;
	}

	public String getSeccion() {
		return seccion == null ? "" : seccion;
	}

	public void setSeccion(String seccion) {
		this.seccion = seccion;
	}

	public Double getTotal() {
		total = detractoras + neutras + promotoras;
		return total;
	}

	public Double getDetractoras() {
		return Math.round(detractoras  * 100.0) / total;
	}

	public void setDetractoras(Double detractoras) {
		this.detractoras = detractoras;
	}

	public Double getNeutras() {
		return Math.round(neutras  * 100.0) / total;
	}

	public void setNeutras(Double neutras) {
		this.neutras = neutras;
	}

	public Double getPromotoras() {
		return Math.round(promotoras  * 100.0) / total;
	}

	public void setPromotoras(Double promotoras) {
		this.promotoras = promotoras;
	}

	public Integer getNumEmpleado() {
		return numEmpleado;
	}

	public void setNumEmpleado(Integer numEmpleado) {
		this.numEmpleado = numEmpleado;
	}

	public Double getTotalCSI() {
		if ((promotoras - detractoras) != 0) {
			totalCSI = (promotoras - detractoras);
		}
		return Math.round(totalCSI  * 100.0) / total;
	}

	public Integer getCal0() {
		return cal0;
	}

	public void setCal0(Integer cal0) {
		this.cal0 = cal0;
	}

	public Integer getCal1() {
		return cal1;
	}

	public void setCal1(Integer cal1) {
		this.cal1 = cal1;
	}

	public Integer getCal2() {
		return cal2;
	}

	public void setCal2(Integer cal2) {
		this.cal2 = cal2;
	}

	public Integer getCal3() {
		return cal3;
	}

	public void setCal3(Integer cal3) {
		this.cal3 = cal3;
	}

	public Integer getCal4() {
		return cal4;
	}

	public void setCal4(Integer cal4) {
		this.cal4 = cal4;
	}

	public Integer getCal5() {
		return cal5;
	}

	public void setCal5(Integer cal5) {
		this.cal5 = cal5;
	}

	public Integer getCal6() {
		return cal6;
	}

	public void setCal6(Integer cal6) {
		this.cal6 = cal6;
	}

	public Integer getCal7() {
		return cal7;
	}

	public void setCal7(Integer cal7) {
		this.cal7 = cal7;
	}

	public Integer getCal8() {
		return cal8;
	}

	public void setCal8(Integer cal8) {
		this.cal8 = cal8;
	}

	public Integer getCal9() {
		return cal9;
	}

	public void setCal9(Integer cal9) {
		this.cal9 = cal9;
	}

	public Integer getCal10() {
		return cal10;
	}

	public void setCal10(Integer cal10) {
		this.cal10 = cal10;
	}

	public void setTotal(Double total) {
		this.total = total;
	}

	public void setTotalCSI(Double totalCSI) {
		this.totalCSI = totalCSI;
	}

}
