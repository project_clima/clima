package json.csi.dto;

public class RoundHelper {
	public static Double roundDouble2Decimales(Double result) {
		if (result == null) {
			return 0d;
		}
		return Math.round(result * 100.0) / 100.0;
	}

	public static Double roundDouble1Decimal(Double result) {
		if (result == null) {
			return 0d;
		}
		return Math.round(result * 10.0) / 10.0;
	}
}
