package json.csi.dto;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import static json.csi.dto.RoundHelper.*;

public class MatrixReportResultDto {

	Integer countColumns;
	Integer countRows;
	Map<Integer,String> colNames;
	Map<Integer,String> rowNames;
	
	Map<Integer, MatrixReportRowResultDto> rows =  new LinkedHashMap<>();


	public MatrixReportResultDto() {
		super();
	}


	public Integer getCountColumns() {
		return countColumns;
	}

	public void setCountColumns(Integer countColumns) {
		this.countColumns = countColumns;
	}

	public Integer getCountRows() {
		return countRows;
	}

	public void setCountRows(Integer countRows) {
		this.countRows = countRows;
	}
	
	public Map<Integer, String> getColNames() {
		return colNames;
	}


	public void setColNames(Map<Integer, String> colNames) {
		this.colNames = colNames;
	}


	public Map<Integer, String> getRowNames() {
		return rowNames;
	}


	public void setRowNames(Map<Integer, String> rowNames) {
		this.rowNames = rowNames;
	}
	public Map<Integer, MatrixReportRowResultDto> getRows() {
		return rows;
	}
	public void setRows(Map<Integer, MatrixReportRowResultDto> rows) {
		this.rows = rows;
	}


}
