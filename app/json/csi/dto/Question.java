package json.csi.dto;

public class Question {
	
	private Object id; 
	private String description;
	
	public Question() {}
	
	public Question(Object id, String description) {
		this.id = id;
		this.description = description;
	}
	
	
	/**
	 * @return the id
	 */
	public Object getId() {
		return id;
	}
	/**
	 * @param id the id to set
	 */
	public void setId(int id) {
		this.id = id;
	}
	/**
	 * @return the description
	 */
	public String getDescription() {
		return description;
	}
	/**
	 * @param description the description to set
	 */
	public void setDescription(String description) {
		this.description = description;
	}

}
