package json.csi.dto;

import java.util.ArrayList;
import java.util.List;

public class CommentsResponse {
	
	private List<Comment> comments = new ArrayList<>();

	public List<Comment> getComments() {
		return comments;
	}

	public void setComments(List<Comment> comments) {
		this.comments = comments;
	}
	
	

}
