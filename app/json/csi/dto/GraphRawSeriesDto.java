package json.csi.dto;

import java.util.Date;

public class GraphRawSeriesDto {
	Date surveyDate;
	Double promMenosDetra;
	Double totalSumResult;
	
	
	public GraphRawSeriesDto(Date surveyDate, Double promMenosDetra, Double totalSumResult) {
		super();
		this.surveyDate = surveyDate;
		this.promMenosDetra = promMenosDetra;
		this.totalSumResult = totalSumResult;
	}
	public Date getSurveyDate() {
		return surveyDate;
	}
	public void setSurveyDate(Date surveyDate) {
		this.surveyDate = surveyDate;
	}
	public Double getPromMenosDetra() {
		return promMenosDetra;
	}
	public void setPromMenosDetra(Double promMenosDetra) {
		this.promMenosDetra = promMenosDetra;
	}
	public Double getTotalSumResult() {
		return totalSumResult;
	}
	public void setTotalSumResult(Double totalSumResult) {
		this.totalSumResult = totalSumResult;
	}
	
	
	
}
