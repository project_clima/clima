package json.csi.dto;

import static json.csi.dto.RoundHelper.roundDouble2Decimales;
import static json.csi.dto.RoundHelper.roundDouble1Decimal;
public class GraphResumeDto {
	Double promotores;
	Double detractores;
	Double neutros;

	Double plan;
	Double general;
	
	
	
	public GraphResumeDto(Double promotores, Double neutros, Double detractores, Double plan) {
		super();
	
		Double total = 
				(promotores==null?0:promotores) + 
				(detractores==null?0:detractores) + 
				(neutros==null?0:neutros);
		
		this.promotores = roundDouble2Decimales((promotores==null?0:promotores) * 100.0 / (total==0?1:total));
		this.detractores= roundDouble2Decimales((detractores==null?0:detractores) * 100.0 / (total==0?1:total));
		this.neutros 	= roundDouble2Decimales((neutros==null?0:neutros) * 100.0 / (total==0?1:total));
		this.plan =  roundDouble1Decimal((plan==null?0:plan));
		this.general = roundDouble1Decimal(((promotores==null?0:promotores) - (detractores==null?0:detractores)) * 100 / (total==0?1:total));
	}
	public Double getPromotores() {
		return promotores;
	}
	public void setPromotores(Double promotores) {
		this.promotores = promotores;
	}
	public Double getDetractores() {
		return detractores;
	}
	public void setDetractores(Double detractores) {
		this.detractores = detractores;
	}
	public Double getNeutros() {
		return neutros;
	}
	public void setNeutros(Double neutros) {
		this.neutros = neutros;
	}
	public Double getPlan() {
		return plan;
	}
	public void setPlan(Double plan) {
		this.plan = plan;
	}
	public Double getGeneral() {
		return general;
	}
	public void setGeneral(Double general) {
		this.general = general;
	}
	
	
}
