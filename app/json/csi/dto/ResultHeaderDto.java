package json.csi.dto;

public class ResultHeaderDto {

  Integer idAgrupador;
  Double result;
  Double countZ1A;
  Double countZ1B;
  Double countZ02;
  Double countZ03;
  Double countZ04;
  Double countZ05;
  Double countZ06;
  Double countZ07;
  Double countZ08;
  Double countZBT;
  
  
	public ResultHeaderDto(Integer idAgrupador, Double result, Double countZ1A, Double countZ1B,
		Double countZ02, Double countZ03, Double countZ04, Double countZ05, Double countZ06, Double countZ07,
		Double countZ08, Double countZBT) {
		super();
		this.idAgrupador = idAgrupador;
		this.result =   roundDouble(result);
		this.countZ1A = roundDouble(countZ1A);
		this.countZ1B = roundDouble(countZ1B);
		this.countZ02 = roundDouble(countZ02);
		this.countZ03 = roundDouble(countZ03);
		this.countZ04 = roundDouble(countZ04);
		this.countZ05 = roundDouble(countZ05);
		this.countZ06 = roundDouble(countZ06);
		this.countZ07 = roundDouble(countZ07);
		this.countZ08 = roundDouble(countZ08);
		this.countZBT = roundDouble(countZBT);
	}
	private double roundDouble(Double result) {
		if (result == null){
			return 0;
		}
		return Math.round(result  * 100.0) / 100.0;
	}
	public Integer getIdAgrupador() {
		return idAgrupador;
	}
	public void setIdAgrupador(Integer idAgrupador) {
		this.idAgrupador = idAgrupador;
	}
	public Double getResult() {
		return result;
	}
	public void setResult(Double result) {
		this.result = result;
	}
	public Double getCountZ1A() {
		return countZ1A;
	}
	public void setCountZ1A(Double countZ1A) {
		this.countZ1A = countZ1A;
	}
	public Double getCountZ1B() {
		return countZ1B;
	}
	public void setCountZ1B(Double countZ1B) {
		this.countZ1B = countZ1B;
	}
	public Double getCountZ02() {
		return countZ02;
	}
	public void setCountZ02(Double countZ02) {
		this.countZ02 = countZ02;
	}
	public Double getCountZ03() {
		return countZ03;
	}
	public void setCountZ03(Double countZ03) {
		this.countZ03 = countZ03;
	}
	public Double getCountZ04() {
		return countZ04;
	}
	public void setCountZ04(Double countZ04) {
		this.countZ04 = countZ04;
	}
	public Double getCountZ05() {
		return countZ05;
	}
	public void setCountZ05(Double countZ05) {
		this.countZ05 = countZ05;
	}
	public Double getCountZ06() {
		return countZ06;
	}
	public void setCountZ06(Double countZ06) {
		this.countZ06 = countZ06;
	}
	public Double getCountZ07() {
		return countZ07;
	}
	public void setCountZ07(Double countZ07) {
		this.countZ07 = countZ07;
	}
	public Double getCountZ08() {
		return countZ08;
	}
	public void setCountZ08(Double countZ08) {
		this.countZ08 = countZ08;
	}
	public Double getCountZBT() {
		return countZBT;
	}
	public void setCountZBT(Double countZBT) {
		this.countZBT = countZBT;
	}
	  
	  
  

}
