package json.csi.dto;

import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Map;
import static json.csi.dto.RoundHelper.roundDouble2Decimales;
public class MatrixReportRowResultDto {

	Map<Integer, MatrixReportValueResultDto> values = new LinkedHashMap<>();
	Double total;
	
	public MatrixReportRowResultDto(){
		super();
	}
	
	public Map<Integer, MatrixReportValueResultDto> getValues() {
		return values;
	}
	public void setValues(Map<Integer, MatrixReportValueResultDto> values) {
		this.values = values;
	}
	public void addValue(Integer id , MatrixReportValueResultDto value){
		values.put(id, value);
		calculateTotal();
	}

	public Double getTotal() {
		return this.total;
	}
	public void setTotal(Double total) {
		this.total = total;
	}
	
	
	public void calculateTotal(){
		Double detraMenosProm = 0d;
		Double total = 0d;
		for (MatrixReportValueResultDto value:values.values()){
			detraMenosProm += value.countPromMenosDetra;
			total += value.countTotal;
		}
		this.total =  roundDouble2Decimales(detraMenosProm*100 / total);
	}
}
