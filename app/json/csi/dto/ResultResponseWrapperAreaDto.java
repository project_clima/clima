package json.csi.dto;

import java.util.List;

public class ResultResponseWrapperAreaDto {
	List<ResultAreaDto> resultHeaderAreaDtoList;
	List<ResultCatalogDto> nextCatalog;

	public ResultResponseWrapperAreaDto(List<ResultAreaDto> resultHeaderAreaDtoList,
			List<ResultCatalogDto> nextCatalog) {
		super();
		
		this.resultHeaderAreaDtoList = resultHeaderAreaDtoList;
		this.nextCatalog = nextCatalog;
	}

	public List<ResultAreaDto> getResultHeaderAreaDtoList() {
		return resultHeaderAreaDtoList;
	}

	public void setResultHeaderAreaDtoList(List<ResultAreaDto> resultHeaderAreaDtoList) {
		this.resultHeaderAreaDtoList = resultHeaderAreaDtoList;
	}

	public List<ResultCatalogDto> getNextCatalog() {
		return nextCatalog;
	}

	public void setNextCatalog(List<ResultCatalogDto> nextCatalog) {
		this.nextCatalog = nextCatalog;
	}
}
