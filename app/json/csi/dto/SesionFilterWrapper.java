package json.csi.dto;

import java.util.Date;

import controllers.Filters;
import controllers.csi.ApplicationCSI;
import play.mvc.Scope.Session;

public class SesionFilterWrapper {

	String idSurvey  = null;
	String idDate  = null;
	Integer filterEncuesta  = null;
	String filterDateType  = null;
	Date dateFrom  = null;
	Date dateTo = null;
	String filterCsiType = null;
	String filterCorporation = null;
	String filterManagement = null;
	String filterWorld = null;
	String filterSection = null;
	String filterEvaluated = null;

	boolean hasDate = false;
	boolean hasSurvey = false;
	String extraFilters  = null;
	public boolean addCSITypeFilter = false;

	public String getCSITypeFilterSurveyTypeIN(String surveyType){
		String CSITypeFilter = "";
		//s.surveyType
		if (addCSITypeFilter){
			CSITypeFilter = " AND "+surveyType+" in (select id from SurveyType where name = 'CSI') ";
		}
		return CSITypeFilter;
	}

	public String getCSITypeFilterSurveyTypeNameEqualsCSI(String surveyName){
		String CSITypeFilter = "";
		// s.name
		if (addCSITypeFilter){
			CSITypeFilter = " AND "+surveyName+" = 'CSI' " ;
		}
		return CSITypeFilter;
	}

	public SesionFilterWrapper(Session session, String idSurvey, String idDate, int extraIndex){

		this.idDate = idDate;
		this.idSurvey = idSurvey;
		this.filterEncuesta = Filters.getSessionFilterEncuesta(session);
		this.filterDateType = Filters.getSessionFilterDateType(session);
		Date[] dateFromTo = Filters.getDateFilters(filterDateType,session);
		this.dateFrom = dateFromTo[0];
		this.dateTo = dateFromTo[1];

		extraFilters  = getExtraSessionFiltersWhere(extraIndex);
	}

	public SesionFilterWrapper(Session session){

		this.filterCsiType = Filters.getSessionFilterCSIType(session);
		this.filterCorporation = Filters.getSessionFilterCorporation(session);
		this.filterManagement = Filters.getSessionFilterManagement(session);
		this.filterWorld = Filters.getSessionFilterWorld(session);
		this.filterSection = Filters.getSessionFilterSection(session);
		this.filterEvaluated = Filters.getSessionFilterEvaluated(session);
	}

	public  String getExtraSessionFiltersWhere(int extraIndex){
		StringBuilder sb =  new StringBuilder();
		if (filterEncuesta != null && filterEncuesta > 0 && idSurvey != null){
			sb.append(" "+idSurvey+" = ?"+(extraIndex+1)+" " );
			this.hasSurvey = true;
			extraIndex++;
		}
		if (dateFrom != null && dateTo != null && idDate != null){
			this.hasDate = true;
			if (this.hasSurvey){
				sb.append(" AND ");
			}
			sb.append(" "+idDate+" >= ?"+(extraIndex+1)+" " // from
					+ "AND "+idDate+" <= ?"+(extraIndex+2)+" " // to 
					);
		}

		return sb.toString();
	}


	public String getFilterCsiType() {
		return filterCsiType;
	}

	public void setFilterCsiType(String filterCsiType) {
		this.filterCsiType = filterCsiType;
	}

	public String getFilterManagement() {
		return filterManagement;
	}

	public void setFilterManagement(String filterManagement) {
		this.filterManagement = filterManagement;
	}

	public String getFilterWorld() {
		return filterWorld;
	}

	public void setFilterWorld(String filterWorld) {
		this.filterWorld = filterWorld;
	}

	public String getFilterSection() {
		return filterSection;
	}

	public void setFilterSection(String filterSection) {
		this.filterSection = filterSection;
	}

	public String getFilterEvaluated() {
		return filterEvaluated;
	}

	public void setFilterEvaluated(String filterEvaluated) {
		this.filterEvaluated = filterEvaluated;
	}

	public String getFilterCorporation() {
		return filterCorporation;
	}

	public void setFilterCorporation(String filterCorporation) {
		this.filterCorporation = filterCorporation;
	}

	public String getIdSurvey() {
		return idSurvey;
	}

	public void setIdSurvey(String idSurvey) {
		this.idSurvey = idSurvey;
	}

	public String getIdDate() {
		return idDate;
	}

	public void setIdDate(String idDate) {
		this.idDate = idDate;
	}

	public Integer getFilterEncuesta() {
		return filterEncuesta;
	}

	public void setFilterEncuesta(Integer filterEncuesta) {
		this.filterEncuesta = filterEncuesta;
	}

	public String getFilterDateType() {
		return filterDateType;
	}

	public void setFilterDateType(String filterDateType) {
		this.filterDateType = filterDateType;
	}

	public Date getDateFrom() {
		return dateFrom;
	}

	public void setDateFrom(Date dateFrom) {
		this.dateFrom = dateFrom;
	}

	public Date getDateTo() {
		return dateTo;
	}

	public void setDateTo(Date dateTo) {
		this.dateTo = dateTo;
	}

	public boolean isHasDate() {
		return hasDate;
	}

	public void setHasDate(boolean hasDate) {
		this.hasDate = hasDate;
	}

	public boolean isHasSurvey() {
		return hasSurvey;
	}

	public void setHasSurvey(boolean hasSurvey) {
		this.hasSurvey = hasSurvey;
	}

	public String getExtraFilters() {
		return extraFilters;
	}

	public void setExtraFilters(String extraFilters) {
		this.extraFilters = extraFilters;
	}

}
