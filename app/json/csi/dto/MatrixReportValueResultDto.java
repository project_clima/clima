package json.csi.dto;

import static json.csi.dto.RoundHelper.roundDouble2Decimales;

import com.google.gson.annotations.Expose;
public class MatrixReportValueResultDto {
	
	@Expose(serialize = false, deserialize = false)
	Double countPromMenosDetra;
	@Expose(serialize = false, deserialize = false)
	Double countTotal;
	
	private Double total;
	
	public MatrixReportValueResultDto(Double countPromMenosDetra, Double countTotal) {
		super();
		this.countPromMenosDetra = countPromMenosDetra;
		this.countTotal = countTotal;
		this.total = roundDouble2Decimales( countPromMenosDetra *100 / countTotal ) ;
	}
	
	public MatrixReportValueResultDto(MatrixRawLineReportResultDto line) {
		this( line.getPromMenosDetra(), line.getTotal());
	}

//	public Double getCountPromMenosDetra() {
//		return countPromMenosDetra;
//	}
//	public void setCountPromMenosDetra(Double countPromMenosDetra) {
//		this.countPromMenosDetra = countPromMenosDetra;
//	}
//	public Double getCountTotal() {
//		return countTotal;
//	}
//	public void setCountTotal(Double countTotal) {
//		this.countTotal = countTotal;
//	}

	public Double getTotal() {
		return total;
	}

	public void setTotal(Double total) {
		this.total = total;
	}
	
}
