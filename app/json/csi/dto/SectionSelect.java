package json.csi.dto;

public class SectionSelect {

    private Integer id;

    public SectionSelect(Integer id) {
        this.id = id;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

}
