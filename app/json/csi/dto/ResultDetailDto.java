package json.csi.dto;

public class ResultDetailDto {

  Integer idAgrupador;
  Double result;
  Double detractores;
  Double neutros;
  Double promotores;
  
  Double p1;
  Double p2;
  Double p3;
  Double p4;
  Double p5;
  Double p6;
  Double p7;
  Double p8;
  Double p9;
  Double p10;
  Double p11;
  Double p12;
  Double p13;
  Double p14;
  Double p15;
  



public ResultDetailDto(Integer idAgrupador, Double result, Double detractores, Double neutros, Double promotores,
		Double p1, Double p2, Double p3, Double p4, Double p5, Double p6, Double p7, Double p8, Double p9,
		Double p10, Double p11, Double p12, Double p13, Double p14, Double p15) {
	super();
	this.idAgrupador = idAgrupador;
	this.result = roundDouble(result);
	this.detractores = roundDouble(detractores);
	this.neutros = roundDouble(neutros);
	this.promotores = roundDouble(promotores);
	this.p1 = roundDouble(p1);
	this.p2 = roundDouble(p2);
	this.p3 = roundDouble(p3);
	this.p4 = roundDouble(p4);
	this.p5 = roundDouble(p5);
	this.p6 = roundDouble(p6);
	this.p7 = roundDouble(p7);
	this.p8 = roundDouble(p8);
	this.p9 = roundDouble(p9);
	this.p10 = roundDouble(p10);
	this.p11 = roundDouble(p11);
	this.p12 = roundDouble(p12);
	this.p13 = roundDouble(p13);
	this.p14 = roundDouble(p14);
	this.p15 = roundDouble(p15);
}
private double roundDouble(Double result) {
	
	if (result == null){
		return 0;
	}
	return Math.round(result  * 100.0) / 100.0;
}
public Integer getIdAgrupador() {
	return idAgrupador;
}
public void setIdAgrupador(Integer idAgrupador) {
	this.idAgrupador = idAgrupador;
}
public Double getResult() {
	return result;
}
public void setResult(Double result) {
	this.result = result;
}
public Double getDetractores() {
	return detractores;
}
public void setDetractores(Double detractores) {
	this.detractores = detractores;
}
public Double getNeutros() {
	return neutros;
}
public void setNeutros(Double neutros) {
	this.neutros = neutros;
}
public Double getPromotores() {
	return promotores;
}
public void setPromotores(Double promotores) {
	this.promotores = promotores;
}
public Double getP1() {
	return p1;
}
public void setP1(Double p1) {
	this.p1 = p1;
}
public Double getP2() {
	return p2;
}
public void setP2(Double p2) {
	this.p2 = p2;
}
public Double getP3() {
	return p3;
}
public void setP3(Double p3) {
	this.p3 = p3;
}
public Double getP4() {
	return p4;
}
public void setP4(Double p4) {
	this.p4 = p4;
}
public Double getP5() {
	return p5;
}
public void setP5(Double p5) {
	this.p5 = p5;
}
public Double getP6() {
	return p6;
}
public void setP6(Double p6) {
	this.p6 = p6;
}
public Double getP7() {
	return p7;
}
public void setP7(Double p7) {
	this.p7 = p7;
}
public Double getP8() {
	return p8;
}
public void setP8(Double p8) {
	this.p8 = p8;
}
public Double getP9() {
	return p9;
}
public void setP9(Double p9) {
	this.p9 = p9;
}
public Double getP10() {
	return p10;
}
public void setP10(Double p10) {
	this.p10 = p10;
}
public Double getP11() {
	return p11;
}
public void setP11(Double p11) {
	this.p11 = p11;
}
public Double getP12() {
	return p12;
}
public void setP12(Double p12) {
	this.p12 = p12;
}
public Double getP13() {
	return p13;
}
public void setP13(Double p13) {
	this.p13 = p13;
}
public Double getP14() {
	return p14;
}
public void setP14(Double p14) {
	this.p14 = p14;
}
public Double getP15() {
	return p15;
}
public void setP15(Double p15) {
	this.p15 = p15;
}



}
