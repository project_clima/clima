package json.csi.dto;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class CsiDBReporDTO {

	private Integer idEvaluated;

	private Integer idEvaluator;

	private String firstName;

	private String lastName;

	private String zona;

	private String subDivision;

	private String almacen;

	private String evaluator;

	private String area4;

	private String area5;

	private String area6;

	private String evaluated;

	private Date fecha;

	private String nombreEvaluador;

	private List<CsiDBRepor> preguntaRespuestaList;

	public CsiDBReporDTO() {
	}

	public CsiDBReporDTO(Integer idEvaluated, Integer idEvaluator, String firstName, String lastName, String zona,
			String subDivision, String almacen, String evaluator, String area4, String area5, String area6, Date fecha,
			String nombreEvaluador) {
		super();
		this.idEvaluated = idEvaluated;
		this.idEvaluator = idEvaluator;
		this.firstName = firstName;
		this.lastName = lastName;
		this.zona = zona;
		this.subDivision = subDivision;
		this.almacen = almacen;
		this.evaluator = evaluator;
		this.area4 = area4;
		this.area5 = area5;
		this.area6 = area6;
		this.evaluated = area6;
		this.fecha = fecha;
		this.nombreEvaluador = nombreEvaluador;
	}

	public Integer getIdEvaluated() {
		return idEvaluated;
	}

	public void setIdEvaluated(Integer idEvaluated) {
		this.idEvaluated = idEvaluated;
	}

	public Integer getIdEvaluator() {
		return idEvaluator;
	}

	public void setIdEvaluator(Integer idEvaluator) {
		this.idEvaluator = idEvaluator;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getZona() {
		return zona;
	}

	public void setZona(String zona) {
		this.zona = zona;
	}

	public String getSubDivision() {
		return subDivision;
	}

	public void setSubDivision(String subDivision) {
		this.subDivision = subDivision;
	}

	public String getAlmacen() {
		return almacen;
	}

	public void setAlmacen(String almacen) {
		this.almacen = almacen;
	}

	public String getEvaluator() {
		return evaluator;
	}

	public void setEvaluator(String evaluator) {
		this.evaluator = evaluator;
	}

	public String getArea4() {
		return area4;
	}

	public void setArea4(String area4) {
		this.area4 = area4;
	}

	public String getArea5() {
		return area5;
	}

	public void setArea5(String area5) {
		this.area5 = area5;
	}

	public String getArea6() {
		return area6;
	}

	public void setArea6(String area6) {
		this.area6 = area6;
	}

	public String getEvaluated() {
		return evaluated;
	}

	public void setEvaluated(String evaluated) {
		this.evaluated = evaluated;
	}

	public Date getFecha() {
		return fecha;
	}

	public void setFecha(Date fecha) {
		this.fecha = fecha;
	}

	public String getNombreEvaluador() {
		return nombreEvaluador;
	}

	public void setNombreEvaluador(String nombreEvaluador) {
		this.nombreEvaluador = nombreEvaluador;
	}

	public void setPreguntaRespuestaList(List<CsiDBRepor> preguntaRespuestaList) {
		this.preguntaRespuestaList = preguntaRespuestaList;
	}

	public List<CsiDBRepor> getPreguntaRespuestaList() {
		if (preguntaRespuestaList == null) {
			preguntaRespuestaList = new ArrayList<>();
		}
		return preguntaRespuestaList;
	}

}
