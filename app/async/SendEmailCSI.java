package async;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.mail.MessagingException;
import play.jobs.*;
import models.*;
import play.Logger;
import play.Play;
import play.mvc.Http.Request;
import utils.GmailMailer;

public class SendEmailCSI extends Job<String> {
    public Request request;
    public int surveyId;
    public int templateId;
    public String employeesToSend;
    public final String salt = Play.configuration.getProperty("application.secret");
    
    public SendEmailCSI(int surveyId, int templateId, String employees, Request request) {
        this.surveyId        = surveyId;
        this.templateId      = templateId;
        this.employeesToSend = employees;
        this.request         = request;
    }
    
    public String doJobWithResult() {
        return sendSurveyEmail();
    }
    /**
     * Método que se encarga de enviar el email
     * @return String
     */
    private String sendSurveyEmail() {
        Survey survey                = Survey.findById(this.surveyId);
        Map<String, String> template = getEmailTemplate();
        List<EmployeeCSI> recipients = getRecipients();
        String emailContents;        
        
        try {
            GmailMailer mailer = new GmailMailer();
            
            // Connect transport
            mailer.connect();
            
            int emailCounter = 1;
            
            for (EmployeeCSI recipient : recipients) {
                emailContents = prepareTemplate(
                    template.get("body"),
                    survey,
                    recipient.firstName,
                    recipient.firstName + " " + recipient.lastName,
                    getSurveyLink(survey.id, recipient.id)
                );
                
                mailer.send(
                    recipient.email,
                    template.get("subject"),
                    emailContents,
                    survey.id,
                    null
                );
                
                Logger.info(recipient.email);
                
                if (emailCounter++ % 50 == 0) {
                    mailer.close();
                    mailer.connect();
                }
				
                //Thread.sleep(1000);
            }
            
            // Save send date
            survey.sendDate = new Date();
            survey.save();
            
            // Close transport
            mailer.close();        
            
            return "El envío ha finalizado correctamente";
        } catch (MessagingException ex) {
            return ex.getMessage();
        }
    }
    
    /**
     * Obtienen los empleados a los que se les enviará el email
     * @return List de EmployeeCSI
     */
    private List<EmployeeCSI> getRecipients() {
        StringBuilder query = new StringBuilder();
        
        query.append("select employee")
            .append(" from EmployeeCSI employee")
            .append(" where employee.id IN ")
            .append(" (")
            .append(" select distinct idEvaluatorEmployee from CsiEvaluationCross where idSurvey = ?1");
        
        if (this.employeesToSend != null && !this.employeesToSend.isEmpty()) {
            query.append(" and idEvaluatorEmployee IN (")
                .append(this.employeesToSend)
                .append(")");
        }
        
        query.append(" )");
        
        return EmployeeCSI.find(query.toString(), this.surveyId).fetch();
    }
    
    /**
     * Obtiene el Asunto y Cuerpo del email que será enviado
     * @return Map
     */
    private Map getEmailTemplate() {
        EmailTemplate template = EmailTemplate.findById(this.templateId);
        
        String subject = template != null ? template.subject : "";
        String body    = template != null ? template.body : "";
        
        Map<String, String> templateParts = new HashMap();
        templateParts.put("subject", subject);
        templateParts.put("body", body);
        
        return templateParts;
    }
    
    /**
     * Obtiene el link de la encuesta
     * @param surveyId
     * @param employeeId
     * @return String
     */
    private String getSurveyLink(int surveyId, int employeeId) {
        String baseUrl = Play.configuration.getProperty("application.baseUrl");
        String userId  = utils.Misc.md5(employeeId + this.salt);
        return baseUrl + "surveys/csi/apply/" +  surveyId + "/" + userId;
    }
    
    /**
     * Obtiene el template del email
     * @param template
     * @param survey
     * @param name
     * @param fullName
     * @param link
     * @return String
     */
    private String prepareTemplate(String template, Survey survey, String name,
                                   String fullName, String link) {        
        HashMap<String, Object> vars = new HashMap();
        SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
        
        vars.put("%collaborator.name%", name);
        vars.put("%collaborator.fullName%", fullName);
        vars.put("%survey.name%", survey.name);
        vars.put("%survey.start%", sdf.format(survey.dateInitial));
        vars.put("%survey.end%", sdf.format(survey.dateEnd));
        vars.put("%survey.link%", link);
        
        if (template != null) {
            for (String key : vars.keySet()) {
                String contents = vars.get(key) != null ? vars.get(key).toString(): "";
                template= template.replace(key, contents);
            }
        }
        
        return template;
    }
}

