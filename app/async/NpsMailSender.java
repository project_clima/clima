package async;

import java.util.List;

import javax.mail.MessagingException;

import json.nps.dto.NpsMailDto;
import models.NpsMailLog;
import play.Logger;
import utils.GmailMailer;

public class NpsMailSender {

    private List<NpsMailDto> mails;

    public NpsMailSender(List<NpsMailDto> mails) {
        this.mails = mails;
    }

    public void run() {

        try {
            GmailMailer mailer = new GmailMailer();
            mailer.connect();
            int emailCounter = 1;

            for (NpsMailDto npsMailDto : mails) {

                mailer.send(npsMailDto.getEmail(), npsMailDto.getSubject(), npsMailDto.getMailBody(),
                        npsMailDto.getSurvey(), "");

                if (emailCounter++ % 50 == 0) {
                    mailer.close();
                    mailer.connect();
                }

                NpsMailLog mailLog = npsMailDto.getMailLog();
                mailLog.save();
            }

            mailer.close();

        } catch (MessagingException e) {
            Logger.error("Ocurrió un error al enviar los correos ", e.fillInStackTrace());
            e.printStackTrace();
        }
    }

}
