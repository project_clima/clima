package models.enume;

public enum NpsTemplateTypeEnum {

    FELICITACION {
        @Override
        public Integer getId() {

            return 1;
        }
    },

    OPORTUNIDAD_MEJORA {
        @Override
        public Integer getId() {
            return 2;
        }
    },
    EXHORTACION {
        @Override
        public Integer getId() {
            return 3;
        }
    },
    CARTA_JEFE {
        @Override
        public Integer getId() {
            return 4;
        }
    },
    CARTA_GENERICA {
        @Override
        public Integer getId() {
            return 5;
        }
    };

    public abstract Integer getId();

}
