package models.enume;

public enum ErrorTypeEnum {
	
	USER_NOT_EXISTS {
		@Override
		public Integer getId() {
			
			return 1;
		}
	},
	
	USER_ID_NOT_EXISTS {
		@Override
		public Integer getId() {
			return 2;
		}
	},
	
	EMPLOYEE_EVALUATOR_CROSS_NOT_EXISTS {
		@Override
		public Integer getId() {
			
			return 3;
		}
	},
	EMPLOYEE_EVALUATED_CROSS_NOT_EXISTS {
		@Override
		public Integer getId() {
			
			return 4;
		}
	},
	EMPLOYEE_EVALUATOR_RESULTS_NOT_EXISTS {
		@Override
		public Integer getId() {
			
			return 5;
		}
	},
	EMPLOYEE_EVALUATED_RESULTS_NOT_EXISTS {
		@Override
		public Integer getId() {
	
			return 6;
		}
	}, 
	EMPLOYEE_NOT_FORMAT {
		@Override
		public Integer getId() {			
			return 7;
		}
	};
	
	
	public abstract Integer getId();

}
