package models;

import play.data.validation.*;
import play.db.jpa.*;
import javax.persistence.*;
import java.util.*;

@Entity
@Table(name = "PUL_SURVEY_SCORE_LOG", schema = "PORTALUNICO")
public class ScoreTask extends GenericModel {
    @Id
    @Column(name = "FN_LOGID", nullable = true)
    @SequenceGenerator(name = "SQ_ID_SURVEY_SCORE", sequenceName = "PORTALUNICO.SQ_ID_SURVEY_SCORE", allocationSize = 1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "SQ_ID_SURVEY_SCORE")
    public Long id;

    @Required
    @MaxSize(255)
    @Column(name = "FD_STRUCTURE_DATE", length = 20)
    public String structureDate;
    
    @Column(name = "FD_EXEC_DATE")
    public Date executionDate;
    
    @Column(name = "FC_STATUS", length = 1)
    public String status;
    
    @Column(name = "FD_CRE_DATE")
    public Date creationDate;

    @Column(name = "FD_MOD_DATE")
    public Date modificationDate;

    @Column(name = "FC_CRE_FOR")
    public String createdBy;

    @Column(name = "FC_MOD_FOR")
    public String modifiedBy;
}
