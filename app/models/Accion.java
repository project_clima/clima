    package models;

import play.data.validation.*;
import play.db.jpa.*;
import javax.persistence.*;
import java.util.*;


@Entity
@Table(name = "PUL_ACTION", schema = "PORTALUNICO")
public class Accion extends GenericModel {
    @Id
    @Column(name = "FN_ACTION_ID", nullable = true)
    @GeneratedValue(strategy = GenerationType.AUTO)    
    public Long id;

    @Required
    @MaxSize(100)
    @Column(name = "FC_DESC_ACTION",length = 100)
    public String nombreAccion;

    @Required
    @MaxSize(100)
    @Column(name = "FC_URL_MAPPING", length = 100)
    public String urlMapping;

    @ManyToOne
    @JoinColumn(name = "FN_GRANT_ID")
    public Permiso permiso;

    @Column(name = "FC_MOD_FOR")
    public String modifiedBy;

    @Column(name = "FC_CRE_FOR") 
    public String createdBy;

    @Column(name = "FD_CRE_DATE") 
    public Date createdDate;

    @Column(name = "FD_MOD_DATE")
    public Date modifiedDate;
}