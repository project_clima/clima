package models;

import java.util.Date;
import play.data.validation.*;
import play.db.jpa.*;
import javax.persistence.*;

@Entity
@Table(name = "PUL_EMAIL_TEMPLATE", schema = "PORTALUNICO")
public class EmailTemplate extends GenericModel {
    @Id
    @Column(name = "FN_ID_TEMPLATE", nullable = true)
    @GeneratedValue(strategy = GenerationType.AUTO)    
    public Integer id;
    
    @Required
    @Column(name = "FC_TEMPLATE_BODY", columnDefinition="CLOB")
    public String body;
    
    @Required
    @MaxSize(255)
    @Column(name = "FC_TEMPLATE_SUBJECT", length = 255)
    public String subject;
    
    @Required
    @ManyToOne
    @JoinColumn(name = "FN_ID_SURVEY_TYPE")
    public SurveyType type;
    
    @Column(name = "FD_CRE_DATE")
    public Date creationDate;
    
    @Column(name = "FD_MOD_DATE")
    public Date modificationDate;
    
    @Column(name = "FC_CRE_FOR")
    public String createdBy;
    
    @Column(name = "FC_MOD_FOR")
    public String modifiedBy;
}