package models;

import play.data.validation.MaxSize;
import play.data.validation.Required;
import play.db.jpa.*;
import javax.persistence.*;
import java.util.*;

@Entity
@Table(name = "PUL_GRANT", schema = "PORTALUNICO")
public class Permiso extends GenericModel {
    @Id
    @Column(name = "FN_GRANT_ID", nullable = true)
    @GeneratedValue(strategy = GenerationType.AUTO)    
    public Long id;
    
    @Required
    @MaxSize(100)
    @Column(name = "FC_DESC_GRANT", length = 100)
    public String nombrePermiso;

    @OneToMany
    @JoinColumn(name = "FN_GRANT_ID")
    public List<Accion> acciones;

    @Required
    @ManyToOne
    @JoinColumn(name = "FN_SECTION_ID")
    public Seccion seccion;

    @Column(name = "FC_MOD_FOR")
    public String modifiedBy;

    @Column(name = "FC_CRE_FOR") 
    public String createdBy;

    @Column(name = "FD_CRE_DATE") 
    public Date createdDate;

    @Column(name = "FD_MOD_DATE")
    public Date modifiedDate;
}
