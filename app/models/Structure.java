package models;

import play.db.jpa.*;
import javax.persistence.*;
import java.util.*;

@Entity
@Table(name = "PUL_structure", schema = "PORTALUNICO")
public class Structure extends GenericModel {

    @Id
    @Column(name = "FN_USERID", nullable = true)
    @GeneratedValue(strategy = GenerationType.AUTO)
    public Long userid;

    @Column(name = "FN_EMPID")
    public Integer empid;

    @Column(name = "FN_ID_STATUS_EMP")
    public Integer idStatusEmp;

    @Column(name = "FC_FIRSTNAME")
    public String firstname;

    @Column(name = "FC_LASTNAME")
    public String lastname;

    @Column(name = "FC_EMAIL")
    public String email;

    @Column(name = "FC_AREA")
    public String area;

    @Column(name = "FN_MANAGER")
    public Integer manager;

    @Column(name = "FN_HR")
    public Integer hr;

    @Column(name = "FN_KEY_PLANT")
    public String keyPlan;

    @Column(name = "FN_ID_STORE")
    public Integer idStore;

    @Column(name = "FN_ID_SECTOR")
    public Integer idSector;

    @Column(name = "FN_DESC_SECTOR")
    public String descriptionSector;

    @Column(name = "FN_ID_LOCATION")
    public Integer idLocation;

    @Column(name = "FN_DESC_LOCATION")
    public String descriptionLocation;

    @Column(name = "FC_MANAGEMENT")
    public String management;

    @Column(name = "FN_ID_DEPARTMENT")
    public Integer idDepartment;

    @Column(name = "FN_ID_FUNCTION")
    public Integer idFunction;

    @Column(name = "FD_HIREDATE")
    public Date hireDate;

    @Column(name = "FC_COUNTRY")
    public String country;

    @Column(name = "FC_CUSTOM01")
    public String custom01;

    @Column(name = "FC_CUSTOM03")
    public String custom03;

    @Column(name = "FC_CUSTOM04")
    public String custom04;

    @Column(name = "FC_CUSTOM05")
    public String custom05;

    @Column(name = "FC_CUSTOM06")
    public String custom06;

    @Column(name = "FC_CUSTOM07")
    public String custom07;

    @Column(name = "FC_CUSTOM08")
    public String custom08;

    @Column(name = "FC_CUSTOM09")
    public String custom09;

    @Column(name = "FN_ID_CUSTOM09")
    public String idCustom09;

    @Column(name = "FC_CUSTOM10")
    public String custom10;

    @Column(name = "FC_CUSTOM_MANAGER")
    public String customManager;

    @Column(name = "FC_DEFAULT_LOCALE")
    public String defaultLocale;

    @Column(name = "FC_GENDER")
    public String gender;

    @Column(name = "FC_TIMEZONE")
    public String timezone;

    @Column(name = "FC_TITLE")
    public String title;

    @Column(name = "FC_MOD_FOR")
    public String modifiedBy;

    @Column(name = "FC_CRE_FOR")
    public String createdBy;

    @Column(name = "FD_CRE_DATE")
    public Date createdDate;

    @Column(name = "FD_MOD_DATE")
    public Date modifiedDate;
      
}
