package models;

import play.data.validation.*;
import play.db.jpa.*;
import javax.persistence.*;
import java.util.*;

@Entity
@Table(name = "PUL_PLANT", schema = "PORTALUNICO")
public class PlantEmployee extends GenericModel {

        @Id
        @Column(name = "FN_KEY_PLANT", nullable = true)
        @GeneratedValue(strategy = GenerationType.AUTO)    
        public String clave;

        @Required
        @MaxSize(100)
        @Column(name = "FC_NAME_PLANT", length = 100)
        public String plantEmployee;
        
        @Column(name = "FC_MOD_FOR")
        public String modifiedBy;

        @Column(name = "FC_CRE_FOR") 
        public String createdBy;

        @Column(name = "FD_CRE_DATE") 
        public Date createdDate;

        @Column(name = "FD_MOD_DATE")
        public Date modifiedDate;
}
