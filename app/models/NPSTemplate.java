package models;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import play.data.validation.Required;
import play.db.jpa.GenericModel;

@Entity
@Table(name = "PUL_NPS_TEMPLATE", schema = "PORTALUNICO")
public class NPSTemplate extends GenericModel {

    @Id
    @Column(name = "FN_ID_TEMPLATE", nullable = true)
    @GeneratedValue(strategy = GenerationType.AUTO)
    public Integer id;

    @Required
    @Column(name = "FC_TEMPLATE", columnDefinition = "CLOB")
    public String bodyTemplete;

    @Column(name = "FC_DES_TEMPLATE")
    public String descTemplate;

    @Column(name = "FN_TYPE")
    public Integer idType;
}
