/**
 * @(#) NpsAnswerTmp 28/03/2017
 */

package models.nps;

import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import play.db.jpa.GenericModel;
import play.db.jpa.Model;

/**
 * Clase entidad para la tabla PUL_NPS_ANSWER_TMP
 * @author Rodolfo Miranda -- Qualtop
 */
@Entity
@Table(name = "PUL_NPS_ANSWER_TMP", schema = "PORTALUNICO")
public class NpsAnswerTmp extends GenericModel{
    
    @Id
    @SequenceGenerator(name="SQL_ID_TMP", sequenceName="PORTALUNICO.SQ_NPS_ID_ANWER_TMP")
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "SQL_ID_TMP")
    @Column(name="FN_ID_ANWER_TMP")
    public int idTmp;
    
    @Column(name = "FC_EMPID")
    public String empId;
    
    @Column(name = "FC_ID_AREA")
    public String area;
    
    @Column(name = "FC_ID_PRACTICES")
    public String practice;
    
    @Column(name = "FC_YEAR")
    public String year;
    
    @Column(name = "FC_MONTH")
    public String month;
    
    @Column(name = "FC_DAY")
    public String day;
    
    @Column(name = "FC_ID_PERSON_EVALUATED")
    public String personEvaluated;
    
    @Column(name = "FC_ANSWER_1")
    public String answer1;
    
    @Column(name = "FC_ANSWER_2")
    public String answer2;
    
    @Column(name = "FC_ANSWER_3")
    public String answer3;
    
    @Column(name = "FC_RECORDING")
    public String recording;
    
    @Column(name = "FC_CLIENT_ID")
    public String clientId;
    
    @Column(name = "FC_ID_CATEGORY_1_1")
    public String category1_1;
    
    @Column(name = "FC_ID_CATEGORY_1_2")
    public String category1_2;
    
    @Column(name = "FC_ID_CATEGORY_1_3")
    public String category1_3;
    
    @Column(name = "FC_CLIENT_NAME")
    public String clientName;
    
    @Column(name = "FC_LADA")
    public String lada;
    
    @Column(name = "FC_PHONE")
    public String phone;
    
    @Column(name = "FC_ID_STORE")
    public String store;
    
    @Column(name = "FC_ANSWER_4")
    public String answer4;
    
    @Column(name = "FC_EMAIL")
    public String email;
    
    @Column(name = "FC_ANSWER_5")
    public String answer5;
    
    @Column(name = "FC_SURVEY_TYPE")
    public String surveyType;
    
    @Column(name = "FC_ID_CATEGORY_2_1")
    public String category2_1;
    
    @Column(name = "FC_ID_CATEGORY_2_2")
    public String category2_2;
    
    @Column(name = "FC_ID_CATEGORY_2_3")
    public String category2_3;
    
    @Column(name = "FC_DOC")
    public String doc;
    
    @Column(name = "FC_TERMINAL")
    public String terminal;
    
    @Column(name = "FC_TICKET_DAY")
    public String ticketDay;
    
    @Column(name = "FC_TICKET_MONTH")
    public String tickectMonth;
    
    @Column(name = "FC_TICKET_YEAR")
    public String ticketYear;
    
    @Column(name = "FC_IP")
    public String ip;
    
    @Column(name = "FC_SKU")
    public String sku;
  
    @Column(name = "FC_DESC_SKU")
    public String skuDesc;      
            
    @Column(name = "FN_COLUMN_COUNT")
    public Integer columns;
    
    @Column(name = "FC_CRE_FOR")
    public String creFor;
    
    @Column(name = "FD_CRE_DATE")
    public Date creDate;
    
    @Column(name = "FC_MOD_FOR")
    public String modFor;
    
    @Column(name = "FD_MOD_DATE")
    public Date modDate;

    @Override
    public String toString() {
        return "NpsAnswerTmp{" + "empId=" + empId + ", area=" + area +
                ", practice=" + practice + ", year=" + year + 
                ", month=" + month + ", day=" + day + 
                ", personEvaluated=" + personEvaluated + 
                ", answer1=" + answer1 + ", answer2=" + answer2 + 
                ", answer3=" + answer3 + ", recording=" + recording + 
                ", clientId=" + clientId + ", category1_1=" + category1_1 + 
                ", category1_2=" + category1_2 + 
                ", category1_3=" + category1_3 + ", clientName=" + clientName + 
                ", lada=" + lada + ", phone=" + phone + ", store=" + store + 
                ", answer4=" + answer4 + ", email=" + email + 
                ", answer5=" + answer5 + ", surveyType=" + surveyType + 
                ", category2_1=" + category2_1 + ", category2_2=" + category2_2 + 
                ", category2_3=" + category2_3 + ", doc=" + doc + 
                ", terminal=" + terminal + ", ticketDay=" + ticketDay + 
                ", tickectMonth=" + tickectMonth + ", ticketYear=" + ticketYear + 
                ", ip=" + ip + ", creFor=" + creFor + ", creDate=" + creDate + 
                ", modFor=" + modFor + ", modDate=" + modDate + '}';
    }
}
