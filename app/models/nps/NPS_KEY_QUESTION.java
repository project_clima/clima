/**
 * @(#) NPS_KEY_QUESTION 8/05/2017
 */

package models.nps;

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Embeddable;

/**
 * Clase entidad para la tabla NPS_KEY_QUESTION
 * @author Rodolfo Miranda -- Qualtop
 */
@Embeddable
class NPS_KEY_QUESTION implements Serializable{

    @Column(name = "FN_ID_ANSWER", nullable = false)
    public Integer idAnswer;

    @Column(name = "FN_ID_QUESTION", nullable = false)
    public Integer idQuestion;

    public NPS_KEY_QUESTION() {

    }

    public NPS_KEY_QUESTION(Integer idAnswer, Integer idQuestion) {
        this.idAnswer = idAnswer;
        this.idQuestion = idQuestion;
    }

    public boolean equals(Object object) {
        if (object instanceof NPS_KEY_QUESTION) {
            NPS_KEY_QUESTION pk = (NPS_KEY_QUESTION)object;
            return idAnswer == pk.idAnswer && idQuestion == pk.idQuestion ;
        } else {
            return false;
        }
    }

    public int hashCode() {
        return (Integer)(idAnswer + idQuestion);
    }
}
