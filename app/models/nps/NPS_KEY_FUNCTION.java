/**
 * @(#) NPS_KEY_FUNCTION 30/06/2017
 */

package models.nps;

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Embeddable;

/**
 * Clase entidad para la tabla NPS_KEY_FUNCTION
 * @author Rodolfo Miranda -- Qualtop
 */
@Embeddable
public class NPS_KEY_FUNCTION implements Serializable{


    @Column(name = "FC_TITLE", nullable = false)
    public String title;

    @Column(name = "FN_ID_FUNCTION", nullable = false)
    public Integer idFunction;

    public NPS_KEY_FUNCTION() {

    }

    public NPS_KEY_FUNCTION(String title, Integer idFunction) {
        this.title = title;
        this.idFunction = idFunction;
    }

    public boolean equals(Object object) {
        if (object instanceof NPS_KEY_FUNCTION) {
            NPS_KEY_FUNCTION pk = (NPS_KEY_FUNCTION)object;
            return title.equals(pk.title) && idFunction.equals(pk.idFunction) ;
        } else {
            return false;
        }
    }

    public int hashCode() {
        return (int)(title.length() + idFunction);
    }
}
