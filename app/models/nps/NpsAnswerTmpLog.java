/**
 * @(#) NpsAnswerTmpLog 17/05/2017
 */

package models.nps;

import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import play.db.jpa.GenericModel;

/**
 * Clase entidad para la tabla PUL_NPS_ANSWER_TMP_LOG
 * @author Rodolfo Miranda -- Qualtop
 */
@Entity
@Table(name="PUL_NPS_ANSWER_TMP_LOG", schema = "PORTALUNICO")
public class NpsAnswerTmpLog extends GenericModel{
    
    @Id
    @Column(name="FN_ID_ANWER_TMP_LOG")
    public int id;
    
    @Column(name="FC_EMPID")
    public String emId;
   
    @Column(name="FC_ID_AREA")
    public String idArea;

    @Column(name="FC_ID_PRACTICES")
    public String idPractices;
 
    @Column(name="FC_YEAR")
    public String year;
    
    @Column(name="FC_MONTH")
    public String month;

        @Column(name="FC_DAY")
        public String day;

        @Column(name="FC_ID_PERSON_EVALUATED")
        public String idPersonEva;

        @Column(name="FC_ANSWER_1")
        public String ans1;

        @Column(name="FC_ANSWER_2")
        public String ans2;

        @Column(name="FC_ANSWER_3")
        public String ans3;
 
        @Column(name="FC_RECORDING")
        public String rcording;
 
        @Column(name="FC_CLIENT_ID")
        public String idClient;
 
        @Column(name="FC_ID_CATEGORY_1_1")
        public String idCat1_1;

        @Column(name="FC_ID_CATEGORY_1_2")
        public String idCat1_2;
 
        @Column(name="FC_ID_CATEGORY_1_3")
        public String idCat1_3;

        @Column(name="FC_CLIENT_NAME")
        public String clientName;
 
        @Column(name="FC_LADA")
        public String lada;
 
        @Column(name="FC_PHONE")
        public String phone;
 
        @Column(name="FC_ID_STORE")
        public String store;
 
        @Column(name="FC_ANSWER_4")
        public String ans4;
 
        @Column(name="FC_EMAIL")
        public String email;
 
        @Column(name="FC_ANSWER_5")
        public String ans5;

        @Column(name="FC_SURVEY_TYPE")
        public String surveyType;
 
        @Column(name="FC_ID_CATEGORY_2_1")
        public String cat2_1;

        @Column(name="FC_ID_CATEGORY_2_2")
        public String cat2_2;
 
        @Column(name="FC_ID_CATEGORY_2_3")
        public String cat2_3;
 
        @Column(name="FC_DOC")
        public String doc;

        @Column(name="FC_TERMINAL")
        public String terminal;
 
        @Column(name="FC_TICKET_DAY")
        public String tickeyDay;
 
        @Column(name="FC_TICKET_MONTH")
        public String ticketMonth;
 
        @Column(name="FC_TICKET_YEAR")
        public String ticketYear;

        @Column(name="FC_IP")
        public String ip;

        @Column(name="FC_SKU")
        public String sku;
 
        @Column(name="FC_DESC_SKU")
        public String descSku;

        @Column(name="FC_CRE_FOR")
        public String creFor;

        @Column(name="FD_CRE_DATE")
        public Date creDate;

        @Column(name="FC_MOD_FOR")
        public String modFor;

        @Column(name="FD_MOD_DATE")
        public Date modDate;

    @Column(name="FC_COMMENTS")
    public String comments;
}
