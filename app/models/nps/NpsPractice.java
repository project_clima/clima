/**
 * @(#) NpsPractice 4/05/2017
 */

package models.nps;

import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import play.db.jpa.GenericModel;

/**
 * Clase entidad para la tabla PUL_NPS_PRACTICE
 * @author Rodolfo Miranda -- Qualtop
 */
@Entity
@Table(name="PUL_NPS_PRACTICE" , schema="PORTALUNICO")
public class NpsPractice extends GenericModel{

    @Id
    @Column( name="FC_ID_PRACTICE",nullable = false )
    //@GeneratedValue(strategy = GenerationType.AUTO)
    public String practice;
    
    @Column( name="FC_DESC_PRACTICE",nullable = false )
    public String desc;
    
    @Column( name="FC_ID_AREA",nullable = false )
    public String idArea;
    
    @Column( name="FD_CRE_DATE",nullable = false )
    public Date creDate;
    
    @Column( name="FD_MOD_DATE",nullable = false )
    public Date modDate;
    
    @Column( name="FC_CRE_FOR",nullable = false )
    public String creFor;
    
    @Column( name="FC_MOD_FOR",nullable = false )
    public String modFor;
   
}
