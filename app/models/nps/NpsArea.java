/**
 * @(#) NpsArea 6/04/2017
 */

package models.nps;

import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import play.db.jpa.GenericModel;

/**
 * Clase entidad para la tabla PUL_NPS_AREA
 * @author Rodolfo Miranda -- Qualtop
 */
@Entity
@Table(name="PUL_NPS_AREA", schema = "PORTALUNICO")
public class NpsArea extends GenericModel{

    @Id
    @Column(name="FC_ID_AREA", nullable = false)
    //@GeneratedValue(strategy = GenerationType.AUTO)
    public String idArea;
    
    @Column(name="FC_DESC_AREA", nullable = false)
    public String desc;
 
    @Column(name="FD_CRE_DATE")
    public Date creDate;
    
    @Column(name="FD_MOD_DATE")
    public Date modDate;
    
    @Column(name="FC_CRE_FOR")
    public String creFor;        
    
    @Column(name="FC_MOD_FOR")
    public String modFor;
            
}
