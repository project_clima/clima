/**
 * @(#) NpsAnswer 20/04/2017
 */

package models.nps;

import play.db.jpa.GenericModel;

/**
 *
 * @author Rodolfo Miranda -- Qualtop
 */
//@Entity
//@Table(name = "PUL_NPS_ANSWER", schema = "PORTALUNICO")
public class NpsAnswer extends GenericModel{
//    @Id
//    @Column(name="FN_ID_ANSWER",nullable = false)
//    public Integer idAnswer;
//    
//    @OneToMany(mappedBy="key.idAnswer", fetch=FetchType.LAZY)
//    @IndexColumn(name = "FN_ID_IMPUGN", base=0)
//    public List<Challenge> challenges;
//    
//    @OneToMany(mappedBy="keyQ.idAnswer", fetch=FetchType.LAZY)
//    @IndexColumn(name = "FN_ID_QUESTION", base=0)
//    public List<QuestionNpsAnswer> qualification;
//    
//    @Column(name="FN_EMPLOYEE_NUMBER",nullable = false)
//    public Integer emNumber;
//    
//    @Column(name="FN_ID_EMPLOYEE_NPS")
//    public Integer idEmployeeNps;
//    
//    @Column(name="FD_STRUCTURE_DATE")
//    public Date structDate;
//    
//    @Column(name="FC_ID_AREA")
//    public String idArea;
//    
//    @Column(name="FC_ID_PRACTICE")
//    public String idPractice;
//    
//    @Column(name="FN_YEAR")
//    public Integer year;
//    
//    @Column(name="FN_MONTH")
//    public Integer month;
//    
//    @Column(name="FN_DAY")
//    public Integer day;
//    
//    @Column(name="FN_ID_PERSON_EVALUATED")
//    public String idPersonEvaluated;
//    
//    @Column(name="FC_RECORDING")
//    public String recording;
//            
//    @Column(name="FC_CLIENT_ID")
//    public String clientId;
//            
//    @Column(name="FC_ID_CATEGORY_1_1")
//    public String idCat1_1;
//            
//    @Column(name="FC_ID_CATEGORY_1_2")
//    public String idCat1_2;
//            
//    @Column(name="FC_ID_CATEGORY_1_3")
//    public String idCat1_3;
//            
//    @Column(name="FC_CLIENT_NAME") 
//    public String clientName;
//            
//    @Column(name="FC_LADA")
//    public String lada;
//            
//    @Column(name="FC_PHONE")
//    public String phone;
//            
//    @Column(name="FN_ID_STORE")
//    public Integer idStore;
//    
//    @Column(name="FC_EMAIL")
//    public String email;
//    
//    @Column(name="FN_SURVEY_ORIGIN_ID")
//    public Integer surveyId;
//    
//    @Column(name="FC_ID_CATEGORY_2_1")
//    public String idCat2_1;
//    
//    @Column(name="FC_ID_CATEGORY_2_2")
//    public String idCat2_2;
//    
//    @Column(name="FC_ID_CATEGORY_2_3")
//    public String idCat2_3;
//    
//    @Column(name="FC_DOC")
//    public String doc;
//    
//    @Column(name="FC_TERMINAL")
//    public String terminal;
//    
//    @Column(name="FC_TICKET_DAY")
//    public String ticketDay;
//    
//    @Column(name="FC_TICKET_MONTH")
//    public String ticketMonth;
//    
//    @Column(name="FC_TICKET_YEAR")
//    public String ticketYear;
//    
//    @Column(name="FC_IP")
//    public String ip;
//    
//    @Column(name="FC_CRE_FOR")
//    public String creFor;
//    
//    @Column(name="FD_CRE_DATE")
//    public Date creDate;
//    
//    @Column(name="FC_MOD_FOR")
//    public String modFor;
//    
//    @Column(name="FD_MOD_DATE")
//    public Date modDate;
//    
//    @Column(name="FC_SECTION_ID")
//    public String idSection;
//    
//    @Column(name="FN_USERID_CHIEF")
//    public Integer idChief;
//    
//    @Column(name="FN_USERID_MANAGER")
//    public Integer idManager;
//    
//    @Column(name="FN_USERID_DIRECTOR")
//    public Integer idDirector;
//    
//    @Column(name="FN_USERID_ZONE")
//    public String idUserZone;
//    
//    @Column(name="FC_TITLE")
//    public String title;
//    
//    @Column(name="FN_ID_FOLLOW")
//    public Integer idFollow;
//    
//    @Column(name="FN_ID_IMPUGN")
//    public Integer idImpugn;
//    
//    @Column(name="FN_USERID")
//    public Integer idUser;
//    
//    @Column(name="FN_ID_FILE")
//    public Integer idFile;
//    
//    @Column(name="FC_ZONE")
//    public String zone;
    
//    @Column(name="FN_SKU")
//    public Integer sku;
//    
//    @Column(name="FC_DESC_SKU")
//    public String skuDesc;
}
