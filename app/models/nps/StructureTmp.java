package models.nps;

import play.db.jpa.*;
import javax.persistence.*;
import java.util.*;

/**
 * Clase entidad para la tabla PUL_STRUCTURE_TMP
 * @author Rodolfo Miranda -- Qualtop
 */
@Entity
@Table(name = "PUL_STRUCTURE_TMP", schema = "PORTALUNICO")
public class StructureTmp extends GenericModel {

	@Id
    @Column(name = "FC_USERID", nullable = false)
    //@GeneratedValue(strategy = GenerationType.AUTO) 
    public String userId; 

	@Column(name = "FC_STATUS")
    public String status;

	@Column(name = "FC_COUNTRY")
    public String country; 

	@Column(name = "FC_CUSTOM01")
    public String custom01; 

	@Column(name = "FC_CUSTOM02")
    public String custom02; 

	@Column(name = "FC_CUSTOM03")
  	public String custom03; 

	@Column(name = "FC_CUSTOM04")
    public String custom04; 

	@Column(name = "FC_CUSTOM05")
    public String custom05;

	@Column(name = "FC_CUSTOM06")
    public String custom06;

	@Column(name = "FC_CUSTOM07")
    public String custom07;

	@Column(name = "FC_CUSTOM08")
    public String custom08;

	@Column(name = "FC_CUSTOM09")
    public String custom09;

	@Column(name = "FC_CUSTOM10")
    public String custom10;

	@Column(name = "FC_CUSTOM13")
    public String custom13;

	@Column(name = "FC_CUSTOM_MANAGER")
    public String customManager;

	@Column(name = "FC_DEFAULT_LOCALE")
    public String defaultLocale;

	@Column(name = "FC_DEPARTMENT")
    public String deparment;

	@Column(name = "FC_DIVISION")
    public String division;

	@Column(name = "FC_EMAIL")
    public String email;

	@Column(name = "FC_EMPID")
    public String empId;

	@Column(name = "FC_FIRSTNAME")
    public String firstName;

	@Column(name = "FC_GENDER")
    public String gender;

	@Column(name = "FC_HIREDATE")
    public String hireDate;

	@Column(name = "FC_HR")
    public String hr;

	@Column(name = "FC_JOBCODE")
    public String jobcode;

	@Column(name = "FC_LASTNAME")
  	public String lastname;

	@Column(name = "FC_LOCATION")
    public String location;

	@Column(name = "FC_MANAGER")
    public String manager;

	@Column(name = "FC_TIMEZONE")
    public String timezone;

	@Column(name = "FC_TITLE")
    public String title;

	@Column(name = "FC_USERNAME")
    public String username;

    @Column(name = "FC_MOD_FOR")
    public String modifiedBy;

    @Column(name = "FC_CRE_FOR") 
    public String createdBy;

    @Column(name = "FD_CRE_DATE") 
    public Date createdDate;

    @Column(name = "FD_MOD_DATE")
    public Date modifiedDate;

    @Override
    public String toString() {
        return "StructureTmp{" + "userId=" + userId + ", status=" + status + 
                ", country=" + country + ", custom01=" + custom01 + 
                ", custom02=" + custom02 + ", custom03=" + custom03 + 
                ", custom04=" + custom04 + ", custom05=" + custom05 + 
                ", custom06=" + custom06 + ", custom07=" + custom07 + 
                ", custom08=" + custom08 + ", custom09=" + custom09 + 
                ", custom10=" + custom10 + ", custom13=" + custom13 + 
                ", customManager=" + customManager + ", defaultLocale=" + 
                defaultLocale + ", deparment=" + deparment + ", division=" + 
                division + ", email=" + email + ", empId=" + empId + 
                ", firstName=" + firstName + ", gender=" + gender + 
                ", hireDate=" + hireDate + ", hr=" + hr + ", jobcode=" + 
                jobcode + ", lastname=" + lastname + ", location=" + location + 
                ", manager=" + manager + ", timezone=" + timezone + ", title=" +
                title + ", username=" + username + ", modifiedBy=" + 
                modifiedBy + ", createdBy=" + createdBy + ", createdDate=" + 
                createdDate + ", modifiedDate=" + modifiedDate + '}';
    }
}
    
   
