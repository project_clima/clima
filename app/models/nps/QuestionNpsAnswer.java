/**
 * @(#) QuestionNpsAnswer 8/05/2017
 */

package models.nps;

import java.util.Date;
import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import play.db.jpa.GenericModel;
import models.NpsAnswer;

/**
 * Clase entidad para la tabla PUL_NPS_QUESTION_ANSWER
 * @author Rodolfo Miranda -- Qualtop
 */
@Entity
@Table(name="PUL_NPS_QUESTION_ANSWER" , schema="PORTALUNICO")
public class QuestionNpsAnswer extends GenericModel{

    @EmbeddedId
    public NPS_KEY_QUESTION keyQ;
    
    @ManyToOne
    @JoinColumn(name="FN_ID_ANSWER", insertable = false, updatable = false)
    public NpsAnswer answer;
    
    @Column(name="FN_ID_CHOICE_ANSWER ")
    public Integer idChoice;
    
    @Column(name="FC_TEXT_ANSWER ")
    public String answerText;
    
    @Column(name="FC_CRE_FOR ")
    public String creFor;
    
    @Column(name="FD_CRE_DATE ")
    public Date creDate;
    
    @Column(name="FC_MOD_FOR ")
    public String modFor;
        
    @Column(name="FD_MOD_DATE ")
    public Date modDate;
               
}
