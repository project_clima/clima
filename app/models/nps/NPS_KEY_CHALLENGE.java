/**
 * @(#) NPS_KEY_CHALLENGE 20/04/2017
 */

package models.nps;

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Embeddable;

/**
 * Clase entidad para la tabla NPS_KEY_CHALLENGE
 * @author Rodolfo Miranda -- Qualtop
 */
@Embeddable
public class NPS_KEY_CHALLENGE implements Serializable{

    @Column(name = "FN_ID_ANSWER", nullable = false)
    public Long idAnswer;

    @Column(name = "FN_ID_IMPUGN", nullable = false)
    public Long idImpugn;

    public NPS_KEY_CHALLENGE() {

    }

    public NPS_KEY_CHALLENGE(Long idAnswer, Long idImpugn) {
        this.idAnswer = idAnswer;
        this.idImpugn = idImpugn;
    }

    public boolean equals(Object object) {
        if (object instanceof NPS_KEY_CHALLENGE) {
            NPS_KEY_CHALLENGE pk = (NPS_KEY_CHALLENGE)object;
            return idAnswer == pk.idAnswer && idImpugn == pk.idImpugn ;
        } else {
            return false;
        }
    }

    public int hashCode() {
        return Long.valueOf(idAnswer + idImpugn).hashCode();
    }
}
