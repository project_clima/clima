/**
 * @(#) FileNPS 17/03/2017
 */
package models.nps;

import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import play.db.jpa.GenericModel;

/**
 * Clase entidad para la tabla PUL_FILE
 * @author Rodolfo Miranda -- Qualtop
 */
@Entity
@Table(name = "PUL_FILE", schema = "PORTALUNICO")
public class FileNPS extends GenericModel {

    @Id
    @Column(name = "FN_ID_FILE", nullable = false)
    @SequenceGenerator(name="SQL_ID", sequenceName="PORTALUNICO.SQ_FILE")
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "SQL_ID")
    public int idFile;

    @Column(name = "FC_FILENAME")
    public String fileName;

    @Column(name = "FC_EXTENSION")
    public String extension;

    @Column(name = "FC_MODULE")
    public String module;

    @Column(name = "FD_LOAD_DATE")
    public Date loadDate;

    @Column(name = "FC_TYPE")
    public String type;

    @Column(name = "FC_STATUS")
    public String status;

    @Column(name = "FC_CRE_FOR", insertable=false, updatable=false)
    public String createFor;

    @Column(name = "FD_CRE_DATE", insertable=false, updatable=false)
    public Date createDate;

    @Column(name = "FC_MOD_FOR", insertable=false, updatable=false)
    public String modifiedFor;

    @Column(name = "FD_MOD_DATE")
    public Date modifiedDate;
}
