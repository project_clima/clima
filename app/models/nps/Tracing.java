/**
 * @(#) Tracing 19/04/2017
 */

package models.nps;

import models.nps.NPS_KEY_TRACING;
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.Table;
import play.db.jpa.GenericModel;

/**
 * Clase entidad para la tabla PUL_NPS_FOLLOWUP
 * @author Rodolfo Miranda -- Qualtop
 */
@Entity
@Table(name="PUL_NPS_FOLLOWUP", schema="PORTALUNICO")
public class Tracing extends GenericModel{
   
    @EmbeddedId
    public NPS_KEY_TRACING key;
    
    @Column(name="FN_ID_FOLLOWUP_STATUS")
    public int status;
    
    @Column(name="FC_TEXT")
    public String text;
    
    @Column(name="FN_USERNAME")
    public String userName;
    
    @Column(name="FD_DATE")
    public Date date;
    
    @Column(name="FC_TITLE")
    public String title;

}
