/**
 * @(#) NPS_KEY_TRACING 19/04/2017
 */
package models.nps;

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Embeddable;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.TableGenerator;

/**
 * Clase entidad para la tabla NPS_KEY_TRACING
 * @author Rodolfo Miranda -- Qualtop
 */
@Embeddable
public class NPS_KEY_TRACING implements Serializable {

    
    @Column(name = "FN_ID_ANSWER", nullable = false)
    public int idAnswer;

    @Column(name = "FN_ID_FOLLOWUP", nullable = false)
    public int idFollow;

    public NPS_KEY_TRACING() {

    }

    public NPS_KEY_TRACING(int idAnswer, int idFollow) {
        this.idAnswer = idAnswer;
        this.idFollow = idFollow;
    }

    public boolean equals(Object object) {
        if (object instanceof NPS_KEY_TRACING) {
            NPS_KEY_TRACING pk = (NPS_KEY_TRACING)object;
            return idAnswer == pk.idAnswer && idFollow == pk.idFollow ;
        } else {
            return false;
        }
    }

    public int hashCode() {
        return (int)(idAnswer + idFollow);
    }
}
