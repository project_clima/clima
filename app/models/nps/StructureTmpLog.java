/**
 * @(#) StructureTmpLog 18/05/2017
 */

package models.nps;

import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import play.db.jpa.GenericModel;
import play.db.jpa.Model;

/**
 * Clase entidad para la tabla PUL_STRUCTURE_TMP_LOG
 * @author Rodolfo Miranda -- Qualtop
 */
@Entity
@Table(name="PUL_STRUCTURE_TMP_LOG", schema = "PORTALUNICO")
public class StructureTmpLog extends GenericModel{
    
    @Id
    @Column(name = "FC_USERID", nullable = false)
    //@GeneratedValue(strategy = GenerationType.AUTO) 
    public String userId; 

	@Column(name = "FC_STATUS")
    public String status;

	@Column(name = "FC_COUNTRY")
    public String country; 

	@Column(name = "FC_CUSTOM01")
    public String custom01; 

	@Column(name = "FC_CUSTOM02")
    public String custom02; 

	@Column(name = "FC_CUSTOM03")
  	public String custom03; 

	@Column(name = "FC_CUSTOM04")
    public String custom04; 

	@Column(name = "FC_CUSTOM05")
    public String custom05;

	@Column(name = "FC_CUSTOM06")
    public String custom06;

	@Column(name = "FC_CUSTOM07")
    public String custom07;

	@Column(name = "FC_CUSTOM08")
    public String custom08;

	@Column(name = "FC_CUSTOM09")
    public String custom09;

	@Column(name = "FC_CUSTOM10")
    public String custom10;

	@Column(name = "FC_CUSTOM13")
    public String custom13;

	@Column(name = "FC_CUSTOM_MANAGER")
    public String customManager;

	@Column(name = "FC_DEFAULT_LOCALE")
    public String defaultLocale;

	@Column(name = "FC_DEPARTMENT")
    public String deparment;

	@Column(name = "FC_DIVISION")
    public String division;

	@Column(name = "FC_EMAIL")
    public String email;

	@Column(name = "FC_EMPID")
    public String empId;

	@Column(name = "FC_FIRSTNAME")
    public String firstName;

	@Column(name = "FC_GENDER")
    public String gender;

	@Column(name = "FC_HIREDATE")
    public String hireDate;

	@Column(name = "FC_HR")
    public String hr;

	@Column(name = "FC_JOBCODE")
    public String jobcode;

	@Column(name = "FC_LASTNAME")
  	public String lastname;

	@Column(name = "FC_LOCATION")
    public String location;

	@Column(name = "FC_MANAGER")
    public String manager;

	@Column(name = "FC_TIMEZONE")
    public String timezone;

	@Column(name = "FC_TITLE")
    public String title;

	@Column(name = "FC_USERNAME")
    public String username;

    @Column(name = "FC_MOD_FOR")
    public String modifiedBy;

    @Column(name = "FC_CRE_FOR") 
    public String createdBy;

    @Column(name = "FD_CRE_DATE") 
    public Date createdDate;

    @Column(name = "FD_MOD_DATE")
    public Date modifiedDate;
    
    @Column(name ="FC_OBSERVACIONES")
    public String obs;
}
