/**
 * @(#) NPS_KEY_NOT_SALE 30/06/2017
 */

package models.nps;

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Embeddable;

/**
 * Clase entidad para la tabla NPS_KEY_NOT_SALE
 * @author Rodolfo Miranda -- Qualtop
 */
@Embeddable
public class NPS_KEY_NOT_SALE implements Serializable{

    @Column(name = "FN_EMPLOYEE_NUMBER", nullable = false)
    public Long empNumber;

    @Column(name = "FN_ID_STORE", nullable = false)
    public Integer idStore;
    
    @Column(name = "FC_ID_AREA", nullable = false)
    public String idArea;

    public NPS_KEY_NOT_SALE() {

    }
    
     public NPS_KEY_NOT_SALE(Long empNumber, Integer idStore, String idArea) {
        this.empNumber = empNumber;
        this.idStore = idStore;
        this.idArea = idArea;
    }

    public boolean equals(Object object) {
        if (object instanceof NPS_KEY_NOT_SALE) {
            NPS_KEY_NOT_SALE pk = (NPS_KEY_NOT_SALE)object;
            return idArea.equals(pk.idArea) && empNumber.equals(pk.empNumber) && 
                    idStore.equals(pk.idStore);
        } else {
            return false;
        }
    }

    public int hashCode() {
        return (int)(idArea.length() + empNumber + idStore);
    }
}
