/**
 * @(#) GenericNps 6/06/2017
 */

package models.nps;

import java.util.List;
import utils.nps.model.Area;

/**
 * Clase dto para NPS
 * @author Rodolfo Miranda -- Qualtop
 */
public class GenericNps<T> {

    private String id;
    private String desc;
    private String nps;
    private List<Area> areas;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getDesc() {
        return desc;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }

    public String getNps() {
        return nps;
    }

    public void setNps(String nps) {
        this.nps = nps;
    }

    public List<Area> getAreas() {
        return areas;
    }

    public void setAreas(List<Area> areas) {
        this.areas = areas;
    }
            
            
}
