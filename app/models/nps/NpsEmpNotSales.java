/**
 * @(#) NpsEmpNotSales 30/06/2017
 */

package models.nps;

import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import play.db.jpa.GenericModel;

/**
 * Clase entidad para la tabla PUL_NPS_EMP_NOT_SALES
 * @author Rodolfo Miranda -- Qualtop
 */
@Entity
@Table(name="PUL_NPS_EMP_NOT_SALES" , schema = "PORTALUNICO")
public class NpsEmpNotSales extends GenericModel{

    @Id
    @Column(name = "FN_EMPLOYEE_NUMBER", nullable = false)
    public Long empNumber;

    @Column(name = "FN_ID_STORE", nullable = false)
    public Integer idStore;
    
    @Column(name = "FC_ID_AREA", nullable = false)
    public String idArea;
    
    @Column(name="FC_CRE_FOR")
    public String createFor;
    
    @Column(name="FD_CRE_DATE")
    public Date createDate;
    
    @Column(name="FC_MOD_FOR")
    public String modifiedFor;
    
    @Column(name="FD_MOD_DATE")
    public Date modifiedDate;
    
    @Column(name="FC_NAME")
    public String name;
    
    @Column(name="FN_USERID_CHIEF")
    public Long userIdChief;
    
    @Column(name="FN_USERID_MANAGER")
    public Long userIdManager;
    
    @Column(name="FN_USERID_DIRECTOR")
    public Long userIdDirector;
    
    @Column(name="FN_USERID_ZONE")
    public Long userIdZonal;
    
    @Column(name="FC_ZONE")
    public String zone;
    
    @Column(name="FN_ID_SECTION")
    public Integer idSection;
    
    @Column(name="FC_TITLE")
    public String title;
    
    @Column(name="FN_USERID")
    public Long userId;
}
