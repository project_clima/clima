/**
 * @(#) NpsBusiness 21/05/2017
 */

package models.nps;

import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import play.db.jpa.GenericModel;

/**
 * Clase entidad para la tabla PUL_STORE_BUSINESS
 * @author Rodolfo Miranda -- Qualtop
 */
@Entity
@Table(name="PUL_STORE_BUSINESS")
public class NpsBusiness extends GenericModel{

    @Id
    @Column(name="FN_STORE_BUSINESS_ID")
    public int busId;
    
    @Column(name="FC_STORE_BUSINESS")
    public String desc;
    
    @Column(name="FC_CRE_FOR")
    public String creFor;
    
    @Column(name="FD_CRE_DATE")
    public Date creDate;
    
    @Column(name="FC_MOD_FOR")
    public String modFor;
    
    @Column(name="FD_MOD_DATE")
    public Date modDate;
}
