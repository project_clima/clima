/**
 * @(#) FunctionByTitle 30/06/2017
 */

package models.nps;

import java.util.Date;
import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.Table;
import play.db.jpa.GenericModel;

/**
 * Clase entidad para la tabla PUL_NPS_FUNCTION_BY_TITLE
 * @author Rodolfo Miranda -- Qualtop
 */
@Entity
@Table(name="PUL_NPS_FUNCTION_BY_TITLE", schema = "PORTALUNICO")
public class FunctionByTitle extends GenericModel{

    @EmbeddedId
    public NPS_KEY_FUNCTION key;
    
    @Column(name="FD_CRE_DATE")
    public Date createDate;
    
    @Column(name="FD_MOD_DATE")
    public Date modifiedDate;
    
    @Column(name="FC_CRE_FOR")
    public String createFor;
    
    @Column(name="FC_MOD_FOR")
    public String modifiedFor;
}
