/**
 * @(#) ChallengeUtil 27/06/2017
 */

package models.nps;

/**
 * Clase de utilidad para las Impugnaciones
 * @author Rodolfo Miranda -- Qualtop
 */
public class ChallengeUtil {

    private Long empId;
    private Long idStore;
    private String zone;
    private Long userId;
    private Long chiefId;
    private Long managerId;
    private Long directorId;
    private Long userZone;
    private Integer section;
    private String error;

    public Long getEmpId() {
        return empId;
    }

    public void setEmpId(Long empId) {
        this.empId = empId;
    }

    public Long getIdStore() {
        return idStore;
    }

    public void setIdStore(Long idStore) {
        this.idStore = idStore;
    }

    public String getZone() {
        return zone;
    }

    public void setZone(String zone) {
        this.zone = zone;
    }

    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }

    public Long getChiefId() {
        return chiefId;
    }

    public void setChiefId(Long chiefId) {
        this.chiefId = chiefId;
    }

    public Long getManagerId() {
        return managerId;
    }

    public void setManagerId(Long managerId) {
        this.managerId = managerId;
    }

    public Long getDirectorId() {
        return directorId;
    }

    public void setDirectorId(Long directorId) {
        this.directorId = directorId;
    }

    public Long getUserZone() {
        return userZone;
    }

    public void setUserZone(Long userZone) {
        this.userZone = userZone;
    }

    public Integer getSection() {
        return section;
    }

    public void setSection(Integer section) {
        this.section = section;
    }

    public String getError() {
        return error;
    }

    public void setError(String error) {
        this.error = error;
    }
    
    
}
