/**
 * @(#) FilesLoad 22/08/2017
 */

package models.nps;

import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import play.db.jpa.GenericModel;

/**
 * Clase entidad para la tabla PUL_FILE_LOAD
 * @author Rodolfo Miranda -- Qualtop
 */
@Entity
@Table(name="PUL_FILE_LOAD", schema="PORTALUNICO")
public class FilesLoadNps extends GenericModel{

    @Id
    @Column(name = "FN_ID_FILE_LOAD", nullable = false)
    public Long idFileLoad;
    
    @Column(name = "FN_ID_FILE")
    public Integer idFile;
    
    @Column(name = "FD_START_DATE")
    public Date startDate;
    
    @Column(name = "FD_END_DATE")
    public Date endDate;
    
    @Column(name = "FN_PERCENT")
    public Float percent;
    
    @Column(name = "FC_COMMENTS")
    public String comments;
    
    @Column(name = "FC_CRE_FOR")
    public String creFor;
    
    @Column(name = "FD_CRE_DATE")
    public Date creDate;
    
    @Column(name = "FC_MOD_FOR")
    public String modFor;
    
    @Column(name = "FD_MOD_DATE")
    public Date modDate;
}
