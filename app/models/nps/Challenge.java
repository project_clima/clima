/**
 * @(#) Challange 20/04/2017
 */
package models.nps;

import java.util.Date;
import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import models.NpsAnswer;
import play.db.jpa.GenericModel;

/**
 * Clase entidad para la tabla PUL_NPS_IMPUGN
 * @author Rodolfo Miranda -- Qualtop
 */
@Entity
@Table(name = "PUL_NPS_IMPUGN", schema = "PORTALUNICO")
public class Challenge extends GenericModel {

    @EmbeddedId
    public NPS_KEY_CHALLENGE key;

    @ManyToOne
    @JoinColumn(name = "FN_ID_ANSWER", insertable = false, updatable = false)
    public NpsAnswer answer;

    @Column(name = "FN_ID_FOLLOWUP")
    public Integer idStatus;

    @Column(name = "FC_TYPE")
    public String type;

    @Column(name = "FC_FIELD1")
    public String field1;

    @Column(name = "FC_FIELD2")
    public String field2;

    @Column(name = "FC_TEXT")
    public String text;

    @Column(name = "FN_USERNAME")
    public String username;

    @Column(name = "FD_DATE")
    public Date date;

    @Column(name = "FC_TITLE")
    public String title;
    
    @Column(name = "FC_FIELD3")
    public String field3;
}
