package models;

import play.data.validation.*;
import play.db.jpa.*;
import javax.persistence.*;
import java.util.*;

@Entity
@Table(name = "PUL_ROLE", schema = "PORTALUNICO")
public class Rol extends GenericModel {
    @Id
    @Column(name = "FN_ID_ROLE", nullable = true)
    @GeneratedValue(strategy = GenerationType.AUTO)
    public Long id;

    @Required
    @MaxSize(100)
    @Column(name = "FC_NAME_ROLE", length = 100)
    public String nombreRol;

    @ManyToMany
    @JoinTable(
        name = "PUL_ROLE_ACTION",
        joinColumns = {@JoinColumn(name = "FN_ID_ROLE")},
        inverseJoinColumns = {@JoinColumn(name = "FN_ACTION_ID")}
    )
    public List<Accion> acciones = new ArrayList<Accion>();

    @Column(name = "FC_MOD_FOR")
    public String modifiedBy;

    @Column(name = "FC_CRE_FOR")
    public String createdBy;

    @Column(name = "FD_CRE_DATE")
    public Date createdDate;

    @Column(name = "FD_MOD_DATE")
    public Date modifiedDate;
    
    @Column(name = "FN_ID_MODULE")
    public Long idModule;
}
