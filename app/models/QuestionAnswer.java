package models;

import play.data.validation.*;
import play.db.jpa.*;
import javax.persistence.*;
import java.util.*;

@Entity
@Table(name = "PUL_QUESTION_ANSWER", schema = "PORTALUNICO")
public class QuestionAnswer extends GenericModel {
    @Id
    @Column(name = "FN_ID_QUESTION_ANSWER", nullable = true)
    @GeneratedValue(strategy = GenerationType.AUTO)    
    public Integer id;
    
    @Required
    @MaxSize(255)
    @Column(name = "FN_ID_EMPLOYEE_CLIMA", length = 255)
    public Integer employee;
    
    @Column(name = "FC_TEXT_ANSWER", length = 1024)
    public String answerText;
    
    @Column(name = "FN_SINGLE_SCORE")
    public Float singleScore;
    
    @Column(name = "FN_WEIGH", precision = 10, scale=4)
    public Float weighing;
    
    @Column(name = "FN_WEIGH_PERCENT", precision = 10, scale=4)
    public Float weighingPercent;
    
    @Column(name = "FD_CRE_DATE")
    public Date creationDate;
    
    @Column(name = "FD_MOD_DATE")
    public Date modificationDate;
    
    @Column(name = "FC_CRE_FOR")
    public String createdBy;
    
    @Column(name = "FC_MOD_FOR")
    public String modifiedBy;
    
    @Column(name = "FN_SURVEY_ID")
    public Integer surveyId;
    
    @Column(name = "FN_ID_BY_SURVEY")
    public Long idBySurvey;
    
    @ManyToOne
    @JoinColumn(name = "FN_ID_QUESTION")
    public Question question;
    
    @ManyToOne
    @JoinColumn(name = "FN_ID_CHOICE_ANSWER")
    public QuestionChoices choice;
}