package models;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import play.data.validation.MaxSize;
import play.db.jpa.GenericModel;

@Entity
@Table(name = "PUL_CSI_EVALUATION_RESULTS", schema = "PORTALUNICO")
public class CSIEvaluationResults extends GenericModel{
	
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "FN_ID_EVA_RESULTS", nullable = true)    
	public Integer idEvaResults;
	
	
	@Column(name = "FN_ID_EVALUATED_EMP")    
	public Integer idEmployee;
	
	@Column(name = "FN_ID_EVALUATOR_EMP")    
	public Integer idEvaluator;

	@MaxSize(19)
	@Column(name = "FN_RESULT")   
	public Integer result;
	
	@Column(name = "FC_RESULT")  
	public String resultText;
	
	@Column(name = "FN_ID_SURVEY")    
	public Integer idSurvey;
	
	@Column(name = "FD_CRE_DATE") 
    public Date createdDate;
	
	@Column(name = "FD_MOD_DATE") 
    public Date modifiedDate;
	
	@MaxSize(30)
	@Column(name = "FC_CRE_BY") 
    public String createdBy;
	
	@MaxSize(30)
	@Column(name = "FC_MOD_BY") 
    public String modifiedBy;
	
	@MaxSize(3000)
	@Column(name = "FC_COMMENTS") 
    public String comments;
	
	@Column(name = "FN_ID_QUESTION")    
	public Integer idQuestion;
}
