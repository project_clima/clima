package models;

import play.data.validation.MaxSize;
import play.data.validation.Required;
import play.db.jpa.*;
import javax.persistence.*;
import java.util.*;

@Entity
@Table(name = "PUL_SPECIAL_PERMISSIONS", schema = "PORTALUNICO")
public class PermisosEspeciales extends GenericModel {
    @Id
    @Column(name = "FN_ID_SPECIAL_PERMISSIONS", nullable = true)
    @GeneratedValue(strategy = GenerationType.AUTO)    
    public Long id;
    
    @Required
    @MaxSize(100)
    @Column(name = "FC_TYPE_PERMIT", length = 100)
    public String typePermit;

    @Required
    @MaxSize(100)
    @Column(name = "FC_VALUE", length = 100)
    public String permitValue;

    @Required
    @ManyToOne
    @JoinColumn(name = "FN_ID_ROLE")
    public Rol role;

    @Column(name = "FC_MOD_FOR")
    public String modifiedBy;

    @Column(name = "FC_CRE_FOR") 
    public String createdBy;

    @Column(name = "FD_CRE_DATE") 
    public Date createdDate;

    @Column(name = "FD_MOD_DATE")
    public Date modifiedDate;
}
