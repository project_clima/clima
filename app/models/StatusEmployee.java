package models;

import play.data.validation.*;
import play.db.jpa.*;
import javax.persistence.*;
import java.util.*;

@Entity
@Table(name = "PUL_STATUS_EMPLOYEE", schema = "PORTALUNICO")
public class StatusEmployee extends GenericModel {


        @Id
        @Column(name = "FN_ID_STATUS_EMP", nullable = true)
        @GeneratedValue(strategy = GenerationType.AUTO)    
        public Integer idStatus;

        @Required
        @MaxSize(100)
        @Column(name = "FC_DESC_STATUS_EMP", length = 100)
        public String descriptionSTATUS;

        @Column(name = "FC_MOD_FOR")
        public String modifiedBy;

        @Column(name = "FC_CRE_FOR") 
        public String createdBy;

        @Column(name = "FD_CRE_DATE") 
        public Date createdDate;

        @Column(name = "FD_MOD_DATE")
        public Date modifiedDate;

}