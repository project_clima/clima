package models;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import play.db.jpa.GenericModel;

@Entity
@Table(name = "PUL_NPS_QUESTION_ANSWER", schema = "PORTALUNICO")
public class NpsQuestionAnswer extends GenericModel {

    @Id
    @Column(name = "FN_ID_ANSWER")
    public Long idAnswer;

    @Id
    @Column(name = "FN_ID_QUESTION")
    public Integer idQuestion;

    @Id
    @Column(name = "FN_ID_CHOICE_ANSWER")
    public Integer idChoiceAnswer;

    @Column(name = "FC_TEXT_ANSWER")
    public String textAnswer;

    @Column(name = "FC_CRE_FOR")
    public String createdBy;

    @Column(name = "FC_MOD_FOR")
    public String modifiedBy;

    @Column(name = "FD_CRE_DATE")
    public Date createdDate;

    @Column(name = "FD_MOD_DATE")
    public Date modifiedDate;

}
