package models;

import play.data.validation.MaxSize;
import play.data.validation.Required;
import play.db.jpa.*;
import javax.persistence.*;
import java.util.*;

@Entity
@Table(name = "SELOG")
public class SecurityLog extends Model{
    @Required
    @MaxSize(100)
    @Column(name = "LOGACT", length = 100)
    public String action;
    
    @Required
    @MaxSize(100)
    @Column(name = "LOGCAT", length = 100)
    public String catalog;
    
    @Required
    @MaxSize(100)
    @Column(name = "LOGDES", length = 100)
    public String description;
    
    @Required
    @MaxSize(10)
    @Column(name = "LOGUSR", length = 50)
    public String analystID;
    
    @Required
    @MaxSize(10)
    @Column(name = "LOGUPD")
    public Date eventDate;
    
    public static void saveLog(String catalog, String message, String analystID) {
        SecurityLog oLog = new SecurityLog();
        oLog.catalog     = catalog;
        oLog.description = message;
        oLog.analystID   = analystID;
        oLog.eventDate   = new Date();
        
        oLog.save();
    }
}