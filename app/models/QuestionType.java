package models;

import play.data.validation.*;
import play.db.jpa.*;
import javax.persistence.*;
import java.util.*;

@Entity
@Table(name = "PUL_TYPE_QUESTION", schema = "PORTALUNICO")
public class QuestionType extends GenericModel {
    @Id
    @Column(name = "FN_ID_TYPE_QUESTION", nullable = true)
    @GeneratedValue(strategy = GenerationType.AUTO)    
    public Integer id;
    
    @Required
    @MaxSize(255)
    @Column(name = "FC_TYPE_QUESTION_NAME", length = 255)
    public String name;

    @Required
    @Column(name = "FN_HAS_CHOICES")
    public Boolean hasChoices;
    
    @Required
    @Column(name = "FN_WITH_NA")
    public Integer withNA;
    
    @Required
    @Column(name = "FN_ORDER")
    public Integer order;
    
    @Column(name = "FD_CRE_DATE")
    public Date creationDate;
    
    @Column(name = "FD_MOD_DATE")
    public Date modificationDate;
    
    @Column(name = "FC_CRE_FOR")
    public String createdBy;
    
    @Column(name = "FC_MOD_FOR")
    public String modifiedBy;
}