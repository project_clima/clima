package models;

import play.data.validation.*;
import play.db.jpa.*;
import javax.persistence.*;
import java.util.*;

@Entity
@Table(name = "PUL_EMPLOYEE_CSI", schema = "PORTALUNICO")
public class CSIEmployee extends GenericModel {
    @Id
    @Column(name = "FN_ID_EMPLOYEE_CSI", nullable = true)
    @GeneratedValue(strategy = GenerationType.AUTO)    
    public Integer id;
    
    @MaxSize(100)
    @Column(name = "FC_MANAGEMENT", length = 100)
    public String management;
    
    @MaxSize(100)
    @Column(name = "FC_FIRST_NAME", length = 100)
    public String firstName;
    
    @MaxSize(100)
    @Column(name = "FC_LAST_NAME", length = 100)
    public String lastName;
    
    @Column(name = "FD_CONTRACT_DATE")
    public Date contractDate;
    
    @MaxSize(255)
    @Column(name = "FC_EMAIL", length = 255)
    public String email;
    
    @Column(name = "FN_EMPLOYEE_NUMBER")
    public Integer employeeNumber;
    
    @Column(name = "FN_HUMAN_RESOURCES")
    public Integer humanResources;
    
    @Column(name = "FN_LEVEL")
    public Integer level;
    
    @Column(name = "FN_MANAGER")
    public Integer manager;
    
    @MaxSize(100)
    @Column(name = "FC_FIRST_TIME", length = 100)
    public String firstTime;
    
    @Column(name = "FN_USERID")
    public Integer userId;
    
    @Column(name = "FD_STRUCTURE_DATE", length = 50)
    public Date structureDate;
    
    @MaxSize(255)
    @Column(name = "FC_DEPARTMENT", length = 255)
    public String department;
    
    @MaxSize(255)
    @Column(name = "FC_FUNCTION", length = 255)
    public String function;

    @MaxSize(1)
    @Column(name = "FN_KEY_PLANT", length = 1)
    public String keyPlant;
    
    @Column(name = "FN_STATUS_EMPLOYEE", length = 50)
    public Integer statusEmployee;
    
    @Column(name = "FN_ID_STORE")
    public Integer idStore;
    
    @Column(name = "FD_CRE_DATE")
    public Date creDate;
    
    @Column(name = "FD_MOD_DATE")
    public Date modDate;
    
    @MaxSize(255)
    @Column(name = "FC_DIVISION", length = 255)
    public String division;
    
    @MaxSize(255)
    @Column(name = "FC_DESC_LOCATION", length = 255)
    public String descLocation;
    
    @MaxSize(10)
    @Column(name = "FC_AREA", length = 10)
    public String area;
    
    @Column(name = "FN_SCORE")
    public Integer score;
    
    @Column(name = "FN_CALIFICACION")
    public Float calificacion;
    
    @MaxSize(255)
    @Column(name = "FC_TITLE", length = 255)
    public String title;
    
    @MaxSize(255)
    @Column(name = "FC_CRE_FOR", length = 255)
    public String creFor;
    
    @MaxSize(30)
    @Column(name = "FC_MOD_FOR", length = 30)
    public String modFor;
    
    @MaxSize(30)
    @Column(name = "FC_ID_LOCATION", length = 30)
    public String idLocation;
    
    @Column(name = "FN_ID_FUNCTION")
    public Integer idFunction;
    
    @MaxSize(255)
    @Column(name = "FC_AREA_C", length = 255)
    public String areaC;
}