package models;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import play.data.validation.MaxSize;
import play.data.validation.Required;
import play.db.jpa.GenericModel;

@Entity
@Table(name = "PUL_EMPLOYEE_SECTIONS", schema = "PORTALUNICO")
public class EmployeeSection extends GenericModel{
	
	@Id
	@Required
    @Column(name = "FN_USERID")   
    public Integer userId;
	
	@Id
	@Required
    @Column(name = "FN_ID_SECTION")
    public Integer idSection;
	
	@Id
	@Required
    @Column(name = "FD_STRUCTURE_DATE")
    public Date structureDate;

}
