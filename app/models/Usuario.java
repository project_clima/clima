package models;

import play.data.validation.*;
import play.db.jpa.*;
import javax.persistence.*;

import org.hibernate.annotations.Cascade;

import java.util.*;

@Entity
@Table(name = "PUL_USER", schema = "PORTALUNICO")
public class Usuario extends GenericModel {

    @Id
    @Column(name = "FN_ID_USER", nullable = true)
    @GeneratedValue(strategy = GenerationType.AUTO)    
    public Long id;

    @Required
    @MaxSize(50)
    @Column(name = "FC_USERNAME", unique = true, length = 50)
    @Unique(message="El nombre de usuario ya se encuentra registrado")
    public String username;

    @MaxSize(65)
    @Column(name = "FC_PASSWORD",length = 65)
    public String password;

    @MaxSize(100)
    @Column(name = "FC_FIRST_NAME",length = 100)
    public String nombre;

    @MaxSize(100)
    @Column(name = "FC_LAST_NAME", length = 100)
    public String apellidos;

    @MaxSize(100)
    @Column(name = "FC_DEPARTMENT", length = 100)
    public String departamento;

    @ManyToMany()    
    @JoinTable(name="PUL_USER_ROLE",               
               joinColumns={@JoinColumn(name="FN_ID_USER")},
               inverseJoinColumns={@JoinColumn(name="FN_ID_ROLE")})
    public List<Rol> roles =  new ArrayList<Rol>();

    @Column(name = "FC_MOD_FOR")
    public String modifiedBy;

    @Column(name = "FC_CRE_FOR") 
    public String createdBy;

    @Column(name = "FD_CRE_DATE") 
    public Date createdDate;

    @Column(name = "FD_MOD_DATE")
    public Date modifiedDate;
    
    @Column(name = "FC_EMAIL")
    public String email;

}