package models;

import play.data.validation.*;
import play.db.jpa.*;
import javax.persistence.*;
import java.util.*;

@Entity
@Table(name = "PUL_SURVEY", schema = "PORTALUNICO")
public class Survey extends GenericModel {
    @Id
    @Column(name = "FN_ID_SURVEY", nullable = true)
    @GeneratedValue(strategy = GenerationType.AUTO)
    public Integer id;

    @Required
    @MaxSize(255)
    @Column(name = "FC_NAME", length = 255)
    public String name;
    
    @Column(name = "FC_WELCOME")
    public String initialSection;
    
    @Column(name = "FC_CLOSING")
    public String endSection;

    @Column(name = "FD_SURVEY_DATE")
    public Date surveyDate;

    @Required
    @Column(name = "FD_DATE_INITIAL")
    public Date dateInitial;

    @Required
    @Column(name = "FD_DATE_END")
    public Date dateEnd;

    @Column(name = "FD_SENDMAIL_DATE")
    public Date sendDate;
    
    @Column(name = "FD_CRE_DATE")
    public Date creationDate;

    @Column(name = "FD_MOD_DATE")
    public Date modificationDate;

    @Column(name = "FC_CRE_FOR")
    public String createdBy;

    @ManyToOne
    @JoinColumn(name = "FC_MOD_FOR", referencedColumnName = "FC_USERNAME")
    public Usuario modifiedBy;

    @Required
    @ManyToOne
    @JoinColumn(name = "FN_ID_SURVEY_TYPE")
    public SurveyType surveyType;

    @OneToMany(orphanRemoval = true)
    @JoinColumn(name = "FN_ID_SURVEY")
    public List<Question> questions;
}
