package models;

import play.data.validation.*;
import play.db.jpa.*;
import javax.persistence.*;
import java.util.*;

@Entity
@Table(name = "PUL_CHOICE_ANSWER", schema = "PORTALUNICO")
public class QuestionChoices extends GenericModel {
    @Id
    @Column(name = "FN_ID_CHOICE_ANSWER", nullable = true)
    @GeneratedValue(strategy = GenerationType.AUTO)    
    public Integer id;
    
    @Required
    @MaxSize(255)
    @Column(name = "FC_CHOICE_TEXT", length = 255)
    public String choiceText;
    
    @Column(name = "FC_CHOICE_VALUE", length = 255)
    public String choiceValue;
    
    @Column(name = "FN_CHOICE_VALUE")
    public Float choiceNumericValue;
    
    @Column(name = "FD_CRE_DATE")
    public Date creationDate;
    
    @Column(name = "FD_MOD_DATE")
    public Date modificationDate;
    
    @Column(name = "FC_CRE_FOR")
    public String createdBy;
    
    @Column(name = "FC_MOD_FOR")
    public String modifiedBy;  
    
    @ManyToOne
    @JoinColumn(name = "FN_ID_QUESTION")
    public Question question;
}