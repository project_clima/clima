package models;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import play.data.validation.MaxSize;
import play.db.jpa.GenericModel;

@Entity
@Table(name = "PUL_EMPLOYEE_CSI", schema = "PORTALUNICO")
public class EmployeeCSI extends GenericModel{
	
	@Id	
	@Column(name = "FN_ID_EMPLOYEE_CSI")
	@SequenceGenerator(name="ID_EMPLOYEE_SECCIONS_ID_GENERATOR", sequenceName="PORTALUNICO.SQ_CSI_EMPLOYEE")
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="ID_EMPLOYEE_SECCIONS_ID_GENERATOR")    
    public Integer id;
	
	@Column(name = "FC_MANAGEMENT")
	public String managament;
	 
    @Column(name = "FC_LAST_NAME")
    public String lastName;
    
    @Column(name = "FD_CONTRACT_DATE")
    public Date contractDate;   
    
    @Column(name = "FC_EMAIL")
    public String email;

    @Column(name = "FN_EMPLOYEE_NUMBER")
    public Integer employeeNumber; 
    
    @Column(name = "FN_HUMAN_RESOURCES")
    public Integer humanResources;
    
    @Column(name = "FN_LEVEL")
    public Integer vLevel;
    
    @Column(name = "FN_MANAGER")
    public Integer manager;

    @Column(name = "FC_FIRST_NAME")
    public String firstName;
    
    @Column(name = "FN_USERID")
    public Integer userId;
    
    @Column(name = "FD_STRUCTURE_DATE")
    public Date structureDate;
    
    @Column(name = "FC_DEPARTMENT")
    public String department;
    
    @Column(name = "FC_FUNCTION")
    public String function;
    
    @Column(name = "FN_KEY_PLANT")
    public String keyPlan;
    
    @ManyToOne
    @JoinColumn(name = "FN_STATUS_EMPLOYEE")
    public StatusEmployee statusEmployee;
    
    @Column(name = "FN_ID_STORE")
    public Integer idStore;

    @Column(name = "FD_CRE_DATE")
    public Date createdDate;
    
    @Column(name = "FD_MOD_DATE")
    public Date modifiedDate;
    
    @Column(name = "FC_DIVISION")
    public String division;
    
    @Column(name = "FC_DESC_LOCATION")
    public String descLocation;
        
    @Column(name = "FC_AREA")
    public String area;

    @Column(name = "FN_SCORE")
    public Double score;

    @Column(name = "FN_CALIFICACION")
    public Float calificacion;
    
    @Column(name = "FC_TITLE")
    public String title;
    
    @Column(name = "FC_CRE_FOR")
    public String createdBy;
    
    @Column(name = "FC_MOD_FOR")
    public String modifiedBy;
    
    @Column(name = "FN_ID_LOCATION")
    public String idLocation;
    
    @Column(name = "FN_ID_FUNCTION")
    public Integer idFunction;
    
    @Column(name = "FC_AREA_C")
    public String areaC;
    
}

