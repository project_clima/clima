package models;

import play.data.validation.*;
import play.db.jpa.*;
import javax.persistence.*;
import java.util.*;

@Entity
@Table(name = "PUL_QUESTION", schema = "PORTALUNICO")
public class Question extends GenericModel {
    @Id
    @Column(name = "FN_ID_QUESTION", nullable = true)
    @GeneratedValue(strategy = GenerationType.AUTO)    
    public Integer id;

    @Required
    @MaxSize(255)
    @Column(name = "FC_QUESTION", length = 255)
    public String question;
    
    @Column(name = "FN_WEIGH", precision = 10, scale=4)
    public Float weighing;
    
    @Column(name = "FN_WEIGH_PERCENT", precision = 10, scale=4)
    public Float weighingPercent;

    @Column(name = "FD_CRE_DATE")
    public Date creationDate;
    
    @Column(name = "FD_MOD_DATE")
    public Date modificationDate;
    
    @Column(name = "FC_CRE_FOR")
    public String createdBy;
    
    @Column(name = "FN_SORT_ORDER")
    public Integer order;
    
    @Column(name = "FN_REQUIRED")
    public Boolean required;
    
    @Column(name = "FC_OTHER_QUESTION")
    public String alternativeQuestion;
    
    @ManyToOne
    @JoinColumn(name = "FC_MOD_FOR", referencedColumnName = "FC_USERNAME")
    public Usuario modifiedBy;
    
    @Required
    @ManyToOne
    @JoinColumn(name = "FN_ID_SURVEY")
    public Survey survey;
    
    @ManyToOne
    @JoinColumn(name = "FN_ID_SUBFACTOR")
    public SubFactor subFactor;
    
    @Required
    @ManyToOne
    @JoinColumn(name = "FN_ID_TYPE_QUESTION")
    public QuestionType questionType;
    
    @OneToMany
    @OrderBy("FC_CHOICE_VALUE desc")
    @JoinColumn(name = "FN_ID_QUESTION")
    public List<QuestionChoices> choices;
}