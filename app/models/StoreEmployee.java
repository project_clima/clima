package models;

import play.data.validation.*;
import play.db.jpa.*;
import javax.persistence.*;
import java.util.*;

@Entity
@Table(name = "PUL_STORE", schema = "PORTALUNICO")
public class StoreEmployee extends GenericModel {
    @Id
    @Column(name = "FN_SEQ_STORE", nullable = true)
    @SequenceGenerator(name = "FN_SEQ_STORE_GENERATOR", sequenceName = "PORTALUNICO.SQ_STORE", allocationSize = 1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "FN_SEQ_STORE_GENERATOR")
    public Integer id;
    
    @Column(name = "FN_ID_STORE", nullable = true)
    public Integer idStore;

    @Required
    @MaxSize(100)
    @Column(name = "FC_DESC_STORE", length = 100)
    public String descriptionStore;

    @Column(name = "FC_SUB_DIV", length = 100)
    public String subDivision;

    @Column(name = "FC_COUNTRY", length = 100)
    public String country;

    @Column(name = "FC_DESC_SUBDIV", length = 100)
    public String descriptionSubdivision;
    
    @Column(name = "FN_HUMAN_RESOURCES", nullable = true)
    public Integer humanResources;
    
    @Column(name = "FN_OTHER", nullable = true)
    public Integer other;
    
    @Column(name = "FN_STORE_HEAD", nullable = true)
    public Integer storeHead;

    @Column(name = "FC_MOD_FOR")
    public String modifiedBy;

    @Column(name = "FC_CRE_FOR") 
    public String createdBy;

    @Column(name = "FD_CRE_DATE") 
    public Date createdDate;

    @Column(name = "FD_MOD_DATE")
    public Date modifiedDate;
    
    @Column(name="FC_ZONE_NPS")
    public String zoneNps;
    
    @Column(name="FN_STORE_GROUP_ID")
    public Integer groupId;
    
    @Column(name="FN_STORE_BUSINESS_ID")
    public Integer busId;
    
    @Column(name = "FD_STRUCTURE_DATE")
    public Date structureDate;
}