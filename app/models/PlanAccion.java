package models;

import play.data.validation.*;
import play.db.jpa.*;
import javax.persistence.*;
import java.util.*;

@Entity
@Table(name = "PUL_PLAN_ACCION", schema = "PORTALUNICO")
public class PlanAccion extends GenericModel {
    @Id
    @Column(name = "FN_ID_PLAN_ACCION", nullable = true)
    @GeneratedValue(strategy = GenerationType.AUTO)    
    public Integer id;

    @MaxSize(255)
    @Column(name = "FC_COMMENTS", length = 1000)
    public String comments;

    @MaxSize(255)
    @Column(name = "FC_CATEGORY_PLAN", length = 255)
    public String categoryPlan;

    @MaxSize(255)
    @Column(name = "FC_HISTORIC", length = 4000)
    public String historic;

    @MaxSize(255)
    @Column(name = "FC_RESPONSIBLES", length = 500)
    public String responsibles;

    @Column(name = "FD_COMMIT_DATE")
    public Date commitDate;

    @MaxSize(50)
    @Column(name = "FC_STATUS_PLAN", length = 50)
    public String statusPlan;
    
    @MaxSize(50)
    @Column(name = "FC_STATUS_REVIEW", length = 50)
    public String statusReview;

    @MaxSize(50)
    @Column(name = "FC_STATUS_TRACING", length = 50)
    public String statusTracing;

    @MaxSize(255)
    @Column(name = "FC_APPROVAL_HISTORY", length = 4000)
    public String approvalHistory;

    @Column(name = "FN_ID_QUESTION")
    public Integer idQuestion;

    @Column(name = "FN_ID_EMPLOYEE_CLIMA")
    public Integer idEmployeeClima;

    @Column(name = "FN_ID_CHOICE_ANSWER")
    public Integer idChoiceAnswer;
    
    @MaxSize(255)
    @Column(name = "FC_QUESTION", length = 255)
    public String question;

    @Column(name = "FD_CRE_DATE")
    public Date creDate;

    @Column(name = "FD_MOD_DATE")
    public Date modDate;
    
    @Column(name = "FC_CRE_FOR")
    public String creFor;

    @Column(name = "FC_MOD_FOR")
    public String modFor;
}