package models;

import java.util.Date;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import play.data.validation.MaxSize;
import play.data.validation.Required;
import play.db.jpa.GenericModel;

@Entity
@Table(name = "PUL_SECTIONS", schema = "PORTALUNICO")
public class Sections extends GenericModel{
	
	@Id
	@Required
    @Column(name = "FN_ID_SECTION")   
    public Integer id;

	@Required
    @MaxSize(100)
    @Column(name = "FC_DESC_SECTION", length = 100)
    public String nombreSeccion;    

    @Column(name = "FC_MOD_FOR")
    public String modifiedBy;

    @Column(name = "FC_CRE_FOR") 
    public String createdBy;

    @Column(name = "FD_CRE_DATE") 
    public Date createdDate;

    @Column(name = "FD_MOD_DATE")
    public Date modifiedDate;
    
    @Column(name = " FN_ID_GOVERNMENT")
    public Integer idGoverment;
    
   
	
}
