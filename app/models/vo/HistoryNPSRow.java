package models.vo;

import java.util.List;
import models.vo.HistoryNPSBlock;

public class HistoryNPSRow {
    private String groupName;
    private double acum;
    private double acumPrevYear;
    private List<HistoryNPSBlock> data;
    private String levelId;

    public String getGroupName() {
        return this.groupName;
    }

    public double getAcum() {
        return this.acum;
    }

    public double getAcumPrevYear() {
        return this.acumPrevYear;
    }

    public List<HistoryNPSBlock> getData() {
        return this.data;
    }

    public String getLevelId() {
        return this.levelId;
    }

    public void setGroupName(String groupName) {
        this.groupName = groupName;
    }

    public void setAcum(double acum) {
        this.acum = acum;
    }

    public void setAcumPrevYear(double acumPrevYear) {
        this.acumPrevYear = acumPrevYear;
    }

    public void setData(List<HistoryNPSBlock> data) {
        this.data = data;
    }

    public void setLevelId(String levelId) {
        this.levelId = levelId;
    }
}