package models.vo;

public class HistoryNPSBlock {
    private String month;
    private double monthNumber;
    private double score;
    private double scorePrev;

    public String getMonth() {
        return this.month;
    }

    public double getMonthNumber() {
        return this.monthNumber;
    }

    public double getScore() {
        return this.score;
    }

    public double getScorePrev() {
        return this.scorePrev;
    }

    public void setMonth(String month) {
        this.month = month;
    }

    public void setMonthNumber(double monthNumber) {
        this.monthNumber = monthNumber;
    }

    public void setScore(double score) {
        this.score = score;
    }

    public void setScorePrev(double scorePrev) {
        this.scorePrev = scorePrev;
    }
}