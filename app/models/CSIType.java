package models;

import play.data.validation.*;
import play.db.jpa.*;
import javax.persistence.*;
import java.util.*;

@Entity
@Table(name = "PUL_CSI_TYPE", schema = "PORTALUNICO")
public class CSIType extends GenericModel {
    @Id
    @Column(name = "FN_ID_CSI_TYPE", nullable = true)
    @GeneratedValue(strategy = GenerationType.AUTO)    
    public Integer id;
    
    @Required
    @MaxSize(50)
    @Column(name = "FC_DESC_CSI_TYPE", length = 50)
    public String desc;
}