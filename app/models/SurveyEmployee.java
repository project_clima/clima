package models;

import play.data.validation.*;
import play.db.jpa.*;
import javax.persistence.*;
import java.util.*;

@Entity
@Table(name = "PUL_SURVEY_EMPLOYEE", schema = "PORTALUNICO")
public class SurveyEmployee extends GenericModel {
    @Column(name = "FC_USERNAME", length = 255)
    public String username;

    @Column(name = "FC_PASSWORD", length = 255)
    public String password;

    @Column(name = "FN_PLAN")
    public Integer maxAttempts;

    @Column(name = "FN_ANSWERED")
    public Integer attempts;

    @Column(name = "FC_SEND_MAIL")
    public String sendMail;

    @Column(name = "FC_EXCEPTION_PLAN")
    public String exceptionPlan;
    
    @Column(name = "FN_TREE_PLAN")
    public Integer treePlan;

    @Column(name = "FD_CRE_DATE")
    public Date creationDate;

    @Column(name = "FD_MOD_DATE")
    public Date modificationDate;

    @Column(name = "FC_CRE_FOR")
    public String createdBy;
    
    @Column(name = "FC_MOD_FOR")
    public String modifiedBy;

    @ManyToOne
    @Id
    @JoinColumn(name = "FN_ID_SURVEY")
    public Survey survey;

    @ManyToOne
    @Id
    @JoinColumn(name = "FN_ID_EMPLOYEE_CLIMA")
    public EmployeeClima employeeClima;
}
