package models;

import play.data.validation.MaxSize;
import play.data.validation.Required;
import play.db.jpa.*;
import javax.persistence.*;
import java.util.*;

@Entity
@Table(name = "PUL_SECTION", schema = "PORTALUNICO")
public class Seccion extends GenericModel{

    @Id
    @Column(name = "FN_SECTION_ID", nullable = true)
    @GeneratedValue(strategy = GenerationType.AUTO)    
    public Long id;

    @Required
    @MaxSize(100)
    @Column(name = "FC_DESC_SECTION", length = 100)
    public String nombreSeccion;

    @OneToMany
    @JoinColumn(name = "FN_SECTION_ID")
    public List<Permiso> permisos;

    @Column(name = "FC_MOD_FOR")
    public String modifiedBy;

    @Column(name = "FC_CRE_FOR") 
    public String createdBy;

    @Column(name = "FD_CRE_DATE") 
    public Date createdDate;

    @Column(name = "FD_MOD_DATE")
    public Date modifiedDate;
    
    @Column(name = "FN_ID_MODULE")
    public Long idModule;
}