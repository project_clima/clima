package models;

import java.text.SimpleDateFormat;
import play.db.jpa.*;
import javax.persistence.*;
import java.util.*;
import models.EmployeeClima;
import models.SurveyEmployee;

@Entity
@Table(name = "PUL_EMAIL_SEND_LOG", schema = "PORTALUNICO")
public class EmailSendLog extends GenericModel {
    @Id
    @Column(name = "ID", nullable = true)
    @GeneratedValue(strategy = GenerationType.AUTO)
    public Integer id;

    @Column(name = "FC_EMPLOYEE_NUMBER")
    public Long employeeNumber;
    
    @Column(name = "FC_FULL_NAME")
    public String name;
    
    @Column(name = "FC_EMAIL")
    public String email;

    @Column(name = "FC_FUNCTION")
    public String function;

    @Column(name = "FC_MESSAGE", length = 2048)
    public String message;

    @Column(name = "FD_SEND_DATE")
    public Date sendDate;
    
    @Column(name = "FN_SURVEY_ID")
    public Integer surveyId;

    @Column(name = "FN_SENT")
    public Boolean sent;
    
    public static void saveLog(String email, int surveyId, String error, boolean sent) {
         SimpleDateFormat sdf = new SimpleDateFormat("MM/yyyy");
        
        if (email == null) {
            return;
        }
        
        Survey survey = Survey.findById(surveyId);
        
        if (survey == null) {
            return;
        }
        
        EmployeeClima employee = EmployeeClima.find(
            "lower(email) = ?1 and to_char(structureDate,'MM/YYYY') = ?2", 
                email.toLowerCase(), sdf.format(survey.surveyDate)
        ).first();
        
        if (employee == null) {
            return;
        }
        
        EmailSendLog log = new EmailSendLog();
        
        log.employeeNumber = employee.employeeNumber;
        log.name           = employee.firstName + " " + employee.lastName;
        log.email          = email;
        log.function       = employee.function;
        log.message        = error;
        log.sendDate       = new Date();
        log.surveyId       = surveyId;
        log.sent           = sent;
        
        log.save();
        updateSurvey(surveyId, employee.id, sent);
    }
    
    public static void updateSurvey(int surveyId, int employeeId, boolean sent) {
        SurveyEmployee surveyEmployee = SurveyEmployee.find(
            "survey.id = ?1 and employeeClima.id = ?2",  surveyId, employeeId
        ).first();
        
        if (surveyEmployee != null && sent == true) {
            surveyEmployee.sendMail = "1";
            surveyEmployee.save();
        }
    }
}
