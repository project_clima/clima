package models;

import java.math.BigDecimal;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import play.db.jpa.GenericModel;

@Entity
@Table(name = "PUL_USER_ROLE", schema = "PORTALUNICO")
public class UserRole extends GenericModel {

    @Id
    @Column(name = "FN_ID_USER")
    public BigDecimal idUser;

    @Id
    @Column(name = "FN_ID_ROLE")
    public Long idRole;

}
