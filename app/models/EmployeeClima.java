package models;

import play.data.validation.*;
import play.db.jpa.*;
import javax.persistence.*;
import java.util.*;

@Entity
@Table(name = "PUL_EMPLOYEE_CLIMA", schema = "PORTALUNICO")
public class EmployeeClima extends GenericModel {

        @Id
        @Column(name = "FN_ID_EMPLOYEE_CLIMA", nullable = true)
        @GeneratedValue(strategy = GenerationType.AUTO)
        public Integer id;

        @Column(name = "FC_LAST_NAME")
        public String lastName;

        @Column(name = "FC_DIVISION")
        public String division;

        @Column(name = "FC_MANAGEMENT")
        public String managament;

        @Column(name = "FC_EMAIL")
        public String email;

        @Column(name = "FC_FIRST_NAME")
        public String firstName;

        @Column(name = "FN_EMPLOYEE_NUMBER")
        public Long employeeNumber;
        
        @Column(name = "FN_HUMAN_RESOURCES")
        public Integer humanResources;

        @Column(name = "FN_LEVEL")
        public Integer vLevel;

        @Column(name = "FN_MANAGER")
        public Integer manager;

        @Column(name = "FC_AREA")
        public String area;

        @Column(name = "FN_USERID")
        public Integer userId;

        @Column(name = "FC_DEPARTMENT")
        public String department;

        @Column(name = "FC_FUNCTION")
        public String function;

        @Column(name = "FN_KEY_PLANT")
        public String keyPlan;

        @Column(name = "FC_DESC_LOCATION")
        public String location;

        @Column(name = "FC_TITLE")
        public String title;

        @ManyToOne
        @JoinColumn(name = "FN_STATUS_EMPLOYEE")
        public StatusEmployee statusEmployee;

        @Column(name = "FN_ID_STORE")
        public Integer idStore;

        @Column(name = "FD_CONTRACT_DATE")
        public Date contractDate;

        @Column(name = "FD_STRUCTURE_DATE")
        public Date structureDate;

        @Column(name = "FC_MOD_FOR")
        public String modifiedBy;

        @Column(name = "FC_CRE_FOR")
        public String createdBy;

        @Column(name = "FD_CRE_DATE")
        public Date createdDate;

        @Column(name = "FD_MOD_DATE")
        public Date modifiedDate;
        
        //FN_OLD_MANAGER
        @Column(name = "FN_OLD_MANAGER")
        public Long oldManager;
}
