package models;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import play.db.jpa.GenericModel;

@Entity
@Table(name = "PUL_NPS_MAIL_SEND_LOG", schema = "PORTALUNICO")
public class NpsMailLog extends GenericModel {

    @Id
    @Column(name = "FN_ID_SEND_MAIL_LOG", nullable = true)
    @GeneratedValue(strategy = GenerationType.AUTO)
    public int id;

    @Column(name = "FN_MONTH")
    public long month;

    @Column(name = "FN_DAY")
    public long day;

    @Column(name = "FN_USERID")
    public long userid;

    @Column(name = "FC_RECORDING")
    public String recording;

    @Column(name = "FN_YEAR")
    public long year;

    @Column(name = "FN_SENT_MAIL")
    public int sentMail;
}
