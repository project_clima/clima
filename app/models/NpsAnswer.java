package models;

import java.util.Date;
import java.util.List;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.OrderBy;
import javax.persistence.Table;
import models.nps.Challenge;
import org.hibernate.annotations.IndexColumn;
import play.db.jpa.GenericModel;

@Entity
@Table(name = "PUL_NPS_ANSWER", schema = "PORTALUNICO")
public class NpsAnswer extends GenericModel {

    @Id
    @Column(name = "FN_ID_ANSWER", nullable = true)
    @GeneratedValue(strategy = GenerationType.AUTO)
    public Long id;

    @OneToMany(mappedBy="key.idAnswer", fetch=FetchType.LAZY)
    @IndexColumn(name = "FN_ID_IMPUGN", base=0)
    public List<Challenge> challenges;
    
    @Column(name = "FN_EMPLOYEE_NUMBER")
    public Long idEmployeeNumber;

    @Column(name = "FN_ID_EMPLOYEE_NPS")
    public Long idEmployeeNps;
    
    @Column(name = "FD_STRUCTURE_DATE")
    public Date structureDate;

    @Column(name = "FC_ID_AREA")
    public String area;

    @Column(name = "FC_ID_PRACTICE")
    public String practice;

    @Column(name = "FN_YEAR")
    public Long year;

    @Column(name = "FN_MONTH")
    public Long month;

    @Column(name = "FN_DAY")
    public Long day;

    @Column(name = "FN_ID_PERSON_EVALUATED")
    public String idEvaluatedPerson;

    @Column(name = "FC_RECORDING")
    public String idRecording;

    @Column(name = "FC_CLIENT_ID")
    public String clientId;

    @Column(name = "FC_ID_CATEGORY_1_1")
    public String category11;

    @Column(name = "FC_ID_CATEGORY_1_2")
    public String category12;

    @Column(name = "FC_ID_CATEGORY_1_3")
    public String category13;

    @Column(name = "FC_CLIENT_NAME")
    public String clientName;

    @Column(name = "FC_LADA")
    public String lada;

    @Column(name = "FC_PHONE")
    public String phone;

    @Column(name = "FN_ID_STORE")
    public Long idStore;

    @Column(name = "FC_EMAIL")
    public String email;

    @Column(name = "FN_SURVEY_ORIGIN_ID")
    public Long surveyOriginId;

    @Column(name = "FC_ID_CATEGORY_2_1")
    public String category21;

    @Column(name = "FC_ID_CATEGORY_2_2")
    public String category22;

    @Column(name = "FC_ID_CATEGORY_2_3")
    public String category23;

    @Column(name = "FC_DOC")
    public String doc;

    @Column(name = "FC_TERMINAL")
    public String terminal;

    @Column(name = "FC_TICKET_DAY")
    public String ticketDay;

    @Column(name = "FC_TICKET_MONTH")
    public String ticketMonth;

    @Column(name = "FC_TICKET_YEAR")
    public String ticketYear;

    @Column(name = "FC_IP")
    public String ip;

    @Column(name = "FC_CRE_FOR")
    public String createdBy;

    @Column(name = "FD_CRE_DATE")
    public Date createdDate;

    @Column(name = "FC_MOD_FOR")
    public String modifiedBy;

    @Column(name = "FD_MOD_DATE")
    public Date modifiedDate;

    @Column(name = "FC_SECTION_ID")
    public String section;

    @Column(name = "FN_USERID_CHIEF")
    public Long userIdChief;

    @Column(name = "FN_USERID_MANAGER")
    public Long userIdManager;

    @Column(name = "FN_USERID_DIRECTOR")
    public Long userIdDirector;

    @Column(name = "FN_USERID_ZONE")
    public Long userIdZone;

    @Column(name = "FC_TITLE")
    public String title;

    @Column(name = "FN_ID_FOLLOW")
    public Long idFollow;

    @Column(name = "FN_ID_IMPUGN")
    public Long idImpugn;

    @Column(name = "FN_USERID")
    public Long userid;

    @Column(name = "FN_ID_FILE")
    public Long idFile;

    @Column(name = "FC_ZONE")
    public String zone;

    @Column(name = "FN_ID_STORE_FILE")
    public Long idStoreFile;

    @Column(name = "FN_STORE_GROUP_ID")
    public Long idStoreGroup;

    @Column(name = "FN_STORE_BUSINESS_ID")
    public Long idBusinessStore;

    @Column(name="FC_SKU")
    public String sku;
    
    @Column(name="FC_DESC_SKU")
    public String skuDesc;
    
    @Column(name="FN_STATUS")
    public Integer status;
    
    @Column(name="FC_USERID_NAME")
    public String userName;
    
    @Column(name="FC_CHIEF_NAME")
    public String chiefName;
    
    @Column(name="FC_MANAGER_NAME")
    public String managerName;
    
    @Column(name="FC_DIRECTOR_NAME")
    public String directorName;
    
    @Column(name="FC_ZONE_NAME")
    public String zoneName;
    
    @OneToMany(cascade=CascadeType.ALL)
    @OrderBy("FN_ID_QUESTION asc")
    @JoinColumn(name = "FN_ID_ANSWER")
    public List<NpsQuestionAnswer> questionsAnswers;

}
