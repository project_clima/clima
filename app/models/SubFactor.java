package models;

import java.util.Date;
import java.util.List;
import play.data.validation.*;
import play.db.jpa.*;
import javax.persistence.*;

@Entity
@Table(name = "PUL_SUBFACTOR", schema = "PORTALUNICO")
public class SubFactor extends GenericModel {
    @Id
    @Column(name = "FN_ID_SUBFACTOR", nullable = true)
    @GeneratedValue(strategy = GenerationType.AUTO)    
    public Integer id;
    
    @Required
    @MaxSize(255)
    @Column(name = "FC_DESC_SUBFACTOR", length = 255)
    public String description;
    
    @Required
    @ManyToOne
    @JoinColumn(name = "FN_ID_FACTOR")
    public Factor factor;
    
    @Column(name = "FN_PERCENT", precision = 10, scale=4)
    public Float percent;
    
    @Column(name = "FN_POINTS", precision = 10, scale=4)
    public Float points;
    
    @Column(name = "FC_CRE_FOR")
    public String createdBy;
    
    @Column(name = "FC_MOD_FOR")
    public String modifiedBy; 

    @Column(name = "FD_CRE_DATE")
    public Date createdDate;

    @Column(name = "FD_MOD_DATE")
    public Date modifiedDate;
    
    @OneToMany
    @JoinColumn(name = "FN_ID_SUBFACTOR")
    public List<Question> questions;
}