package models;

import java.util.Date;
import java.util.List;
import play.data.validation.*;
import play.db.jpa.*;
import javax.persistence.*;

@Entity
@Table(name = "PUL_PARAM_PLAN", schema = "PORTALUNICO")
public class Goal extends GenericModel {    
    @Id
    @Column(name = "FN_PLANID", nullable = true)
    @SequenceGenerator(name = "SQ_PARAM_PLAN", sequenceName = "PORTALUNICO.SQ_PARAM_PLAN", allocationSize = 1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "SQ_PARAM_PLAN")
    public Integer id;
    
    @MaxSize(255)
    @Column(name = "FC_DESC_PLAN", length = 255)
    public String description;
    
    @Column(name = "FN_YEAR")
    public Integer year;
    
    @Column(name = "FN_MAX_VALUE", precision = 10, scale=4)
    public Float maxValue;
    
    @Column(name = "FN_MIN_VALUE", precision = 10, scale=4)
    public Float minValue;
    
    @Column(name = "FC_CRE_FOR", length = 30)
    public String createdBy;
    
    @Column(name = "FC_MOD_FOR", length = 30)
    public String modifiedBy; 

    @Column(name = "FD_CRE_DATE")
    public Date createdDate;

    @Column(name = "FD_MOD_DATE")
    public Date modifiedDate;
}