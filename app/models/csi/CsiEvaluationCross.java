package models.csi;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import play.db.jpa.GenericModel;

@Entity
@Table(name = "PUL_CSI_EVALUATION_CROSS", schema = "PORTALUNICO")
public class CsiEvaluationCross extends GenericModel {

	@Id
    @Column(name = "FN_ID_EVA_CROSS", nullable = true)
    @GeneratedValue(strategy = GenerationType.AUTO)
	public Integer id;
	
	@Column(name = "FN_EVALUATED_EMP")
	public Integer idEvaluatedEmployee;
	
	@Column(name = "FN_EVALUATOR_EMP")
	public Integer idEvaluatorEmployee;
	
	@Column(name = "FN_ID_SURVEY")
	public Integer idSurvey;
	
	@Column(name = "FD_CRE_DATE")
	public Date createdDate;
	
	@Column(name = "FD_MOD_DATE")
	public Date modifiedDate;
	
	@Column(name = "FC_CRE_BY")
	public String createdBy;
	
	@Column(name = "FC_MOD_BY")
	public String modifiedBy;

	@Override
	public String toString() {
		return "CsiEvaluationCross [id=" + id + ", idEvaluatedEmployee=" + idEvaluatedEmployee
				+ ", idEvaluatorEmployee=" + idEvaluatorEmployee + ", idSurvey=" + idSurvey + ", createdDate="
				+ createdDate + ", modifiedDate=" + modifiedDate + ", createdBy=" + createdBy + ", modifiedBy="
				+ modifiedBy + "]";
	}
	
	

}
