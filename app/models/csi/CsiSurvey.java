package models.csi;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import models.SurveyType;
import play.data.validation.Required;
import play.db.jpa.GenericModel;

@Entity
@Table(name = "PUL_CSI_SURVEY", schema = "PORTALUNICO")
public class CsiSurvey extends GenericModel {	
	@Id
    @Column(name = "FN_ID_SURVEY", nullable = false)    
    public Integer idSurvey;
	
	@Column(name = "FN_GOAL", nullable = false)
	public Integer goal;
	
	@Column(name="FN_ID_CSI_TYPE", nullable = false)
	public Integer surveyType;
//    @Required
//    @ManyToOne
//    @JoinColumn(name = "FN_ID_SURVEY_TYPE")
//    public SurveyType surveyType;
    
	@Column(name="FC_AREA", nullable = true)
	public String area;
	
	@Column(name="FN_IMPUGNED", nullable = false)
	public Integer impugned;
	
	
}
