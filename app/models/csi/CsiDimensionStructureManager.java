package models.csi;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import models.StatusEmployee;
import play.db.jpa.GenericModel;

@Entity
@Table(name = "PUL_CSI_DIM_StructureManager", schema = "PORTALUNICO")
public class CsiDimensionStructureManager extends GenericModel {	
	
	
	
//	FN_USERID            NUMBER(19) NOT NULL ,--ID del usuario
//	FN_ID_EMPLOYEE_CSI   NUMBER(19) NOT NULL ,-- ID de empleado tabla de resultados
//	FN_EMPLOYEE_NUMBER   NUMBER(19) NULL ,-- Numero de empleado Liverpool
	

	@Column(name = "FN_USERID")
    public Integer userId;
    
	@Id	
	@Column(name = "FN_ID_EMPLOYEE_CSI")
	public Integer id;

    @Column(name = "FN_EMPLOYEE_NUMBER")
    public Integer employeeNumber; 
//	FN_LEVEL             NUMBER(19) NULL ,-- Nivel del empleado
//	FC_TITLE             VARCHAR2(255) NULL ,-- Titulo del empleado
//	FC_AREA              VARCHAR2(10) NULL ,	-- Area 

    @Column(name = "FN_LEVEL")
    public Integer vLevel;

    @Column(name = "FC_TITLE")
    public String title;
    
    @Column(name = "FC_AREA")
    public String area;

    
//	FN_MANAGER           NUMBER(19) NULL ,-- el  manager  del empleado
//	FN_HUMAN_RESOURCES   NUMBER(19) NULL ,
//	FD_STRUCTURE_DATE    DATE NULL ,

    @Column(name = "FN_MANAGER")
    public Integer manager;

    @Column(name = "FN_HUMAN_RESOURCES")
    public Integer humanResources;

    @Column(name = "FD_STRUCTURE_DATE")
    public Date structureDate;
    
    
//	FN_STATUS_EMPLOYEE   NUMBER(19) NULL ,
//	FC_DEPARTMENT VARCHAR2(255 BYTE) ,
//	FC_MANAGEMENT VARCHAR2(100 BYTE),
//	FN_ID_STORE          NUMBER NULL ,
//	FC_SUB_DIV           VARCHAR2(50 BYTE) NULL ,-- Zona 


    @ManyToOne
    @JoinColumn(name = "FN_STATUS_EMPLOYEE")
    public StatusEmployee statusEmployee;
    @Column(name = "FC_DEPARTMENT")
    public String department;
	@Column(name = "FC_MANAGEMENT")
	public String managament;
    @Column(name = "FN_ID_STORE")
    public Integer idStore;
    @Column(name = "FC_SUB_DIV", length = 100)
    public String subDivision;
    
    
    
    
    
    //@Column(name = "FC_FUNCTION")
    //public String function;

    

    
    

    @Column(name = "FN_MANAGER_0")
    public Integer managerLevel0;
    @Column(name = "FN_MANAGER_1")
    public Integer managerLevel1;
    @Column(name = "FN_MANAGER_2")
    public Integer managerLevel2;
    @Column(name = "FN_MANAGER_3")
    public Integer managerLevel3;
    @Column(name = "FN_MANAGER_4")
    public Integer managerLevel4;
    @Column(name = "FN_MANAGER_5")
    public Integer managerLevel5;
    @Column(name = "FN_MANAGER_6")
    public Integer managerLevel6;
    @Column(name = "FN_MANAGER_7")
    public Integer managerLevel7;
    @Column(name = "FN_MANAGER_8")
    public Integer managerLevel8;
    @Column(name = "FN_MANAGER_9")
    public Integer managerLevel9;

    @Column(name = "FN_MANAGER_10")
    public Integer managerLevel10;
    @Column(name = "FN_MANAGER_11")
    public Integer managerLevel11;
    @Column(name = "FN_MANAGER_12")
    public Integer managerLevel12;
    @Column(name = "FN_MANAGER_13")
    public Integer managerLevel13;
    @Column(name = "FN_MANAGER_14")
    public Integer managerLevel14;
    @Column(name = "FN_MANAGER_15")
    public Integer managerLevel15;
	
}
