package models.csi;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import play.db.jpa.GenericModel;

@Entity
@Table(name = "PUL_CSI_EMPLOYEE_SECTIONS", schema = "PORTALUNICO")
public class CsiEmployeeSections extends GenericModel {

    @Id
    @Column(name = "FN_ID_EMPLOYEE_SECTIONS", nullable = true)
    @GeneratedValue(strategy = GenerationType.AUTO)
    public Integer id;

    @Column(name = "FN_ID_SECTION")
    public Integer idSection;

    @Column(name = "FN_ID_USER")
    public Integer idUser;

    @Column(name = "FD_STRUCTURE_DATE")
    public Date structureDate;

}
