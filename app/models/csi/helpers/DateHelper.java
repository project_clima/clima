package models.csi.helpers;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

public class DateHelper {
	/**
	 * Convierte en fecha un String según el formato indicado
	 * @param formato
	 * @param dateStr
	 * @return Date
	 * @throws ParseException
	 */
	public static Date string2Date (String formato, String dateStr) throws ParseException{
		final DateFormat format = new SimpleDateFormat(formato);
		return  format.parse(dateStr);
		
	}
	
	/**
	 * Convierte en fecha un string
	 * @param dateStr
	 * @return Date
	 * @throws ParseException
	 */
	public static Date string2Date (String dateStr) throws ParseException{
		final DateFormat format = new SimpleDateFormat("yyyy/MM/dd");
		return  format.parse(dateStr);
		
	}
	
	/**
	 * Convierte en String una fecha según el formato indicado
	 * @param date
	 * @param format
	 * @return String
	 */
	public static String date2String(Date date, String format) {
		SimpleDateFormat formatter = new SimpleDateFormat(format);
		return  formatter.format(date.getTime());
	}
	/**
	 * Convierte en String una fecha
	 * @param date
	 * @return String
	 */
	public static String date2String(Date date) {
		SimpleDateFormat formatter = new SimpleDateFormat("yyyy/MM/dd");
		return  formatter.format(date.getTime());
	}
	
	/**
	 * Mueve a primera hora una fecha
	 * @param fecha
	 * @return Date
	 * @throws ParseException
	 */
	public static Date moverAPrimeraHora(String fecha) throws ParseException {
		return moverAPrimeraHora(DateHelper.string2Date(fecha));	
	}
	/**
	 * Resta un milisegundo a una fecha
	 * @param fecha
	 * @return Date
	 * @throws ParseException
	 */
	public static Date restarUnMilliSegundo(String fecha) throws ParseException {
		return restarUnMilliSegundo(DateHelper.string2Date(fecha));	
	}
	/**
	 * Regresa el último día del mes
	 * @param fecha
	 * @return Date
	 * @throws ParseException
	 */
	public static Date ultimoDiaDelMes(String fecha) throws ParseException {
		return ultimoDiaDelMes(DateHelper.string2Date(fecha));	
	}
	/**
	 * Mueve a última hora una fecha
	 * @param fecha
	 * @return Date
	 * @throws ParseException
	 */
	public static Date moverAUltimaHora(String fecha) throws ParseException {
		return moverAUltimaHora(DateHelper.string2Date(fecha));	
	}
	
	/**
	 * Mueve a primera hora una fecha. Regresa la fecha en el formato dado
	 * @param fecha
	 * @param formato
	 * @return Date
	 * @throws ParseException
	 */
	public static Date moverAPrimeraHora(String fecha,String formato) throws ParseException {
		return moverAPrimeraHora(DateHelper.string2Date(formato,fecha));	
	}
	/**
	 * Resta un milisegundo a una fecha. Regresa la fecha en el formato dado
	 * @param fecha
	 * @param formato
	 * @return Date
	 * @throws ParseException
	 */
	public static Date restarUnMilliSegundo(String fecha,String formato) throws ParseException {
		return restarUnMilliSegundo(DateHelper.string2Date(formato,fecha));	
	}
	/**
	 * Regresa el último día del mes. Regresa la fecha en el formato dado
	 * @param fecha
	 * @param formato
	 * @return
	 * @throws ParseException
	 */
	public static Date ultimoDiaDelMes(String fecha,String formato) throws ParseException {
		return ultimoDiaDelMes(DateHelper.string2Date(formato,fecha));	
	}
	/**
	 * Mueve última hora una fecha. Regresa la fecha en el formato dado
	 * @param fecha
	 * @param formato
	 * @return Date
	 * @throws ParseException
	 */
	public static Date moverAUltimaHora(String fecha,String formato) throws ParseException {
		return moverAUltimaHora(DateHelper.string2Date(formato,fecha));	
	}

	/**
	 * Regresa el último día del mes
	 * @param pFecha
	 * @return Date
	 */
	public static Date ultimoDiaDelMes(Date pFecha) {
     	if (pFecha == null)
    	  return null;
	    Calendar calendarAux = Calendar.getInstance();
	    calendarAux.setTime(pFecha);
	    calendarAux.add(Calendar.MONTH, 1);
	    calendarAux.add(Calendar.MILLISECOND, -1);
	    return calendarAux.getTime();	          	  
	}

	/**
	 * Resta un milisegundo a la fecha
	 * @param pFecha
	 * @return Date
	 */
	public static Date restarUnMilliSegundo(Date pFecha) {
     	if (pFecha == null)
    	  return null;
	    Calendar calendarAux = Calendar.getInstance();
	    calendarAux.setTime(pFecha);
	    calendarAux.add(Calendar.MILLISECOND, -1);
	    return calendarAux.getTime();	          	  
	}
	
	/**
	 * Mueve a primera hora una fecha
	 * @param pFecha
	 * @return Date
	 */
	public static Date moverAPrimeraHora(Date pFecha) {
     	if (pFecha == null)
    	  return null;
	    Calendar calendarAux = Calendar.getInstance();
	    calendarAux.setTime(pFecha);
	    calendarAux.set(Calendar.HOUR_OF_DAY, 0);
	    calendarAux.set(Calendar.MINUTE, 0);
	    calendarAux.set(Calendar.SECOND, 0);
	    calendarAux.set(Calendar.MILLISECOND, 0);
	    return calendarAux.getTime();	          	  
	}
	/**
	 * Mueve a última hora una fecha
	 * @param pFecha
	 * @return
	 */
	public static Date moverAUltimaHora(Date pFecha) {
     	if (pFecha == null)
    	  return null;
	    Calendar calendarAux = Calendar.getInstance();
	    calendarAux.setTime(pFecha);
	    calendarAux.set(Calendar.HOUR_OF_DAY, 23);
	    calendarAux.set(Calendar.MINUTE, 59);
	    calendarAux.set(Calendar.SECOND, 59);
	    calendarAux.set(Calendar.MILLISECOND, 99);
	    return calendarAux.getTime();	          	  
	}
	
}
