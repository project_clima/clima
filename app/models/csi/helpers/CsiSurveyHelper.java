package models.csi.helpers;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import models.Survey;
import models.SurveyType;
import models.Usuario;

public class CsiSurveyHelper {
	/**
	 * Crea un rago de años
	 * @return List de Integer
	 */
	 public static List<Integer> getYearsRange() {
	        int minYear = querys.SurveysGeneratorQuery.getYearCsiStructure("min");
	        int maxYear = querys.SurveysGeneratorQuery.getYearCsiStructure("max");
	        
	        List<Integer> years = new ArrayList();
	        for (int year = minYear; year <= maxYear; year++) {
	            years.add(year);
	        }
	        
	        return years;
	    }
	    
	 /**
	  * Crea una encuesta
	  * @param year
	  * @param month
	  * @param nameSurvey
	  * @param validityFrom
	  * @param validityTo
	  * @param user
	  * @return Survey
	  */
	 public static Survey createSurvey(String year, String month, String nameSurvey, Date validityFrom, Date validityTo, Usuario user) {
	    	
			
			Survey survey =  new Survey();		
			survey.surveyType = SurveyType.findById(5);
			survey.name = nameSurvey;
			survey.createdBy = user.username;
			survey.creationDate = new Date() ;
			survey.modifiedBy =  user;
			survey.modificationDate = new Date() ;
			survey.dateEnd =  validityTo ;
			survey.dateInitial  =  validityFrom ;
			survey.surveyDate  = getSurveyDate(year, month);
			
			return survey;
	    }
	    
	 /**
	  * Obtiene la fecha para la encuesta
	  * @param year
	  * @param month
	  * @return Date
	  */
	    public static Date getSurveyDate(String year, String month) {
	    	Calendar surveyDate = Calendar.getInstance();
			surveyDate.set(Calendar.YEAR, Integer.parseInt(year));
			surveyDate.set(Calendar.MONTH, (Integer.parseInt(month) -1));
			surveyDate.set(Calendar.DAY_OF_MONTH, 1);
			surveyDate.set(Calendar.HOUR, 0);
			surveyDate.set(Calendar.MINUTE, 0);
			surveyDate.set(Calendar.SECOND, 0);
			
			return surveyDate.getTime();
			
	    }
}
