package models.csi.helpers;

import java.util.Date;

import models.CSIEvaluationResults;
import models.csi.CsiEvaluationCross;
import models.csi.CsiEvaluationCrossLog;
import models.enume.ErrorTypeEnum;

public class CsiEvaluationHelper {
	
	/**
	 * Crea el cruce de evaluaciones
	 * @param evaluador
	 * @param evaluado
	 * @param idSurvey
	 * @param username
	 * @return CsiEvaluationCross
	 */
	public static CsiEvaluationCross createCsiEvaluationCross(Integer evaluador, Integer evaluado, Integer idSurvey, String username) {
		
		CsiEvaluationCross csiEvaluationCross = new CsiEvaluationCross();
		csiEvaluationCross.createdBy = username;
		csiEvaluationCross.createdDate = new Date();
		csiEvaluationCross.idEvaluatedEmployee = evaluado;
		csiEvaluationCross.idEvaluatorEmployee = evaluador;
		csiEvaluationCross.idSurvey = idSurvey;	
		
		return csiEvaluationCross;
		
	}
	
	/**
	 * Crea el log de cruce de evaluaciones
	 * @param idSurvey
	 * @param idEmployee
	 * @param idErrorType
	 * @return CsiEvaluationCrossLog
	 */
	public static CsiEvaluationCrossLog createCsiEvaluationCrossLog(Integer idSurvey, String idEmployee, Integer idErrorType) {
		
		CsiEvaluationCrossLog csiEvaluationCrossLog = new CsiEvaluationCrossLog();
		csiEvaluationCrossLog.idEmployee = idEmployee;
		csiEvaluationCrossLog.idErrorType = idErrorType;
		csiEvaluationCrossLog.idSurvey = idSurvey;
		csiEvaluationCrossLog.save();
		
		return csiEvaluationCrossLog;
		
	}
	
	/**
	 * Guarda los Resultados de la evaluación
	 * @param idSurvey
	 * @param idEmployee
	 * @param comments
	 * @param username
	 * @param question
	 * @param result
	 * @param resultText
	 * @param idEvaluator
	 * @return CSIEvaluationResults
	 */
	public static CSIEvaluationResults createCsiEvaluationResults(Integer idSurvey, Integer idEmployee, String comments, String username, Integer question, 
			Integer result, String resultText, Integer idEvaluator) {
		
		CSIEvaluationResults csiEvaluationResults = new CSIEvaluationResults();
		csiEvaluationResults.idEmployee = idEmployee;
		csiEvaluationResults.idEvaluator = idEvaluator;
		csiEvaluationResults.comments = comments;
		csiEvaluationResults.idSurvey = idSurvey;
		csiEvaluationResults.createdBy = username;
		csiEvaluationResults.createdDate = new Date();
		csiEvaluationResults.idQuestion = question;
		csiEvaluationResults.resultText = resultText;
		csiEvaluationResults.result =  result;
		csiEvaluationResults.save();
		
		return csiEvaluationResults;
		
	}

}
