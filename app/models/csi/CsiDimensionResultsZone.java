package models.csi;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import models.StatusEmployee;
import play.data.validation.MaxSize;
import play.db.jpa.GenericModel;

@Entity
@Table(name = "PUL_CSI_DIM_RESULTS", schema = "PORTALUNICO")
public class CsiDimensionResultsZone extends GenericModel {	
	
//		FN_ID_SURVEY				   NUMBER NOT NULL ,    -- id de la encuesta 
//		FC_AREA							VARCHAR2(50),
//		FN_ID_CSI_TYPE					NUMBER NOT NULL,

	@Id
  	@Column(name = "FN_ID_SURVEY", nullable = true)
  	public Integer idSurvey;

	@Id
  	@Column(name = "FC_AREA", nullable = true)
  	public Integer idArea;

	@Id
  	@Column(name = "FN_ID_CSI_TYPE", nullable = true)
  	public String idCsiType;

	
//			FN_ID_EVALUATED_EMP			   NUMBER NOT NULL ,    -- evaluado
//			FN_ID_QUESTION					 NUMBER(19, 0) ,
//			FN_SORT_ORDER					NUMBER(10, 0),
//			FN_ID_EVA_RESULTS				 NUMBER NOT NULL, 
//			FN_USERID_EVALUATED            NUMBER(19) NOT NULL ,-- evaluado_userid
//			FC_DEPAR_MANA3_EVALUATED VARCHAR2(255 BYTE) , -- departamento evaluador_direccion_desc			
				
	
		

		@Id
		@Column(name = "FN_ID_EVALUATED_EMP")    
		public Integer idEvaluated;
		
		@Id
		@Column(name = "FN_ID_QUESTION")    
		public Integer idQuestion;

	    @Column(name = "FN_SORT_ORDER")
	    public Integer order;
	    
		@Id
		@Column(name = "FN_ID_EVA_RESULTS", nullable = true)    
		public Integer idEvaResults;
		
		@Id
	    @Column(name = "FN_USERID_EVALUATED")
	    public Integer userIdEvaluated;
	    
		@Id
	    @Column(name = "FC_DEPAR_MANA3_EVALUATED")
	    public String direccionManager3Evaluado;
		
//
//		FN_MANAGER_0_EVALUATED           NUMBER(19) NULL , -- jerarquia de managers del evaluador
//		FN_MANAGER_1_EVALUATED           NUMBER(19) NULL , -- jerarquia de managers del evaluador
//		FN_MANAGER_2_EVALUATED           NUMBER(19) NULL , -- jerarquia de managers del evaluador
//		FN_MANAGER_3_EVALUATED           NUMBER(19) NULL , -- jerarquia de managers del evaluador
//		FN_MANAGER_4_EVALUATED           NUMBER(19) NULL , -- jerarquia de managers del evaluador
//		FN_MANAGER_5_EVALUATED           NUMBER(19) NULL , -- jerarquia de managers del evaluador
//		FN_MANAGER_6_EVALUATED           NUMBER(19) NULL , -- jerarquia de managers del evaluador
//		FN_MANAGER_7_EVALUATED           NUMBER(19) NULL , -- jerarquia de managers del evaluador
//		FN_MANAGER_8_EVALUATED           NUMBER(19) NULL , -- jerarquia de managers del evaluador
//		FN_MANAGER_9_EVALUATED           NUMBER(19) NULL , -- jerarquia de managers del evaluador
//		FN_MANAGER_10_EVALUATED           NUMBER(19) NULL ,-- jerarquia de managers del evaluador
//		FN_MANAGER_11_EVALUATED           NUMBER(19) NULL ,-- jerarquia de managers del evaluador
//		FN_MANAGER_12_EVALUATED           NUMBER(19) NULL ,-- jerarquia de managers del evaluador
//		FN_MANAGER_13_EVALUATED           NUMBER(19) NULL ,-- jerarquia de managers del evaluador
//		FN_MANAGER_14_EVALUATED           NUMBER(19) NULL ,-- jerarquia de managers del evaluador
//		FN_MANAGER_15_EVALUATED           NUMBER(19) NULL ,-- jerarquia de managers del evaluador
		

		@Id
	    @Column(name = "FN_MANAGER_0_EVALUATED")
	    public Integer idManager0Evaluado;

		@Id
	    @Column(name = "FN_MANAGER_1_EVALUATED")
	    public Integer idManager1Evaluado;
		@Id
	    @Column(name = "FN_MANAGER_2_EVALUATED")
	    public Integer idManager2Evaluado;
		@Id
	    @Column(name = "FN_MANAGER_3_EVALUATED")
	    public Integer idManager3Evaluado;
		@Id
	    @Column(name = "FN_MANAGER_4_EVALUATED")
	    public Integer idManager4Evaluado;
		@Id
	    @Column(name = "FN_MANAGER_5_EVALUATED")
	    public Integer idManager5Evaluado;
		@Id
	    @Column(name = "FN_MANAGER_6_EVALUATED")
	    public Integer idManager6Evaluado;
		@Id
	    @Column(name = "FN_MANAGER_7_EVALUATED")
	    public Integer idManager7Evaluado;
		@Id
	    @Column(name = "FN_MANAGER_8_EVALUATED")
	    public Integer idManager8Evaluado;
		@Id
	    @Column(name = "FN_MANAGER_9_EVALUATED")
	    public Integer idManager9Evaluado;

		@Id
	    @Column(name = "FN_MANAGER_10_EVALUATED")
	    public Integer idManager10Evaluado;

		@Id
	    @Column(name = "FN_MANAGER_11_EVALUATED")
	    public Integer idManager11Evaluado;
		@Id
	    @Column(name = "FN_MANAGER_12_EVALUATED")
	    public Integer idManager12Evaluado;
		@Id
	    @Column(name = "FN_MANAGER_13_EVALUATED")
	    public Integer idManager13Evaluado;
		@Id
	    @Column(name = "FN_MANAGER_14_EVALUATED")
	    public Integer idManager14Evaluado;
		@Id
	    @Column(name = "FN_MANAGER_15_EVALUATED")
	    public Integer idManager15Evaluado;
		
		
		
		
		
		
		
//		FN_ID_EVALUATOR_EMP			   NUMBER NOT NULL ,    -- evaluador
//		FN_ID_STORE_EVALUATOR  		   NUMBER NULL , -- ID de la tienda evaluador_store
//		FC_SUB_DIV_EVALUATOR           VARCHAR2(50 BYTE) NULL ,-- Zona  evaluador_zona
//		FN_userid_EVALUATOR			   NUMBER(19) NOT NULL , -- evaluador_userid
//		FN_MANAGER_3           		   NUMBER(19) NULL, ---  dirección  fn_manager_3
//		FC_DEPARTMENT_MANAGER_3		   VARCHAR2(255 BYTE) , -- departamento evaluador_direccion_desc
//		FN_RESULT					   NUMBER DEFAULT NULL , -- calif
		
		@Id
		@Column(name = "FN_ID_EVALUATOR_EMP")    
		public Integer idEvaluator;
		
		@Id
	    @Column(name = "FN_ID_STORE_EVALUATOR")
	    public Integer idStoreEvaluator;
		
		@Id
	    @Column(name = "FC_SUB_DIV_EVALUATOR", length = 100)
	    public String subDivisionEvaluator;
		@Id
	    @Column(name = "FN_userid_EVALUATOR")
	    public Integer userIdEvaluator;
		@Id
	    @Column(name = "FN_MANAGER_3_EVALUATOR")
	    public Integer idDireccionManager3Evaluador;
		@Id
	    @Column(name = "FC_DEPAR_MANA3_EVALUATOR")
	    public String direccionManager3Evaluador;

		@Id
		@MaxSize(19)
		@Column(name = "FN_RESULT")   
		public Integer result;
		

		@MaxSize(19)
		@Column(name = "FN_COUNT_detra")
		public Integer countDetractores;

		@MaxSize(19)
		@Column(name = "FN_COUNT_neut")
		public Integer countNeutros;
		

		@MaxSize(19)
		@Column(name = "FN_COUNT_prom")
		public Integer countPromotores;
		

		@MaxSize(19)
		@Column(name = "FN_COUNT_ZBD")
		public Integer countZBD;

		@MaxSize(19)
		@Column(name = "FN_COUNT_ZCO")
		public Integer countZCO;

		@MaxSize(19)
		@Column(name = "FN_COUNT_Z02")
		public Integer countZ02;

		@MaxSize(19)
		@Column(name = "FN_COUNT_Z03")
		public Integer countZ03;

		@MaxSize(19)
		@Column(name = "FN_COUNT_Z04")
		public Integer countZ04;

		@MaxSize(19)
		@Column(name = "FN_COUNT_Z05")
		public Integer countZ05;

		@MaxSize(19)
		@Column(name = "FN_COUNT_Z06")
		public Integer countZ06;

		@MaxSize(19)
		@Column(name = "FN_COUNT_Z07")
		public Integer countZ07;

		@MaxSize(19)
		@Column(name = "FN_COUNT_Z08")
		public Integer countZ08;

		@MaxSize(19)
		@Column(name = "FN_COUNT_Z1A")
		public Integer countZ1A;

		@MaxSize(19)
		@Column(name = "FN_COUNT_Z1B")
		public Integer countZ1B;

		@MaxSize(19)
		@Column(name = "FN_COUNT_ZBT")
		public Integer countZBT;
		
		
}
