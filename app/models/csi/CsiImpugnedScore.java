package models.csi;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import play.db.jpa.GenericModel;

@Entity
@Table(name = "PUL_CSI_IMPUGNED_SCORE", schema = "PORTALUNICO")
public class CsiImpugnedScore extends GenericModel {

    @Id
    @Column(name = "FN_ID_EMPLOYEE", nullable = false)
    public Integer id;

    @Column(name = "FN_ID_SURVEY", nullable = false)
    public Integer idSurvey;

    @Column(name = "FN_IMPUGNED", nullable = false)
    public Integer impugned;
}