package models.csi;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import play.db.jpa.GenericModel;

@Entity
@Table(name = "PUL_CSI_EVAL_CROSS_LOG", schema = "PORTALUNICO")
public class CsiEvaluationCrossLog extends GenericModel {

	@Id
    @Column(name = "FN_ID_EVAL_CROSS_LOG", nullable = true)
    @GeneratedValue(strategy = GenerationType.AUTO)
	public Integer id;
	
	@Column(name = "FC_NUM_EMPLOYEE")
	public String idEmployee;
	
	@Column(name = "FN_ERROR_TYPE")
	public Integer idErrorType;
	
	@Column(name = "FN_ID_SURVEY")
	public Integer idSurvey;
}
