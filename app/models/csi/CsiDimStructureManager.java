package models.csi;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import play.db.jpa.GenericModel;

@Entity
@Table(name = "PUL_CSI_DIM_StructureManager", schema = "PORTALUNICO")
public class CsiDimStructureManager extends GenericModel {
	@Id
	@Column(name = "FN_ID_EMPLOYEE_CSI", nullable = false)
	public Integer idEmployeCsi;
	@Column(name = "FN_EMPLOYEE_NUMBER", nullable = true)
	public Integer employeeNumber;
	@Column(name = "FN_USERID", nullable = false)
	public Integer idUsuario;
	@Column(name = "FN_LEVEL", nullable = false)
	public Integer idNivel;
	@Column(name = "FC_TITLE", nullable = true, length = 255)
	public String title;
	@Column(name = "FC_AREA", nullable = true, length = 10)
	public String aera;
	@Column(name = "FN_MANAGER", nullable = false)
	public Integer idManager;
	@Column(name = "FN_HUMAN_RESOURCES", nullable = true)
	public Integer humanResources;
	@Column(name = "FD_STRUCTURE_DATE", nullable = true)
	public Date fechaEstructura;
	@Column(name = "FN_STATUS_EMPLOYEE", nullable = true)
	public Integer estatusEmpluyee;
	@Column(name = "FC_DEPARTMENT", nullable = true, length = 255)
	public String department;
	@Column(name = "FC_MANAGEMENT", nullable = true, length = 100)
	public String managment;
	@Column(name = "FN_ID_STORE", nullable = true)
	public Integer idStore;
	@Column(name = "FC_SUB_DIV", nullable = true, length = 50)
	public String zona;
	@Column(name = "FN_MANAGER_0", nullable = true)
	public Integer manager0;
	@Column(name = "FN_MANAGER_1", nullable = true)
	public Integer manager1;
	@Column(name = "FN_MANAGER_2", nullable = true)
	public Integer manager2;
	@Column(name = "FN_MANAGER_4", nullable = true)
	public Integer manager4;
	@Column(name = "FN_MANAGER_5", nullable = true)
	public Integer manager5;
	@Column(name = "FN_MANAGER_6", nullable = true)
	public Integer manager6;
	@Column(name = "FN_MANAGER_7", nullable = true)
	public Integer manager7;
	@Column(name = "FN_MANAGER_8", nullable = true)
	public Integer manager8;
	@Column(name = "FN_MANAGER_9", nullable = true)
	public Integer manager9;
	@Column(name = "FN_MANAGER_10", nullable = true)
	public Integer manager10;
	@Column(name = "FN_MANAGER_11", nullable = true)
	public Integer manager11;
	@Column(name = "FN_MANAGER_12", nullable = true)
	public Integer manager12;
	@Column(name = "FN_MANAGER_13", nullable = true)
	public Integer manager13;
	@Column(name = "FN_MANAGER_14", nullable = true)
	public Integer manager14;
	@Column(name = "FN_MANAGER_15", nullable = true)
	public Integer manager15;
}
