package models;

import play.data.validation.*;
import play.db.jpa.*;
import javax.persistence.*;
import java.util.*;

@Entity
@Table(name = "PUL_SURVEY_TYPE", schema = "PORTALUNICO")
public class SurveyType extends GenericModel {
    @Id
    @Column(name = "FN_ID_SURVEY_TYPE", nullable = true)
    @GeneratedValue(strategy = GenerationType.AUTO)    
    public Integer id;
    
    @Required
    @MaxSize(50)
    @Column(name = "FC_SURVEY_NAME", length = 50)
    public String name;
    
    @MaxSize(50)
    @Column(name = "FC_SURVEY_SUBTYPE", length = 50)
    public String subType;

    @Column(name = "FD_CRE_DATE")
    public Date creationDate;
    
    @Column(name = "FD_MOD_DATE")
    public Date modificationDate;
    
    @Column(name = "FC_CRE_FOR")
    public String createdBy;
    
    @Column(name = "FC_MOD_FOR")
    public String modifiedBy;
}