package models;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import play.db.jpa.GenericModel;

@Entity
@Table(name = "PUL_ERROR_TYPE", schema = "PORTALUNICO")
public class ErrorType extends GenericModel {

	@Id
    @Column(name = "FN_ID_ERROR_TYPE", nullable = true)
    @GeneratedValue(strategy = GenerationType.AUTO)	
	public Integer id;
	
	@Column(name = "FC_DESCRIPTION")
	public String description;
	
}
