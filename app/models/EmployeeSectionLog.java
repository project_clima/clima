package models;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import play.data.validation.MaxSize;
import play.data.validation.Required;
import play.db.jpa.GenericModel;

@Entity
@Table(name = "PUL_EMPLOYEE_SECTIONS_LOG", schema = "PORTALUNICO")
public class EmployeeSectionLog extends GenericModel{
	
	@Id	
	@SequenceGenerator(name="ID_EMPLOYEE_SECCION_ID_GENERATOR", sequenceName="PORTALUNICO.SQ_EMPLOYEE_SECTION_LOG_ID")
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="ID_EMPLOYEE_SECCION_ID_GENERATOR")
    @Column(name = "FN_EMPLOYEE_SECTIONS_ID")     
    public Integer employeeSectionId;

	@Required    
    @Column(name = "FN_USERID")
    public Integer userId;  
	
	@Required  
    @Column(name = "FN_ID_SECTION")
    public Integer idSection;
	
	@Required    
    @Column(name = "FN_TYPE_ERROR")
    public Integer typeError;
	
	@Required
	@Column(name = "FD_CREATE_DATE")
    public Date createDate;
	
	@MaxSize(30)
	@Column(name = "FD_CREATE_FOR", length = 30)
    public String createFor;
	
	@Required
	@Column(name = "FD_STRUCTURE_DATE")
    public Date structureDate;

}
