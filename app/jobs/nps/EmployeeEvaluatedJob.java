package jobs.nps;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

import async.NpsMailSender;
import json.nps.dto.NpsMailDto;
import models.NPSTemplate;
import models.NpsAnswer;
import models.NpsMailLog;
import models.NpsQuestionAnswer;
import models.Structure;
import models.Usuario;
import models.enume.NpsTemplateTypeEnum;
import play.Play;
import play.jobs.Job;
import utils.DateUtils;
import utils.NumberConverterUtils;

public class EmployeeEvaluatedJob extends Job {

    public void doJob() {

        NPSTemplate cartaFelicitacion = NPSTemplate.find("idType = ?", NpsTemplateTypeEnum.FELICITACION.getId())
                .first();
        NPSTemplate cartaOportunidadMejora = NPSTemplate
                .find("idType = ?", NpsTemplateTypeEnum.OPORTUNIDAD_MEJORA.getId()).first();
        NPSTemplate cartaExhortacion = NPSTemplate.find("idType = ?", NpsTemplateTypeEnum.EXHORTACION.getId()).first();

        // TODO solo para pruebas
        // Date todaySince = DateUtils.initDate(2017, 5, 13, 0, 0, 0, 0);
        // Date todayTo = DateUtils.initDate(2017, 5, 13, 23, 59, 59, 999);

        Date todaySince = DateUtils.initDateHour(0, 0, 0, 0);
        Date todayTo = DateUtils.initDateHour(23, 59, 59, 999);

        String template = "";
        String subject = "Evaluación de Servicio";

        StringBuffer query = new StringBuffer();
        query.append(" createdDate between ?1 and ?2 ").append(" and userid is not null ")
                .append(" and idRecording not in ")
                .append(" (select ml.recording from NpsMailLog ml, NpsAnswer an where an.createdDate between ?3 and ?4 and an.year = ml.year and an.month = ml.month and an.day = ml.day and an.idRecording = ml.recording and an.userid = ml.userid )");

        List<NpsAnswer> evaluations = NpsAnswer.find(query.toString(), todaySince, todayTo, todaySince, todayTo)
                .fetch();

        List<NpsMailDto> mails = new ArrayList<>();

        for (NpsAnswer evaluation : evaluations) {

            if (evaluation.userid != null) {
            	
            	Usuario evaluatedUser = Usuario.findById(evaluation.userid);
                
            	if(DirectorEvaluatedJob.hasPermissions(evaluatedUser.roles)) {
                	                
	                Structure user = Structure.findById(evaluation.userid);
	
	                List<NpsQuestionAnswer> questionsAnswers = NpsQuestionAnswer
	                        .find(" idAnswer = ? order by idQuestion asc ", evaluation.id).fetch();
	
	                if (questionsAnswers != null && !questionsAnswers.isEmpty()) {
	                    int serviceScore = NumberConverterUtils.getValuefromText(questionsAnswers.get(0).textAnswer);
	                    if (serviceScore >= 0) {
	                        if (serviceScore >= 9 && serviceScore <= 10) { // Carta
	                                                                       // felicitación
	                            template = prepareSalesmanTemplate(cartaFelicitacion.bodyTemplete,
	                                    user.firstname + " " + user.lastname,
	                                    getDate(evaluation.year, evaluation.month, evaluation.day), "" + serviceScore,
	                                    questionsAnswers.get(2).textAnswer);
	
	                        } else if (serviceScore >= 7 && serviceScore <= 8) { // Carta
	                                                                             // de
	                                                                             // oportunidad
	                                                                             // de
	                                                                             // mejora
	
	                            template = prepareSalesmanTemplate(cartaOportunidadMejora.bodyTemplete,
	                                    user.firstname + " " + user.lastname,
	                                    getDate(evaluation.year, evaluation.month, evaluation.day), "" + serviceScore,
	                                    questionsAnswers.get(2).textAnswer);
	
	                        } else if (serviceScore >= 0 && serviceScore <= 6) { // Carta
	                                                                             // exhortación
	                            template = prepareSalesmanTemplate(cartaExhortacion.bodyTemplete,
	                                    user.firstname + " " + user.lastname,
	                                    getDate(evaluation.year, evaluation.month, evaluation.day), "" + serviceScore,
	                                    questionsAnswers.get(2).textAnswer);
	
	                        }
	
	                        NpsMailLog mailLog = new NpsMailLog();
	                        mailLog.day = evaluation.day;
	                        mailLog.month = evaluation.month;
	                        mailLog.recording = evaluation.idRecording;
	                        mailLog.sentMail = 1;
	                        mailLog.userid = evaluation.userid;
	                        mailLog.year = evaluation.year;
	
	                        NpsMailDto mailDto = new NpsMailDto(mailLog, template, subject,
	                                evaluation.surveyOriginId.intValue(), user.email);
	                        mails.add(mailDto);
	
	                    }
	                }
                }
            }
        }

        new NpsMailSender(mails).run();

    }

    private Date getDate(Long year, Long month, Long day) {

        Calendar date = Calendar.getInstance();
        date.set(Calendar.YEAR, year.intValue());
        date.set(Calendar.MONTH, month.intValue() - 1);
        date.set(Calendar.DAY_OF_MONTH, day.intValue());

        return date.getTime();

    }

    private String prepareSalesmanTemplate(String template, String name, Date surveyDate, String answer,
            String comment) {
        HashMap<String, Object> vars = new HashMap();

        SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");

        String baseUrl = Play.configuration.getProperty("application.baseUrl");

        vars.put("%employee.name%", name);
        vars.put("%survey.date%", sdf.format(surveyDate));
        vars.put("%survey.answer%", answer);
        vars.put("%survey.comment%", comment);
        vars.put("%baseUrl%", baseUrl);

        if (template != null) {
            for (String key : vars.keySet()) {

                template = template.replace(key, vars.get(key) != null ? vars.get(key).toString() : "");
            }
        }

        return template;
    }

}
