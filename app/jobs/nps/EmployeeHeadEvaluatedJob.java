package jobs.nps;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import models.NPSTemplate;
import models.NpsAnswer;
import models.Rol;
import models.Structure;
import models.Usuario;
import models.enume.NpsTemplateTypeEnum;
import play.Play;
import play.jobs.Job;
import play.jobs.On;
import utils.DateUtils;
import utils.NpsMailUtils;
import utils.NumberConverterUtils;

@On("0 0 5 * * ?")
public class EmployeeHeadEvaluatedJob extends Job {

    private static String ROW = "<tr style='height: 25px; %row.style%'>"
            + "<td style='border-spacing:0px; text-align: left; font-size: 11px;' width='250'>%adviser.name%</td> "
            + "<td style='border-spacing:0px; text-align: center; font-size: 11px;' width='117'>%evaluations%</td> "
            + "<td style='border-spacing:0px; text-align: center; font-size: 11px; color:%service.row.color%;' width='116'>%nps.service%</td> "
            + "<td style='border-spacing:0px; text-align: center; font-size: 11px; color:%recomendation.row.color%;' width='117'>%nps.recomendation%</td> </tr>";

    public void doJob() {

        StringBuffer query = new StringBuffer();
        NPSTemplate cartaJefe = NPSTemplate.find("idType = ?", NpsTemplateTypeEnum.CARTA_JEFE.getId()).first();

        // TODO solo para pruebas
        //Date todaySince = DateUtils.initDate(2017, 5, 13, 0, 0, 0, 0);
        //Date todayTo = DateUtils.initDate(2017, 5, 13, 23, 59, 59, 999);

        Date todayTo = DateUtils.initDateHour(23, 59, 59, 999);
        
        Calendar today = Calendar.getInstance();
        today.setTime(todayTo);
        
        Date todaySince = DateUtils.initDate(today.get(Calendar.YEAR), today.get(Calendar.MONTH), today.get(Calendar.DAY_OF_MONTH) -1, 0, 0, 0, 0);

        query.append("select distinct userIdChief from NpsAnswer ")
        .append(" where createdDate between ?1 and ?2 ");
        

        List<Long> idsUserChief = NpsAnswer.find(query.toString(), todaySince, todayTo).fetch();
         
        StringBuffer queryEvaluation = new StringBuffer();
        queryEvaluation.append(" userIdChief = ?1 and year = ?2 and month = ?3 and day between ?4 and ?5  ")        			   
        			   .append(" order by userid");
        for (Long idUser : idsUserChief) {
        	
        	Usuario evaluatedUser = Usuario.findById(idUser);
        	
        	if(DirectorEvaluatedJob.hasPermissions(evaluatedUser != null ? evaluatedUser.roles : new ArrayList<Rol>())) {

	            Map<Long, List<NpsAnswer>> userEvaluations = new HashMap<>();
	            
	            List<NpsAnswer> evaluations = NpsAnswer
	                    .find(queryEvaluation.toString(),
	                            idUser, 
	                            Long.valueOf(today.get(Calendar.YEAR)), 
	                            Long.valueOf(today.get(Calendar.MONTH) + 1),
	                            1L, 
	                            Long.valueOf(today.get(Calendar.DAY_OF_MONTH))	                           
	                            )
	                    .fetch();
	            
	            int totalServicePromoter = 0;
	            int totalServiceDetractor = 0;
	            int totalRecomendationPromoter = 0;
	            int totalRecomendationDetractor = 0;
	            int totalCountEvaluations = 0;
	            int row = 0;
	
	            StringBuffer rows = new StringBuffer();
	
	            for (NpsAnswer evaluation : evaluations) {
	                List<NpsAnswer> evaluationsOfUser = userEvaluations.get(evaluation.userid);
	
	                if (evaluationsOfUser == null) {
	                    evaluationsOfUser = new ArrayList<>();
	                }
	
	                evaluationsOfUser.add(evaluation);
	                userEvaluations.put(evaluation.userid, evaluationsOfUser);
	            }
	
	            for (Long user : userEvaluations.keySet()) {
	                Structure usuario = Structure.findById(user);
	                List<NpsAnswer> totalEvaluations = userEvaluations.get(user);
	                int servicePromoter = 0;
	                int serviceDetractor = 0;
	                int recomendationPromoter = 0;
	                int recomendationDetractor = 0;
	                int total = totalEvaluations.size();
	
	                for (NpsAnswer npsAnswer : totalEvaluations) {
	                    int serviceAnswer = NumberConverterUtils
	                            .getValuefromText(npsAnswer.questionsAnswers.get(0).textAnswer);
	                    int recomendationAnswer = NumberConverterUtils
	                            .getValuefromText(npsAnswer.questionsAnswers.get(1).textAnswer);
	                    if (serviceAnswer >= 9 && serviceAnswer <= 10) {
	                        servicePromoter++;
	                    } else if (serviceAnswer >= 0 && serviceAnswer <= 6) {
	                        serviceDetractor++;
	                    }
	
	                    if (recomendationAnswer >= 9 && recomendationAnswer <= 10) {
	                        recomendationPromoter++;
	                    } else if (recomendationAnswer >= 0 && recomendationAnswer <= 6) {
	                        recomendationDetractor++;
	                    }
	
	                }
	                totalServicePromoter = totalServicePromoter + servicePromoter;
	                totalServiceDetractor = totalServiceDetractor + serviceDetractor;
	                totalRecomendationPromoter = totalRecomendationPromoter + recomendationPromoter;
	                totalRecomendationDetractor = totalRecomendationDetractor + recomendationDetractor;
	                totalCountEvaluations = totalCountEvaluations + total;
	
	                BigDecimal npsService = new BigDecimal(servicePromoter - serviceDetractor);
	                BigDecimal npsRecomendation = new BigDecimal(recomendationPromoter - recomendationDetractor);
	                npsService = npsService.divide(new BigDecimal(total), 2, RoundingMode.HALF_UP)
	                        .multiply(new BigDecimal(100));
	                npsRecomendation = npsRecomendation.divide(new BigDecimal(total), 2, RoundingMode.HALF_UP)
	                        .multiply(new BigDecimal(100));
	
	                rows.append(addRow(ROW, usuario.lastname + " " + usuario.firstname, totalEvaluations.size(),
	                        npsService, npsRecomendation, row++));
	            }
	
	            if (!userEvaluations.keySet().isEmpty()) {
	                BigDecimal totalNpsService = new BigDecimal(totalServicePromoter - totalServiceDetractor);
	                BigDecimal totalNpsRecomendation = new BigDecimal(
	                        totalRecomendationPromoter - totalRecomendationDetractor);
	                totalNpsService = totalNpsService.divide(new BigDecimal(totalCountEvaluations), 2, RoundingMode.HALF_UP)
	                        .multiply(new BigDecimal(100));
	                totalNpsRecomendation = totalNpsRecomendation
	                        .divide(new BigDecimal(totalCountEvaluations), 2, RoundingMode.HALF_UP)
	                        .multiply(new BigDecimal(100));
	
	                Structure chiefUser = Structure.findById(idUser);
	
	                String tableFooter = addRow(ROW, chiefUser.lastname + " " + chiefUser.firstname, totalCountEvaluations,
	                        totalNpsService, totalNpsRecomendation, 2);
	                String bodyMail = prepareTemplate(cartaJefe.bodyTemplete, chiefUser.firstname + " " + chiefUser.lastname,
	                        rows.toString(), tableFooter);
	
	                NpsMailUtils.sendMail(bodyMail, chiefUser.email);
	                
	            }
        	}
        }
    }

    private static String addRow(String template, String name, int evaluations, BigDecimal npsService, BigDecimal npsRecomendation,
            int row) {

        HashMap<String, Object> vars = new HashMap();

        String serviceColor = getColorByScore(npsService.intValue());
        String recomendationColor = getColorByScore(npsRecomendation.intValue());

        vars.put("%adviser.name%", name);
        vars.put("%evaluations%", evaluations);
        vars.put("%nps.service%", npsService.setScale(2, RoundingMode.HALF_UP));
        vars.put("%nps.recomendation%", npsRecomendation.setScale(2, RoundingMode.HALF_UP));
        vars.put("%row.style%", (row % 2) == 0 ? "" : "background-color:#E8E8E8;");
        vars.put("%service.row.color%", serviceColor);
        vars.put("%recomendation.row.color%", recomendationColor);

        if (template != null) {
            for (String key : vars.keySet()) {

                template = template.replace(key, vars.get(key) != null ? vars.get(key).toString() : "");
            }
        }

        return template;
    }

    private static String prepareTemplate(String template, String name, String body, String footer) {

        HashMap<String, Object> vars = new HashMap();

        String baseUrl = Play.configuration.getProperty("application.baseUrl");
        String evaluationsUrl = Play.configuration.getProperty("application.evaluationsUrl");

        vars.put("%employee.name%", name);
        vars.put("%table.body%", body);
        vars.put("%table.footer%", footer);
        vars.put("%baseUrl%", baseUrl);
        vars.put("%evaluation.detail%", evaluationsUrl);
        

        if (template != null) {
            for (String key : vars.keySet()) {

                template = template.replace(key, vars.get(key) != null ? vars.get(key).toString() : "");
            }
        }

        return template;
    }

    private static String getColorByScore(int score) {
        String color = "";
        if (score <= 60) {
            color = "rgb(255, 0, 0)";
        } else if (score > 60 && score <= 80) {
            color = "rgb(255, 191, 0)";
        } else if (score > 80 && score <= 100) {
            color = "rgb(95, 180, 4)";
        }

        return color;
    }

}
