package jobs.nps;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import json.nps.dto.EvaluationDto;
import models.NPSTemplate;
import models.NpsAnswer;
import models.Structure;
import models.Usuario;
import models.enume.NpsTemplateTypeEnum;
import play.jobs.Job;
import play.jobs.On;
import utils.NpsMailUtils;

@On("0 0 7 * * ?")
public class OperationsEvaluatedJob extends Job {

    public void doJob() {

        StringBuffer query = new StringBuffer();

        query.append("select distinct s.manager from NpsAnswer a, Structure s ")
                .append(" where a.year = ?1 and a.month = ?2 and s.userid = a.userIdZone  ");

        // Date todayTo = DateUtils.initDate(2017, 5, 1, 23, 59, 59, 999);
        // //TODO solo para pruebas

        Calendar today = Calendar.getInstance();
        // today.setTime(todayTo); //TODO solo para pruebas

        if (today.get(Calendar.DAY_OF_MONTH) == 1) { // Si es primero de mes,
                                                     // entonces se obtiene el
                                                     // mes anterior
            if (today.get(Calendar.MONTH) == 0) {
                today.set(Calendar.YEAR, today.get(Calendar.YEAR) - 1);
                today.set(Calendar.MONTH, 11);
                today.set(Calendar.DAY_OF_MONTH, 31);
            } else {
                today.set(Calendar.MONTH, today.get(Calendar.MONTH) - 1);
                today.set(Calendar.DAY_OF_MONTH, today.getActualMaximum(Calendar.DAY_OF_MONTH));
            }
        }

        boolean isLastDayOfMonth = today.get(Calendar.DAY_OF_MONTH) == today.getActualMaximum(Calendar.DAY_OF_MONTH);

        // isLastDayOfMonth = true; //TODO solo para pruebas

        if (isLastDayOfMonth) {
            NPSTemplate cartaGenerica = NPSTemplate.find("idType = ?", NpsTemplateTypeEnum.CARTA_GENERICA.getId())
                    .first();
            List<Integer> idsUsersOperations = NpsAnswer.find(query.toString(), Long.valueOf(today.get(Calendar.YEAR)),
                    Long.valueOf(today.get(Calendar.MONTH) + 1)).fetch();

            for (Integer idUserOperations : idsUsersOperations) {
            	
            	Usuario evaluatedUser = Usuario.findById(idUserOperations);
                
            	if(DirectorEvaluatedJob.hasPermissions(evaluatedUser.roles)) {
            		
            		int idRow = 0;
                    StringBuffer htmlRows = new StringBuffer();
                    Map<String, Integer> totals = new HashMap<>();
                    String zone = "";

                    Map<Long, List<NpsAnswer>> userZoneBoss = new HashMap<>();
                    List<NpsAnswer> evaluations = NpsAnswer
                            .find(" select a from NpsAnswer a, Structure s where s.manager = ?1 and a.userIdZone = s.userid and a.year = ?2 and a.month = ?3 and a.day between ?4 and ?5 order by a.userIdZone",
                                    idUserOperations, Long.valueOf(today.get(Calendar.YEAR)),
                                    Long.valueOf(today.get(Calendar.MONTH) + 1), 1L,
                                    Long.valueOf(today.get(Calendar.DAY_OF_MONTH)))
                            .fetch();

                    for (NpsAnswer evaluation : evaluations) {
                        if (zone == null || zone.isEmpty())
                            zone = evaluation.zone;

                        List<NpsAnswer> evaluations4ZoneBoss = userZoneBoss.get(evaluation.userIdZone);

                        if (evaluations4ZoneBoss == null) {
                            evaluations4ZoneBoss = new ArrayList<>();
                        }
                        evaluations4ZoneBoss.add(evaluation);
                        userZoneBoss.put(evaluation.userIdZone, evaluations4ZoneBoss);
                    }

                    for (Long idUserZoneBoss : userZoneBoss.keySet()) {
                        List<Long> complaints = NpsAnswer
                                .find("select distinct a.id from NpsAnswer a, NpsQuestionAnswer qa, QuestionChoices qc where a.userIdZone = ?1  and a.year = ?2 and a.month = ?3 and a.day between ?4 and ?5  and qa.idAnswer = a.id and qc.id = qa.idChoiceAnswer and qc.choiceNumericValue is not null and qc.choiceNumericValue <= 8",
                                        idUserZoneBoss, Long.valueOf(today.get(Calendar.YEAR)),
                                        Long.valueOf(today.get(Calendar.MONTH) + 1), 1L,
                                        Long.valueOf(today.get(Calendar.DAY_OF_MONTH)))
                                .fetch();
                        EvaluationDto evaluationRow = NpsMailUtils.addRow(idUserZoneBoss, userZoneBoss.get(idUserZoneBoss),
                                complaints.size());
                        totals = NpsMailUtils.addTotals(totals, evaluationRow.getTotalEvaluations(), "total");
                        totals = NpsMailUtils.addTotals(totals, evaluationRow.getTotalComplaints(), "complaints");
                        totals = NpsMailUtils.addTotals(totals, evaluationRow.getTotalOpen(), "open");
                        totals = NpsMailUtils.addTotals(totals, evaluationRow.getTotalTracing(), "tracing");
                        totals = NpsMailUtils.addTotals(totals, evaluationRow.getTotalSolved(), "solved");

                        htmlRows.append(NpsMailUtils.prepareHtmlRow(NpsMailUtils.ROW, evaluationRow.getFullName(),
                                evaluationRow.getTotalEvaluations(), evaluationRow.getTotalComplaints(),
                                evaluationRow.getTotalOpen(), evaluationRow.getTotalTracing(),
                                evaluationRow.getTotalSolved(), idRow++));
                    }

                    if (!userZoneBoss.keySet().isEmpty()) {
                        Structure operationsUser = Structure.findById(Long.valueOf(idUserOperations.intValue()));

                        String resumeRow = NpsMailUtils.prepareHtmlRow(NpsMailUtils.ROW, "Almacenes",
                                totals.get("total") != null ? totals.get("total") : 0,
                                totals.get("complaints") != null ? totals.get("complaints") : 0,
                                totals.get("open") != null ? totals.get("open") : 0,
                                totals.get("tracing") != null ? totals.get("tracing") : 0,
                                totals.get("solved") != null ? totals.get("solved") : 0, 0);

                        String bodyMail = NpsMailUtils.prepareEmail(cartaGenerica.bodyTemplete,
                                operationsUser.firstname + " " + operationsUser.lastname, htmlRows.toString(), resumeRow);
                        NpsMailUtils.sendMail(bodyMail, operationsUser.email);
                    }
            		
            	}
            }
        }
    }

}
