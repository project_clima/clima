package jobs.nps;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import json.nps.dto.EvaluationDto;
import models.NPSTemplate;
import models.NpsAnswer;
import models.Structure;
import models.Usuario;
import models.enume.NpsTemplateTypeEnum;
import play.jobs.Job;
import play.jobs.On;
import utils.NpsMailUtils;

@On("0 0 7 * * ?")
public class ZoneBossEvaluatedJob extends Job {

    public void doJob() {

        StringBuffer query = new StringBuffer();

        query.append("select distinct userIdZone from NpsAnswer ")
                .append(" where year = ?1 and month = ?2 order by userIdZone asc ");

        // Date todayTo = DateUtils.initDate(2017, 5, 1, 23, 59, 59, 999);
        // //TODO solo para pruebas

        Calendar today = Calendar.getInstance();
        // today.setTime(todayTo); //TODO solo para pruebas

        if (today.get(Calendar.DAY_OF_MONTH) == 1) { // Si es primero de mes,
                                                     // entonces se obtiene el
                                                     // mes anterior
            if (today.get(Calendar.MONTH) == 0) {
                today.set(Calendar.YEAR, today.get(Calendar.YEAR) - 1);
                today.set(Calendar.MONTH, 11);
                today.set(Calendar.DAY_OF_MONTH, 31);
            } else {
                today.set(Calendar.MONTH, today.get(Calendar.MONTH) - 1);
                today.set(Calendar.DAY_OF_MONTH, today.getActualMaximum(Calendar.DAY_OF_MONTH));
            }
        }

        boolean isLastDayOfMonth = today.get(Calendar.DAY_OF_MONTH) == today.getActualMaximum(Calendar.DAY_OF_MONTH);

        // isLastDayOfMonth = true; //TODO solo para pruebas

        if (isLastDayOfMonth) {
            NPSTemplate cartaGenerica = NPSTemplate.find("idType = ?", NpsTemplateTypeEnum.CARTA_GENERICA.getId())
                    .first();
            List<Long> idsUserZoneBoss = NpsAnswer.find(query.toString(), Long.valueOf(today.get(Calendar.YEAR)),
                    Long.valueOf(today.get(Calendar.MONTH) + 1)).fetch();

            for (Long idZoneBoss : idsUserZoneBoss) {
            	
            	Usuario evaluatedUser = Usuario.findById(idZoneBoss);
                
            	if(DirectorEvaluatedJob.hasPermissions(evaluatedUser.roles)) {
            		
            		int idRow = 0;
                    StringBuffer htmlRows = new StringBuffer();
                    Map<String, Integer> totals = new HashMap<>();
                    String zone = "";

                    Map<Long, List<NpsAnswer>> userDirectors = new HashMap<>();
                    List<NpsAnswer> evaluations = NpsAnswer.find(
                            " userIdZone = ?1 and year = ?2 and month = ?3 and day between ?4 and ?5 order by userIdDirector",
                            idZoneBoss, Long.valueOf(today.get(Calendar.YEAR)), Long.valueOf(today.get(Calendar.MONTH) + 1),
                            1L, Long.valueOf(today.get(Calendar.DAY_OF_MONTH))).fetch();

                    for (NpsAnswer evaluation : evaluations) {
                        if (zone == null || zone.isEmpty())
                            zone = evaluation.zone;

                        List<NpsAnswer> evaluations4Director = userDirectors.get(evaluation.userIdDirector);

                        if (evaluations4Director == null) {
                            evaluations4Director = new ArrayList<>();
                        }
                        evaluations4Director.add(evaluation);
                        userDirectors.put(evaluation.userIdDirector, evaluations4Director);
                    }

                    for (Long idUserDirector : userDirectors.keySet()) {
                        List<Long> complaints = NpsAnswer
                                .find("select distinct a.id from NpsAnswer a, NpsQuestionAnswer qa, QuestionChoices qc where a.userIdDirector = ?1  and a.year = ?2 and a.month = ?3 and a.day between ?4 and ?5  and qa.idAnswer = a.id and qc.id = qa.idChoiceAnswer and qc.choiceNumericValue is not null and qc.choiceNumericValue <= 8 and a.userIdZone = ?6",
                                        idUserDirector, Long.valueOf(today.get(Calendar.YEAR)),
                                        Long.valueOf(today.get(Calendar.MONTH) + 1), 1L,
                                        Long.valueOf(today.get(Calendar.DAY_OF_MONTH)), idZoneBoss)
                                .fetch();
                        EvaluationDto evaluationRow = NpsMailUtils.addRow(idUserDirector, userDirectors.get(idUserDirector),
                                complaints.size());
                        totals = NpsMailUtils.addTotals(totals, evaluationRow.getTotalEvaluations(), "total");
                        totals = NpsMailUtils.addTotals(totals, evaluationRow.getTotalComplaints(), "complaints");
                        totals = NpsMailUtils.addTotals(totals, evaluationRow.getTotalOpen(), "open");
                        totals = NpsMailUtils.addTotals(totals, evaluationRow.getTotalTracing(), "tracing");
                        totals = NpsMailUtils.addTotals(totals, evaluationRow.getTotalSolved(), "solved");

                        htmlRows.append(NpsMailUtils.prepareHtmlRow(NpsMailUtils.ROW, evaluationRow.getFullName(),
                                evaluationRow.getTotalEvaluations(), evaluationRow.getTotalComplaints(),
                                evaluationRow.getTotalOpen(), evaluationRow.getTotalTracing(),
                                evaluationRow.getTotalSolved(), idRow++));
                    }

                    if (!userDirectors.keySet().isEmpty()) {
                        Structure zoneBossUser = Structure.findById(idZoneBoss);

                        String resumeRow = NpsMailUtils.prepareHtmlRow(NpsMailUtils.ROW, "Zona " + zone,
                                totals.get("total") != null ? totals.get("total") : 0,
                                totals.get("complaints") != null ? totals.get("complaints") : 0,
                                totals.get("open") != null ? totals.get("open") : 0,
                                totals.get("tracing") != null ? totals.get("tracing") : 0,
                                totals.get("solved") != null ? totals.get("solved") : 0, 0);

                        String fullName = "-- --";
                        if (zoneBossUser != null)
                            fullName = zoneBossUser.firstname + " " + zoneBossUser.lastname;

                        String bodyMail = NpsMailUtils.prepareEmail(cartaGenerica.bodyTemplete, fullName,
                                htmlRows.toString(), resumeRow);
                        NpsMailUtils.sendMail(bodyMail, zoneBossUser != null ? zoneBossUser.email : "");

                    }
            	}

            }
        }
    }

}
