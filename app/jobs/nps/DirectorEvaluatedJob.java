package jobs.nps;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import controllers.UserAdminNPS;
import json.nps.dto.EvaluationDto;
import models.Accion;
import models.NPSTemplate;
import models.NpsAnswer;
import models.Rol;
import models.Structure;
import models.Usuario;
import models.enume.NpsTemplateTypeEnum;
import play.jobs.Job;
import play.jobs.On;
import utils.NpsMailUtils;

@On("0 0 7 * * ?")
public class DirectorEvaluatedJob extends Job {

    public void doJob() {
        StringBuffer query = new StringBuffer();

        query.append("select distinct userIdDirector from NpsAnswer ").append(" where year = ?1 and month = ?2");

        // Date todayTo = DateUtils.initDate(2017, 5, 20, 23, 59, 59, 999);
        // //TODO solo para pruebas

        Calendar today = Calendar.getInstance();
        // today.setTime(todayTo); //TODO solo para pruebas

        if (today.get(Calendar.DAY_OF_MONTH) == 1) { // Si es primero de mes,
                                                     // mes anterior
            if (today.get(Calendar.MONTH) == 0) {
                today.set(Calendar.YEAR, today.get(Calendar.YEAR) - 1);
                today.set(Calendar.MONTH, 11);
                today.set(Calendar.DAY_OF_MONTH, 31);
            } else {
                today.set(Calendar.MONTH, today.get(Calendar.MONTH) - 1);
                today.set(Calendar.DAY_OF_MONTH, today.getActualMaximum(Calendar.DAY_OF_MONTH));
            }
        }

        boolean isMonday = today.get(Calendar.DAY_OF_WEEK) == Calendar.MONDAY;
        // isMonday = true; //TODO Solo para pruebas

        if (isMonday) {
            NPSTemplate cartaGenerica = NPSTemplate.find("idType = ?", NpsTemplateTypeEnum.CARTA_GENERICA.getId())
                    .first();
            List<Long> idsUserDirector = NpsAnswer.find(query.toString(), Long.valueOf(today.get(Calendar.YEAR)),
                    Long.valueOf(today.get(Calendar.MONTH) + 1)).fetch();

            for (Long idDirector : idsUserDirector) {
            	
                int idRow = 0;
                StringBuffer htmlRows = new StringBuffer();
                Map<String, Integer> managerTotals = new HashMap<>();

                Map<Long, List<NpsAnswer>> userManagers = new HashMap<>();
                
                Usuario userDirector = Usuario.findById(idDirector);
                if(hasPermissions(userDirector.roles)) {
	                List<NpsAnswer> evaluations = NpsAnswer.find(
	                        " userIdDirector = ?1 and year = ?2 and month = ?3 and day between ?4 and ?5 order by userIdManager",
	                        idDirector, Long.valueOf(today.get(Calendar.YEAR)), Long.valueOf(today.get(Calendar.MONTH) + 1),
	                        1L, Long.valueOf(today.get(Calendar.DAY_OF_MONTH))).fetch();
	
	                for (NpsAnswer evaluation : evaluations) {
	
	                    List<NpsAnswer> evaluations4Manager = userManagers.get(evaluation.userIdManager);
	                    if (evaluations4Manager == null) {
	                        evaluations4Manager = new ArrayList<>();
	                    }
	                    evaluations4Manager.add(evaluation);
	                    userManagers.put(evaluation.userIdManager, evaluations4Manager);
	                }
	
	                for (Long idUserManager : userManagers.keySet()) {
	
	                    List<Long> complaints = NpsAnswer
	                            .find("select distinct a.id from NpsAnswer a, NpsQuestionAnswer qa, QuestionChoices qc where a.userIdManager = ?1   and a.year = ?2 and a.month = ?3 and a.day between ?4 and ?5  and qa.idAnswer = a.id and qc.id = qa.idChoiceAnswer and qc.choiceNumericValue is not null and qc.choiceNumericValue <= 8 and a.userIdDirector = ?6",
	                                    idUserManager, Long.valueOf(today.get(Calendar.YEAR)),
	                                    Long.valueOf(today.get(Calendar.MONTH) + 1), 1L,
	                                    Long.valueOf(today.get(Calendar.DAY_OF_MONTH)), idDirector)
	                            .fetch();
	                    EvaluationDto evaluationRow = NpsMailUtils.addRow(idUserManager, userManagers.get(idUserManager),
	                            complaints.size());
	                    managerTotals = NpsMailUtils.addTotals(managerTotals, evaluationRow.getTotalEvaluations(), "total");
	                    managerTotals = NpsMailUtils.addTotals(managerTotals, evaluationRow.getTotalComplaints(),
	                            "complaints");
	                    managerTotals = NpsMailUtils.addTotals(managerTotals, evaluationRow.getTotalOpen(), "open");
	                    managerTotals = NpsMailUtils.addTotals(managerTotals, evaluationRow.getTotalTracing(), "tracing");
	                    managerTotals = NpsMailUtils.addTotals(managerTotals, evaluationRow.getTotalSolved(), "solved");
	
	                    htmlRows.append(NpsMailUtils.prepareHtmlRow(NpsMailUtils.ROW, evaluationRow.getFullName(),
	                            evaluationRow.getTotalEvaluations(), evaluationRow.getTotalComplaints(),
	                            evaluationRow.getTotalOpen(), evaluationRow.getTotalTracing(),
	                            evaluationRow.getTotalSolved(), idRow++));
	                }
	
	                if (!userManagers.keySet().isEmpty()) {
	                    Structure userManager = Structure.findById(idDirector);
	
	                    String managerRow = NpsMailUtils.prepareHtmlRow(NpsMailUtils.ROW,
	                            userManager.lastname + " " + userManager.firstname,
	                            managerTotals.get("total") != null ? managerTotals.get("total") : 0,
	                            managerTotals.get("complaints") != null ? managerTotals.get("complaints") : 0,
	                            managerTotals.get("open") != null ? managerTotals.get("open") : 0,
	                            managerTotals.get("tracing") != null ? managerTotals.get("tracing") : 0,
	                            managerTotals.get("solved") != null ? managerTotals.get("solved") : 0, 0);
	
	                    String bodyMail = NpsMailUtils.prepareEmail(cartaGenerica.bodyTemplete,
	                            userManager.firstname + " " + userManager.lastname, htmlRows.toString(), managerRow);
	                    NpsMailUtils.sendMail(bodyMail, userManager.email);
	                }
                }
            }
        }
    }
    
    public static boolean hasPermissions(List<Rol> roles) {
    	if(roles != null ) {
	    	for (Rol rol : roles) {
				for (Accion accion : rol.acciones) {
					if(accion.id == UserAdminNPS.ENVIAR_CORREO){
						return true;
					}
				}
			}
    	}
    	return false;
    }

}
