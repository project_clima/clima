package jobs.nps;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import json.nps.dto.EvaluationDto;
import models.NPSTemplate;
import models.NpsAnswer;
import models.Rol;
import models.Structure;
import models.Usuario;
import models.enume.NpsTemplateTypeEnum;
import play.jobs.Job;
import play.jobs.On;
import utils.DateUtils;
import utils.NpsMailUtils;

@On("0 0 6 * * ?")
public class ManagerEvaluatedJob extends Job {

    public void doJob() {

        StringBuffer query = new StringBuffer();

        // TODO solo para pruebas
        // Date todaySince = DateUtils.initDate(2017, 5, 13, 0, 0, 0, 0);
        // Date todayTo = DateUtils.initDate(2017, 5, 13, 23, 59, 59, 999);

        
        Date todayTo = DateUtils.initDateHour(23, 59, 59, 999);

        Calendar today = Calendar.getInstance();
        today.setTime(todayTo);
         
        Date todaySince = DateUtils.initDate(today.get(Calendar.YEAR), today.get(Calendar.MONTH), today.get(Calendar.DAY_OF_MONTH) -1, 0, 0, 0, 0);

        NPSTemplate cartaGenerica = NPSTemplate.find("idType = ?", NpsTemplateTypeEnum.CARTA_GENERICA.getId()).first();

        query.append("select distinct userIdManager from NpsAnswer ").append(" where createdDate between ?1 and ?2");

        List<Long> idsUserManager = NpsAnswer.find(query.toString(), todaySince, todayTo).fetch();

        StringBuffer queryEvaluation = new StringBuffer("  userIdManager = ?1 and year = ?2 and month = ?3 and day between ?4 and ?5")		   
		   .append(" order by userIdChief ");
        
        for (Long idManager : idsUserManager) {
        	
        	Usuario evaluatedUser = Usuario.findById(idManager);
            
        	
        	if(DirectorEvaluatedJob.hasPermissions(evaluatedUser != null ? evaluatedUser.roles : new ArrayList<Rol>())) {
        		
        		int idRow = 0;
                StringBuffer htmlRows = new StringBuffer();
                Map<String, Integer> managerTotals = new HashMap<>();

                Map<Long, List<NpsAnswer>> userChiefs = new HashMap<>();
                List<NpsAnswer> evaluations = NpsAnswer.find(queryEvaluation.toString(),
                        idManager, 
                        Long.valueOf(today.get(Calendar.YEAR)), 
                        Long.valueOf(today.get(Calendar.MONTH) + 1), 
                        1L,
                        Long.valueOf(today.get(Calendar.DAY_OF_MONTH))                       
                        ).fetch();

                for (NpsAnswer evaluation : evaluations) {
                    List<NpsAnswer> evaluations4Chief = userChiefs.get(evaluation.userIdChief);

                    if (evaluations4Chief == null) {
                        evaluations4Chief = new ArrayList<>();
                    }
                    evaluations4Chief.add(evaluation);
                    userChiefs.put(evaluation.userIdChief, evaluations4Chief);
                }

                for (Long idUserChief : userChiefs.keySet()) {
                    EvaluationDto evaluationRow = NpsMailUtils.addRow(idUserChief, userChiefs.get(idUserChief));
                    managerTotals = NpsMailUtils.addTotals(managerTotals, evaluationRow.getTotalEvaluations(), "total");
                    managerTotals = NpsMailUtils.addTotals(managerTotals, evaluationRow.getTotalComplaints(), "complaints");
                    managerTotals = NpsMailUtils.addTotals(managerTotals, evaluationRow.getTotalOpen(), "open");
                    managerTotals = NpsMailUtils.addTotals(managerTotals, evaluationRow.getTotalTracing(), "tracing");
                    managerTotals = NpsMailUtils.addTotals(managerTotals, evaluationRow.getTotalSolved(), "solved");

                    htmlRows.append(NpsMailUtils.prepareHtmlRow(NpsMailUtils.ROW, evaluationRow.getFullName(),
                            evaluationRow.getTotalEvaluations(), evaluationRow.getTotalComplaints(),
                            evaluationRow.getTotalOpen(), evaluationRow.getTotalTracing(), evaluationRow.getTotalSolved(),
                            idRow++));
                }

                if (!userChiefs.keySet().isEmpty()) {
                    Structure userManager = Structure.findById(idManager);

                    String managerRow = NpsMailUtils.prepareHtmlRow(NpsMailUtils.ROW,
                            userManager.lastname + " " + userManager.firstname,
                            managerTotals.get("total") != null ? managerTotals.get("total") : 0,
                            managerTotals.get("complaints") != null ? managerTotals.get("complaints") : 0,
                            managerTotals.get("open") != null ? managerTotals.get("open") : 0,
                            managerTotals.get("tracing") != null ? managerTotals.get("tracing") : 0,
                            managerTotals.get("solved") != null ? managerTotals.get("solved") : 0, 0);

                    String bodyMail = NpsMailUtils.prepareEmail(cartaGenerica.bodyTemplete,
                            userManager.firstname + " " + userManager.lastname, htmlRows.toString(), managerRow);

                    NpsMailUtils.sendMail(bodyMail, userManager.email);
                }
        		
        	}

        }
    }

}
