package jobs.csi;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;

import concurrent.csi.SaveThread;
import models.csi.CsiEvaluationCross;
import models.csi.CsiEvaluationCrossLog;
import play.Logger;
import play.jobs.Job;
import utils.MailUtils;

public class CrossEvaluationAsyncCSIJob extends Job {

    private List<CsiEvaluationCross> cross;
    private List<CsiEvaluationCrossLog> logs;
    private Integer idSurvey;
    private String email;

    public CrossEvaluationAsyncCSIJob(List<CsiEvaluationCrossLog> logs, List<CsiEvaluationCross> cross,
            Integer idSurvey, String email) {
        this.logs = logs;
        this.cross = cross;
        this.idSurvey = idSurvey;
        this.email = email;
    }

    public void doJob() {
        saveEvaluationCross();
    }

    /**
     * Método para guardar el cruce de evaluaciones y mandar el email con los detalles
     */
    private void saveEvaluationCross() {

        try {
            if(cross.size() > 0) {
                saveMultiThread(cross.size(), cross); // Save the cross evaluation
            }else if(logs.size() > 0) {
                saveMultiThread(logs.size(), logs); // Save event logs for cross
                                                // evaluation
            }

            String guardados = "\n Cruces cargados: " + cross.size();
            String guardadosLogs = "\n Cruces no cargados: " + logs.size();

            MailUtils.sendMail("Ejecución cruces de encuesta " + idSurvey,
                    "El procesamiento de cruces para la encuesta  " + idSurvey + " fue exitoso" + guardados
                            + guardadosLogs,
                    idSurvey, email);
        } catch (Exception e) {
            Logger.error("Error saving Evaluation Cross ", e.fillInStackTrace());
            e.printStackTrace();
            MailUtils.sendMail("Ejecución cruces de encuesta " + idSurvey,
                    "Ocurrió el siguiente error al guardar los cruces " + e.getLocalizedMessage(), idSurvey, email);
        }

    }

    /**
     * Método para guardar en segundo plano
     * @param listSize
     * @param objects
     * @throws ExecutionException
     * @throws InterruptedException
     */
    private <T> void saveMultiThread(int listSize, List<T> objects) throws ExecutionException, InterruptedException {
        int chunkSize = 3;
        int iterations = 0;
       
        iterations = listSize / chunkSize;
        if (listSize % chunkSize > 0) {
            iterations++;
        }
        
        int position = 0;

        ExecutorService service = Executors.newFixedThreadPool(iterations);
        List<Future<Runnable>> futures = new ArrayList<Future<Runnable>>();

        for (int i = 0; i < iterations; i++) {

            List<T> evaluations2Save = new ArrayList<>();
            for (int j = 0; j < chunkSize; j++) {
                if (position < listSize)
                    evaluations2Save.add(objects.get(position++));
            }

            Future f = service.submit(new SaveThread(evaluations2Save));
            futures.add(f);
        }

        for (Future<Runnable> f : futures) {
            //f.get();
             Logger.info("::"+f.get()+"::"+f.isDone());
        }

        service.shutdownNow();
    }
}
