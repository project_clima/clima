package jobs.csi;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.logging.Level;

import com.ibm.as400.util.commtrace.Format;
import com.jcraft.jsch.JSchException;
import com.jcraft.jsch.SftpException;

import SchedulerTask.nps.LoadNPSStructureJob;
import controllers.csi.SnapStructure;
import controllers.csi.StructureCSI;
import play.Logger;
import play.Play;
import play.jobs.Job;
import play.jobs.On;
import utils.nps.sftp.SFTPConnection;
import utils.nps.vars.EnvironmentVar;

// Scheduler tipo Quartz: http://www.quartz-scheduler.org/documentation/quartz-2.x/tutorials/crontrigger.html
@On("0 0 2 10 * ?") //Fire at 2 am every 10th of the month
public class EmployeeOnMonthlyCSIJob extends Job {
	/**
	 * Obtienen el archivo de secciones
	 */
    public void doJob() {

        SFTPConnection sftpConnection = null;
        File file = new File("tempUserSectionCSI.xlsx");
        Logger.info("Inicio del proceso EmployeeOnMonthlyCSIJob ");
        try {
            sftpConnection = new SFTPConnection(EnvironmentVar.HOST,
                    EnvironmentVar.USER, EnvironmentVar.PASSWORD,
                    EnvironmentVar.PORT, EnvironmentVar.DIRECTORY_STRUCT);
            sftpConnection.configSftp();
            
            String fileName = sftpConnection.getSectionsFile(EnvironmentVar.DIRECTORY_STRUCT);
            
            if ( !fileName.equalsIgnoreCase("")) {
                String user = Play.configuration.getProperty("FileCSIEmployeeSectionsUser");
                //Call Function snap structure

                java.sql.Date structureDate = StructureCSI.loadSnapStructure(user);
                SnapStructure.loadSnapStructure(structureDate);

                if (structureDate != null) {

                    FileOutputStream oFile;
                    sftpConnection.changeDirectory(EnvironmentVar.DIRECTORY_STRUCT);

                    //Load sections employee    	         
                    //File file = new File(pathFile.trim());
                    oFile = new FileOutputStream(file);
                    sftpConnection.getFile(fileName, oFile);

                    StructureCSI.loadSectionsEmployee(file, user, structureDate);

                    oFile.flush();
                    oFile.close();
                    Logger.info("Fin del proceso EmployeeOnMonthlyCSIJob ");
                }
            } else {
                Logger.info(" No existe archivo de secciones  ");
            }
        } catch (JSchException ex) {
            java.util.logging.Logger.
                    getLogger(EmployeeOnMonthlyCSIJob.class.getName()).
                    log(Level.SEVERE, null, ex);
        } catch (SftpException ex) {
            java.util.logging.Logger.
                    getLogger(EmployeeOnMonthlyCSIJob.class.getName()).
                    log(Level.SEVERE, null, ex);
        } catch (FileNotFoundException ex) {
            java.util.logging.Logger.getLogger(EmployeeOnMonthlyCSIJob.class.
                    getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            java.util.logging.Logger.getLogger(EmployeeOnMonthlyCSIJob.class.
                    getName()).log(Level.SEVERE, null, ex);
        } finally {
            if (sftpConnection != null) {
                sftpConnection.disconnect();
            }
            file.delete();
        }
    }

}
