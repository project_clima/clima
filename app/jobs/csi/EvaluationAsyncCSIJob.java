package jobs.csi;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;

import concurrent.csi.SaveThread;
import controllers.csi.SnapEvaluationResults;
import models.CSIEvaluationResults;
import models.csi.CsiEvaluationCrossLog;
import play.Logger;
import play.jobs.Job;
import utils.MailUtils;

public class EvaluationAsyncCSIJob extends Job {

    private List<CSIEvaluationResults> results;
    private List<CsiEvaluationCrossLog> logs;
    private Integer idSurvey;
    private String email;

    /**
     * Método para guardar la carga de resultados
     */
    private void saveResultsAndLogs() {
        try {

            saveMultiThread(results.size(), results); // Save the evaluation
                                                      // results
            saveMultiThread(logs.size(), logs); // Save event logs for
                                                // evaluation results
            SnapEvaluationResults.loadSnapResults(idSurvey);

            String guardados = "\n Cantidad de resultados cargados: " + results.size();
            String guardadosLogs = "\n Cantidad de resultados no cargados: " + logs.size();

            MailUtils.sendMail("Ejecución resultados de encuesta " + idSurvey,
                    "El procesamiento de resultados para la encuesta  " + idSurvey + " fue exitoso" + guardados
                            + guardadosLogs,
                    idSurvey, email);
        } catch (Exception e) {        	
            MailUtils.sendMail("Ejecución resultados de encuesta " + idSurvey,
                    "Ocurrió el siguiente error al guardar los resultados de la encuesta " + e.getLocalizedMessage(),
                    idSurvey, email);
            Logger.error("Error ocurred at save  method" + e.fillInStackTrace());
           
        }
    }

    /**
     * Método para guardar en segundo plano
     * @param listSize
     * @param objects
     * @throws ExecutionException
     * @throws InterruptedException
     */
    private <T> void saveMultiThread(int listSize, List<T> objects) throws ExecutionException, InterruptedException {

        int chunkSize = listSize / 16;
        if (chunkSize <= 0)
            chunkSize = listSize;
        int iterations = 16;
        if (listSize % 16 > 0)
            iterations++;

        int position = 0;

        int poolSize = 3;
        ExecutorService service = Executors.newFixedThreadPool(poolSize);
        List<Future<Runnable>> futures = new ArrayList<Future<Runnable>>();

        for (int i = 0; i < iterations; i++) {

            List<T> evaluations2Save = new ArrayList<>();
            for (int j = 0; j < chunkSize; j++) {
                if (position < listSize)
                    evaluations2Save.add(objects.get(position++));
            }

            Future f = service.submit(new SaveThread(evaluations2Save));
            futures.add(f);
        }

        for (Future<Runnable> f : futures) {
            f.get();
        }

        service.shutdownNow();
    }

    /**
     * @return the results
     */
    public List<CSIEvaluationResults> getResults() {
        return results;
    }

    /**
     * @param results
     *            the results to set
     */
    public void setResults(List<CSIEvaluationResults> results) {
        this.results = results;
    }

    /**
     * @return the logs
     */
    public List<CsiEvaluationCrossLog> getLogs() {
        return logs;
    }

    /**
     * @param logs
     *            the logs to set
     */
    public void setLogs(List<CsiEvaluationCrossLog> logs) {
        this.logs = logs;
    }

    /**
     * @return the idSurvey
     */
    public Integer getIdSurvey() {
        return idSurvey;
    }

    /**
     * @param idSurvey
     *            the idSurvey to set
     */
    public void setIdSurvey(Integer idSurvey) {
        this.idSurvey = idSurvey;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getEmail() {
        return email;
    }

    public void doJob() {
        saveResultsAndLogs();
    }

}
