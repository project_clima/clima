package jobs.csi;

import java.util.Date;

import play.Logger;
import play.jobs.Job;
import play.jobs.OnApplicationStart;

@OnApplicationStart
public class EmployeeApplicationStartCSIJob extends Job{

	public void doJob() {
        Logger.info("OnApplicationStart job ..." + new Date());
    }
}
