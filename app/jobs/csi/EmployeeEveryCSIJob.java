package jobs.csi;

import java.util.Date;

import play.Logger;
import play.jobs.Every;
import play.jobs.Job;
import play.jobs.On;

//@Every("1h")
//@Every("3mn"),@Every("30mn")
//@Every("1mn")
@On("0 0 2 10 * ?")
public class EmployeeEveryCSIJob extends Job{

	
    public void doJob() {
        Logger.info("@Every 1 minute job ..." + new Date());
    }
}
