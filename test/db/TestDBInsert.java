/**
 * @(#) TestDBInsert 17/03/2017
 */

package db;

import com.ibm.as400.access.FTP;
import java.io.File;
import java.io.IOException;
import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.SQLException;
import java.sql.Types;
import java.text.ParseException;
import java.util.Date;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.persistence.Query;
import models.FileNPS;
import models.StructureTmp;
import oracle.jdbc.OracleConnection;
import org.hibernate.internal.SessionImpl;
import org.junit.BeforeClass;
import org.junit.Test;
import play.db.jpa.JPA;
import play.test.UnitTest;
import querys.StructureTmpQuery;
import utils.FormatDateUtil;
import utils.ReadCsvFile;

/**
 *
 * @author Rodolfo Miranda -- Qualtop
 */
public class TestDBInsert extends UnitTest{

    private static FTP ftpClient;
    
    private static File file;
    
    private static final String CSV_FILE = 
            "InformeEstructura_Activos_17032018.csv";
    private static final String USER = "BD";
    private static String ID_FILE = "";
    
    @BeforeClass
    public static void startFTP(){
        String server = "localhost";
        int port = 21;
        String user = "rodo";
        String pass = "rodo";
        ftpClient = new FTP(server,user,pass);
        try {
            ftpClient.connect();
        } catch (IOException ex) {
            System.out.println("Oops! Something wrong happened");
            ex.printStackTrace();
        }
    }
    
    @Test
    public void testInsertFile() throws ParseException{
        
        try {
            
            Query query = JPA.em().
                    createNativeQuery(
                            "select SYS_CONTEXT( 'USERENV', 'CURRENT_USER' "
                            + " ) from dual");
            String user = (String) query.getSingleResult();
            
            FileNPS fileNps = new FileNPS();
            fileNps.fileName = CSV_FILE;
            fileNps.extension= "csv";
            fileNps.module = "NPS";
            fileNps.loadDate = FormatDateUtil.ownFormat.
                    parse(FormatDateUtil.ownFormat.format(new Date()));
            fileNps.type = "AUTO";
            fileNps.status = "READ";
            fileNps.createFor = user;
            fileNps.createDate = FormatDateUtil.ownFormat.
                    parse(FormatDateUtil.ownFormat.format(new Date()));
            fileNps.save();
            
            
            
            ID_FILE = Integer.toString( fileNps.idFile );
            String created = fileNps.createFor;
            Date createDate = fileNps.createDate;
            String modifiedFor = fileNps.modifiedFor;
            
            ftpClient.cd("\\Users\\Qualtop\\Documents\\liverpool documents");
            
            System.out.println(ftpClient.pwd());
            
            file = new File("temp.csv");
            boolean imput = ftpClient.get(CSV_FILE,
                    file);
            
            ftpClient.disconnect();
            
            ReadCsvFile reader = new ReadCsvFile();
            reader.setFileReader(file);
            List<StructureTmp> records = reader.readCvsFile();
            
            int deleted = StructureTmpQuery.truncateTableRH();
            
            fileNps.idFile = Integer.parseInt(ID_FILE);
            fileNps.fileName = CSV_FILE;
            fileNps.extension= "csv";
            fileNps.module = "NPS";
            fileNps.loadDate = FormatDateUtil.ownFormat.
                    parse(FormatDateUtil.ownFormat.format(new Date()));
            fileNps.type = "AUTO";
            fileNps.status = "PROC";
            fileNps.createFor = created;
            fileNps.createDate = FormatDateUtil.ownFormat.
                    parse(FormatDateUtil.ownFormat.format(createDate));
            fileNps.modifiedFor = user;
            fileNps.modifiedDate = FormatDateUtil.ownFormat.
                    parse(FormatDateUtil.ownFormat.format(new Date()));
            fileNps.save();
            
            
            for( StructureTmp record : records ){
                //System.out.println(record.toString());
                record.save();
            }
            OracleConnection oracleConnection = null;
            SessionImpl sessionImpl = ((SessionImpl) JPA.em().getDelegate());
            Connection connection = sessionImpl.connection();
            if (connection.isWrapperFor(OracleConnection.class)){
                oracleConnection= connection.unwrap(OracleConnection.class);  
            }else{
            // recover, not an oracle connection
            }
            if(oracleConnection != null){
                CallableStatement callableStatement = oracleConnection.
                    prepareCall("{ ? = call PORTALUNICO."
                            + "PK_CARGA_ESTRUCTURA.FN_INS_STRUCTURE(?, ?) }");
                callableStatement.registerOutParameter(1, Types.INTEGER);
                callableStatement.setInt(2, Integer.parseInt(ID_FILE));
                callableStatement.setString(3, USER);
                
                
                callableStatement.execute();
                
                int error = callableStatement.getInt(1);
                System.out.println("Result ** "  + error);
            }else{
                System.out.println("Oracle connection fail");
            }
            
            
        } catch (IOException ex) {
            Logger.getLogger(TestDBInsert.class.getName()).
                    log(Level.SEVERE, null, ex);
        } catch (SQLException ex) {
            Logger.getLogger(TestDBInsert.class.getName()).
                    log(Level.SEVERE, null, ex);
        }
    }

}
