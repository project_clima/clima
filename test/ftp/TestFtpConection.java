/**
 * @(#) TestFtpConection 15/03/2017
 */

package ftp;

import com.jcraft.jsch.Channel;
import com.jcraft.jsch.ChannelSftp;
import com.jcraft.jsch.JSch;
import com.jcraft.jsch.JSchException;
import com.jcraft.jsch.Session;
import com.jcraft.jsch.SftpException;
import java.io.IOException;
import java.util.Properties;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;
import play.Play;
import play.test.UnitTest;

/**
 *
 * @author Rodolfo Miranda -- Qualtop
 */
public class TestFtpConection extends UnitTest{
    
    private static ChannelSftp sftp;
    private static Session sftpSession;
    
    private static String host;  
    private static String user;  
    private static String port;
    private static String password;
    private static String directory;
    
    @BeforeClass
    public static void startFtp(){
        JSch jsch = new JSch();
        
        host = Play.configuration.get("ftp.url").toString();
        user = Play.configuration.get("ftp.user").toString();
        port = Play.configuration.get("ftp.host").toString();
        password = Play.configuration.get("ftp.pwd").toString();
        directory = Play.
                configuration.get("ftp.path.load.automatic.results").toString();
        
        try {        
            sftpSession = jsch.getSession(user, host,
                    Integer.valueOf(port));
            
            Properties config   = new Properties(); 
        
            config.put("StrictHostKeyChecking", "no");
            sftpSession.setConfig(config);
            sftpSession.setConfig( "PreferredAuthentications", "password");
            sftpSession.setPassword(password);
            sftpSession.connect();
            
            Channel channel  = sftpSession.openChannel("sftp");
            sftp = ( ChannelSftp ) channel;
            sftp.connect();
            
        } catch (JSchException ex) {
            Logger.getLogger(TestFtpConection.class.getName()).
                    log(Level.SEVERE, null, ex);
        }
    }
    
    @Test
    public void testFtpVariables(){
        assertEquals("transferencias.liverpool.com.mx", host);
    }
    
    @Test
    public void testClient(){
        assertNotNull(sftp);
    }
    
    @Test
    public void testClientPath(){
        assertEquals("/pruebas/respaldo/CDI", directory);
    }
    
    @Test
    public void testClientChangeDirectory() throws IOException, SftpException{
        
        sftp.cd(directory);
       
        assertEquals(directory,sftp.pwd());
    }
    
    @AfterClass
    public static void stopFtp(){
        sftp.exit();
        sftp.disconnect();
        sftpSession.disconnect();
    }
}
