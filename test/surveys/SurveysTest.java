package surveys;

import org.junit.Test;
import play.test.FunctionalTest;
import play.mvc.Http.Response;

public class SurveysTest extends FunctionalTest {

    @Test
    public void simpleSurveyAccessTest() {
        Response response = GET("/surveys/apply/1");
        assertStatus(302, response);
        assertHeaderEquals("Location", "/secure/login", response);
    }
    
    @Test
    public void surveyAccessWithFakeUrlCredentialsTest() {
        Response response = GET("/surveys/apply/1/fakeUser/12345");
        assertStatus(302, response);
        assertHeaderEquals("Location", "/secure/login", response);
    }
    
    @Test
    public void surveyAccessWithRealUrlCredentialsTest() {
        Response response = GET("/surveys/apply/1/jperales/827CCB0EEA8A706C4C34A16891F84E7Bs");
        assertStatus(302, response);
        assertHeaderEquals("Location", "/secure/login", response);
    }
}