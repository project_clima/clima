function getProcessId() {
    var action = baseUrl + 'fileloadnps/manualLoadFileJob';
    $.ajax({
        url: action,
        success: function (data) {
            webSocket(data);
        }
    });
}

function getErrorFile() {
    var action = baseUrl + 'fileloadnps/downloadFileError';
    var myfile = $("#file")[0].files[0];
    if (!myfile) {
       
    }
    var uploadFile = new FormData();
    uploadFile.append("file", myfile);

    var req = new XMLHttpRequest();
    req.open("POST", action, true);
    req.responseType = "blob";
    req.onload = function (event) {
        var blob = req.response;
        if (blob.size > 0) {
            var header = req.getResponseHeader('Content-Disposition');
            var filename = header.match(/filename="(.+)"/)[1];
            //var fileName = req.getResponseHeader("filename") //if you have the fileName header available
            var link = document.createElement('a');
            link.href = window.URL.createObjectURL(blob);
            link.download = filename;
            link.click();
        }
    };
    req.send(uploadFile);
}

function webSocket(data) {
    var action = baseUrl + 'fileloadnps/getStatus';
    var intervalId = null;

    var varName = function(){
        var textFinal = "";
        $.ajax({
            url: action,
            method: 'get',
            data: {
                idFile: Number(data)
            },
            success: function (data) {
                console.log(data);
                if (!data.includes('null,null')){
                    var datos = data.split(",");
                    var uploadProgress = parseInt(datos[0]);
                    var error = datos[1];
                    var idFile = datos[2];

                    if ( !error.includes('Error')) {
                        if (uploadProgress >= 100) {
                            var errores = uploadProgress - 100;
                            uploadProgress = 100;
                            //prgs = 'Proceso terminado ' + uploadProgress + "%";
                            textFinal = 'Proceso Terminado ';
                            $('#message label').html("");
                            $('#message label').html("<font color='red'> INFO: Número de errores en el archivo cargado : " + errores +"</font>");
                            getErrorFile();
                            eliminateFile(idFile);
                            clearInterval(intervalId);
                        }
                    } else {
                        $('#message label').html("<font color='red'> "+ error +".</font>");
                        clearInterval(intervalId);
                    }

                    var prgs = uploadProgress + "%";
                    $("#progressbar-file").css("width", prgs)
                            .attr("aria-valuenow", uploadProgress).html(textFinal + prgs);
                }
            }
        });
    };
     intervalId = setInterval(varName, 15000);
}

function eliminateFile(idFile) {
    var action = baseUrl + 'fileloadnps/deleteFile';
    $.ajax({
            url: action,
            method: 'get',
            data: {
                idFile: Number(idFile)
            },
            success: function (data) {
                console.log(data);
            }
        });
}