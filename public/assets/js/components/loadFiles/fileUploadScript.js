var uploadProgress = 0;
$(document).ready(function () {
    var options = {
        beforeSubmit: function (arr, $form, options) {
            $('#message label').html("");
            if ($('#typeO').val() !== '-1') {
                var type = $('#typeO').val();
                var file = $('#file')[0].files[0];
                var filename = $('input[type=file]').val().replace(/.*(\/|\\)/, '');
                var endFile = filename.split(".");

                if (filename !== '' && endFile !== '') {
                    if (endFile[1] !== null && endFile[1].toLowerCase() === 'csv') {
                        if (type === 'ct'){
                            var j = filename.indexOf("_",2);
                            var suffix = filename.substring(0, j);
                            if (suffix === 'C_TIENDA') {
                                var reader = new FileReader();
                                reader.onload = function(progressEvent){
                                    var lines = this.result.split('\r');
                                    var headers = lines[0].split(',');
                                    if (headers.length === 5) {
                                        if (headers[0] === 'ID_TIENDA' && headers[1] === 'DES_TIENDA' &&
                                            headers[2] === 'ZONA' && headers[3] === 'GRUPO' && headers[4] === 'NEGOCIO') {
                                            return true;
                                        }
                                    }else {
                                        $('#message label').html("<font color='red'> INFO: El archivo no tiene los encabezados correctos \n\
                                            (ID_TIENDA,DES_TIENDA,ZONA,GRUPO,NEGOCIO).</font>");
                                        return false;
                                    }
                                };
                                reader.readAsText(file, "iso-8859-1");
                               
                            } else {
                                $('#message label').html("<font color='red'> INFO: El archivo no tiene el nombre correcto (C_TIENDA_?.csv).</font>");
                                return false;
                            }
                            
                                
                        } else if (type === 'ca') {
                            var j = filename.indexOf("_",2);
                            var suffix = filename.substring(0, j);
                            if (suffix === 'C_AREAS') {
                                var reader = new FileReader();
                                reader.onload = function(progressEvent){
                                    var lines = this.result.split('\r');
                                    var headers = lines[0].split(',');
                                    if (headers.length === 2) {
                                        if (headers[0] === 'ID_AREA' && headers[1] === 'DESC_AREA' ) {
                                            return true;
                                        }
                                    } else {
                                        $('#message label').html("<font color='red'> INFO: El archivo no tiene los encabezados correctos \n\
                                            (ID_AREA,DESC_AREA).</font>");
                                        return false;
                                    }
                                };
                                reader.readAsText(file, "iso-8859-1");
                                
                            } else {
                                $('#message label').html("<font color='red'> INFO: El archivo no tiene el nombre correcto (C_AREAS_?.csv).</font>");
                                return false;
                            }
                           
                        } else if (type === 'cp') {
                            var j = filename.indexOf("_",2);
                            var suffix = filename.substring(0, j);
                            if (suffix === 'C_PRACTICA') {
                                var reader = new FileReader();
                                reader.onload = function(progressEvent){
                                    var lines = this.result.split('\r');
                                    var headers = lines[0].split(',');
                                    if (headers.length === 3) {
                                        if (headers[0] === 'ID_PRACTICA' && headers[1] === 'DES_PRACTICA' &&
                                            headers[2] === 'ID_AREA' ) {
                                            return true;
                                        }
                                    } else {
                                        $('#message label').html("<font color='red'> INFO: El archivo no tiene los encabezados correctos \n\
                                            (ID_PRACTICA,DESC_PRACTICA,ID_AREA).</font>");
                                        return false;
                                    }
                                };
                                reader.readAsText(file, "iso-8859-1");
                                
                            } else {
                                $('#message label').html("<font color='red'> INFO: El archivo no tiene el nombre correcto (C_PRACTICA_?.csv).</font>");
                                return false;
                            }
                            
                        } else if (type === 'cs') {
                            var j = filename.indexOf("_",2);
                            var suffix = filename.substring(0, j);
                            if (suffix === 'C_SECCION') {
                                var reader = new FileReader();
                                reader.onload = function(progressEvent){
                                    var lines = this.result.split('\r');
                                    var headers = lines[0].split(',');
                                    if (headers.length === 2) {
                                        if (headers[0] === 'ID_SECCION' && headers[1] === 'DES_SECCION' ) {
                                            return true;
                                        }
                                    } else {
                                        $('#message label').html("<font color='red'> INFO: El archivo no tiene los encabezados correctos \n\
                                            (ID_SECCION,DESC_SECCION).</font>");
                                        return false;
                                    }
                                };
                                reader.readAsText(file, "iso-8859-1");
                                
                            } else {
                                $('#message label').html("<font color='red'> INFO: El archivo no tiene el nombre correcto (C_SECCION_?.csv).</font>");
                                return false;
                            }
                            
                        } else if (type === 'cnv') {
                            var j = filename.indexOf("_",5);
                            var suffix = filename.substring(0, j);
                            if (suffix === 'C_NO_VENTAS') {
                                var reader = new FileReader();
                                reader.onload = function(progressEvent){
                                    var lines = this.result.split('\r');
                                    var headers = lines[0].split(',');
                                    if (headers.length === 10) {
                                        if (headers[0] === 'ID_EMPLEADO' && headers[1] === 'ID_TIENDA' &&
                                            headers[2] === 'ID_AREA' && headers[3] === 'NOMBRE' && headers[4] === 'CPERSON EMPLEADO' &&
                                            headers[5] === 'CPERSON_JEFE' && headers[6] === 'CPERSON_GERENTE' &&
                                            headers[7] === 'CPERSON_DIRECTOR' && headers[8] === 'CPERSON_ZONA' && headers[9] === 'ID_SECCION') {
                                            return true;
                                        }
                                    } else {
                                        $('#message label').html("<font color='red'> INFO: El archivo no tiene los encabezados correctos \n\
                                            (ID_EMPLEADO,ID_TIENDA,ID_AREA NOMBRE,CPERSON_EMPLEADO,CPERSON_JEFE,CPERSON_GERENTE,CPERSON_DIRECTOR,CPERSON_ZONA,ID_SECCION).</font>");
                                        return false;
                                    }
                                };
                                reader.readAsText(file, "iso-8859-1");
                                
                            } else {
                                $('#message label').html("<font color='red'> INFO: El archivo no tiene el nombre correcto (C_NO_VENTAS_?.csv).</font>");
                                return false;
                            }
                            
                        } else if (type === 'cf') {    
                            var j = filename.indexOf("_",2);
                            var suffix = filename.substring(0, j);
                            if (suffix === 'C_FUNCIONES') {
                                var reader = new FileReader();
                                reader.onload = function(progressEvent){
                                    var lines = this.result.split('\r');
                                    var headers = lines[0].split(',');
                                    if (headers.length === 3) {
                                        if (headers[0] === 'NIVEL' && headers[1] === 'ID_FUNCION' &&
                                            headers[2] === 'DESC_FUNCION' ) {
                                            return true;
                                        }
                                    } else {
                                        $('#message label').html("<font color='red'> INFO: El archivo no tiene los encabezados correctos \n\
                                            (NIVEL,ID_FUNCION,DESC_FUNCION).</font>");
                                        return false;
                                    }
                                };
                                reader.readAsText(file, "iso-8859-1");
                                
                            } else {
                                $('#message label').html("<font color='red'> INFO: El archivo no tiene el nombre correcto (C_FUNCIONES_?.csv).</font>");
                                return false;
                            }
                            
                        
                        } else if (type === 'sap') {
                            var j = filename.indexOf("_");
                            var suffix = filename.substring(0, j);
                            if (suffix === 'ESTRUCTURA') {
                                var reader = new FileReader();
                                reader.onload = function(progressEvent){
                                    var lines = this.result.split('\r');
                                    var headers = lines[0].split(',');
                                    if (headers.length === 31) {
                                        if (headers[0] === 'STATUS' && headers[1] === 'USERID' &&
                                            headers[2] === 'COUNTRY' && headers[3] === 'CUSTOM01' && headers[4] === 'CUSTOM02' &&
                                            headers[5] === 'CUSTOM03' && headers[6] === 'CUSTOM04' &&
                                            headers[7] === 'CUSTOM05' && headers[8] === 'CUSTOM06' && headers[9] === 'CUSTOM07' &&
                                            headers[10] === 'CUSTOM08' && headers[11] === 'CUSTOM09' &&
                                            headers[12] === 'CUSTOM10' && headers[13] === 'CUSTOM13' && headers[14] === 'CUSTOM_MANAGER' &&
                                            headers[15] === 'DEFAULT_LOCALE' && headers[16] === 'DEPARTMENT' &&
                                            headers[17] === 'DIVISION' && headers[18] === 'EMAIL' && headers[19] === 'EMPID' &&
                                            headers[20] === 'FIRSTNAME' && headers[21] === 'GENDER' &&
                                            headers[22] === 'HIREDATE' && headers[23] === 'HR' && headers[24] === 'JOBCODE' &&
                                            headers[25] === 'LASTNAME' && headers[26] === 'LOCATION' &&
                                            headers[27] === 'MANAGER' && headers[28] === 'TIMEZONE' && headers[29] === 'TITLE' &&
                                            headers[30] === 'USERNAME') {
                                            return true;
                                        }
                                    } else {
                                        $('#message label').html("<font color='red'> INFO: El archivo no tiene los encabezados correctos \n\
                                            (STATUS,USERID,COUNTRY,CUSTOM01,CUSTOM02,CUSTOM03,CUSTOM04,CUSTOM05,CUSTOM06,CUSTOM07,CUSTOM08,CUSTOM09,\n\
                                               ,CUSTOM10,CUSTOM13,CUSTOM_MANAGER,DEFAULT_LOCALE,DEPARTMENT,DIVISION,EMAIL,EMPID,\n\
                                               ,FIRSTNAME,GENDER,HIREDATE,HR,JOBCODE,LASTNAME,LOCATION,MANAGER,TIMEZONE,TITLE,USERNAME).</font>");
                                        return false;
                                    }
                                };
                                reader.readAsText(file, "iso-8859-1");
                                
                               
                            } else {
                                $('#message label').html("<font color='red'> INFO: El archivo no tiene el nombre correcto (ESTRUCTURA_?.CSV).</font>");
                                return false;
                            }
                            
                        } else {
                            $('#message label').html("<font color='red'> INFO: Verifica que sea la opción correcta</font>");
                            return false;
                        }
                    } else if (endFile[1] !== null && endFile[1].toLowerCase() === 'txt') {
                        if (type === 'bd') {
                            var j = filename.indexOf("_");
                            var suffix = filename.substring(0, j);
                            if (suffix === 'INFO') {
                                var reader = new FileReader();
                                reader.onload = function(progressEvent){
                                    
                                };
                                reader.readAsText(file, "iso-8859-1");
                                
                                return true;
                            } else {
                                $('#message label').html("<font color='red'> INFO: El archivo no tiene el nombre correcto (INFO_?.txt).</font>");
                                return false;
                            }
                            
                        } else {
                            $('#message label').html("<font color='red'> INFO: Verifica que sea la opción correcta</font>");
                            return false;
                        }
                    } else {
                        $('#message label').html("<font color='red'> INFO: Archivo Invalido, debe ser un archivo CSV o TXT</font>");
                        return false;
                    }
                } else {
                    $('#message label').html("<font color='red'> INFO: Selecciona un archivo</font>");
                    $('input[type=file]').click();
                    return false;
                }
            } else {
                $('#message label').html("<font color='red'> INFO: Escoje alguna de las opciones</font>");
                return false;
            }
            
        },
        beforeSend: function () {
            // clear everything
            $("#progressbar-file").css("width", "0%").attr("aria-valuenow", 0);
        },
        uploadProgress: function (event, position, total, percentComplete) {
            //console.log(event + " "+ position+ " " +total+ " "+ percentComplete);
            if ($('input[name=load_sap]:checked').val() === 'complete') {
                uploadProgress = (percentComplete);
            } else {
                uploadProgress = (percentComplete / 4);
            }

            var prgs = uploadProgress + "%";
            $("#progressbar-file").css("width", prgs)
                    .attr("aria-valuenow", uploadProgress).html(prgs);

            //$("#percent").html(percentComplete + '%');


        },
        success: function () {
            if ($('input[name=load_sap]:checked').val() === 'complete') {
                $(".progress-bar").width('100%');
                //$("#percent").html('100%');
                //getProcessId();
            } else {
                $(".progress-bar").width('25%');
                //$("#percent").html('100%');
                getProcessId();
            }

        },
        complete: function (response) {
            //$("#message").html("<font color='blue'>Your file has been uploaded!</font>");
        },
        error: function () {
            //$("#message").html("<font color='red'> ERROR: unable to upload files</font>");
            console.log("3434");
        }
    };
    $("#formPlay").ajaxForm(options);

    $('input[type="file"]').change(function () {
        $("#progressbar-file").css("width", "0%").attr("aria-valuenow", 0);
//    $('#myFormId').resetForm();
    });
});
