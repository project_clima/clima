/****************Variables*****************/
var levels = {
    'company': 'Compañía',
    'group': '',
    'business': '',
    'comparativa': ''
};

/***** Fill menu groups******************/

function fillGroups(data) {

    $('#groups-nps').html("");

    var groups = data;

    for (var k in groups) {

        var l = groups[k];

        $('ul#groups-nps').append(
                '<li class="dropdown-submenu fst-lvl-li" id="group-' + l.id + '">\n\
                <a class="test" tabindex="-1" data-target="group" data-group-id="' + l.id + '"\n\
                href="javascript:void(0);" onclick="changeBtnGroup(this, true)">'
                + l.desc + '<span class="glyphicon glyphicon-menu-right pull-right arrow-nps"></span></a>\n\
                <ul class="dropdown-menu second-lvl-li">\n\
                \n\
                </ul>\n\
                </li>\n\
                '
                );

        var list = groups[k];

        for (var i = 0; i < list.business.length; i++) {

            var e = list.business[i];

            $("#group-" + list.id + " ul").append(
                    ' <li><a tabindex="-1" ' +
                    ' data-group="' + list.id + '"' +
                    ' data-desc="' + e.desc + '"' +
                    ' data-target="business" ' +
                    ' data-busns-id="' + e.id + '"' +
//                    ' href="" ' +
                    ' onclick="changeBtnBusiness(this)"' +
                    ' >'
                    + e.desc +
                    ' </a>' +
                    ' </li>'

                    );
        }
    }

    $('#btn-groups').fadeIn(400);

    $(document).on('click', '.dropdown-menu :not(dropdown-submenu)', function (e) {
        e.stopPropagation();
    });

    $('.dropdown-submenu a.test').on("click", function (e) {
        $(this).next('ul').toggle();
        e.stopPropagation();
        e.preventDefault();
    });

}
/*****************************************/
/////////////////////////////////////////
/****************************************/
function changeBtnGroup(chb, closeMenu) {

    var _this = $(chb);
    var id = _this.data("group-id");
    var name = _this.text();
    grp = id;

    localStorage.setItem("group", grp);
    localStorage.setItem("groupN", name);

    resetVariables('group');
    resetLocalStorage('group');
    resetNames('group');

    loadData();
//    updateBreadcrumb('group',null);
//    Liverpool.loading('show');
//    resetFilters();
//    
//    openMainTable();

    if (closeMenu) {
        console.log("closeMenu");
        $(chb).parent().parent().parent().removeClass("open");
    }
}
//////////////////////////////////////////
/********Change button panel business****/
function changeBtnBusiness(chb) {

    var chb = $(chb);
    var id = chb.data("busns-id");
    var name = chb.text();
    bsns = id;

    localStorage.setItem("business", bsns);
    localStorage.setItem("businessN", name);

    resetVariables('business');
    resetLocalStorage('business');
    resetNames('business');

    loadData();
//    Liverpool.loading('show');
//    resetFilters();
//    
//    createZones();
//    
//    openMainTable();
}
///////////////////////////////////////////
/*****************************************/
function getEvsStores() {

    $(".managers-row").hide();
    $("#btn-section").hide();

    getManagersChiefs("?zNps=" + JSON.stringify(zoneStore));

    loadData();
}
///////////////////////////////////////////
/*****************************************/
function getEvsChiefs() {

    $("#btn-section").hide();

    createSections("?zNps=" + JSON.stringify(zoneStore));

    loadData();
}
///////////////////////////////////////////
/*****************************************/
function getEvsSections() {


    loadData();
}
///////////////////////////////////////////
/*****************************************/
function changeDates() {
    createGroups('?firstDate=' + from + '&lastDate=' + to);
    loadData();
}
///////////////////////////////////////////
function updateBreadcrumb(level, filter, levelId) {
    var $li = $('#levels-breadcrumb li[data-level="' + level + '"]');

    if ($li.length === 0) {
        var text = '<span class="glyphicon glyphicon-chevron-right"></span> ' + levels[level];

        var $li = $('<li></li>').html(text)
                .attr('data-level', level)
                .attr('data-filter', filter);

        $('#levels-breadcrumb').append($li);
    } else {
        removeFromBreadcrumb($li);
    }
}
;

function removeFromBreadcrumb($li) {
    var index = parseInt($('#levels-breadcrumb > li').index($li));
    var length = $('#levels-breadcrumb > li').length;

    for (var i = index + 1; i < length; i++) {
        $('#levels-breadcrumb > li:eq(' + i + ')').addClass('to-remove');
    }

    $('#levels-breadcrumb > li.to-remove').remove();
}
;

function loadData() {
    if (typeof (Storage) !== 'undefined') {

        from = localStorage.getItem("from") ? localStorage.getItem("from") : "";
        to = localStorage.getItem("to") ? localStorage.getItem("to") : "";
        zoneStore = JSON.parse(localStorage.getItem("compare")) ?
                JSON.parse(localStorage.getItem("compare")) : [];
        grp = localStorage.getItem("group") ? localStorage.getItem("group") : "";
        bsns = localStorage.getItem("business") ? localStorage.getItem("business") : "";

        var group = localStorage.getItem("groupN");
        var business = localStorage.getItem("businessN");
        var comparativa = localStorage.getItem("comparativa");

        levels['group'] = group;
        levels['business'] = business;
        levels['comparativa'] = comparativa;

        // var filter  = null;

        if (grp !== null && grp !== '') {
            //filter = "{'groupId': " + groupId + "}";
            updateBreadcrumb('company', null);
            updateBreadcrumb('group', null);
        }

        if (bsns !== null && bsns !== '') {
            //filter =  "{'groupId': " + groupId + ", 'businessId': " + businessId + "}";
            //findBusiness();
            updateBreadcrumb('business', null);
        }

        if (zoneStore !== null && zoneStore.length > 0) {
            updateBreadcrumb('comparativa', null);
        }

        if (grp !== '' && bsns === '' && zoneStore < 1) {

            return loadTable('group');
        } else if (grp !== '' && bsns !== '' && zoneStore.length < 1) {


            return loadTable('business');
        } else if (grp !== '' && bsns !== '' && zoneStore.length > 0) {

            return loadTable('comparativa');
        }
    }

    loadTable();
}
;

function loadTable(level) {

    var currentLevel = level || 'company';

    if (level === 'business') {
        createZones();
    }

    if (level === 'comparativa') {
        checkFlow(levels['comparativa']);
    } else {
        resetFilters();
    }

    resetNames(level);
    resetVariables(level);
    updateBreadcrumb(currentLevel, null);
    resetLocalStorage(level);
    Liverpool.loading('show');
    checkFilters(levels['comparativa']);

    openMainTable();
}

function resetVariables(type) {
    if (type === 'company') {
        resetFilters();
        resetNames('company');
        grp = '';
        bsns = '';
        chfs = [];
        strs = [];
        managers = [];
        sections = [];
        zoneStore = [];
    }
    if (type === 'group') {
        bsns = '';
        chfs = [];
        strs = [];
        managers = [];
        sections = [];
        zoneStore = [];
    }

    if (type === 'business') {

        chfs = [];
        strs = [];
        managers = [];
        sections = [];
        zoneStore = [];
    }

    if (type === 'comparativa') {
        switch (levels['comparativa']) {
            case '| Comparativa Ubicaciones':
                chfs = [];
                managers = [];
                sections = [];
                break;
            case '| Comparativa Jefes':

                managers = [];
                sections = [];
                break;
            case '| Comparativa Secciones':

                break;
        }

    }

}

function checkFlow(comparativa) {
    if (comparativa === '| Comparativa Ubicaciones') {
        $(".managers-row").hide();
        $("#btn-section").hide();
        if ($('.zone-row').css('display') == 'none') {
            createZones();
        }
        getManagersChiefs("?zNps=" + JSON.stringify(zoneStore));
    } else if (comparativa === '| Comparativa Jefes' ||
            comparativa === '| Comparativa Secciones') {
        if ($('.zone-row').css('display') == 'none') {
            createZones();
        }
        $("#btn-section").hide();
        getManagersChiefs("?zNps=" + JSON.stringify(zoneStore));
        createSections("?zNps=" + JSON.stringify(zoneStore));
    } else if (comparativa === '| Comparativa Secciones') {

    }
}

function resetNames(level) {
    if (level === 'company') {
        localStorage.setItem("groupN", "");
        localStorage.setItem("businessN", "");
        localStorage.setItem("comparativa", "");
        levels['group'] = '';
        levels['business'] = '';
        levels['comparativa'] = '';

    } else if (level === 'group') {
        localStorage.setItem("businessN", "");
        localStorage.setItem("comparativa", "");

        levels['business'] = '';
        levels['comparativa'] = '';
    } else if (level === 'business') {
        localStorage.setItem("comparativa", "");

        levels['comparativa'] = '';
    }
}

function resetLocalStorage(level) {
    if (level === 'company') {
        localStorage.setItem("group", "");
        localStorage.setItem("business", "");
        localStorage.setItem("comparativa", "");
        localStorage.setItem("compare", JSON.stringify([]));
    } else if (level === 'group') {
        localStorage.setItem("business", "");
        localStorage.setItem("comparativa", "");
        localStorage.setItem("compare", JSON.stringify([]));
    } else if (level === 'business') {
        localStorage.setItem("comparativa", "");
        localStorage.setItem("compare", JSON.stringify([]));
    }
}

