
var Liverpool = function() {
    
    var binary = '';
    
    $(function() {
        $('.sidebar .nav').navgoco({
            caretHtml: false,
            accordion: true
        });

        $('#toggle-left').on('click', function() {
            var bodyEl = $('#main-wrapper');
            ($(window).width() > 767) ? $(bodyEl).toggleClass('sidebar-mini'): $(bodyEl).toggleClass('sidebar-opened');
        });
    });
    
    var nl2br = function (str, is_xhtml) {
        var breakTag = (is_xhtml || typeof is_xhtml === 'undefined') ? '<br ' + '/>' : '<br>';

        return (str + '').replace(/([^>\r\n]?)(\r\n|\n\r|\r|\n)/g, '$1' + breakTag + '$2');
    };
    
    var setMessage = function (container, message, type) {
        var div = $('<div />');
        div.append(nl2br(message));
        div.addClass('alert alert-' + type).prepend('<button type="button" class="close" data-dismiss="alert">&times;</button>');
        container.html(div);
    };
    
    var toggleDisable = function(el) {
        $.each(el, function(){
            if ($(this).is(':disabled')) {
                $(this).removeAttr('disabled');
            } else {
                $(this).attr('disabled', 'disabled');
            }
        });
    };
    
    var loading = function(action, msg) {
        if(action && action == "show") {
            $("#loading-sign-msg").text(msg || "Un momento por favor");
            $(".loading-div").show();
        } else {
            $(".loading-div").fadeOut(250);
        }
    };

    var strDate = function(d) {// <-- JavaScript date object
        var date = d || new Date();
                    
        var day = ("0" + date.getDate()).slice(-2);
        var month = ("0" + (date.getMonth() + 1)).slice(-2);
        var year = date.getFullYear();

        return day + "/" + month + "/" + year;
    };

    var pad = function(n, width, z) {
        z = z || '0';
        n = n + '';
        return n.length >= width ? n : new Array(width - n.length + 1).join(z) + n;
    };
    
    var scrollTo = function ($el) {
        $('html,body').animate({scrollTop: $el.offset()==undefined?0:$el.offset().top - 20});
    };

    var getSearchElem = function(key) {
        var value = "";
        if(key) {
            var search = location.search;
            search = search.substring(1, search.length);//Drop '?'
            var kv = search.split("&");

            for(var i = 0; i < kv.length; i++) {
                var k = kv[i].split("=")[0];
                var v = kv[i].split("=")[1];

                if(k == key) {
                    value = v;
                    break;
                }
            }
        }
        return value;
    };

    var initAudioControls = function() {
        var ctrls = $(".play-liv-control");

        for(var i = 0; i < ctrls.length; i++) {
            var c = ctrls[i];
            var ap = document.createElement("audio");
            var $ap = $(ap);
            $ap.css("width", "950px");
            var audioPath =  baseUrl + 'evaluations/getRecordAudio?filename=' + c.dataset.audioSrc;
            
            $ap.attr("controls", "controls");
            $ap.attr("controlsList", "nodownload");
            getFileAudio(audioPath, function (audioUrl) { //File found
                $ap.append('<source src="' + audioUrl + '" type="audio/mpeg">');
                $(c).append($ap);
            }, function () { // File not found
                $(c).append($ap);
            });
        }
    };

    var fitImgVentana = function(img) {
      $(".modal-content").animate({
        "width": $(img).width() + 30
      }, 300, function() {
        //$(".modal-content").css({marginLeft: "auto", marginRight: "auto"})
        $('.modal-content').animate({
          'left': '50%',
          'margin-left': -$(img).width()/2
        }, 300);
        $(".bootstrap-dialog-title").html("");
      });
    }
    
    return {
        setMessage: setMessage,
        toggleDisable: toggleDisable,
        loading: loading,
        strDate: strDate,
        pad: pad,
        scrollTo: scrollTo,
        getSearchElem: getSearchElem,
        initAudioControls: initAudioControls,
        fitImgVentana: fitImgVentana
    };
}();
function getFileAudio(audio, successCB, errorCB) {
    $.ajax({
        url: audio,
        type: 'HEAD',
        async: false,
        success : function(){
            successCB(audio);
        },
        error : function(){
           errorCB()          
        }
    });
}
