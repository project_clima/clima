$(document).ready(function() {
    Str.matchCharByClass("percent", /^[0-9]*$/g);

    for(var i = 0; i < 34; i++) {
        $("#table-1 tbody").append(
            "<tr>" +
                "<td>" +
                    (i + 1) +
                "</td>" +
                "<td>" +
                    "Zona " + (i + 1) +
                "</td>" +
                "<td>" +
                    "Área " + (i + 1) +
                "</td>" +
                "<td>" +
                    (i * 3) + "%" +
                "</td>" +
            "</tr>"
        );
    }

    ut.init("table-1", {
        disableSearch: true,
        pageSize: 4,
        tableStyling: false
    });
});