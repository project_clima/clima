var Tracking = function () {
    var startDate           = null;
    var endDate             = null;
    var period              = null;
    var detractorsGraph     = null;
    var statusGraph         = null;
    var currentFilter       = false;
    var currentLevel        = false;
    var tabletracking       = null;
    var detailStoreTable    = null;
    var detailManagerTable  = null;
    
    var levels = {
        'company': 'Compañía',
        'group': 'Grupos',
        'business': 'Negocios',
        'zone': 'Zonas',
        'location': 'Ubicaciones',
        'store': 'Tiendas',
        'manager': 'Gerentes'
    };
    
    var levelsByName = {};
    
    var months = [
        'Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo', 'Junio', 'Julio', 
        'Agosto', 'Septiembre', 'Octubre', 'Noviembre', 'Diciembre'
    ];
    
    $(function () {            
        $('#levels-breadcrumb').on('click', 'li', function () {
            if (!$(this).is(':last-child')) {
                loadTable($(this).data('level'), null, $(this).data('filter'));
                removeFromStorage($(this).data('level'));
            }
        });

        $('#table-data').on('click', '.table-tracking tbody tr', function () {
            var id      = $(this).data('id');
            var filter  = $(this).data('filter');
            var target  = $(this).data('target');
            var persist = $(this).data('persist');

            if (typeof target === 'undefined') {
                return false;
            }
            
            if (typeof persist !== 'undefined') {
                persistData(target, id);
            }
            
            levelsByName[target] = $(this).data('level-name');
            saveLevelsNamesToStorage();
            loadTable(target, id, filter);
        });
        
        initGraphs();
        loadData();
    });
    
    var loadData = function () {
        if (typeof(Storage) !== 'undefined') {
            getLevelsNamesFromStorage();
            startDate = localStorage.getItem('from');
            endDate   = localStorage.getItem('to');
            period    = localStorage.getItem('period');
        
            var filter  = null;
            var groupId = localStorage.getItem('group') || null;
            var businessId = localStorage.getItem('business') || null;

            if (groupId !== null) {
                filter = "{'groupId': " + groupId + "}";
                updateBreadcrumb('company', null);
                updateBreadcrumb('group', filter);
            }

            if (businessId !== null) {
                filter =  "{'groupId': " + groupId + ", 'businessId': " + businessId + "}";
                updateBreadcrumb('business', filter);
            }
            
            if (groupId !== null && businessId === null) {
                return loadTable('group', groupId, filter);
            } else if (groupId !== null && businessId !== null) {
                return loadTable('business', businessId, filter);
            }
        }
        
        loadTable();
    };
    
    var persistData = function (target, id) {
        localStorage.setItem(target, id);
    };
    
    var getLevelsNamesFromStorage = function () {
        if (localStorage.getItem('tracking-levels-names') === null) {
            levelsByName = {};
        } else {
            levelsByName = JSON.parse(localStorage.getItem('tracking-levels-names'));
        }
    };
    
    var saveLevelsNamesToStorage = function () {
        localStorage.setItem('tracking-levels-names', JSON.stringify(levelsByName));
    };
    
    var removeFromStorage = function (level) {
        var levelsAllowed = ['company', 'group', 'business'];
        var index = levelsAllowed.indexOf(level);
        
        if (index !== -1) {
            for (var i = index + 1; i < levelsAllowed.length; i++) {
                localStorage.removeItem(levelsAllowed[i]);
            }
        } 
    };
    
    var changeFilterDate = function (start, end, option) {
        startDate = start;
        endDate = end;
        period = option;
        
        loadTable(currentLevel, null, currentFilter);
    };
    
    var loadTable = function (level, levelId, filter) {
        currentLevel = level || 'company';
        currentFilter = filter;
        
        var data = {
            level: currentLevel,
            startDate: startDate,
            endDate: endDate,
            levelId: levelId || null
        };
        
        if (currentFilter) {
            $.extend(data, JSON.parse(currentFilter.replace(/'/ig, '"')));
        }
        
        Liverpool.loading('show');
        
        $.post(baseUrl + 'tracing/getLevelData', data, function (result) {
            $('#table-data').empty().html(result);
            updateDatatable();
            updateGraphData();            
            updateDateBreadcrumb();
            updateBreadcrumb(currentLevel, filter, levelId);
            changeTitle(currentLevel, levelId);
            renderDetailsTable();
            Liverpool.loading('hide');
        });
    };
    
    var updateBreadcrumb = function (level, filter, levelId) {
        var $li = $('#levels-breadcrumb li[data-level="' + level + '"]');
        
        if ($li.length === 0) {
            var levelName = levelsByName[level] || 'Compañía';
            var text = '<span class="glyphicon glyphicon-chevron-right"></span> ' + levelName;
            
            if (level === 'zone') {
                text = '<span class="glyphicon glyphicon-chevron-right"></span> Zona ' + levelId.replace('Zona ', '');
            }
            
            var $li = $('<li></li>').html(text)
                .attr('data-level', level)
                .attr('data-filter', filter);
        
            $('#levels-breadcrumb').append($li);
        } else {
            removeFromBreadcrumb($li);
        }
    };
    
    var updateDateBreadcrumb = function () {
        var chevron = '<span class="glyphicon glyphicon-chevron-right"></span> ';
        var periodDate = startDate + ' - ' + endDate;
        var periodName = period || 'Mensual';
        
        if ((!isEmpty(startDate) && !isEmpty(endDate)) && isEmpty(period)) {
            periodName = predictPeriodName(startDate, endDate);
        }
        
        if (periodName === 'Mensual') {
            var date = startDate ? parseDate(startDate) : new Date();            
            periodDate = months[date.getMonth()] + ' ' + date.getFullYear();
        }
        
        $('.period-name').html(chevron + periodName);
        $('.period-date').html(chevron + periodDate);
    };
    
    var isEmpty = function (value) {
        return $.trim(value) === '' || value === null || typeof value === 'undefined';
    };
    
    var predictPeriodName = function () {
        var start = startDate.split('/');
        var end = endDate.split('/');
        var lastDay = getLastDayOfTheMonth(parseInt(end[2]), parseInt(end[1]));
        
        if ((parseInt(end[1]) - parseInt(start[1]) === 2) && parseInt(end[1]) % 3 === 0) {
            return 'Trimestral';
        }
        
        if ((parseInt(start[1]) !== parseInt(end[1])) || 
            (parseInt(start[0]) !== 1 || parseInt(end[0]) !== lastDay)) {
            return 'Periodo';
        }
        
        return 'Mensual';
    };
    
    var parseDate = function (date) {
        var dateParts = date.split('/');
        
        return new Date(dateParts[2], parseInt(dateParts[1]) - 1, dateParts[0]);
    };
    
    var getLastDayOfTheMonth = function (year, month) {
        return new Date(year, month, 0).getDate();
    };
    
    var changeTitle = function (level, levelId) {
        var title = levelsByName[level] || 'Compañía';
            
        if (level === 'zone') {
            title = 'Zona ' + levelId.replace('Zona ', '');
        }
            
        $('#current-level').html(title.toUpperCase());
    };
    
    var removeFromBreadcrumb = function ($li) {
        var index = parseInt($('#levels-breadcrumb > li').index($li));
        var length = $('#levels-breadcrumb > li').length;

        for (var i = index + 1; i < length; i++) {
            $('#levels-breadcrumb > li:eq(' + i + ')').addClass('to-remove');
        }

        $('#levels-breadcrumb > li.to-remove').remove();
    };
    
    var updateGraphData = function () {
        var data = {
            'detractors': 0,
            'promoters': 0,
            'open': 0,
            'process': 0,
            'finished': 0
        };
        
        $('.table-tracking tbody tr').each(function () {
            data.detractors += parseFloat($('.data-detractors', $(this)).html());
            data.promoters += parseFloat($('.data-promoters', $(this)).html());
            data.open += parseFloat($('.data-open', $(this)).html());
            data.process += parseFloat($('.data-process', $(this)).html());
            data.finished += parseFloat($('.data-finished', $(this)).html());
        });
        
        updateDetractosGraphData(data.detractors, data.promoters);
        updateStatusGraphData(data.open, data.process, data.finished);
    };
    
    var updateDetractosGraphData = function (detractors, promoters) {
        var data = [{
            name: 'Promotoras',
            y: promoters
        }, {
            name: 'Detractoras',
            y: detractors
        }];
        
        detractorsGraph.series[0].setData(data, false);
        detractorsGraph.redraw();
    };
    
    var updateStatusGraphData = function (open, process, finished) {
        var data = [{
            name: 'Abiertas',
            y: open
        },
        {
            name: 'Seguimiento',
            y: process
        },
        {
            name: 'Finalizadas',
            y: finished
        }];
        
        statusGraph.series[0].setData(data, false);
        statusGraph.redraw();
    };
    
    var renderDetractorsGraph = function () {
        Highcharts.setOptions({
            colors: ['#548235', '#C55A11']
        });

        detractorsGraph = Highcharts.chart('graph-detractors-promoters', {
            chart: {
                plotBackgroundColor: null,
                plotBorderWidth: null,
                plotShadow: false,
                type: 'pie'
            },
            title: {
                text: ''
            },
            tooltip: {
                pointFormat: '<b>{point.percentage:.1f}%</b>'
            },
            plotOptions: {
                pie: {
                    allowPointSelect: true,
                    cursor: 'pointer',
                    dataLabels: {
                        enabled: true,
                        format: '<b>{point.name}</b>: {point.percentage:.1f} %',
                        style: {
                            color: (Highcharts.theme && Highcharts.theme.contrastTextColor) || 'black',
                            fontSize: '13px'
                        }
                    },
                    showInLegend: true
                }
            },
            series: [{
                name: '.',
                colorByPoint: true,
                data: []
            }]
        });
    };
    
    var renderStatusGraph = function () {
        Highcharts.setOptions({
            colors: ['#C55A11', '#FFD966', '#548235']
        });
        
        statusGraph = Highcharts.chart('graph-status', {
            chart: {
                plotBackgroundColor: null,
                plotBorderWidth: null,
                plotShadow: false,
                type: 'pie'
            },
            title: {
                text: ''
            },
            tooltip: {
                pointFormat: '<b>{point.percentage:.1f}%</b>'
            },
            plotOptions: {
                pie: {
                    allowPointSelect: true,
                    cursor: 'pointer',
                    dataLabels: {
                        enabled: true,
                        format: '<b>{point.name}</b>: {point.percentage:.1f} %',
                        style: {
                            color: (Highcharts.theme && Highcharts.theme.contrastTextColor) || 'black',
                            fontSize: '13px'
                        }
                    },
                    showInLegend: true
                }
            },
            series: [{
                name: '.',
                colorByPoint: true,
                data: []
            }]
        });
    };
    
    var renderDetailsTable = function () {
        if (currentLevel === 'business') {
            renderDetailStoresTable();
            renderDetailManagerTable();
        } else {
            if (detailStoreTable !== null) {
                detailStoreTable.fnDestroy();
            }
            
            if (detailManagerTable !== null) {
                detailManagerTable.fnDestroy();
            }
            
            detailManagerTable = null;
            detailStoreTable = null;            
        }
    };
    
    var updateDatatable = function () {    
        
        if (tabletracking !== null) {
            tabletracking.fnDestroy();
        }
        
        tabletracking = $('.table-tracking').dataTable({
            "iDisplayLength": 150,
            "bFilter": true,
            "bLengthChange": false,
            "bAutoWidth": false,
            "bDestroy": true,
            "bSort": true,
            "bInfo": false,
            "oLanguage": {
                "sZeroRecords": "Ningún Registro",
                "sInfo": "Mostrando tiendas de _START_ al _END_ de _TOTAL_ totales",
                "sInfoEmpty": "0 Tiendas",
                 "sPaginatePrevious": "Previous page",
                 "sProcessing": "Cargando...",
                 "oPaginate": {
                    "sFirst":    "<<",
                    "sLast":     ">>",
                    "sNext":     ">",
                    "sPrevious": "<"
                },
                "sSearch": "Buscar:"
            },
            "pagingType": "full_numbers"
        });
    };
    
    var renderDetailStoresTable = function () {        
        detailStoreTable = $('#detail-stores').dataTable({
            "iDisplayLength": 150,
            "bFilter": true,
            "bLengthChange": false,
            "bAutoWidth": false,
            "bDestroy": true,
            "bSort": true,
            "bInfo": false,
            "oLanguage": {
                "sZeroRecords": "Ningún Registro",
                "sInfo": "Mostrando tiendas de _START_ al _END_ de _TOTAL_ totales",
                "sInfoEmpty": "0 Tiendas",
                 "sPaginatePrevious": "Previous page",
                 "sProcessing": "Cargando...",
                 "oPaginate": {
                    "sFirst":    "<<",
                    "sLast":     ">>",
                    "sNext":     ">",
                    "sPrevious": "<"
                },
                "sSearch": "Buscar:"
            },
            "pagingType": "full_numbers"
        });
    };
    
    var renderDetailManagerTable = function () {        
        detailManagerTable = $('#detail-managers').dataTable({
            "iDisplayLength": 150,
            "bFilter": true,
            "bLengthChange": false,
            "bAutoWidth": false,
            "bDestroy": true,
            "bSort": true,
            "bInfo": false,
            "oLanguage": {
                "sZeroRecords": "Ningún Registro",
                "sInfo": "Mostrando gerentes de _START_ al _END_ de _TOTAL_ totales",
                "sInfoEmpty": "0 Gerentes",
                 "sPaginatePrevious": "Previous page",
                 "sProcessing": "Cargando...",
                 "oPaginate": {
                    "sFirst":    "<<",
                    "sLast":     ">>",
                    "sNext":     ">",
                    "sPrevious": "<"
                },
                "sSearch": "Buscar:"
            },
            "pagingType": "full_numbers"
        });
    };
    
    var initGraphs = function () {
        renderDetractorsGraph();
        renderStatusGraph();
    };
    
    return {
        changeFilterDate: changeFilterDate
    };
}();