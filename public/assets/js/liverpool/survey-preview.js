var SurveyPreview = function() {
    var maxPage         = 1;
    var currentProgress = 0;
    
    var bindEvents = function() {        
        $('#start-survey').click(function() {
            $('#page-start').hide();
            $('#title-progress').removeClass('hidden').show();
            $('#page-1').removeClass('hidden').show();
        });

        $('.next-page').click(function() {
            var currentPage = $(this).data('current');
            var nextPage    = $(this).data('next');
            
            $('#page-' + currentPage).hide();
            $('#page-' + nextPage).removeClass('hidden').show();
            progress(currentPage);          
        });

        $('.prev-page').click(function() {
            var current = $(this).data('current');
            var prev    = $(this).data('prev');

            $('#page-' + current).hide();
            $('#page-' + prev).removeClass('hidden').show();

            if (prev === 'start') {
                $('#title-progress').hide();
            }
        });
        
        $('#close-survey').click(function(e) {
            e.preventDefault();
        });
    };
    
    var progress = function(currentPage) {
        maxPage         = currentPage > maxPage ? currentPage : maxPage;
        currentProgress = (parseInt(maxPage) * 100) / $('.page').length;

        $('#current-survey-password').text(currentProgress.toFixed(0));
        $('#survey-progress .progress-bar').css({'width': currentProgress + '%'});
        $('#survey-progress .progress-bar').attr('aria-valuenow', currentProgress);
        
        $('html, body').animate({scrollTop: '0px'}, 750);
    };
    
    var init = function() {
        bindEvents();
    };
    
    init();
};

new SurveyPreview();