var EmailTemplateEdit = function() {
    var bindEvents = function () {
        $('#preview').click(function () {
            preview();
        });
    };
    
    var setEditor = function () {
        tinymce.init({
            selector: '#template-body',
            language: 'es_MX',
            height : "300",
            menubar: false,
            plugins: [
                'advlist autolink lists link image charmap hr anchor pagebreak',
                'visualblocks visualchars code fullscreen preview',
                'media nonbreaking save table contextmenu directionality',
                'emoticons template paste textcolor colorpicker textpattern imagetools'
            ],
            toolbar: 'fontsizeselect',
            fontsize_formats: '8pt 10pt 12pt 14pt 18pt 24pt 36pt',
            toolbar1: 'insertfile undo redo | styleselect | fontsizeselect | fontselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent',
            toolbar2: 'link image media | forecolor backcolor | emoticons | table | code | fullscreen preview',
            plugin_preview_width: 1000,
            elementath: false,
            convert_urls: false,
            setup: function (ed) {
                ed.on('init', function(args) {
                    $('#mdl-add-edit').modal('show');
                });
                
                ed.on('blur', function() {
                    tinymce.triggerSave();
                    $("#template-body").html( tinyMCE.activeEditor.getContent() );
                });
                
                ed.on('keyup', function (ed, event) {
                    tinymce.triggerSave();
                });
            }
        });
    };
    
    var validate = function (section) {
        $('#template-form').validate({
            'ignore': '',
            messages: {
                'template.subject': {
                    'required': 'Por favor, ingrese el asunto del correo'
                },
                'template.body': {
                    'required': 'Por favor, ingrese el contenido del correo'
                }
            }
        });
    };
    
    var preview = function() {
        var url = baseUrl + "emailtemplates/";
        
        var data = {
            'template.subject': $('#template-subject').val(),
            'template.body': encodeURI($('#template-body').val()),
            'template.id': $('#template-id').val(),
            'template.type.id': $('#template-type').val()
        };

        $.post(url + 'preview?_' + Math.random(), data, function (data) {            
            $('#preview-iframe').attr('src', 'data:text/html,' + escape(data) + '<p style="display: none"> ' +  Math.random() +  ' </p>');
            $('#preview-subject').text($('#template-subject').val());
            $('#mdl-preview').modal('show');
        });
    };
    
    var init = function () {
        bindEvents();
        validate();
        setEditor();
    };
    
    return {
        init: init
    };
}();

EmailTemplateEdit.init();