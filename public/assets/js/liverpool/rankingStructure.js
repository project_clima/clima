function loadTopData(data){

    fillTop(data);
    fillRigthTable(data);
    
    Liverpool.loading("hide");
}

function getTopNps(path){
    var action = '/structureranking/getTopNps' + path;
    
    Liverpool.loading("show");
    $.ajax({
            url: action,
            success: function(data){
                loadTopData(data);
            }
    });
}

$(document).ready(function() {
    getTopNps("");
});

