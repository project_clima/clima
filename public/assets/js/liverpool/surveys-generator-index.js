var Surveys = function() {
    var oTable     = null;
    var structure  = null;
    var deselected = [];
    var isEdit = false;

    var bindEvents = function() {
        $(function () {
            $('#add').click(function () {
                isEdit = false;
                
                $.get(baseUrl + 'surveysgenerator/addSurvey', function (data) {
                    showForm(data);
                });
            });

            $('.table-lp').on('click', '.edit', function (e) {
                e.preventDefault();
                isEdit = true;
                
                $.get($(this).attr('href'), function (data) {
                    showForm(data);

                    if ($('#surveyType').val() !== '' || $('#month').val() !== '' || $('#year').val() !== '') {
                        setTree();
                    }
                });
            });

            $('.table-lp').on('click', '.delete', function (e) {
                e.preventDefault();
                var url = $(this).attr('href');

                swal({
                    title: "",
                    text: "¿Eliminar encuesta?",
                    type: "warning", 
                    showCancelButton: true,
                    cancelButtonText: "Cancelar",
                    confirmButtonText: "Continuar",
                    closeOnConfirm: true
                }, function() {
                    deleteSurvey(url);
                });
            });

            $('#copy').click(function () {
                var selected = $('[name="survey[]"]:checked').length;

                if (selected === 0) {
                    swal({
                        title: "",
                        text: "Debe seleccionar la encuesta a copiar.",
                        type: "warning", 
                        showCancelButton: false,
                        confirmButtonText: "Aceptar",
                        closeOnConfirm: true
                    });

                    return false;
                }

                copySurvey($('[name="survey[]"]:checked').val());
            });

            $('#download').click(function () {
                var selected = $('[name="survey[]"]:checked').length;

                if (selected === 0) {
                    swal({
                        title: "",
                        text: "Debe seleccionar la encuesta a exportar.",
                        type: "warning", 
                        showCancelButton: false,
                        confirmButtonText: "Aceptar",
                        closeOnConfirm: true
                    });

                    return false;
                }

                location.href = baseUrl + 'surveysgenerator/export?surveyId=' + $('[name="survey[]"]:checked').val();
            });
        });

        $('#mdl-add-edit').on('change', 'select', function () {
            if ($('#month').val() !== '' && $('#year').val() !== '') {
                $('#surveyType').removeAttr('disabled');
            } else {
                $('#surveyType').attr('disabled', 'disabled').val('');
                
                if (structure !== null) {
                    $('#structureTree').jstree('destroy');
                }
            }
            
            if ($('#surveyType').val() === '' || $('#month').val() === '' || $('#year').val() === '') {
                return;
            }

            setTree();
        });
    };

    var showForm = function (data) {
        $('#survey-create-form').trigger('reset');
        $('#mdl-add-edit').empty().html(data).modal('show');

        $('.input-daterange').datepicker({
            format: "dd/mm/yyyy",
            language: 'es',
            autoclose: true
        });
        
        $('.input-group-addon').click(function(e) {
            $(this).siblings('.date-picker').focus();
        }).css({'cursor': 'pointer'});

        validate();
    };

    var validate = function() {
        $('#survey-create-form').validate({
            'ignore': '',
            'messages': {
                'survey.name': {
                    'required': 'Por favor, ingrese el nombre de la encuesta'
                },
                'survey.dateInitial': {
                    'required': 'Por favor, ingrese la fecha inicial'
                },
                'survey.dateEnd': {
                    'required': 'Por favor, ingrese la fecha final'
                },
                'month': {
                    'required': 'Por favor, seleccione el mes'
                },
                'year': {
                    'required': 'Por favor, seleccione el año'
                },
                'survey.surveyType.id': {
                    'required': 'Por favor, seleccione el tipo de encuesta'
                }
            },
            errorPlacement: function(error, element) {
                if (element.next().is('.input-group-addon')) {
                    error.insertAfter(element.next().parent());
                } else {
                    error.insertAfter(element);
                }
            },
            submitHandler: function (form) {
                save(form);
            }
        });
    };

    var save = function(form) {
        var isEdit = $('#id').val() !== '' ? true : false;
        getNodes();

        $.ajax({
            url: $(form).attr('action'),
            data: $(form).serialize(),
            type: 'post',
            dataType: 'json',
            success: function (response) {
                if (response.error) {
                    Liverpool.setMessage($('.inner-message'), response.message, 'danger');
                } else {
                    if (!isEdit) {
                        location.href = baseUrl + 'surveysgenerator/sections?surveyId=' + response.message;
                    } else {
                        oTable.api().ajax.reload();
                        $('#mdl-add-edit').modal('hide');
                    }                        
                }
            },
            beforeSend: function () {
                $('button', $(form)).attr('disabled', 'disabled');
            },
            complete: function () {
                $('button', $(form)).removeAttr('disabled');
            },
            error: function () {
                Liverpool.setMessage($('.inner-message'), 'La encuesta no pudo ser guardada. Por favor, intente nuevamente.', 'warning');
            }
        });
    };

    var deleteSurvey = function (url) {
        $.ajax({
            url: url,
            type: 'delete',
            success: function (data) {
                if (!data.error) {
                    oTable.api().ajax.reload();
                    Liverpool.setMessage($('.message'), 'Encuesta eliminada exitosamente.', 'success');
                } else {
                    Liverpool.setMessage($('.message'), data.message, 'danger');
                }
            },
            error: function () {
                Liverpool.setMessage($('.message'), 'La encuesta no pudor ser eliminada. Por favor, intente nuevamente.', 'warning');
            },
            beforeSend: function() {
                Liverpool.loading("show");
            },
            complete: function () {
                Liverpool.loading("hide");
                Liverpool.scrollTo($('.message'));
            }
        });
    };

    var copySurvey = function (surveyId) {
        $.ajax({
            url: baseUrl + 'surveysgenerator/copy?surveyId=' + surveyId,
            type: 'post',
            success: function (data) {
                if (!data.error) {
                    oTable.api().ajax.reload();
                    Liverpool.setMessage($('.message'), 'Encuesta copiada exitosamente.', 'success');
                } else {
                    Liverpool.setMessage($('.message'), data.message, 'warning');
                }
            },
            error: function () {
                Liverpool.setMessage($('.message'), 'La encuesta no pudor ser copiada. Por favor, intente nuevamente.', 'warning');
            },
            beforeSend: function() {
                Liverpool.loading("show");
            },
            complete: function () {
                Liverpool.loading("hide");
                Liverpool.scrollTo($('.message'));
            }
        });
    };

    var setTable = function(url) {
        oTable = $('.table-lp').dataTable({
            "iDisplayLength": 20,
            "bFilter": true,
            "searchDelay": 500,
            "bLengthChange": false,
            "bAutoWidth": false,
            "bDestroy": true,
            "bSort": false,
            "bInfo": false,
            "oLanguage": {
                "sZeroRecords": "Ningún Registro",
                "sInfo": "Mostrando Encuestas de _START_ al _END_ de _TOTAL_ totales",
                "sInfoEmpty": "0 Encuestas",
                "sPaginatePrevious": "Previous page",
                "sProcessing": "Cargando...",
                "oPaginate": {
                    "sFirst":    "<<",
                    "sLast":     ">>",
                    "sNext":     ">",
                    "sPrevious": "<"
                },
                "sSearch": "Buscar:"
            },
            "fnCreatedRow": function(nRow, aData, iDataIndex) {
                var editLink      = $('#edit-survey').length === 0 ? 
                                        '' : '<a href="' + baseUrl +'surveysgenerator/editSurvey?surveyId=' + aData[6] + '" class="btn btn-sm btn-warning edit" title="Editar"><i class="glyphicon glyphicon-pencil link-glyp"></i></a>';
                var questionsLink = $('#edit-survey').length === 0 ? 
                                        '' : '<a href="' + baseUrl +
                                             (typeof flagNPS !== 'undefined' && flagNPS ? 'SurveyGenNPS' : 'surveysgenerator') +
                                             '/sections?surveyId=' + aData[6] + '" class="btn btn-sm btn-green" title="Preguntas"><i class="glyphicon glyphicon-th-list link-glyp"></i></a>';
                var deleteLink    = $('#delete-survey').length === 0 ? 
                                        '' : '<a href="' + baseUrl +'surveysgenerator/deleteSurvey?surveyId=' + aData[6] + '" class="btn btn-sm btn-red delete" title="Borrar"><i class="glyphicon glyphicon-trash link-glyp"></i></a>';

                $('td:eq(0)', nRow).html('<input value="' + aData[0] + '" type="radio" id="sr-' + aData[0] + '" name="survey[]" />').addClass('text-center');
                $('td:eq(6)', nRow).html(editLink + questionsLink + deleteLink).addClass('actions text-right').attr('nowrap', 'nowrap');
            },
            "bProcessing": false,
            "bServerSide": true,
            "bStateSave" : false,
            "sAjaxSource": baseUrl + 'surveysgenerator/list',
            "pagingType": "full_numbers"
        });
    };

    var setTree = function () {
        if (structure !== null) {
            $('#structureTree').jstree('destroy');
        }

        deselected = [];

        structure = $('#structureTree').on('changed.jstree', function (e, data) {
            if (typeof data.node === 'undefined') {
                return;
            }

            setDeselectedNodes(data.action, data.node.id);
        }).jstree({
            "plugins" : [ "checkbox" ],
            "checkbox": {
                "three_state": false,
                "cascade": ''
            },
            'core' : {
                'check_callback' : true,
                'data' : {
                    'url' : function (node) {
                      return node.id === '#' ?
                        baseUrl + 'surveysgenerator/getRootStructure' :
                        baseUrl + 'surveysgenerator/getStructure';
                    },
                    'data' : function (node) {
                        return { 
                            'id' : node.id,
                            'type': $('#surveyType').val(),
                            'month': $('#month').val(),
                            'year': $('#year').val(),
                            'surveyId': $('#id').val()
                        };
                    }
                }
            }
        });
    };

    var setDeselectedNodes = function (action, id) {            
        if (action === 'deselect_node') {
            deselected.push(id);
        } else {
            deselected.splice(deselected.indexOf(id), 1);
        }
    };

    var getNodes = function () {
        var nodes = $('#structureTree').jstree(true).get_selected();            
        $('#userIds').val(nodes.join(','));
        $('#deselectedUserIds').val(deselected.join(','));
    };

    var init = function() {
        bindEvents();
        setTable();
        validate();
    };

    init();
}();