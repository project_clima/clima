   var z = [];
function fillTable(data){
    z = data;
    $("#table-tracing tbody").html("");
    for(var i = 0; i < z.length; i++) {
        $("#table-tracing tbody").append(
            "<tr style='cursor: pointer;' class='text-center' onclick='openZone(this)'\n\
                    data-row-id='"+z[i].zone+"'>" +
                "<td>" +
                    "Zona " + z[i].zone +
                "</td>" +
                "<td>" +
                    z[i].promotoras +
                "</td>" +
                "<td>" +
                    z[i].detra_pasivas +
                "</td>" +
                "<td>" +
                    z[i].totalRespuestas +
                "</td>" +
                "<td>" +
                    z[i].percentPromo +
                "</td>" +
                "<td>" +
                    z[i].percentDetra +
                "</td>" +
                "<td>" +
                    z[i].seguimiento +
                "</td>" +
                "<td>" +
                    z[i].percentSeg +
                "</td>" +
                "<td>" +
                    z[i].sin_seguimiento +
                "</td>" +
                "<td>" +
                    z[i].numGerentes +
                "</td>" +
                "<td>" +
                    z[i].closed +
                "</td>" +
                "<td>" +
                    "<span class='glyphicon glyphicon-eye-open span-see' data-row-id='"+z[i].zone+"'>" +
                    "</span>" +
                "</td>" +
            "</tr>"
        );
    }

    function openZone(row){
        var id = $(row).data("row-id");
        var $childRow = $("tr[id-child='" + id + "']");
        $("tr[id-child]").remove();

        if($childRow.length > 0) {
            //Hide and delete:
            $childRow.hide(500, function() {
                $childRow.remove();
            });
        } else {
            //Create and show:
            var path = "?rowId=" + id +"&t="+JSON.stringify(zoneStore) +"&firstDate=" + from + "&lastDate=" + to;
            Liverpool.loading("show");
            $.get("/nps/detailStores" + path,
            function(data, textStatus, jqXHR) {
                Liverpool.loading("hide");
                $childRow = $(
                    "<tr style='display: none;' id-child='" + id + "'>" +
                        "<td colspan='19' class='sub-row'>" +
                            data +
                        "</td>" +
                    "</tr>"
                );

                $(row).after($childRow);
                $childRow.show(500);
            });
        }
    }

    //view charts event:
    $(".span-see").click(function() {
        var zone = $(this).data("row-id");
        $(".row-table").fadeOut(100, function() {
            graph(zone);
            $(".row-charts").fadeIn(100);
        });
    });
    Liverpool.loading("hide");
}

   function getRow(arr,zone){
      for( var i = arr.length; i--; ){
          if( arr[i].zone === zone ){
              return i;
          }
      }
      return -1;
  }

function graph(idZone){
    // left chart:
    var i = getRow(z,idZone);
    Highcharts.chart('chart-prom-det', {
        chart: {
            plotBackgroundColor: null,
            plotBorderWidth: null,
            plotShadow: false,
            type: 'pie'
        },
        colors: ["#306898", "#007E33"],
        title: {
            text: 'Promotoras - Detractoras'
        },
        tooltip: {
            pointFormat: '<b>{point.percentage:.1f}%</b>'
        },
        plotOptions: {
            pie: {
                allowPointSelect: true,
                cursor: 'pointer',
                dataLabels: {
                    enabled: true,
                    format: "{y} %"
                },
                showInLegend: true
            }
        },
        series: [{
            name: '',
            colorByPoint: true,
            data: [{
                name: 'Detractoras/Pasivas',
                y: Number(z[i].percentDetra)
            }, {
                name: 'Promotoras',
                y: Number(z[i].percentPromo),
                sliced: true,
                selected: true
            }]
        }]
    });

    // center chart:
    Highcharts.chart('chart-tracing', {
        chart: {
            type: 'column'
        },
        colors: ["#306898"],
        title: {
            text: 'Seguimiento a evaluaciones detractoras'
        },
        subtitle: {
            text: ''
        },
        tooltip: false,
        xAxis: {
            crosshair: true,
        },
        yAxis: {
            min: 0,
            max: 100,
            title: false
        },
        plotOptions: {
            column: {
                pointPadding: 0.2,
                borderWidth: 0,
                dataLabels: {
                    enabled: true,
                    format: "{y} %"
                },
            }
        },
        series: [{
            name: '% de Seguimiento',
            data: Number([z[i].percentSeg])
        }]
    });

    // right chart:
    Highcharts.chart('chart-status', {
        chart: {
            plotBackgroundColor: null,
            plotBorderWidth: null,
            plotShadow: false,
            type: 'pie'
        },
        colors: ["#FF8800", "#306898", "#007E33"],
        title: {
            text: 'Estatus de evaluaciones detractoras'
        },
        tooltip: {
            pointFormat: '<b>{point.percentage:.1f}%</b>'
        },
        plotOptions: {
            pie: {
                allowPointSelect: true,
                cursor: 'pointer',
                dataLabels: {
                    enabled: true,
                    format: "{y} %"
                },
                showInLegend: true
            }
        },
        series: [{
            name: '',
            colorByPoint: true,
            data: [{
                name: 'No contactado',
                y: Number(z[i].percentSinS)
            },{
                name: 'En proceso de solución',
                y: Number(z[i].percentSeg)
            },{
                name: 'Finalizado',
                y: Number(z[i].percentCd),
                sliced: true,
                selected: true
            }]
        }]
    });

}

function getTracings(path){
    var action = '/tracing/getTracings' + path;
    Liverpool.loading("show");
    $.ajax({
        url:action,
        success:function(data){
            fillTable(data);
        }
    });
}

$(document).ready(function() {
    
    //paginate:
//    ut.init("table-tracing", {
//        tableStyling: false,
//        disableSearch: true
//    });
    
    getTracings("");
    $("#back-to-table").click(function() {
        $(".row-charts").fadeOut(100, function() {
            $(".row-table").fadeIn(100);
        });
    });

    $(".row-charts").hide();
});