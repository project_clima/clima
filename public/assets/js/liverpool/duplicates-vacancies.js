var DV = function () {
    var duplicatesTable = null;
    var vacanciesTable = null;
    
    var bindEvents = function () {
        $('#add-duplicate').click(function () {
            Liverpool.loading('show');
            
            $.post(baseUrl + 'duplicatesvacancies/duplicate?period='  + $('#duplicate-period-table').val(), 
            function (data) {
                openModalDuplicate(data);           
            });
        });
        
        $('#add-vacant').click(function () {
            Liverpool.loading('show');
            
            $.post(baseUrl + 'duplicatesvacancies/vacant?period='  + $('#vacant-period-table').val(), 
            function (data) {
                openModalVacant(data);
            });
        });
        
        $('.modal-contents').on('change', '#duplicate-period', function () {
            loadStores($(this).val(), $('#duplicate-location'));
        });
        
        $('.modal-contents').on('change', '#vacant-period', function () {
            loadStores($(this).val(), $('#vacant-location'));
        });

        $('#duplicate-dependants').on('click', '.remove-dependant', function () {
            removeDependant($(this).parent(), 'duplicate');
        });

        $('.modal-contents').on('click', '.remove-dependant', function () {
            removeDependant($(this).parent(), 'vacant');
        });

        $('.modal-contents').on('click', '.add-dependant', function () {
            var type   = $(this).data('type');
            var $input = $('input[data-type="' + type + '"]');
            var id     = $('#' + type + '-id').val();        

            if (id !== '') {        
                addEmployee(id, $input.val(), type);
                $input.val('').focus();
                
                $('#' + type + '-id').val('');
                $('button[data-type="' + type + '"]').attr('disabled', 'disabled');
            }
        });
        
        $('#search-duplicate-form').submit(function (e) {
            e.preventDefault();
            setDuplicatesTable($('#duplicate-period-table').val(), $('#duplicate-search-term').val());
        });
        
        $('#search-vacant-form').submit(function (e) {
            e.preventDefault();
            setVacanciesTable($('#vacant-period-table').val(), $('#vacant-search-term').val());
        });
        
        $('#duplicates-table').on('click', '.edit-duplicate', function () {
            var userId = $(this).data('id');
            
            Liverpool.loading('show');
            
            $.post(baseUrl + 'duplicatesvacancies/duplicate?userId=' + userId, function (data) {
                openModalDuplicate(data, true);
            });
        });
        
        $('#duplicates-table').on('click', '.delete-duplicate', function () {
            var userId = $(this).data('id');
            var period = $('#duplicate-period-table').val();
            swal({
                title: "",
                text: "¿Eliminar duplicado?",
                type: "warning", 
                showCancelButton: true,
                cancelButtonText: "Cancelar",
                confirmButtonText: "Continuar",
                closeOnConfirm: true
            }, function() {
                deleteRow(userId, 'duplicate',period);
            });            
        });
        
        $('#vacancies-table').on('click', '.edit-vacant', function () {
            var userId = $(this).data('id');
            
            Liverpool.loading('show');
            
            $.post(baseUrl + 'duplicatesvacancies/vacant?userId=' + userId, 
            function (data) {
                openModalVacant(data, true);
            });
        });
        
        $('#vacancies-table').on('click', '.delete-vacant', function () {
            var userId = $(this).data('id');
            var period = $('#vacant-period-table').val();
            swal({
                title: "",
                text: "¿Eliminar vacante?",
                type: "warning", 
                showCancelButton: true,
                cancelButtonText: "Cancelar",
                confirmButtonText: "Continuar",
                closeOnConfirm: true
            }, function() {
                deleteRow(userId, 'vacant',period);
            });            
        });
    };
    
    var openModalVacant = function (data, edit) {
        $('#vacancies-modal').empty().html(data);
        $('#mdl-add-vacant').modal('show');

        autocompleteDependants($('#ac-vacant'), 'vacant');
        autocompleteManager('manager-name', 'manager-user-id', 'vacant');
        validate($('#vacancies-form'));
        setSelect2('#vacant-function, #vacant-location');
        Liverpool.loading('hide');
        
        if (!edit) {
            loadStores($('#vacant-period').val(), $('#vacant-location'));
        } else {
            Liverpool.loading('hide');
        }   
    };
    
    var openModalDuplicate = function (data, edit) {
        $('#duplicates-modal').empty().html(data);
        $('#mdl-add-duplicate').modal('show');
        
        autocompleteDependants($('#ac-duplicate'), 'duplicate');
        autocompleteEmployee();
        autocompleteManager('duplicate-manager-name', 'duplicate-manager-id', 'duplicate');
        validate($('#duplicates-form'));
        setSelect2('#duplicate-function, #duplicate-location');
        
        if (!edit) {
            loadStores($('#duplicate-period').val(), $('#duplicate-location'));
        } else {
            Liverpool.loading('hide');
        }                
    };
    
    var validate = function ($form) {
        $form.validate({
            'ignore': '',
            rules: {
                'userId': {
                    'required': true
                },
                'location': {
                    'required': true
                },
                'position': {
                    'required': true
                },
                'function': {
                    'required': true
                },
                'managerUserId': {
                    'required': true
                }
            },
            messages: {
                'userId': {
                    'required': 'Por favor, seleccione al empleado.'
                },
                'location': {
                    'required': 'Por favor, seleccione una ubicación.',
                },
                'position': {
                    'required': 'Por favor, ingrese el puesto.',
                },
                'function': {
                    'required': 'Por favor, seleccione una función.',
                },
                'managerUserId': {
                    'required': 'Por favor, seleccione al jefe.',
                }
            },
            submitHandler: function (form) {
                save($(form));
            }
        });
    };
    
    var loadStores = function (period, $el) {
        var url = baseUrl + 'duplicatesvacancies/getStores?structureDate=' + period;
        
        Liverpool.loading('show');
        $.get(url, function (data) {
            $el.empty().html(data);
            $el.select2("destroy");
            $el.select2();
            Liverpool.loading('hide');
        });
    };
    
    var setSelect2 = function (selector) {
        $(selector).select2({
            allowClear: true
        });
    };
    
    var autocompleteDependants = function ($el, type) {
        $el.autocomplete({
            autoFocus: false,
            delay: 200,
            minLength: 3,
            source: function(request, response) {
                searchEmployees(request, response, type);
            },
            select: function(event, ui) {
                $(this).val(ui.item.text);

                if (ui.item.id !== '') {
                    $('#' + type + '-id').val(ui.item.id);
                    $('button[data-type="' + type +'"]').removeAttr('disabled');
                }            

                return false;
            },
            change: function(event, ui){
                $(this).val($(this).val());
            }
        }).data( "ui-autocomplete" )._renderItem = function(ul, item){
            return $("<li></li>")
                .data('item.autocomplete', item)
                .append("<a>" + item.text + "</a>").appendTo(ul);
        };
    };
    
    var autocompleteEmployee = function () {
        $('#user-id').autocomplete({
            autoFocus: false,
            delay: 200,
            minLength: 3,
            source: function(request, response) {
                searchEmployees(request, response, 'duplicate');
            },
            select: function(event, ui) {
                $(this).val(ui.item.text);

                if (ui.item.id !== '') {
                    $('#duplicate-user-id').val(ui.item.id);
                }            

                return false;
            },
            change: function(event, ui){
                $(this).val($(this).val());
            }
        }).data( "ui-autocomplete" )._renderItem = function(ul, item){
            return $("<li></li>")
                .data('item.autocomplete', item)
                .append("<a>" + item.text + "</a>").appendTo(ul);
        };
    };
    
    var autocompleteManager = function (fielText, fieldHidden, type) {
        $('#' + fielText).autocomplete({
            autoFocus: false,
            delay: 200,
            minLength: 3,
            source: function(request, response) {
                searchEmployees(request, response, type);
            },
            select: function(event, ui) {
                $(this).val(ui.item.text);

                if (ui.item.id !== '') {
                    $('#' + fieldHidden).val(ui.item.id);
                }

                return false;
            },
            change: function(event, ui){
                $(this).val($(this).val());
            }
        }).data( "ui-autocomplete" )._renderItem = function(ul, item){
            return $("<li></li>")
                .data('item.autocomplete', item)
                .append("<a>" + item.text + "</a>").appendTo(ul);
        };
    };
    
    var setDuplicatesTable = function (date, search) {
        date = date || $('#duplicate-period-table').val();
        search = search || $('#duplicate-search-term').val();
        
        if (duplicatesTable !== null) {
            duplicatesTable.fnDestroy();
        }
        
        duplicatesTable = $('#duplicates-table').dataTable({
            "iDisplayLength": 20,
            "bFilter": false,
            "bLengthChange": false,
            "bAutoWidth": false,
            "bDestroy": true,
            "bSort": false,
            "bInfo": false,
            "oLanguage": {
                "sZeroRecords": "Ningún Registro",
                "sInfo": "Mostrando duplicados de _START_ al _END_ de _TOTAL_ totales",
                "sInfoEmpty": "0 Duplicados",
                 "sPaginatePrevious": "Previous page",
                 "sProcessing": "Cargando...",
                 "oPaginate": {
                    "sFirst":    "<<",
                    "sLast":     ">>",
                    "sNext":     ">",
                    "sPrevious": "<"
                }
            },
            "fnCreatedRow": function(nRow, aData, iDataIndex) {
                var actions = '<div class="text-right">' +
                              '      <button class="btn btn-info btn-sm edit-duplicate" data-id="' + aData[5] + '">' +
                              '          <i class="fa fa-pencil"></i>' +
                              '      </button>' +
                              '      <button class="btn btn-danger btn-sm delete-duplicate" data-id="' + aData[6] + '">' +
                              '          <i class="fa fa-trash"></i>' +
                              '      </button>' +
                              '  </div>';
                      
                $('td:eq(5)', nRow).html(actions);
            },
            "bProcessing": true,
            "bServerSide": true,
            "bStateSave" : false,
            "sAjaxSource": baseUrl + 'duplicatesvacancies/listDuplicates?structureDate=' + date + '&search=' + search,
            "pagingType": "full_numbers"
        });
    };
    
    var setVacanciesTable = function (date, search) {
        date = date || $('#vacant-period-table').val();
        search = search || $('#vacant-search-term').val();
        
        if (vacanciesTable !== null) {
            vacanciesTable.fnDestroy();
        }
        
        vacanciesTable = $('#vacancies-table').dataTable({
            "iDisplayLength": 20,
            "bFilter": false,
            "bLengthChange": false,
            "bAutoWidth": false,
            "bDestroy": true,
            "bSort": false,
            "bInfo": false,
            "oLanguage": {
                "sZeroRecords": "Ningún Registro",
                "sInfo": "Mostrando vacantes de _START_ al _END_ de _TOTAL_ totales",
                "sInfoEmpty": "0 Vacantes",
                 "sPaginatePrevious": "Previous page",
                 "sProcessing": "Cargando...",
                 "oPaginate": {
                    "sFirst":    "<<",
                    "sLast":     ">>",
                    "sNext":     ">",
                    "sPrevious": "<"
                }
            },
            "fnCreatedRow": function(nRow, aData, iDataIndex) {
                var actions = '<div class="text-right">' +
                              '      <button class="btn btn-info btn-sm edit-vacant" data-id="' + aData[5] + '">' +
                              '          <i class="fa fa-pencil"></i>' +
                              '      </button>' +
                              '      <button class="btn btn-danger btn-sm delete-vacant" data-id="' + aData[6] + '">' +
                              '          <i class="fa fa-trash"></i>' +
                              '      </button>' +
                              '  </div>';
                      
                $('td:eq(5)', nRow).html(actions);
            },
            "bProcessing": true,
            "bServerSide": true,
            "bStateSave" : false,
            "sAjaxSource": baseUrl + 'duplicatesvacancies/listVacancies?structureDate=' + date + '&search=' + search,
            "pagingType": "full_numbers"
        });
    };
    
    var searchEmployees = function (request, response, type) {
        $.ajax({
            cache: false,
            url: baseUrl + 'duplicatesvacancies/getEmployees',
            type: 'post',
            dataType: 'json',
            data: {
                term: request.term,
                date: function () {
                    return $('#' + type + '-period').val();
                }
            },
            success: function(results) {
                if (results.length === 0) {
                    response([{text: request.term, id: ''}]);
                } else {
                    $("#no-results").empty();
                    response(results);
                }
            }
        });
    };
        
    var addEmployee = function (id, name, type) {        
        $("<li></li>")
            .append(name)
            .append(
                '   <input type="hidden" name="dependants[]" value="' + id + '" />' +
                '   <span class="remove-dependant">' +
                '       <i class="fa fa-times"></i>' +
                '   </span>'
            )
            .addClass('clearfix')
            .appendTo('#' + type + '-dependants');
    };
    
    var save = function ($form) {
        var type = $form.data('type');
        
        $.ajax({
            url: $form.attr('action'),
            data: $form.serialize(),
            type: 'post',
            dataType: 'json',
            beforeSend: function () {
                $('button', $form).attr('disabled', 'disabled');
            },
            success: function (result) {
                Liverpool.setMessage($('.message-' + type), result.message, 'success');
                $('#mdl-add-' + type).modal('hide');
                reloadTable(type);
            },
            error: function () {
                Liverpool.setMessage($('.inner-message-' + type), 'El registro no pudo ser agregado. Por favor, intente nuevamente.', 'warning');
            },
            complete: function () {
                $('button', $form).removeAttr('disabled');
            }
        });
    };
    
    var deleteRow = function (userId, type, period) {
        $.ajax({
            url: baseUrl + 'duplicatesvacancies/deleteDuplicate?userId=' + userId +"&period=" + period,
            type: 'delete',
            dataType: 'json',
            success: function (result) {
                if (!result.error) {
                    reloadTable(type);
                }
                
                Liverpool.setMessage($('.message-' + type), result.message, result.type);
            },
            beforeSend: function () {
                Liverpool.loading('show');
            },
            complete: function () {
                Liverpool.loading('hide');
            }
        });
    };
    
    var reloadTable = function (type) {
        if (type === 'duplicate') {
            duplicatesTable.api().ajax.reload();
        } else {
            vacanciesTable.api().ajax.reload();
        }
    };
    
    var removeDependant = function ($el, type) {
        $el.remove();
        $('#' + type + '-dependants-count').val($('#' + type + '-dependants > li').length);
    };
    
    var init = function () {
        $(function () {
            bindEvents();
            setDuplicatesTable();
            setVacanciesTable();
        });
    };
    
    init();
}();