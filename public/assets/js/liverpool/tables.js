var Table = {};

Table.applyFontColor = function(tableId){
    $(tableId + ' tbody >tr >td:not(:first-child)').each(function(){
        var td = $(this).text();

        if(!isNaN(td)){
            $(this).css('text-align', 'center');
            if(parseFloat(parseFloat(td).toFixed(2))< parseFloat((60).toFixed(2)) && parseFloat(parseFloat(td).toFixed(2)) > parseFloat((0).toFixed(2))){
            	$(this).css('color', '#FF0000');
            }else if(parseFloat(parseFloat(td).toFixed(2)) < parseFloat((80).toFixed(2)) && parseFloat(parseFloat(td).toFixed(2)) >= parseFloat((60).toFixed(2))){
            	$(this).css('color', '#FFBF00');
            }else if(parseFloat(parseFloat(td).toFixed(2)) <= parseFloat((100).toFixed(2)) && parseFloat(parseFloat(td).toFixed(2)) >= parseFloat((80).toFixed(2))){
            	$(this).css('color', '#5FB404');
            }else{
            	$(this).html("--")
            }
        }
    });
}

Table.applyBlueShades = function(tableId) {
    $(tableId + ' tbody tr td.colored-td').each(function(){
        var td = $(this);
        td.css("text-align", "center");
        var t = td.text();
        
        if (t > 0 && t < 71) {
            td.css({
                'background-color': '#7030A0',
                'color': '#fff'
            });
        } else if (t >= 71 && t < 76) {
            td.css({
                'background-color': '#DE0000',
                'color': '#fff'
            });
        } else if (t >= 76 && t < 80) {
            td.css('background-color', '#FFC000');
        } else if (t >= 80 && t < 85) {
            td.css('background-color', '#FFFF00');
        } else if (t >= 85 && t < 91) {
            td.css({
                'background-color': '#00B050',
                'color': '#fff'
            });
        } else if (t >= 91 && t < 96) {
            td.css({
                'background-color': '#008000',
                'color': '#fff'
            });
        } else if (t >= 96) {
           td.css({
                'background-color': '#0070C0',
                'color': '#fff'
            });
        }
    });
};

Table.getTotalFromTableColumn = function(tableId, colIndex) {
    var $t = $(tableId);

    var total = 0;
    $("tbody tr", $t).each(function(i, v) {
        var nbr = $("td:nth-child(" + colIndex + ")", $(v)).text();
        nbr = Number(nbr) || 0;

        total += nbr;
    });

    return total;
};

Table.getAverageFromTableColumn = function(tableId, colIndex) {
    var $t = $(tableId);

    var total = 0;
    var rows = 0;
    $("tbody tr", $t).each(function(i, v) {
        var nbr = $("td:nth-child(" + colIndex + ")", $(v)).text();
        nbr = Number(nbr) || 0;

        total += nbr;
        rows++;
    });

    return total / rows;
};

Table.getData =  function(tableId) {
    var $t = $(tableId);
    var data = [];

    $("tbody tr", $t).each(function(i, v) {
        var $tr = (v);
        var newRow = [];
        data.push(newRow);

        $("td", $tr).each(function(i, v) {
            var val = $(v).text();
            if($.isNumeric(val)) {
                val = Number(val);
            }

            newRow.push(val);
        });
    });

    return data;
};

Table.exportToExcel = function(tableId, template, fileName) {
    var json = JSON.stringify(Table.getData(tableId));
    json = json.replace('"', '\"');

    var form = document.createElement("form");
    form.setAttribute("id", "excel-form");
    form.setAttribute("action", baseUrl + "excelcontroller/generateexcel");
    form.setAttribute("method", "post");
    form.style.display = "none";
    
    $('body #excel-form').remove();
    $('body').append(form);
    
    $(form).append("<input type='hidden' id='json' name='json' value='" + json + "''>");
    $(form).append("<input type='text' id='hidden' name='template' value='" +template + "'>");
    $(form).append(fileName ? '<input type="hidden" id="fileName" name="fileName" value="' + fileName + '">' : "");
    $(form).submit();
};