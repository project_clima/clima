
/*********Variables Globales************/
var zones = {};
var chfs = [];
var strs = [];
var zoneStore = [];
var zoneStoreTmp = [];
var from = "";
var to = "";
var grp = '';
var bsns = '';
var bsns_name = '';
var tN = '';
var tB = '';
var managers = [];
var sections = [];
var type = '';
var _zone = '';
var _zones = [];
var _store = '';
var _store_name = '';
var _man = '';
var _man_name = '';
var _chf_name = '';
var _chf = '';
var _sec = '';
var _sec_name = '';
var _global_area = [];
var _ranking = [0];
var _tRanking = ' ';
var _compare = 0;
var tabs;
/////////////////////////////////////////

/*******change manager button************/
function changeMButton(chb) {
    var chb = $(chb);
    var id = chb.data("manager");
    var idS = chb.data("store-id");
    var idZ = chb.data("zone-id");
    var idC = Number(chb.val());
    var thereAre = thereAreChecked(chb.parent().parent());
    var all = allChecked(chb.parent().parent());
    var v = chb.val();
    var indexZ = find(zoneStore, idZ);
    var indexT = findStore(zoneStore[indexZ].stores, idS);
    var indexM = 0;

    if (!zoneStore[indexZ].stores[indexT].managers) {
        zoneStore[indexZ].stores[indexT].managers = [];
    }

    indexM = findManager(zoneStore[indexZ].stores[indexT].managers, id);

    if (chb.is(":checked")) {

        if (thereAre) {
            //btn.attr("class", "btn btn-info col-xs-12");
            //btnUp.attr("class", "btn btn-xs btn-info m-4 col-xs-12 btn-show-zone");
            if (indexM === -1) {
                zoneStore[indexZ].stores[indexT].managers.push({
                    id: id,
                    chiefs: []
                });
            }
            indexM = findManager(zoneStore[indexZ].stores[indexT].managers, id);
            zoneStore[indexZ].stores[indexT].managers[indexM].chiefs.push({
                id: idC
            });
            if (all) {

                //    btn.attr("class", "btn btn-primary col-xs-12");
                //    btnUp.attr("class", "btn btn-xs btn-primary m-4 col-xs-12 btn-show-zone");

            }
        }
    } else {
        var cIndex = findChief(zoneStore[indexZ].stores[indexT].managers[indexM].chiefs, idC);

        zoneStore[indexZ].stores[indexT].managers[indexM].chiefs.splice(cIndex, 1);
        if (!thereAre) {
            zoneStore[indexZ].stores[indexT].managers.splice(indexM, 1);
            //    btn.attr("class", "btn btn-default col-xs-12");
            //    btnUp.attr("class", "btn btn-xs btn-default m-4 col-xs-12 btn-show-zone");
        } else {

            //    btn.attr("class", "btn btn-info col-xs-12");
            //    btnUp.attr("class", "btn btn-xs btn-info m-4 col-xs-12 btn-show-zone");
        }
    }
}
//////////////////////////////////////////
/*******change manager button************/
function changeSButton(chb) {
    var chb = $(chb);
    var id = chb.data("manager");
    var idS = chb.data("store-id");
    var idZ = chb.data("zone-id");
    var chief = chb.data("chief");
    var idSecS = chb.val();
    var thereAre = thereAreChecked(chb.parent().parent());
    var all = allChecked(chb.parent().parent());
    var v = chb.val();
    var indexZ = find(zoneStore, idZ);
    var indexT = findStore(zoneStore[indexZ].stores, idS);
    var indexM = findManager(zoneStore[indexZ].stores[indexT].managers, id);
    var indexC = findChief(zoneStore[indexZ].stores[indexT].managers[indexM].chiefs, chief);
    var indexSec = 0;

    if (!zoneStore[indexZ].stores[indexT].managers[indexM].chiefs[indexC].sections) {
        zoneStore[indexZ].stores[indexT].managers[indexM].chiefs[indexC].sections = [];
    }

    if (chb.is(":checked")) {

        if (thereAre) {
            //btn.attr("class", "btn btn-info col-xs-12");
            //btnUp.attr("class", "btn btn-xs btn-info m-4 col-xs-12 btn-show-zone");

            zoneStore[indexZ].stores[indexT].managers[indexM].chiefs[indexC].sections.push({
                id: idSecS
            });
            if (all) {

                //    btn.attr("class", "btn btn-primary col-xs-12");
                //    btnUp.attr("class", "btn btn-xs btn-primary m-4 col-xs-12 btn-show-zone");

            }
        }
    } else {
        var indexSec = findChief(zoneStore[indexZ].stores[indexT].
                managers[indexM].chiefs[indexC].sections, idSecS);

        zoneStore[indexZ].stores[indexT].
                managers[indexM].chiefs[indexC].sections.splice(indexSec, 1);

        if (!thereAre) {
            zoneStore[indexZ].stores[indexT].managers[indexM].chiefs[indexC].sections = [];
            //    btn.attr("class", "btn btn-default col-xs-12");
            //    btnUp.attr("class", "btn btn-xs btn-default m-4 col-xs-12 btn-show-zone");
        } else {

            //    btn.attr("class", "btn btn-info col-xs-12");
            //    btnUp.attr("class", "btn btn-xs btn-info m-4 col-xs-12 btn-show-zone");
        }
    }
}
//////////////////////////////////////////

/*********change zone button apply*******/
function changeBtnColor(chb) {
    var chb = $(chb);
    var zone = chb.data("id-zone");
    var index = chb.data("zone");
    var thereAre = thereAreChecked(chb.parent().parent());
    var all = allChecked(chb.parent().parent());
    var btn = $("button[data-zone='" + index + "']");
    var idStore = chb.data("sub-id");
    var desc = chb.data("desc");
    var btnUp = $("#row-zones button[data-id-zone='" + index + "']");
    var contains1 = 0;
    var index = find(zoneStore, zone);

    if (chb.is(":checked")) {
        if (thereAre) {
            btn.attr("class", "btn btn-info col-xs-12");
            btnUp.attr("class", "btn btn-xs btn-info m-4 col-xs-12 btn-show-zone");
            if (index === -1) {
                zoneStore.push({
                    id: zone,
                    stores: []
                });
            }

            index = find(zoneStore, zone);
            zoneStore[index].stores.push({
                id: idStore
            });
            //strs.push(idStore);            
            if (all) {

                btn.attr("class", "btn btn-primary col-xs-12");
                btnUp.attr("class", "btn btn-xs btn-primary m-4 col-xs-12 btn-show-zone");
            }
        }
    } else {
        contains1 = findStore(zoneStore[index].stores, idStore);
        zoneStore[index].stores.splice(contains1, 1);
        if (!thereAre) {
            zoneStore.splice(index, 1);
            btn.attr("class", "btn btn-default col-xs-12");
            btnUp.attr("class", "btn btn-xs btn-default m-4 col-xs-12 btn-show-zone");
        } else {

            btn.attr("class", "btn btn-info col-xs-12");
            btnUp.attr("class", "btn btn-xs btn-info m-4 col-xs-12 btn-show-zone");
        }
    }
}
//////////////////////////////////////////
/***************Fill menu managers*******/
function man(d) {
    var zIndex = 1;

    $('#boss-level-1').html("");
    for (var i = 0; i < d.length; i++) {
        $('#boss-level-1').append(
                '<div class="panel panel-default">' +
                '<div class="panel-heading" data-toggle="collapse" data-parent=\n\
                    "boss-level-1" href="#collapse' + zIndex + '-1-1-1">' +
                '<label data-toggle="collapse" data-parent="boss-level-1" ' +
                ' href="#collapse' + zIndex + '-1-1-1" class="normal-lbl"> ' + d[i].id + '</label>' +
                '</div>' +
                '<div id="collapse' + zIndex + '-1-1-1" class="panel-collapse collapse">' +
                '<!-- ---------------------- -->' +
                '<div class="panel-body">' +
                '<div class="panel-group group-level-2" id="boss-level-2-' + d[i].id.replace(' ','-') + '">' +
                '</div>' +
                '</div>' +
                '<!-- ---------------------- -->' +
                '</div>' +
                '</div>'
                );
        zIndex++;
    }
    zIndex = 1;
    var tIndex = 1;
    for (var i = 0; i < d.length; i++) {

        var stores = d[i].stores;
        for (var j = 0; j < stores.length; j++) {
            
            $('#boss-level-2-' + d[i].id.replace(' ', '-')).append(
                    '<div class="panel panel-default">' +
                    '<div class="panel-heading" data-toggle="collapse" data-parent=\n\
                        "boss-level-2" href="#collapse' + zIndex + "-" + tIndex + '-1-2">' +
                    '<label data-toggle="collapse" data-parent="boss-level-2" ' +
                    'href="#collapse' + zIndex + "-" + tIndex + '-1-2"' +
                    'class="normal-lbl">' + $("input[data-sub-id='" +
                            stores[j].id + "']").data("desc") + '</label>' +
                    '</div>' +
                    '<div id="collapse' + zIndex + "-" + tIndex + '-1-2" class=\n\
                        "panel-collapse collapse">' +
                    '<div class="panel-body">' +
                    '<!-- ---------------------- -->' +
                    '<div class="panel-body">' +
                    '<div class="panel-group group-level-3" id="boss-level-3-' +
                    stores[j].id + '">' +
                    '</div>' +
                    '</div>' +
                    '<!-- ---------------------- -->' +
                    '</div>' +
                    '</div>' +
                    '</div>'
                    );
            tIndex++;
        }
        zIndex++;
    }
    zIndex = 1;
    tIndex = 1;
    var mIndex = 1;
    for (var i = 0; i < d.length; i++) {
        var stores = d[i].stores;
        for (var j = 0; j < stores.length; j++) {
            var managers = stores[j].managers;
            for (var k = 0; k < managers.length; k++) {
                $('#boss-level-3-' + stores[j].id).append(
                        '<div class="panel panel-default">' +
                        '<div class="panel-heading" data-toggle="collapse" \n\
                            data-parent="boss-level-2" href="#collapse' + zIndex +
                        "-" + tIndex + "-" + mIndex + '-3">' +
                        '<label data-toggle="collapse" \n\
                            data-parent="boss-level-2" href="#collapse' + zIndex +
                        "-" + tIndex + "-" + mIndex + '-3" class="normal-lbl">' +
                        managers[k].desc + '</label>' +
                        '<button class="btn btn-xs btn-default btn-all-chb" \n\
                            data-manager="' + managers[k].id + '" ' +
                        ' data-zone-id="' + d[i].id + '" data-store-id="' +
                        stores[j].id + '">Seleccionar todo</button>' +
                        '</div>' +
                        '<div id="collapse' + zIndex + "-" + tIndex + "-" +
                        mIndex + '-3" class="panel-collapse collapse">' +
                        '<div class="panel-body m-8" id="manager-' + managers[k].id + '">' +
                        '</div>' +
                        '</div>' +
                        '</div>'
                        );
                $("button[data-manager='" + managers[k].id + "']").click(function (event) {
                    event.stopPropagation();

                    var btn = $(this);
                    var id = btn.data("manager");
                    var idS = btn.data("store-id");
                    var idZ = btn.data("zone-id");

                    if (btn.hasClass("btn-primary")) {
                        btn.attr("class", "btn btn-xs btn-default btn-all-cnb");
                    } else {
                        btn.attr("class", "btn btn-xs btn-primary btn-all-cnb");
                    }

                    var indexZ = find(zoneStore, idZ);
                    var indexT = findStore(zoneStore[indexZ].stores, idS);
                    var indexM = 0;

                    if (!zoneStore[indexZ].stores[indexT].managers) {
                        zoneStore[indexZ].stores[indexT].managers = [];
                    }
                    indexM = findManager(zoneStore[indexZ].stores[indexT].
                            managers, id);
                    if (btn.hasClass("btn-primary")) {

                        if (indexM === -1) {
                            zoneStore[indexZ].stores[indexT].managers.push({
                                id: id,
                                chiefs: []
                            });
                        } else {
                            zoneStore[indexZ].stores[indexT].managers[indexM].
                                    chiefs = [];
                        }
                        indexM = findManager(zoneStore[indexZ].stores[indexT].
                                managers, id);
                        $("#boss-level-1 div.manager-" + id + " input").
                                prop("checked", "checked");
                        $("#boss-level-1 div.manager-" + id + " input").each(
                                function () {

                                    zoneStore[indexZ].stores[indexT].
                                            managers[indexM].chiefs.push({
                                        id: Number($(this).val())
                                    });
                                }
                        );
                    } else {
                        $("#boss-level-1 div.manager-" + id + " input").
                                removeAttr("checked");
                        zoneStore[indexZ].stores[indexT].managers.splice(indexM, 1);
                    }
                });
                mIndex++;
            }
            tIndex++;
        }
        zIndex++;
    }
    zIndex = 1;
    tIndex = 1;
    var mIndex = 1;
    for (var i = 0; i < d.length; i++) {
        var stores = d[i].stores;
        for (var j = 0; j < stores.length; j++) {
            var managers = stores[j].managers;
            for (var k = 0; k < managers.length; k++) {
                var chiefs = managers[k].chiefs;
                for (var l = 0; l < chiefs.length; l++) {
                    $('#collapse' + zIndex + "-" + tIndex + "-" + mIndex +
                            '-3 #manager-' + managers[k].id).append(
                            '<div class="checkbox manager-' + managers[k].id + '">' +
                            '<input class="m-l-0" type="checkbox" onchange="changeMButton(this)"' +
                            ' data-manager="' + managers[k].id + '" ' +
                            ' data-zone-id="' + d[i].id + '"' +
                            ' data-store-id="' + stores[j].id + '" ' +
                            ' value="' + chiefs[l].id + '"><label>' +
                            chiefs[l].desc + ' - ' + chiefs[l].title + '</label>' +
                            '</div>'
                            );
                }
                mIndex++;
            }
            tIndex++;
        }
        zIndex++;
    }
    $(".managers-row").fadeIn(400);
}
//////////////////////////////////////////
/***********Principal Function***********/
function fillSectionsZones(zones) {
    var zIndex = 1;
    var mIndex = 1;

    $('#section-level-1').html("");

    for (var i = 0; i < zones.length; i++) {

        var stores = zones[i].stores;

        if (stores) {

            for (var j = 0; j < stores.length; j++) {

                var managers = stores[j].managers;

                if (managers) {

                    for (var k = 0; k < managers.length; k++) {

                        var chiefs = managers[k].chiefs;

                        if (chiefs) {

                            for (var l = 0; l < chiefs.length; l++) {

                                $('#section-level-1').append(
                                        '<div class="panel panel-default">' +
                                        '<div class="panel-heading" data-toggle="collapse" \n\
                                        data-parent="section-level-1" href="#section-'
                                        + mIndex + '-1">' +
                                        '<label data-toggle="collapse" \n\
                                        data-parent="section-level-1" href="#section-'
                                        + mIndex + '-1" class="normal-lbl">' + chiefs[l].desc
                                        + '</label>' +
                                        '<button class="btn btn-xs btn-default btn-all-chb" \n\
                                        data-manager="' + managers[k].id + '"' +
                                        ' data-zone-id="' + zones[i].id + '" data-store-id="'
                                        + stores[j].id + '" data-chief="'
                                        + chiefs[l].id + '" ' +
                                        '>Seleccionar todo</button>' +
                                        '</div>' +
                                        '<div id="section-' + mIndex + '-1" \n\
                                        class="panel-collapse collapse">' +
                                        '<div class="panel-body m-8" id="chief-'
                                        + chiefs[l].id + '">' +
                                        '</div>' +
                                        '</div>' +
                                        '</div>'
                                        );

                                $("button[data-chief='" + chiefs[l].id + "']").click(function (event) {
                                    event.stopPropagation();
                                    var btn = $(this);
                                    var id = btn.data("chief");
                                    var idM = btn.data("manager");
                                    var idS = btn.data("store-id");
                                    var idZ = btn.data("zone-id");

                                    if (btn.hasClass("btn-primary")) {
                                        btn.attr("class", "btn btn-xs btn-default btn-all-cnb");
                                    } else {
                                        btn.attr("class", "btn btn-xs btn-primary btn-all-cnb");
                                    }
                                    var indexZ = find(zoneStore, idZ);
                                    var indexT = findStore(zoneStore[indexZ].stores, idS);
                                    var indexM = findManager(zoneStore[indexZ].stores[indexT].managers, idM);
                                    var indexC = findChief(zoneStore[indexZ].stores[indexT].managers[indexM].chiefs, id);
                                    var indexSec = 0;

                                    if (!zoneStore[indexZ].stores[indexT].managers[indexM].chiefs[indexC].sections) {
                                        zoneStore[indexZ].stores[indexT].managers[indexM].chiefs[indexC].sections = [];
                                    }

                                    if (btn.hasClass("btn-primary")) {

                                        zoneStore[indexZ].stores[indexT].managers[indexM].chiefs[indexC].sections = [];

                                        $("#section-level-1 div.chief-" + id + " input").
                                                prop("checked", "checked");
                                        $("#section-level-1 div.chief-" + id + " input").
                                                each(function () {
                                                    zoneStore[indexZ].stores[indexT].
                                                            managers[indexM].chiefs[indexC].sections.push({
                                                        id: $(this).val()
                                                    });
                                                }
                                                );
                                    } else {
                                        $("#section-level-1 div.chief-" + id + " input").
                                                removeAttr("checked");
                                        zoneStore[indexZ].stores[indexT].managers[indexM].chiefs[indexC].sections = [];
                                    }
                                });
                                mIndex++;
                            }
                        }
                    }
                }
            }
        }
    }

    for (var i = 0; i < zones.length; i++) {

        var stores = zones[i].stores;
        if (stores) {
            for (var j = 0; j < stores.length; j++) {
                var managers = stores[j].managers;
                if (managers) {
                    for (var k = 0; k < managers.length; k++) {
                        var chiefs = managers[k].chiefs;
                        if (chiefs) {
                            for (var l = 0; l < chiefs.length; l++) {
                                var secs = chiefs[l].sections;
                                if (secs) {
                                    for (var m = 0; m < secs.length; m++) {
                                        $('#chief-' + chiefs[l].id).append(
                                                '<div class="checkbox chief-' + chiefs[l].id + '">' +
                                                '<input class="m-l-0" type="checkbox" onchange="changeSButton(this)"' +
                                                ' data-chief="' + chiefs[l].id + '" ' +
                                                ' data-manager="' + managers[k].id + '" ' +
                                                ' data-zone-id="' + zones[i].id + '"' +
                                                ' data-store-id="' + stores[j].id + '" ' +
                                                ' value="' + secs[m].id + '"><label>' +
                                                secs[m].desc + ' - ' + secs[m].id + '</label>' +
                                                '</div>'
                                                );
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
    }
    $("#btn-section").fadeIn(400);
}

//////////////////////////////////////////
/****************************************/
function changeTotalZones(btn) {

    var index = 0;
    for (var k in zones) {
        $("button[data-zone='" + index + "']").click();
        index++;
    }
}
//////////////////////////////////////////
/***********Principal Function***********/
function hi2() {
    var index = 0;
    $("#row-zones").html("");

    for (var k in zones) {
        var l = zones[k];
        $("#row-zones").append(
                '<div class="col-xs-3">' +
                '<button data-id-zone="' + index
                + '" class="btn btn-xs btn-default m-4 col-xs-12 btn-show-zone">' +
                l.desc +
                '</button>' +
                '</div>'
                );
        index++;
    }
    $("#row-zones").append(
            '<div class="col-xs-3">' +
            '<div class="checkbox" style="top: -7px;">' +
            '<label><input type="checkbox" id="allStores" \n\
                onchange="changeTotalZones(this)" value="">Ver todas</label>' +
            '</div>' +
            '</div>'
            );
    $("#row-zones").append("<div class='row'></div>");
    index = 0;
    for (var k in zones) {
        var list = zones[k];
        $("#row-zones").append(
                "<div class='form-group'>" +
                "<div style='display: none;' data-text='" + list.desc +
                "' class='col-xs-12 div-zone blue-border' id='zone-" +
                index + "'>" +
                "<button class='btn btn-default col-xs-12' data-id='" +
                list.id + "'data-zone='" + index + "'>" +
                list.stores.length + " (seleccionar todo)" +
                "</button>" +
                "</div>" +
                "</div>"
                );
        $("button[data-zone='" + index + "']").click(function () {
            var btn = $(this);
            var id = btn.data("zone");
            var idZone = btn.data("id");
            var btnUp = $("#row-zones div button[data-id-zone='" + id + "']");
            if (btn.hasClass("btn-primary")) {
                btn.attr("class", "btn btn-default col-xs-12");
                btnUp.attr("class",
                        "btn btn-xs btn-default m-4 col-xs-12 btn-show-zone");
            } else {
                btn.attr("class", "btn btn-primary col-xs-12");
                btnUp.attr("class",
                        "btn btn-xs btn-primary m-4 col-xs-12 btn-show-zone");
            }
            var index = find(zoneStore, idZone);
            if (btn.hasClass("btn-primary")) {

                if (index === -1) {
                    zoneStore.push({
                        id: idZone,
                        stores: []
                    });
                } else {
                    zoneStore[index].stores = [];
                }
                index = find(zoneStore, idZone);
                $("#row-zones div.zone-" + id + " input").prop("checked", "checked");
                $("#row-zones div.zone-" + id + " input").each(function () {

                    zoneStore[index].stores.push({
                        id: Number($(this).val())
                    });
                });
            } else {
                $("#row-zones div.zone-" + id + " input").removeAttr("checked");
                zoneStore.splice(index, 1);
            }
        });
        for (var i = 0; i < list.stores.length; i++) {
            var e = list.stores[i];
            $("#zone-" + index).append(
                    /*
                     '<div class="checkbox col-xs-4 zone-' + index + '">' +
                     '<label class="normal-lbl"><input type="checkbox" onchange="changeBtnColor(this)"' +
                     ' data-zone="' +index + '"' +
                     ' data-sub-id="' + i + '"' +
                     ' value="' + e + '"' +
                     '">' + e + '</label>' +
                     '</div>'
                     */
                    "<div class='col-xs-4 zone-" + index + "'>" +
                    '<input type="checkbox" onchange="changeBtnColor(this)"' +
                    ' data-zone="' + index + '"' +
                    ' data-id-zone="' + list.id + '"' +
                    ' data-desc="' + e.desc + '"' +
                    ' data-sub-id="' + e.id + '" value="' + e.id + '">' +
                    ' <label class="normal-lbl">' + e.desc + '</label>' +
                    '</div>'
                    /*
                     '<div class="checkbox">' +
                     '<label><input type="checkbox" onchange="changeBtnColor(this)"' +
                     ' value="' + e + '" data-zone="' + index +'" data-sub-id="' +
                     i + '">' + e + '</label>' +
                     '</div>'
                     */
                    );
        }
        var zoneHTML = "";
        for (var i = 0; i < list.length; i++) {
            var e = list[i];
            zoneHTML += "<div class='col-xs-6 zone-" + index + "'>" +
                    '<input type="checkbox" onchange="changeBtnColor(this)"' +
                    ' data-zone="' + index + '"' +
                    ' data-sub-id="' + i + '" value="' + e + '">' +
                    ' <label class="normal-lbl">' + e + '</label>' +
                    '</div>';
        }

        $("#zone-" + index).append(zoneHTML);
        index++;
    }

    $(".btn-show-zone").click(function () {
        var _this = $(this);
        var id = _this.data("id-zone");
        var div = $("#row-zones div#zone-" + id);
        $("#row-zones div.div-zone").each(function (i, v) {
            if (div.is($(v)) == false) {
                $(v).hide();
            }
        });
        $(".btn-show-zone").css("font-weight", "normal");
        if (div.is(":hidden")) {
            div.fadeIn(400);
            _this.css("font-weight", "bold");
        } else {
            div.hide();
            _this.css("font-weight", "normal");
        }
    });
//    for (var i = 0; i < managers.length; i++) {
//        var e = managers[i];
//        $("#row-managers").append(
//                '<div class="checkbox">' +
//                '<label><input type="checkbox" value="' + e + '">' + e + '</label>' +
//                '</div>'
//                );
//    }

//    for(var i = 0; i < sections.length; i++) {
//        var e = sections[i];
//        var val = Liverpool.pad((i + 1), 3);
//
//        $("#row-sections").append(
//            '<div class="checkbox">' +
//              '<label><input type="checkbox" value="' + val + '">' + val + " - " + e + '</label>' +
//            '</div>'
//        );
//    }
    //Liverpool.loading("hide");
    $(".zone-row").fadeIn(400);
}
//////////////////////////////////////////
/******Utilery functions*****************/
function thereAreChecked(panel) {
    var thereAre = false;
    $("input[type='checkbox']", panel).each(function (i, v) {
        if ($(v).is(":checked")) {
            thereAre = true;
        }
    });
    return thereAre;
}
//////////////////////////////////////////
function allChecked(panel) {
    var all = true;
    $("input[type='checkbox']", panel).each(function (i, v) {
        if (!$(v).is(":checked")) {
            all = false;
        }
    });
    return all;
}
/****************************************/
function loadingShow() {
    $('#groups-t-nps').fadeOut(200);
    $('#groups-t-nps-loading').show();
    $('.load-nps').fadeIn(200);
}
//////////////////////////////////////////
/****************************************/
function loadingHide() {
    if ($('#groups-t-nps').css('display') == 'none') {
        $('#groups-t-nps-loading').hide();
        $('#groups-t-nps').fadeIn(200);
    }
    $('.load-nps').hide();
}
//////////////////////////////////////////
function loadingHideG() {
    $('#groups-t-nps').hide();
    $('#groups-t-nps-loading').hide();
    $('.load-nps').hide();
}
//////////////////////////////////////////
/*********Days in months*****************/
function daysInMonth(month, year) {
    return new Date(year, month, 0).getDate();
}
//////////////////////////////////////////
/*********Remove element*****************/
function remove(arr, item) {
    for (var i = arr.length; i--; ) {
        if (arr[i] === item) {
            arr.splice(i, 1);
        }
    }
}
//////////////////////////////////////////  
function find(arr, item) {
    for (var i = arr.length; i--; ) {
        if (arr[i].id === item) {
            return i;
        }
    }
    return -1;
}
//////////////////////////////////////////  
function findStore(arr, store) {
    for (var i = arr.length; i--; ) {
        if (arr[i].id === store) {
            return i;
        }
    }
    return -1;
}
//////////////////////////////////////////
function findManager(arr, manager) {
    for (var i = arr.length; i--; ) {
        if (arr[i].id === manager) {
            return i;
        }
    }
    return -1;
}
//////////////////////////////////////////
function findChief(arr, chief) {
    for (var i = arr.length; i--; ) {
        if (arr[i].id === chief) {
            return i;
        }
    }
    return -1;
}
//////////////////////////////////////////

/////////////////////////////////////////
function findElement(arr, element) {
    for (var i = 0; i < arr.length; i++) {
        if (arr[i] == element) {
            return i;
        }
    }
    return -1;
}
/****************************************/
function containsArea(arr, element) {
    for (var i = 0; i < arr.length; i++) {
        if (arr[i].desc === element) {
            return i;
        }
    }
    return -1;
}
/////////////////////////////////////////
/****************************************/
function containsAreaD(arr, element) {
    for (var i = 0; i < arr.length; i++) {
        if (arr[i] === element) {
            return i;
        }
    }
    return -1;
}
/////////////////////////////////////////
function onlyZones() {
    var zones = [];
    for (var k in zoneStore) {
        zones.push({
            id: zoneStore[k].id
        });
    }
    return zones;
}
//////////////////////////////////////////
/****************************************/
function resetFilters() {

    if ($('.zone-row').css('display') == 'block') {
        $('.zone-row').hide();
    }
    if ($('.managers-row').css('display') == 'block') {
        $('.managers-row').hide();
    }
    if ($('.section-row').css('display') == 'block') {
        $('.section-row').hide();
    }
}
/////////////////////////////////////////

/****************************************/
function resizeSpaceLinesChart() {
    var div = $('#hNps');

    if (div.hasClass('col-sm-9')) {
        div.attr("class", "col-xs-12 col-sm-5");
    } else {
        div.attr("class", "col-xs-12 col-sm-9");
    }

    $("#loading-top-tables").show();
    
    if ( typeof  mainLineChart !== 'undefined') {
        mainLineChart.reflow();
    }
    
    if ( typeof mainLineChartTrim !== 'undefined') {
        mainLineChartTrim.reflow();
    }
}
//////////////////////////////////////////
/********Document Ready******************/
$(document).ready(function () {

    from = localStorage.getItem("from") ? localStorage.getItem("from") : "";
    to = localStorage.getItem("to") ? localStorage.getItem("to") : "";
    
    var period  = localStorage.getItem("period") ? localStorage.getItem("period") : "";
    var context = window.location.pathname.split('/');
    
    if (context[2] === 'evaluations') {
        createGroups('?firstDate=' + from + '&lastDate=' + to );
    }

    $("input[name='date-kind']").change(function (e) {
        e.stopPropagation();

        var v = $(this).val();

        if (v == "trim") {

            $("#row-range").fadeOut(200, function () {
                $("#row-trim").fadeIn(200);
            });

        } else if (v == "range") {

            $("#row-trim").fadeOut(200, function () {
                $("#row-range").fadeIn(200);
            });

        }
    });

    $("#apply-date").click(function () {
        $('.dropdown-date').removeClass('open');

        var context = window.location.pathname.split('/');
        var div = fwt.query(".fwt-tab-active-up", false, document);
        var option = div.textContent;

        if (option == 'Trimestral') {

            var year = $("#date-year").data('datepicker')
                    .getFormattedDate('yyyy');
            var div2 = fwt.query("button.btn.btn-xs.btn-primary.btn-long"
                    , false, document);
            var months = div2.textContent.trim().split('-');
            var m = 0;
            var m2 = 0;

            if (months[0].trim() == 'Ene') {
                m = '01';
            } else if (months[0].trim() == 'Abr') {
                m = '04';
            } else if (months[0].trim() == 'Jul') {
                m = '07';
            } else if (months[0].trim() == 'Oct') {
                m = '10';
            }
            if (months[1].trim() == 'Mar') {
                m2 = '03';
            } else if (months[1].trim() == 'Jun') {
                m2 = '06';
            } else if (months[1].trim() == 'Sep') {
                m2 = '09';
            } else if (months[1].trim() == 'Dic') {
                m2 = '12';
            }
            from = '01/' + m + '/' + year;
            to = daysInMonth(m2, year) + '/' + m2 + '/' + year;

        } else if (option == 'Mensual') {

            var date = $("#date-and-month").data('datepicker').
                    getFormattedDate('mm/yyyy');
            var dateS = date.split("/");
            var days = daysInMonth(dateS[0], dateS[1]);
            from = '01' + '/' + date;
            to = days + '/' + date;

        } else if (option == 'Periodo') {

            from = $("#date-from").data('datepicker').
                    getFormattedDate('dd/mm/yyyy');
            to = $("#date-to").data('datepicker').
                    getFormattedDate('dd/mm/yyyy');
        }
        
        localStorage.setItem("from", from);
        localStorage.setItem("to", to);
        localStorage.setItem("period",option);
        
        if (typeof Tracking !== 'undefined') {
            Tracking.changeFilterDate(from, to, option);
            return;
        }

        if (context[2] === 'evaluations') {
            
            changeDates();
        } else if (context[2] === 'application') {
            resetWithDates();
            
        } else if (context[2] === 'historyreport') {

            getHistoryReport('?' + path)

        }
        
        $('#regM').html(option);
        $('#range').html(from + ' - ' + to);
    });

    $("#apply-zone").click(function () {
        $('.dropdown').removeClass('open');

        flow = '';
        var context = window.location.pathname.split('/');
        localStorage.setItem("compare", JSON.stringify(zoneStore));
        localStorage.setItem("comparativa", '| Comparativa Ubicaciones');
        if (context[2] == 'evaluations') {

            strs = [];

            for (var i = 0; i < zoneStore.length; i++) {
                var stores = zoneStore[i].stores;
                if (stores) {
                    for (var j = 0; j < stores.length; j++) {
                        strs.push(stores[j].id);
                    }
                }
            }

            getEvsStores();

        } else if (context[2] == 'application') {

            var path = '?firstDate=' + from + '&lastDate=' + to +
                    '&group=' + grp + '&business=' + bsns + '&op=' +
                    question + '&type=' + tB;

            cleanScreen();
            loadingShow();

            //resetArrays();   

            Liverpool.loading("show");
            updateLine(tB, 'business', 'Comparativa Ubicación');
            _compare = 0;

            getNpsArray(path);

            resizeSpaceLinesChart();

            getNpsTop('?firstDate=' + from + '&lastDate=' + to
                    + '&business=' + bsns
                    + '&op=' + question);
            getNpsGlobalArea(path);

            fillSelect(1);

            strs = [];

            getNpsDetailStore('?firstDate=' + from + '&lastDate=' + to +
                    '&zNps=' + JSON.stringify(zoneStore) + '&business=' + bsns
                    + '&op=' + question + '&group=' + grp);

            getManagersChiefs("?zNps=" + JSON.stringify(zoneStore));

        }
    });
    $("#apply-managers").click(function () {

        $(".dropdown").removeClass("open");

        var context = window.location.pathname.split('/');
        localStorage.setItem("compare", JSON.stringify(zoneStore));
        localStorage.setItem("comparativa", '| Comparativa Jefes');
        if (context[2] == 'evaluations') {
            
            chfs = [];
            
            for (var i = 0; i < zoneStore.length; i++) {
                var stores = zoneStore[i].stores;
                if (stores) {
                    for (var j = 0; j < stores.length; j++) {
                        var managers = stores[j].managers;
                        if (managers) {
                            for (var k = 0; k < managers.length; k++) {
                                var chiefs = managers[k].chiefs;
                                if (chiefs) {
                                    for (var l = 0; l < chiefs.length; l++) {
                                        chfs.push(chiefs[l].id);
                                    }
                                }
                            }
                        }
                    }
                }
            }
            getEvsChiefs();
        } else if (context[2] == 'application') {

            var path = '?firstDate=' + from + '&lastDate=' + to + '&group=' + grp
                    + '&business=' + bsns + '&op=' + question + '&type=' + tN;

            cleanScreen();
            loadingShow();

            //resetArrays();     
            Liverpool.loading("show");

            updateLine(tB, 'business', 'Comparativa Jefes');
            _compare = 1;

            flow = 'Compare';

            getNpsArray(path);
            resizeSpaceLinesChart();

            getNpsTop('?firstDate=' + from + '&lastDate=' + to + '&business=' +
                    bsns + '&op=' + question);

            getNpsGlobalArea(path);
            compareChiefs('?firstDate=' + from + '&lastDate=' + to + '&zNps=' +
                    JSON.stringify(zoneStore) + '&business=' + bsns +
                    '&op=' + question + '&group=' + grp);

            createSections("?zNps=" + JSON.stringify(zoneStore));
            
        }
    });
    $("#apply-section").click(function () {

        $(".dropdown").removeClass("open");

        var context = window.location.pathname.split('/');
        localStorage.setItem("compare", JSON.stringify(zoneStore));
        localStorage.setItem("comparativa", '| Comparativa Secciones');
        if (context[2] == 'evaluations') {
            
            sections = [];
            
            for (var i = 0; i < zoneStore.length; i++) {
                var stores = zoneStore[i].stores;
                if (stores) {
                    for (var j = 0; j < stores.length; j++) {
                        strs.push(stores[j].id);
                    var managers = stores[j].managers;
                    if (managers) {
                        for (var k = 0; k < managers.length; k++) {
                            var chiefs = managers[k].chiefs;
                            if (chiefs) {
                                for (var l = 0; l < chiefs.length; l++) {
                                    var secs = chiefs[l].sections;
                                    if (secs) {
                                        for (var m = 0; m < secs.length; m++) {
                                            sections.push(secs[m].id);
                                        }
                                    }
                                }
                            }
                        }
                    }
                    }
                }
            }
            getEvsSections();

        } else if (context[2] == 'application') {
            
            var path = '?firstDate=' + from + '&lastDate=' + to + '&group=' + grp
                + '&business=' + bsns + '&op=' + question + '&type=' + tN;
            
            cleanScreen();
            loadingShow();

            flow = 'Compare';

            //resetArrays();     
            Liverpool.loading("show");

            updateLine(tB, 'business', 'Comparativa Seccion');
            _compare = 2;

            getNpsArray(path);
            resizeSpaceLinesChart();

            getNpsTop('?firstDate=' + from + '&lastDate=' + to + '&business=' +
                    bsns + '&op=' + question);

            getNpsGlobalArea(path);
            compareSections('?firstDate=' + from + '&lastDate=' + to + '&zNps=' +
                    JSON.stringify(zoneStore) + '&business=' + bsns +
                    '&op=' + question + '&group=' + grp);

            //$("#btn-section").fadeIn(400);
        }

    });

    tabs = fwt.init("tabs-dates", {
        width: 380,
        height: 140,
        fixedTabs: true,
        centerTabs: true
    });

    
    var y = new Date().getFullYear();
    var m = new Date().getMonth();
    
    $("#date-year").datepicker({

        format: " yyyy", // Notice the Extra space at the beginning
        viewMode: "years",
        minViewMode: "years",
        language: 'es'

    }).css("text-align", "center").datepicker('setDate', new Date());
    $(".btns-trim button").click(function () {
        $(".btns-trim button").each(function (i, v) {
            $(v).attr("class", "btn btn-default btn-xs btn-long");
        });
        $(this).attr("class", "btn btn-primary btn-xs btn-long");
    });

    if (context[2] === 'historyreport') {
        $("#date-from").datepicker({

            format: "dd/mm/yyyy",
            language: 'es',
            todayHighlight: true,
            autoclose: true

        }).datepicker('setDate', '01/01/' + y);
    }else{
        $("#date-from").datepicker({

            format: "dd/mm/yyyy",
            language: 'es',
            todayHighlight: true,
            autoclose: true

        }).datepicker('setDate', '01/' + Number(m + 1) + '/' + y);
    }
    
    if (context[2] === 'historyreport') {
        $("#date-to").datepicker({

            format: "dd/mm/yyyy",
            language: 'es',
            todayHighlight: true,
            autoclose: true

        }).datepicker('setDate', '31/12/' + y);
    }else{
        $("#date-to").datepicker({

            format: "dd/mm/yyyy",
            language: 'es',
            todayHighlight: true,
            autoclose: true

        }).datepicker('setDate', new Date());
    }

    $("#date-and-month").datepicker({

        format: "mm/yyyy",
        language: 'es',
        autoclose: true,
        startView: "months",
        minViewMode: "months"

    }).css("text-align", "center").datepicker('setDate', new Date());
    
    if (context[2] === 'historyreport') {
        tabs[2].click();
    }
    
    if(period !== null && period !== "") {
        /*
        var tabs = fwt.init("tabs-dates", {
            width: 380,
            height: 140,
            fixedTabs: true,
            centerTabs: true
        });
        */

        if(period === "Trimestral") {
            var dt = from.split("/");
            var m = new Date(dt[1]+"/"+dt[0]+"/"+dt[2]).getMonth();
            $("#date-year").datepicker('setDate',new Date(from));
            $(".btns-trim button").each(function (i, v) {
                $(v).attr("class", "btn btn-default btn-xs btn-long");
            });
            if( (m+1) > 0 && (m+1) < 4 ) {
                $("#filter-date-quarter-Q1").attr("class", "btn btn-primary btn-xs btn-long");
            } else if( (m+1) > 3 && (m+1) < 7 ) {
                $("#filter-date-quarter-Q2").attr("class", "btn btn-primary btn-xs btn-long");
            } else if( (m+1) > 6 && (m+1) < 10 ) {
                $("#filter-date-quarter-Q3").attr("class", "btn btn-primary btn-xs btn-long");
            } else if( (m+1) > 9 && (m+1) < 13 ) {
                $("#filter-date-quarter-Q4").attr("class", "btn btn-primary btn-xs btn-long");
            }
            
            tabs[0].click();
        } else if(period === "Mensual") {
            var fields = from.split("/");
            $("#date-and-month").datepicker('setDate',new Date(fields[1]+"/"+fields[0]+"/"+fields[2]));
            tabs[1].click();
            
        } else if(period === "Periodo") {
            if(from !== "" && to !== "") {
                $('date-from').datepicker('setDate',from);
                $('date-to').datepicker('setDate',to);
            }
            tabs[2].click();
        }
        $('#regM').html(period);
        $('#range').html(from + ' - ' + to);
    }
    
    $("#btn-serv").click(function () {
        $(this).attr("class", "btn btn-primary btn-xs");
        $("#btn-recom").attr("class", "btn btn-default btn-xs");
    });

    $("#btn-recom").click(function () {
        $(this).attr("class", "btn btn-primary btn-xs");
        $("#btn-serv").attr("class", "btn btn-default btn-xs");
    });

    $(".date-input").datepicker().on('changeDate', function (ev) {
        $('.datepicker').fadeOut(400);
    });

    $(".btn-all-chb").click(function (e) {

        e.stopPropagation();

        if ($(this).attr("all-checked")) {
            $("input[type=checkbox]", $(this).parent().parent()).
                    removeAttr("checked");
            $(this).removeAttr("all-checked");
            $(this).text("Seleccionar todo");
            $(this).addClass("btn-default").removeClass("btn-primary");
        } else {
            $("input[type=checkbox]", $(this).parent().parent()).
                    prop("checked", "checked");
            $(this).attr("all-checked", "all-checked");
            $(this).text("Remover todo");
            $(this).addClass("btn-primary").removeClass("btn-default");
        }
    });

    $("#side-bar-nps .dropdown-toggle").unbind("click").on('click', function (event) {
        $('.dropdown-toggle').parent().removeClass('open');
        $(this).parent().toggleClass('open');
    });
    
    $('#side-bar-nps .dropdown-toggle').on('click', function () {
        $('.dropdown-toggle').parent().removeClass('open');
        $(this).parent().toggleClass('open');
    });

    $('body').unbind("click").on('click', function (e) {
        if (!$('.li-nps-menu, .special-menu').is(e.target) 
            && $('.li-nps-menu, .special-menu').has(e.target).length === 0 
            && $('.open').has(e.target).length === 0
        ) {
            $('.li-nps-menu').parent().parent().removeClass('open');
            $('.special-menu').parent().removeClass('open');
        }
    });

    /*
    $(".li-nps-menu").click(function(event) {
        event.stopPropagation();
    });
    $(".managers-li").click(function() {
        event.stopPropagation();
    });
    */
});
