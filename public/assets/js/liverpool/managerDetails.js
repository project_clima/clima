
var mTable = null;

function openChief(row) {
    var id      = $(row).data("id-man");
    var name    = $(row).data("man-name");
    var zone    = $(row).data("id-zone");
    var store   = $(row).data("id-store");
    _man        = id;
    _man_name   = name;
    //_zone       = zone;
    _store      = store;
    managers    = [];
    managers.push(id);
    //Create and show:
    var path = "?man=" + id + "&zNps=" + JSON.stringify(zoneStore) + "&firstDate="
                + from + "&lastDate=" + to + '&store=' + store + '&business=' 
                + bsns + '&op=' + question + '&group=' + grp;

    Liverpool.loading("show");
    
    cleanScreen();
    loadingShow();
    
    tN = name;

    getNpsArray('?firstDate=' + from + '&lastDate=' + to + '&zNps=' +
                JSON.stringify(zoneStore) + '&store=' + store + '&group=' + grp +
                '&op=' + question + '&man=' + id + '&business=' + bsns + 
                '&type=' + tN);
    resizeSpaceLinesChart();
    
    getNpsTop('?firstDate=' + from + '&lastDate=' + to + '&group=' + grp +
            '&op=' + question + '&business=' + bsns);
    getNpsGlobalArea(path);
    updateLine(tN, 'gerente','');
    
    chfs        = [];
    sections    = [];
    
    datailChiefNps(path);
}

function findArea(arr, item) {
    for (var i = arr.length; i--; ) {
        if (arr[i].id === item) {
            return i;
        }
    }
    return -1;
}

function fillManagers(data) {

    $(".table-nps-store").hide();

    var npsMan  = data;
    var ap      = '';
    
    if( mTable !== null ){
        mTable.destroy();
        //rTable.clear();
        $("#manager-table").empty();
        $("#manager-table").append("<tbody></tbody>");
    }
    //Main table:
    
    var headers = [];
    headers.push({'title': 'Rank'},
            {'title': 'Ubicación'},
                {'title': 'Gerente'},
                 {'title': 'NPS Total'},
                 {'title': 'Tendencia'});
//    $("#manager-table").html("");
//    
//    $("#manager-table").append("<thead></thead>" +
//                             "<tbody></tbody>");
//                     
//    ap ="<tr>" +
//            "<th>" +
//                "Almacen" +
//            "</th>" +
//            "<th>"+
//                "Gerente"+
//            "</th>"+
//            "<th>" +
//                "NPS Total" +
//            "</th>" +
//                "<th style='border-right: 10px solid #F7F7F8;'>Tendencia</th>" +
//            "</th>";
    for (var i = 0; i < _global_area.length; i++) {
        headers.push({'title':_global_area[i].desc});
    }
//    ap += "</tr>";
//    $("#manager-table thead").append(ap);

    ap = '';
    $("#manager-table tbody").html("");
    for (var i = 0; i < npsMan.length; i++) {
        //var score = (Math.floor(Math.random() * 20 + 80)) + ".00";
        var clazz = "score green-score";
        var arrow = "up";
        
        if (Number(npsMan[i].nps) > 70 && Number(npsMan[i].nps) < 90) {
            clazz = "score yellow-score";
        } else if (Number(npsMan[i].nps) < 70) {
            clazz = "score red-score";
        }
        
        var dif = Math.abs(Number(npsMan[i].nps) - Number(npsMan[i].npsP));

        if (Number(npsMan[i].nps) < Number(npsMan[i].npsP)) {
            if(dif <= 0.50) {
               
                arrow = "glyphicon glyphicon-pause equals score amber-score";
            }else{
                
                arrow = "down";
            }
        }else if(Number(npsMan[i].nps) === Number(npsMan[i].npsP)) {
            
            arrow = "glyphicon glyphicon-pause equals score amber-score";
        }else if(Number(npsMan[i].nps) > Number(npsMan[i].npsP)) {
            if(dif <= 0.50) {
                
                arrow = "glyphicon glyphicon-pause equals score amber-score";
            }
        }
        ap = "<tr style='cursor: pointer; '" +
                " data-id-man='"
                + npsMan[i].id + "'" + "data-id-store='" + npsMan[i].idParent + "'" +
                " data-man-name='" + npsMan[i].desc + "'"
                + " onclick='openChief(this)'>" +
                "<td>" +
                "</td>" +
                "<td>" +
                npsMan[i].idParent + ' - ' +  npsMan[i].parent+
                "</td>" +
                "<td>" +
                npsMan[i].desc +
                "</td>" +
                "<td>" +
                "<span class='" + clazz + "'>" +
                Number(npsMan[i].nps).toFixed(2) +
                "</span>" +
                "</td>" +
                "<td style='border-right: 10px solid #F7F7F8;'>" +
                "<span class='glyphicon glyphicon-arrow-" + arrow
                + " " + clazz + "'>" +
                "</span>" +
                "</td>";

        for (var j = 0; j < _global_area.length; j++) {
            var f = findArea(npsMan[i].areas, _global_area[j].id);
            if (f !== -1) {
                var claz = "score green-score";
                if (Number(npsMan[i].areas[f].nps) > 70 && Number(npsMan[i].areas[f].nps) < 90) {
                    claz = "score yellow-score";
                } else if (Number(npsMan[i].areas[f].nps) < 70) {
                    claz = "score red-score";
                }
                ap = ap + "<td>" +
                        "<span class='" + claz + "'>" +
                        Number(npsMan[i].areas[f].nps).toFixed(2) +
                        "</span>" +
                        "</td>";
            } else {
                ap = ap + "<td>" +
                        "<span>" +
                        "</span>" +
                        "</td>";
            }
        }
        managers.push(npsMan[i].id);
        ap = ap + "</tr>";
        $("#manager-table tbody").append(ap);
    }

    mTable = $('#manager-table').DataTable({
        "dom": '<<<"col-xs-12 col-md-6"f><"col-xs-12 col-md-6"l>><t>ip>',
        columns: headers,
        "oLanguage": {
            "sZeroRecords": "Ningún Registro",
            "sInfo": "Mostrando del _START_ al _END_ de _TOTAL_",
            "sInfoEmpty": "0 Eventos",
            "sPaginatePrevious": "Previous page",
            "sProcessing": "Cargando...",
            "sLengthMenu": "Mostrar _MENU_ registros",
            "sSearch": "Busqueda:",
            "oPaginate": {
                "sFirst": "<<",
                "sLast": ">>",
                "sNext": ">",
                "sPrevious": "<"
            }
        },
        destroy:true,
        //retrieve: true,
        "lengthMenu": [[30, 500, 100, -1], [30, 50, 100, "Todos"]]
    });

    mTable.on('order.dt search.dt', function () {
             mTable.column(0, { search: 'applied', order: 'applied' }).nodes().each(function (cell, i) {
                 cell.innerHTML = i + 1;
             });
         }).draw();

    $(".table-nps-manager").show();
    Liverpool.loading("hide");
}

