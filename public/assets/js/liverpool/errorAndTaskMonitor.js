$(document).ready(function() {
    for(var i = 0; i< 32; i++) {
        $("#table-results tbody").append(
            $("<tr>" +
                "<td>" +
                    "12/09/2016" +
                "</td>" +
                "<td>" +
                    "BD Resultados" +
                "</td>" +
                "<td>" +
                    "200" +
                "</td>" +
                "<td>" +
                    "Finalizado sin errores" +
                "</td>" +
                "<td>" +
                    "Carga finalizada correctamente" +
                "</td>" +
                "<td>" +
                    "CBENITEZ" +
                "</td>" +
            "</tr>").css("cursor", "pointer").click(function() {
                BootstrapDialog.show({
                    size: BootstrapDialog.SIZE_WIDE,
                    title: "Detalle",
                    message: $("<div></div>").load("/ErrorAndTaskMonitor/detail"),
                    buttons: [{
                        label: "<span class='glyphicon glyphicon-download'></span>",
                        cssClass: "btn-primary",
                        action: function(modal) {
                            modal.close();
                        }
                    }, {
                        label: "<span class='glyphicon glyphicon-repeat'></span>",
                        cssClass: "btn-warning",
                        action: function(modal) {
                            modal.close();
                        }
                    }, {
                        label: "<span class='glyphicon glyphicon-remove'></span>",
                        cssClass: "btn-danger",
                        action: function(modal) {
                            modal.close();
                        }
                    }]
                })
            })
        );
    }

    ut.init("table-results", {
        tableStyling: false,
        disableSearch: true
    })
});
