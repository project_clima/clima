
var sTable = null;

function openManager(row) {
    var id = $(row).data("str-id");
    var name = $(row).data("str-name");
    var zone = $(row).data("id-zone");

    _store = id;
    _store_name = name;
    strs = [];
    strs.push(id);
    //Create and show:
    var path = "?store=" + id + "&zNps=" + JSON.stringify(zoneStore)
            + "&firstDate=" + from + "&lastDate=" + to + '&business=' + bsns +
            '&op=' + question + '&group=' + grp;

    Liverpool.loading("show");

    cleanScreen();
    loadingShow();

    tN = name;
    getNpsArray('?firstDate=' + from + '&lastDate=' + to + '&zNps=' +
            JSON.stringify(zoneStore) + '&group=' + grp + '&op=' + question +
            '&store=' + id + '&business=' + bsns + '&type=' + tN);
    getNpsTop('?firstDate=' + from + '&lastDate=' + to + '&group=' + grp +
            '&op=' + question + '&business=' + bsns);

    resizeSpaceLinesChart();
    getNpsGlobalArea(path);
    updateLine(tN, 'almacen', "");

    managers    = [];
    chfs        = [];
    sections    = [];

    detailManagerNps(path);
}

function findArea(arr, item) {
    for (var i = arr.length; i--; ) {
        if (arr[i].id === item) {
            return i;
        }
    }
    return -1;
}

function fillStores(data) {

    //$(".table-nps-zone").hide();

    //Stores table:
    var npsTable = data;
    var ap = '';
    //Main table:

    if (sTable !== null) {
        sTable.destroy();
        //rTable.clear();
        $("#stores-table").empty();
        $("#stores-table").append("<tbody></tbody>");
    }
    //Main table:

    var headers = [];
    headers.push({'title': 'Rank'},
            {'title': 'Zona'},
            {'title': 'Ubicación'},
            {'title': 'NPS Total'},
            {'title': 'Tendencia'});
//    $("#stores-table").html("");
//    $("#stores-table").append("<thead></thead>" +
//            "<tbody></tbody>");
//    ap = "<tr>" +
//            "<th>" +
//            "Zona" +
//            "</th>" +
//            "<th>" +
//            "Almacen"+
//            "</th>"+
//            "<th>" +
//            "NPS Total" +
//            "</th>" +
//            "<th style='border-right: 10px solid #F7F7F8;'>Tendencia</th>" +
//            "</th>";
    for (var i = 0; i < _global_area.length; i++) {
        headers.push({'title': _global_area[i].desc});
    }
//    ap += "</tr>";
//    $("#stores-table thead").append(ap);
//    ap = '';

    for (var i = 0; i < npsTable.length; i++) {
        //var score = (Math.floor(Math.random() * 20 + 80)) + ".00";
        var clazz = "score green-score";
        var arrow = "up";
        
        if (Number(npsTable[i].nps) > 70 && Number(npsTable[i].nps) < 90) {
            clazz = "score yellow-score";
        } else if (Number(npsTable[i].nps) < 70) {
            clazz = "score red-score";
        }
        
        var dif = Math.abs(Number(npsTable[i].nps) - Number(npsTable[i].npsP));

        if (Number(npsTable[i].nps) < Number(npsTable[i].npsP)) {
            if(dif <= 0.50) {
                
                arrow = "glyphicon glyphicon-pause equals score amber-score";
            }else{
                
                arrow = "down";
            }
        }else if(Number(npsTable[i].nps) === Number(npsTable[i].npsP)) {
            
            arrow = "glyphicon glyphicon-pause equals score amber-score";
        }else if(Number(npsTable[i].nps) > Number(npsTable[i].npsP)) {
            if(dif <= 0.50) {
                
                arrow = "glyphicon glyphicon-pause equals score amber-score";
            }
        }
        
        ap = "<tr style='cursor: pointer;' data-str-id='" + npsTable[i].id + "'" +
                " data-id-zone='" + npsTable[i].idParent + "'" +
                " data-str-name='" + npsTable[i].desc + "'" +
                " onclick='openManager(this)'>" +
                "<td>" +
                "</td>" +
                "<td>" +
                npsTable[i].idParent +
                "</td>" +
                "<td>" +
                npsTable[i].id + ' - ' + npsTable[i].desc +
                "</td>" +
                "<td>" +
                "<span class='" + clazz + "'>" +
                Number(npsTable[i].nps).toFixed(2) +
                "</span>" +
                "</td>" +
                "<td style='border-right: 10px solid #F7F7F8;'>" +
                "<span class='glyphicon glyphicon-arrow-" + arrow + " " + clazz
                + "'>" +
                "</span>" +
                "</td>";

        for (var j = 0; j < _global_area.length; j++) {
            var f = findArea(npsTable[i].areas, _global_area[j].id);
            if (f !== -1) {
                var claz = "score green-score";
                if (Number(npsTable[i].areas[f].nps) > 70 && Number(npsTable[i].areas[f].nps) < 90) {
                    claz = "score yellow-score";
                } else if (Number(npsTable[i].areas[f].nps) < 70) {
                    claz = "score red-score";
                }
                ap = ap + "<td>" +
                        "<span class='" + claz + "'>" +
                        Number(npsTable[i].areas[f].nps).toFixed(2) +
                        "</span>" +
                        "</td>";
            } else {
                ap = ap + "<td>" +
                        "<span'>" +
                        "</span>" +
                        "</td>";
            }
        }

        strs.push(npsTable[i].id);
        ap = ap + "</tr>";
        $("#stores-table tbody").append(ap);
    }

    sTable = $('#stores-table').DataTable({
        "dom": '<<<"col-xs-12 col-md-6"f><"col-xs-12 col-md-6"l>><t>ip>',
        columns: headers,
        "oLanguage": {
            "sZeroRecords": "Ningún Registro",
            "sInfo": "Mostrando del _START_ al _END_ de _TOTAL_",
            "sInfoEmpty": "0 Eventos",
            "sPaginatePrevious": "Previous page",
            "sProcessing": "Cargando...",
            "sLengthMenu": "Mostrar _MENU_ registros",
            "sSearch": "Busqueda:",
            "oPaginate": {
                "sFirst": "<<",
                "sLast": ">>",
                "sNext": ">",
                "sPrevious": "<"
            }
        },
        "lengthMenu": [[30, 500, 100, -1], [30, 50, 100, "Todos"]]
    });

    sTable.on('order.dt search.dt', function () {
             sTable.column(0, { search: 'applied', order: 'applied' }).nodes().each(function (cell, i) {
                 cell.innerHTML = i + 1;
             });
         }).draw();

    $(".table-nps-store").show();
}
