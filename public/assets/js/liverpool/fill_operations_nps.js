var groupTable = null;

function loadData() {
    
    from = localStorage.getItem("from") ? localStorage.getItem("from") : "";
    to = localStorage.getItem("to") ? localStorage.getItem("to") : "";
    zoneStore = JSON.parse(localStorage.getItem("compare")) ? 
                    JSON.parse(localStorage.getItem("compare")) : [];
    grp = localStorage.getItem("group") ? localStorage.getItem("group") : "";
    bsns = localStorage.getItem("business") ? localStorage.getItem("business") : "";

    var group = localStorage.getItem("groupN");
    var business = localStorage.getItem("businessN");
    var comparativa = localStorage.getItem("comparativa");

   //        Liverpool.loading("show");
//        getNpsArray('?firstDate=' + from + '&lastDate=' + to + '&op=' + question);
//        createGroups('?firstDate=' + from + '&lastDate=' + to + '&op=' + question);

        if (grp !== null && grp !== '') {
            //filter = "{'groupId': " + groupId + "}";
            //updateBreadcrumb('company', null);
            tN = group;
            updateLine(group, 'group', 'less');
        }

        if (bsns !== null && bsns !== '') {
            //filter =  "{'groupId': " + groupId + ", 'businessId': " + businessId + "}";
            //findBusiness();
            tN = business;
            updateLine(business, 'business', '');
        }

//            if (zoneStore !== null && zoneStore.length > 0) {
//                updateBreadcrumb('comparativa', null);
//            }

        if (grp !== '' && bsns === '') {

            //return loadTable('group');
        } else if (grp !== '' && bsns !== '' ) {


            //return loadTable('business');
        }
//            else if (grp !== '' && bsns !== '' && zoneStore.length > 0) {
//                
//                return loadTable('comparativa');
//            }

    loadTable();

}

function loadTable(level) {

    var currentLevel = level || 'company';

    if (bsns !== null && bsns !== '') {
        $('#npsT label:nth-last-child(1) a').click();
    } else if (grp !== null && grp !== '') {
        $('#npsT label:nth-last-child(1) a').click();
    } else {
        Liverpool.loading("show");
        getNpsArray('?firstDate=' + from + '&lastDate=' + to + '&op=' + question);
        createGroups('?firstDate=' + from + '&lastDate=' + to + '&op=' + question);
    }

}

function fillBusiness(data) {
    //fillPrincipal(data);
    fillGroupRigthTable(data, "Negocio");

    Liverpool.loading("hide");

    $(".groups-table-nps").fadeIn(400);
}
//////////////////////////////////////////
/*****************************************/
function fillPrincipal(groups) {

    Liverpool.loading("hide");

    _global_area = groups;

    var totalG = groups.length;
    var nDivs = parseInt(totalG / 4) + 1;
    var indice = 0;
    var index = 0;

    $('#groups-t-nps').html("");

    for (var i = 0; i < nDivs; i++) {
        $('#groups-t-nps').append('<div class="col-xs-6 form-group" id="num-'
                + i + '"></div>');
    }

    for (var k in groups) {
        var l = groups[k];

        index = parseInt(indice / 4);

        var clazz = "score green-score";
        var arrow = "up";

        if (Number(l.nps) > 70 && Number(l.nps) < 90) {
            clazz = "score yellow-score";
        } else if (Number(l.nps) < 70) {
            clazz = "score red-score";
        }
//        if (Number(l.nps) < 90) {
//            clazz = "score orange-score";
//            arrow = "down";
//        }

//        var dif = Math.abs(Number(npsTable[i].nps) - Number(npsTable[i].npsP));
//
//        if (Number(npsTable[i].nps) < Number(npsTable[i].npsP)) {
//            if(dif <= 0.50) {
//                clazz = "score black-score";
//                arrow = "glyphicon glyphicon-pause equals score amber-score";
//            }else{
//                clazz = "score orange-score";
//                arrow = "down";
//            }
//        }else if(Number(npsTable[i].nps) === Number(npsTable[i].npsP)) {
//            clazz = "score black-score";
//            arrow = "glyphicon glyphicon-pause equals score amber-score";
//        }else if(Number(npsTable[i].nps) > Number(npsTable[i].npsP)) {
//            if(dif <= 0.50) {
//                clazz = "score black-score";
//                arrow = "glyphicon glyphicon-pause equals score amber-score";
//            }
//        }

        div2 = '<div class="col-xs-12 col-sm-3">' +
                '<div class="panel panel-default">' +
                '<div class="panel-heading small-panel">' + l.desc + '</div>' +
                '<div class="panel-body small-body">' +
                '<span class="big ' + clazz + '" >' + Number(l.nps).toFixed(2) + '</span>' +
//                '<span class="glyphicon glyphicon-arrow-' + arrow + ' ' + clazz
//                + '"></span>' +
                '</div>' +
                '</div>' +
                '</div>';


        $('#groups-t-nps #num-' + index).append(div2);
        indice++;
    }

}
//////////////////////////////////////////
/*****************************************/
function fillGroupRigthTable(groups, type) {

    var npsTableG = groups;
    var ap = '';
    var areasN = [];
    var head = '';

    
    if (groupTable !== null) {
        groupTable.destroy();
        //rTable.clear();
            //Main table:
        $("#groups-right-table").html("");
        $("#groups-right-table").append("<thead></thead>" +
                "<tbody></tbody>");
    }

    ap = "<tr>" +
            "<th>" +
            "Rank" +
            "</th>" +
            "<th>" +
            type +
            "</th>" +
            "<th>" +
            "NPS Total" +
            "</th>" +
            "<th class='table-g'>Tendencia</th>" +
            "</th>";

    //areasN.push("No.");

    for (var i = 0; i < npsTableG.length; i++) {

        if (npsTableG[i].areas) {
            for (var j = 0; j < npsTableG[i].areas.length; j++) {
                if (containsAreaD(areasN,npsTableG[i].areas[j].desc) === -1) {
                    ap = ap + "<th>" + npsTableG[i].areas[j].desc + "</th>"
                    areasN.push(npsTableG[i].areas[j].desc);
                }
            }
        }
        if (type === 'Negocio') {
            break;
        }
    }

    ap += "</tr>";
    $("#groups-right-table thead").append(ap);

    ap = '';

    for (var i = 0; i < npsTableG.length; i++) {
        //var score = (Math.floor(Math.random() * 20 + 80)) + ".00";
        var clazz = "score green-score";
        var arrow = "up";

        if (Number(npsTableG[i].nps) > 70 && Number(npsTableG[i].nps) < 90) {
            clazz = "score yellow-score";
        } else if (Number(npsTableG[i].nps) < 70) {
            clazz = "score red-score";
        }

        var dif = Math.abs(Number(npsTableG[i].nps) - Number(npsTableG[i].npsP));

        if (Number(npsTableG[i].nps) < Number(npsTableG[i].npsP)) {
            if (dif <= 0.50) {
                
                arrow = "glyphicon glyphicon-pause equals score amber-score";
            } else {
                
                arrow = "down";
            }
        } else if (Number(npsTableG[i].nps) === Number(npsTableG[i].npsP)) {
            
            arrow = "glyphicon glyphicon-pause equals score amber-score";
        } else if (Number(npsTableG[i].nps) > Number(npsTableG[i].npsP)) {
            if (dif <= 0.50) {
                
                arrow = "glyphicon glyphicon-pause equals score amber-score";
            }
        }

        if (type === 'Grupo') {
            head = "<tr style='cursor: pointer;'" + "data-group-name='" + npsTableG[i].desc + "'" +
                    " data-group-id='" + npsTableG[i].id + "'" +
                    "' onclick='changeBtnGroup(this)'>";
        } else {
            head = "<tr style='cursor: pointer;'" + "data-busns-name='" + npsTableG[i].desc + "'" +
                    "data-busns-id='" + npsTableG[i].id + "'" +
                    "' onclick='changeBtnBusiness(this)'>";
        }
        ap = head +
                "<td>" +
                "</td>" +
                "<td>" +
                npsTableG[i].desc +
                "</td>" +
                "<td>" +
                "<span class='" + clazz + "'>" +
                Number(npsTableG[i].nps).toFixed(2) +
                "</span>" +
                "</td>" +
                "<td style='border-right: 10px solid #F7F7F8;'>" +
                "<span class='glyphicon glyphicon-arrow-" + arrow + " " +
                clazz + "'>" +
                "</span>" +
                "</td>";

        if (npsTableG[i].areas) {
            var k = 0;
            var j = 0;

            for (j = 0; j < areasN.length; j++) {

                var f = containsArea(npsTableG[i].areas, areasN[j]);

                if (f > -1) {

                    //for (k = 0; k < npsTableG[i].areas.length; k++) {
                        //var f = findArea(npsTableG.areas[i].areas, _global_area[j].id);
                        //if (f !== -1) {
                        var claz = "score green-score";

                        if (Number(npsTableG[i].areas[f].nps) > 70 && Number(npsTableG[i].areas[f].nps) < 90) {
                            claz = "score yellow-score";
                        } else if (Number(npsTableG[i].areas[f].nps) < 70) {
                            claz = "score red-score";
                        }
                        ap = ap + "<td>" +
                                "<span class='" + claz + "'>" +
                                Number(npsTableG[i].areas[f].nps).toFixed(2) +
                                "</span>" +
                                "</td>";
                    //}
                } else {
                    ap = ap + "<td>" +
                            "<span'>" +
                             "</span>" +
                            "</td>";
                }
                j += k;
            }
        }

        ap = ap + "</tr>";
        $("#groups-right-table tbody").append(ap);
    }
    
        groupTable = $('#groups-right-table').DataTable({
            "dom": '<<<"col-xs-12 col-md-6"f><"col-xs-12 col-md-6"l>><t>ip>',
            columnDefs: [
                { orderable: false, className: 'table-g', targets: 3 },
                { orderable: true, targets: '_all' }
            ],
            "bPaginate": false,
            "bFilter": false,
            "bLengthChange": false,
            "bInfo": false,
            "oLanguage": {
                "sZeroRecords": "Ningún Registro",
                "sInfo": "Mostrando del _START_ al _END_ de _TOTAL_",
                "sInfoEmpty": "0 Eventos",
                "sPaginatePrevious": "Previous page",
                "sProcessing": "Cargando...",
                
                "oPaginate": {
                    "sFirst": "<<",
                    "sLast": ">>",
                    "sNext": ">",
                    "sPrevious": "<"
                }
            }            
        });
        
        groupTable.on('order.dt search.dt', function () {
             groupTable.column(0, { search: 'applied', order: 'applied' }).nodes().each(function (cell, i) {
                 cell.innerHTML = i + 1;
             });
         }).draw();
        
    loadingHideG();
}

///////////////////////////////////////////
