var secTable = null;

function openSellers(row) {

    var id      = $(row).data("id-sec");
    var name    = $(row).data("sec-name");
    var zone    = $(row).data("id-zone");
    var chief   = $(row).data("id-chief");
    var store   = $(row).data("id-store");
    _sec        = id;
    _sec_name   = name;
    sections    = [];
    sections.push(id);

    var path = "?section=" + id + "&zNps=" + JSON.stringify(zoneStore) +
            "&firstDate=" + from + "&lastDate=" + to + '&store=' + store +
            '&business=' + bsns + '&op=' + question + '&group=' + grp;

    Liverpool.loading("show");

    cleanScreen();
    loadingShow();

    tN = id;

    getNpsArray('?firstDate=' + from + '&lastDate=' + to + '&zNps=' +
            JSON.stringify(zoneStore) + '&group=' + grp + '&op=' + question +
            '&store=' + store + '&section=' + id + '&business=' + bsns +
            '&type=' + tN);
    getNpsTop('?firstDate=' + from + '&lastDate=' + to + '&group=' + grp +
            '&op=' + question + '&business=' + bsns);
    resizeSpaceLinesChart();
    getNpsGlobalArea(path);

    updateLine(tN, 'seccion','');

    detailSellersNps(path);
}

function findArea(arr, item) {

    for (var i = arr.length; i--; ) {
        if (arr[i].id === item) {
            return i;
        }
    }

    return -1;
}

function fillSections(data) {
    $(".table-nps-chief").hide();

    var npsSection = data;
    var ap = '';

    if (secTable !== null) {
        secTable.destroy();
        //rTable.clear();
        $("#section-table").empty();
        $("#section-table").append("<tbody></tbody>");
    }
    //Main table:

    var headers = [];
    headers.push({'title': 'Rank'},
            {'title': 'Ubicación'},
            {'title': 'Sección'},
            {'title': 'NPS Total'},
            {'title': 'Tendencia'});
//    ap = "<tr>" +
//            "<th>" +
//            "Almacen" +
//            "</th>" +
//            "<th>"+
//            "Seccción"+
//            "</th>"+
//            "<th>" +
//            "NPS Total" +
//            "</th>" +
//            "<th style='border-right: 10px solid #F7F7F8;'>Tendencia</th>" +
//            "</th>";

    for (var i = 0; i < _global_area.length; i++) {
        headers.push({'title': _global_area[i].desc});
    }

//    ap += "</tr>";
//    
//    $("#section-table thead").append(ap);
//    
//    ap = '';

    for (var i = 0; i < npsSection.length; i++) {
        //var score = (Math.floor(Math.random() * 20 + 80)) + ".00";
        var clazz = "score green-score";
        var arrow = "up";
        
        if (Number(npsSection[i].nps) > 70 && Number(npsSection[i].nps) < 90) {
            clazz = "score yellow-score";
        } else if (Number(npsSection[i].nps) < 70) {
            clazz = "score red-score";
        }
        var dif = Math.abs(Number(npsSection[i].nps) - Number(npsSection[i].npsP));

        if (Number(npsSection[i].nps) < Number(npsSection[i].npsP)) {
            if(dif <= 0.50) {
                
                arrow = "glyphicon glyphicon-pause equals score amber-score";
            }else{
                
                arrow = "down";
            }
        }else if(Number(npsSection[i].nps) === Number(npsSection[i].npsP)) {
            
            arrow = "glyphicon glyphicon-pause equals score amber-score";
        }else if(Number(npsSection[i].nps) > Number(npsSection[i].npsP)) {
            if(dif <= 0.50) {
                
                arrow = "glyphicon glyphicon-pause equals score amber-score";
            }
        }

        ap = "<tr style='cursor: pointer; '" + "'" + " data-id-sec='"
                + npsSection[i].id + "'" + " data-sec-name='"
                + npsSection[i].desc + "'" + "data-id-store='" + npsSection[i].idStore + "'"
                + " onclick='openSellers(this)'>" +
                "<td>" +
                "</td>" +
                "<td>" +
                npsSection[i].idStore + ' - ' + npsSection[i].storeDesc +
                "</td>" +
                "<td>" +
                (typeof npsSection[i].desc !== 'undefined' ? npsSection[i].desc + '-' + npsSection[i].id : npsSection[i].id) +
                "</td>" +
                "<td>" +
                "<span class='" + clazz + "'>" +
                Number(npsSection[i].nps).toFixed(2) +
                "</span>" +
                "</td>" +
                "<td style='border-right: 10px solid #F7F7F8;'>" +
                "<span class='glyphicon glyphicon-arrow-" + arrow + " "
                + clazz + "'>" +
                "</span>" +
                "</td>";

        for (var j = 0; j < _global_area.length; j++) {

            var f = findArea(npsSection[i].areas, _global_area[j].id);

            if (f !== -1) {
                var claz = "score green-score";

                if (Number(npsSection[i].areas[f].nps) > 70 && Number(npsSection[i].areas[f].nps) < 90) {
                    claz = "score yellow-score";
                } else if (Number(npsSection[i].areas[f].nps) < 70) {
                    claz = "score red-score";
                }

                ap = ap + "<td>" +
                        "<span class='" + claz + "'>" +
                        Number(npsSection[i].areas[f].nps).toFixed(2) +
                        "</span>" +
                        "</td>";
            } else {
                ap = ap + "<td>" +
                        "<span>" +
                        "</span>" +
                        "</td>";
            }
        }

        sections.push(npsSection[i].id);

        ap = ap + "</tr>";

        $("#section-table tbody").append(ap);
    }

    secTable = $('#section-table').DataTable({
        "dom": '<<<"col-xs-12 col-md-6"f><"col-xs-12 col-md-6"l>><t>ip>',
        columns: headers,
        "oLanguage": {
            "sZeroRecords": "Ningún Registro",
            "sInfo": "Mostrando del _START_ al _END_ de _TOTAL_",
            "sInfoEmpty": "0 Eventos",
            "sPaginatePrevious": "Previous page",
            "sProcessing": "Cargando...",
            "sLengthMenu": "Mostrar _MENU_ registros",
            "sSearch": "Busqueda:",
            "oPaginate": {
                "sFirst": "<<",
                "sLast": ">>",
                "sNext": ">",
                "sPrevious": "<"
            }
        },
        "lengthMenu": [[30, 500, 100, -1], [30, 50, 100, "Todos"]]
    });
    
    secTable.on('order.dt search.dt', function () {
             secTable.column(0, { search: 'applied', order: 'applied' }).nodes().each(function (cell, i) {
                 cell.innerHTML = i + 1;
             });
         }).draw();
    
    $(".table-nps-section").show();
    Liverpool.loading("hide");
}
