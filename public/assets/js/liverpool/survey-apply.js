var SurveyApply = function(answerMode, url) {
    var maxPage         = 1;
    var currentProgress = 0;
    var $form           = null;
    
    var bindEvents = function() {
        $.extend($.validator.messages, {
            required: "Aún no contestas esta pregunta"
        });
        
        $('#start-survey').click(function() {
            $('#page-start').hide();
            $('#title-progress').removeClass('hidden').show();
            $('#page-1').removeClass('hidden').show();
            
            $('html, body').animate({scrollTop: '0px'}, 750);
        });

        $('.next-page').click(function() {
            var currentPage = $(this).data('current');
            var nextPage    = $(this).data('next');
            var isLastPage  = parseInt(currentPage) === $('.page').length;

            $form.validate().settings.ignore = ":disabled, :hidden";

            if ($form.valid()) {
                if (!isLastPage) {
                    $('#page-' + currentPage).hide();
                    $('#page-' + nextPage).removeClass('hidden').show();
                }
                
                save($('#page-' + currentPage), isLastPage);
                
                progress(currentPage);
            }
        });

        $('.prev-page').click(function() {
            var current = $(this).data('current');
            var prev    = $(this).data('prev');

            $('#page-' + current).hide();
            $('#page-' + prev).removeClass('hidden').show();

            if (prev === 'start') {
                $('#title-progress').hide();
            }
        });
        
        $('#close-survey, .close-survey').click(function(e) {
            e.preventDefault();
            var redirect  = $(this).attr('href');
            var logoutUrl = $(this).data('logout-url');
            
            closeSession(logoutUrl, redirect);
        });
    };
    
    var setValidator = function() {
        $form = $('#survey-form');
        
        $form.validate({
            'ignore': '',
            errorPlacement: function (error, element) {
                var parent = '';

                if ((element.prop('type') === 'checkbox' || element.prop('type') === 'radio') && !element.hasClass('faces')) {
                    error.insertAfter(element.closest('.choices'));
                } else if (element.hasClass('faces')) {
                    parent = element.parent().parent().parent().parent().parent();
                    error.insertAfter($('.face-question > .error-faces', parent));
                }  else {
                    error.insertAfter(element);
                }
            }
        });
    };
    
    var progress = function(currentPage) {
        maxPage         = currentPage > maxPage ? currentPage : maxPage;
        currentProgress = (parseInt(maxPage) * 100) / $('.page').length;

        $('#current-survey-password').text(currentProgress.toFixed(0));
        $('#survey-progress .progress-bar').css({'width': currentProgress + '%'});
        $('#survey-progress .progress-bar').attr('aria-valuenow', currentProgress);
        
        $('html, body').animate({scrollTop: '0px'}, 750);
    };
    
    var save = function($page, isLastPage) {
        if (answerMode === 'single') {
            savePartial($page, isLastPage);
        } else if (answerMode === 'shared' && isLastPage) {
            saveComplete($page);
        }
    };

    var savePartial = function($page, isLastPage) {
        var $next = $('.next-page', $page);
        
        var data = $('input[type=text], input[type=hidden],' +
                     'input[type=radio]:checked, input[type=checkbox]:checked,' +
                     'textarea, select', $page
                    ).serialize();
            
        data += isLastPage ? '&close=true' : '';
        data += '&username=' + $('input[name="username"]').val();
        data += '&userId=' + $('input[name="userId"]').val();
        
        $.ajax({
            url: url,
            type: 'post',
            dataType: 'json',
            data: data,
            success: function() {
                $('#page-' + $next.data('current')).hide();
                $('#page-' + $next.data('next')).removeClass('hidden').show();
            },
            beforeSend: function() {
                toggleDisable($('button.btn-survey'));
            },
            complete: function() {
                toggleDisable($('button.btn-survey'));
            }
        });
    };
    
    var saveComplete = function($page) {
        var $next = $('.next-page', $page);
        
        var data = $('input[type=text], input[type=hidden],' +
                     'input[type=radio]:checked, input[type=checkbox]:checked,' +
                     'textarea, select', $form
                    ).serialize();

        $.ajax({
            url: url,
            type: 'post',
            dataType: 'json',
            data: data + '&close=true',
            success: function(data) {
                if (data.error && data.message === 'Maximun attempts reached') {
                    showAlertPlan();
                } else {
                    $('#page-' + $next.data('current')).hide();
                    $('#page-' + $next.data('next')).removeClass('hidden').show();
                }
            },
            beforeSend: function() {
                toggleDisable($('button.btn-survey'));
                loading('show');
            },
            complete: function() {
                toggleDisable($('button.btn-survey'));
                loading();
            }
        });
    };
    
    var closeSession = function(logoutUrl, redirect) {
        $.get(logoutUrl, function () {
            location.href = redirect;
        });
    };
    
    var toggleDisable = function(el) {
        $.each(el, function(){
            if ($(this).is(':disabled')) {
                $(this).removeAttr('disabled');
            } else {
                $(this).attr('disabled', 'disabled');
            }
        });
    };
    
    var showAlertPlan = function () {
        swal({
            title: "",
            text: "Mientras usted respondía la encuesta el plan fue alcanzado por lo cual ya no podrá ser enviada. " +
                  "Haga clic en Salir para abandonar la encuesta o en Continuar para permanecer en la misma.",
            type: "warning", 
            showCancelButton: true,
            cancelButtonText: "Continuar",
            confirmButtonText: "Salir",
            closeOnConfirm: true
        }, function() {
            closeSession('/secure/logout', 'http://www.liverpool.com.mx');
        });
    };
    
    var init = function() {
        setValidator();
        bindEvents();
    };
    
    var loading = function(action, msg) {
        if(action && action === "show") {
            $("#loading-sign-msg").text(msg || "Guardando");
            $(".loading-div").fadeIn(250);
        } else {
            $(".loading-div").fadeOut(250);
        }
    };
    
    return {
        init: init
    };
};