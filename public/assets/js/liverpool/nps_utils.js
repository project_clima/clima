/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

var rTable      = null;
var rightTable  = null;

function openStores(row) {

    var id = $(row).data("row-id");
    var g = $(row).data("grp-id");
    var b = $(row).data("bsn-id");
    _zone = id;
    _zones = [];
    _zones.push(id);
    //Create and show:
    var path = "?zone=" + id + "&zNps=" + JSON.stringify(zoneStore)
            + "&firstDate=" + from + "&lastDate=" + to +
            '&business=' + bsns + '&op=' + question + '&group=' + grp;

    Liverpool.loading("show");

    tN = id;

    cleanScreen();
    loadingShow();

    getNpsArray('?firstDate=' + from + '&lastDate=' + to + '&zNps=' +
            JSON.stringify(zoneStore) + '&group=' + grp + '&op=' + question +
            '&zone=' + id + '&business=' + bsns + '&type=' + tN);

    getNpsTop('?firstDate=' + from + '&lastDate=' + to + '&group=' + grp +
            '&op=' + question + '&business=' + bsns);
    resizeSpaceLinesChart();

    strs = [];
    managers = [];
    chfs = [];
    sections = [];

    getNpsGlobalArea(path);
    updateLine(tN, 'zona', "");
    getNpsDetailStore(path);
}

function fillTop(data) {

    npsTop = data;
    var dataStoreHighest = [];
    var dataStoreLowest = [];
    var dataManagerHighest = [];
    var dataManagerLowest = [];
    var dataSectionHighest = [];
    var dataSectionLowest = [];

    if (npsTop.topDesc.stores.length > 0) {
        for (var i = 0; i < npsTop.topDesc.stores.length; i++) {
            dataStoreHighest.push({
                name: npsTop.topDesc.stores[i].desc,
                score: npsTop.topDesc.stores[i].nps
                
            });
        }
    }
    if (npsTop.topAsc.stores.length > 0) {
        for (var i = 0; i < npsTop.topAsc.stores.length; i++) {
            dataStoreLowest.push({
                name: npsTop.topAsc.stores[i].desc,
                score: npsTop.topAsc.stores[i].nps
                
            });
        }
    }


    if (npsTop.topDesc.managers.length > 0) {
        for (var i = 0; i < npsTop.topDesc.managers.length; i++) {
            dataManagerHighest.push({
                name: npsTop.topDesc.managers[i].desc,
                score: npsTop.topDesc.managers[i].nps
                
            });
        }
    }
    if (npsTop.topAsc.managers.length > 0) {
        for (var i = 0; i < npsTop.topAsc.managers.length; i++) {
            dataManagerLowest.push({
                name: npsTop.topAsc.managers[i].desc,
                score: npsTop.topAsc.managers[i].nps
                
            });
        }
    }

    if (npsTop.topDesc.sections.length > 0) {
        for (var i = 0; i < npsTop.topDesc.sections.length; i++) {
            dataSectionHighest.push({
                name: npsTop.topDesc.sections[i].desc,
                score: npsTop.topDesc.sections[i].nps
                
            });
        }
    }
    if (npsTop.topAsc.sections.length > 0) {
        for (var i = 0; i < npsTop.topAsc.sections.length; i++) {
            dataSectionLowest.push({
                name: npsTop.topAsc.sections[i].desc,
                score: npsTop.topAsc.sections[i].nps
                
            });
        }
    }

    $("#menu1 table > tbody").html("");
    for (var i = 0; i < dataStoreHighest.length; i++) {

        var e = dataStoreHighest[i];
        var arrow = "glyphicon glyphicon-arrow-up score green-score";
        var dif = Math.abs(Number(e.score) - Number(e.scoreP));

        var claz = "score green-score";

        if (Number(e.score) > 70 && Number(e.score) < 90) {
            claz = "score yellow-score";
        } else if (Number(e.score) < 70) {
            claz = "score red-score";
        }

        $("#menu1 table > tbody").append(
                "<tr style='font-size: 10px; font-weight: bold;'>" +
                "<td>" +
                (e.name || "") +
                "</td>" +
                "<td>" +
                "<span class='"+claz+"'>" +
                e.score +
                "</span>" +
                "</td>" +
                
                "</tr>"
                );
    }

    //Highest table:
    $("#menu3 table > tbody").html("");
    for (var i = 0; i < dataManagerHighest.length; i++) {

        var e = dataManagerHighest[i];
        var arrow = "glyphicon glyphicon-arrow-up score green-score";
        var dif = Math.abs(Number(e.score) - Number(e.scoreP));
        var claz = "score green-score";

        if (Number(e.score) > 70 && Number(e.score) < 90) {
            claz = "score yellow-score";
        } else if (Number(e.score) < 70) {
            claz = "score red-score";
        }

        $("#menu3 table > tbody").append(
                "<tr style='font-size: 10px; font-weight: bold;'>" +
                "<td>" +
                (e.name || "") +
                "</td>" +
                "<td>" +
                "<span class='"+claz+"'>" +
                e.score +
                "</span>" +
                "</td>" +
                
                "</tr>"
                );
    }

    //Highest table:
    $("#menu2 table > tbody").html("");
    for (var i = 0; i < dataSectionHighest.length; i++) {

        var e = dataSectionHighest[i];
        var arrow = "glyphicon glyphicon-arrow-up score green-score";
        var dif = Math.abs(Number(e.score) - Number(e.scoreP));
        var claz = "score green-score";

        if (Number(e.score) > 70 && Number(e.score) < 90) {
            claz = "score yellow-score";
        } else if (Number(e.score) < 70) {
            claz = "score red-score";
        }

        $("#menu2 table > tbody").append(
                "<tr style='font-size: 10px; font-weight: bold;'>" +
                "<td>" +
                (e.name || "") +
                "</td>" +
                "<td>" +
                "<span class='"+claz+"'>" +
                e.score +
                "</span>" +
                "</td>" +
                
                "</tr>"
                );
    }

    //Lowest table:
    $("#menu4 table > tbody").html("");
    for (var i = 0; i < dataStoreLowest.length; i++) {

        var e = dataStoreLowest[i];
        var arrow = "glyphicon glyphicon-arrow-up score green-score";
var dif = Math.abs(Number(e.score) - Number(e.scoreP));

        var claz = "score green-score";

        if (Number(e.score) > 70 && Number(e.score) < 90) {
            claz = "score yellow-score";
        } else if (Number(e.score) < 70) {
            claz = "score red-score";
        }

        $("#menu4 table > tbody").append(
                "<tr style='font-size: 10px; font-weight: bold;'>" +
                "<td>" +
                (e.name || "") +
                "</td>" +
                "<td>" +
                "<span class='"+claz+"'>" +
                e.score +
                "</span>" +
                "</td>" +
                
                "</tr>"
                );
    }

    //Lowest table:
    $("#menu6 table > tbody").html("");
    for (var i = 0; i < dataManagerLowest.length; i++) {

        var e = dataManagerLowest[i];
        var arrow = "glyphicon glyphicon-arrow-up score green-score";
var dif = Math.abs(Number(e.score) - Number(e.scoreP));

        var claz = "score green-score";

        if (Number(e.score) > 70 && Number(e.score) < 90) {
            claz = "score yellow-score";
        } else if (Number(e.score) < 70) {
            claz = "score red-score";
        }

        $("#menu6 table > tbody").append(
                "<tr style='font-size: 10px; font-weight: bold;'>" +
                "<td>" +
                (e.name || "") +
                "</td>" +
                "<td>" +
                "<span class='"+claz+"'>" +
                e.score +
                "</span>" +
                "</td>" +
                
                "</tr>"
                );
    }

    //Lowest table:
    $("#menu5 table > tbody").html("");
    for (var i = 0; i < dataSectionLowest.length; i++) {

        var e = dataSectionLowest[i];
        var arrow = "glyphicon glyphicon-arrow-up score green-score";
        var dif = Math.abs(Number(e.score) - Number(e.scoreP));
        var claz = "score green-score";

        if (Number(e.score) > 70 && Number(e.score) < 90) {
            claz = "score yellow-score";
        } else if (Number(e.score) < 70) {
            claz = "score red-score";
        }

        $("#menu5 table > tbody").append(
                "<tr style='font-size: 10px; font-weight: bold;'>" +
                "<td>" +
                (e.name || "") +
                "</td>" +
                "<td>" +
                "<span class='"+claz+"'>" +
                e.score +
                "</span>" +
                "</td>" +
                
                "</tr>"
                );
    }

    $(".high-nps .nav-tabs a").click(function () {
        $(this).tab('show');
    });

    $(".low-nps .nav-tabs a").click(function () {
        $(this).tab('show');
    });
}

function fillRigthTable(data, type) {

    npsTable = data;
    var ap = '';

    //Main table:
    if (rightTable !== null) {
        rightTable.destroy();
        //rTable.clear();
        $("#right-table").html("");
        $("#right-table").append("<thead></thead>" +
            "<tbody></tbody>");
    }
    
    ap = "<tr>" +
            "<th>" +
            "Rank" +
            "</th>" +
            "<th>" +
            type +
            "</th>" +
            "<th>" +
            "NPS Total" +
            "</th>" +
            "<th style='border-right: 10px solid #F7F7F8;'>Tendencia</th>" +
            "</th>";

    for (var i = 0; i < _global_area.length; i++) {
        ap = ap + "<th>" + _global_area[i].desc + "</th>"
    }

    ap += "</tr>";

    $("#right-table thead").append(ap);

    ap = '';

    for (var i = 0; i < npsTable.zAreas.length; i++) {
        //var score = (Math.floor(Math.random() * 20 + 80)) + ".00";
        var clazz = "score green-score";
        var arrow = "up";

        if (Number(npsTable.zAreas[i].nps) > 70 && Number(npsTable.zAreas[i].nps) < 90) {
            clazz = "score yellow-score";
        } else if (Number(npsTable.zAreas[i].nps) < 70) {
            clazz = "score red-score";
        }

        var dif = Math.abs(Number(npsTable.zAreas[i].nps) - Number(npsTable.zAreas[i].npsP));

        if (Number(npsTable.zAreas[i].nps) < Number(npsTable.zAreas[i].npsP)) {
            if (dif <= 0.50) {
               
                arrow = "glyphicon glyphicon-pause equals score amber-score";
            } else {
                
                arrow = "down";
            }
        } else if (Number(npsTable.zAreas[i].nps) === Number(npsTable.zAreas[i].npsP)) {
            
            arrow = "glyphicon glyphicon-pause equals score amber-score";
        } else if (Number(npsTable.zAreas[i].nps) > Number(npsTable.zAreas[i].npsP)) {
            if (dif <= 0.50) {
                
                arrow = "glyphicon glyphicon-pause equals score amber-score";
            }
        }

        ap = "<tr style='cursor: pointer;' data-row-id='"
                + npsTable.zAreas[i].id + "'" +
                " data-grp-id='" + grp + "'" + " data-bsn-id='" + bsns + "'" +
                " onclick='openStores(this)'>" +
                "<td>" +
                "</td>" +
                "<td>" +
                npsTable.zAreas[i].id +
                "</td>" +
                "<td>" +
                "<span class='" + clazz + "'>" +
                Number(npsTable.zAreas[i].nps).toFixed(2) +
                "</span>" +
                "</td>" +
                "<td style='border-right: 10px solid #F7F7F8;'>" +
                "<span class='glyphicon glyphicon-arrow-" + arrow + " " +
                clazz + "'>" +
                "</span>" +
                "</td>";

        for (var j = 0; j < _global_area.length; j++) {
            var f = findArea(npsTable.zAreas[i].areas, _global_area[j].id);

            if (f !== -1) {
                var claz = "score green-score";

                if (Number(npsTable.zAreas[i].areas[f].nps) > 70 && Number(npsTable.zAreas[i].areas[f].nps) < 90) {
                    claz = "score yellow-score";
                } else if (Number(npsTable.zAreas[i].areas[f].nps) < 70) {
                    claz = "score red-score";
                }
                ap = ap + "<td>" +
                        "<span class='" + claz + "'>" +
                        Number(npsTable.zAreas[i].areas[f].nps).toFixed(2) +
                        "</span>" +
                        "</td>";
            } else {
                ap = ap + "<td>" +
                        "<span>" +
                        "</span>" +
                        "</td>";
            }
        }
        _zones.push(npsTable.zAreas[i].id);
        npsTable.zAreas[i].areas = [];
        ap = ap + "</tr>";

        

        $("#right-table tbody").append(ap);
    }
    
    rightTable = $('#right-table').DataTable({
            "dom": '<<<"col-xs-12 col-md-6"f><"col-xs-12 col-md-6"l>><t>ip>',
            columnDefs: [
                { orderable: false, className: 'table-g', targets: 3 },
                { orderable: true, targets: '_all' }
            ],
            "bPaginate": false,
            "bFilter": false,
            "bLengthChange": false,
            "bInfo": false,
            "oLanguage": {
                "sZeroRecords": "Ningún Registro",
                "sInfo": "Mostrando del _START_ al _END_ de _TOTAL_",
                "sInfoEmpty": "0 Eventos",
                "sPaginatePrevious": "Previous page",
                "sProcessing": "Cargando...",
                
                "oPaginate": {
                    "sFirst": "<<",
                    "sLast": ">>",
                    "sNext": ">",
                    "sPrevious": "<"
                }
            }            
        });
    
    rightTable.on('order.dt search.dt', function () {
             rightTable.column(0, { search: 'applied', order: 'applied' }).nodes().each(function (cell, i) {
                 cell.innerHTML = i + 1;
             });
         }).draw();
    
    if ($('.table-nps').css('display') == 'none') {
        $(".table-nps").show(200);
    }
    fillSelect(0);
}
////////////////////////////////////////
/**************************************/
function fillRankear(data) {

    var ranking = [];
    var type = '';
    if (data.type == 'STORE') {
        type = 'Ubicación';
        ranking = data.stores;
    } else if (data.type == 'MANAGER') {
        type = 'Gerente';
        ranking = data.mans;
    } else if (data.type == 'CHIEF') {
        type = 'Jefe';
        ranking = data.chfs;
    } else if (data.type == 'SELLER') {
        type = 'Asesor';
        ranking = data.sells;
    }

    var ap = '';

    if (rTable !== null) {
        rTable.destroy();
        //rTable.clear();
        $("#ranking-table").empty();
        $("#ranking-table").append("<tbody></tbody>");
    }
    //Main table:

    var headers = [];
    headers.push({'title': 'Rank'},
            {'title': type},
            {'title': 'NPS Total'},
            {'title': 'Tendencia'});
//    ap = "<tr>" +
//            "<th>" +
//            type +
//            "</th>" +
//            "<th>" +
//            "NPS Total" +
//            "</th>" +
//            "<th style='border-right: 10px solid #F7F7F8;'>Tendencia</th>" +
//            "</th>";
//
    for (var i = 0; i < _global_area.length; i++) {
        headers.push({'title': _global_area[i].desc});
    }
//
//    ap += "</tr>";
//
//    $("#ranking-table thead").append(ap);

    ap = '';

    for (var i = 0; i < ranking.length; i++) {
        //var score = (Math.floor(Math.random() * 20 + 80)) + ".00";
        var clazz = "score green-score";
        var arrow = "up";

        if (Number(ranking[i].nps) > 70 && Number(ranking[i].nps) < 90) {
            clazz = "score yellow-score";
        } else if (Number(ranking[i].nps) < 70) {
            clazz = "score red-score";
        }

        var dif = Math.abs(Number(ranking[i].nps) - Number(ranking[i].npsP));

        if (Number(ranking[i].nps) < Number(ranking[i].npsP)) {
            if (dif <= 0.50) {
                
                arrow = "glyphicon glyphicon-pause equals score amber-score";
            } else {
                
                arrow = "down";
            }
        } else if (Number(ranking[i].nps) === Number(ranking[i].npsP)) {
       
            arrow = "glyphicon glyphicon-pause equals score amber-score";
        } else if (Number(ranking[i].nps) > Number(ranking[i].npsP)) {
            if (dif <= 0.50) {
                
                arrow = "glyphicon glyphicon-pause equals score amber-score";
            }
        }

        ap = "<tr style='cursor: pointer;' >" +
                "<td>" +
                "</td>" +
                "<td>" +
                (data.type === 'STORE' ? ranking[i].id + ' - ' + ranking[i].desc : ranking[i].desc ) +
                "</td>" +
                "<td>" +
                "<span class='" + clazz + "'>" +
                Number(ranking[i].nps).toFixed(2) +
                "</span>" +
                "</td>" +
                "<td style='border-right: 10px solid #F7F7F8;'>" +
                "<span class='glyphicon glyphicon-arrow-" + arrow + " " +
                clazz + "'>" +
                "</span>" +
                "</td>";

        for (var j = 0; j < _global_area.length; j++) {
            var f = findArea(ranking[i].areas, _global_area[j].id);

            if (f !== -1) {
                var claz = "score green-score";

                if (Number(ranking[i].areas[f].nps) > 70 && Number(ranking[i].areas[f].nps) < 90) {
                    claz = "score yellow-score";
                } else if (Number(ranking[i].areas[f].nps) < 70) {
                    claz = "score red-score";
                }
                ap = ap + "<td>" +
                        "<span class='" + claz + "'>" +
                        Number(ranking[i].areas[f].nps).toFixed(2) +
                        "</span>" +
                        "</td>";
            } else {
                ap = ap + "<td>" +
                        "<span>" +
                        "</span>" +
                        "</td>";
            }
        }

        ap = ap + "</tr>";
        if (data.type == 'STORE') {
            strs.push(ranking[i].id);
        }

        $("#ranking-table tbody").append(ap);
    }

    rTable = $('#ranking-table').DataTable({
        "dom": '<<<"col-xs-12 col-md-6"f><"col-xs-12 col-md-6"l>><t>ip>',
        columns: headers,
        "oLanguage": {
            "sZeroRecords": "Ningún Registro",
            "sInfo": "Mostrando del _START_ al _END_ de _TOTAL_",
            "sInfoEmpty": "0 Eventos",
            "sPaginatePrevious": "Previous page",
            "sProcessing": "Cargando...",
            "sLengthMenu": "Mostrar _MENU_ registros",
            "sSearch": "Busqueda:",
            "bFilter": false,
            "bLengthChange": false,
            "bAutoWidth": false,
            "bDestroy": true,
            "bSort": false,
            "bInfo": false,
            "oPaginate": {
                "sFirst": "<<",
                "sLast": ">>",
                "sNext": ">",
                "sPrevious": "<"
            }
        },
        //retrieve: true,
        "lengthMenu": [[30, 50, 100, -1], [30, 50, 100, "Todos"]]
    });

    rTable.on('order.dt search.dt', function () {
             rTable.column(0, { search: 'applied', order: 'applied' }).nodes().each(function (cell, i) {
                 cell.innerHTML = i + 1;
             });
         }).draw();

    if ($('.table-nps-ranking').css('display') == 'none') {
        $(".table-nps-ranking").show(500);
    }

    fillSelect(5);
}
////////////////////////////////////////
/**************************************/
function findArea(arr, item) {
    for (var i = arr.length; i--; ) {
        if (arr[i].id === item) {
            return i;
        }
    }
    return -1;
}
////////////////////////////////////////
/*************Update line business**************/
function updateLine(line, tt, type) {
    var l = $("#npsT label").length;
    var texts = [];
    $("#npsT label").each(function () {
        texts.push($(this).text().trim());
    });
//    if (tt === 'business' && texts.length > 3 && type === '') {
//        type = 'less';
//    }
    $('#npsT').html("");
    if ((type === 'less' && texts.length > 1)) {
        var index = findElement(texts, line);
        if (index !== -1) {
            texts = texts.slice(0, index);
        }
    } else if (type.includes('Comparativa') && texts.length > 1) {
        var index = findElement(texts, line);
        if (index !== -1) {
            texts = texts.slice(0, index + 1);
        }
    } else if ((type.includes('Ranking') && texts.length > 1)) {
        //var index = findElement(texts, line);
        var last = texts[(texts.length - 1)];
        if (last.includes('Ranking')) {
            texts = texts.slice(0, texts.length - 1);
        }
    }
    l = texts.length;

    for (var i = 0; i < l; i++) {
        texts[i] = texts[i].replace('>', '');
        if (texts[i].includes('Comparativa')) {
            $('#npsT').append('<label class="normal-lbl"><a href="javascript:void(0);" '
                    + 'data-comp="' + _compare + '"' +
                    'onclick="resetNps(this)" data-reset-g="' + grp +
                    '" data-reset-b="' + bsns + '"> ' +
                    '<span class="glyphicon glyphicon-chevron-right"></span>' +
                    texts[i] + '</a></label>');
        } else {
            $('#npsT').append('<label class="normal-lbl"><a href="javascript:void(0);" '
                    + 'data-level="' + i + '"' + 'data-info="' + texts[i] + '"' +
                    'onclick="resetNps(this)" data-reset-g="' + grp +
                    '" data-reset-b="' + bsns + '"> ' +
                    '<span class="glyphicon glyphicon-chevron-right"></span>' +
                    texts[i] + '</a></label>');
        }
    }
    if (!type.includes('Ranking') && !type.includes('Comparativa')) {
//        $('#npsT').append('<label class="normal-lbl">' +
//                '<span style="color: #306898; font-weight: bold;"> ' +
//                '<span class="glyphicon glyphicon-chevron-right"></span>' +
//                line + '</span>' +
//                '</label>');
        $('#npsT').append('<label class="normal-lbl"><a href="javascript:void(0);" '
                + 'data-level="' + i + '"' + 'data-info="' + type + '"' +
                'onclick="resetNps(this)" data-reset-g="' + grp +
                '" data-reset-b="' + bsns + '" data-sin="sin"> ' +
                '<span style="color: #306898; font-weight: bold;"> ' +
                '<span class="glyphicon glyphicon-chevron-right"></span>' +
                line + '</span></a></label>');
    } else {
        $('#npsT').append('<label class="normal-lbl">' +
                '<span style="color: #306898; font-weight: bold;"> ' +
                '<span class="glyphicon glyphicon-chevron-right"></span>' +
                ' |  ' + type + '</span>' +
                '</label>');
    }
}
/****************************************/
function resetNps(a) {
    var _a = $(a);
    var level = _a.data("level");
    var info = _a.data("info");
    var comp = _a.data("comp");
    var sin = _a.data("sin");
    flow = '';
    tN = info;
    cleanScreen();

    if (typeof comp !== 'undefined') {
        if (Number(comp) === 0) {
            
            managers = [];
            chfs = [];
            sections = [];
            $("#apply-zone").click();
        } else if (Number(comp) === 1) {
            
            chfs = [];
            sections = [];
            $("#apply-managers").click();
        } else if (Number(comp) === 2) {
            
            sections = [];
            $("#apply-section").click();
        }
    } else {
        if (level === 0) {

            grp = '';
            bsns = '';
            _zones = []
            strs = [];
            managers = [];
            chfs = [];
            sections = [];

            resetLocalStorage('company');
            $('#npsT').html("");
            $('#npsT').append('<label class="normal-lbl">' +
                    '<span style="color: #306898; font-weight: bold;">Compañia</span>' +
                    '</label>');

            getNpsArray("?firstDate=" + from + "&lastDate=" + to + '&op=' + question);

            createGroups("?firstDate=" + from + "&lastDate=" + to + '&op=' + question);

            resetFilters();

        } else if (level === 1) {

            bsns = '';
            _zones = [];
            strs = [];
            managers = [];
            chfs = [];
            sections = [];

            resetLocalStorage('group');
            getNpsArray("?firstDate=" + from + "&lastDate=" + to + "&group=" + grp + "&type=" + tN);

            resetFilters();

            createBusiness("?firstDate=" + from + "&lastDate=" + to + "&group=" + grp);

            if (!sin) {
                updateLine(info, "group", "less");
            }

        } else if (level === 2) {

            _zones = [];
            zoneStore = [];
            strs = [];
            managers = [];
            chfs = [];
            sections = [];

            resetLocalStorage('business');
            getNpsArray('?firstDate=' + from + '&lastDate=' + to + '&zNps='
                    + JSON.stringify(zoneStore) + '&business=' + bsns + '&op='
                    + question + '&type=' + tN);

            resizeSpaceLinesChart();

            getNpsTop('?firstDate=' + from + '&lastDate=' + to + '&zNps=' +
                    JSON.stringify(zoneStore) + '&business=' + bsns + '&op=' +
                    question + '&group=' + grp);

            getNpsGlobalArea('?firstDate=' + from + '&lastDate=' + to + '&zNps='
                    + JSON.stringify(zoneStore) + '&business=' + bsns + '&op=' +
                    question + '&type=' + tN);
            
            getNpsRanking('?firstDate=' + from + '&lastDate=' + to + '&zNps=' +
                    JSON.stringify(zoneStore) + '&business=' + bsns + '&op=' +
                    question + '&group=' + grp);
            
            if ($('.zone-row').css('display') == 'none') {
                createZones();
            }
            
            if (!sin) {
                updateLine(info, "business", "less");
            }
        } else if (level === 3) {

            var path = "?zone=" + _zone + "&zNps=" + JSON.stringify(zoneStore)
                    + "&firstDate=" + from + "&lastDate=" + to +
                    '&business=' + bsns + '&op=' + question + '&group=' + grp;
            tN = _zone;

            getNpsArray('?firstDate=' + from + '&lastDate=' + to + '&zNps=' +
                    JSON.stringify(zoneStore) + '&group=' + grp + '&op=' +
                    question + '&zone=' + _zone + '&business=' + bsns + '&type=' + tN);

            resizeSpaceLinesChart();

            getNpsTop('?firstDate=' + from + '&lastDate=' + to + '&zNps=' +
                    JSON.stringify(zoneStore) + '&business=' + bsns + '&op=' +
                    question + '&group=' + grp);

            getNpsGlobalArea(path);

            strs = [];
            managers = [];
            chfs = [];
            sections = [];

            if (!sin) {
                updateLine(tN, 'zona', "less");
            }

            getNpsDetailStore(path);

        } else if (level === 4) {

            var path = "?store=" + _store + "&zNps=" + JSON.stringify(zoneStore)
                    + "&firstDate=" + from + "&lastDate=" + to + '&business=' +
                    bsns + '&op=' + question + '&group=' + grp;

            tN = _store_name;

            getNpsArray('?firstDate=' + from + '&lastDate=' + to + '&zNps=' +
                    JSON.stringify(zoneStore) + '&group=' + grp + '&op=' + question +
                    '&store=' + _store + '&business=' + bsns + '&type=' + tN);

            resizeSpaceLinesChart();

            getNpsTop('?firstDate=' + from + '&lastDate=' + to + '&zNps=' +
                    JSON.stringify(zoneStore) + '&business=' + bsns + '&op=' +
                    question + '&group=' + grp);

            getNpsGlobalArea(path);

            managers = [];
            chfs = [];
            sections = [];

            if (!sin) {
                updateLine(tN, 'almacen', "less");
            }

            detailManagerNps(path);

        } else if (level === 5) {

            var path = "?man=" + _man + "&zNps=" + JSON.stringify(zoneStore)
                    + "&firstDate=" + from + "&lastDate=" + to + '&store=' +
                    _store + '&business=' + bsns + '&op=' +
                    question + '&group=' + grp;
            tN = _man_name;

            getNpsArray('?firstDate=' + from + '&lastDate=' + to + '&zNps=' +
                    JSON.stringify(zoneStore) + '&store=' + _store + '&group=' + grp +
                    '&op=' + question + '&man=' + _man + '&business='
                    + bsns + '&type=' + tN);

            resizeSpaceLinesChart();

            getNpsTop('?firstDate=' + from + '&lastDate=' + to + '&zNps=' +
                    JSON.stringify(zoneStore) + '&business=' + bsns + '&op=' +
                    question + '&group=' + grp);

            getNpsGlobalArea(path);

            if (!sin) {
                updateLine(tN, 'gerente', "less");
            }

            chfs = [];
            sections = [];

            datailChiefNps(path);
        } else if (level === 6) {

            var path = "?chief=" + _chf + "&zNps=" + JSON.stringify(zoneStore) + '&man='
                    + _man + "&firstDate=" + from + "&lastDate=" + to + '&store=' + _store +
                    '&business=' + bsns + '&op=' + question + '&group=' + grp;
            tN = _chf_name;

            getNpsArray('?firstDate=' + from + '&lastDate=' + to + '&zNps=' +
                    JSON.stringify(zoneStore) + '&group=' + grp + '&op=' + question +
                    '&store=' + _store + '&chief=' + _chf +
                    '&business=' + bsns + '&type=' + tN);

            resizeSpaceLinesChart();

            getNpsTop('?firstDate=' + from + '&lastDate=' + to + '&zNps=' +
                    JSON.stringify(zoneStore) + '&business=' + bsns + '&op=' +
                    question + '&group=' + grp);
            getNpsGlobalArea(path);

            if (!sin) {
                updateLine(tN, 'jefe', "less");
            }

            sections = [];

            detailSectionNps(path);
        } else if (level === 7) {

            var path = "?section=" + _sec + "&zNps=" + JSON.stringify(zoneStore)
                    + "&firstDate=" + from + "&lastDate=" + to + '&store=' + _store +
                    '&business=' + bsns + '&op=' + question + '&group=' + grp;
            tN = _sec_name;

            getNpsArray('?firstDate=' + from + '&lastDate=' + to + '&zNps=' + '&chief='
                    + _chf + JSON.stringify(zoneStore) + '&group=' + grp + '&op=' +
                    question + '&store=' + _store + '&section=' + _sec +
                    '&business=' + bsns + '&type=' + tN);

            resizeSpaceLinesChart();

            getNpsTop('?firstDate=' + from + '&lastDate=' + to + '&zNps=' +
                    JSON.stringify(zoneStore) + '&business=' + bsns + '&op=' +
                    question + '&group=' + grp);
            getNpsGlobalArea(path);

            if (!sin) {
                updateLine(tN, 'seccion', "less");
            }

            detailSellersNps(path);
        }
    }
}
/////////////////////////////////////////
/*****************************************/
function reset() {

    if (_ranking.length > 1) {
        var val = _ranking[_ranking.length - 2];
        _ranking.splice((_ranking.length - 2), 2);
        $('#select-nps').val(val).change();

    }

}
///////////////////////////////////////////
/****************************************/
function filterSelect() {
    //(val);

    var val = '';
    var text = '';

    val = $('select.input-filter option:selected').val();
    text = $('select.input-filter option:selected').text();
    //_ranking    = val;
    //_tRanking   = text;

    _ranking.push(Number(val));


    flow = '';

    cleanScreenRanking();
    loadingShow();

    var path = '?firstDate=' + from + '&lastDate=' + to + '&group=' + grp +
            '&business=' + bsns + '&op=' + question + '&type=' + tN;

    if (val == Number(0) && zoneStore.length > 0) {

        if (tB === tN) {
            strs = [];
            _zones = [];
        }

        if (Number(_compare) === 0) {
            $("#apply-zone").click();
        } else if (Number(_compare) === 1) {
            $("#apply-managers").click();
        } else if (Number(_compare) === 2) {
            $("#apply-section").click();
        }
    } else if (val == Number(0)) {
        if (tB === tN) {
            strs = [];
            _zones = [];
        }

        $('#npsT label:nth-last-child(2) a').click();

//        getNpsRanking('?firstDate=' + from + '&lastDate=' + to + '&zNps=' +
//                JSON.stringify(zoneStore) + '&business=' + bsns + '&op=' + question
//                + '&group=' + grp);

    } else if (val == Number(4)) {  // Ranking for stores

        rankingFor('?firstDate=' + from + '&lastDate=' + to +
                '&zone=' + _zones + '&business=' + bsns
                + '&op=' + question + '&group=' + grp + '&rankear=' + 'STORE');

    } else if (val == Number(5)) { //Ranking for Managers

        rankingFor('?firstDate=' + from + '&lastDate=' + to + '&zone=' + _zones +
                '&store=' + strs + '&business=' + bsns
                + '&op=' + question + '&group=' + grp + '&rankear=' + 'MANAGER');

    } else if (val == Number(6)) {  // Ranking for Chiefs

        rankingFor('?firstDate=' + from + '&lastDate=' + to + '&zone=' + _zones +
                '&store=' + strs + '&business=' + bsns + '&managers=' + managers
                + '&op=' + question + '&group=' + grp + '&rankear=' + 'CHIEF');

    } else if (val == Number(7)) {  //Ranking for Sections

        rankingFor('?firstDate=' + from + '&lastDate=' + to + '&zone=' + _zones +
                '&store=' + strs + '&business=' + bsns + '&managers=' + managers +
                '&chiefs=' + chfs
                + '&op=' + question + '&group=' + grp + '&rankear=' + val);

    } else if (val == Number(8)) {  //Ranking for Sellers

        rankingFor('?firstDate=' + from + '&lastDate=' + to + '&zone=' + _zones +
                '&store=' + strs + '&business=' + bsns + '&managers=' + managers +
                '&chiefs=' + chfs + '&sections=' + sections
                + '&op=' + question + '&group=' + grp + '&rankear=' + 'SELLER');
    }

    if (Number(val) === 0) {
        if ($('.return-nps').css('display') === 'block') {
            $('.return-nps').hide();
        }
        //updateLine(tN, '', '');
    } else {
        if ($('.return-nps').css('display') === 'none') {
            $('.return-nps').fadeIn(200);
        }
        updateLine('| Ranking', '', "Ranking " + text);

    }
}
//////////////////////////////////////////
function resetWithDates() {

    var texts = [];
    $("#npsT label").each(function () {
        texts.push($(this).text().trim());
    });
    var l = texts.length;
    getNpsArray('?firstDate=' + from + '&lastDate=' + to + '&group=' + grp + 
            '&op=' + question + '&type=' + tN + '&business=' + bsns);
    if (texts[(l - 1)].includes('Ranking')) {
        filterSelect();
    } else if (texts[l - 1].includes('Comparativa')) {
        var comparativas = texts[l - 1].split(' ');
        if (comparativas.length > 2) {
            if (comparativas[2].equals('Ubicación')) {
                $("#apply-zone").click();
            } else if (comparativas[2].equals('Jefes')) {
                $("#apply-managers").click();
            } else if (comparativas[2].equals('Seccion')) {
                $("#apply-section").click();
            }
        }
    } else {
        if (texts.length === 1) {
            
            createGroups('?firstDate=' + from + '&lastDate=' + to + '&op=' + question);
        } else {
            $('#npsT label:nth-last-child(1) a').click();
        }
    }
}

function resetLocalStorage(level) {
    if (level === 'company') {
        localStorage.setItem("group", "");
        localStorage.setItem("business", "");
        localStorage.setItem("comparativa", "");
        localStorage.setItem("compare", JSON.stringify([]));
    } else if (level === 'group') {
        localStorage.setItem("business", "");
        localStorage.setItem("comparativa", "");
        localStorage.setItem("compare", JSON.stringify([]));
    } else if (level === 'business') {
        localStorage.setItem("comparativa", "");
        localStorage.setItem("compare", JSON.stringify([]));
    }
}