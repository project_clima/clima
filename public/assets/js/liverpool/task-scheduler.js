$.validator.addMethod("time24", function(value) {
    if (!/^\d{2}:\d{2}$/.test(value)) return false;

    var parts = value.split(':');
    if (parts[0] > 23 || parts[1] > 59) return false;
    return true;
}, "Invalid time format.");
            
var Task = function() {
    var bindEvents = function() {
        $(function () {            
            $('#calculate').click(function () {
                $.get(baseUrl + 'scoretaskscheduler/addCalculationTask', function (data) {
                    showForm(data);
                });
            });
        });
    };

    var showForm = function (data) {
        $('#task-form').trigger('reset');
        $('#mdl-add-edit').empty().html(data).modal('show');

        $('.input-daterange').datepicker({
            format: 'dd/mm/yyyy',
            language: 'es',
            autoclose: true,
            startDate: 'today'
        });
        
        $('#executionHour').timepicker({
            minuteStep: 1,
            defaultTime: false,
            showMeridian: false
        });
        
        $('.input-group-addon').click(function(e) {
            $(this).siblings('.date-picker').focus();
        }).css({'cursor': 'pointer'});

        validate();
    };

    var validate = function() {
        $('#task-form').validate({
            'ignore': '',
            'rules': {
                'executionHour': {
                    'required': true,
                    'time24': true
                }
            },
            'messages': {
                'structureDate': {
                    'required': 'Por favor, seleccione el periodo.'
                },
                'executionDay': {
                    'required': 'Por favor, seleccione el día de ejecución.'
                },
                'executionHour': {
                    'required': 'Por favor, seleccione la hora de ejecución.',
                    'time24': 'Por favor, agregue una hora válida en formato de 24 horas.'
                }
            },
            errorPlacement: function(error, element) {
                if (element.next().is('.input-group-addon')) {
                    error.insertAfter(element.next().parent());
                } else {
                    error.insertAfter(element);
                }
            },
            submitHandler: function (form) {
                save(form);
            }
        });
    };

    var save = function(form) {
        $.ajax({
            url: $(form).attr('action'),
            data: $(form).serialize(),
            type: 'post',
            dataType: 'json',
            success: function (response) {
                if (response.error) {
                    Liverpool.setMessage($('.inner-message'), response.message, 'warning');
                } else {
                    Liverpool.setMessage($('.message'), response.message, 'success');
                    $('#mdl-add-edit').modal('hide');                    
                }
            },
            beforeSend: function () {
                $('button', $(form)).attr('disabled', 'disabled');
            },
            complete: function () {
                $('button', $(form)).removeAttr('disabled');
            },
            error: function () {
                Liverpool.setMessage($('.inner-message'), 'La tarea no pudo ser agregada. Por favor, intente nuevamente.', 'warning');
            }
        });
    };
    
    var init = function() {
        bindEvents();
    };

    init();
}();