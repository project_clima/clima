/******Call ajax nps company*************/
function getNpsArray(args) {
    var action = baseUrl + 'nps/getNpsCompanyData' + args;
    Liverpool.loading("show");
    $.ajax({
        url: action,
        success: function (data) {
            hi(data);
            Liverpool.loading("hide");
            
        }
    });
}
;
function getNpsTop(args) {
    var action = baseUrl + 'nps/getNpsTop' + args;
    $.ajax({
        url: action,
        success: function (data) {
            getTop(data);
        }
    });
}
;
function getNpsBackground(args) {
    var _this = {};
    _this.cb = null;

    var action = baseUrl + 'nps/getNpsCompanyData' + args;
    $.ajax({
        url: action,
        success: function (data) {
            _this.cb(data);
        }
    });

    return {
        done: function(f) {
            _this.cb = f || function(){};
        }
    }
}
;
function getNpsRanking(args) {
    var action = baseUrl + 'nps/getNpsRanking' + args;
    $.ajax({
        url: action,
        success: function (data) {
            fillRanking(data, "Zona");
        }
    });
}
;
/****************************************/
function getNpsDetailStore(path) {
    $.get(baseUrl + "nps/detailStores" + path,
            function (data, textStatus, jqXHR) {
                fillStores(data);
                fillSelect(1);
            });
}
//////////////////////////////////////////  
/****************************************/
function getNpsDetailSection(path) {
    $.get(baseUrl + "nps/detailSection" + path,
            function (data, textStatus, jqXHR) {
                fillSections(data);
            });
}
////////////////////////////////////////// 
/****************************************/
function getNpsGroup(path) {
    $.get(baseUrl + "nps/detailGroup" + path,
            function (data, textStatus, jqXHR) {
                //fillStores(data);
            });
}
////////////////////////////////////////// 
/******Call get create menu groups******/
function createGroups(path) {
    var action = baseUrl + 'nps/getNpsGroups' + path;
    $.ajax({
        url: action,
        success: function (data) {
            fillGroups(data);
        }
    });
}
//////////////////////////////////////////
/******Call get create menu zones********/
function createBusiness(path) {
    var action = baseUrl + 'nps/detailBusiness' + path;
    $.ajax({
        url: action,
        success: function (data) {
            fillBusiness(data);
        }
    });
}
//////////////////////////////////////////
/******Call get create menu zones********/
function getNpsGlobalArea(path) {
    var action = baseUrl + 'nps/detailGlobalAreas' + path;
    $.ajax({
        url: action,
        success: function (data) {
            fillPrincipal(data);
        }
    });
}
//////////////////////////////////////////
/******Call get create menu zones********/
function createZones() {
    var action = baseUrl + 'nps/getNpsZones?business=' + bsns + '&group=' + grp;
    $.ajax({
        url: action,
        success: function (data) {
            zones = data;
            hi2(data);
        }
    });
}
//////////////////////////////////////////
/******Call get create menu zones********/
function createSections(path) {
    var action = baseUrl + 'nps/getNpsSections?zNps=' + JSON.stringify(zoneStore);
    $.ajax({
        url: action,
        success: function (data) {
            fillSectionsZones(data);
        }
    });
}
//////////////////////////////////////////
/******Call get create menu mamagers********/
function getManagersChiefs(path) {
    var action = baseUrl + 'nps/getManagersChiefs' + path;
    $.ajax({
        url: action,
        success: function (data) {
            man(data);
        }
    });
}
//////////////////////////////////////////
/*******************************************/

/*******************************************/
function detailStoreNps(path) {
    $.get(baseUrl + "nps/detailStores" + path,
            function (data, textStatus, jqXHR) {
                fillStores(data);
            });
}

/*******************************************/
function detailManagerNps(path) {
    $.get(baseUrl + "nps/detailManager" + path,
            function (data, textStatus, jqXHR) {
                Liverpool.loading("hide");
                fillManagers(data);
                fillSelect(2);
            });
}
/*******************************************/
function datailChiefNps(path) {
    $.get(baseUrl + "nps/detailChief" + path,
            function (data, textStatus, jqXHR) {
                fillChiefs(data);
                fillSelect(3);
            });
}
/*******************************************/
function detailSectionNps(path) {
    $.get(baseUrl + "nps/detailSection" + path,
            function (data, textStatus, jqXHR) {
                fillSections(data);
                fillSelect(3);
            });
}

/******************************************/
function detailSellersNps(path,type) {
    $.get(baseUrl + "nps/detailSellers" + path,
            function (data, textStatus, jqXHR) {
                fillSellers(data);
                fillSelect(4);
            });
}
/*******************************************/
function rankingFor(path) {
    $.get(baseUrl + "nps/rankingFor" + path,
        function (data, textStatus, jqXHR) {
            //_zones = [];
            //strs = [];
            fillRankear(data);
    });
}
///////////////////////////////////////////////
/*********************************************/
function compareChiefs(path){
    $.get(baseUrl + "nps/compareChiefs" + path,
        function (data, textStatus, jqXHR) {
            fillChiefs(data);
            fillSelect();
    });
}
///////////////////////////////////////////////
/*********************************************/
function compareSections(path){
    $.get(baseUrl + "nps/compareSections" + path,
        function (data, textStatus, jqXHR) {
            fillSections(data);
            fillSelect();
    });
}
///////////////////////////////////////////////