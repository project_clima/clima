var AnswerSurvey = function(isPreview) {
    var maxPage         = 1;
    var currentProgress = 0;
    var $form           = null;
    
    var bindEvents = function() {
        $.extend($.validator.messages, {
            required: "Aún no contestas esta pregunta"
        });
        
        $('#start-survey').click(function() {
            $('#page-start').hide();
            $('#title-progress').removeClass('hidden').show();
            $('#page-1').removeClass('hidden').show();
        });

        $('.next-page').click(function() {
            var currentPage = $(this).data('current');
            var isLastPage  = parseInt(currentPage) === $('.page').length;

            $form.validate().settings.ignore = ":disabled, :hidden";

            if ($form.valid()) {                
                if (typeof isPreview === 'undefined' || isPreview === false) {
                    save($('#page-' + currentPage), isLastPage);
                } else {
                    changePage($('#page-' + currentPage));
                    progress(currentPage);
                }
            }       
        });

        $('.prev-page').click(function() {
            var current = $(this).data('current');
            var prev    = $(this).data('prev');

            $('#page-' + current).hide();
            $('#page-' + prev).removeClass('hidden').show();

            if (prev === 'start') {
                $('#title-progress').hide();
            }
        });
        
        $('#close-survey').click(function(e) {
            e.preventDefault();
            
            var redirect  = $(this).attr('href');
            var logoutUrl = $(this).data('logout-url');
            
            closeSession(logoutUrl, redirect);
        });
        
        $('.numeric-nps').change(function() {
            var question = $(this).data('question');
            var value    = $('[data-question="' + question + '"]:checked').val();
            
            if (parseInt(value) < 9) {
                $('#alternative-' + question + ' textarea').addClass('required').focus();
            } else {
                $('#alternative-' + question + ' textarea').removeClass('required');
            }
        });
    };
    
    var setValidator = function() {
        $form = $('#survey-form');
        
        $form.validate({
            'ignore': '',
            errorPlacement: function (error, element) {
                if (element.hasClass('numeric-nps')) {
                    error.insertAfter($('#error-numeric-nps-' + $(element).data('question')));
                }  else {
                    error.insertAfter(element);
                }
            }
        });
    };
    
    var changePage = function ($page) {
        var $current = $('.next-page', $page);
        
        $('#page-' + $current.data('current')).hide();
        $('#page-' + $current.data('next')).removeClass('hidden').show();
    };
    
    var save = function($page, isLastPage) {        
        var data = $('input[type=text], input[type=hidden],' +
                     'input[type=radio]:checked, input[type=checkbox]:checked,' +
                     'textarea, select', $page
                    ).serialize();
        
        data += '&surveyId=' + $('#survey-id').val();
        data += isLastPage ? '&close=true' : '';
        
        $.ajax({
            url: baseUrl + 'surveycsi/save',
            type: 'post',
            dataType: 'json',
            data: data,
            success: function() {
                changePage($page);
                progress($('.next-page', $page).data('current'));
            },
            beforeSend: function() {
                toggleDisable($('button.btn-survey'));
                loading('show');
            },
            complete: function() {
                toggleDisable($('button.btn-survey'));
                loading('hide');
            }
        });
    };
    
    var toggleDisable = function(el) {
        $.each(el, function(){
            if ($(this).is(':disabled')) {
                $(this).removeAttr('disabled');
            } else {
                $(this).attr('disabled', 'disabled');
            }
        });
    };
    
    var progress = function(currentPage) {
        maxPage         = currentPage > maxPage ? currentPage : maxPage;
        currentProgress = (parseInt(maxPage) * 100) / $('.page').length;

        $('#current-survey-password').text(currentProgress.toFixed(0));
        $('#survey-progress .progress-bar').css({'width': currentProgress + '%'});
        $('#survey-progress .progress-bar').attr('aria-valuenow', currentProgress);
        
        $('html, body').animate({scrollTop: '0px'}, 750);
    };
    
    var loading = function(action, msg) {
        if(action && action === "show") {
            $("#loading-sign-msg").text(msg || "Guardando");
            $(".loading-div").fadeIn(250);
        } else {
            $(".loading-div").fadeOut(250);
        }
    };
    
    var closeSession = function(logoutUrl, redirect) {
        if (isPreview) {
            return location.href = redirect;
        }
                
        $.get(logoutUrl, function () {
            location.href = redirect;
        });
    };
    
    var init = function() {
        setValidator();
        bindEvents();
    };
    
    init();
};