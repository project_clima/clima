var months = [
    "Enero", "Febrero", "Marzo", "Abril", "Mayo", "Junio",
    "Julio", "Agosto", "Septiembre", "Octubre", "Noviembre", "Diciembre"
];
var monthsSmall = [
    "Ene", "Feb", "Mar", "Abr", "May", "Jun",
    "Jul", "Ago", "Sep", "Oct", "Nov", "Dic"
];

var globalParams = {};

var currYear = "";
var prevYear = "";
var acumCurrCircle = 0;
var smallTable1 = 0;
var smallTable2 = 0;
var smallTable3 = 0;
// calculate the number of columns based on this:
var highestMonth = -1;
var lowestMonth = 10;
var mainLineChart;
var plan = 91;
globalQuestion = 202;
var nps = {};
var timeLineLinks = [];
var tagsLevel = {
    section: "Sección",
    business: "Negocio",
    zone: "Zona",
    boss: "Jefe",
    seller: "Vendedor",
    manager: "Gerente",
    store: "Tienda"
}
var from;
var to;

var objectBusiness = {};
var prevYearBusiness = {};
var totalObjects = {};
var totalObjectsB = {};

var tableCenter = null;// table in the center of circle

function getMinMax(array, which) {
    val = 0;
    for(var i = 0; i < array.length; i++) {
        var arrVal = array[i];

        if(arrVal != null) {//ignore nulls
            if(which == "min") {
                if(val < arrVal) {
                    val = arrVal;
                }
            } else {// max assumed
                if(val > arrVal) {
                    val = arrVal;
                }
            }
        }
    }
}

function prevButNotCurrentYear(prevMonth, rowData) {
  for(var i = 0; i < rowData.length; i++) {
    if(rowData[i].monthNumber == prevMonth) {
      return false;
    }
  }

  return true;
}

function loadInfo(data, params, NPSRootData) {
    $("#history-table thead .top-row").html("");
    $("#history-table thead .bottom-row").html("");
    $("#history-table tbody").html("");
    var objectData = rowToObject(data.results.months, params, false, data.isSelect);
    var prevYearData = rowToObject(data.resultsPrevYear.months, params, false, data.isSelect);
    
    objectBusiness =   rowToObject(data.business.stores || {}, params, true);
    prevYearBusiness =   rowToObject(data.businessPrevYear.stores || {}, params, true);
    
    totalObjects = {};
    totalObjectsB = {};
    
    mergeObjects(objectData);
    mergeObjects(prevYearData);
    
    mergeObjectsB(objectBusiness);
    mergeObjectsB(prevYearBusiness);
    
    // insert prev year data into objectData:
    for(var k in totalObjects) {
        var row = objectData[k];
        if(row) {
            for(var i = 0; i < row.data.length; i++) {
                var monthBlock = row.data[i];

                // previous year data:
                for(var k2 in prevYearData) {
                    var rowPrev = prevYearData[k2];

                    for(var j = 0; j < rowPrev.data.length; j++) {
                        var monthBlockPrev = rowPrev.data[j];
                        var currMonth = monthBlock.monthNumber;
                        var prevMonth = monthBlockPrev.monthNumber;

                        // if same "department" and month:
                        if(k == k2) {
                          if(currMonth == prevMonth) {
                            monthBlock.scorePrev = monthBlockPrev.score;
                          } else {
                            if(prevButNotCurrentYear(prevMonth, row.data)) {
                              var newBlock = {
                                month: monthBlockPrev.month,
                                monthNumber: monthBlockPrev.monthNumber,
                                score: 0,
                                scorePrev: monthBlockPrev.score
                              };

                              row.data.push(newBlock);
                            }
                          }
                        }
                    }
                }
            }
            totalObjects[k].data = objectData[k].data;
        }else {
            //totalObjects[k].data = prevYearData[k].data;
            
                    var rowPrev = prevYearData[k];

                    for(var j = 0; j < rowPrev.data.length; j++) {
                        var monthBlockPrev = rowPrev.data[j];
                        
                            
                              var newBlock = {
                                month: monthBlockPrev.month,
                                monthNumber: monthBlockPrev.monthNumber,
                                score: 0,
                                scorePrev: monthBlockPrev.score
                              };

                              totalObjects[k].data.push(newBlock);
                            
                          
                        
                    }
                
        }
    }

    // same with business:
    for(var k in totalObjectsB) {
        var row = objectBusiness[k];
        if(row) {
            for(var i = 0; i < row.data.length; i++) {
                var monthBlock = row.data[i];

                // previous year data:
                for(var k2 in prevYearBusiness) {
                    var rowPrev = prevYearBusiness[k2];

                    for(var j = 0; j < rowPrev.data.length; j++) {
                        var monthBlockPrev = rowPrev.data[j];
                        var currMonth = monthBlock.monthNumber;
                        var prevMonth = monthBlockPrev.monthNumber;

                        // if same "department" and month:
                        if(k == k2) {
                          if(currMonth == prevMonth) {
                            monthBlock.scorePrev = monthBlockPrev.score;
                          } else {
                            if(prevButNotCurrentYear(prevMonth, row.data)) {
                              var newBlock = {
                                month: monthBlockPrev.month,
                                monthNumber: monthBlockPrev.monthNumber,
                                score: 0,
                                scorePrev: monthBlockPrev.score
                              };

                              row.data.push(newBlock);
                            }
                          }
                        }
                        /*
                        */
                    }
                }
            }
            totalObjectsB[k].data = objectBusiness[k].data;
        }else {
            //totalObjectsB[k].data = prevYearBusiness[k].data;
            var rowPrev = prevYearBusiness[k];

                    for(var j = 0; j < rowPrev.data.length; j++) {
                        var monthBlockPrev = rowPrev.data[j];
                        
                            
                              var newBlock = {
                                month: monthBlockPrev.month,
                                monthNumber: monthBlockPrev.monthNumber,
                                score: 0,
                                scorePrev: monthBlockPrev.score
                              };

                              totalObjectsB[k].data.push(newBlock);
                            
                          
                        
                    }
        }
    }

    // grab "acum" data:
    for(var k in totalObjects) {
        var row = objectData[k];
        var row2 = totalObjects[k];
           for(var j = 0; j < data.results.acum.length; j++) {
                var blockAcum = data.results.acum[j];

                if(blockAcum[0] == k) {
                    if(row)
                        row.acum = blockAcum[2] || 0;
                    if(row2)
                        row2.acum = blockAcum[2] || 0;
                    if(params.level == "zone" ) {
                        if(row)
                            row.acum = blockAcum[1];
                        if(row2)
                            row2.acum = blockAcum[1];
                    }
                    break;
                }
           }
    }


    // grab "acum" data from previous year:
    for(var k in totalObjects) {
      var row = objectData[k];
      var row2 = totalObjects[k];
      for(var i = 0; i < data.resultsPrevYear.acum.length; i++) {
        var blockAcum = data.resultsPrevYear.acum[i];

        if(blockAcum[0] == k) {
            if(row)
                row.acumPrevYear = blockAcum[2] || 0;
            if(row2)
                row2.acumPrevYear = blockAcum[2] || 0;
            if(params.level == "zone") {
                if(row)
                    row.acumPrevYear = blockAcum[1];
                if(row2)
                    row2.acumPrevYear = blockAcum[1];
            }
            break;
        }
      }
    }

    // same for business:
    console.log("objectBusiness\n", JSON.stringify(objectBusiness));
    for(var k in totalObjectsB) {
        var row = objectBusiness[k];
        var row2 = totalObjectsB[k];
            for(var j = 0; j < data.business.acum.length; j++) {
                var blockAcum = data.business.acum[j];

                if(blockAcum[0] == k) {
                    if(row)
                        row.acum = blockAcum[2] || 0;
                    if(row2)
                        row2.acum = blockAcum[2] || 0;
                    if(params.level == "zone" && !data.isSelect) {
                        //row.acum = blockAcum[1];
                    }
                    break;
                }
           }
        
    }
    
    if(data.businessPrevYear) {
      for(var k in totalObjectsB) {
        var row = objectBusiness[k];
        var row2 = totalObjectsB[k];
        for(var i = 0; i < data.businessPrevYear.acum.length; i++) {
          var blockAcum = data.businessPrevYear.acum[i];

          if(blockAcum[0] == k) {
              if(row)
                row.acumPrevYear = blockAcum[2] || 0;
              if(row2)
                row2.acumPrevYear = blockAcum[2] || 0;
              if(params.level == "zone" && !data.isSelect) {
                  //row.acumPrevYear = blockAcum[1];
              }
              break;
          }
        }
      }
    }
    


    var placesVisible = 
        params.level == "zone" ||
        params.level == "store" ||
        params.level == "manager" ||
        params.level == "boss" ||
        params.level == "section" ||
        params.level == "seller";

    if (params.currLevel === 'business') {
        $("#li-places").show(200);
        $("#input-places").show(200);
    } else {
        $("#li-places").hide();
        $("#input-places").hide();
        $('#history-tabs li:eq(0) a').tab('show');
    }

    if(placesVisible) {
        // load tab "Ubicaciones":
        var prs = {
            businessId: params.businessId,
            groupId: params.groupId,
            startDate: from,
            endDate: to
        };
        
        $.ajax({
            type: "POST",
            url: baseUrl + "HistoryReport/getDetailLocations",
            data: {
                "jsonData": JSON.stringify(totalObjectsB),
                "lowestMonth": lowestMonth,
                "highestMonth": highestMonth,
                "currYear": currYear,
                "prevYear": prevYear,
                "area": params.area
            }
        }).done(function(data, textStatus, jqXHR) {
           $("#container-locations").html(data);
        }).fail(function(jqXHR, textStatus, errorThrown) {
            console.log(textStatus, errorThrown);
        });
    } else {
        $("#li-places").hide();
        $("#input-places").hide();
    }

    // call init template:
    
    var theData = {
        "level": params.level || "index",
        "jsonData": JSON.stringify(totalObjects),
        "lowestMonth": lowestMonth,
        "highestMonth": highestMonth,
        "groupId": params.groupId,
        "businessId": params.businessId,
        "zoneId": params.zoneId,
        "storeId": params.storeId,
        "managerId": params.managerId,
        "bossId": params.bossId,
        "area": params.area,
        "sectionId": params.sectionId
    };
    $.ajax({
        type: "POST",
        url: baseUrl + "HistoryReport/renderTemplateTable",
        data: theData
    }).done(function(data, textStatus, jqXHR) {
        $("#table-target").html(data);

        mainLineChart.reflow();
    }).fail(function(jqXHR, textStatus, errorThrown) {
        console.error(textStatus, errorThrown);
    });


    // data for the lines charts:
    if(NPSRootData) {
        var monthsCurrYear = [];
        var monthsPrevYear = [];
        for(var i = 0; i < NPSRootData.months.length; i++) {
            var elem = NPSRootData.months[i];
            monthsCurrYear.push(elem);
        }

        for(var i = 0; i < NPSRootData.monthsLess1.length; i++) {
            var elem = NPSRootData.monthsLess1[i];
            monthsPrevYear.push(elem);
        }

        var monthsCurrYearTrim = [];
        var monthsPrevYearTrim = [];
        for(var i = 0; i < NPSRootData.triMonths.length; i++) {
            var elem = NPSRootData.triMonths[i] || 0;
            monthsCurrYearTrim.push(elem);
        }

        for(var i = 0; i < NPSRootData.triMonthsLess1.length; i++) {
            var elem = NPSRootData.triMonthsLess1[i] || 0;
            monthsPrevYearTrim.push(elem);
        }

        //make a "trim" on the arrays:
        var arrays = [
            monthsCurrYear,
            monthsCurrYearTrim,
            monthsPrevYear,
            monthsPrevYearTrim
        ]

        minY = getMinMax(monthsCurrYear, "min");
        maxY = getMinMax(monthsCurrYear, "max");
        minYTrim = getMinMax(monthsCurrYearTrim, "min");
        maxYTrim = getMinMax(monthsCurrYearTrim, "max");
    }

    var categos = [];
    for(var i = 1; i <= 12; i++) {
        categos.push(monthsSmall[i - 1]);
    }

    mainLineChart = Highcharts.chart('main-line-chart', {
        chart: {
            type: 'line',
            height: 333
        },
        legend: {
            align: "right",
            verticalAlign: "middle"
        },
        colors: [
            "rgb(48, 104, 152)",
            "rgb(74, 145, 13)"
        ],
        title: {
            text: 'Histórico ',
            y: 20,
            style: {
                fontSize: '13px'
            }
        },
        xAxis: {
            categories: categos,
            color: "#337AB7"
        },
        yAxis: {
            title: {
                text: '',
                style: {
                    "font-size": "20px"
                }
            },
            max: maxY,
            min: minY//lowest
        },
        plotOptions: {
            line: {
                dataLabels: {
                    enabled: true
                },
                enableMouseTracking: true
            }
        },
        tooltip: false,
        series: [{
            name: currYear - 1,
            data: monthsPrevYear
        }, {
            name: currYear,
            data: monthsCurrYear
        }]
    });

    mainLineChartTrim = Highcharts.chart('main-line-chart-trim', {
        chart: {
            type: 'line',
            height: 333,
            events: {
                load: function() {
                    $("#main-line-chart-trim").hide();
                }
            }
        },
        legend: {
            align: "right",
            verticalAlign: "middle"
        },
        colors: [
            "rgb(48, 104, 152)",
            "rgb(74, 145, 13)"
        ],
        title: {
            text: 'Histórico NPS',
            y: 20,
            style: {
                fontSize: '13px'
            }
        },
        xAxis: {
            categories: ["Ene - Mar", "Abr - Jun", "Jul - Sep", "Oct - Dic"],
            color: "#337AB7"
        },
        yAxis: {
            title: {
                text: ''
            },
            max: maxYTrim,
            min: minYTrim//lowest,
        },
        plotOptions: {
            line: {
                dataLabels: {
                    enabled: true
                },
                enableMouseTracking: true
            }
        },
        tooltip: false,
        series: [{
            name: currYear - 1,
            data: monthsCurrYearTrim
        }, {
            name: currYear,
            data: monthsPrevYearTrim
        }]
    });

    var generalNPS = Number(nps.npsCompany[3]);

    mainChart = Highcharts.chart('main-chart', {
        chart: {
            type: 'solidgauge',
            marginTop: 50,
            height: 330
        },

        title: {
            text: 'NPS ' + nps.type,
            y: 20,
            style: {
                fontSize: '13px'
            }
        },

        tooltip: {
            formatter: function () {
                return "PLAN " + plan;
            },
            positioner: function () {
                return {
                    x: 100,
                    y: 210
                }
            }
        },

        pane: {
            center: ['50%', '35%'],
            startAngle: 0,
            endAngle: 360,
            y: 0,
            background: [{// Track for Move
                    outerRadius: '75%',
                    innerRadius: '60%',
                    backgroundColor: "#DDDADB",
                    borderWidth: 0
                }]
        },

        yAxis: {
            min: 0,
            max: 100,
            lineWidth: 0,
            tickPositions: []
        },

        plotOptions: {
            solidgauge: {
                outerRadius: '75%',
                innerRadius: '60%',
                dataLabels: {
                    enabled: false
                },
                linecap: 'square',
                stickyTracking: false
            }
        },

        series: [{
            name: 'PLAN',
            borderColor: "#337AB7",
            data: [{
                color: "#337AB7",
                radius: '75%',
                y: acumCurrCircle
            }]
        }]
    },
    function (mainChart) {
        tableCenter = mainChart.renderer.html(
            "<table class='test-class'>" +
            "<tbody>" +
            "<tr><td class='td-acum-circle'>" + acumCurrCircle +
            "<span class='span-percent-circle'>%</span></td></tr>" +
            "</tbody>" +
            "</table>"
        )
        .css({
            color: "#4A910D",
            fontSize: "36px",
            fontWeight: "bold"
        })
        .add();

        tableCenter.align(Highcharts.extend(tableCenter.getBBox(), {
            align: 'center',
            x: 0, // offset
            verticalAlign: 'midle',
            y: 143 // offset
        }), null, 'spacingBox');

        var table = mainChart.renderer.html(
                "<table class='resume-circle-table'>" +
                "<tbody>" +
                "<tr>" +
                "<td class='quant quant-prom'>" +
                  smallTable1 +
                "</td>" +
                "<td class='quant quant-neutro'>" +
                  smallTable2 +
                "</td>" +
                "<td class='quant quant-detra'>" +
                  smallTable3 +
                "</td>" +
                "</tr>" +
                "<tr>" +
                "<td class='cell-green'>" +
                "</td>" +
                "<td class='cell-yellow'>" +
                "</td>" +
                "<td class='cell-orange'>" +
                "</td>" +
                "</tr>" +
                "<tr>" +
                "<td class='text-label'>" +
                "Promotores" +
                "</td>" +
                "<td class='text-label'>" +
                "Neutros" +
                "</td>" +
                "<td class='text-label'>" +
                "Detractores" +
                "</td>" +
                "</tr>" +
                "</tbody>" +
                "</table>"
                ).add();

        table.align(Highcharts.extend(table.getBBox(), {
            align: 'center',
            x: 0, // offset
            verticalAlign: 'bottom',
            y: 0 // offset
        }), null, 'spacingBox');
    });

    // change between line charts:

    $("#btn-trim").click(function() {
        $("#main-line-chart").hide(100, function() {
            $("#main-line-chart-trim").show(100, function() {
                mainLineChartTrim.reflow();
            });
        });
        $("#btn-mes").attr("class", "btn btn-xs btn-default");
        $(this).attr("class", "btn btn-xs btn-primary");
    });

    $("#btn-mes").click(function() {
        $("#main-line-chart-trim").hide(100, function() {
            $("#main-line-chart").show(100, function() {
                mainLineChart.reflow();
            });
        });
        $("#btn-trim").attr("class", "btn btn-xs btn-default");
        $(this).attr("class", "btn btn-xs btn-primary")
    });

    // datatable:
    $("#history-table").dataTable({
        "iDisplayLength": 20,
        "bFilter": true,
        "bLengthChange": false,
        "bAutoWidth": false,
        "bDestroy": true,
        "bSort": false,
        "bInfo": false,
        "oLanguage": {
            "sZeroRecords": "Ningún Registro",
            "sInfo": "Mostrando tiendas de _START_ al _END_ de _TOTAL_ totales",
            "sInfoEmpty": "0 Tiendas",
             "sPaginatePrevious": "Previous page",
             "sProcessing": "Cargando...",
             "oPaginate": {
                "sFirst":    "<<",
                "sLast":     ">>",
                "sNext":     ">",
                "sPrevious": "<"
            },
            "sSearch": "Buscar:"
        },
        "pagingType": "full_numbers"
    });

    // question selector:
    $("#btn-serv, #btn-recom").unbind("click").click(function() {
        var v = $(this).val();
        globalQuestion = v;
        var id = $(this).attr("id");

        if(id == "btn-serv") {
            $(this).attr("class", "btn btn-xs btn-primary");
            $("#btn-recom").attr("class", "btn btn-xs btn-default");
        } else if(id == "btn-recom") {
            $(this).attr("class", "btn btn-xs btn-primary");
            $("#btn-serv").attr("class", "btn btn-xs btn-default");
        }

        Liverpool.loading("show");
        var newPars = $.extend(true, {}, globalParams);
        newPars.idQuestion = v;

        var parsNps = {
            group: params.groupId,
            business: params.businessId,
            zone: params.zoneId,
            store: params.storeId,
            man: params.managerId,
            chief: params.bossId,
            section: params.sectionId,
            op: newPars.idQuestion,
            startDate: from,
            endDate: to
        };

        $.ajax({
        url: baseUrl + "nps/getNpsCompanyData",
            data: parsNps
        }).done(function(data, textStatus, jqXHR) {
            nps = data;
            acumCurrCircle = data.npsCompany[3] || 0;
            smallTable1 = data.npsCompany[0] || 0;
            smallTable2 = data.npsCompany[1] || 0;
            smallTable3 = data.npsCompany[2] || 0;
          
            $.ajax({
                url: baseUrl + "HistoryReport/getLevelData",
                data: newPars
            }).done(function(data, textStatus, jqXHR) {
                loadInfo(data, newPars, nps);
            }).fail(function(jqXHR, textStatus, errorThrown) {
                console.log(textStatus, errorThrown);
            });
        }).fail(function(jqXHR, textStatus, errorThrown) {
            console.log(textStatus, errorThrown);
        });

    });

    Liverpool.loading("hide");
}

function mergeObjects(obj1) {
        
    for(var k in obj1) {
        var row = obj1[k];
        
        if(!totalObjects[k]) {
            totalObjects[k] = {
                data: [],
                groupName: row.groupName,
                levelId : k
            };
        }
    }
}

function mergeObjectsB(obj1) {
        
    for(var k in obj1) {
        var row = obj1[k];
        
        if(!totalObjectsB[k]) {
            totalObjectsB[k] = {
                data: [],
                groupName: row.groupName,
                levelId : k
            };
        }
    }
}

function rowToObject(data, params, isStore, isSelect) {

    var dataObject = {};

    for(var i = 0; i < data.length; i++) {
        var elem = data[i];
        var id = elem[0];

        if(!dataObject[id]) {
            dataObject[id] = {
                data: [],
                groupName: elem[1]
            };
        }

        var mn = elem[2];
        var m = months[mn - 1];
        var score = elem[3];

        // data layouts changes for "zone";
        /*if(params.currLevel == "business") {
            dataObject[id].groupName = elem[0];
            mn = elem[1];
            m = months[mn - 1];
            score = elem[2];
        }

        if(isStore) {
            dataObject[id].groupName = elem[1];
            mn = elem[2];
            m = months[mn - 1];
            score = elem[3];
        }*/
        
        if (elem.length === 4) {
            dataObject[id].groupName = elem[1];
            mn = elem[2];
            m = months[mn - 1];
            score = elem[3];
        }
        
        if (elem.length === 3) {
            dataObject[id].groupName = elem[0];
            mn = elem[1];
            m = months[mn - 1];
            score = elem[2];
        }

        dataObject[id].data.push({
            month: m,
            monthNumber: mn,
            score: score
        });

        dataObject[id].levelId = id;
    }

    return dataObject;
}

function getHistoryReport(params) {
    var action = baseUrl + 'historyreport/getLevelData';
    Liverpool.loading("show");
    // load general nps chart data:
    $.ajax({
        url: baseUrl + "nps/getNpsCompanyData",
        data: {
            group: params.groupId,
            business: params.businessId,
            zone: params.zoneId,
            store: params.storeId,
            man: params.managerId,
            chief: params.bossId,
            section: params.sectionId,
            op: globalQuestion,
            startDate: from,
            endDate: to
        }
    }).done(function(data, textStatus, jqXHR) {
        nps = data;
        // time line:
        acumCurrCircle = data.npsCompany[3] || 0;
        smallTable1 = data.npsCompany[0] || 0;
        smallTable2 = data.npsCompany[1] || 0;
        smallTable3 = data.npsCompany[2] || 0;
     
        timeLineLinks.push({
            id: params.level,
            url: action,
            params: params,
            index: timeLineLinks.length + 1,
            tag: params.labelTimeLine || "Compañía"
        });
        
        if (params.level === 'zone' && timeLineLinks.length > 4) {
            timeLineLinks[3].id = params.level;
            timeLineLinks[3].url = action;
            timeLineLinks[3].params = params;
            timeLineLinks[3].tag = params.labelTimeLine || "Compañía";
            timeLineLinks.pop();
        }
        
        renderTimeLine(timeLineLinks);

        globalParams = params;

        // load table data:
        $.ajax({
            url: action,
            data: params,
            success: function(dataMain) {
                // small table "acumulado":
                $.ajax({
                    url: baseUrl + "HistoryReport/getAcumulados",
                    data: params,
                    success: function(data){
                        $("#lblPrevYear").html(dataMain.prevYear);
                        $("#lblCurrYear").html(dataMain.currYear);
                        $("#acumCurrYear").html(data[0] || '-');
                        $("#acumPrevYear").html(data[1] || '-');

                        currYear = dataMain.currYear;
                        prevYear = dataMain.prevYear;
                        loadInfo(dataMain, params, nps);
                    }
                });
            }
        });
    }).fail(function(jqXHR, textStatus, errorThrown) {
        console.log(textStatus, errorThrown);
    });
}

function renderTimeLine(line) {
    $("#timeline-div").html("");

    for(var i = 0; i < line.length; i++) {
        var item = line[i];
        $div = $("<div class='timeline-item'></div>");
        $div.attr("data-item-id", item.id);
        
        $div.html(
            "<span class='glyphicon glyphicon-chevron-right'></span>" +
            "&nbsp;&nbsp;" +
            item.tag
        );

        (function(div, pars) {
            div.click(function() {
                // delete from that point forwards:
                var startDeleting = false;
                $("#timeline-div .timeline-item").each(function(i, v) {
                    if(div.data("item-id") == $(v).data("item-id")) {
                        startDeleting = true;
                    }

                    if(startDeleting) {
                        $(v).remove();
                        timeLineLinks.splice(i);
                    }
                });


                getHistoryReport(pars);
            });
        })($div, item.params);

        $("#timeline-div").append($div);
    }
}

$(function() {         
    getHistoryReport({});

    setTimeout(function() {
        $("#apply-date").unbind("click").click(function () {
            $('.dropdown-date').removeClass('open');

            var context = window.location.pathname.split('/');
            var div = fwt.query(".fwt-tab-active-up", false, document);
            var option = div.textContent;

            if (option == 'Trimestral') {

                var year = $("#date-year").data('datepicker')
                        .getFormattedDate('yyyy');
                var div2 = fwt.query("button.btn.btn-xs.btn-primary.btn-long"
                        , false, document);
                var months = div2.textContent.trim().split('-');
                var m = 0;
                var m2 = 0;

                if (months[0].trim() == 'Ene') {
                    m = '01';
                } else if (months[0].trim() == 'Abr') {
                    m = '04';
                } else if (months[0].trim() == 'Jul') {
                    m = '07';
                } else if (months[0].trim() == 'Oct') {
                    m = '10';
                }
                if (months[1].trim() == 'Mar') {
                    m2 = '03';
                } else if (months[1].trim() == 'Jun') {
                    m2 = '06';
                } else if (months[1].trim() == 'Sep') {
                    m2 = '09';
                } else if (months[1].trim() == 'Dic') {
                    m2 = '12';
                }
                from = '01/' + m + '/' + year;
                to = daysInMonth(m2, year) + '/' + m2 + '/' + year;

            } else if (option == 'Mensual') {

                var date = $("#date-and-month").data('datepicker').
                        getFormattedDate('mm/yyyy');
                var dateS = date.split("/");
                var days = daysInMonth(dateS[0], dateS[1]);
                from = '01' + '/' + date;
                to = days + '/' + date;

            } else if (option == 'Periodo') {

                from = $("#date-from").data('datepicker').
                        getFormattedDate('dd/mm/yyyy');
                to = $("#date-to").data('datepicker').
                        getFormattedDate('dd/mm/yyyy');
            }
            
            localStorage.setItem("from", from);
            localStorage.setItem("to", to);
            localStorage.setItem("period",option);
            
            if (typeof Tracking !== 'undefined') {
                Tracking.changeFilterDate(from, to, option);
                return;
            }

            var link = timeLineLinks[timeLineLinks.length - 1];
            var params = link.params;
            params.startDate = from;
            params.endDate = to;
            
            $(".timeline-item:nth-child(" + (timeLineLinks.length) + ")").click();
            
            $('#regM').html(option);
            $('#range').html(from + ' - ' + to);
        });
    }, 3000);
});
