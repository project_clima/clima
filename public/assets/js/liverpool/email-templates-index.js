var EmailTemplateIndex = function() {
    var oTable   = null;
    
    var bindEvents = function () {
        $(function() {
            $('.table-lp').on('click', '.preview', function (e) {
                e.preventDefault();
                preview($(this).attr('href'));
            });
        });
    };

    var setTable = function(url) {
        var editUrl    = $('.table-lp').data('edit');
        var previewUrl = $('.table-lp').data('preview');
        var source     = $('.table-lp').data('source');
        
        oTable = $('.table-lp').dataTable({
            "iDisplayLength": 5,
            "bFilter": false,
            "bLengthChange": false,
            "bAutoWidth": false,
            "bDestroy": true,
            "bSort": false,
            "bInfo": false,
            "oLanguage": {
                "sZeroRecords": "Ningún Registro",
                "sInfo": "Mostrando Encuestas de _START_ al _END_ de _TOTAL_ totales",
                "sInfoEmpty": "0 Encuestas",
                 "sPaginatePrevious": "Previous page",
                 "sProcessing": "Cargando...",
                 "oPaginate": {
                    "sFirst":    "<<",
                    "sLast":     ">>",
                    "sNext":     ">",
                    "sPrevious": "<"
                }
            },
            "fnCreatedRow": function(nRow, aData, iDataIndex) {
                var place = Liverpool.getSearchElem("place");

                var hrefPreview = previewUrl + '?surveyTypeId=' + aData[2];
                var hrefEdit = editUrl + '?surveyTypeId=' + aData[2];

                var preview = '<a href="' + hrefPreview + '" class="btn btn-sm btn-green preview" title="Vista Previa"><i class="glyphicon glyphicon-eye-open"></i></a>';
                var edit    = '<a href="' + hrefEdit + '" class="btn btn-sm btn-blue edit-template" title="Editar"><i class="glyphicon glyphicon-pencil"></i></a>';
                
                $('td:eq(2)', nRow).html(preview + edit).addClass('text-right');
            },
            "bProcessing": true,
            "bServerSide": true,
            "bStateSave" : false,
            "sAjaxSource": source,
            "pagingType": "full_numbers"
        });
    };
    
    var preview = function(url) {
        $.get(url, function (data) {            
            $('#preview-iframe').attr('src', 'data:text/html,' + escape(data));
            $('#mdl-preview').modal('show');
        });
    };

    var init = function() {
        bindEvents();
        setTable();
    };

    init();
}();