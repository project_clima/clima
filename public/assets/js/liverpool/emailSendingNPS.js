$(document).ready(function() {
    
    $(function(){
            $('#mail-create-form').on('submit', function(e){
                e.preventDefault();   
                var data = new FormData(this);
                $.ajax({
                	url: baseUrl + 'emailSendingNPS/uploadFile',
                    type: 'POST', //or POST
                    processData: false,
                    contentType: false,
                    cache: false,
                    data: data,
                    success: function(data){
                    	var idResponse = data['idResponse'];
                    	
                    	if(idResponse == 500) {
                    		Liverpool.setMessage($('.inner-message'), data['errorMessage'], 'warning');
                    	}
                    },
                     beforeSend: function() {
                		Liverpool.loading("show");
            		},
            		complete: function () {
                		Liverpool.loading("hide");
                		Liverpool.scrollTo($('.message'));
            		}
                });
            });
        });
    
	
    $("#date-from-email-nps").datepicker({
        format: "dd/mm/yyyy",
        language: 'es',
        todayHighlight: true,
        autoclose: true
    });

    $("#date-to-email-nps").datepicker({
        format: "dd/mm/yyyy",
        language: 'es',
        todayHighlight: true,
        autoclose: true
    });

    $("#preview").click(function() {
        BootstrapDialog.show({
            title: 'Título',
            message: 'Contenido del email aquí.'
        });
    });
    
});

$('#uploadLayout').click(function () {
	$('#mdl-upload-layout').modal('show');
});

function validateUpload() {	
    $('#mail-create-form').validate({
        'ignore': '',
        'rules': {
            'npsFile': {
                required: true                
            }            
          },
        'messages': {
            'npsFile': {
                'required': 'Por favor, seleccione un archivo'
            }
        }
    }).form();
    
    if($("#mail-create-form").valid()) {
		return true; 			
	} else {
		return false;
	}
};

