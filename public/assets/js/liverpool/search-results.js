var SearchResults = function() {
    var oTable   = null;
        
    var bindEvents = function() {
        $('#structureDate').change(function () {
            setTable($('#search').val(), $('#structureDate').val());
        });
        
        $('#search-btn').click(function () {
            setTable($('#search').val(), $('#structureDate').val());
        });
        
        $('#search').keypress(function (e) {
            if(e.which === 13)
            {
                setTable($(this).val(), $('#structureDate').val());
            }
        });
        
        $('#export').click(function () {
            var search = $('#search').val();
            var date   = $('#structureDate').val();
            
            var url = baseUrl + 'searchresults/export?search=' + search + '&date=' + date;
            
            window.location.href = url;
        });
        
        setTable($('#search').val(), $('#structureDate').val());
    };

    var setTable = function(search, date) {
        Liverpool.loading('show');
        
        if (oTable !== null) {
            oTable.fnDestroy();
        }
                
        oTable = $('#search-table').dataTable({
            "iDisplayLength": 10,
            "bFilter": false,
            "bLengthChange": false,
            "bAutoWidth": false,
            "bDestroy": true,
            "bSort": false,
            "bInfo": false,
            "oLanguage": {
                "sZeroRecords": "Ningún Registro",
                "sInfo": "Mostrando registros de _START_ al _END_ de _TOTAL_ totales",
                "sInfoEmpty": "0 Registros",
                 "sPaginatePrevious": "Previous page",
                 "sProcessing": "Cargando...",
                 "oPaginate": {
                    "sFirst":    "<<",
                    "sLast":     ">>",
                    "sNext":     ">",
                    "sPrevious": "<"
                }
            },
            "bProcessing": true,
            "bServerSide": true,
            "bStateSave" : false,
            "sAjaxSource": baseUrl + 'searchresults/results?search=' + search + '&date=' + date,
            "pagingType": "full_numbers",
            "fnDrawCallback": function () {
                Liverpool.loading('hide');
            }
        });
    };

    bindEvents();
}();


