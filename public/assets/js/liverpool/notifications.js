function checkIfNeedsReview() {
	$.ajax({
		url: baseUrl + "notifications/needsreview"
	}).done(function(data) {
		if(data == "true") {
			swal({
				title: "Mensaje",
				text: "Hay planes de acción pendientes de revisión",
				type: "warning",
				confirmButtonText: "OK",
				}, function() {
					checkIfHasRejected();
				}
			);
		} else {
			checkIfHasRejected();
		}
	}).fail(function(jqXHR, textStatus, errorThrown) {
		console.log(errorThrown);
		Liverpool.loading("hide");
	});
}

function checkIfHasRejected() {
	$.ajax({
		url: baseUrl + "notifications/hasrejected"
	}).done(function(data) {
		if(data == "true") {
			swal({
				title: "Mensaje",
				text: "Hay planes de acción con estatus rechazado",
				type: "warning",
				confirmButtonText: "OK",
				}, function() {}
			);
		}
	}).fail(function(jqXHR, textStatus, errorThrown) {
		console.log(errorThrown);
		Liverpool.loading("hide");
	});
}

checkIfNeedsReview();
