var nombreEmpleado;
var funcionEmpleado;

var homeAction = "No ve nada";
var evalAction = "No ve nada";
var impAction = "No ve nada";
var segAction = "No ve nada";
var replAction = "No ve nada";
var repoAction = "No ve nada";
var notiAction = "No envía correo";
var hisAction = "No ve nada";

var homeVal = 0;
var evalVal = 0;
var impVal = 0;
var segVal = 0;
var replVal = 0;
var repoVal = 0;
var notiVal = 0;
var hisVal = 0;

$(document).ready(function() {
	$("#select-home").append(llenarCombo());
	$("#select-historico").append(llenarCombo());
    $("#select-evaluaciones").append(llenarCombo());
    $("#select-replica").append(
    		"<option value=4>Sí puede contestar todo</option>" +
    		"<option value=3>Sí puede contestar solo lo que le corresponde</option>" +
    		llenarCombo());
    $("#select-reporte").append(llenarCombo());
    $("#select-notificacion").append(
    		"<option value=1> Sí envía correo</option>"+
    		"<option value=0>No envía correo</option>");
    $("#select-impugnaciones").append(llenarComboRealizar("impugnaciones ")+
    		llenarCombo()+
    		"<option value=6>Sí realiza y contesta impugnaciones de todo</option>"+
    		"<option value=5>Sí realiza y contesta impugnaciones solo lo que le corresponde</option>");
    $("#select-seguimiento").append(llenarComboRealizar("seguimiento ")+
    		llenarCombo());
    
    $("#btn-validar").click(
       	function(){
       		var emp = $("#employee-number").val();
       		if(emp != 0 || emp != null || emp != ''){
       			$("#admin").css('display', 'inline');
       			$.ajax({
           			url: 'validateEmployeeNumber',
           			type: 'POST',
           			data: {employeeNumber: emp},
           			success:function(response){
           				if(response.length>0) {
           					isAdmin($("#employee-number").val());
           					$("#select-funciones").attr("disabled", true);
           					$("#btn-borrar-empleado").show();
           					$("#btn-validar").hide();
           					nombreEmpleado = response[0][1] + ' ' + response[0][2];
           					funcionEmpleado = response[0][3];
           				}else{
           					swal("", "El emplado no esta en la estructura.", "warning");
           					//Liverpool.setMessage($('.inner-message'), "El emplado no esta en la estructura", 'warning');
           				}
           			}
           		});
       			
          		$.ajax({
          			url: 'getEmployeeRoles',
           			type: 'POST',
           			data: {employeeNumber: $("#employee-number").val()},
           			success:function(response){
           				homeAction = "No ve nada";
           				evalAction = "No ve nada";
           				impAction = "No ve nada";
           				segAction = "No ve nada";
           				replAction = "No ve nada";
           				repoAction = "No ve nada";
           				notiAction = "No envía correo";
           				hisAction = "No ve nada";
                  
           				if(response.length>0){
           					$("#by-func-table").css('display', 'none');
           					$("#indiv-table").css('display', 'inline');
           					
           					revisarRoles(response);
           					mostrarRolesIndiv();
           					
           				}else{
           					$("#indiv-table").css('display', 'inline');
           					$("#select-home").val(-1);
           					$("#select-evaluaciones").val(-1);
           					$("#select-replica").val(-1);
           					$("#select-reporte").val(-1);
           					$("#select-notificacion").val(-1);
           					$("#select-impugnaciones").val(-1);
           					$("#select-seguimiento").val(-1);
           					$("#select-historico").val(-1);
           					$("#indiv-table tbody").append(
           							"<tr id='tr-"+$("#employee-number").val()+"'>" +
           								"<td>" + $("#employee-number").val()+ "</td>"+
           								"<td>" + nombreEmpleado + "</td>" +
           								"<td>" + funcionEmpleado + "</td>" +
           								"<td style= 'text-align: center;'>" + '--' + "</td>" +
           								"<td style= 'text-align: center;'>" + '--' + "</td>" +
           								"<td style= 'text-align: center;'>" + '--' + "</td>" +
           								"<td style= 'text-align: center;'>" + '--' + "</td>" +
           								"<td style= 'text-align: center;'>" + '--' + "</td>" +
           								"<td style= 'text-align: center;'>" + '--' + "</td>" +
           								"<td style= 'text-align: center;'>" + '--' + "</td>" +
                          "<td style= 'text-align: center;'>" + '--' + "</td>" +
           							"</tr>"
           							);
           				}
           			}
          		});
          		
          		$("#select-funciones").attr("disabled", true);
        		$("#btn-borrar-empleado").show();
        		$("#btn-validar").hide();
            $("#by-func-table").css('display', 'none');
       		}else{
       			emptyEmp();
       		}
       	}
    );
    
    $("#btn-borrar-empleado").click(
    	function(){
    		$("#select-funciones").attr("disabled", false);
    		$("#admin").css('display', 'none');
			$("#btn-borrar-empleado").hide();
			$("#btn-validar").show();
			$("#indiv-table tbody > tr").remove();
			$("#employee-number").val("");
			$("#select-home").val(-1);
			$("#select-evaluaciones").val(-1);
			$("#select-replica").val(-1);
			$("#select-reporte").val(-1);
			$("#select-notificacion").val(-1);
			$("#select-impugnaciones").val(-1);
			$("#select-seguimiento").val(-1);
			$("#select-admin").val(-1);
			$("#select-historico").val(-1);
    	}
    );
    
    $("#btn-borrar-funcion").click(
       	function(){
       		$("#employee-number").attr("disabled", false);
    		$("#btn-borrar-funcion").hide();
    		$("#select-funciones").val("");
    		$("#by-func-table tbody > tr").remove();
    		$("#btn-validar").attr("disabled", false);
    		$("#select-home").val(-1);
			$("#select-evaluaciones").val(-1);
			$("#select-replica").val(-1);
			$("#select-reporte").val(-1);
			$("#select-notificacion").val(-1);
			$("#select-impugnaciones").val(-1);
			$("#select-seguimiento").val(-1);
			$("#select-historico").val(-1);
       	}
    );
    
    $("#select-funciones").change(
      function(){
    		if($("#select-funciones").val() == ''){
    			$("#btn-borrar-funcion").click();
    		}else{
    			$("#by-func-table tbody > tr").remove();
    			$.ajax({
          			url: 'getFunctionRoles',
           			type: 'POST',
           			data: {
           				functionName:$("#select-funciones option:selected").text(),
           				idFunction:$("#select-funciones").val()
           			},
           			success:function(response){
           				homeAction = "No ve nada";
           				evalAction = "No ve nada";
           				impAction = "No ve nada";
           				segAction = "No ve nada";
           				replAction = "No ve nada";
           				repoAction = "No ve nada";
           				notiAction = "No envía correo";
           				hisAction = "No ve nada";
           				
           				if(response.length>0){
           					revisarRoles(response);
           					
           					$("#by-func-table").css('display', 'inline');
           					$("#indiv-table").css('display', 'none');
           					
           					mostrarRolesFuncion();
           				}else{
           					$("#by-func-table").css('display', 'inline');
           					$("#indiv-table").css('display', 'none');
           					$("#by-func-table tbody").append(
           							'<tr><td colspan="8" style="text-align: center;">Sin resultados</td></tr>'
           					);
           					$("#select-home").val(-1);
           					$("#select-evaluaciones").val(-1);
           					$("#select-replica").val(-1);
           					$("#select-reporte").val(-1);
           					$("#select-notificacion").val(-1);
           					$("#select-impugnaciones").val(-1);
           					$("#select-seguimiento").val(-1);
           					$("#select-historico").val(-1);
           				}
           			}
          		});
    			
    			$("#employee-number").attr("disabled", true);
        		$("#btn-borrar-funcion").show();
        		$("#btn-validar").attr("disabled", true);
    		}
    	}
    );
    
    $.ajax({
        url: baseUrl + 'useradminnps/getNPSFunctions',
        type:'POST',
        success: function (data) {
           for(var i = 0; i < data.length; i++) {
        	   $("#select-funciones").append("<option value=" + data[i][1] + ">" + data[i][0]  + "</option>");
           }
        }
    });
});

isAdmin = function(employeeNumber){
	$.ajax({
		url: 'getIsAdmin',
		type: 'POST',
		data: {employeeNumber: employeeNumber},
		success:function(response){
			if(response == 'ok'){
				$("#select-admin").val(1);
			}else{
				$("#select-admin").val(2)
			}
		}
	});
}

llenarCombo = function(){
	return "<option value=2>Sí ve todo</option>"+
			"<option value=1>Sí ve solo lo que le corresponde </option>"+
			"<option value=0>No ve nada </option>";
}

llenarComboRealizar = function(complemento){
	return "<option value=4>Sí realiza "+complemento+"de todo</option>"+
			"<option value=3>Sí realiza "+complemento+" solo lo que corresponde</option>";
}

agregarRol = function(){
	homeAction = "No ve nada";
	evalAction = "No ve nada";
	impAction = "No ve nada";
	segAction = "No ve nada";
	replAction = "No ve nada";
	repoAction = "No ve nada";
	notiAction = "No envía correo";
	hisAction = "No ve nada";
	
	if($("#select-funciones").val() == null || $("#select-funciones").val() == '' && $("#employee-number").val() == '' || $("#employee-number").val() == null){
		emptyEmployee();
	}else{
		if($("#select-funciones").val() == null || $("#select-funciones").val() == '' || $("#select-funciones").val() < 0){
			$("#by-func-table").css('display', 'none');
			$("#indiv-table").css('display', 'inline');
			if($("#employee-number").val() == '' || $("#employee-number").val() == null){
			}else{
				if($("#select-admin").val() < 0 ||
						$("#select-home").val() < 0 ||  
						$("#select-evaluaciones").val() < 0 ||   
						$("#select-replica").val() < 0 || 
						$("#select-reporte").val() < 0 || 
						$("#select-notificacion").val() < 0 || 
						$("#select-impugnaciones").val() < 0 ||
						$("#select-historico").val() < 0 || 
						$("#select-seguimiento").val() < 0 ){
							emptyCombos();
				}else{
					asignarPermisosIndividual();
					mostrarRolesIndiv();
				}
			}
		}else{
			if($("#select-home").val() < 0 ||  
					$("#select-evaluaciones").val() < 0 ||   
					$("#select-replica").val() < 0 || 
					$("#select-reporte").val() < 0 || 
					$("#select-notificacion").val() < 0 || 
					$("#select-impugnaciones").val() < 0 || 
					$("#select-historico").val() < 0 ||
					$("#select-seguimiento").val() < 0 ){
						emptyCombos();
			}else{
				$("#by-func-table").css('display', 'inline');
				$("#indiv-table").css('display', 'none');
				asignarPermisosFuncion();
				mostrarRolesFuncion();
			}
		
		}
	}
	
//	if($("#select-funciones").val()>0){
//		$("#by-func-table").css('display', 'inline');
//		$("#indiv-table").css('display', 'none');
//		asignarPermisosFuncion();
//		mostrarRolesFuncion();
//		
//	}else{
//		emptySelect();
//		$("#by-func-table").css('display', 'none');
//		$("#indiv-table").css('display', 'inline');
//		if($("#employee-number").val() == '' || $("#employee-number").val() == null){}
//		else{
//			asignarPermisosIndividual();
//			mostrarRolesIndiv();
//		}
//	}
}

emptyCombos = function () {
    swal({
        title: "",
        text: "Debe seleccionar un permiso válido para cada uno de los módulos",
        type: "warning", 
        //showCancelButton: true,
        confirmButtonText: "Aceptar",
        closeOnConfirm: false
    });

}

emptyEmployee = function () {
    swal({
        title: "",
        text: "Ingrese un número de empleado valido o seleccione una función",
        type: "warning", 
        //showCancelButton: true,
        confirmButtonText: "Aceptar",
        closeOnConfirm: false
    });

}
emptyEmp = function () {
    swal({
        title: "",
        text: "Ingrese un número de empleado valido",
        type: "warning", 
        //showCancelButton: true,
        confirmButtonText: "Aceptar",
        closeOnConfirm: false
    });

}


mostrarRolesIndiv = function(){
	$.ajax({
		url:'getEmployeeRoles',
		type:'POST',
		data:{employeeNumber:$("#employee-number").val()},
		success: function(data){
			for(var i=0;i<data.length;i++){
				VerificarAccion(data[i]);
			}
			
			if(!$("#tr-"+$("#employee-number").val()).length){
				$("#indiv-table tbody").append(
						"<tr id='tr-"+$("#employee-number").val()+"'>" +
							"<td>" + $("#employee-number").val()+ "</td>"+
							"<td>" + nombreEmpleado + "</td>" +
							"<td>" + funcionEmpleado + "</td>" +
							"<td>" + homeAction + "</td>" +
							"<td>" + evalAction + "</td>" +
							"<td>" + impAction + "</td>" +
							"<td>" + segAction + "</td>" +
							"<td>" + replAction + "</td>" +
							"<td>" + repoAction + "</td>" +
							"<td>" + notiAction + "</td>" +
							"<td>" + hisAction + "</td>" +
						"</tr>"
						);
			}else{
				$("#tr-"+$("#employee-number").val()).html('');
				$("#tr-"+$("#employee-number").val()).html(
						"<td>" + $("#employee-number").val()+ "</td>"+
						"<td>" + nombreEmpleado + "</td>" +
						"<td>" + funcionEmpleado + "</td>" +
						"<td>" + homeAction + "</td>" +
						"<td>" + evalAction + "</td>" +
						"<td>" + impAction + "</td>" +
						"<td>" + segAction + "</td>" +
						"<td>" + replAction + "</td>" +
						"<td>" + repoAction + "</td>" +
						"<td>" + notiAction + "</td>"+
						"<td>" + hisAction + "</td>");
			}
		}
	});
}

mostrarRolesFuncion = function(){
	var idFuncion = $("#select-funciones").val();
	var nameFunction = $("#select-funciones option:selected").text();
	
	$.ajax({
		url: 'getFunctionRoles',
		type:'POST',
		data: {
			functionName:nameFunction,
			idFunction:idFuncion
		},
		success: function (data) {
			for(var i=0;i<data.length;i++){
				VerificarAccion(data[i]);
			}
			
			if(!$("#tr-func-" + idFuncion).length){
				$("#by-func-table tbody").append(
						"<tr id='tr-func-"+idFuncion+"'>" +
							"<td>" + nameFunction+ "</td>"+
							"<td>" + homeAction + "</td>" +
							"<td>" + evalAction + "</td>" +
							"<td>" + impAction + "</td>" +
							"<td>" + segAction + "</td>" +
							"<td>" + replAction + "</td>" +
							"<td>" + repoAction + "</td>" +
							"<td>" + notiAction + "</td>" +
							"<td>" + hisAction + "</td>" +
						"</tr>"
						);
			}else{
				$("#tr-func-" + idFuncion).html('');
				$("#tr-func-" + idFuncion).html(
						"<td>" + nameFunction+ "</td>"+
						"<td>" + homeAction + "</td>" +
						"<td>" + evalAction + "</td>" +
						"<td>" + impAction + "</td>" +
						"<td>" + segAction + "</td>" +
						"<td>" + replAction + "</td>" +
						"<td>" + repoAction + "</td>" +
						"<td>" + notiAction + "</td>" +
						"<td>" + hisAction + "</td>");
			}
		}
	});
}


revisarRoles = function(data){
	homeVal = 0;
	evalVal = 0;
	impVal = 0;
	segVal = 0;
	replVal = 0;
	repoVal = 0;
	notiVal = 0;
	hisVal = 0;
	
	for(var i=0; i<data.length;i++){
		VerificarAccion(data[i]);
	}
	
	$("#select-home").val(homeVal);
    $("#select-evaluaciones").val(evalVal);
    $("#select-replica").val(replVal);
    $("#select-reporte").val(repoVal);
    $("#select-notificacion").val(notiVal);
    $("#select-impugnaciones").val(impVal);
    $("#select-seguimiento").val(segVal);
    $("#select-historico").val(hisVal);
}

VerificarAccion = function(data){
	if(data==105)	{homeAction = "Sí ve todo"; homeVal =2}
	if(data==106)	{homeAction = "Sí ve solo lo que le corresponde"; homeVal =1}
	if(data==107)	{evalAction = "Sí ve todo"; evalVal=2}
	if(data==108)	{evalAction = "Sí ve solo lo que le corresponde"; evalVal=1}
	if(data==104)	{repoAction = "Sí ve todo"; repoVal=2}
	if(data==124)	{repoAction = "Sí ve solo lo que le corresponde"; repoVal=1}
	if(data==109)	{impAction = "Sí realiza impugnaciones de todo"; impVal= 4}
	if(data==110)	{impAction = "Sí realiza impugnaciones solo lo que corresponde"; impVal=3}
	if(data==111)	{impAction = "Sí ve todo"; impVal=2}
	if(data==112)	{impAction = "Sí ve solo lo que le corresponde"; impVal=1}
	if(data==113)	{impAction = "Sí realiza y contesta impugnaciones de todo"; impVal=6}
	if(data==114)	{impAction = "Sí realiza y contesta impugnaciones solo lo que corresponde"; impVal=5}
	
	if(data==115)	{segAction = "Sí realiza seguimiento de todo"; segVal=4}
	if(data==116)	{segAction = "Sí realiza seguimiento solo lo que corresponde"; segVal=3}
	if(data==117)	{segAction = "Sí ve todo"; segVal=2}
	if(data==118)	{segAction = "Sí ve solo lo que corresponde"; segVal=1}
	
	if(data==120)	{replAction = "Sí puede contestar solo lo que le corresponde"; replVal=3}
	if(data==127)	{replAction = "Sí puede contestar todo"; replVal=4}
	if(data==126)	{replAction = "Sí envia replica solo lo que le corresponde";replVal=1}
	if(data==125)	{replAction = "Sí envia replica de todo"; replVal=2}
	if(data==93)	{notiAction = "Sí envía correo"; notiVal=1}
	if(data==100)	{hisAction = "Sí ve todo"; hisVal=2}
	if(data==123)	{hisAction = "Sí ve solo lo que le corresponde"; hisVal=1}
}

asignarPermisosFuncion = function(){
	var home  = $("#select-home").val();
	var evaluaciones = $("#select-evaluaciones").val();
	var impugnaciones = $("#select-impugnaciones").val();
	var seguimiento = $("#select-seguimiento").val();
	var replica = $("#select-replica").val();
	var reporte = $("#select-reporte").val();
	var notificaciones = $("#select-notificacion").val();
	var historico = $("#select-historico").val();
	var nameFunction = $("#select-funciones option:selected").text();
	var idFunction = $("#select-funciones").val();
	$.ajax({
        url: 'asignarPermisosPorFuncion',
        type:'POST',
        data: {
        	home :home,
        	evaluaciones :evaluaciones,
        	impugnaciones: impugnaciones,
        	seguimiento : seguimiento,
        	replica : replica,
        	reporte : reporte,
        	notificaciones: notificaciones,
        	historico : historico,
          nameFunction : nameFunction,
          idFunction : idFunction
        },
        beforeSend: function (){
         	Liverpool.loading("show");
         },
         complete: function (){
         	Liverpool.loading("hide");
         },
        success: function (data) {
        	swal("", "Guardó cambios" + data, "success");
        	//alert("Guardó cambios" + data);
        },
        error: function(){
        	swal("", "Ocurrio un error", "error");
        	//alert("Hubo un error")
        }
	});
}

asignarPermisosIndividual = function(){
	var admin = $("#select-admin").val()
	var home  = $("#select-home").val();
	var evaluaciones = $("#select-evaluaciones").val();
	var impugnaciones = $("#select-impugnaciones").val();
	var seguimiento = $("#select-seguimiento").val();
	var replica = $("#select-replica").val();
	var reporte = $("#select-reporte").val();
	var notificaciones = $("#select-notificacion").val();
	var historico = $("#select-historico").val();
	var employeeNumber = $("#employee-number").val();
	
	$.ajax({
        url: 'asignarPermisoIndividual',
        type:'POST',
        data: {
        	admin :admin,
        	home :home,
        	evaluaciones :evaluaciones,
        	impugnaciones: impugnaciones,
        	seguimiento : seguimiento,
        	replica : replica,
        	reporte : reporte,
        	notificaciones: notificaciones,
        	historico : historico,
        	employeeNumber : employeeNumber
        },
        beforeSend: function (){
         	Liverpool.loading("show");
         },
         complete: function (){
         	Liverpool.loading("hide");
         },
        success: function (data) {
        	swal("", "Guardó cambios", "success");
        	//alert("Guardó cambios");
        },
        error:function(){
        	swal("", "Ocurrio un error", "error");
        	//alert("Hubo un error")
        }
	});
}
