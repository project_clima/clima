var Snapshots = function() {
    var oTable     = null;

    var bindEvents = function() {
        $(function () {
            $('#add').click(function () {
                showForm('Crear Foto', '', 'add');
            });
            
            $('.table-lp').on('click', '.edit', function () {
                showForm('Editar Foto', $(this).data('period'), 'edit');
            });
            
            $('.table-lp').on('click', '.delete', function () {
                var period = $(this).data('period');
                
                swal({
                    title: "",
                    text: "¿En realidad desea eliminar la foto de la estructura?",
                    type: "warning", 
                    html: true,
                    showCancelButton: true,
                    cancelButtonText: "Cancelar",
                    confirmButtonText: "Continuar",
                    closeOnConfirm: true
                }, function() {
                    deleteSnapshot(period);
                });
            });
            
             $("#edit-period").datepicker({
		format: "mm/yyyy",
		language: 'es',
		autoclose: true,
		startView: "months", 
		minViewMode: "months"
            });
            
            $('.input-group-addon').click(function(e) {
                $(this).siblings('.date-picker').focus();
            }).css({'cursor': 'pointer'});
        });
    };

    var showForm = function (title, data, type) {
        $('.period-option').hide().removeClass('hidden');
        $('#add-period, #edit-period').attr('disabled', 'disabled');
        
        $('#' + type + '-period-wrapper').show();
        $('#' + type + '-period').removeAttr('disabled');
        
        $('.inner-message').empty();
        $('#snapshot-create-form').trigger('reset');
        $('#mdl-add-edit').modal('show');
        $('#mdl-add-edit .modal-title').text(title);
        $('#mdl-add-edit #original-period').val(data);
        
        validate();
    };

    var validate = function() {
        $('#snapshot-create-form').validate({
            'messages': {
                'period': {
                    'required': 'Por favor, seleccione el periodo de la estructura'
                }
            },
            errorPlacement: function(error, element) {
                if (element.next().is('.input-group-addon')) {
                    error.insertAfter(element.next().parent());
                } else {
                    error.insertAfter(element);
                }
            },
            submitHandler: function (form) {
                save(form);
            }
        });
    };

    var save = function(form) {
        if ($('#period').val() === $('#original-period').val()) {
            $('#mdl-add-edit').modal('hide');
            return;
        }
        
        $.ajax({
            url: $(form).attr('action'),
            data: $(form).serialize(),
            type: 'post',
            dataType: 'json',
            success: function (response) {
                if (response.type === 'exists') {
                    confirmOverwrite(form);
                } else {
                    oTable.api().ajax.reload();
                    $('#mdl-add-edit').modal('hide');
                    Liverpool.setMessage($('.message'), "Foto de estructura guardada correctamente", 'success');
                }
            },
            beforeSend: function () {
                Liverpool.loading('show');
                $('button', $(form)).attr('disabled', 'disabled');
            },
            complete: function () {
                $('button', $(form)).removeAttr('disabled');
                Liverpool.loading('hide');
            },
            error: function (jqXHR, textStatus) {
                var message = 'La foto no pudo ser guardada. Por favor, intente nuevamente';
                
                if (jqXHR.status === 400) {
                    message = 'El periodo de la foto no puede ser modificado. Actualmente se encuentra en uso.';
                }
                
                if (jqXHR.status === 422) {
                    message = 'Ya existe otra foto de estructura para el periodo seleccionado.';
                }
                
                Liverpool.setMessage($('.inner-message'), message, 'warning');
            }
        });
    };
    
    var confirmOverwrite = function (form) {        
        swal({
            title: "",
            text: "Ya existe una foto de la estructura para el periodo seleccionado. " +
                  "Haga clic en <strong>Continuar</strong> si desea reemplazarla o en <strong>Cancelar</strong> de lo contrario.",
            type: "warning", 
            html: true,
            showCancelButton: true,
            cancelButtonText: "Cancelar",
            confirmButtonText: "Continuar",
            closeOnConfirm: true
        }, function() {
            replace(form);
        });
    };
    
    var replace = function (form) {
        $.ajax({
            url: baseUrl + 'structuresnapshots/replace',
            data: $(form).serialize(),
            type: 'post',
            dataType: 'json',
            success: function (response) {
                oTable.api().ajax.reload();
                $('#mdl-add-edit').modal('hide');
            },
            beforeSend: function () {
                $('button', $(form)).attr('disabled', 'disabled');
                Liverpool.loading('show');
            },
            complete: function () {
                $('button', $(form)).removeAttr('disabled');
                Liverpool.loading('hide');
            },
            error: function () {
                Liverpool.setMessage($('.inner-message'), 'La foto no pudo ser creada. Por favor, intente nuevamente', 'warning');
            }
        });
    };
    
    var deleteSnapshot = function (period) {
        $.ajax({
            url: baseUrl + 'structuresnapshots/delete',
            data: {period: period},
            type: 'delete',
            dataType: 'json',
            success: function (response) {
                oTable.api().ajax.reload();
                Liverpool.setMessage($('.message'), "Foto eliminada correctamente", 'success');
            },
            beforeSend: function () {
                Liverpool.loading('show');
            },
            complete: function () {
                Liverpool.loading('hide');
            },
            error: function (jqXHR, textStatus) {
                var message = 'La foto no pudo ser eliminada. Por favor, intente nuevamente';
                
                if (jqXHR.status === 400) {
                    message = 'La foto no puede ser eliminada. Actualmente se encuentra en uso.';
                }
                
                Liverpool.setMessage($('.message'), message, 'danger');
            }
        });
    };

    var setTable = function(url) {
        oTable = $('.table-lp').dataTable({
            "iDisplayLength": 20,
            "bLengthChange": false,
            "bFilter": false,
            "bAutoWidth": false,
            "bDestroy": true,
            "bSort": false,
            "bInfo": false,
            "bPaginate": false,
            "oLanguage": {
                "sZeroRecords": "Ningún Registro",
                "sInfo": "_START_ al _END_ de _TOTAL_ totales",
                "sInfoEmpty": "0 registros",
                "sPaginatePrevious": "Previous page",
                "sProcessing": "Cargando...",
                "oPaginate": {
                    "sFirst":    "<<",
                    "sLast":     ">>",
                    "sNext":     ">",
                    "sPrevious": "<"
                },
                "sSearch": "Buscar:"
            },
            "fnCreatedRow": function(nRow, aData, iDataIndex) {                
                var editLink      = '<button data-period="' + aData + '" class="btn btn-sm btn-warning edit" title="Editar"><i class="glyphicon glyphicon-pencil link-glyp"></i></buttom>';
                                        
                var deleteLink    = '<button data-period="' + aData + '" class="btn btn-sm btn-red delete" title="Borrar"><i class="glyphicon glyphicon-trash link-glyp"></i></button>';
                
                $('td:eq(0)', nRow).html(aData);
                $('td:eq(1)', nRow)
                    .html(editLink + deleteLink)
                    .addClass('actions text-right')
                    .attr('nowrap', 'nowrap')
                    .css({'text-align': 'right'});
            },
            "bProcessing": false,
            "bServerSide": true,
            "bStateSave" : false,
            "sAjaxSource": baseUrl + 'structuresnapshots/list',
            "pagingType": "full_numbers"
        });
    };

    var init = function() {
        bindEvents();
        setTable();
    };

    init();
}();