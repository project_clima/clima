var EmailParametrization = function() {
    var oTable   = null;
    
    var bindEvents = function () {
        $(function () {
            autoComplete();
        });
    };

    var validate = function() {
        $('#survey-create-form').validate({
            'ignore': '',
            'messages': {
                'survey.name': {
                    'required': 'Por favor, ingrese el nombre de la encuesta'
                },
                'survey.dateInitial': {
                    'required': 'Por favor, indique la fecha inicial'
                },
                'survey.dateEnd': {
                    'required': 'Por favor, indique la fecha final'
                }
            },
            errorPlacement: function(error, element) {
                if (element.next().is('.input-group-addon')) {
                    error.insertAfter(element.next().parent());
                } else {
                    error.insertAfter(element);
                }
            },
            submitHandler: function (form) {
                save(form);
            }
        });
    };

    var setTable = function(url) {
        oTable = $('.table-lp').dataTable({
            "iDisplayLength": 5,
            "bFilter": false,
            "bLengthChange": false,
            "bAutoWidth": false,
            "bDestroy": true,
            "bSort": false,
            "bInfo": false,
            "oLanguage": {
                "sZeroRecords": "Ningún Registro",
                "sInfo": "Mostrando Encuestas de _START_ al _END_ de _TOTAL_ totales",
                "sInfoEmpty": "0 Encuestas",
                 "sPaginatePrevious": "Previous page",
                 "sProcessing": "Cargando...",
                 "oPaginate": {
                    "sFirst":    "<<",
                    "sLast":     ">>",
                    "sNext":     ">",
                    "sPrevious": "<"
                }
            },
            "fnCreatedRow": function(nRow, aData, iDataIndex) {
                $('td:eq(0)', nRow).html('<input value="' + aData[0] + '" type="checkbox" name="survey[]" />');
                $('td:eq(0)', nRow).addClass('text-center');
                
                $('td:eq(2)', nRow).html('<i class="glyphicon glyphicon-pencil"></i>').addClass('text-center');
                $('td:eq(3)', nRow).html('<i class="glyphicon glyphicon-lock"></i>').addClass('text-center');
            },
            "bProcessing": true,
            "bServerSide": true,
            "bStateSave" : false,
            "sAjaxSource": '/emailparametrization/list',
            "pagingType": "full_numbers"
        });
    };
    
    var autoComplete = function() {        
        $('#search-emails').autocomplete({
            autoFocus: true,
            minLength: 3,
            delay: 15,
            source: '/emailparametrization/getPositions',
            select: function(event, ui) {                
                $(this).val(ui.item.value);
                return false;
            },
            change: function(event, ui){
                $(this).val((ui.item ? ui.item.value : $(this).val()));
            }
        });
    };
    
    var init = function() {
        setTable();
        bindEvents();
    };

    init();
}();