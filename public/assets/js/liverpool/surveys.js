$.extend($.validator.messages, {
    required: "Campo requerido.",
    number: "Por favor, ingrese un valor numérico válido."
});

$.fn.dataTableExt.oApi.fnStandingRedraw = function(oSettings) {
    //redraw to account for filtering and sorting
    // concept here is that (for client side) there is a row got inserted at the end (for an add)
    // or when a record was modified it could be in the middle of the table
    // that is probably not supposed to be there - due to filtering / sorting
    // so we need to re process filtering and sorting
    // BUT - if it is server side - then this should be handled by the server - so skip this step
    if(oSettings.oFeatures.bServerSide === false){
        var before = oSettings._iDisplayStart;
        oSettings.oApi._fnReDraw(oSettings);
        //iDisplayStart has been reset to zero - so lets change it back
        oSettings._iDisplayStart = before;
        oSettings.oApi._fnCalculateEnd(oSettings);
    }
      
    //draw the 'current' page
    oSettings.oApi._fnDraw(oSettings);
};
        
var SurveysQuestions = function () {
    var index = 0;
    var oTable;
    var editable = false;
    
    var defaultAnswers = {
        'CARAS': [
            {
                'text': 'TOTALMENTE DE ACUERDO',
                'value': '100',
                'order': '4'
            },
            {
                'text': 'DE ACUERDO',
                'value': '75',
                'order': '3'
            },
            {
                'text': 'EN DESACUERDO',
                'value': '50',
                'order': '2'
            },
            {
                'text': 'TOTALMENTE EN DESACUERDO',
                'value': '25',
                'order': '1'
            },
            {
                'text': 'N/A',
                'value': '',
                'order': '0'
            }
        ],
        'SI / NO': [
            {
                'text': 'SI',
                'value': ''
            },
            {
                'text': 'NO',
                'value': ''
            }
        ]
    };
    
    var bindEvents = function () {
        $(document).on('focusin', function(e) {
            if ($(e.target).closest(".mce-window").length) {
                e.stopImmediatePropagation();
            }
        });
    
        $('#add').click(function () {
            showForm();
        });
        
        $('.input-daterange').datepicker({
            format: "dd/mm/yyyy",
            language: 'es',
            autoclose: true,
            startDate: 'today'
        });
        
        $('.edit-section').click(function (e) {
            e.preventDefault();
            var section = $(this).data('section');            
            var link    = $(this).attr('href');
            
            $.ajax({
                url: link,
                success: function (data) {
                    showForm(data, section);                    
                }
            });
        });
        
        $('.add-question').click(function (e) {
            e.preventDefault();         
            var link = $(this).attr('href');
            
            $.ajax({
                url: link,
                success: function (data) {
                    showFormQuestion(data);
                    validateQuestions();
                    $('.numeric').numeric();
                    $('.float').numeric({'allow': '.'});
                    index = 0;
                }
            });
        });
        
        $('.modal').on('hidden.bs.modal', function(e) {
            $('#mdl-add-edit form').trigger('reset');
        });
        
        $('#mdl-add-edit').on('change', '#question-type', function (e) {
            var hasChoices = $('option:selected', $(this)).data('has-choices');
            var type       = $('option:selected', $(this)).data('text');
            
            if (hasChoices !== false) {
                populateDefaultAnswers(type);
                $('#choices').removeClass('hidden');
            } else {
                $('#table-choices tbody').empty();
                $('#choices').addClass('hidden');
            }
            
            if (type === 'NUMERICA') {
                $('#alternative-question').removeClass('hidden').addClass('required');
            } else {
                $('#alternative-question').addClass('hidden').removeClass('required');
            }
            
            $('#has-choices').val(hasChoices);
        });
        
        $('#mdl-add-edit').on('click', '#add-question', function (e) {           
            $('#table-choices > tbody').append(getChoiceRow(index++, '', ''));
        });
        
        $('#mdl-add-edit').on('click', '.delete-choice', function (e) {
            var $this = $(this);
            
            swal({
                title: "",
                text: "¿Eliminar respuesta?",
                type: "warning", 
                showCancelButton: true,
                cancelButtonText: "Cancelar",
                confirmButtonText: "Continuar",
                closeOnConfirm: true
            }, function() {
                var $tr = $this.parent().parent();
                $tr.remove();
            });            
        });
        
        $('.table-lp').on('click', '.edit-question', function (e) {            
            var link = baseUrl + 'questions/editQuestion?questionId=' + $(this).data('id');
            
            $.ajax({
                url: link,
                success: function (data) {
                    showFormQuestion(data);
                    validateQuestions();
                    $('.numeric').numeric();
                    $('.float').numeric({'allow': '.'});
                    index = $('#table-choices tbody tr').length + 1;
                }
            });
        });
        
        $('.table-lp').on('click', '.delete-question', function (e) {
            var questionId = $(this).data('id');
            
            swal({
                title: "",
                text: "¿Eliminar pregunta?",
                type: "warning", 
                showCancelButton: true,
                cancelButtonText: "Cancelar",
                confirmButtonText: "Continuar",
                closeOnConfirm: true
            }, function() {
                deleteQuestion(questionId);
            }); 
        });
        
        $('#enable').click(function() {
            $(this).hide();
            $('#list-all-questions').hide();
            loadFacesTable();
        });
        
        $('.cancel-edit-questions').click(function () {            
            $('#enable').show();
            $('#list-edit-questions').hide();            
            $('#list-all-questions').show();
        });
        
        
        $('.save-questions').click(function () {
            saveQuestionsWeigh();
        });
    };
    
    var populateDefaultAnswers = function (type) {
        $('#table-choices tbody').empty();
        
        if (typeof defaultAnswers[type] === 'undefined') {
            return;
        }
        
        var answers = defaultAnswers[type];
        
        $.each(answers, function (idx, answer) {
            $('#table-choices tbody').append(getChoiceRow(index++, answer.text, answer.value));
        });
    };
    
    var getChoiceRow = function (idx, text, value, order) {
        var $tr       = $('<tr />');
        var $tdOption = $('<td />');
        var $tdValue  = $('<td />');
        var $tdAction = $('<td />');
        
        var tr = "<tr>\n" +
                     "    <td class=\"col-md-6\">\n" +
                     "        <input type=\"text\" id=\"choice-text-" + idx + "\" class=\"form-control input-sm required\" name=\"choices[" + idx + "][choiceText]\" value=\"" + text + "\" />\n" +
                     "    </td>\n" +
                     "    <td>\n" +
                     "        <input type=\"text\" id=\"choice-value-" + idx + "\" class=\"form-control input-sm number\" name=\"choices[" + idx + "][choiceNumericValue]\" value=\"" + value + "\" />\n" +
                     "    </td>\n" +
                     "    <td class=\"col-md-1 text-center\">\n" +
                     "        <button type=\"button\" class=\"btn btn-red btn-sm delete-choice\">\n" +
                     "            <i class=\"glyphicon glyphicon-trash\"></i>\n" +
                     "        </button>\n" +
                     "    </td>\n" +
                     "</tr>";
             
        return tr;
    };
    
    var setEditor = function () {
        tinymce.init({
            selector: '#mdl-add-edit .tiny',
            language: 'es_MX',
            height : "300",
            menubar: false,
            plugins: [
                'advlist autolink lists link image charmap hr anchor pagebreak',
                'visualblocks visualchars code fullscreen preview',
                'media nonbreaking save table contextmenu directionality',
                'emoticons template paste textcolor colorpicker textpattern imagetools'
            ],
            toolbar1: 'insertfile undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent',
            toolbar2: 'link image media | forecolor backcolor | emoticons | table | code | fullscreen preview',
            plugin_preview_width: 1000,
            elementpath: false,
            extended_valid_elements: 'span',
            convert_urls: false,
            setup: function (ed) {
                ed.on('init', function(args) {
                    $('#mdl-add-edit').modal('show');
                });
                
                ed.on('blur', function() {
                    tinymce.triggerSave();
                });
                
                ed.on('keyup', function (ed, event) {
                    tinymce.triggerSave();
                });
            }
        });
    };
    
    var showForm = function (data, section) {
        if (data) {
            $('#mdl-add-edit').empty().html(data);
            $('.mce-container').css('position', 'relative');
            validateInformationSections(section);
            setEditor();
        }      
    };
    
    var showFormQuestion = function (data) {
        if (data) {
            $('#mdl-add-edit').empty().html(data);
            $('#mdl-add-edit').modal('show');
            $('#question-subfactor').select2();
        }      
    };
    
    var validateInformationSections = function (section) {
        $('#information-sections-form').validate({
            'ignore': '',
            messages: {
                'survey.initialSection': {
                    'required': 'Por favor, agregue el contenido de la sección inicial'
                },
                'survey.endSection': {
                    'required': 'Por favor, agregue el contenido de la sección final'
                }
            },
            submitHandler: function (form) {
                save(form, section);
            }
        });
    };
    
    var validateQuestions = function () {
        $('#survey-questions-form').validate({
            'ignore': '',
            'rules': {
                'question.required': {
                    'required': true
                },
                'question.subFactor.id': {
                    required: function () {
                        return $('#question-type').val() === '2';
                    }
                }
            },
            messages: {
                'question.questionType.id': {
                    'required': 'Por favor, seleccione el tipo de pregunta.'
                },
                'question.order': {
                    'required': 'Por favor, indique el orden de la pregunta.',
                    'max': 'El orden debe ser menor a 999.'
                },
                'question.question': {
                    'required': 'Por favor, ingrese la pregunta.'
                },
                'question.weighing': {
                    'required': 'Por favor, ingrese el valor de la pregunta.',
                    'number': 'Por favor, ingrese una valor numérico válido.',
                    'remote': 'Por favor, ingrese un valor menor.'
                },
                'question.required': {
                    'required': 'Por favor, indique si la respuesta es requerida o no.'
                },
                'question.subFactor.id': {
                    'required': 'Por favor, seleccione un factor.'
                },
                'question.alternativeQuestion': {
                    'required': 'Por favor, ingrese la pregunta alternativa.'
                }
            },
            submitHandler: function (form) {
                saveQuestion(form);
            },
            errorPlacement: function (error, element) {
                if (element.prop('type') === 'radio') {
                    error.insertAfter(element.closest('div'));
                } else {
                    error.insertAfter(element);
                }
            }
        });
    };
    
    var save = function (form, section) {        
        $.ajax({
            url: $(form).attr('action'),
            data: $(form).serialize(),
            type: 'post',
            dataType: 'json',
            success: function (data) {
                if (data.error) {
                    Liverpool.setMessage($('.inner-message'), data.message, 'warning');
                } else {
                    var html = '<div class="alert alert-success">' +
                               '     <i class="fa fa-check"></i> La sección ya ha sido definida' +
                               '</div>';
                    
                    $('#' + section).html(html);
                    $('#mdl-add-edit').modal('hide');
                }
            }
        });
    };
    
    var saveQuestion = function (form) {
        if ($('#table-choices tbody tr').length === 0  && $('#has-choices').val() === 'true') {
            return Liverpool.setMessage(
                $('.inner-message'),
                'Por favor, ingrese las respuestas u opciones para la pregunta.',
                'danger'
            );
        }
        
        $.ajax({
            url: $(form).attr('action'),
            data: $(form).serialize(),
            type: 'post',
            dataType: 'json',
            success: function (data) {
                if (data.error) {
                    Liverpool.setMessage($('.inner-message'), data.message, 'warning');
                } else {
                    reloadTable();
                    $('#mdl-add-edit').modal('hide');
                }
            },
            beforeSend: function() {
                $('button', $(form)).attr('disabled', 'disabled');
            },
            complete: function () {
                $('button', $(form)).removeAttr('disabled');
            },
            error: function() {
                Liverpool.setMessage(
                    $('.inner-message'),
                    'La pregunta no pudo ser guardada. Por favor, intente nuevamente.',
                    'warning'
                );
            }
        });
    };
    
    var deleteQuestion = function (questionId) {
        $.ajax({
            url: baseUrl + 'questions/deleteQuestion?questionId=' + questionId,
            type: 'delete',
            dataType: 'json',
            success: function (data) {
                if (!data.error) {
                    reloadTable();
                } else {
                    swal({
                        title: "",
                        text: "La pregunta no puede ser eliminada ya que la encuesta ha sido contestada en al menos una ocasión.",
                        type: "warning", 
                        html: true
                    });
                }
            }
        });
    };
    
    var displaySection = function (survey, container, field) {
        $.get(baseUrl + 'surveysgenerator/getSurvey?surveyId=' + survey, function (data) {
            $(container).html(data[field]);
        });
    };
    
    var setTable = function(url) {
        oTable = $('#question-list').dataTable({
            "iDisplayLength": 10,
            "bFilter": true,
            "searchDelay": 500,
            "bLengthChange": false,
            "bAutoWidth": false,
            "bDestroy": true,
            "bSort": false,
            "bInfo": false,
            "oLanguage": {
                "sZeroRecords": "Ninguna Pregunta",
                "sInfo": "Mostrando Encuestas de _START_ al _END_ de _TOTAL_ totales",
                "sInfoEmpty": "0 Encuestas",
                 "sPaginatePrevious": "Previous page",
                 "sProcessing": "Cargando...",
                 "oPaginate": {
                    "sFirst":    "<<",
                    "sLast":     ">>",
                    "sNext":     ">",
                    "sPrevious": "<"
                },
                "sSearch": "Buscar:"
            },
            "fnCreatedRow": function(nRow, aData, iDataIndex) {
                var editLink   = '<button class="btn btn-warning btn-sm edit-question" data-id="' + aData[6] + '"><i class="glyphicon glyphicon-pencil"></i></button>';
                var deleteLink = '<button class="btn btn-red btn-sm delete-question" data-id="' + aData[6] + '"><i class="glyphicon glyphicon-trash"></i></button>';
                
                if (aData[7] == 2) {
                    $('td:eq(3)', nRow).addClass('editable').attr('data-pk', aData[6]);
                } else {
                    $(nRow).addClass('not-editable');
                }
               
                $('td:eq(6)', nRow).html(editLink + deleteLink).addClass('actions');
            },
            "bProcessing": false,
            "bServerSide": true,
            "bStateSave" : false,
            "sAjaxSource": baseUrl + 'questions/listQuestions?surveyId=' + $('#survey-id').val() + '&_r=' + Math.random(),
            "pagingType": "full_numbers",
            "fnDrawCallback": function (oSettings) {
                setEditable('#question-list .editable', 'Editar valor');
                
                if (editable) {
                    $('#question-list .not-editable').hide();
                } else {
                    $('#question-list .not-editable').show();
                }
            }
        });
    };
    
    var reloadTable = function () {
        oTable.fnStandingRedraw();
    };
    
    var setEditable = function(elem, text) {
        $(elem).editable({
            showbuttons: true,
            mode: 'inline',
            type: 'text',
            url: baseUrl + 'questions/editValue',
            title: text,
            disabled: !editable,
            emptytext: '',
            params: function (params) {
                return params;
            },
            validate: function (value) {
                if (isNaN(value) || $.trim(value) === '') {
                    return 'Por favor, ingrse un valor númerico válido';
                }

                if (!isNaN(value) && parseFloat(value) < 0) {
                    return 'Valor no válido';
                }
            }
        });
    };
    
    var loadFacesTable = function () {
        var survey =  $('#survey-id').val();
        
        Liverpool.loading('show');
        
        $.get(baseUrl + 'questions/getFacesQuestions?surveyId=' + survey, function (data) {
            $('#list-edit-questions .table-responsive').empty().html(data);
            $('#list-edit-questions').removeClass('hidden').hide().fadeIn('fast');
            
            $('html,body').animate({
                scrollTop: $('#list-edit-questions').offset().top - 100
            }, 200);
            
            $('#list-edit-questions input').first().focus();
            Liverpool.loading('hide');
        });
        
    };
    
    var saveQuestionsWeigh = function() {
        var invalids = 0;
        
        $('#question-list-edit input').removeClass('error');
        $('#question-list-edit td > div.error').remove();
        
        $('#question-list-edit input').each(function () {
            if (isNaN($(this).val()) || $.trim($(this).val()) === '') {
                $(this).addClass('error');
                $('<div class="error">Ingrese un valor numérico válido</div>').insertAfter($(this));
                invalids++;
            } else {
                var weigh     = parseFloat($(this).val());
                var points    = $(this).data('points');
                var sumPoints = sumQuestionPoints($(this).data('subfactor'));
                var maxValue  = points - (sumPoints - weigh);

                if (weigh > Number(maxValue).toFixed(2)) {
                    invalids++;
                    $(this).addClass('error');
                    $('<div class="error">La suma no debe ser mayor a ' + points + '</div>').insertAfter($(this));
                }
                
                if ((Math.round(sumPoints * 100) / 100) < points) {
                    invalids++;
                    $(this).addClass('error');
                    $('<div class="error">La suma de puntos debe ser igual<br />a los puntos del subfactor (' + points + ')</div>').insertAfter($(this));
                }
            }
        });
        
        if (invalids > 0) {
            swal({
                title: "",
                text: "Por favor corrija los errores antes de continuar.",
                type: "warning", 
                html: true
            });
        } else {
            updateQuestionsWeigh();
        }
    };
    
    var updateQuestionsWeigh = function () {
        var data = {};
        
        $('#question-list-edit input').each(function () {
            var id   = $(this).data('pk');            
            data[id] = $(this).val();
        });
        
        $.ajax({
            url: baseUrl + 'questions/updateWeigh',
            data: {questions: data},
            type: 'post',
            dataType: 'json',
            success: function (data) {
                $('.cancel-edit-questions').trigger('click');
                reloadTable();
            }
        });
    };
    
    var sumQuestionPoints = function (subfactor) {
        var sum = 0;
        
        $("#question-list-edit input[data-subfactor='" + subfactor +"']").each(function () {
            sum += parseFloat($(this).val());
        });
        
        return sum;
    };
    
    var init = function () {
        bindEvents();
        setTable();
    };
    
    init();
}();

