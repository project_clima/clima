var sellTable = null;

function findArea(arr, item) {
    for (var i = arr.length; i--; ) {
        if (arr[i].id === item) {
            return i;
        }
    }
    return -1;
}

function fillSellers(data, manName, zone) {
    $(".table-nps-section").hide();

    var npsSeller = data;
    var ap = '';

    if (sellTable !== null) {
        sellTable.destroy();
        //rTable.clear();
        $("#seller-table").empty();
        $("#seller-table").append("<tbody></tbody>");
    }

//    ap = "<tr>" +
//            "<th>" +
//            "Alamacen" +
//            "</th>"+
//            "<th>"+
//            "Vendedor"+
//            "</th>" +
//            "<th>" +
//            "NPS Total" +
//            "</th>" +
//            "<th style='border-right: 10px solid #F7F7F8;'>Tendencia</th>" +
//            "</th>";
    var headers = [];
    headers.push({'title': 'Rank'},
            {'title': 'Ubicación'},
            {'title': 'Asesor'},
            {'title': 'NPS Total'},
            {'title': 'Tendencia'});

    for (var i = 0; i < _global_area.length; i++) {
        headers.push({'title': _global_area[i].desc});
    }
//    
//    ap += "</tr>";
//    
//    $("#seller-table thead").append(ap);
//    
//    ap = '';

    for (var i = 0; i < npsSeller.length; i++) {

        var clazz = "score green-score";
        var arrow = "up";

        if (Number(npsSeller[i].nps) > 70 && Number(npsSeller[i].nps) < 90) {
            clazz = "score yellow-score";
        } else if (Number(npsSeller[i].nps) < 70) {
            clazz = "score red-score";
        }
        
        var dif = Math.abs(Number(npsSeller[i].nps) - Number(npsSeller[i].npsP));

        if (Number(npsSeller[i].nps) < Number(npsSeller[i].npsP)) {
            if(dif <= 0.50) {
                
                arrow = "glyphicon glyphicon-pause equals score amber-score";
            }else{
                
                arrow = "down";
            }
        }else if(Number(npsSeller[i].nps) === Number(npsSeller[i].npsP)) {
            
            arrow = "glyphicon glyphicon-pause equals score amber-score";
        }else if(Number(npsSeller[i].nps) > Number(npsSeller[i].npsP)) {
            if(dif <= 0.50) {
                
                arrow = "glyphicon glyphicon-pause equals score amber-score";
            }
        }

        ap = "<tr>" +
                "<td>" +
                "</td>" +
                "<td>" +
                npsSeller[i].idStore + ' - ' + npsSeller[i].descStore +
                "</td>" +
                "<td>" +
                npsSeller[i].desc +
                "</td>" +
                "<td>" +
                "<span class='" + clazz + "'>" +
                Number(npsSeller[i].nps).toFixed(2) +
                "</span>" +
                "</td>" +
                "<td style='border-right: 10px solid #F7F7F8;'>" +
                "<span class='glyphicon glyphicon-arrow-" + arrow + " "
                + clazz + "'>" +
                "</span>" +
                "</td>";

        for (var j = 0; j < _global_area.length; j++) {

            var f = findArea(npsSeller[i].areas, _global_area[j].id);

            if (f !== -1) {
                var claz = "score green-score";
                if (Number(npsSeller[i].areas[f].nps) > 70 && Number(npsSeller[i].areas[f].nps) < 90) {
                    claz = "score yellow-score";
                } else if (Number(npsSeller[i].areas[f].nps) < 70) {
                    claz = "score red-score";
                }
                ap = ap + "<td>" +
                        "<span class='" + claz + "'>" +
                        Number(npsSeller[i].areas[f].nps).toFixed(2) +
                        "</span>" +
                        "</td>";
            } else {
                ap = ap + "<td>" +
                        "<span>" +
                        "</span>" +
                        "</td>";
            }
        }

        ap = ap + "</tr>";
        $("#seller-table tbody").append(ap);
    }

    sellTable = $('#seller-table').DataTable({
        "dom": '<<<"col-xs-12 col-md-6"f><"col-xs-12 col-md-6"l>><t>ip>',
        columns: headers,
        "oLanguage": {
            "sZeroRecords": "Ningún Registro",
            "sInfo": "Mostrando del _START_ al _END_ de _TOTAL_",
            "sInfoEmpty": "0 Eventos",
            "sPaginatePrevious": "Previous page",
            "sProcessing": "Cargando...",
            "sLengthMenu": "Mostrar _MENU_ registros",
            "sSearch": "Busqueda:",
            "oPaginate": {
                "sFirst": "<<",
                "sLast": ">>",
                "sNext": ">",
                "sPrevious": "<"
            }
        },
        "lengthMenu": [[30, 500, 100, -1], [30, 50, 100, "Todos"]]
    });

    sellTable.on('order.dt search.dt', function () {
             sellTable.column(0, { search: 'applied', order: 'applied' }).nodes().each(function (cell, i) {
                 cell.innerHTML = i + 1;
             });
         }).draw();

    $(".table-nps-seller").show();
    Liverpool.loading("hide");
}
