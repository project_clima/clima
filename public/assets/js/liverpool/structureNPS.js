$(document).ready(function() {
    for(var i = 0; i < 34; i++) {
        $("#table-structure tbody").append(
            "<tr>" +
                "<td>" +
                    "09234993" +
                "</td>" +
                "<td>" +
                    "JAROMOG" +
                "</td>" +
                "<td>" +
                    "Jassiel" +
                "</td>" +
                "<td>" +
                    "Romo González" +
                "</td>" +
                "<td>" +
                    "Gerencia" +
                "</td>" +
                "<td>" +
                    "1" +
                "</td>" +
                "<td>" +
                    "Juan Pérez" +
                "</td>" +
                "<td>" +
                    "Juan López" +
                "</td>" +
                "<td>" +
                    "3" +
                "</td>" +
                "<td>" +
                    "1A" +
                "</td>" +
            "</tr>"
        );
    }

    ut.init("table-structure", {
        tableStyling: false,
        pageSize: 7,
        disableSearch: true
    });

    $("#table-structure tbody tr").each(function(i, v) {
        $(v).css({
            "cursor": "pointer"
        }).click(function() {
            BootstrapDialog.show({
                title: "Modificar estructura",
                message: $('<div>Cargando datos...</div>')
                .load('/ModalStructureNPS/index'),
                buttons: [{
                    label: "<span class='glyphicon glyphicon-lock'></span>",
                    cssClass: "btn-success",
                    action: function(dialog) {
                        dialog.close();
                    }
                }, {
                    label: "<span class='glyphicon glyphicon-remove'></span>",
                    cssClass: "btn-danger",
                    action: function(dialog) {
                        dialog.close();
                    }
                }]
            });
        });
    });
});