var Filters = function(sessionLevel, isAdmin){
    this.level   = parseFloat(sessionLevel);
    this.isAdmin = isAdmin;
    
    if (store.get('selectedStructureDate')) {
        $("#structureDate").val(store.get('selectedStructureDate'));
    }
};

Filters.prototype.createSelect = function (id, defaultOption) {
    var $select = $('<select />');
    
    $select
        .attr('name', 'level' + id)
        .attr('id', 'level' + id)
        .attr('data-level', id)
        .attr('onchange', 'searchNextLevel(this)')
        .addClass('form-control');
    
    if (defaultOption) {
        var $option = $('<option />');
        
        $option
            .val('-1')
            .text('Seleccione una opción');

        $select.html($option); 
    }
    
    return $select;
};

Filters.prototype.fillLevel1 = function (cb) {
    var self = this;
    
    if (!self.isAdmin) {
        self.createOtherLevel(1);
        return cb();
    }
    
    $.ajax({
        url: baseUrl + "resultmatrix/getLevels1",
        type:'get',
        data: {
            level: 1,
            date: store.get('selectedStructureDate') || $("#structureDate").val()
        },
        success: function (data) {
            var $select = self.createSelect('1', true);
            var options = [];

            
            for (var i = 0; i < data.length; i++) {                    
                options.push({
                    'value': data[i][1],
                    'text': data[i][0],
                    'data-department': data[i][2],
                    'data-username': data[i][3]
                });
            };
            
            if (self.level != 1 && !self.isAdmin) {
                $select.attr('disabled', true);
            }
            
            self.fillOptions(options, $select);
            self.createOtherLevel(2);
            $('#div_select_level_1').empty().append($select);
            
            var selected = store.get('level1') ? store.get('level1').selected : '-1';
            self.storeLevel(1, options, selected, false);
            self.fromLocalStorage();
            
            cb && cb();
        }
    });
};

Filters.prototype.createOtherLevel = function (start) {
    for (var i = start; i < 9; i++) {
        var $select = this.createSelect(i, true);
        
        if (i < this.level && !this.isAdmin) {
            $select.attr('disabled', true);
        }
        
        $('#div_select_level_' + i).empty().append($select);
    }
};

Filters.prototype.fillLoggedUser = function () {
    var self = this;
    
    if (self.isAdmin) {
        return;
    }
    
    $.ajax({
        url: baseUrl + "dashboardenviroment/getDepartment",
        async: false,
        type:'get',
        data: {
            date: store.get('selectedStructureDate') || $("#structureDate").val()
        },
        success: function (data) {
            var $select = $("#level" + self.level);
            var $option = $('<option />');

            $option
                .val(data[3])
                .text(data[0])
                .attr('data-department', data[1])
                .attr('data-username', data[2])
                .attr('data-surveys', data[4])
                .attr('data-answered', data[5]);
        
            if (!self.previousSelection(self.level)) {
                $option.attr('selected', true);
            }
            
            $select.append($option);
            $select.attr("disabled", true);

            self.fillNextLevelLoggedUser();
        }
    });
};

Filters.prototype.previousSelection = function (currentLevel) {
    var prevSelected = false;
    
    for (var i = currentLevel - 1; i > 0; i--) {
        if ($('#level' + i).val() !== '-1') {
            prevSelected = true;
            break;
        }
    }
    
    return prevSelected;
};

Filters.prototype.fillNextLevelLoggedUser = function () {
    var userId = $("#level" + this.level + " option:selected").val();
    var date   = store.get('selectedStructureDate') || $("#structureDate").val();
    var self   = this;
    
    $.ajax({
        url: baseUrl + "resultmatrix/getLevels",
        async: false,
        type:'get',
        data: {
            department: userId,
            date: date
        },
        success: function (data) {
            var nextLevel = self.level + 1;
            var $select   = $('#level' + nextLevel);
            var options   = [];
            
            self.clearFilter($select);
            for (var i = 0; i < data.length; i++) {                
                options.push({
                    'value': data[i][1],
                    'text': data[i][0],
                    'data-department': data[i][2],
                    'data-username': data[i][3],
                    'data-surveys': data[i][4],
                    'data-answered': data[i][5]
                });
            };
            
            self.fillOptions(options, $select);
            
            var selected = store.get('level' + nextLevel) ?
                            store.get('level' + nextLevel).selected : '-1';
                            
            self.storeLevel(nextLevel, options, selected, false, true);
            self.fromLocalStorage();
        }
    });
};

Filters.prototype.fillSpecialPermissions = function () {
    var self = this;
    
    if (self.isAdmin) {
        return;
    }
    
    $.ajax({
        url: baseUrl + 'resultmatrix/getSpecialPermissions',
        type: 'get',
        async: false,
        data: {date : $("#structureDate").val()},
        success: function (data) {
            if (data && data.length > 0) {
                var options = {};
                
                $.each(data, function (idx, permission) {
                    var $select = $("#level" + permission.level);  
                    
                    if (typeof options[permission.level] === 'undefined') {
                        options[permission.level] = [];
                    }
                    
                    for (var i = 0; i < permission.data.length; i++) {
                        options[permission.level].push({
                            'value': permission.data[i][1],
                            'text': permission.data[i][0],
                            'data-department': permission.data[i][2],
                            'data-username': permission.data[i][3]
                        });
                        
                        $('[value="' + permission.data[i][1] + '"]', $select).remove();
                    }
                    
                    var selected = store.get('level' + permission.level) ? 
                                    store.get('level' + permission.level).selected : '-1';
                                    
                    self.storeLevel(permission.level, options[permission.level], selected, false, true);     
                    
                    for (var i = permission.level; i < 9; i++) {
                        $('#level' + i).attr('disabled', false);
                    }
                });
                
                self.fromLocalStorage();
            }
        }
    });
};

Filters.prototype.clearFilter = function ($select) {
    var $option = $('<option />');
    
    $option
        .val('-1')
        .text('Seleccione una opción');

    $select
        .empty()
        .html($option);
};

Filters.prototype.clearFilters = function (level) {
    var self = this;
    
    for (var i = level; i < 9; i++) {
        var $select = $('#level' + i);
        
        if ($select.is(':disabled')) {
            return;
        }
        
        self.clearFilter($select);
    }
};

Filters.prototype.fillLevel = function (department, date, level, cb) {
    var self = this;
    
    $.ajax({
        url: baseUrl + 'resultmatrix/getLevels',
        type:'get',
        data: {
            "department": department,
            "date": date
        },
        success: function (data) {
            var $select = $('#level' + level);
            var options = [];

            self.clearFilter($select);
            
            for (var i = 0; i < data.length; i++) {                    
                options.push({
                    'value': data[i][1],
                    'text': data[i][0],
                    'data-department': data[i][2],
                    'data-username': data[i][3],
                    'data-surveys': data[i][4],
                    'data-answered': data[i][5]
                });
            };
            
            self.fillOptions(options, $select);
            self.storeLevel(level, options, department, true);
            cb && cb();            
        }
    });
};

Filters.prototype.fillOptions = function (options, $select) {
    var $option = {};
    
    $.each(options, function (idx, option) {
        $option = $('<option />');
        
        $.each(option, function (key, value) {
            if (key === 'value') {
                $option.val(value);
            } else if (key === 'text') {
                $option.text(value);
            } else {
                $option.attr(key, value);
            }
            
            $('[value="' + value + '"]', $select).remove();
        });
        
        $select.append($option);
    });
};

Filters.prototype.storeLevel = function (level, options, selected, clear, special) {
    var data = {
        options: options,
        selected: '-1'
    };
    
    store.set('level' + level, data);
    store.set('lastLevel', special ? level : level - 1);
    store.set('lastSelected', selected);
    
    this.setPrevSelectedOption(special ? level : level - 1, selected);
    
    if (clear === true) {
        this.removeFromLocalStorage(level);
    }
};

Filters.prototype.removeFromLocalStorage = function (nextLevel) {   
    for (var i = nextLevel + 1; i <= 8; i++) {
        store.remove('level' + i);
    }
};

Filters.prototype.setPrevSelectedOption = function (level, option) {
    if (level === 0) {
        this.setSelected(1, option);
        return;
    }
    
    if (typeof store.get('level' + level) !== 'undefined') {
        this.setSelected(level, option);
    }
};

Filters.prototype.setSelected = function (level, option) {
    var levelData      = store.get('level' + level);
    levelData.selected = option;

    store.set('level' + level, levelData);
};

Filters.prototype.fromLocalStorage = function () {
    var self = this;
    
    store.forEach(function(level, data) {
        var $select = $('#' + level);
        
        if ($select.length) {
            self.fillOptions(data.options, $select);
            $select.val(data.selected);
        }
    });
};

Filters.prototype.getLastLevelSelected = function () {
    var lastLevel = '';

    for (var level = 8; level > 0; level--) {
        if ($('#level' + level).val() !== '-1') {
            lastLevel = level;
            break;
        }
    }

    return lastLevel;
};

/*
 * Manage Events
 */
$("#structureDate").change(function () {
    if (typeof(Storage) !== "undefined") {
        localStorage.clear();
    }
        
    store.set('selectedStructureDate', $(this).val());
});