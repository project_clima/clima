function getChallenges(path){
    var action = '/challenges/getChallenges';
    var vZ = $("#zone").val();
    var vA = $("#area").val();
    var vP = $("#practice").val();
    
    console.log(vZ+ " " + vA + " " + vP);
    if( path == '' ){
        action += '?z='+ vZ + '&a=' +vA + '&p=' + vP;
    }else{
        action += path + '&z='+ vZ + '&a=' +vA + '&p=' + vP;
    }
   
    Liverpool.loading("show");
    $.ajax({
        url: action,
        success:function(data){
            console.log(data);
            fillChallenges(data);
        }
    });
}

function fillChallenges(challenges){
    
    var series = [];
    
    for( var i = 0; i < challenges.uC.length; i++ ){
        series.push({
            name: challenges.uC[i].year,
            data: challenges.uC[i].countChalls
        });
    }
    console.log(series);
    Highcharts.chart('chart-zone', {
        colors: ['#058DC7', '#50B432', '#ED561B', '#DDDF00', '#24CBE5', '#64E572', 
             '#FF9655', '#FFF263', '#6AF9C4'],
        chart: {
            
            type: 'column'
        },
        title: {
            text: 'Línea de tiempo'
        },
        subtitle: {
            text: 'Historial de impugnaciones'
        },xAxis: {
            categories: challenges.categories,
            crosshair: true
        },
        yAxis: {
            title: {
                text: 'Número de impugnaciones'
            }
        },
        tooltip: {
            headerFormat: '<span style="font-size:10px">{point.key}</span><table>',
            pointFormat: '<tr><td style="color:{series.color};padding:0">{series.name}: </td>' +
                '<td style="padding:0"><b>{point.y:.1f} impg</b></td></tr>',
            footerFormat: '</table>',
            shared: true,
            useHTML: true
        },
        plotOptions: {
            column: {
                pointPadding: 0.2,
                borderWidth: 0
            }
        },
        series: series
    });

    // fill table:
    for(var i = 0; i < 32; i++) {
        $("#table-challenges tbody").html(
            "<tr class='text-center'>" +
                "<td>" +
                    "2" +
                "</td>" +
                "<td>" +
                    "2" +
                "</td>" +
                "<td>" +
                    "Juan Pérez" +
                "</td>" +
                "<td>" +
                    "Jefe Dpto." +
                "</td>" +
                "<td>" +
                    "Arturo Ortiz" +
                "</td>" +
                "<td>" +
                    "24673468" +
                "</td>" +
                "<td>" +
                    "6" +
                "</td>" +
                "<td>" +
                    "8" +
                "</td>" +
                "<td>" +
                    "31/01/2016" +
                "</td>" +
                "<td>" +
                    "20/03/2017" +
                "</td>" +
                "<td>" +
                    "31/01/2016" +
                "</td>" +
                "<td>" +
                    "20/03/2017" +
                "</td>" +
            "</tr>"
        );
    }

    //paginate:

//    ut.init("table-challenges", {
//        tableStyling: false,
//        disableSearch: true
//    });

    var mainTable = $('#table-challenges').DataTable({
		"oLanguage": {
			"sZeroRecords": "Ningún Registro",
			"sInfo": "Mostrando del _START_ al _END_ de _TOTAL_",
			"sInfoEmpty": "0 Eventos",
			"sPaginatePrevious": "Previous page",
			"sProcessing": "Cargando...",
			"sLengthMenu": "Mostrar _MENU_ registros",
			"oPaginate": {
				"sFirst":    "<<",
				"sLast":     ">>",
				"sNext":     ">",
				"sPrevious": "<"
			}
		},
                //destroy:true
                retrieve: true
	});
    
    Liverpool.loading("hide");
}

$(document).ready(function() {
    getChallenges("");
});