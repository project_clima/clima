
var cTable = null;

function openSection(row) {

    var id      = $(row).data("id-chf");
    var idMan   = $(row).data("id-man");
    var name    = $(row).data("chf-name");
    var zone    = $(row).data("id-zone");
    var store   = $(row).data("id-store");

    _chf        = id;
    _chf_name   = name;
    //_zone       = zone;
    _store      = store;

    chfs        = [];
    chfs.push(id);
    
    var path = "?chief=" + id + "&zNps=" + JSON.stringify(zoneStore) + "&firstDate="
                + from + "&lastDate=" + to + '&store=' + store + '&business=' +
                bsns + '&op=' + question + '&group=' + grp + '&man=' + idMan;

    Liverpool.loading("show");

    cleanScreen();
    loadingShow();

    tN = name;

    getNpsArray('?firstDate=' + from + '&lastDate=' + to + '&zNps='
                + JSON.stringify(zoneStore) + '&group=' + grp + '&op=' + question
                + '&store=' + store + '&chief=' + id + '&business=' + bsns +
                '&type=' + tN + '&man=' + idMan);
    
    getNpsTop('?firstDate=' + from + '&lastDate=' + to + '&group=' + grp +
                '&op=' + question + '&business=' + bsns);

    resizeSpaceLinesChart();
    getNpsGlobalArea(path);

    updateLine(tN, 'jefe',"");

    sections = [];

    detailSectionNps(path);
}

function findArea(arr, item) {

    for (var i = arr.length; i--; ) {
        if (arr[i].id === item) {
            return i;
        }
    }

    return -1;
}

function fillChiefs(data) {

    $(".table-nps-manager").hide();

    var npsChief = data;
    var ap = '';
    
    if( cTable !== null ){
        cTable.destroy();
        //rTable.clear();
        $("#chief-table").empty();
        $("#chief-table").append("<tbody></tbody>");
    }
    //Main table:
    
    var headers = [];
    headers.push({'title': 'Rank'},
            {'title': 'Ubicación'},
            {'title': 'Jefe'},
            {'title': 'NPS Total'},
            {'title': 'Tendencia'});
//    $("#chief-table").html("");
//    $("#chief-table").append("<thead></thead>" +
//            "<tbody></tbody>");
//    ap = "<tr>" +
//            "<th>" +
//                "Almacen" +
//            "</th>" +
//            "<th>"+
//                "Jefe"+
//            "</th>"+
//            "<th>" +
//                "NPS Total" +
//            "</th>" +
//                "<th style='border-right: 10px solid #F7F7F8;'>Tendencia</th>" +
//            "</th>";
    
    for (var i = 0; i < _global_area.length; i++) {
        headers.push({'title':_global_area[i].desc});
    }
//    
//    ap += "</tr>";
//    
//    $("#chief-table thead").append(ap);
    
    ap = '';
    
    for (var i = 0; i < npsChief.length; i++) {

        var clazz = "score green-score";
        var arrow = "up";
        
        if (Number(npsChief[i]) > 70 && Number(npsChief[i].nps) < 90) {
            clazz = "score yellow-score";
        } else if (Number(npsChief[i].nps) < 70) {
            clazz = "score red-score";
        }

        var dif = Math.abs(Number(npsChief[i].nps) - Number(npsChief[i].npsP));

        if (Number(npsChief[i].nps) < Number(npsChief[i].npsP)) {
            if(dif <= 0.50) {
                
                arrow = "glyphicon glyphicon-pause equals score amber-score";
            }else{
                
                arrow = "down";
            }
        }else if(Number(npsChief[i].nps) === Number(npsChief[i].npsP)) {
            
            arrow = "glyphicon glyphicon-pause equals score amber-score";
        }else if(Number(npsChief[i].nps) > Number(npsChief[i].npsP)) {
            if(dif <= 0.50) {
                
                arrow = "glyphicon glyphicon-pause equals score amber-score";
            }
        }

        ap = "<tr style='cursor: pointer; '"
                + " data-id-chf='" + npsChief[i].id + "'"
                + " data-id-man='" + npsChief[i].idParent + "'" +
                " data-chf-name='" + npsChief[i].desc + "'"
                + "data-id-store='" + npsChief[i].idStore + "'"
                + " onclick='openSection(this)'>" +
                "<td>" +
                "</td>" +
                "<td>" +
                    npsChief[i].idStore + ' - ' + npsChief[i].descStore +
                "</td>" +
                "<td>" +
                    npsChief[i].desc +
                "</td>" +
                "<td>" +
                "<span class='" + clazz + "'>" +
                    Number(npsChief[i].nps).toFixed(2) +
                "</span>" +
                "</td>" +
                "<td style='border-right: 10px solid #F7F7F8;'>" +
                "<span class='glyphicon glyphicon-arrow-" + arrow + " "
                    + clazz + "'>" +
                "</span>" +
                "</td>";

        for (var j = 0; j < _global_area.length; j++) {

            var f = findArea(npsChief[i].areas, _global_area[j].id);
            
            if (f !== -1) {
                var claz = "score green-score";
                if (Number(npsChief[i].areas[f].nps) > 70 && Number(npsChief[i].areas[f].nps) < 90) {
                    claz = "score yellow-score";
                } else if (Number(npsChief[i].areas[f].nps) < 70) {
                    claz = "score red-score";
                }
                ap = ap + "<td>" +
                        "<span class='" + claz + "'>" +
                        Number(npsChief[i].areas[f].nps).toFixed(2) +
                        "</span>" +
                        "</td>";
            } else {
                ap = ap + "<td>" +
                        "<span>" +
                        "</span>" +
                        "</td>";
            }
        }
        chfs.push(npsChief[i].id);
        ap = ap + "</tr>";
        $("#chief-table tbody").append(ap);
    }
    
    cTable = $('#chief-table').DataTable({
        "dom": '<<<"col-xs-12 col-md-6"f><"col-xs-12 col-md-6"l>><t>ip>',
        columns: headers,
        "oLanguage": {
            "sZeroRecords": "Ningún Registro",
            "sInfo": "Mostrando del _START_ al _END_ de _TOTAL_",
            "sInfoEmpty": "0 Eventos",
            "sPaginatePrevious": "Previous page",
            "sProcessing": "Cargando...",
            "sLengthMenu": "Mostrar _MENU_ registros",
            "sSearch": "Busqueda:",
            "oPaginate": {
                "sFirst": "<<",
                "sLast": ">>",
                "sNext": ">",
                "sPrevious": "<"
            }
        },
        "lengthMenu": [[30, 500, 100, -1], [30, 50, 100, "Todos"]]
    });
    
    cTable.on('order.dt search.dt', function () {
             cTable.column(0, { search: 'applied', order: 'applied' }).nodes().each(function (cell, i) {
                 cell.innerHTML = i + 1;
             });
         }).draw();
    
    $(".table-nps-chief").show();
    Liverpool.loading("hide");
}
