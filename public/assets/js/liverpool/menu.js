var LiverpoolSideMenu = function() {
    var menu = {
        "ActionPlan": "plan",
        "Application": "enviroment",
        "ApplicationNPS": "start",
        "CSI": "edit-csi",
        "ResultsCSI": "reports-csi",
        "DetailsCSI": "reports-csi",
        "CSI": "results-csi",
        "SurveysCSI": "surveys-csi",
        "Users": "users-csi",
        "EmailCSI": "edit-csi",
        "RolesCSI": "edit-csi",
        "StructureCSI": "edit-csi",
        "EmailMonitor": "edit",
        "EmailParametrization": "edit",
        "EmailTemplates": "edit",
        "Evaluations": "eval",
        "Fortalezas": "enviroment",
        "Goals": "catalogs",
        "History": "enviroment",
        "HorizontalAnalysis": "enviroment",
        "NPS": "reports",
        "OpenQuestions": "enviroment",
        "Ranking": "enviroment",
        "ResultMatrix": "enviroment",
        "Results": "enviroment",
        "Seguridad": "edit",
        "SendSurvey": "edit",
        "Structure": "edit",
        "StructureSnapshots": "edit",
        "SurveysGenerator": "edit",
        "TaskManagerNPS": "nps",
        "FileLoadNPS": "nps",
        "Stores": "catalogs",
        "SearchResults": "enviroment",
        "Challenges": "eval",
        "Tracing": "reports",
        "StructureRanking": "reports",
        "HistoryReport": "reports",
        "NonEvalEmployees": "eval",
        "StructureNPS": "nps",
        "SurveyGenNPS": "nps",
        "EmailNPS": "nps",
        "ErrorAndTaskMonitor": "nps",
        "UserAdminNPS": "nps",
        "DuplicatesVacancies": "edit"
    };
    
    var setOpen = function() {
        var controller = $('body').data('controller');
        var anchorPath = $('body').data('path');
        var path       = '/' + controller.toLowerCase() + '/';
        var menuOpen   = menu[controller] || '';        
        var arrayPath  = anchorPath.replace(/^\//,'').split('/');
        var elemt      = getSearchElem("df");
        
        if (anchorPath === '/users/list/' || anchorPath === '/users/list' || arrayPath[0] === 'tracking') {
            $('.sidebar a[href^="' + anchorPath +  '"]').addClass('active'); 
            
            if (arrayPath[0] === 'tracking'&&elemt==="1") { 
                menuOpen = 'tracking'; 
            }else  if (arrayPath[0] === 'tracking'&&elemt==="") { 
                menuOpen = 'enviroment'; 
            } 
        } else {
            $('.sidebar a[href^="' + anchorPath +  '"]').addClass('active');

            if (arrayPath[0] === 'application'&&elemt==="1") { 
                menuOpen = 'tracking'; 
            } 
        }
        
        $('#menu-' + menuOpen).addClass('open');
    };

    
    
    var getSearchElem = function(key) { 
       var search = location.search.replace("?", ""); 
       search = search.split("&"); 
       for(var i = 0; i < search.length; i++) { 
           var set = search[i]; 
           var k = set.split("=")[0]; 
           var v = set.split("=")[1]; 
           if(k === key) { 
               return v; 
           } 
       } 
       return ""; 
    };
    
    setOpen();
}();

