$.fn.dataTableExt.oApi.fnStandingRedraw = function(oSettings) {
    if(oSettings.oFeatures.bServerSide === false){
        var before = oSettings._iDisplayStart;
        oSettings.oApi._fnReDraw(oSettings);
        oSettings._iDisplayStart = before;
        oSettings.oApi._fnCalculateEnd(oSettings);
    }
    
    oSettings.oApi._fnDraw(oSettings);
};

$.validator.addMethod("notEqualTo", function(v, e, p) {  
    return this.optional(e) || v !== $(p).val();
}, "Please specify a different value");

var Stores = function() {
    var oTable    = null;
    var newPeriod = null;

    var bindEvents = function() {
        $(function () {
            $('#add').click(function () {
                showForm(0);
            });
            
            $('.table-lp').on('click', '.edit', function () {
                showForm(
                    $(this).data('id'),
                    $(this).data('hr'),
                    $(this).data('sh'),
                    $(this).data('other')
                );
            });
            
            $('.table-lp').on('click', '.delete', function () {
                var storeId = $(this).data('id');
                
                swal({
                    title: "",
                    text: "¿En realidad desea eliminar la tienda?",
                    type: "warning", 
                    html: true,
                    showCancelButton: true,
                    cancelButtonText: "Cancelar",
                    confirmButtonText: "Continuar",
                    closeOnConfirm: true
                }, function() {
                    deleteStore(storeId);
                });
            });
            
            $('#mdl-add-edit').on('change', '#subdivision', function () {
                var description = $('option:selected', $(this)).text();
                $('#description-subdivision').val(description);
            });
            
            $('#search').keyup(function () {
                oTable.api().ajax.reload();
            });
            
            $('#stores-filters').on('change', '#period', function () {
                oTable.api().ajax.reload();
            });
            
            $("#dest-period").datepicker({
		format: "mm/yyyy",
		language: 'es',
		autoclose: true,
		startView: "months", 
		minViewMode: "months"
            });
            
            $('#copy-stores').click(function () {
                reloadPeriods($('#source-period'), true);
                $('.inner-message').empty();
                $('#copy-store-form').trigger('reset');
                $('#copy-store-form').validate().resetForm();
                $('#mdl-copy-stores').modal('show');                
            });
            
            validateCopy();            
        });
    };
    
    var reloadPeriods = function ($el, selected) {
        $.get(baseUrl + 'stores/getStructureDates', function (periods) {
            var $select = $el || $('#period');
            $select.empty();
            
            $.each(periods, function (idx, period) {
                var $option = $('<option />');
                $option.val(period);
                $option.text(period);
                
                $select.append($option);
            });
            
            if (newPeriod !== null && selected) {
                $select.val(newPeriod);
            }
        });
    };
    
    var setSelectTo = function ($el, name) {
        var val = $el.val();
        
        $el.select2({
            formatResult: format,
            minimumInputLength: 2,
            allowClear: true,
            placeholder: 'Seleccionar',
            ajax: {
                url: baseUrl + 'stores/getEmployees',
                dataType: 'json',
                quietMillis: 250,
                data: function (term, page) {
                    return {
                        search: term,
                        structurePeriod: $('#structure-date').val()
                    };
                },
                results: function (data, page) {
                    return { results: data.items };
                },
                cache: false
            },
            initSelection : function (element, callback) {
                callback({"text": name, "id": val});
            }
        });
    };
    
    var format =  function (state) {
        if (!state.id) return state.text;
        return state.text;
    };

    var showForm = function (storeId, hr, sh, other) {
        $.get(baseUrl + 'stores/form?storeId=' + storeId, function (data) {
            $('#mdl-add-edit').empty().html(data).modal('show');
            $('#store-form').trigger('reset');
            $('#subdivision').val($('#subdivision').data('subdivision'));
            
            setSelectTo($("#store-head"), sh || '');
            setSelectTo($("#human-resources"), hr || '');
            setSelectTo($("#other"), other || '');
            validate();
        });
    };

    var validate = function() {
        $('#store-form').validate({
            'ignore': '',
            'rules': {
                'store.idStore': {
                    'required': true,
                    'digits': true,
                    remote: {
                        url: baseUrl + 'stores/verifyId',
                        type: 'POST',
                        data: {
                            id: function() {
                                return $('#id-store').val();
                            },
                            originalId: function() {
                                return $('#original-store-id').val();
                            },
                            structureDate: function() {
                                return $('#structure-date').val();
                            }
                        }
                    }
                },
                'store.descriptionStore': {
                    'required': true
                },
                'store.subDivision': {
                    'required': true
                }
            },
            'messages': {
                'store.idStore': {
                    'required': 'Por favor, ingrese el número de la tienda',
                    'digits': 'Por favor, ingrese solo números',
                    'remote': 'Número de tienda duplicado'
                },
                'store.descriptionStore': {
                    'required': 'Por favor, ingrese el nombre de la tienda'
                },
                'store.subDivision': {
                    'required': 'Por favor, seleccione la subdivisión'
                },
                'store.country': {
                    'required': 'Por favor, ingrese la clave del país'
                }
            },
            errorPlacement: function(error, element) {
                if (element.next().is('.input-group-addon')) {
                    error.insertAfter(element.next().parent());
                } else {
                    error.insertAfter(element);
                }
            },
            submitHandler: function (form) {
                save(form);
            }
        });
    };
    
    var validateCopy = function() {
        $('#copy-store-form').validate({
            'ignore': '',
            'rules': {
                'sourcePeriod': {
                    'required': true
                },
                'destPeriod': {
                    'required': true,
                    'notEqualTo': '#source-period'
                }
            },
            'messages': {
                'sourcePeriod': {
                    'required': 'Por favor, seleccione el periodo a copiar'
                },
                'destPeriod': {
                    'required': 'Por favor, seleccione el periodo en el cual se hará la copia',
                    'notEqualTo': 'El periodo no puede ser el mismo al periodo que será copiado'
                }
            },
            errorPlacement: function(error, element) {
                if (element.next().is('.input-group-addon')) {
                    error.insertAfter(element.next().parent());
                } else {
                    error.insertAfter(element);
                }
            },
            submitHandler: function (form) {
                copyStores(form);
            }
        });
    };

    var save = function(form) {        
        $.ajax({
            url: $(form).attr('action'),
            data: $(form).serialize(),
            type: 'post',
            dataType: 'json',
            success: function (response) {
                $('#mdl-add-edit').modal('hide');
                
                oTable.fnStandingRedraw();
                Liverpool.setMessage($('.message'), response.message, 'success');
            },
            beforeSend: function () {
                Liverpool.loading('show');
                $('button', $(form)).attr('disabled', 'disabled');
            },
            complete: function () {
                $('button', $(form)).removeAttr('disabled');
                Liverpool.loading('hide');
            },
            error: function (jqXHR, textStatus) {
                var message = 'La tienda no pudo ser guardada. Por favor, intente nuevamente';                
                Liverpool.setMessage($('.inner-message'), message, 'warning');
            }
        });
    };
    
    var copyStores = function(form) {
        newPeriod = $("#dest-period").val();
        
        $.ajax({
            url: $(form).attr('action'),
            data: $(form).serialize(),
            type: 'post',
            dataType: 'json',
            success: function (response) {
                $('#mdl-copy-stores').modal('hide');
                
                reloadPeriods(null, true);
                oTable.fnDraw();
                Liverpool.setMessage($('.message'), response.message, 'success');
            },
            beforeSend: function () {
                Liverpool.loading('show');
                $('button', $(form)).attr('disabled', 'disabled');
            },
            complete: function () {
                $('button', $(form)).removeAttr('disabled');
                Liverpool.loading('hide');
            },
            error: function (jqXHR, textStatus) {
                var message = 'La copia no pudo ser realizada. Por favor, intente nuevamente';                
                Liverpool.setMessage($('.inner-message'), message, 'warning');
            }
        });
    };
    
    var deleteStore = function (storeId) {
        $.ajax({
            url: baseUrl + 'stores/delete',
            data: {storeId: storeId},
            type: 'delete',
            dataType: 'json',
            success: function (response) {
                if (!response.error) {
                    oTable.api().ajax.reload();
                }                
                
                Liverpool.setMessage($('.message'), response.message, response.type);
            },
            beforeSend: function () {
                Liverpool.loading('show');
            },
            complete: function () {
                Liverpool.loading('hide');
            },
            error: function (jqXHR, textStatus) {
                var message = 'La tienda no pudo ser eliminada. Por favor, intente nuevamente';                
                Liverpool.setMessage($('.message'), message, 'danger');
            }
        });
    };

    var setTable = function() {
        oTable = $('.table-lp').dataTable({
            "iDisplayLength": 20,
            "bLengthChange": false,
            "bFilter": false,
            "bAutoWidth": false,
            "bDestroy": true,
            "bSort": false,
            "bInfo": false,
            "bPaginate": true,
            "oLanguage": {
                "sZeroRecords": "Ningún Registro",
                "sInfo": "_START_ al _END_ de _TOTAL_ totales",
                "sInfoEmpty": "0 registros",
                "sPaginatePrevious": "Previous page",
                "sProcessing": "Cargando...",
                "oPaginate": {
                    "sFirst":    "<<",
                    "sLast":     ">>",
                    "sNext":     ">",
                    "sPrevious": "<"
                },
                "sSearch": "Buscar:"
            },
            "fnCreatedRow": function(nRow, aData, iDataIndex) {
                $('td:eq(4)', nRow).html(aData[7]);
                $('td:eq(5)', nRow).html(aData[9]);
                $('td:eq(6)', nRow).html(aData[11]);
                
                $('td:eq(7)', nRow)
                    .empty()
                    .addClass('actions text-right')
                    .attr('nowrap', 'nowrap')
                    .css({'text-align': 'right'});
            
                if ($('#can-edit').length) {
                    $('td:eq(7)', nRow).append(
                        '<button data-id="' + aData[13] + '" data-hr="' + aData[7] + '" data-sh="' + aData[9] + '" data-other="' + aData[11] + '"  class="btn btn-sm btn-warning edit" title="Editar"><i class="glyphicon glyphicon-pencil link-glyp"></i></buttom>'
                    );
                }
                
                if ($('#can-delete').length) {
                    $('td:eq(7)', nRow).append('<button data-id="' + aData[13] + '" class="btn btn-sm btn-red delete" title="Borrar"><i class="glyphicon glyphicon-trash link-glyp"></i></button>');
                }
            },
            "bProcessing": false,
            "bServerSide": true,
            "bStateSave" : false,
            "sAjaxSource": baseUrl + 'stores/list',
            "pagingType": "full_numbers",
            "fnServerParams": function (aoData) {
                aoData.push(
                    {name: "period", value: $('#period').val()}
                );
                aoData.push(
                    {name: "search", value: $('#search').val()}
                );
            }
        });
    };

    var init = function() {
        bindEvents();
        setTable();
    };

    init();
}();