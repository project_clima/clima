var Str = function() {
    var matchChar = function(textFieldId, maskRegex) {
        var tf = document.getElementById(textFieldId);

        tf.onkeypress = function(e) {
            var v = String.fromCharCode(e.which);

            if(!v.match(maskRegex)) {
                return false;
            } else {
                return true;
            }
        };
    };

    var matchCharByClass = function(textFieldClass, maskRegex) {
        var tfs = document.getElementsByClassName(textFieldClass);

        for(var i = 0; i < tfs.length; i++) {
            tfs[i].onkeypress = function(e) {
                var v = String.fromCharCode(e.which);

                if(!v.match(maskRegex)) {
                    return false;
                } else {
                    return true;
                }
            };
        }
    };
    
    return {
        matchChar: matchChar,
        matchCharByClass: matchCharByClass
    }
}();