$.extend($.validator.messages, {
    required: "Campo requerido.",
    number: "Por favor, ingrese un valor numérico válido."
});
        
var SurveysFactors = function () {
    var editable = false;
    var oFactorsTable;
    var oSubFactorsTable;
    
    var bindEvents = function () {
        $('.add-factor').click(function (e) {
            e.preventDefault();
            
            var url = $(this).attr('href');
            
            $.post(url, function (data) {
                showForm(data, oFactorsTable);
            });
        });
        
        $('.add-subfactor').click(function (e) {
            e.preventDefault();
            
            var url = $(this).attr('href');
            
            $.post(url, function (data) {
                showForm(data, oSubFactorsTable);
            });
        });
        
        $('#factors-list').on('click', '.edit-factor', function (e) {
            e.preventDefault();
            
            var url = baseUrl + 'factors/formFactor?surveyId=' + $('#survey-id').val() + '&factorId=' + $(this).data('id');
            
            $.post(url, function (data) {
                showForm(data, oFactorsTable);
            });
        });
        
        $('#subfactors-list').on('click', '.edit-subfactor', function (e) {
            e.preventDefault();
            
            var url = baseUrl + 'factors/formSubFactor?surveyId=' + $('#survey-id').val() + '&subFactorId=' + $(this).data('id');
            
            $.post(url, function (data) {
                showForm(data, oSubFactorsTable);
            });
        });
        
        $('#factors-list').on('click', '.delete-factor', function () {
            var factorId = $(this).data('id');
            
            swal({
                title: "",
                text: "¿Eliminar factor?",
                type: "warning", 
                showCancelButton: true,
                cancelButtonText: "Cancelar",
                confirmButtonText: "Continuar",
                closeOnConfirm: true
            }, function() {
                deleteFactor(factorId);
            });
        });
        
        $('#subfactors-list').on('click', '.delete-subfactor', function () {
            var subFactorId = $(this).data('id');
            
            swal({
                title: "",
                text: "¿Eliminar subfactor?",
                type: "warning", 
                showCancelButton: true,
                cancelButtonText: "Cancelar",
                confirmButtonText: "Continuar",
                closeOnConfirm: true
            }, function() {
                deleteSubfactor(subFactorId);
            });
        });
        
        $('#enableEditSubfactors').click(function() {
            $('#subfactors-list .editable').each(function () {
                var value = $(this).html();
                var id    = $(this).data('pk');

                var $input = $('<input />');
                
                $input.attr('type', 'text')
                    .addClass('form-control input-sm')
                    .val(value);
                
                $(this).html($input);
            });
            
            $(this).hide();
            $('.subfactors-edit-actions').removeClass('hidden').hide().fadeIn('fast');
        });
        
        $('.cancel-edit').click(function () {
            $('#subfactors-list .editable').each(function () {
                var value = $('input', this).val();
                $(this).html(value);
            });
            
            $('#enableEditSubfactors').show();
            $('.subfactors-edit-actions').hide();
            oSubFactorsTable.api().ajax.reload();
        });
        
        $('.save-subfactors').click(function () {
            saveSubfactors();
        });
    };
    
    var showForm = function (data, table) {
        if (data) {
            $('#mdl-add-edit').empty().html(data).modal('show');
            validateFormFactor(table);
        }      
    };
    
    var validateFormFactor = function (table) {
        $('#survey-factors-form').validate({
            'ignore': '',
            submitHandler: function (form) {
                saveFactor(form, table);
            }
        });
    };
    
    var saveFactor = function (form, table) {        
        $.ajax({
            url: $(form).attr('action'),
            data: $(form).serialize(),
            type: 'post',
            dataType: 'json',
            success: function (data) {
                if (data.error) {
                    Liverpool.setMessage($('.inner-message'), data.message, 'warning');
                } else {
                    oFactorsTable.api().ajax.reload();
                    oSubFactorsTable.api().ajax.reload();
                    $('#mdl-add-edit').modal('hide');
                    $('.cancel-edit').trigger('click');
                }
            },
            beforeSend: function() {
                $('button', $(form)).attr('disabled', 'disabled');
            },
            complete: function () {
                $('button', $(form)).removeAttr('disabled');
            }
        });
    };
    
    var deleteFactor = function (factorId) {
        $.ajax({
            url: baseUrl + 'factors/deleteFactor?factorId=' + factorId,
            type: 'delete',
            dataType: 'json',
            success: function (data) {
                if (!data.error) {
                    oFactorsTable.api().ajax.reload();
                } else {
                    swal({
                        title: "",
                        text: data.message,
                        type: "error"
                    });
                }
            }
        });
    };
    
    var deleteSubfactor = function (subFactorId) {
        $.ajax({
            url: baseUrl + 'factors/deletesSubFactor?subFactorId=' + subFactorId,
            type: 'delete',
            dataType: 'json',
            success: function (data) {
                if (!data.error) {
                    oSubFactorsTable.api().ajax.reload();
                } else {
                    swal({
                        title: "",
                        text: data.message,
                        type: "error"
                    });
                }
            }
        });
    };
    
    var setTableFactors = function() {
        oFactorsTable = $('#factors-list').dataTable({
            "iDisplayLength": 10,
            "bFilter": false,
            "bPaginate": false,
            "searchDelay": 500,
            "bLengthChange": false,
            "bAutoWidth": false,
            "bDestroy": true,
            "bSort": false,
            "bInfo": false,
            "oLanguage": {
                "sZeroRecords": "Ningún Factor",
                "sInfo": "Mostrando Factores de _START_ al _END_ de _TOTAL_ totales",
                "sInfoEmpty": "0 Encuestas",
                 "sPaginatePrevious": "Previous page",
                 "sProcessing": "Cargando...",
                 "oPaginate": {
                    "sFirst":    "<<",
                    "sLast":     ">>",
                    "sNext":     ">",
                    "sPrevious": "<"
                },
                "sSearch": "Buscar:"
            },
            "fnCreatedRow": function(nRow, aData, iDataIndex) {
                var editLink   = '<button class="btn btn-sm btn-warning edit-factor" data-id="' + aData[3] + '"><i class="glyphicon glyphicon-pencil"></i></button>';
                var deleteLink = '<button class="btn btn-sm btn-red delete-factor" data-id="' + aData[3] + '"><i class="glyphicon glyphicon-trash"></i></button>';

                $('td:eq(3)', nRow).html(editLink + deleteLink).addClass('actions');
            },
            "bProcessing": false,
            "bServerSide": true,
            "bStateSave" : false,
            "sAjaxSource": baseUrl + 'factors/listFactors?surveyId=' + $('#survey-id').val(),
            "pagingType": "full_numbers"
        });
    };
    
    var setTableSubfactors = function() {
        oSubFactorsTable = $('#subfactors-list').dataTable({
            "iDisplayLength": 10,
            "bPaginate": false,
            "bFilter": false,
            "searchDelay": 500,
            "bLengthChange": false,
            "bAutoWidth": false,
            "bDestroy": true,
            "bSort": false,
            "bInfo": false,
            "oLanguage": {
                "sZeroRecords": "Ningún Sub Factor",
                "sInfo": "Mostrando Sub Factores de _START_ al _END_ de _TOTAL_ totales",
                "sInfoEmpty": "0 Encuestas",
                 "sPaginatePrevious": "Previous page",
                 "sProcessing": "Cargando...",
                 "oPaginate": {
                    "sFirst":    "<<",
                    "sLast":     ">>",
                    "sNext":     ">",
                    "sPrevious": "<"
                },
                "sSearch": "Buscar:"
            },
            "fnCreatedRow": function(nRow, aData, iDataIndex) {
                var editLink   = '<button class="btn btn-sm btn-warning edit-subfactor" data-id="' + aData[4] + '"><i class="glyphicon glyphicon-pencil"></i></button>';
                var deleteLink = '<button class="btn btn-sm btn-red delete-subfactor" data-id="' + aData[4] + '"><i class="glyphicon glyphicon-trash"></i></button>';
                
                $('td:eq(3)', nRow)
                    .addClass('editable')
                    .attr('data-pk', aData[4])
                    .attr('data-factor', aData[5])
                    .attr('data-points', aData[6]);
                $('td:eq(4)', nRow).html(editLink + deleteLink).addClass('actions');
            },
            "bProcessing": false,
            "bServerSide": true,
            "bStateSave" : false,
            "sAjaxSource": baseUrl + 'factors/listSubFactors?surveyId=' + $('#survey-id').val(),
            "pagingType": "full_numbers",
            "fnDrawCallback": function (oSettings) {
                setEditable('#subfactors-list .editable', 'Editar valor');      
            }
        });
    };
    
    var setEditable = function(elem, text) {
        $(elem).editable({
            mode: 'inline',
            type: 'text',
            url: baseUrl + 'factors/editPoints',
            title: text,
            disabled: !editable,
            emptytext: '',
            params: function (params) {
                return params;
            },
            validate: function (value) {
                if (isNaN(value) || $.trim(value) === '') {
                    return 'Por favor, ingrse un valor númerico válido';
                }

                if (!isNaN(value) && parseFloat(value) < 0) {
                    return 'Valor no válido';
                }
            }
        });
    };
    
    var saveSubfactors = function() {
        var invalids = 0;
        
        $('#subfactors-list .editable input').removeClass('error');
        $('#subfactors-list .editable div.error').remove();
        
        $('#subfactors-list .editable input').each(function () {
            if (isNaN($(this).val()) || $.trim($(this).val()) === '') {
                $(this).addClass('error');
                $('<div class="error">Ingrese un valor numérico válido</div>').insertAfter($(this));
                invalids++;
            } else {            
                var subfactor = parseFloat($(this).val());
                var $parent   = $(this).parent();
                var points    = $parent.data('points');
                var factor    = $parent.data('factor');
                var sumPoints = sumSubfactorsPoints(factor);
                var maxValue  = points - (sumPoints - subfactor);            

                if (subfactor > maxValue) {
                    invalids++;
                    $(this).addClass('error');
                    $('<div class="error">La suma no debe ser mayor a ' + points + '</div>').insertAfter($(this));
                }
                
                if ((Math.round(sumPoints * 100) / 100) < points) {
                    invalids++;
                    $(this).addClass('error');
                    $('<div class="error">La suma de puntos debe ser igual<br />a los puntos del factor (' + points + ')</div>').insertAfter($(this));
                }
            }
        });
        
        if (invalids > 0) {
            swal({
                title: "",
                text: "Por favor corrija los errores antes de continuar.",
                type: "warning", 
                html: true
            });
        } else {
            updateSubfactorsPoints();
        }
    };
    
    var updateSubfactorsPoints = function () {
        var data = {};
        
        $('#subfactors-list .editable input').each(function () {
            var id   = $(this).parent().data('pk');            
            data[id] = $(this).val();
        });
        
        $.ajax({
            url: baseUrl + 'factors/updateSubfactorPoints',
            data: {subfactors: data},
            type: 'post',
            dataType: 'json',
            success: function (data) {
                $('.cancel-edit').trigger('click');
                oSubFactorsTable.api().ajax.reload();
            }
        });
    };
    
    var sumSubfactorsPoints = function (factor) {
        var sum = 0;
        
        $("#subfactors-list .editable[data-factor='" + factor +"'] input").each(function () {
            sum += parseFloat($(this).val());
        });
        
        return sum;
    };
    
    var init = function () {
        bindEvents();
        setTableFactors();
        setTableSubfactors();
    };
    
    init();
}();

