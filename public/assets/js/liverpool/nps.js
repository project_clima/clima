/************Variables globales**********/
var nps             = {};
var question        = '';
var generalNPS;
var plan            = 91;
var mainChart;
var _this;
var namesHighest    = [];
var namesLowest     = [];
var npsTable        = [];
var flow = '';
var arsName = [
    'Ubicacion',
    'Gerentes',
    'Jefes',
    //'Secciones',
    'Asesor'
];

var level = [
    4,
    5, //Gerentes
    6, //Jefes
    //7, //Secciones
    8 //Vendedores
];

/****************************************/
function changeBtnGroup(chb) {
    var _this = $(chb);
    var id = _this.data("group-id");
    b = "";
    flow = '';
    grp = id;
    tN = _this.data("group-name");

    cleanScreen();
    loadingShow();

    Liverpool.loading("show");
    localStorage.setItem("group", grp);
    localStorage.setItem("groupN", tN);
    getNpsArray('?firstDate=' + from + '&lastDate=' + to + '&zNps='
            + JSON.stringify(zoneStore) + '&group=' + grp + '&op='
            + question + '&type=' + tN);

    createBusiness('?firstDate=' + from + '&lastDate=' + to + '&zNps='
            + JSON.stringify(zoneStore) + '&group=' + grp + '&op='
            + question);

    updateLine(tN, 'group', 'less');

    $('.zone-row').fadeOut(400);
    $(".second-lvl-li").hide();
}
//////////////////////////////////////////
//////////////////////////////////////////
/********Change button panel business****/
function changeBtnBusiness(chb) {

    var chb = $(chb);
    var id = chb.data("busns-id");
    flow = '';
    tN = chb.data("busns-name");
    tB = tN;
    bsns_name = tN;
    bsns = id;
    _zones = [];
    strs = [];

    cleanScreen();
    loadingShow();

    Liverpool.loading("show");
    localStorage.setItem("business", bsns);
    localStorage.setItem("businessN", tN);
    updateLine(tN, 'business', '');

    getNpsArray('?firstDate=' + from + '&lastDate=' + to + '&zNps='
            + JSON.stringify(zoneStore) + '&business=' + bsns + '&op=' + question +
            '&type=' + tN);

    resizeSpaceLinesChart();

    getNpsTop('?firstDate=' + from + '&lastDate=' + to + '&zNps=' +
            JSON.stringify(zoneStore) + '&business=' + bsns + '&op=' + question +
            '&group=' + grp);

    createZones();

    getNpsGlobalArea('?firstDate=' + from + '&lastDate=' + to + '&zNps='
            + JSON.stringify(zoneStore) + '&business=' + bsns + '&op=' + question
            + '&group=' + grp);

    getNpsRanking('?firstDate=' + from + '&lastDate=' + to + '&zNps=' +
            JSON.stringify(zoneStore) + '&business=' + bsns + '&op=' + question
            + '&group=' + grp);
}
///////////////////////////////////////////
/**********Fill Top *********************/
function getTop(top) {

    fillTop(top);

    var mj = Math.round(Number(top.goal[0]));
    var mn = Math.round(Number(top.goal[2]));

    miniChart = Highcharts.chart('participation-chart', {
        chart: {
            type: 'bar'
        },
        title: {
            text: 'Cumplimiento de meta por almacén:',
            style: {
                "font-size": "14px"
            }
        },
        xAxis: {
            categories: ['']
        },
        colors: [
            "#D36900",
            "#008800"
        ],
        yAxis: {
            min: 0,
            max: 100,
            title: {
                text: ''
            },
            labels: {
                enabled: false
            }
        },
        legend: {
            layout: "vertical",
            reversed: true,
            align: "left"
        },
        plotOptions: {
            column: {
                stacking: "percent"
            },
            series: {
                dataLabels: {
                    enabled: true,
                    color: "#FFF",
                    format: "{point.percentage:.0f}%"
                },
                stacking: 'percent'
            }
        },
        tooltip: false,
        series: [{
                name: 'Abajo',
                data: [mn]
            }, {
                name: 'Arriba',
                data: [mj]
            }]
    });

    $('#mj').html(top.goal[1] + "  Almacenes");
    $('#mn').html(top.goal[3] + "  Almacenes");
    $("#loading-top-tables").hide();
    $('#tNps').fadeIn(200);
    mainLineChart.reflow();
    mainLineChartTrim.reflow();
    miniChart.reflow();
}
/////////////////////////////////////////
function fillRanking(ranking, type) {

    fillRigthTable(ranking, type);
    //fillSelect(bsns_name, 0);

}
//////////////////////////////////////////
/****************************************/
function fillSelect(lev) {

    if (!isNaN(Number(lev)) && flow != 'Compare') {
        if (Number(lev) < 5) {

            $('select.input-filter').html("");

            $('select.input-filter').append($('<option>',
                    {
                        value: '0',
                        text: 'Rankear por'
                    }));

            for (var i = lev; i < arsName.length; i++) {

                $('select.input-filter').append($('<option>',
                        {
                            value: level[i],
                            text: arsName[i]
                        }));
            }
        }
        $('.select-nps').fadeIn(200);
    }
    loadingHide();
}
//////////////////////////////////////////
/*****************************************/
function getLess(arr) {
    for (var i = 0; i < arr.length; i++) {
        if (arr[i] !== null ) {
            return arr[i];
        }
    }
    return 0;
}
///////////////////////////////////////////
/*****Principal function*****************/
function hi(data) {
    nps = data;
    question = nps.question;
    generalNPS = Number(nps.npsCompany[3]);

    var arrayTemp = nps.months.slice();
    arrayTemp.sort(sortFloat);
    var lessA1 = getLess(arrayTemp);
    var lessA11 = arrayTemp[11];
    arrayTemp = nps.monthsLess1.slice();
    arrayTemp.sort(sortFloat);
    var lessA2 = getLess(arrayTemp);
    var lessA22 = arrayTemp[11];
    arrayTemp = nps.triMonths.slice();
    arrayTemp.sort(sortFloat);
    var lessA3 = getLess(arrayTemp);
    var lessA33 = arrayTemp[11];
    arrayTemp = nps.triMonthsLess1.slice();
    arrayTemp.sort(sortFloat);
    var lessA4 = getLess(arrayTemp);
    var lessA44 = arrayTemp[11];

    if (lessA2 < lessA1) {
        lessA1 = lessA2;
    }

    if (lessA22 > lessA11) {
        lessA11 = lessA22;
    }

    if (lessA4 < lessA3) {
        lessA3 = lessA4;
    }

    if (lessA44 > lessA33) {
        lessA33 = lessA44;
    }
    mainChart = Highcharts.chart('main-chart', {
        chart: {
            type: 'solidgauge',
            marginTop: 50,
            height: 330
        },

        title: {
            text: 'NPS ' + nps.type,
            y: 20,
            style: {
                fontSize: '13px'
            }
        },

        tooltip: {
            formatter: function () {
                return "PLAN " + plan;
            },
            positioner: function () {
                return {
                    x: 100,
                    y: 210
                }
            }
        },

        pane: {
            center: ['50%', '35%'],
            startAngle: 0,
            endAngle: 360,
            y: 0,
            background: [{// Track for Move
                    outerRadius: '75%',
                    innerRadius: '60%',
                    backgroundColor: "#DDDADB",
                    borderWidth: 0
                }]
        },

        yAxis: {
            min: 0,
            max: 100,
            lineWidth: 0,
            tickPositions: []
        },

        plotOptions: {
            solidgauge: {
                outerRadius: '75%',
                innerRadius: '60%',
                dataLabels: {
                    enabled: false
                },
                linecap: 'square',
                stickyTracking: false
            }
        },

        series: [{
                name: 'PLAN',
                borderColor: "#337AB7",
                data: [{
                        color: "#337AB7",
                        radius: '75%',
                        y: generalNPS
                    }]
            }]
    },
            function (mainChart) {
                //mainChart.renderer.label("sdfhsadkkkkkkk").add();

                var textX = mainChart.plotLeft + (mainChart.plotWidth * 0.5);
                var textY = mainChart.plotTop + (mainChart.plotHeight * 0.5);

                var span = '<span id="pieChartInfoText" \n\
                    style="position:absolute; text-align:center;">';
                span +=
                        '<span class="main-circle-lbl">' +
                        generalNPS + "<span class='span-percent-circle'>%</span>" +
                        '</span>';
                span += '</span>';

                $("#addText").html(span);
                span = $('#pieChartInfoText');

                span.css('left', textX + (span.width() * -0.5));
                span.css('top', textY + (span.height() * -0.8));

                var table = mainChart.renderer.html(
                        "<table class='resume-circle-table'>" +
                        "<tbody>" +
                        "<tr>" +
                        "<td class='quant'>" +
                        Number(nps.npsCompany[0]) +
                        "</td>" +
                        "<td class='quant'>" +
                        Number(nps.npsCompany[1]) +
                        "</td>" +
                        "<td class='quant'>" +
                        Number(nps.npsCompany[2]) +
                        "</td>" +
                        "</tr>" +
                        "<tr>" +
                        "<td class='cell-green'>" +
                        "</td>" +
                        "<td class='cell-yellow'>" +
                        "</td>" +
                        "<td class='cell-orange'>" +
                        "</td>" +
                        "</tr>" +
                        "<tr>" +
                        "<td class='text-label'>" +
                        "Promotores" +
                        "</td>" +
                        "<td class='text-label'>" +
                        "Neutros" +
                        "</td>" +
                        "<td class='text-label'>" +
                        "Detractores" +
                        "</td>" +
                        "</tr>" +
                        "</tbody>" +
                        "</table>"
                        ).add();

                table.align(Highcharts.extend(table.getBBox(), {
                    align: 'center',
                    x: 0, // offset
                    verticalAlign: 'bottom',
                    y: 0 // offset
                }), null, 'spacingBox');
            });

    //Second graph:

    var randomNPSValues = nps.months;
    //nps year -1
    var randomNPSValuesPrevYear = nps.monthsLess1;

    var randomNPSValuesTrim = nps.triMonths;

    var randomNPSValuesTrimPrevYear = nps.triMonthsLess1;

    var currYear = Number(new Date().getFullYear());

    mainLineChart = Highcharts.chart('main-line-chart', {
        chart: {
            type: 'line',
            height: 333
        },
        colors: [
            "rgb(48, 104, 152)",
            "rgb(74, 145, 13)"
        ],
        title: {
            text: 'Histórico ' + nps.type,
            y: 20,
            style: {
                fontSize: '13px'
            }
        },
        xAxis: {
            categories: ['Ene', 'Feb', 'Mar', 'Abr', 'May', 'Jun', 'Jul',
                'Ago', 'Sep', 'Oct', 'Nov', 'Dic'],
            color: "#337AB7"
        },
        yAxis: {
            title: {
                text: '',
                style: {
                    "font-size": "20px"
                }
            },
            max: lessA11,
            min: lessA1//lowest
        },
        plotOptions: {
            line: {
                dataLabels: {
                    enabled: true
                },
                enableMouseTracking: true
            }
        },
        tooltip: false,
        /*
         tooltip: {
         enabled: true,
         formatter: function () {
         return "<div style='color: #008800;'>" + this.y + 
         "</div> almacenes por encima de la meta<br>" +
         "<div style='color: #FF8800;'>34</div> almacenes por debajo de la meta<br>"
         }
         },         */
        series: [{
                name: currYear - 1,
                data: randomNPSValuesPrevYear
            }, {
                name: currYear,
                data: randomNPSValues
            }]
    });

    //mainLineChart.series[0].hide();

    mainLineChartTrim = Highcharts.chart('main-line-chart-trim', {
        chart: {
            type: 'line',
            height: 333
        },
        colors: [
            "rgb(48, 104, 152)",
            "rgb(74, 145, 13)"
        ],
        title: {
            text: 'Histórico Trimestral',
            y: 20,
            style: {
                fontSize: '13px'
            }
        },
        xAxis: {
            categories: ["Ene - Mar", "Abr - Jun", "Jul - Sep", "Oct - Dic"],
            color: "#337AB7"
        },
        yAxis: {
            title: {
                text: ''
            },
            max: lessA33,
            min: lessA3//lowest,
        },
        plotOptions: {
            line: {
                dataLabels: {
                    enabled: true
                },
                enableMouseTracking: true
            }
        },
        tooltip: false,
        /*
         tooltip: {
         enabled: true,
         formatter: function () {
         return "<div style='color: #008800;'>" + this.y +
         "</div> almacenes por encima de la meta<br>" +
         "<div style='color: #FF8800;'>34</div> almacenes por debajo de la meta<br>" +
         "1800 evalueciones"
         }
         },
         */
        series: [{
                name: currYear - 1,
                data: randomNPSValuesTrimPrevYear
            }, {
                name: currYear,
                data: randomNPSValuesTrim
            }]
    });

    //mainLineChartTrim.series[0].hide();

    $("#right-table_filter").hide();

    $("#second-table_filter").hide();
    $(".dataTables_length").css("font-size", "12px");
    $(".dataTables_length select").css({
        "font-size": "12px"
    });

    $("#btn-map").click(function () {
        location.hash = "#page-bottom";
    });
    
    if ($("#btn-mes").hasClass("btn-primary")) {
        $("#main-line-chart-trim").hide();
        $("#main-line-chart").show();
    }else if ($("#btn-trim").hasClass("btn-primary")) {
        $("#main-line-chart-trim").show();
        $("#main-line-chart").hide();
    }
    
    $(".table-nps-zone").hide();

    $("#btn-trim").click(function () {
        $(this).attr("class", "btn btn-primary btn-xs");
        $("#btn-mes").attr("class", "btn btn-default btn-xs");
        $("#main-line-chart").hide();
        $("#main-line-chart-trim").show(200, function() {
            mainLineChartTrim.reflow();
        });
    });

    $("#btn-mes").click(function () {
        $(this).attr("class", "btn btn-primary btn-xs");
        $("#btn-trim").attr("class", "btn btn-default btn-xs");
        $("#main-line-chart-trim").hide();
        $("#main-line-chart").show(200, function() {
            mainLineChart.reflow();
        });
    });

}
//////////////////////////////////////////
/**********Clean vriables****************/
function cleanVariables() {
    zones = {};
    chfs = [];
    strs = [];
    //zoneStore = [];
    //from = "";
    //to = "";
    //grp = '';
    //bsns = '';
    tN = '';
    managers = [];
    sections = [];
    type = '';
    //question = '';
}
//////////////////////////////////////////
/*********Clean home screen**************/
function cleanScreen() {
    //$('#npsT').html("");
    //$('#npsT').append('<label class="normal-lbl">'+
    //               '<span style="color: #306898; font-weight: bold;">Compañia</span>'+
    //              '</label>');
    var div = $('#hNps');
    if (div.hasClass('col-sm-5')) {
        div.attr("class", "col-xs-12 col-sm-9");
    }
    $('#tNps').hide();

    if ($('.select-nps').css('display') == 'block') {
        $('.select-nps').hide();
    }

    if ($('#groups-t-nps').css('display') == 'block') {
        $('#groups-t-nps').hide();
        $("#groups-t-nps-loading").show();
    }

    if ($('.groups-table-nps').css('display') == 'block') {
        $('.groups-table-nps').hide();
    }
    if ($('.table-nps').css('display') == 'block') {
        $('.table-nps').hide();
    }
    if ($('.table-nps-ranking').css('display') == 'block') {
        $('.table-nps-ranking').hide();
    }
    if ($('.table-nps-zone').css('display') == 'block') {
        $('.table-nps-zone').hide();
    }
    if ($('.table-nps-store').css('display') == 'block') {
        $('.table-nps-store').hide();
    }
    if ($('.table-nps-manager').css('display') == 'block') {
        $('.table-nps-manager').hide();
    }
    if ($('.table-nps-chief').css('display') == 'block') {
        $('.table-nps-chief').hide();
    }
    if ($('.table-nps-section').css('display') == 'block') {
        $('.table-nps-section').hide();
    }
    if ($('.table-nps-seller').css('display') == 'block') {
        $('.table-nps-seller').hide();
    }
        if ($('.return-nps').css('display') === 'block') {
        $('.return-nps').hide();
    }
}
//////////////////////////////////////////
/*****************************************/
function fillGroups(groups) {
    //fillPrincipal(groups);
    fillGroupRigthTable(groups, "Grupo");

    $(".groups-table-nps").fadeIn(400);
}
///////////////////////////////////////////
/*********Clean home screen**************/
function cleanScreenRanking() {
    if ($('.groups-table-nps').css('display') == 'block') {
        $('.groups-table-nps').hide();
    }
    if ($('.table-nps').css('display') == 'block') {
        $('.table-nps').hide();
    }
    if ($('.table-nps-ranking').css('display') == 'block') {
        $('.table-nps-ranking').hide();
    }
    if ($('.table-nps-zone').css('display') == 'block') {
        $('.table-nps-zone').hide();
    }
    if ($('.table-nps-store').css('display') == 'block') {
        $('.table-nps-store').hide();
    }
    if ($('.table-nps-manager').css('display') == 'block') {
        $('.table-nps-manager').hide();
    }
    if ($('.table-nps-chief').css('display') == 'block') {
        $('.table-nps-chief').hide();
    }
    if ($('.table-nps-section').css('display') == 'block') {
        $('.table-nps-section').hide();
    }
    if ($('.table-nps-seller').css('display') == 'block') {
        $('.table-nps-seller').hide();
    }
}
//////////////////////////////////////////
/****************************************/
function sortFloat(a, b) {
    return a - b;
}
//////////////////////////////////////////
/**************Document ready function***/
$(document).ready(function () {
    
    $( window ).load(function() {
        // Run code
        BootstrapDialog.show({
              title: " ",
              message: $(
                "<div>" +
                  "<img alt='imagen' onload='Liverpool.fitImgVentana(this)' class='ventana-img' src=" +
                  (baseUrl + 'public/assets/images/ventana.jpg') +
                  ">" +
                "</div>"
              )
            });
      });
    
    loadData();
    
    $("#btn-recom").click(function () {
        $(this).attr("class", "btn btn-primary btn-xs");
        $("#btn-serv").attr("class", "btn btn-default btn-xs");
        question = $(this).val();
        $("#btn-serv").prop("disabled", false);
        $(this).prop("disabled", true);

        Liverpool.loading("show");

        cleanVariables();
        cleanScreen();
        resetFilters();
        loadingShow();
        getNpsArray('?firstDate=' + from + '&lastDate=' + to + '&op=' + question);
        createGroups('?firstDate=' + from + '&lastDate=' + to + '&op=' + question);
        $('#npsT').html("");
        $('#npsT').append('<label class="normal-lbl">' +
                '<span style="color: #306898; font-weight: bold;">Compañia</span>' +
                '</label>');
    });

    $("#btn-serv").click(function () {
        $(this).attr("class", "btn btn-primary btn-xs");
        $("#btn-recom").attr("class", "btn btn-default btn-xs");
        question = $(this).val();
        $("#btn-recom").prop("disabled", false);
        $(this).prop("disabled", true);

        Liverpool.loading("show");

        cleanVariables();
        cleanScreen();
        resetFilters();
        loadingShow();
        getNpsArray('?firstDate=' + from + '&lastDate=' + to + '&op=' + question);
        //updateLine(tN, '', '');
        createGroups('?firstDate=' + from + '&lastDate=' + to + '&op=' + question);
        $('#npsT').html("");
        $('#npsT').append('<label class="normal-lbl">' +
                '<span style="color: #306898; font-weight: bold;">Compañia</span>' +
                '</label>');
    });
});