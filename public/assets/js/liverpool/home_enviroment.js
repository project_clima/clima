var colors=['#336797', '#6d9dd4', '#8cb0cf', '#c2dcf2'];
var dataCharts=[];

String.prototype.stripAccents = function(){
	var accent = [
		/[\300-\306]/g, /[\340-\346]/g, // A, a
		/[\310-\313]/g, /[\350-\353]/g, // E, e
		/[\314-\317]/g, /[\354-\357]/g, // I, i
		/[\322-\330]/g, /[\362-\370]/g, // O, o
		/[\331-\334]/g, /[\371-\374]/g, // U, u
		/[\321]/g, /[\361]/g, // N, n
		/[\307]/g, /[\347]/g, // C, c
	];
        
	var noaccent = ['A','a','E','e','I','i','O','o','U','u','N','n','C','c'];
	
	var str = this;
        
	for(var i = 0; i < accent.length; i++){
		str = str.replace(accent[i], noaccent[i]);
	}
	
	return str;
};

function cleanCharts(){
    $("#chartsDiv").empty();
    dataCharts=[];
}

function createCharts( names, percent){
    cleanCharts();
    
    var classCss = "4";
    
    if(names.length === 4){
       classCss = "3";     
    }
    
    for(var i=0; i < names.length; i++ ) {
        //html
        var divCharts = document.createElement("div");

        //divCharts.setAttribute("class", "col-md-"+classCss);
        
        // col-md-3 distorts the graphics in some cases, using col-md-4 seems to fix that:
        divCharts.setAttribute("class", "col-md-4");
        
        divCharts.setAttribute("style", "height: 260px; margin-bottom: 25px;");
        
        var title = document.createElement("div");
        title.setAttribute("class", "chart-title");
        title.id = "chart" + i;
        title.innerHTML = names[i] + " " + Number(percent[names[i]]).toFixed(0) + "%";
        
        var canvas = document.createElement("canvas");
        canvas.id= "bar" + i;
        canvas.height = 140;
        
        divCharts.appendChild(title);
        divCharts.appendChild(canvas);
        $("#chartsDiv").append(divCharts);
        
        //data
        dataCharts.push({
            labels:[],
            datasets:[{
                    fillColor:['#6d9dd4', '#8cb0cf', '#c2dcf2'],
                    data:[]
            }]
        });
        
    }
}
function showCharts(){
   for(var i=0; i< dataCharts.length; i++){
       if (dataCharts[i].datasets[0].data.length === 0) {
           return;
       }
       
       var min = Math.min.apply(null, dataCharts[i].datasets[0].data);
       
       var elem  = document.getElementById("bar"+i);
       var chart = elem.getContext("2d");
       
       $(elem).css({
           'min-height': '200px'
       });
       
       new Chart(chart).Bar(dataCharts[i], {
        responsive: true,
        inGraphDataShow : true,
        rotateLabels: 0,
        scaleFontSize: 9,
        graphMax: 100,
        graphMin : min-15,
        yAxisMinimumInterval : 5
       });    
   }
   
   //window.dispatchEvent(new Event('resize'));   
};
var randomScalingFactor = function() {
    return Math.round(Math.random() * 100);
};

function chartHeaders(factor, factorName, levelName){
    var score = 0;

    for (var i = 0; i < factor.length; i++) {           
        if(factor[i].department.stripAccents() === levelName.stripAccents()){
            score = factor[i].score;   
        }            
    }

    return score;
}


function fillChart(subfactor, nameSubfactor, namesCharts, levelName){
    //devolver la posiscion del nombre del factor en el array + 1
    var average    = 0;
    var position   = 0;
    var factorName = subfactor[0].factor;
    
    for(var i = 0; i < subfactor.length; i++){
        if(subfactor[i].department.stripAccents() === levelName.stripAccents()){
            average = Math.round(subfactor[i].score);
            
            if(subfactor[i].factor !== factorName){
                 position= (namesCharts.indexOf(subfactor[i].factor));
                 subfactorBar(nameSubfactor, average, position);
            }else{
                position= (namesCharts.indexOf(factorName));
                subfactorBar(nameSubfactor, average, position);
            }
        }

    }
        
}

function subfactorBar(subfactorName, average, position){
    if(subfactorName.indexOf(" ") > 0){
        subfactorName=subfactorName.replace(" ", "\n");
    }
    
    dataCharts[position].labels.push(subfactorName);
    dataCharts[position].datasets[0].data.push(average);
    
}
