/*!
 * fwt.js JavaScript/CSS Library v0.0.1
 * https://github.com/tavuntu/fwt.js
 *
 * Copyright fwt.js
 * Released under the MIT license
 * https://opensource.org/licenses/MIT
 */

var fwt = {};
fwt.tabs = {};

fwt.byId = function(id) {
    return document.getElementById(id);
};

fwt.query = function(q, all, p) {
    var parent = document;
    if (p) {
        parent = p;
    }

    if (all == true) {
        return parent.querySelectorAll(q);
    }
    return parent.querySelector(q);
};

fwt.create = function(tag) {
    return document.createElement(tag);
};

fwt.wrap = function(element, wrapper) {
    element.parentNode.insertBefore(wrapper, element);
    wrapper.appendChild(element);
};

fwt.init = function(tabsId, params) {
    var params = params || {};

    if (!fwt.tabs[tabsId]) {
        fwt.tabs[tabsId] = {
            id: tabsId,
            reference: fwt.byId(tabsId)
        };

        var ref = fwt.tabs[tabsId].reference;
        if(!ref) {
            return;
        }
        
        ref.setAttribute("class", "fwt-container-up");

        var divBody = fwt.create("div");
        divBody.setAttribute("class", "fwt-body");
        divBody.innerHTML = ref.innerHTML;
        ref.innerHTML = divBody.outerHTML;

        var frames = fwt.query("div[data-tab-title], div[data-tab-icon]", true, ref);

        var wrapper = document.createElement("div");
        wrapper.setAttribute("class", "fwt-main-wrapper");
        fwt.wrap(ref, wrapper);

        var divTabs = fwt.create("div");
        divTabs.setAttribute("class", "fwt-tabs-container");
        wrapper.insertBefore(divTabs, wrapper.firstChild);

        //Params:
        if (params.width) {
            //Assume number:
            var w = params.width + "px";
            if (typeof params.width == "string") {
                //Assume percentage:
                w = params.width;
            }
            wrapper.style.maxWidth = w;
            wrapper.style.minWidth = w;
        }

        var defaultTabHeight = "32px";
        var defaultPanelHeight = "320px";
        if (params.height) {
            //Assume number:
            var h = params.height + "px";
            if (typeof params.height == "string") {
                //Assume percentage:
                h = params.height;
            }
            wrapper.style.maxHeight = h;
            wrapper.style.minHeight = h;
        } else {
            wrapper.style.maxHeight = defaultPanelHeight;
            wrapper.style.minHeight = defaultPanelHeight;
        }

        //Iterate frames:
        ref.tabReferences = [];
        for (var i = 0; i < frames.length; i++) {
            var e = frames[i];
            e.setAttribute("class", "fwt-frame fwt-fade-in fwt-is-paused");
            //Tab id on frames:
            e.setAttribute("tab-id", "tab-" + i);

            //Tab:
            var tabText = e.dataset.tabTitle || "";
            var divTab = fwt.create("div");
            divTab.style.padding = "12px";

            ref.tabReferences.push(divTab);

            if (e.dataset.tabIcon) {
                var img = fwt.create("img");
                img.src = e.dataset.tabIcon;
                divTab.appendChild(img);

                if (!tabText) {
                    //Center if only icon:
                    divTab.style.justifyContent = "center"; /* align horizontal */
                } else {
                    img.style.paddingRight = "4px";
                }
            }
            //Explicit center, text, icon or both:
            if (params.centerTabs) {
                divTab.style.justifyContent = "center";
            }

            divTab.setAttribute("class", "fwt-tab fwt-tab-up")
            divTab.innerHTML += tabText;
            divTab.setAttribute("tab-id", "tab-" + i);

            //Params:
            divTab.style.minHeight = params.tabHeight ? (params.tabHeight + "px") : defaultTabHeight;
            divTab.style.maxHeight = params.tabHeight ? (params.tabHeight + "px") : defaultTabHeight;

            if (params.tabWidth) {
                divTab.style.minWidth = "px";
                divTab.style.maxWidth = "px";
            }

            if (params.fixedTabs) {
                divTab.style.width = 1 / frames.length * 100 + "%";
            }

            var arrowDiv = fwt.create("div");
            arrowDiv.classList.add("fwt-arrow-div");
            var tabWrapper = fwt.create("div");

            if (i == 1) {
                divTab.setAttribute("class", "fwt-tab fwt-tab-active-up");
            } else {
                e.style.display = "none";
            }

            var frames = fwt.query("div[data-tab-title], div[data-tab-icon]", true, ref);
            if (frames.length > 0) {
                frames[1].classList.remove("fwt-is-paused");
            }

            divTab.onclick = function() {
                //Select clicked:
                var _this = this;
                var tabs = fwt.query("div.fwt-tab", true, wrapper);
                for (var j = 0; j < tabs.length; j++) {
                    var t = tabs[j];
                    t.setAttribute("class", "fwt-tab fwt-tab-up");
                }
                _this.setAttribute("class", "fwt-tab fwt-tab-active-up");

                //Show corresponding frame:
                for (var j = 0; j < frames.length; j++) {
                    var e = frames[j];
                    if (e.getAttribute("tab-id") == _this.getAttribute("tab-id")) {
                        e.classList.remove("fwt-is-paused");
                        e.style.display = "block";
                    } else {
                        e.classList.add("fwt-is-paused");
                        e.style.display = "none";
                    }
                }
            };
            divTabs.appendChild(divTab);
        }

        return ref.tabReferences;
    }
};