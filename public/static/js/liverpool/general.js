$(function() {
    $('.sidebar .nav').navgoco({
        caretHtml: false,
        accordion: true
    });
    
    $('#toggle-left').on('click', function() {
        var bodyEl = $('#main-wrapper');
        ($(window).width() > 767) ? $(bodyEl).toggleClass('sidebar-mini'): $(bodyEl).toggleClass('sidebar-opened');
    });
    
    /*var doughnutData = [

        {
            value: 70,
            color: "#326697",
            highlight: "#1F7BB6",
            label: "Firefox"
        }
    ];

    var ctx3 = document.getElementById("doughnut-chart-area").getContext("2d");
    
    window.myDoughnut = new Chart(ctx3).Doughnut(doughnutData, {
        responsive: true,
        backgroundColor: '#000000'
    });*/
    
    var randomScalingFactor = function() {
        return Math.round(Math.random() * 100);
    };
    
    var data1 = {
        labels: ["Empresa", "Calidad", "Valores"],        
        datasets: [{
            fillColor: ['#336797', '#6d9dd4'],
            data: [randomScalingFactor(), randomScalingFactor(), randomScalingFactor()]
        }]
    };
    
    var data2 = {
        labels: ["Aprendizaje", "Lugar", "Comunicación", "Entorno"],
        rotateLabels: 0,
        datasets: [{
            fillColor: ['#336797', '#6d9dd4', '#8cb0cf', '#c2dcf2'],
            data: [randomScalingFactor(), randomScalingFactor(), randomScalingFactor(), randomScalingFactor()]
        }]
    };
    
    var data3 = {
        labels: ["Servicio", "Actitud", "Calidad"],
        datasets: [{
            fillColor: ['#336797', '#6d9dd4', '#8cb0cf'],
            data: [randomScalingFactor(), randomScalingFactor(), randomScalingFactor()]
        }]
    };
    
    var data4 = {
        labels: ["Colaboración", "Participación", "Apoyo"],
        datasets: [{
            fillColor: ['#336797', '#6d9dd4', '#8cb0cf'],
            data: [randomScalingFactor(), randomScalingFactor(), randomScalingFactor()]
        }]
    };
    
    var bar1 = document.getElementById("bar1").getContext("2d");
    var bar2 = document.getElementById("bar2").getContext("2d");
    var bar3 = document.getElementById("bar3").getContext("2d");
    var bar4 = document.getElementById("bar4").getContext("2d");
    
    new Chart(bar1).Bar(data1, {
        responsive: true,
        rotateLabels: 0,
        scaleFontSize: 9
    });
    
    new Chart(bar2).Bar(data2, {
        responsive: true,
        rotateLabels: 0,
        scaleFontSize: 9
    });
    
    new Chart(bar3).Bar(data3, {
        responsive: true,
        rotateLabels: 0,
        scaleFontSize: 9
    });
    
    new Chart(bar4).Bar(data4, {
        responsive: true,
        rotateLabels: 0,
        scaleFontSize: 9
    });
});
        